/*******************************************************************************

                                 TimeUtils.h
                                 -----------

  ****************************************************************************
  *                                                                          *
  *   This library is free software; you can redistribute it and/or          *
  *   modify it under the terms of the GNU Lesser General Public             *
  *   License as published by the Free Software Foundation; either           *
  *   version 2.1 of the License, or (at your option) any later version.     *
  *                                                                          *
  *   This library is distributed in the hope that it will be useful,        *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *
  *   Lesser General Public License for more details.                        *
  *                                                                          *
  *   You should have received a copy of the GNU Lesser General Public       *
  *   License along with this library; if not, write to the Free Software    *
  *   Foundation, Inc.,                                                      *
  *   59 Temple Place, Suite 330, Boston, MA  02111-1307  USA                *
  *                                                                          *
  ****************************************************************************

  Copyright (C) 2005 The University of Chicago

  Authors:
  Andres Colubri

  Description:
  Defines the Chronometer class and the ConvTime function.

*******************************************************************************/

#ifndef __TIME_UTILS_H__
#define __TIME_UTILS_H__

#include <ctime>

enum TTimeUnit {tuSec, tuMin, tuHour, tuDay};

// Returns time0 (given in unit0) converted to unit1.
double ConvTime(double time0, TTimeUnit unit0, TTimeUnit unit1);

// This class allows to calculate time intervals.
class TChronometer
{
public:
    void Start() { time_start = clock(); } // Starts counting.
    void Save() // Records current time interval and keeps counting.
    { time_end = clock(); time_diff = (time_end - time_start) / CLOCKS_PER_SEC; }
    void Stop() { Save(); Start(); } // Stops counting.
    double TimeDiff(TTimeUnit unit = tuSec) // Time interval in the specified unit.
    { return ConvTime(time_diff, tuSec, unit); }
private:
    clock_t time_start, time_end;
    double time_diff;
};

#endif
