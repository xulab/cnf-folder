#ifndef _RESTYPE
#define _RESTYPE
//#include <vector>
#include "misc.h"
using namespace std;

class Atomtype{
	int type;
	string name;
public:
	Atomtype(int ia, const string an){type=ia; name=an;};
	bool isname(const string an);
	string getname(){return name;};
	int gettype(){return type;};
};
//
class Restype{
	int type;
	string name;
	vector <string> neibs[2];
	vector <Atomtype> atypes;
public:
	Restype(){};
	Restype(int t, const string s){type=t; name=s;}; 
	void setRestype(const string s){name=s;}; 
	void addatom(const string s);
	void addatom(int type, const string s){atypes.push_back(Atomtype(type,s));};
	void addneib(const string s, const string s2);
	string getname(){return name;};
	vector <Atomtype> *getatypes(){return &atypes;};
	vector <string> *getneibs(){return neibs;};
	void prtneib();
	int getnatm(){return atypes.size();};
	int define(string anam, int &pos);
};
/*class ProRestype{
	vector <Restype> rtypes;
public:
	ProRestype();
	void prtneib();
	int resDefine(string rnam);
	Restype *getrtype(int id){return &rtypes.at(id);};
	int getnatm(int id){return rtypes.at(id).getnatm();};
	int define(int id, string anam, int &pos){rtypes.at(id).define(anam, pos);};
};*/
extern vector <Restype> PRtype;
extern int nrestype;
extern const char rnam3_std[][4];
extern const char rnam1_std[];

void initPRtypes(int);
int resDefine(string rn);
int resDefine1(char rn);
int defineAtype(string rn, string an);
void initPRtypes0(int flag);

#endif
