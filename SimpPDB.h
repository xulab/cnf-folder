#ifndef _SIMP_PDB
#define _SIMP_PDB

#define LONGEST_CHAIN 2000
#include <iostream>

using namespace std;
class SimPDB
{
public:
	const char* mProteinFileName;
     	int mNumResidue;
     	double* mCAlpha;
     	void read();

public:
      SimPDB(const char* aProteinFileName);
      ~SimPDB();
};
#endif
