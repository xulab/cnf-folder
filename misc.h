#ifndef _MISC
#define _MISC
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <iostream>
#include <cstdio>
#include <cstdarg>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

extern int DEBUG;
/*template <class T>
inline bool str2dat(const string &line, T &data, ios_base & (*fmt)(ios_base&)) {
   istringstream iss(line);
   return !(iss>>fmt>>data).fail();
}*/
template <class T>
inline int str2dat(const string &line, T *data){
   istringstream iss(line);
	int n=0;
   while(! (iss>>data[n]).fail() ) n++;
	return n;
}
template <class T>
inline bool str2dat(const string &line, const int n, T *data){
   istringstream iss(line);
	for(int i=0; i<n; i++){
   	if( (iss>>data[i]).fail() )  return false;
	}
	return true;
}
template <class T>
inline int str2dat(const string &line, vector<T> &data){
   istringstream iss(line);
	T dt; int count=0;
   while(! (iss>>dt).fail() ){
		data.push_back(dt);
		count ++;
	}
	return count;
}
template <class T>
inline T sum(int n, T *x){
	T s=0;
	for(int i=0; i<n; i++) s += x[i];
	return s;
}
inline void cross_product(double *r1,double *r2,double *val){
    val[0] = r1[1]*r2[2]-r1[2]*r2[1];
    val[1] = -r1[0]*r2[2]+r1[2]*r2[0];
    val[2] = r1[0]*r2[1]-r1[1]*r2[0];
}
inline double dot_product(int n, double *r1,double *r2){
    double value=0;
    for(int i=0;i<n;i++) value += r1[i]*r2[i];
    return value;
}
inline double dot_product(double *r1,double *r2){
	return dot_product(3, r1, r2);
}
/*
inline double normalize(double *x){
	double r = sqrt(dot_product(x, x));
	for(int m=0; m<3; m++) x[m] /= max(r, 1.0e-4);
	return r;
}
*/
inline string trimstr(string line){
	int i, is, ie, size=line.size();
	if(size==0) return line;
	for(i=0; i<size; i++)
		if(line.at(i) != ' ') break;
	is = i;
	for(i=size-1; i>=0; i--)
		if(line.at(i) != ' ') break;
	ie = i;
	return line.substr(is,ie-is+1);
}
inline FILE *openfile(const string file, const string action){
	FILE *fp = fopen(file.c_str(), action.c_str());
	if(fp == NULL) fprintf(stderr, "Warning, Fail to open file: %s\n", file.c_str());
	return fp;
}
inline int findargs(int argc, char *argv[], const string tstr){
	for(int i=0; i<argc; i++)
		if(tstr == argv[i]) return i;
	return -1;
}
template <class T>
inline void prtdim(int n, T data){
	for(int i=0; i<n; i++) cout<<'\t'<<data[i];
	cout<<endl;
}
template <class T>
inline T maxval(int n, T data){
	T t0 = data[0];
	for(int i=1; i<n; i++){
		if(data[i]>t0) t0 = data[i];
	}
}
template <class T>
inline T minval(int n, T data){
	T t0 = data[0];
	for(int i=1; i<n; i++){
		if(data[i]<t0) t0 = data[i];
	}
}
template <class T>
inline void copy(int n, T src, T targ){
	for(int i=0; i<n; i++) targ[i] = src[i];
}
template <class T>
inline void swap(T &a1, T &a2){
	T a3 = a1; a1 = a2; a2 = a3;
}
inline void die(char *fmt, ...){
	va_list ptr; va_start(ptr, fmt);
	vfprintf(stderr, fmt, ptr);
	va_end(ptr);
	exit(1);
}
#endif
