#ifndef _UTIL_H
#define _UTIL_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#include "IP_constant.h"

typedef struct _RAND_NODE{
	int number;
	void* data;
}RAND_NODE;


class ToolKit {

	public:

	static int isEmpty(char* s);
	static int isAA(char a);
	static int round(float x);
	static void allege(bool b, char* message);
	static float sumOfColumn(float* m,int c, int rSize, int cSize);
	static float sumOfRow(float* m,int r, int rSize, int cSize);
	static float sumOfMatrix(float* m, int rSize, int cSize);

	//flag 1 for ascending, 0 for descending
	static void sort(double* list, int size, bool flag);
	static void sort(int* list, int size, bool flag);

	static int GEdouble(const void* p1, const void* p2);
	static int LEdouble(const void* p1, const void* p2);
	static int GEint(const void* p1, const void* p2);
	static int LEint(const void* p1, const void* p2);

	static void ShuffleArray(int* src, int* dest, int len); //shuffle an array src, store result to dest
	static char AlignSymbol(int res1, int res2);



};


unsigned long pow3(int x);
unsigned long pow2(int x);
unsigned long pow(int x,int y);

#define ABS(x) ((x)>0?(x):(-(x)))
#define SQUARE(x) ((x)*(x))

#define MAX2(x,y)	((x)>(y)?(x):(y))
#define MAX(x,y) 	MAX2(x,y)
#define MAX3(x,y,z) 	MAX2((z),MAX2((x),(y)))
#define MIN2(x,y)	((x)>(y)?(y):(x))
#define MIN(x,y)	MIN2(x,y)
#define MIN3(x,y,z)	MIN2((z),MIN2((x),(y)))
#define min(x,y)	MIN(x,y)
#define min2(x,y) 	MIN2(x,y)
#define min3(x,y)	MIN3(x,y)
#define max(x,y)	MAX(x,y)
#define max2(x,y)	MAX2(x,y)
#define max3(x,y,z)	MAX3(x,y,z)

//match two protein sequences, output the position alignment information
//pos_align, s1[i] is aligned to s2[pos_align[i]] if pos_align[i]>=0
//otherwise, s1[i] is aligned to nowhere
//we align s1 and s2 to maximize the common residues
//gaps are allowed in alignment
//
//the caller must allocate enough memory for pos_align
//
//return value is the number of alignment positions

int Match2Seq(char* s1,int len1, char* s2, int len2, int* pos_align);

#endif

