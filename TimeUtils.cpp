/*******************************************************************************

                                TimeUtils.cpp
                                -------------

  ****************************************************************************
  *                                                                          *
  *   This library is free software; you can redistribute it and/or          *
  *   modify it under the terms of the GNU Lesser General Public             *
  *   License as published by the Free Software Foundation; either           *
  *   version 2.1 of the License, or (at your option) any later version.     *
  *                                                                          *
  *   This library is distributed in the hope that it will be useful,        *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *
  *   Lesser General Public License for more details.                        *
  *                                                                          *
  *   You should have received a copy of the GNU Lesser General Public       *
  *   License along with this library; if not, write to the Free Software    *
  *   Foundation, Inc.,                                                      *
  *   59 Temple Place, Suite 330, Boston, MA  02111-1307  USA                *
  *                                                                          *
  ****************************************************************************

  Copyright (C) 2005 The University of Chicago

  Authors:
  Andres Colubri

  Description:
  Implements the Chronometer class and the ConvTime function.

*******************************************************************************/

#include "TimeUtils.h"

// ConvCoef[i] contains the radio of time unit i (as defined in the enumerated
// type TTimeUnit) ba time i-1. Value 0 is meaningless.
const int ConvCoef[4] = {1, 60, 60, 24};

double ConvTime(double time0, TTimeUnit unit0, TTimeUnit unit1)
{
    int i;
    double time1;

    time1 = time0;

    if (unit0 < unit1)
        for (i = unit0+1; i <= unit1; i++) time1 = time1 / ConvCoef[i];
    else
        for (i = unit1+1; i <= unit0; i++) time1 = time1 * ConvCoef[i];

    return time1;
}


