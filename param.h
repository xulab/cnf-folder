#ifndef _PARAM_H
#define _PARAM_H

#include <stdio.h>
#include <stdlib.h>

#include "IP_constant.h"
#include "config.h"

#define MaxSolventIntervalNum	10
#define MaxContactNum	20

#define CONTACT_T_GENERAL	0
#define CONTACT_T_SHORT		1
#define CONTACT_T_LONG		2

class CParam {

#define PAIR_INTERACTION	0
#define MUTATION		1
#define DISTINDEPENDENT		2
#define DISTDEPEND5		5
#define DISTDEPEND7		7
#define DISTDEPEND9		9
#define DISTDEPEND11		11
#define DISTDEPEND13		13
#define PPIINTERACTION		100


	private:
		Config* config;
	
		int pairInteraction[ResidueTypes][ResidueTypes];
		int mutation[ResidueTypes][ResidueTypes];
		int singleton[ResidueTypes][3][3];
		int pairInteraction5[ResidueTypes][ResidueTypes];
		int pairInteraction7[ResidueTypes][ResidueTypes];
		int pairInteraction9[ResidueTypes][ResidueTypes];
		int pairInteraction11[ResidueTypes][ResidueTypes];
		int pairInteraction13[ResidueTypes][ResidueTypes];

		int PPIpairInteraction[ResidueTypes][ResidueTypes];
		
		float solventAcc[ResidueTypes][MaxSolventIntervalNum][3];
		int solventIntervalNum;
		float solventBound[MaxSolventIntervalNum];


		float contactCapacity[MaxContactNum][ResidueTypes][StructTypes];
		float sContactCapacity[MaxContactNum][ResidueTypes][StructTypes];
		float lContactCapacity[MaxContactNum][ResidueTypes][StructTypes];

		int maxContactNum[StructTypes];
		int maxsContactNum[StructTypes];
		int maxlContactNum[StructTypes];

		float volumeContact[ResidueTypes][ResidueTypes+1];
		float volumeSolvent[ResidueTypes];

		int pairInteractionAver[ResidueTypes];
		int mutationAver[ResidueTypes];
		int singletonAver[ResidueTypes];

		bool singletonIsReady;
		bool mutationIsReady;
		bool pairInteractionIsReady;
		bool contactCapacityIsReady;
		bool solventIsReady;	//solvent param is new parameters for singleton



	public:
		CParam();
		~CParam();

		void setConfig(Config* conf){config=conf;};

		//input function	
		//void InputPairInteraction(char* filename);
		void InputPairInteraction(char* filename,int flag);
		void InputMutation(char* filename);
		void InputSingleton(char* filename);
		void InputContactCapacity(char* filename);
		void InputSolvent(char* filename);
		void InputVolumeContact(char* filename);

		//access methods

		int getPPIPairInteraction(int i, int j, float dist=-1);

		int getPairInteraction(int i,int j,float dist);
		int getMutation(int i,int j);
		int getSingleton(int residue,int ss,int solventType,int solvent);
		float getSolvent(int residue,int ss,int solventType,int solvent);

		float getContactCapacity(int cNum,int residue,int ss,int type=CONTACT_T_GENERAL);

		int getPairInteractionAver(int i);
		int getMutationAver(int i);
		int getSingletonAver(int i);

		float getVolumeContact(int lResidue, int rResidue);
		float getVolumeSolvent(int residue);

		//trace method
		void printPairInteraction();
		void printMutation();
		void printSingleton();
		void print();

		void CalcAver();
	private:
		
		//void InputPairTable(char* filename, int flag);
		void InputPairTable(char* filename, int dest[ResidueTypes][ResidueTypes]);
};




#endif

