#ifndef WIN32
#include <unistd.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "IP_constant.h"
#include "template.h"
#include "config.h"
#include "util.h"
#include "param.h"
#include "psp.h"



char* AA2Name[21]={"ALA","ARG","ASN","ASP","CYS","GLN","GLU", "GLY","HIS","ILE","LEU","LYS","MET","PHE","PRO","SER","THR", "TRP","TYR","VAL","NON"};


TEMPLATE::TEMPLATE(char* tName){

       if (tName==0){
       	//report
      		fprintf(stderr,"fatal error: template name is null\n!");
		
       		exit(-1);
       	}

       	templateName=new char[strlen(tName)+1];

       	strncpy(templateName,tName,strlen(tName));
       	templateName[strlen(tName)]=0;

       	memset(templateSeq,0,MaxTemplateSize*sizeof(TemplateSingleton));
	cores=new CORE[MaxCoreSize];
       	memset(cores,0,MaxCoreSize*sizeof(CORE));
       	memset(linkGraph,0,MaxCoreSize*MaxCoreSize*sizeof(int));

	gapEdgeAdded=false;

	flag_pruneEdge=false;

	//isProspectTreeAvailable=false;

	//HBondAvailable=false;

	contactList=new CONTACT[MaxTemplateSize*20];

	VolContact_array=0;

       	//topComplexityMode=-1;

//     	getContent();


}

TEMPLATE::~TEMPLATE(){

		if (templateName)	delete templateName;

		//if (PSP_array) delete []PSP_array;
		if (PSM_array) delete []PSM_array;
		if (cores) delete []cores;
		if (VolContact_array) delete []VolContact_array;
		if (contactList) delete []contactList;
}

char* TEMPLATE::getName(){
        return templateName;
}

char TEMPLATE::getChainChar(){
	if (strlen(templateName)<=4) return 0;
	if (templateName[4]!='_')	return templateName[4];
	else return 0;
}

TemplateSingleton* TEMPLATE::operator[] (int i){
	ToolKit::allege(i>=0 && i<templateSize,"operator [] out of range in TEMPLATE");
	return templateSeq+i;
}


CORE* TEMPLATE::getCore(int i){
	ToolKit::allege(i>=0 && i<coreSize,"out of range in access core");
	return cores+i;
}

CORE* TEMPLATE::getCores(){
	return cores;
}

int TEMPLATE::getLinkGraph(int i,int j){
	ToolKit::allege(i>=0 && i<coreSize && j>=0 && j<coreSize, "out of bound in access of linkGraph");
	return linkGraph[i][j];
}
int TEMPLATE::getCutLinkNum(int i,int j){

	int cut=0;
	for(int l=i;l<=j;l++)
		for(int r=0;r<coreSize;r++){
			if (r>=i && r<=j) continue;
			cut+=linkGraph[l][r];
		}
	return cut;
}

double TEMPLATE::getScoreGraph(int i,int j){
	ToolKit::allege(i>=0 && i<coreSize && j>=0 && j<coreSize, "out of bound in access of linkGraph");
	return scoreGraph[i][j];
}
void TEMPLATE::setConfig(Config* conf){
	if (conf==0){
	      	fprintf(stderr,"conf is null!\n");
	}
       	config=conf;
}


void TEMPLATE::printSeq(){
       	printf("\n%s:",templateName);
       	for(int i=0;i<templateSize;i++)
       		printf("%c %f %f %f\n",templateSeq[i].cResidue,templateSeq[i].xPos,templateSeq[i].yPos,templateSeq[i].zPos);
       	printf("\n");

}

void TEMPLATE::printCore(){
	printf("\ncores of %s (leftSeqPos,rightSeqPos,type):\n",templateName);
       	int coreLen=0;

       	for(int i=0;i<coreSize;i++){
       		printf("[%d,%d,%d] ",cores[i].leftSeqPos,cores[i].rightSeqPos,cores[i].coreType);
       		coreLen+=cores[i].rightSeqPos-cores[i].leftSeqPos+1;

       	}
       	printf("\ncoreLen=%d,coresize=%d\n",coreLen,coreSize);



}


void TEMPLATE::getContent(){

/*
	char absFilePath[MaxFileNameLength];
	memset(absFilePath,0,MaxFileNameLength*sizeof(char));
	sprintf(absFilePath,"%s%s.%s",config->TemplatePath,templateName,config->TemplateType);

	FILE* fp=fopen(absFilePath,"r");

	if (fp==0){

		//report err
		char s[1024];
		sprintf(s,"can't open the file %s for read!",absFilePath);
		config->writeErrorLog(s);
		exit(-1);

	}

	int i=0;

	char s[MaxLineSize];


	while(fgets(s,MaxLineSize,fp)){

		if (ToolKit::isEmpty(s)) continue;

//		printf("s=%s\n",s);

		StringTokenizer strToken(s," \t\n\r");
		char* nextToken=strToken.getNextToken();

		if (nextToken==NULL || !nextToken[0]) continue;


		if (strncmp(nextToken,"RES",3)==0){

			addOneResidue(i,&strToken,s); //insert this residue to i-th pos 
			i++;
		}


	}

	templateSize=i;



	fclose(fp);

	if (templateSize>MaxTemplateSize){
		char str[4096];
		sprintf(str,"fatal error: template size (%d) is beyond the maximum allowed %d\n",templateSize, MaxTemplateSize);
		fprintf(stderr,"%s",str);
		exit(-1);
	}

        printf("finish reading template information from its template file!\n");

	//collect the alphabetical representation of this template sequence
	for(i=0;i<templateSize;i++)
		seq[i]=templateSeq[i].cResidue;
	seq[i]=0;


	//begin to read PSM information
	PSM_array=new float[templateSize*21];
	PSM[0]=PSM_array;
	for(i=1;i<templateSize;i++)
		PSM[i]=PSM[i-1]+21;
	memset(PSM_array,0,sizeof(float)*templateSize*21);


		VolContact_array=new float[templateSize*templateSize];
		memset(VolContact_array,0,sizeof(float)*templateSize*templateSize);
		for(int i=0;i<templateSize;i++)
			VolContact[i]=VolContact_array+i*templateSize;

		//read .vol files 
		char volfile[MaxLineSize];
		sprintf(volfile,"%s%s.vol",config->TempVolPath,templateName);
		if (config->debug_level>=DEBUG_LEVEL3)
			printf("loading volume file for template %s\n",templateName);
		LoadVolContact(volfile);
	}
*/

	Load();

//	extractCore();
//	calcLinks();
//	calcContacts();

	//compute loopSize;
	//if (config->debug_level>=DEBUG_LEVEL2)
	//	printf("estimating loop size...\n");
	//estimateMinLoopSize();

}

void TEMPLATE::LoadVolContact(char* file){
	FILE* fp=fopen(file,"r");
	if (!fp){
		fprintf(stderr,"failed to open %s for read\n",file);
		exit(-1);
	}
	char buf[MaxLineSize];
	while(fgets(buf,MaxLineSize,fp)){
		char resID1[10];
		char resID2[10];
		char AA1[4];
		char AA2[4];
		float volContact;

		int ret=sscanf(buf,"%s%s%s%s%f",resID1,AA1,resID2,AA2,&volContact);
		if (ret<5) continue;
		
		int index1=ResidueIndexByResID(resID1);
		if (index1<0) continue;
		if (strcasecmp(resID2,"-1")==0){
			templateSeq[index1].volSolvent=volContact;
		}
		else if(strcasecmp(resID2,"-10")==0){
			templateSeq[index1].subunitSolvent=volContact;
		}else{
			int index2=ResidueIndexByResID(resID2);
			if (index2>=0)
				VolContact[index1][index2]=volContact;
		}

	}
	fclose(fp);
}

float TEMPLATE::VolumeContact(int i, int j){
	if (config->UseVolume){
		if (i<0 || i>=templateSize){
			fprintf(stderr,"ERROR:i =%d out of bound in TEMPLATE::VolumeContact\n",i);
			exit(-1);
		}
		if (j<0 || j>=templateSize){
			fprintf(stderr,"ERROR:j = %d out of bound in TEMPLATE::VolumeContact\n",j);
			exit(-1);
		}

		return VolContact[i][j];
	}
	else return 0;
}

int TEMPLATE::LoadPSMInfo(char* file,float* PSM_array){

/*
	if (config->debug_level >=DEBUG_LEVEL3)
		printf("open the PSM file %s for read...\n",file);
*/

	FILE* fp=fopen(file,"r");
	if (!fp){
		//write log
		char s[1024];
		sprintf(s,"failed to open file %s for read\n",file);
		config->writeErrorLog(s);
		return 0;
	}

/*
	if (config->debug_level>=DEBUG_LEVEL3)
		printf("read the PSM file %s line-by-line...\n",file);
*/

	bool good=true;
	char s[4096];
	int lineCount=0;
	float* a=PSM_array;
	while(lineCount<templateSize && fgets(s,4096,fp)){
		if (strstr(s,"null")!=0) {
			good=false;
			break;
		}
	
		if (ToolKit::isEmpty(s)) continue;
		
	
/*	
		if (config->debug_level>=DEBUG_LEVEL3)
			printf("got a line=%s",s);
*/

		int ret=sscanf(s,"%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f",a,a+1,a+2, a+3,a+4,a+5,a+6,a+7,a+8,a+9,a+10,a+11,a+12,a+13,a+14,a+15,a+16,a+17,a+18,a+19);
		if (ret<20){
			char msg[1024];
			sprintf(msg,"error in file %s:%s\n",file,s);
			config->writeErrorLog(msg);
			good=false;
			break;
			//continue;
		}else a=a+21;
		lineCount++;
	}
	fclose(fp);

	if (lineCount!=templateSize){
		char msg[1024];
		sprintf(msg,"%s: inconsistent between PSM and FSSP\n",file);
		config->writeErrorLog(msg);
		 good=false;
	}

	if (!good) return 0;


	for(int i=0;i<templateSize;i++)
		for(int j=0;j<20;j++)
			PSM[i][j]*=-0.1;
	return 1;


}

int TEMPLATE::getTemplateSize(){
	return templateSize;
}

int TEMPLATE::getCoreSize(){
	return coreSize;
}


void TEMPLATE::addOneResidue(int i, StringTokenizer* strToken, char* buf){

	if (strToken==0 || i<0)	{
		//report err
		fprintf(stderr,"add one residue err!");
		return;
	}

	char* s;


	
	s=strToken->getNextToken();
	templateSeq[i].flag=atoi(s);


	s=strToken->getNextToken();

	if (s==0){
		fprintf(stderr,"strToken is null!");

	}
	sprintf(templateSeq[i].buf,"%s",buf);

	templateSeq[i].cResidue=toupper(s[0]);

	templateSeq[i].residue=(short)AA1Coding[AA2SUB[templateSeq[i].cResidue-'A']];

	//
	s=strToken->getNextToken();
	int len=strlen(s);

	if (s[len-1]>='0' || s[len-1]<='9'){
		templateSeq[i].extraChar=0;
		templateSeq[i].resID=atoi(s);
	}else{
		templateSeq[i].extraChar=s[len-1];
		s[len-1]=0;
		templateSeq[i].resID=atoi(s);
	}

	templateSeq[i].gapFlag=0;


	//get the secondary struct type

	s=strToken->getNextToken();

	switch(s[0]){
		case 'H':
			templateSeq[i].structType=(short)HELIX;
			break;
		case 'E':
			templateSeq[i].structType=(short)SHEET;
			break;
		default:
			templateSeq[i].structType=(short)LOOP;
			break;

	}

	//get the solventType

	templateSeq[i].solventType=atoi(strToken->getNextToken());

	templateSeq[i].solvent=templateSeq[i].solventType;

	if ( templateSeq[i].flag && maxAcc[templateSeq[i].residue]>0){
		float acc=100.0*templateSeq[i].solventType/maxAcc[templateSeq[i].residue];
		if (acc<=BuryCore1)
			templateSeq[i].solventType=(short)BURIED;
		else if (acc>BuryCore2)
			templateSeq[i].solventType=(short)EXPOSED;
		else
			templateSeq[i].solventType=(short)INTERMEDIATE;
	

	}else templateSeq[i].solventType=(short)NO_ASSIGNMENT;

	s=strToken->getNextToken();
	templateSeq[i].xPos=atof(s);
	templateSeq[i].yPos=atof(strToken->getNextToken());
	templateSeq[i].zPos=atof(strToken->getNextToken());
	templateSeq[i].xPos2=atof(strToken->getNextToken());
	templateSeq[i].yPos2=atof(strToken->getNextToken());
	templateSeq[i].zPos2=atof(strToken->getNextToken());

	//printf("after read the template content!");
}


void TEMPLATE::setTemplate(){

	//insert code here to preprocess the gap

}

void TEMPLATE::extractCore(){
	
	if (templateSize<=0){
		fprintf(stderr,"template size is 0!");
		return;
	}


	short ss; //secondary struct type
	short ss_prev; //the last secondary struct type;


	int coreIndex=-1;
	int tempIndex=0;
	int entry;


	ss_prev=NO_ASSIGNMENT;


	while(tempIndex<templateSize ){

		ss=templateSeq[tempIndex].structType;
		entry=templateSeq[tempIndex].flag;

		while(tempIndex<templateSize && !((ss==HELIX || ss==SHEET) && entry>=CoreEntry)){
			tempIndex++;
			ss=templateSeq[tempIndex].structType;
			entry=templateSeq[tempIndex].flag;
		}


		if (tempIndex>=templateSize)	break;


		ss_prev=ss;



		//enter into the new core

		coreIndex++;
			
		cores[coreIndex].leftSeqPos=tempIndex;
		cores[coreIndex].coreType=ss;




		while(tempIndex<templateSize && ss==ss_prev && entry>=CoreEntry)
		{
			tempIndex++;
			ss=templateSeq[tempIndex].structType;
			entry=templateSeq[tempIndex].flag;
		}

		//end of the core
		cores[coreIndex].rightSeqPos=tempIndex-1;
		cores[coreIndex].len=cores[coreIndex].rightSeqPos-cores[coreIndex].leftSeqPos+1;

		if (cores[coreIndex].len<config->MinCoreLen){
			//skip this core
			coreIndex--;
		}





	}



	coreSize=coreIndex+1;

	for(int l=0;l<templateSize;l++)
		templateSeq[l].secondaryStruct=-1;

	for(int i=0;i<coreSize;i++)
		for(int l=cores[i].leftSeqPos;l<=cores[i].rightSeqPos;l++)
			templateSeq[l].secondaryStruct=i;

	if (coreSize==0 && config->debug_level>=DEBUG_LEVEL2){
		printf("%s is a template without cores!\n",getName());
	}

	if (config->debug_level>=DEBUG_LEVEL3)
		printf("coreSize of template %s is %d\n",templateName,coreSize);

	if (coreSize>MaxCoreSize){
		char str[4096];
		sprintf(str,"fatal error:coreSize (%d) is beyond the allowed %d\n",coreSize, MaxCoreSize);
		fprintf(stderr,"%s",str);
		config->writeErrorLog(str);
		exit(-1);
	}

}


void TEMPLATE::calcLinks(){

	if (coreSize<=0) return;

	//printCore();

	int i;
	for( i=0;i<coreSize;i++){
		cores[i].localLinkNum=0;
		cores[i].leftLinkNum=0;
		cores[i].rightLinkNum=0;
	}

	//compute the internal links

	calcInternalLinks();

	//compute the external links
	calcExternalLinks();

/* commented on Jan 14, 2004 by Jinbo Xu, it won't improve alignment accuracy by doing so

	//delete invalid cores, if the external link num of one core
	//is too small, then there is a great chance that this core
	//wouldn't be conserved in some case, so treat it as loop

	int deleteFlag[MaxCoreSize];
	memset(deleteFlag,0,sizeof(int)*MaxCoreSize);

	for(i=0;i<coreSize;i++){
		if (cores[i].leftLinkNum+cores[i].rightLinkNum<config->MinExLinkNum){
			//delete this core
			deleteFlag[i]=1;
		}
	}

	CORE tmpCores[MaxCoreSize];
	int j=0;
	for(i=0;i<coreSize;i++){
		if (!deleteFlag[i]){
			memcpy(tmpCores+j,cores+i,sizeof(CORE));
			j++;
		}
	}

	memcpy(cores,tmpCores,sizeof(CORE)*j);
	coreSize=j;
	
	//printCore();

	//recalculate links
	if (coreSize<=0) return;

	for( i=0;i<coreSize;i++){
		cores[i].localLinkNum=0;
		cores[i].leftLinkNum=0;
		cores[i].rightLinkNum=0;
	}
	calcInternalLinks();
	calcExternalLinks();

*/


	//compute link graph
	for(i=0;i<coreSize;i++)
		for(int j=i;j<coreSize;j++)
			linkGraph[i][j]=0;

	for(i=0;i<coreSize;i++){
		linkGraph[i][i]=cores[i].localLinkNum;

		for(int j=0;j<cores[i].rightLinkNum;j++)
			linkGraph[i][cores[i].rightLinks[j].rCoreIndex]++;
	}

	for(i=0;i<coreSize;i++)
		for(int j=0;j<i;j++)
			linkGraph[i][j]=linkGraph[j][i];



}

void TEMPLATE::printLink(){

	printf("\n links of cores (localLink,leftLink,rightLink)");
	for(int i=0;i<coreSize;i++){
		printf("[%d,%d,%d]",cores[i].localLinkNum,cores[i].leftLinkNum,cores[i].rightLinkNum);
		printf("  ");
	}


	outputLinkGraph();

}

void TEMPLATE::outputLinkGraph(){

	if (coreSize<=0) return;

	int i;

	printf("link graph:\n");
	for(i=0;i<coreSize;i++){
		printf("\n");
		for(int j=0;j<coreSize;j++)
			printf("%3d ",linkGraph[i][j]);
	}
	printf("\n");

	for(int k=1;k<=coreSize-1;k++)
		for(int i=0;i<coreSize;i++){
			if (linkGraph[i][i+k]>0)
				printf("(%d %d : %d)\n",i,i+k,linkGraph[i][i+k]);
		}

	if (!config->PrintPS)  return;

	//output a PS file for this graph
	char fileName[256];
	sprintf(fileName,"%s.ps",templateName);

	FILE* fp=fopen(fileName,"w");
	if (!fp){
		fprintf(stderr,"cannot open %s for write!\n",fileName);
		exit(-1);
	}
	
	fprintf(fp,"%!Postscript\n");


	int leftX=0;
	int rightX=590;
	int xWidth=(rightX-leftX)/(coreSize-1);
	int y=200;

	int Xs[MaxCoreSize];

	for(i=0;i<coreSize;i++)
		Xs[i]=leftX+xWidth*i;

	//draw points
	int radius=2;

	for(i=0;i<coreSize;i++)
		fprintf(fp,"%d %d %d 0 360 arc\n",Xs[i],y,radius);

	fprintf(fp,"/Hevlvetica findfont 12 scalefont setfont\n");
	for(i=0;i<coreSize;i++)
		fprintf(fp,"%d %d moveto (%d) show\n",Xs[i],y-8,i);
	for(i=0;i<coreSize;i++)
		fprintf(fp,"%d %d moveto (%d) show\n",Xs[i],y-20,cores[i].coreType);

	fprintf(fp,"stroke\n");
	//draw lines
	fprintf(fp,"newpath\n");
	fprintf(fp,"%d %d moveto\n",Xs[0],y);
	for(i=1;i<coreSize;i++)
		fprintf(fp,"%d %d lineto\n",Xs[i],y);
	fprintf(fp,"closepath stroke\n");
	

	//draw edges
	fprintf(fp,"newpath\n");
	for(i=0;i<coreSize;i++)
		for(int j=i+2;j<coreSize;j++)
			if (linkGraph[i][j]>0){
				int x=Xs[i]+Xs[j];
				x/=2;
				int r=Xs[j]-Xs[i];
				r/=2;
				fprintf(fp,"%d %d %d 0 180 arc\n",x,y,r);

			}

	fprintf(fp,"closepath stroke\n");

	fprintf(fp,"/Hevlvetica findfont 12 scalefont setfont\n");

	for(i=0;i<coreSize;i++)
		for(int j=i+2;j<coreSize;j++)
			if (linkGraph[i][j]>0){
				int x=Xs[i]+Xs[j];
				x/=2;
				int r=Xs[j]-Xs[i];
				r/=2;

				int y_text=y+r+2;
				fprintf(fp,"%d %d moveto (%d) show\n",x,y_text,linkGraph[i][j]);
			}


	fprintf(fp,"showpage\n");

	if (fp) {
		fclose(fp);
	}

}




void TEMPLATE::calcInternalLinks(){


	if (coreSize<=0) return;	

	if (config==0){
		fprintf(stderr,"config is null!\n");
		exit(-1);
	}

	if (config->debug_level>=DEBUG_LEVEL2)
		printf("calculate internal links...\n");

	for(int i=0;i<coreSize;i++)
	{

		if (cores[i].coreType==LOOP)	continue;


		//compute the internal links
		for(int idx1=cores[i].leftSeqPos;idx1<=cores[i].rightSeqPos;idx1++){
			//TemplateSingleton* res1=templateSeq+idx1;


			for(int idx2=idx1+1;idx2<=cores[i].rightSeqPos;idx2++)
			{

				if (cores[i].localLinkNum>=MaxLocalLinkSize){
					char message[MaxLineSize];
					sprintf(message,"fatal error: local link number %d goes beyond MaxLocalLinkSize (%d)!\n",cores[i].localLinkNum,MaxLocalLinkSize);
					config->writeErrorLog(message);
					fprintf(stderr,"%s",message);
					exit(-1);
				}

				LINK* link=cores[i].localLinks+cores[i].localLinkNum;

				if (config->UseVolume){

#define ZEROVOLCONTACT(x)	(fabs(x)<=0.01)

					//these codes are for Brendan's contact definition
					//contacts are not symmetry in this sense
					if (ZEROVOLCONTACT(VolContact[idx1][idx2]) && ZEROVOLCONTACT(VolContact[idx2][idx1])) continue;

					//link->dist in this situation is not meaningful
					link->dist=VolContact[idx1][idx2]+VolContact[idx2][idx1];
					link->VolContact1=VolContact[idx1][idx2];
					link->VolContact2=VolContact[idx2][idx1];
				
					link->lCoreIndex=i;
					link->rCoreIndex=i;
					link->lCoreOffset=idx1-cores[i].leftSeqPos;
					link->rCoreOffset=idx2-cores[i].leftSeqPos;
					link->lSeqIndex=idx1;
					link->rSeqIndex=idx2;
					cores[i].localLinkNum++;

				}else{
					if ((idx2-idx1)<(config->MinPairDist))	continue;
					//TemplateSingleton* res2=templateSeq+idx2;

					float dist=0;

					dist=DistOf2AAs(idx1,idx2);

					if (dist>config->MaxContactDist) continue;

					link->dist=dist;
					link->lCoreIndex=i;
					link->rCoreIndex=i;
					link->lCoreOffset=idx1-cores[i].leftSeqPos;
					link->rCoreOffset=idx2-cores[i].leftSeqPos;
					link->lSeqIndex=idx1;
					link->rSeqIndex=idx2;
					cores[i].localLinkNum++;
				}
			}
		}


	}
}

void TEMPLATE::calcExternalLinks(){

	if (coreSize<=0) return;

	//compute the external links
	for(int i=0;i<coreSize;i++)
	{
		for(int j=i+1;j<coreSize;j++)
		{
			if (cores[j].coreType==LOOP)	continue;
			for(int idx1=cores[i].leftSeqPos;idx1<=cores[i].rightSeqPos;idx1++)
			{
				//TemplateSingleton* res1=templateSeq+idx1;
				for(int idx2=cores[j].leftSeqPos;idx2<=cores[j].rightSeqPos;idx2++)
				{

					if (cores[i].rightLinkNum>=MaxRightLinkSize){
						char message[MaxLineSize];
						sprintf(message,"fatal error: rightLinkNum %d goes beyond MaxRightLinkSize (%d)!\n",cores[i].rightLinkNum,MaxRightLinkSize);
						config->writeErrorLog(message);
						fprintf(stderr,"%s",message);
						exit(-1);
					}
					if (cores[j].leftLinkNum>=MaxLeftLinkSize){
						char message[MaxLineSize];
						sprintf(message,"fatal error: leftLinkNum %d goes beyond MaxLeftLinkSize (%d)!\n",cores[i].leftLinkNum,MaxLeftLinkSize);
						config->writeErrorLog(message);
						fprintf(stderr,"%s",message);
						exit(-1);
					}

					LINK* link1=cores[i].rightLinks+cores[i].rightLinkNum;
					LINK* link2=cores[j].leftLinks+cores[j].leftLinkNum;

					if (config->UseVolume){

						//use the contact definition proposed by Brendan

						if (ZEROVOLCONTACT(VolContact[idx1][idx2]) && ZEROVOLCONTACT(VolContact[idx2][idx1]))
							continue;

						//link1->dist is not meaningful in this situtation
						link1->dist=VolContact[idx1][idx2]+VolContact[idx2][idx1];
						link1->VolContact1=VolContact[idx1][idx2];
						link1->VolContact2=VolContact[idx2][idx1];

						link1->lCoreIndex=i;
						link1->rCoreIndex=j;
						link1->lCoreOffset=idx1-cores[i].leftSeqPos;
						link1->rCoreOffset=idx2-cores[j].leftSeqPos;
						link1->lSeqIndex=idx1;
						link1->rSeqIndex=idx2;

						memcpy(link2,link1,sizeof(LINK));
						cores[i].rightLinkNum++;
						cores[j].leftLinkNum++;
					}else {
						if ((idx2-idx1)<config->MinPairDist) continue;
						//TemplateSingleton* res2=templateSeq+idx2;
/*
						float dist=0;
						dist+=SQUARE(res1->xPos-res2->xPos);
						dist+=SQUARE(res1->yPos-res2->yPos);
						dist+=SQUARE(res1->zPos-res2->zPos);
						if (res1->xPos>=MAXCOOR-1 || res2->xPos>=MAXCOOR-1) dist=MAXCOOR;

						if (dist>config->DistCutOff) continue;
*/
						float dist=DistOf2AAs(idx1,idx2);
						if (dist>config->MaxContactDist) continue;

						link1->dist=(DIST_T)dist;
						link1->lCoreIndex=i;
						link1->rCoreIndex=j;
						link1->lCoreOffset=idx1-cores[i].leftSeqPos;
						link1->rCoreOffset=idx2-cores[j].leftSeqPos;
						link1->lSeqIndex=idx1;
						link1->rSeqIndex=idx2;

						memcpy(link2,link1,sizeof(LINK));
						cores[i].rightLinkNum++;
						cores[j].leftLinkNum++;

					}
				}
			}
		}
	}
}

void TEMPLATE::estimateMinLoopSize(){

	if (coreSize<=0) return;

	cores[0].loopSize=0;
	for(int i=1;i<coreSize;i++){
		cores[i].loopSize=0;
		int prevPos=cores[i-1].rightSeqPos;
		int postPos=cores[i].leftSeqPos;
		TemplateSingleton* res1=templateSeq+prevPos;
		TemplateSingleton* res2=templateSeq+postPos;
		double dist=0;
		if (res1->xPos<=MAXCOOR-1 && res1->yPos<=MAXCOOR-1 && res1->zPos<=MAXCOOR-1 && res2->xPos<=MAXCOOR-1 && res2->yPos<=MAXCOOR-1 && res2->zPos<=MAXCOOR-1){
			dist+=SQUARE(res1->xPos-res2->xPos);
			dist+=SQUARE(res1->yPos-res2->yPos);
			dist+=SQUARE(res1->zPos-res2->zPos);
			
		}
		double minDist=MINDISTOf2CORES;
		while(dist>minDist*minDist && cores[i].loopSize<postPos-prevPos-1){
			cores[i].loopSize++;
			minDist=11.621*pow(cores[i].loopSize+0.25,0.359)+0.5;
		}
	}
		
}



		
void TEMPLATE::calcNativePairScore(CParam* p){

	if (coreSize<=0) return;
	
	memset(scoreGraph,0,MaxCoreSize*MaxCoreSize*sizeof(float));
	int i;
	for(i=0;i<coreSize;i++)
		for(int j=0;j<cores[i].rightLinkNum;j++){
			LINK* link=cores[i].rightLinks+j;
			int tRes1=templateSeq[link->lSeqIndex].residue;
			int tRes2=templateSeq[link->rSeqIndex].residue;
		
			scoreGraph[i][link->rCoreIndex]+=p->getPairInteraction(tRes1,tRes2,link->dist);
			
		}


	int totalEntries=0;
	int totalNegative=0;

	for(i=0;i<coreSize;i++)
		for(int j=i+1;j<coreSize;j++){
			totalEntries+=(scoreGraph[i][j]!=0);
			totalNegative+=(scoreGraph[i][j]<0);

			scoreGraph[j][i]=scoreGraph[i][j];
	}

	if (config->debug_level>=DEBUG_LEVEL1)
		printf("finished computing native pairwise score for template %s\n",templateName);

	if (config->debug_level>=DEBUG_LEVEL2){
		printf("\nScoreGraph of %s:",templateName );
		printf("\n total=%d,neg=%d,percent=%.1f\n",totalEntries,totalNegative,totalNegative*1.0/totalEntries);

		for(i=0;i<coreSize;i++){
			printf("\n");
			for(int j=0;j<coreSize;j++)
				printf("%4.0f ",scoreGraph[i][j]);
		}
	}

}


void TEMPLATE::print(FILE* fp){
	int i;
	for(i=0;i<templateSize;i++)
	{
		TemplateSingleton* singleton=templateSeq+i;
		char* name;
		if (singleton->residue<20 && singleton->residue>=0)
			name =AA2Name[singleton->residue];
		else name=AA2Name[20];
		int flag=singleton->flag;
		if (flag>0) flag=1;
		else flag=0;
		int resnum=singleton->resID;
		char extra=' ';
		char ss=singleton->structType;
		if (singleton->structType==LOOP)
			ss='L';
		else if (singleton->structType==HELIX)
			ss='H';
		else ss='E';

		fprintf(fp,"RES %d %s %d %d%c %c\n",i,name,flag,resnum,extra,ss); 
	}
	for(i=0;i<coreSize;i++)
	{
		for(int linkIndex=0;linkIndex<cores[i].rightLinkNum;linkIndex++)
		{
			LINK* link=cores[i].rightLinks+linkIndex;
			int l=link->lSeqIndex;
			int r=link->rSeqIndex;
			double dist=link->dist;
			
			fprintf(fp,"PAIR %d %d %lf %c\n",l,r,dist,'T');
		}
	}

}



void TEMPLATE::calcMinCut(){

	if (coreSize<=0) return;

	CUT cuts[MaxCoreSize][MaxCoreSize];

	memset(cuts,0,sizeof(CUT)*coreSize*coreSize);

	int i;
	for(i=1;i<coreSize-1;i++){
		for(int j=0;j<i;j++)
			for(int j1=i+1;j1<coreSize;j1++)
				cuts[i][i].cut+=linkGraph[j][j1];

		for(int r=i+1;r<coreSize-1;r++){
			int j;
			for(j=0;j<i;j++)
				for(int j1=i+1;j1<r;j1++)
					cuts[i][r].cut+=linkGraph[j][j1];
			for(j=r+1;j<coreSize;j++)
				for(int j1=i+1;j1<r;j1++)
					cuts[i][r].cut+=linkGraph[j1][j];
		}
	}
/*
	for(i=1;i<coreSize-1;i++)
		for(int j=1;j<coreSize-1-i;j++){
			if (cuts[j].cut>cuts[j+1].cut){
				CUT tmp;
				memcpy(&tmp,&cuts[j+1],sizeof(CUT));
				memcpy(&cuts[j+1],&cuts[j],sizeof(CUT));
				memcpy(&cuts[j],&tmp,sizeof(CUT));
			}
		}
*/

	printf("\n%s (coreSize=%d):",templateName,coreSize);

	for(i=1;i<coreSize-1;i++){
		printf("\n");
		for(int j=1;j<coreSize-1;j++)
			if (j<i) printf(" - ");
			else printf(" %d ",cuts[i][j].cut);
	}

}


//add edges between two adjacent cores
void TEMPLATE::addGapEdge(){
	if (gapEdgeAdded) return;
	for(int i=0;i<coreSize-1;i++){
		linkGraph[i][i+1]+=10;
		linkGraph[i+1][i]+=10;
	}
	gapEdgeAdded=true;
}

//delete the added edges between two adjacent cores
void TEMPLATE::deleteGapEdge(){
	if (!gapEdgeAdded) return;
	for(int i=0;i<coreSize-1;i++){
		if (linkGraph[i][i+1]>=10){
			linkGraph[i][i+1]-=10;
			linkGraph[i+1][i]-=10;
		}
	}
	gapEdgeAdded=false;
}

void TEMPLATE::pruneEdge(){
	for(int i=0;i<coreSize-1;i++)
		for(int j=i+2;j<coreSize;j++)
			if (linkGraph[i][j]>0 && linkGraph[i][j]<=1){
				linkGraph[i][j]=0;
				linkGraph[j][i]=0;
			}
}

int TEMPLATE::getEdge(int i,int j){
	
	return linkGraph[i][j]>0?linkGraph[i][j]:0;
}


/*
//not used right now
void TEMPLATE::ReadProspectTree(){
	
	char cutFilePath[MaxFileNameLength];
	sprintf(cutFilePath,"%s%s.%s",config->PartitionPath,templateName,config->PartitionType);

	FILE* fp=fopen(cutFilePath,"r");
	if (!fp){
		printf("WARNING:can not open %s for read!\n",cutFilePath);
		isProspectTreeAvailable=false;
		return;
	}

	int node_index=0;
	char s[1024];

	PTREENODE* node;

	while(fgets(s,1024,fp))
	{
		if (strncmp(s,"REMARK",5)==0){
		}else if(strncmp(s,"TREE",4)==0){
			short lchild,rchild;

			node=prospect_tree+node_index;
			sscanf(s,"TREE %*hd%hd%hd%hd%hd%hd%hd",&node->depth,&node->anchorNum,&node->lCore,&node->rCore,&lchild,&rchild);
			node->lChild=prospect_tree+lchild;
			node->rChild=prospect_tree+rchild;
			node->number=node_index;
			node_index++;
		}else if(strncmp(s,"ANCHOR",6)==0){
			for(int j=0;j<node->anchorNum;j++){
				char formatStr[256];
				sprintf(formatStr,"ANCHOR ");
				for(int i=0;i<j;i++)
					strcat(formatStr,"%*hd");
				strcat(formatStr,"%hd");
				sscanf(s,formatStr,node->anchorList+j);
			}
		}else{
		}
	}
	prospect_treeSize=node_index;
	isProspectTreeAvailable=true;

	if (fp){
		fclose(fp);
	}
	//for debug
	for(int i=0;i<prospect_treeSize;i++){
		node=prospect_tree+i;
		printf("\nTREE %hd %hd %hd %hd %hd %hd %hd",i,node->depth,node->anchorNum,node->lCore,node->rCore,node->lChild-prospect_tree,node->rChild-prospect_tree);
		printf("\nANCHOR ");
		for(int j=0;j<node->anchorNum;j++)
			printf(" %hd ",node->anchorList[j]);

	}
}

//not used right now
void TEMPLATE::pruneEdgeByPROSPECT(){
	
	short pruned=0;

	for(int i=0;i<prospect_treeSize;i++){
		PTREENODE* treeNode=prospect_tree+i;
		short lnode=treeNode->lCore;
		short rnode=treeNode->rCore;
		short* anchors=treeNode->anchorList;
		short anchorNum=treeNode->anchorNum;

		int isAnchor[MaxCoreSize];
		memset(isAnchor,0,sizeof(int)*coreSize);
		int l;
		for(l=0;l<anchorNum;l++)
			isAnchor[anchors[l]]=1;

		for(l=lnode;l<=rnode;l++){
			for(int l2=0;l2<coreSize;l2++)
			{
				if (l2>=lnode && l2<=rnode) continue;
				if (linkGraph[l][l2]<=0) continue;
				//check to see if l or l2 is in anchors
				if (isAnchor[l]||isAnchor[l2]) continue;
				linkGraph[l][l2]=linkGraph[l2][l]=0;
				pruned++;
			}
		}
	}

	if (pruned>0){
		printf("PROSPECT pruned %d edges for template %s\n",pruned,templateName);
	}
}

*/




float* TEMPLATE::getPSM(int tPos){
	if (tPos>=templateSize || tPos<0){
		fprintf(stderr,"out of bound in getPSM!\n");
		exit(-1);
	}
	return PSM[tPos];
}

/*
short* TEMPLATE::getPSP(int tPos){
	if (tPos>=templateSize || tPos<0){
		fprintf(stderr,"out of bound in getPSP!\n");
		exit(-1);
	}
	return PSP[tPos];
}
*/


void TEMPLATE::calcContacts(){
	contactNum=0;

	for(int i=0;i<templateSize;i++){
		TemplateSingleton* res1=templateSeq+i;
		if (res1->flag==0) continue;	//it is not a meaningful position

		int contacts=0;
		int contact1=0;	
		int contact2=0;	

		for(int j=0;j<templateSize;j++){
			if (abs(j-i)<4) continue;
			if (templateSeq[j].flag==0) continue;

			float dist=0;
			dist=DistOf2AAs(i,j);
			if (dist<=config->MaxContactDist){

				if (i<j){
					contactList[contactNum].dist=dist;
					contactList[contactNum].leftPos=i;
					contactList[contactNum].rightPos=j;
					contactNum++;
				}


				contacts++;
				
				if (abs(i-j)<10)
					contact1++;
				else contact2++;

			}
		}
		templateSeq[i].contactNum=contacts;
		templateSeq[i].sContactNum=contact1;
		templateSeq[i].lContactNum=contact2;
	}
}

/*
float TEMPLATE::DistOf2AAs(int i, int j){
	TemplateSingleton* res1=templateSeq+i;
	TemplateSingleton* res2=templateSeq+j;
	float dist=0;
	if (INVALIDCOOR(res1->xPos) || INVALIDCOOR(res1->yPos) || INVALIDCOOR(res1->zPos))
		dist=999.9;
	else if (INVALIDCOOR(res2->xPos) || INVALIDCOOR(res2->yPos) || INVALIDCOOR( res2->zPos))
		dist=999.9;
	else{
		dist+=SQUARE(res1->xPos-res2->xPos);
		dist+=SQUARE(res1->yPos-res2->yPos);
		dist+=SQUARE(res1->zPos-res2->zPos);
		dist=sqrt(dist);
	}
	return dist;
}
*/

float TEMPLATE::DistOf2AAs(int tPos1, int tPos2, int type){
	TemplateSingleton* res1=templateSeq+tPos1;
	TemplateSingleton* res2=templateSeq+tPos2;

	float dist=0;

	if (type==CA2CA){
		dist+=SQUARE(res1->xPos2-res2->xPos2);
		dist+=SQUARE(res1->yPos2-res2->yPos2);
		dist+=SQUARE(res1->zPos2-res2->zPos2);
		dist=sqrt(dist);

		if (INVALIDCOOR(res1->xPos2)) dist=999.0;
		if (INVALIDCOOR(res1->yPos2)) dist=999.0;
		if (INVALIDCOOR(res1->zPos2)) dist=999.0;
		if (INVALIDCOOR(res2->xPos2)) dist=999.0;
		if (INVALIDCOOR(res2->yPos2)) dist=999.0;
		if (INVALIDCOOR(res2->zPos2)) dist=999.0;
	}else if(type==CB2CB){
		if (INVALIDCOOR(res1->xPos) || INVALIDCOOR(res1->yPos) || INVALIDCOOR(res1->zPos))
			dist=999.9;
		else if (INVALIDCOOR(res2->xPos) || INVALIDCOOR(res2->yPos) || INVALIDCOOR( res2->zPos))
			dist=999.9;
		else{
			dist+=SQUARE(res1->xPos-res2->xPos);
			dist+=SQUARE(res1->yPos-res2->yPos);
			dist+=SQUARE(res1->zPos-res2->zPos);
			dist=sqrt(dist);
		}
	}else if(type==CA2CB){
		if (INVALIDCOOR(res1->xPos2) || INVALIDCOOR(res1->yPos2) || INVALIDCOOR(res1->zPos2))
			dist=999.9;
		else if (INVALIDCOOR(res2->xPos) || INVALIDCOOR(res2->yPos) || INVALIDCOOR( res2->zPos))
			dist=999.9;
		else{
			dist+=SQUARE(res1->xPos2-res2->xPos);
			dist+=SQUARE(res1->yPos2-res2->yPos);
			dist+=SQUARE(res1->zPos2-res2->zPos);
			dist=sqrt(dist);
		}
	}else if(type==CB2CA){
		if (INVALIDCOOR(res1->xPos) || INVALIDCOOR(res1->yPos) || INVALIDCOOR(res1->zPos))
			dist=999.9;
		else if (INVALIDCOOR(res2->xPos2) || INVALIDCOOR(res2->yPos2) || INVALIDCOOR( res2->zPos2))
			dist=999.9;
		else{
			dist+=SQUARE(res1->xPos-res2->xPos2);
			dist+=SQUARE(res1->yPos-res2->yPos2);
			dist+=SQUARE(res1->zPos-res2->zPos2);
			dist=sqrt(dist);
		}
	}else dist=999.0;

	return dist;
	
}
//link statistics
void TEMPLATE::statLinks(){
	FILE* fp=fopen("contact.stat","ab");
	if (!fp){
		fprintf(stderr,"can not open contact.stat for add\n");
		return;
	}


	FILE* link_fp=fopen("links.stat","ab");
	if (!link_fp){
		fprintf(stderr,"can not open links.stat for add\n");
		return;
	}

//	fprintf(fp,"## begin template:%s*******************************\n",templateName);
//	fprintf(fp,"##res1\tres2\tpos1\tpos2\tss1\tss2\tseqd\tdist\n");
	for(int i=0;i<templateSize;i++){
		TemplateSingleton* res1=templateSeq+i;
		int ss1=0;
		if (res1->structType==HELIX)
			ss1=1;
		else if(res1->structType==SHEET)
			ss1=2;
		else ss1=0;

		

		int contacts=0;
		int contact1=0;	
		int contact2=0;	

		for(int j=0;j<templateSize;j++){
			if (abs(j-i)<5) continue;

			TemplateSingleton* res2=templateSeq+j;

			float dist=0;

			dist=DistOf2AAs(i,j);

			if (dist<=9+0.0001){
				int ss2=0;
				if (res2->structType==HELIX)
					ss2=1;
				else if(res2->structType==SHEET)
					ss2=2;


				contacts++;
				
				if (abs(i-j)<10)
					contact1++;
				else contact2++;
				if (j-i>=5)
					fprintf(link_fp,"%d\t%d\t%d\t%d\t%d\t%d\t%.3f\n",res1->residue,res2->residue,ss1,ss2,i,j,sqrt(dist));

			}
		}
		fprintf(fp,"%d\t%d\t%d\t%d\t%d\t%d\n",i,res1->residue,ss1,contacts,contact1,contact2);
	}
//	fprintf(fp,"##end template:%s*******************************\n",templateName);
	fclose(fp);
	fclose(link_fp);
}

//solvent accessibility statisitics
void TEMPLATE::statSolvent(){
	FILE* fp=fopen("solvent.stat","ab");
	for (int i=0;i<templateSize;i++){
		TemplateSingleton* AA=templateSeq+i;
		if ( maxAcc[AA->residue]>0 && AA->solvent<=maxAcc[AA->residue]){
			fprintf(fp,"%d\t%d\t%.4f\t%d\n",AA->structType,AA->residue,AA->solvent*1.0/maxAcc[AA->residue],AA->solvent);
		}
	}
	fclose(fp);
}
int TEMPLATE::ResidueIndexByResID(char* resID){
	char lastchar=resID[strlen(resID)-1];
	if (!isdigit(lastchar)){
		char tmpResID[10];
		sprintf(tmpResID,"%s",resID);
		tmpResID[strlen(resID)-1]=0;
		return ResidueIndexByResID(atoi(tmpResID),lastchar);
	}
	return ResidueIndexByResID(atoi(resID),' ');
}

int TEMPLATE::ResidueIndexByResID(int resID, char extraChar){
	int low=0;
	int high=templateSize-1;

	while(low<=high){
		int mid=(low+high)/2;
		TemplateSingleton* AA=templateSeq+mid;
		if (AA->resID<resID){
			low=mid+1;
		}else if (AA->resID>resID){
			high=mid-1;
		}else if (AA->extraChar==0 || isspace(AA->extraChar)){
			if (isspace(extraChar)) return mid;
			low=mid+1;
		}else if (isspace(extraChar)){
			high=mid-1;
		}else if (tolower(AA->extraChar) < tolower(extraChar)){
			low=mid+1;
		}else if (tolower(AA->extraChar) > tolower(extraChar)){
			high=mid-1;
		}else {//equal
			return mid;
		}
	}
	return -1;
}

/*
void TEMPLATE::LoadHBonds(char* file){
	FILE* fp=fopen(file,"r");
	if (!fp){
		HBondAvailable=false;
		return;
	}
	char* fmt="%s%s%s%s";
	int bondcount=0;
	char buf[4096];
	while(fgets(buf,4096,fp)){
		char s1[10];
		char s2[10];
		char atomName1[10];
		char atomName2[10];
		sscanf(buf,fmt,s1,atomName1,s2,atomName2);

		//printf("buf=%s",buf);

		char extraChar1=' ';
		char extraChar2=' ';
		int len1=strlen(s1);
		int len2=strlen(s2);
		if (!isdigit(s1[len1-1])){
			extraChar1=s1[len1-1];
			s1[len1-1]=0;
			len1--;
		}
		if (!isdigit(s2[len2-1])){
			extraChar2=s2[len2-1];
			s2[len2-1]=0;
			len2--;
		}
		int resID1=atoi(s1);
		int resID2=atoi(s2);

		int index1=ResidueIndexByResID(resID1,extraChar1);
		int index2=ResidueIndexByResID(resID2,extraChar2);

		if (index1< 0 || index2<0) continue;

		if (strcasecmp(atomName1,"CB")==0 && strcasecmp(atomName2,"CB")==0){
			if (index1< index2)
				HBonds[index1][index2]=(HBonds[index1][index2] | CB2CB);
			else
				HBonds[index2][index1]=(HBonds[index2][index1] | CB2CB);
			bondcount++;
		}else if(strcasecmp(atomName1,"HA")==0){
			if (strcasecmp(atomName2, "HN")==0){
				if (index1< index2)
					HBonds[index1][index2]=(HBonds[index1][index2] | HA2HN);
				else
					HBonds[index2][index1]=(HBonds[index2][index1] | HN2HA);
				bondcount++;
			}else if(strcasecmp(atomName2,"HA")==0){
				if (index1< index2)
					HBonds[index1][index2]=(HBonds[index1][index2] | HA2HA);
				else
					HBonds[index2][index1]=(HBonds[index2][index1] | HA2HA);
				bondcount++;
			}
		}else if(strcasecmp(atomName1,"HN")==0){
			if (strcasecmp(atomName2, "HN")==0){
				if (index1< index2)
					HBonds[index1][index2]=(HBonds[index1][index2] | HN2HN);
				else
					HBonds[index2][index1]=(HBonds[index2][index1] | HN2HN);
				bondcount++;
			
			}else if(strcasecmp(atomName2,"HA")==0){
				if (index1< index2)
					HBonds[index1][index2]=(HBonds[index1][index2] | HN2HA);
				else
					HBonds[index2][index1]=(HBonds[index2][index1] | HA2HN);
				bondcount++;
			}
		}


		
	}
	fclose(fp);

	fprintf(stderr,"%d hbonds read from %s\n",bondcount,file);
	//transpose matrix
	for(int i=1;i<templateSize;i++)
		for(int j=0;j<i;j++)
			HBonds[i][j]=HBonds[j][i];
	HBondAvailable=true;
}
*/

char TEMPLATE::GetStructTypeChar(int i){
	char ss;
	if (templateSeq[i].structType==LOOP)
		ss='L';
	else if (templateSeq[i].structType==HELIX)
		ss='H';
	else ss='E';
	return ss;
}

int TEMPLATE::GetStructType(int i){
	return templateSeq[i].structType;
}

bool TEMPLATE::WithinCore(int tPos){

	for(int i=0;i<getCoreSize();i++){
		if (tPos>=cores[i].leftSeqPos && tPos<=cores[i].rightSeqPos)
			return true;
	}
	return false;
}


void TEMPLATE::Load(){

	char absFilePath[MaxFileNameLength];
	memset(absFilePath,0,MaxFileNameLength*sizeof(char));

	//sprintf(absFilePath,"%s%s.%s",config->TemplatePath,templateName,config->TemplateType);
	sprintf(absFilePath,"%s/FSSP/%s.%s",getenv("CRF_HOME"),templateName,"fssp");
	FILE* fp=fopen(absFilePath,"r");

	if (fp==0){

		//report err
		char s[1024];
		sprintf(s,"can't open the file %s for read!",absFilePath);
		exit(-1);

	}

	int i=0;

	char s[MaxLineSize];


	while(fgets(s,MaxLineSize,fp)){

		if (ToolKit::isEmpty(s)) continue;

//		printf("s=%s\n",s);

		StringTokenizer strToken(s," \t\n\r");
		char* nextToken=strToken.getNextToken();

		if (nextToken==NULL || !nextToken[0]) continue;


		if (strncmp(nextToken,"RES",3)==0){

			addOneResidue(i,&strToken,s); //insert this residue to i-th pos 
			i++;
		}


	}

	templateSize=i;



	fclose(fp);

	if (templateSize>MaxTemplateSize){
		char str[4096];
		sprintf(str,"fatal error: template size (%d) is beyond the maximum allowed %d\n",templateSize, MaxTemplateSize);
		fprintf(stderr,"%s",str);
		exit(-1);
	}



	//begin to read PSM information
	PSM_array=new float[templateSize*21];
	PSM[0]=PSM_array;
	for(i=1;i<templateSize;i++)
		PSM[i]=PSM[i-1]+21;
	memset(PSM_array,0,sizeof(short)*templateSize*21);

        /*
	if (config->UsePSM){
		char PSMFile[1024];
		
		//sprintf(PSMFile,"%s%s.psm",config->PSMPath,templateName);
		sprintf(PSMFile,"%s%s%s.psm",getenv("CRF_HOME"), "/PSM/",templateName);
                int ret=	LoadPSMInfo(PSMFile,PSM_array);
		if (!ret){
			config->EnablePSM(false);
		}

	}
        */



        /*

	if (config->UseVolume){
		//allocate memory for VolContact_array
		VolContact_array=new float[templateSize*templateSize];
		memset(VolContact_array,0,sizeof(float)*templateSize*templateSize);
		for(int i=0;i<templateSize;i++)
			VolContact[i]=VolContact_array+i*templateSize;

		//read .vol files 
		char volfile[MaxLineSize];
		sprintf(volfile,"%s%s.vol",config->TempVolPath,templateName);
		fprintf(stderr,"begin loading volume file for template %s\n",templateName);
		LoadVolContact(volfile);
		fprintf(stderr,"after loading volume file for template %s\n",templateName);
	}*/

}

void TEMPLATE::SetResidueFlag(int pos, short flag){
	if (pos<0 || pos>=templateSize) return;
	templateSeq[pos].flag=flag;
}


short TEMPLATE::GetResidueFlag(int pos){
	if (pos<0 || pos>=templateSize) return 0;
	return templateSeq[pos].flag;
}

int TEMPLATE::GetCoreLen(int coreIndex){
	if (coreIndex<0 || coreIndex>=getCoreSize())	return 0;
	return cores[coreIndex].rightSeqPos-cores[coreIndex].leftSeqPos+1;
}

int TEMPLATE::CoreIndexByResIndex(int resIndex){
	for(int j=0;j<getCoreSize();j++){
		if (resIndex>=cores[j].leftSeqPos && resIndex<=cores[j].rightSeqPos)
			return j;
	}
	return -1;
}
int TEMPLATE::GetCoreHeadPosition(int coreIndex){
	if (coreIndex<0 || coreIndex>=getCoreSize())	return -1;
	return cores[coreIndex].leftSeqPos;
}
	
