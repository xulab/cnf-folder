#ifndef _STRING_TOKEN_H
#define _STRING_TOKEN_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>



class StringTokenizer {

	private:

	char* str;
	char* delimiter;
	int pointer;

	char* token;




	public :

	StringTokenizer(char* s,char* delimiter);
	~StringTokenizer();
	char* getNextToken();
	void rewind();

    // *********** BY TINA, May 13, 2003
    // this is to emulate more fully the Java counterpart
    int countTokens();
    // *********** BY TINA, May 13, 2003

	private:

	int isDelimiter(char c);
	


};

int isEmpty(char* s);

#endif

