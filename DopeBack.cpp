/*******************************************************************************
  Created by Feng Zhao referencing from : DOPE-Back-AllAtom.cpp of Copyright (C) 2005 The University of Chicago

  Authors: 
  Min-yi Shen, original code
  Andres Colubri, coversion to C++
  James Fitzgerald, backbone dependence

  Description:
  PL plugin to calculate Min-yi's Statistical Potential for Fold
  Recognition. Backbone Dependent and allows for all atom treatment.

*******************************************************************************/

#include <cstddef> // Needed to use NULL.
#include <fstream>
#include <cstring>
#include <cmath>
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "strtokenizer.h"
#include "DopeBack.h"

using namespace std;

#define UNK -1
#define INF_CHAIN 10000

DopeBack::DopeBack()
{
	// Amino Acid Names
	ALA = GetResidueCode("ALA");
	ARG = GetResidueCode("ARG");
	ASN = GetResidueCode("ASN");
	ASP = GetResidueCode("ASP");
	CYS = GetResidueCode("CYS");
	GLN = GetResidueCode("GLN");
	GLU = GetResidueCode("GLU");
	GLY = GetResidueCode("GLY");
	HIS = GetResidueCode("HIS");
	ILE = GetResidueCode("ILE");
	LEU = GetResidueCode("LEU");
	LYS = GetResidueCode("LYS");
	MET = GetResidueCode("MET");
	PHE = GetResidueCode("PHE");
	PRO = GetResidueCode("PRO");
	SER = GetResidueCode("SER");
	THR = GetResidueCode("THR");
	TRP = GetResidueCode("TRP");
	TYR = GetResidueCode("TYR");
	VAL = GetResidueCode("VAL");

    InitializeMemory(30);
    InitializeEnergy();
    SetDefConfig();
}

DopeBack::~DopeBack()
{
   ReleaseMemory();
}

int DopeBack::GetDOPEcode(int aa, const string &at)
{
    if (add_backbone)
    {
        if (at == "N") return 0;
        if (at == "CA") return 1;
        if (at == "C") return 2;
        if (at == "O") return 3;
    } 
     
    if (add_centroids)
    {
        if (((at == "CM") || (at == "MC")) && (aa != GLY)) return 4;
    }
    
    if (add_hydrogens)
    { 
        if (((at == "HN") || (at == "H"))&& (aa != PRO)) return 5;
    }

    if (add_side_chains)
    { 
        if ((at == "CB") && (aa != GLY)) return 6;
    }

    if (add_side_chains && !add_only_CB)
    {
        if (aa == ARG)
        {
            if (at == "CG") return 7;
            if (at == "CD") return 8;
            if (at == "NE") return 9;
            if (at == "CZ") return 10;
            if ((at == "NH1") || (at == "NH2") || (at == "NH")) return 11;
        }
        if (aa == ASN)
        {
            if (at == "CG") return 7;
            if (at == "OD1") return 8;
            if (at == "ND2") return 9;
        }
        if (aa == ASP)
        {
            if (at == "CG") return 7;
            if ((at == "OD1") || (at == "OD2") || (at == "OD")) return 8;
        }
        if (aa == CYS)
        {
            if (at == "SG") return 7;
        }
        if (aa == GLN)
        {
            if (at == "CG") return 7;
            if (at == "CD") return 8;
            if (at == "OE1") return 9;
            if (at == "NE2") return 10;
        }
        if (aa == GLU)
        {
            if (at == "CG") return 7;
            if (at == "CD") return 8;
            if ((at == "OE1") || (at == "OE2") || (at == "OE")) return 9;
        }
        if (aa == HIS)
        {
            if (at == "CG") return 7;
            if (at == "ND1") return 8;
            if (at == "CD2") return 9;
            if (at == "CE1") return 10;
            if (at == "NE2") return 11;
        }
        if (aa == ILE)
        {
            if (at == "CG1") return 7;
            if (at == "CG2") return 8;
            if ((at == "CD1") || (at == "CD2") || (at == "CD")) return 9;
        }
        if (aa == LEU)
        {
            if (at == "CG") return 7;
            if ((at == "CD1") || (at == "CD2") || (at == "CD")) return 8;
        }
        if (aa == LYS)
        {
            if (at == "CG") return 7;
            if (at == "CD") return 8;
            if (at == "CE") return 9;
            if (at == "NZ") return 10;
        }
        if (aa == MET)
        {
            if (at == "CG") return 7;
            if (at == "SD") return 8;
            if (at == "CE") return 9;
        }
        if (aa == PHE)
        {
            if (at == "CG") return 7;
            if ((at == "CD1") || (at == "CD2") || (at == "CD")) return 8;
            if ((at == "CE1") || (at == "CE2") || (at == "CE")) return 9;
            if (at == "CZ") return 10;
        }
        if (aa == PRO)
        { 
            if (at == "CG") return 7;
            if (at == "CD") return 8;
        }
        if (aa == SER)
        {
            if (at == "OG") return 7;
        }
        if (aa == THR)
        {
            if (at == "OG1") return 7;
            if (at == "CG2") return 8;
        }
        if (aa == TRP)
        {
            if (at == "CG") return 7;
            if (at == "CD1") return 8;
            if (at == "CD2") return 9;
            if (at == "NE1") return 10;
            if (at == "CE2") return 11;
            if (at == "CE3") return 12;
            if (at == "CZ2") return 13;
            if (at == "CZ3") return 14;
            if (at == "CH2") return 15;
        }
        if (aa == TYR)
        {
            if (at == "CG") return 7;
            if ((at == "CD1") || (at == "CD2") || (at == "CD")) return 8;
            if ((at == "CE1") || (at == "CE2") || (at == "CE")) return 9;
            if (at == "CZ") return 10;
            if (at == "OH") return 11;
        }
        if (aa == VAL)
        {
            if ((at == "CG1") || (at == "CG2") || (at == "CG")) return 7;
        }
    }
    return UNK;
}

// get the index of the atom given its name
// insert_flag: 0-- search the atom code only; 
//              1-- add the aa_str into the atom code map if not found
// return -1 for "result not found"
int DopeBack::GetAtomCode(int aa, string atom_str, int insert_flag)
{
	mapATOMstr2int::iterator mapit;
	char aa_str[10];
	sprintf(aa_str, "%d", aa);
	string idx_str = atom_str + "_" + aa_str;
	mapit = patom_s2i->find(idx_str);
	if (mapit == patom_s2i->end()) 
	{
		if (insert_flag==1)
		{
			// label not found, insert it to the map
			int icode = GetDOPEcode(aa, atom_str);
			
			//cerr << idx_str << ":" << icode << endl;

			patom_s2i->insert(pair<string, int>(idx_str, icode));
			patom_i2s->insert(pair<int, string>(icode, idx_str));
			return icode;
		}
		else
			return -1;
	} else { 
		 return mapit->second;
	}
};

int DopeBack::LoadFromFile(string fn, int fn_num)
{
	string aa0_str, at0_str, aa1_str, at1_str;
	int aa0, rb0, at0, aa1, rb1, at1, bin;
	double energy;
	
	cerr << "Dope-Back Loading file " << fn << endl;
	ifstream par_file;
	par_file.close();
	par_file.clear();
	par_file.open(fn.c_str(), ios::in);
	if (!par_file.is_open())
	{
		cerr << "Dope-Back Loading file " << fn << " failed!" << endl;
		return 0;
	}

	while (par_file.good())
	{
		par_file >> aa0_str;
		par_file >> at0_str;
		par_file >> rb0;
		par_file >> aa1_str;
		par_file >> at1_str;
		par_file >> rb1;
	
		if (par_file.eof()) break;
		
		aa0 = GetResidueCode(aa0_str, 1); // 1 is insert flag
        	aa1 = GetResidueCode(aa1_str, 1);
		at0 = GetAtomCode(aa0, at0_str);
		at1 = GetAtomCode(aa1, at1_str);

		if ((aa0 != UNK) && (rb0 != UNK) && (at0 != UNK) && (aa1 != UNK) && (rb1 != UNK) && (at1 != UNK)) {
			if (fn_num == 0) {
				if ((rb0 == 1) && (rb1 == 1))
		  			for (bin = 0; bin < n_bins; bin++) {
						par_file >> energy;
						SheetSheetEnergy[aa0][at0][aa1][at1][0][bin] = energy;
					}
				else if ((rb0 == 1) && (rb1 == 2))
					for (bin = 0; bin < n_bins; bin++) {
						par_file >> energy;
						SheetSheetEnergy[aa0][at0][aa1][at1][1][bin] = energy;
					}
				else if ((rb0 == 2) && (rb1 == 2))
					for (bin = 0; bin < n_bins; bin++) {
						par_file >> energy;
						SheetSheetEnergy[aa0][at0][aa1][at1][2][bin] = energy;
					}
				else {
					cerr << "Incorrectly Formatted Sheet-Sheet Input File" << endl;
					return 1;
				}
			}
			else if (fn_num == 1) {
				if ((rb0 == 1) && (rb1 == 4))
					for (bin = 0; bin < n_bins; bin++) {
						par_file >> energy;
						SheetHelixEnergy[aa0][at0][aa1][at1][0][bin] = energy;
					}
				else if ((rb0 == 2) && (rb1 == 4))
				  	for (bin = 0; bin < n_bins; bin++) {
						par_file >> energy;
						SheetHelixEnergy[aa0][at0][aa1][at1][1][bin] = energy;
				  	}
				else {
				 	cerr << "Incorrectly Formatted Sheet-Helix Input File" << endl;
				 	return 1;
				}
			}
			else if (fn_num == 2) {
				if ((rb0 == 4) && (rb1 == 4))
				  for (bin = 0; bin < n_bins; bin++) {
						par_file >> energy;
						HelixHelixEnergy[aa0][at0][aa1][at1][bin] = energy;
					}
				else {
					cerr << "Incorrectly Formatted Helix-Helix Input File" << endl;
					return 1;
				}
			}
	  		else if (fn_num == 3) {
				if (rb0 == 1) {
					if (rb1 == 3)
						for (bin = 0; bin < n_bins; bin++) {
							par_file >> energy;
							Energy1[aa0][at0][aa1][at1][bin] = energy;
						}
				}
				else if (rb0 == 2) {
					if (rb1 == 3)
						for (bin = 0; bin < n_bins; bin++) {
						    par_file >> energy;
						    Energy2[aa0][at0][aa1][at1][bin] = energy;
						}
				}
				else if (rb0 == 3) {
					if (rb1 == 3)
						for (bin = 0; bin < n_bins; bin++) {
						    par_file >> energy;
						    OtherEnergy[aa0][at0][aa1][at1][bin] = energy;
					 	}
				 	else if (rb1 == 4)
				 		for (bin = 0; bin < n_bins; bin++) {
						    par_file >> energy;
						    Energy4[aa1][at1][aa0][at0][bin] = energy;
					 	}
				}
				else {
				   cerr << "Incorrectly Formatted Input File" << endl;
				   return 1;
				}
			}
			else {
				cerr << "Invalid fn_number" << endl;
				return 1;
			}
		}
		else 
			for (bin = 0; bin < n_bins; bin++) 
				par_file >> energy;
	}
	par_file.close();
	par_file.clear();
	
	return 0;
}

void DopeBack::Resize(int n)
{
    ReleaseMemory();
    InitializeMemory(n);
    InitializeEnergy();
}

void DopeBack::InitializeMemory(int n)
{
	n_bins = n;
	
	int aa0, aa1, at0, at1, i;
	for (aa0 = 0; aa0 < 20; aa0++)
		for (at0 = 0; at0 < 16; at0++)
    		for (aa1 = 0; aa1 < 20; aa1++)
			for (at1 = 0; at1 < 16; at1++)
			{
				if (0 < n_bins) {
					HelixHelixEnergy[aa0][at0][aa1][at1] = new double[n_bins];
					Energy1[aa0][at0][aa1][at1] = new double[n_bins];
					Energy2[aa0][at0][aa1][at1] = new double[n_bins];
					Energy4[aa0][at0][aa1][at1] = new double[n_bins];
					OtherEnergy[aa0][at0][aa1][at1] = new double[n_bins];
				}
				else {
					HelixHelixEnergy[aa0][at0][aa1][at1] = NULL;
					Energy1[aa0][at0][aa1][at1] = NULL;
					Energy2[aa0][at0][aa1][at1] = NULL;
					Energy4[aa0][at0][aa1][at1] = NULL;
					OtherEnergy[aa0][at0][aa1][at1] = NULL;
				}
				for (i = 0; i < 3; i++) {
					if (0 < n_bins)
						SheetSheetEnergy[aa0][at0][aa1][at1][i] = new double[n_bins];
					else 
						SheetSheetEnergy[aa0][at0][aa1][at1][i] = NULL; 
				}
				for (i = 0; i < 2; i++) {
					if (0 < n_bins) 
						SheetHelixEnergy[aa0][at0][aa1][at1][i] = new double[n_bins];
					else 
						SheetHelixEnergy[aa0][at0][aa1][at1][i] = NULL; 
				}
			}
}

void DopeBack::ReleaseMemory()
{
	if (0 >= n_bins)
		return;

	int aa0, aa1, at0, at1, i;
	for (aa0 = 0; aa0 < 20; aa0++) 
		for (at0 = 0; at0 < 16; at0++)
			for (aa1 = 0; aa1 < 20; aa1++)
				for (at1 = 0; at1 < 16; at1++) {
					delete[] HelixHelixEnergy[aa0][at0][aa1][at1];
					delete[] Energy1[aa0][at0][aa1][at1];
					delete[] Energy2[aa0][at0][aa1][at1];
					delete[] Energy4[aa0][at0][aa1][at1];
					delete[] OtherEnergy[aa0][at0][aa1][at1];
					for (i = 0; i < 3; i++) 
						delete[] SheetSheetEnergy[aa0][at0][aa1][at1][i];
					for (i = 0; i < 2; i++) 
						delete[] SheetHelixEnergy[aa0][at0][aa1][at1][i];
				}
}

void DopeBack::InitializeEnergy()
{
	int aa0, aa1, at0, at1, i, bin;
	for (aa0 = 0; aa0 < 20; aa0++)
		for (at0 = 0; at0 < 16; at0++)
			for (aa1 = 0; aa1 < 20; aa1++)
				for (at1 = 0; at1 < 16; at1++)
					for (bin = 0; bin < n_bins; bin++) {
						HelixHelixEnergy[aa0][at0][aa1][at1][bin] = 0.0;
						Energy1[aa0][at0][aa1][at1][bin] = 0.0;
						Energy2[aa0][at0][aa1][at1][bin] = 0.0;
						Energy4[aa0][at0][aa1][at1][bin] = 0.0;
						OtherEnergy[aa0][at0][aa1][at1][bin] = 0.0;
						for (i = 0; i < 3; i++) 
							SheetSheetEnergy[aa0][at0][aa1][at1][i][bin] = 0.0;
						for (i = 0; i < 2; i++) 
							SheetHelixEnergy[aa0][at0][aa1][at1][i][bin] = 0.0;
					}
}

double DopeBack::PairEnergyFromTable(int aa1, int rb1, int at1, int aa2, int rb2, int at2, int bin)
{
	if (aa1 != UNK && rb1 != UNK && at1 != UNK && aa2 != UNK && rb2 != UNK && at2 != UNK)
		if (-1 < bin && bin < n_bins) {
			// Sheet-Sheet Interaction Energies
			if (rb1 == 0 && rb2 == 0) return SheetSheetEnergy[aa1][at1][aa2][at2][0][bin];
			else if (rb1 == 1 && rb2 == 0) return SheetSheetEnergy[aa2][at2][aa1][at1][1][bin];
			else if (rb1 == 0 && rb2 == 1) return SheetSheetEnergy[aa1][at1][aa2][at2][1][bin];
			else if (rb1 == 1 && rb2 == 1) return SheetSheetEnergy[aa1][at1][aa2][at2][2][bin];
			// Sheet-Helix Interaction Energies
			else if (rb1 == 0 && rb2 == 3) return SheetHelixEnergy[aa1][at1][aa2][at2][0][bin];
			else if (rb1 == 1 && rb2 == 3) return SheetHelixEnergy[aa1][at1][aa2][at2][1][bin];
			else if (rb1 == 3 && rb2 == 0) return SheetHelixEnergy[aa2][at2][aa1][at1][0][bin];
			else if (rb1 == 3 && rb2 == 1) return SheetHelixEnergy[aa2][at2][aa1][at1][1][bin];
			// Helix-Helix Interaction Energy
			else if (rb1 == 3 && rb2 == 3) return HelixHelixEnergy[aa1][at1][aa2][at2][bin];
			// Beta-Other Interactions
			else if (rb1 == 0 && (rb2 == 2 || rb2 == 4 || rb2 == 5)) return Energy1[aa1][at1][aa2][at2][bin];
			else if (rb2 == 0 && (rb1 == 2 || rb1 == 4 || rb1 == 5)) return Energy1[aa2][at2][aa1][at1][bin];
			// PPII-Other Interactions
			else if (rb1 == 1 && (rb2 == 2 || rb2 == 4 || rb2 == 5)) return Energy2[aa1][at1][aa2][at2][bin];
			else if (rb2 == 1 && (rb1 == 2 || rb1 == 4 || rb1 == 5)) return Energy2[aa2][at2][aa1][at1][bin];
			// Helix-Other Interactions
			else if (rb1 == 3 && (rb2 == 2 || rb2 == 4 || rb2 == 5)) return Energy4[aa1][at1][aa2][at2][bin];
			else if (rb2 == 3 && (rb1 == 2 || rb1 == 4 || rb1 == 5)) return Energy4[aa2][at2][aa1][at1][bin];
			// Unusual Interactions
			else if ((rb1 == 2 || rb1 == 4 || rb1 == 5) && (rb2 == 2 || rb2 == 4 || rb2 == 5)) return OtherEnergy[aa1][at1][aa2][at2][bin];
		}
		
	return 0.0;
}

double DopeBack::PairEnergy(string saa0, int rb0, string sat0, string saa1, int rb1, string sat1, int cd, double dist, int interaction)
{
	double e_pair = 0;
	// Only accesses one energy matrix at a time.
	if ( ( interaction == 0 && rb0 == 1 && rb1 == 1 ) ||
      		(interaction == 1 && ( (rb0 == 1 && rb1 == 2) || (rb0 == 2 && rb1 == 1) ) ) ||
      		(interaction == 2 && rb0 == 2 && rb1 == 2 ) ||
      		(interaction == 3 && ((rb0 == 1 && (rb1 == 4)) || (rb0 == 4 && rb1 == 1) ) ) ||
      		(interaction == 4 && ((rb0 == 2 && (rb1 == 4)) || (rb0 == 4 && rb1 == 2) ) ) ||
      		(interaction == 5 && rb0 == 4 && (rb1 == 4) ) || 
      		(interaction == 6 && (rb0 == 1 && (rb1 == 3 || rb1 == 5 || rb1 == 6) ) || ( (rb0 == 3 || rb0 == 5 || rb0 == 6) && rb1 == 1)) ||
      		(interaction == 7 && (rb0 == 2 && (rb1 == 3 || rb1 == 5 || rb1 == 6) ) || ( (rb0 == 3 || rb0 == 5 || rb0 == 6) && rb1 == 2)) ||
      		(interaction == 8 && (rb0 == 4 && (rb1 == 3 || rb1 == 5 || rb1 == 6) ) || ( (rb0 == 3 || rb0 == 5 || rb0 == 6) && rb1 == 4)) ||
      		(interaction == 9 && ((rb0 == 3 || rb0 == 5 || rb0 == 6) && (rb1 == 3 || rb1 == 5 || rb1 == 6) ) ) ) 
	{
		int aa0 = GetResidueCode(saa0);
		int aa1 = GetResidueCode(saa1);
		int c0 = GetAtomCode(aa0, sat0);
		int c1 = GetAtomCode(aa1, sat1);
    		if (c0 == UNK || c1 == UNK)
    			return e_pair;
		
		//cerr << "c0="<<c0<<"; c1="<<c1<<"; " << aa0 << ";" << aa1 << ";" << saa0<<";"<<saa1<<";"<<sat0<<";"<<sat1 << endl;
		//cerr << min_chain_dist << " <= " << cd << " <= " << max_chain_dist << endl;
		if (min_chain_dist <= cd && cd <= max_chain_dist)
		{
			double r1, ri, f1, f2;
			// The distance lies between middle points of bins ri and ri + 1.
			r1 = 2.0 * (dist - 0.25);
			ri = int(r1);
			
			// Linear interpolation between the energy value at those bins.
			f1 = r1 - ri;
			f2 = 1.0 - f1;
			
			if (0 < ri)
				// When ri = n_bins - 1, PairEnergy(ri + 1) gives 0, so we are interpolating
				// between the last energy value stored in the table and 0, which is ok.
				e_pair = f2 * PairEnergyFromTable(aa0, rb0-1, c0, aa1, rb1-1, c1, (int)ri) +
		         		 f1 * PairEnergyFromTable(aa0, rb0-1, c0, aa1, rb1-1, c1, (int)(ri+1));
			else 
				e_pair = PairEnergyFromTable(aa0, rb0-1, c0, aa1, rb1-1, c1, 0);
			//cerr << "f1="<<f1<<"; f2="<<f2<<"; ri="<<ri<<"; ("<<aa0<<","<<c0<<","<<rb0-1<<")--("<<aa1<<","<<c1<<","<<rb1-1<<"):"<<e_pair<<endl;
		}
	}
	return e_pair;
}

void DopeBack::SetDefConfig()
{
    // Setting defaults.
    dist_cutoff = 15.0;
    num_bin = 30;
    sheet_sheet_par_file = "";
    sheet_helix_par_file = "";
    helix_helix_par_file = "";
    other_par_file = "";

    add_bonded = false;
    use_DM = false;

    add_backbone = true;
    add_hydrogens = false;
    add_centroids = false; 
    add_rb_centroids = false;
    back_cent_file = "";
    add_side_chains = true; 
    add_only_CB = true;

    min_chain_dist = 1;
    max_chain_dist = INF_CHAIN;

    energy_coeff = 1.0;
    interaction_coeff[0] = 1.0;
    interaction_coeff[1] = 1.0;
    interaction_coeff[2] = 1.0;
    interaction_coeff[3] = 1.0;

    update_codes = true;
}

int DopeBack::ReadConfigFile(const char* FileName)
{
	string line, par, val;
	ifstream CfgFile;
	cerr << "Dope-Back Reading configuration file " << FileName << endl;
	CfgFile.open(FileName, ios::in | ios::binary);
	while (CfgFile.good())
	{
		// Moves through the lines until the HEADER record is found.
		getline(CfgFile, line);
		if (CfgFile.eof()) break;
		strtokenizer strtokens(line,"=");
                int num=strtokens.count_tokens();
                if (num<2) continue;
		par = strtokens.token(0);
		val = strtokens.token(1);
		if (par == "Distance cutoff") dist_cutoff = atof(val.c_str());
    		else if (par == "Number of bins") 
		{
			num_bin = atoi(val.c_str());
			Resize(num_bin);
		}  
		else if (par == "Sheet-Sheet parameter file") sheet_sheet_par_file = val;
		else if (par == "Sheet-Helix parameter file") sheet_helix_par_file = val;
		else if (par == "Helix-Helix parameter file") helix_helix_par_file = val;
		else if (par == "Other parameter file") other_par_file = val;
		
		else if (par == "Add bonded atoms") add_bonded = atoi(val.c_str());
		else if (par == "Use Distance Matrix") use_DM = atoi(val.c_str());
		else if (par == "Update atom codes") update_codes = atoi(val.c_str());            
		
		else if (par == "Add backbone atoms") add_backbone = (val=="yes"); 
		else if (par == "Add hydrogen atoms") add_hydrogens = (val=="yes");
		else if (par == "Add side-chain centroids") add_centroids = (val=="yes");
		else if (par == "Add backbone dependent centroids") add_rb_centroids = (val=="yes");
		else if (par == "Backbone dependent centroid file") back_cent_file = val;
		else if (par == "Add side-chain atoms") add_side_chains = (val=="yes");
		else if (par == "Add beta-carbons only") add_only_CB = (val=="yes");
		
		else if (par == "Minimum chain distance") min_chain_dist = atoi(val.c_str());
		else if (par == "Maximum chain distance") max_chain_dist = atoi(val.c_str());
		
		else if (par == "Energy coefficient") energy_coeff = atof(val.c_str());
		else if (par == "Sheet-Sheet coefficient") interaction_coeff[0] = atof(val.c_str());
		else if (par == "Sheet-Helix coefficient") interaction_coeff[1] = atof(val.c_str());
		else if (par == "Helix-Helix coefficient") interaction_coeff[2] = atof(val.c_str());
		else if (par == "Other coefficient") interaction_coeff[3] = atof(val.c_str());
		else {
		    //cout << "Unrecognized line in configuration file : " << line << endl;
		    //return 1;
		}
	}
	CfgFile.close();
	return 0;
}

void DopeBack::Init(const char* cfgFileName)
{
	ReadConfigFile(cfgFileName);
			
    if (interaction_coeff[0] != 0.0) LoadFromFile(sheet_sheet_par_file, 0);
    if (interaction_coeff[1] != 0.0) LoadFromFile(sheet_helix_par_file, 1);
    if (interaction_coeff[2] != 0.0) LoadFromFile(helix_helix_par_file, 2);
    if (interaction_coeff[3] != 0.0) LoadFromFile(other_par_file, 3);
}
