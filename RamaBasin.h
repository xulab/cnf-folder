/*******************************************************************************

                              RamaTypology.h
                              --------------

  ****************************************************************************
  *                                                                          *
  *   This library is free software; you can redistribute it and/or          *
  *   modify it under the terms of the GNU Lesser General Public             *
  *   License as published by the Free Software Foundation; either           *
  *   version 2.1 of the License, or (at your option) any later version.     *
  *                                                                          *
  *   This library is distributed in the hope that it will be useful,        *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *
  *   Lesser General Public License for more details.                        *
  *                                                                          *
  *   You should have received a copy of the GNU Lesser General Public       *
  *   License along with this library; if not, write to the Free Software    *
  *   Foundation, Inc.,                                                      *
  *   59 Temple Place, Suite 330, Boston, MA  02111-1307  USA                *
  *                                                                          *
  ****************************************************************************

  Copyright (C) 2005 The University of Chicago

  Authors:
  Andres Colubri

  Description:
  Defines the structures and classes to handle Ramachandran types.

*******************************************************************************/

#ifndef __RAMA_BASIN_H__
#define __RAMA_BASIN_H__

#include <fstream>
#include <vector>
#include <string>
using namespace std;

#define PHI 0
#define PSI 1
#define RBASIN_BORDER 0.1 // Distance from the border of a Ramachandran basin.

struct PhiPsiBox
{
    double Corner[2]; // Lower left corner.
    // Box length along the phi and psi axes (the coordinates of the upper right
    // corner are Corner[phi]+Length[phi] and Corner[psi]+Length[psi].
    double Length[2];
    int LoadFromStream(ifstream &stream);
};

struct RamaBasin
{
    RamaBasin() { Code = 0; Name = "";}
    ~RamaBasin() {}

    int Code;
    string Name;
    PhiPsiBox BoundBox;

    int LoadFromStream(ifstream &stream);
    
    void PhiPsiPoint(double &phi, double &psi, double r_phi, double r_psi)
    {
        // RBASIN_BORDER is used to avoid falling in the edges.
        phi = BoundBox.Corner[PHI] + RBASIN_BORDER + r_phi * (BoundBox.Length[PHI] - 2 * RBASIN_BORDER);
        psi = BoundBox.Corner[PSI] + RBASIN_BORDER + r_psi * (BoundBox.Length[PSI] - 2 * RBASIN_BORDER);
    }

    void BasinLimits(double &minphi, double &maxphi, double &minpsi, double &maxpsi)
    {
        minphi = BoundBox.Corner[PHI] + RBASIN_BORDER;
        minpsi = BoundBox.Corner[PSI] + RBASIN_BORDER;
        maxphi = BoundBox.Corner[PHI] + BoundBox.Length[PHI] - RBASIN_BORDER;
        maxpsi = BoundBox.Corner[PSI] + BoundBox.Length[PSI] - RBASIN_BORDER;
    }

    double AngleDiff(double a1, double a2)
    {
    	double n;
    	return 360.0 * modf((a1 - a2) / 360.0, &n);
    }
    void PhiPsiToBasinCoords(double &phi, double &psi)
    {
        phi = BoundBox.Corner[PHI] + AngleDiff(phi, BoundBox.Corner[PHI]);
        psi = BoundBox.Corner[PSI] + AngleDiff(psi, BoundBox.Corner[PSI]);
    }
    bool ContainsPhiPsi(double phi, double psi)
    {
        PhiPsiToBasinCoords(phi, psi);
        return (BoundBox.Corner[PHI] <= phi) &&
               (phi <= BoundBox.Corner[PHI] + BoundBox.Length[PHI]) &&
               (BoundBox.Corner[PSI] <= psi) &&
               (psi <= BoundBox.Corner[PSI] + BoundBox.Length[PSI]);
    }
    bool FixPhiPsi(double &phi, double &psi)
    {
        double minphi, maxphi, minpsi, maxpsi;
        BasinLimits(minphi, maxphi, minpsi, maxpsi);
        PhiPsiToBasinCoords(phi, psi);

        bool fixed = false;
        if (phi < minphi) { phi = minphi; fixed = true; }
        if (maxphi < phi) { phi = maxphi; fixed = true; }

        if (psi < minpsi) { psi = minpsi; fixed = true; }
        if (maxpsi < psi) { psi = maxpsi; fixed = true; }
        return fixed;
    }
};

class RamaTypology
{
public:
    RamaTypology() { Name = ""; BasinsList.clear();}
    ~RamaTypology() {}

    int LoadFromFile(const string &FileName);
    int PhiPsiToRBasin(double phi, double psi);

protected:
    string Name;
    vector<RamaBasin> BasinsList;

    int LoadFromStream(ifstream &stream);
    
    void PhiPsiPoint(int RB, double &phi, double &psi, double r_phi, double r_psi)
    { BasinsList[RB].PhiPsiPoint(phi, psi, r_phi, r_psi);  }

    void BasinLimits(int RB, double &minphi, double &maxphi, double &minpsi, double &maxpsi)
    { BasinsList[RB].BasinLimits(minphi, maxphi, minpsi, maxpsi); }
    void PhiPsiToBasinCoords(int RB, double &phi, double &psi)
    { BasinsList[RB].PhiPsiToBasinCoords(phi, psi); }
};

#endif
