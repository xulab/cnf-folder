/* -------------------------------------------------------------------- */
/* p-sea.c								*/
/*									*/
/* Assignations des structures secondaires par analyses des angles et	*/
/* distances entre Ca							*/
/*									*/
/* Calcul des angles et liaisons a l'oeuvre dans les proteines		*/
/*									*/
/* calcul des angles phi, psi, omega 					*/
/* phi = (C-1,N,CA,C)							*/
/* psi = (N,CA,C,N+1)							*/
/* omega = (CA,C,N+1,CA+1)						*/
/*									*/
/* calcul de chi1, chi2, chi3 d'une entree de la pdb			*/
/*									*/
/* calcul de alpha, tau et r12						*/
/* (Levitt J. Mol. Biol. 1976 ,vol. 104, 59-107)			*/
/*									*/
/* ici les angles sont defini comme suit : 				*/
/* alpha = (CA-1,CA,CA+1,CA+2)						*/
/* tau = (CA_CA-1,CA_CA+1)						*/
/* beta = (CB-1,CB,CB+1,CB+2)						*/
/* nu = (CB-1,CB,CB+1)							*/
/*									*/
/* calcul de la distance (r13) entre le CA+1 et le CA-1 pour un residu	*/
/*									*/
/* les angles sont donnes entre -180 et 180 degres			*/
/* si un angle ne peut etre determine pour une raison			*/
/* ou une autre dans un residu, il prend la valeur			*/
/* 500 (NO_ANGLE) de meme pour la distance				*/
/*									*/
/* -------------------------------------------------------------------- */

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

#include "p-sea.h"

/* -------------------- */
/* CONSTANTES		*/
/* -------------------- */
# define LINE_LENGTH	128	/* length max of a line in pdb file	*/

#define PI		3.141592645	/* acos(-1) */
#define D2R		0.017453293	/* PI/180. */
#define R2D		1./D2R		/* 180./PI */
#define DIST_MAX_CA	20.0

#define POURCENT	'%'

#define OK 1
#define YES 1
#define NO 0

#define HAS_NO_CA_ATOM		'A'
#define HAS_NO_CB_ATOM		'B'
#define HAS_NO_N_ATOM		'N'
#define HAS_NO_CO_ATOM		'C'
#define HAS_NO_CG_ATOM		'G'
#define HAS_NO_CD_ATOM		'D'
#define HAS_NO_CE_ATOM		'E'
#define AT_START_OF_CHAIN	'S'
#define AT_END_OF_CHAIN		'F'

#define alfa  	'a'
#define	beta 	'b'
#define coil  	'c'
#define sheet  	's'

#define N_AA 26
#define N_ST 4
#define N_K 8

#define MLL 80				/* longueur ligne d'impression */
#define FNL 80				/* longueur ligne d'impression */
/* -------------------- */
/* variables		*/
/* -------------------- */
int 		comp_phipsi  = 0,
   		comp_dist14 = 0,
   		comp_def = 0,
   		comp_cons = 0,
   		comp_dom = 0,
   		comp_kwasi = 0,
   		comp_total = 0,
		comp_alphatau = 0,
		comp_betanu = 0,
   		comp_chi = 0,
		nb_res_tot = 0,
		moy_dis = 0,
		moy_ang = 0;

  FILE		*fil = NULL,
		*fil_out = NULL,
		*fil_ca = NULL,
		*filpdb = NULL,
		*pdbout = NULL;

   float	dca_max = DIST_MAX_CA;

   char		filin[FNL],
		fica[FNL],
		filout[FNL],
		pdbnam[FNL],
		filnam[FNL],
		getfil[FNL];

int 	HYDRE[26] = {0,0,1,0,0,1,0,0,1,0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,1,0};
int	TOT_ST[N_AA][N_ST];
/* -------------------- */
/*internal prototypes	*/
/* -------------------- */
static void do_help(void);
/* -------------------------------------------------------------------- */

static char *sResiduNames        = "/UNK/ALA/CYS/CSE/MSE/HYP/ASP/GLU/PHE/GLY/HIS/ILE/LYS/LEU/MET\
					/ASN/PRO/GLN/ARG/SER/THR/VAL/TRP/TYR/";
static char *sResiduNoChi1       = "/GLY/ALA/";
static char *sResiduNoChi2       = "/CYS/CSE/SER/THR/VAL/";
static char *sResiduNoChi3       = "/ILE/LEU/ASP/ASN/";

/* -------------------------------------------------------------------- */
/* macros								*/
/* -------------------------------------------------------------------- */
#ifndef NEW
#define NEW(obj)		(obj *)malloc(sizeof(obj))
#define NEWN(obj, dim)		(obj *)malloc(dim * sizeof(obj))
#endif
#define TV(var, lim, flo)       (var < (lim + flo) && var > (lim - flo))
#define TNT(var, li, ls)       (var < ls && var > li)
#define MAX(a,b)                ((a > b)? a : b)
#define PM(a, b, c)             sqrt(a*a + b*b + c*c)

#define GET_DIST(O1, O2) (float)(sqrt((double)((O1->x - O2->x)*(O1->x - O2->x) + \
				(O1->y - O2->y)*(O1->y - O2->y) + \
				 (O1->z - O2->z)*(O1->z - O2->z))))


#define GET_FIELD(start, len, fmt, var) 		  		\
	if (sscanf(pdb_field(line, start, len), fmt, var) != 1) {	\
	   myerror("get_atom: bad atom format", 0);			\
	   return 0;							\
	}


/* -------------------------------------------------------------------- */
/* prototypes   internes       						*/
/* -------------------------------------------------------------------- */
static	ResAtom	*resatom_cpy(ResAtom *dst, ResAtom *src);
static	ResAtom	*atom_resatom_cpy(ResAtom *dst, Atom *src);
static	ResAtom	*get_named_atom(char *name, Residu *r, ResAtom *dst);
static	double  resatom_distca( ResAtom *at);
static	void	angle_error(char *nam1, int num1, char *spos,
		   char *nam2, int num2, char smiss, int subroutine);
static	char	*low(char *str);
static	char	*trimr(char *str, int n);
static	Residu	*new_residu(void);
static	int	get_atom(char *line, Atom *atom);
static	ResAtom	*put_atom_in_res(Atom *atom, Residu *res);
static	double	atom_dihed(ResAtom *ra);
static	double	atom_angle(ResAtom *ra);
static	int	compute_phipsi(Residu *r, ResAtom *ra,
			int do_phi, int do_psi, int do_ome);
static	int	compute_chis(Residu  *r, ResAtom *ra,
			int do_chi1, int do_chi2, int do_chi3);
static	int	compute_alpha_tau(Residu  *r, ResAtom *ra,
		int do_tau, int do_alpha);
static	char	*pdb_field(char *line, int start, int len);
static	void	myInfoOnStderr(char *info);
static ResAtom *init_pos(Residu *pre, float distance, float angl,float dihd);
static ResAtom *atom_pos(ResAtom *ra, float dist, float angl, float dihd);
	double 	get_distNC( Residu *r);
	void	analyse_dist234(Residu *res);
	void	anal_ang234(Residu *res);
	void	anal_cons234(Residu *res);
	void	assign_def(Residu *fir_res);
	void	assign_def2(Residu *fir_res);
	void	libere(Residu *fir_res);
	int 	check_repeat(Residu *first_res, Residu *new);
	Residu	*recherche_deb(Residu *deb);
/*--------------------------------------------------------------*/
/* analyse							*/
/*--------------------------------------------------------------*/
void analyse(Residu *first_res)
{
   Residu	*res;
   float	dihint = 50.0,
		angint = 108.0,
		disint = 3.81;
   int		limite = 500, n;
	ResAtom *ra = NEW(ResAtom);


	moy_dis = 0;
	moy_ang = 0;

	fprintf(stderr,"\n\tAnalyse de la structure\n\n");
	for (res = first_res->next, nb_res_tot = 0 ; res ; res = res->next, nb_res_tot++) {
	   res->cons = 'c';
           get_alpha_tau(res);
           get_distca_1_2(res);
           /*get_distca_1_3(res);*/ /*get_distca_1_4(res);*/
           get_distca_1_5(res);	/* contient get_distca_1_3() et get_distca_1_4()*/
	}


  	if (comp_dist14 || comp_cons || comp_dom || comp_total){
		assign_def(first_res);
  	}
	else
  	if (comp_def){
		assign_def2(first_res);
  	}
	else{
	anal_cons234(first_res);
  for (res = first_res->next, nb_res_tot = 0 ; res ; res = res->next, nb_res_tot++) {
   	
/*fprintf(pdbout,"ATOM  %5d  CA  %s %5d    %8.3f%8.3f%8.3f\n",res->num,res->name,res->num,ra->x,ra->y,ra->z);*/

	if (!comp_dist14 && !comp_def && !comp_kwasi && !comp_dom && !comp_cons && !comp_total)
		printf(" %-4s %4hd %c %c ", res->name, res->num, res->chain, res->cons);
	
	if (comp_alphatau) {
           get_alpha_tau(res);
           get_distca_1_2(res);
	   printf("%8.3f %8.3f %8.3f ", res->alpha, res->tau, res->dis12);
	   /*printf("COOINT    %4d CA  %-4s %4d %c %8.3f %8.3f %8.3f ",res->num, res->name, res->num,res->chain,res->alpha, res->tau, res->dis12);*/
	}
	
	if (comp_phipsi) {
           get_phi_psi(res);
	   printf("%7.1f %7.1f %7.1f ", res->phi, res->psi, res->ome);
	}

	if (comp_chi) {
	   get_chi(res);
	   printf("%7.1f %7.1f %7.1f ", res->x1, res->x2,  res->x3);
	}
	if (comp_kwasi) {
        	get_phi_psi(res);
		printf("%c %-4s %4hd %c %c ", res->code, res->name, res->num, res->chain, res->cons);
		printf("%6.1f %6.1f %6.1f ", res->phi, res->psi, res->ome);
		if (get_named_atom("N",res, ra))
			printf(" %8.3f %8.3f %8.3f",ra->x,ra->y,ra->z);
		if (get_named_atom("CA",res, ra))
			printf(" %8.3f %8.3f %8.3f",ra->x,ra->y,ra->z);
		if (get_named_atom("C",res, ra))
			printf(" %8.3f %8.3f %8.3f",ra->x,ra->y,ra->z);
	}

	
	if (!comp_dist14 && !comp_def  && !comp_dom && !comp_cons && !comp_total)
			printf("\n");
  }	/* fin du for */
	} /* fin du else */
	fprintf(stderr,"\n\tFin de l'analyse\n");

}
/*--------------------------------------------------------------*/
/* main program							*/
/*--------------------------------------------------------------*/

main(int argc, char **argv)
{
   char		str[256],
		*ptr;
   Residu	*cur_res,
		*first_res;
   int		i, index = 0, dec = 0,  j, argv2lu = 0,
		hit = 0,
		limite = 500,
		carg,
		errflag = 0;
   extern	char *optarg;
   extern	int optind, opterr;
   char		*map_ca,
		*map,
		line[MLL];


if (argc < 1){
        fprintf(stderr, "\n usage: p-sea pdb_file_name.lis [ pdb_file_name.sea]\n\n");
	exit(0);
}
if (argc == 2 && argv[1][0] != '-'){
	comp_dist14 = 1;
	if ((fil = fopen(argv[1], "r")) == NULL){ 			/* error exit with 1	*/
		fprintf(stderr,"\n\t Fichier '%s' illisible\n", argv[1]);
  	    	exit(1);	 /* myerror(strcat(strcpy(str, "cannot open file"), argv[i]), 1);*/
	}
	else
		strcpy(getfil,argv[1]);
}
if (argc == 3 && argv[1][0] != '-' && argv[2][0] != '-'){
	comp_dist14 = 1;
	if ((fil = fopen(argv[1], "r")) == NULL){ 			/* error exit with 1	*/
		fprintf(stderr,"\n\t Fichier '%s' illisible\n", argv[1]);
  	    	exit(1);	 /* myerror(strcat(strcpy(str, "cannot open file"), argv[i]), 1);*/
	}
	else
		strcpy(getfil,argv[1]);
	if (argc == 3){
		if ((fil_out = fopen(argv[2], "w")) == NULL){                       /* error exit with 1    */
                	fprintf(stderr,"\n\t Fichier '%s' illisible\n", argv[2]);
                	exit(1);         /* myerror(strcat(strcpy(str, "cannot open file"), argv[i]), 1);*/
        	}
        	else{
                	strcpy(filout,argv[2]);
			argv2lu = 1;
		}
	}
}
else
   for (i = 1; i < argc; i++) {		/* arguments			*/
      if (*argv[i] != '-') {
	   if ((fil = fopen(argv[i], "r")) == NULL){ 			/* error exit with 1	*/
		fprintf(stderr,"\n\t Fichier '%s' illisible\n", argv[i]);
  	     	exit(1);	 /* myerror(strcat(strcpy(str, "cannot open file"), argv[i]), 1);*/
	   }
	   else
		strcpy(getfil,argv[i]);
      }
      else {				/* it is an argument		*/
         ptr = argv[i]+1;
         while(*ptr) { 
           switch(*ptr++) {
         
            case 'p':	comp_phipsi   = 1;	/* calcule les angles phi, psi, omega */
            		break;
            
            case 'm':   comp_dist14 = 1;			 /* CA_CA + 3 distance   */
            		break;

            case 'n':   comp_def = 1;				/* CA_CA + 3 distance   */
            		break;

            case 'a':	comp_alphatau = 1;
            		break;

            case 'b':	comp_betanu = 1;
            		break;

            case 'x':	comp_chi = 1;
            		break;

            case 'c':	comp_cons = 1;
            		break;

            case 'd':	comp_dom = 1;
            		break;

            case 't':   comp_total = 1;		
            		break;

            case 'w':	comp_kwasi = 1;	/* calcule assignation, phi,psi, xyzN, xyzCa, xyzC */
			/* comp_dist14 = 1;*/
            		break;

            case 'h':	do_help();
		        exit(0);
            		break;

	    default:  	strcpy(str,argv[i]);
			strcat(str," argument non valable");
			myerror(str,5);
			/*exit(0);*/
	    		break;
           }
	 }
      }
   }

   	if (fil == NULL){
		printf("\n Quel fichier existant lire [* %s *] ? ", getfil);
		gets(getfil);
		sscanf(getfil,"%s",filin);
		if ((fil = fopen(filin, "r")) == NULL) 	/* error exit with 1	*/
			exit (1);
            	else
			comp_dist14 = 1;
	}
/********************************************************************************/
	if (argv2lu == 0){	
	   if (getfil[0] == '.' && getfil[1] == '.')
		j = 2;
	   else
		j = 0;	
	   dec = 0;
	   for (; j < MLL; j++){
		if (getfil[j] == '/')
			dec = j+1;
	   }
	   for (j = dec; j < MLL; j++){
		if (getfil[j] == '.')
			break;
		else
			filout[j-dec] = getfil[j];
	   }

	   strncat(filout,".sea",4);
	}
	if ( (fil_out = fopen(filout, "w") ) == NULL)    /* error exit with 1    */
		exit(1);
	else
                fprintf(stderr,"\n\tsortie : \t%s\n",filout);
		
/********************************************************************************/

while (fgets(pdbnam,FNL,fil) != NULL) {
        if (strncmp(pdbnam, "REMARK", 6) == 0 || !strncmp(pdbnam, "HEADER", 6) || !strncmp(pdbnam, "ATOM  ", 6)){
                first_res = readPdbResidues(fil);   
                fclose(fil);
                sscanf(getfil,"%s",filnam);
                fprintf(fil_out,">%s\n",filnam);
        }
        else {
                if (strncmp(pdbnam, "#", 1 ) == 0)
                        continue; 
		sscanf(pdbnam,"%s",filnam);
                if ( (filpdb = fopen(filnam, "r") ) == NULL)    /* error exit with 1    */
                        continue;
                fprintf(stderr,"\n\t lecture de: %s \n",filnam);
		/*if (filnam[33] == '.')
			i = 0;
		else
		if (filnam[34] == '.')
			i = 1;
		else
			i = 2;
		filnam[33+i] = '\n';
		filnam[34+i] = '\0';*/
		if ( comp_cons == 1){
			for (i = 1; i < FNL;i++){
				if (filnam[i] == '/')
					index = i;
				if (filnam[i-1] == '.' && filnam[i] == 'f'){
					filnam[i-1] = '\n';
					filnam[i] = '\0';
					break;
				}
			}
                	fprintf(fil_out,">%s",filnam+index);
		}
		else{
                	fprintf(fil_out,">%s\n",filnam);
                	fprintf(stdout,">%s\n",filnam);
		}
                first_res = readPdbResidues(filpdb);    /* first res is a dummy res     */
                fclose(filpdb);
		for (i = 0; i < FNL;i++)
			filnam[i] = '\0';
        }

	if (first_res->next == NULL)				/* error exit with 2 */	
	   	   myerror("error in reading pdb file", 2);
	else
		first_res->chain = first_res->next->chain; 
		
	analyse(first_res);		/* ------------------ output loop ----------------------- */

	fprintf(fil_out,"\n");
 	fprintf(stderr, "\n\tNombre total de residus %d\n\n", nb_res_tot);
	libere(first_res);	
}	/* while */	/*--------------------- scan loop ---------------------------------------*/

   exit(1);
}

/****************************************************************************************/
/*	impression	 								*/
/****************************************************************************************/
int	imp_cons(Residu *res, int n, Residu *first_res, char let)
{
	char	rc, rl;

	if (res->ca == -1)
		return n;
	
	if (n%MLL == 0 && n != 0){				/* MLL = 80 */
		fprintf(fil_out,"\n");
		n = 0;
	}
	if (res->chain != res->prev->chain){
		fprintf(fil_out,"\n+\n");
		n = 0;
	}
	if (comp_cons == 1){
		if (let == 'a')
			let = '&';
		else
		if (let == 'b')
			let = '/';
		/*else
		if (let == 'c')
			let = '-';*/
		else
			let = '-';
	}
	/*if (let != '-')*/
	fprintf(fil_out,"%c", let);
	/*if ((let >= 'a' && let <= 'c') || let == '-'){
		if ((rc = res->cons) == '-')
			rc = 'c';
		if ((rl = res->code) == '-')
			rl = 'X';
	}*/
		/*TOT_ST[rl - 'A'][rc - 'a']++;
		TOT_ST[rl - 'A'][3]++;*/

return n;
}
/****************************************************************************************/
/*	test d'impression 								*/
/****************************************************************************************/
int	test_imp(Residu *res, int n, Residu *first_res, char let)
{
	int i, INS_MAX = 200;

		n = imp_cons(res, n, first_res, let);
/*fprintf(stdout,"%d %d %f %c %c\n", n, res->prev->num - res->num, get_distNC(res), res->chain+'A', res->prev->chain+'A');*/
		if (res->next  && res->num != res->next->num - 1 \
			&& 
		(!TV(res->dis12, 3.81, 0.25) || !TV(res->dis12, 2.95, 0.15))	/*Cis-proline*/
			&& !TV(get_distNC(res),1.24,0.2)\
			&& res->chain == res->next->chain)
			for (i = res->num/*, n--*/; i < INS_MAX && i < res->next->num -1; i++, n++){
					n = imp_cons(res, n, first_res, '-');
	fprintf(stdout,"\t - %d\n", n);	
			}
	
return n;
}
/****************************************************************************************/
/*	impression	 								*/
/****************************************************************************************/
void	list_imp(Residu *first_res)
{
	int n;
	Residu 	*res = NULL;

  	for (res = first_res->next, n = 0 ; res ; res = res->next, n++) {
		if (res->chain != res->prev->chain && check_repeat(first_res, res) ) {	
			res = recherche_deb(res);
		}
		else
			n = test_imp(res,n,first_res, res->cons);
		res->cons = 'c';
	}
	fprintf(fil_out,"\n");	
}
/****************************************************************************************/
/*	test d'assignement 								*/
/****************************************************************************************/
void	libere(Residu *first_res)
{
	Residu 	*res, *r;
	
  	for (res = first_res->next; res ;) {
		r = res;
		free(res);
		res = r->next;
	}
}
/****************************************************************************************/
/*	test d'assignement 								*/
/****************************************************************************************/
void	assign_def3(Residu *first_res)
{
	int	n = 0;
	Residu 	*r = NULL, *res = NULL;
	ResAtom at[2];

  	/*	for (res = first_res->next, n = 0 ; res ; res = res->next, n++) {
	   		fprintf(fil_out,"%c  %4hd %3.1f \n", res->code, res->num);
	   		fprintf(fil_out,"%c  %4hd %3.1f ", res->code, res->num,res->dis12);
fprintf(fil_out,"%6.1f %6.1f %6.1f %7.0f %7.0f %c\n",res->dis13,res->dis14, res->dis15, \
res->tau, res->alpha, res->cons);
	   		res->cons = 'c';
		}*/
}
/****************************************************************************************/
/*	test d'assignement 								*/
/****************************************************************************************/
void	assign_def2(Residu *first_res)
{
	int	n = 0,i, INS_MAX = 200;
	Residu 	*r = NULL, *res = NULL;
	ResAtom at[2];

  	for (res = first_res->next, n = 0 ; res ; res = res->next, n++) {
		if (res->chain != res->prev->chain ){
			if ( res->next && check_repeat(first_res, res)){	
				res = recherche_deb(res);
			}
			else{
	fprintf(fil_out,"+\n");
				fprintf(fil_out,"%c%c\n", res->code, res->cons);	
			}
		}
		else{
	if (res->next \
		&& res->num < res->next->num - 1 \
		&& (!TV(res->dis12, 3.81, 0.25) || !TV(res->dis12, 2.95, 0.15))	/*Cis-proline*/
		&& !TV(get_distNC(res),1.24,0.2) && res->chain == res->next->chain)
		for (i = res->num/*, n--*/; i < INS_MAX && i != res->next->num -1; i++, n++){
				fprintf(fil_out,"--\n");	
		}
				fprintf(fil_out,"%c%c\n", res->code, res->cons);	
		/*res->cons = 'c';*/
		}
	}
/*	fprintf(fil_out,"\n");	*/
}
/****************************************************************************************/
/*	test d'assignement 								*/
/****************************************************************************************/
void	assign_def(Residu *first_res)
{
	int	n = 0;
	Residu 	*r = NULL, *res = NULL;
	ResAtom at[2];

		/*anal_cons234(first_res);*/
	if (!comp_cons){
  		for (res = first_res->next, n = 0 ; res ; res = res->next, n++) {
			if (res->chain != res->prev->chain && res->next && check_repeat(first_res, res)){	
					res = recherche_deb(res);
			}
			else {
				n = test_imp(res,n,first_res, res->code);
			}
			res->cons = 'c';
		}
		anal_cons234(first_res);
                fprintf(fil_out,"\n>p-sea\n");
	}
	list_imp(first_res);			/* consensus */
/*
	analyse_dist234(first_res);
	list_imp(first_res); */			/* distances */
	/*
	anal_ang234(first_res);			
	list_imp(first_res);	*/		/* angles */
}
/****************************************************************************************/
/*	test d'assignement 								*/
/****************************************************************************************/
int	test_ang_alpha(Residu *res)
{

	/*return (TV(res->alpha, 50., 15.) && TV(res->tau, 89, 15.)
			&& TV(res->dis15, 6.5, 0.7));*/
	return ( (TV(res->alpha, 50., 25.) && TNT(res->tau, 79, 100.))
		||  (TV(res->alpha, 51., 11.) && TNT(res->tau, 77, 106.)));
}
/****************************************************************************************/
/*	test d'assignement 								*/
/****************************************************************************************/
int	test_ang_beta(Residu *res)
{

	/*return ( (TV(res->alpha, -148., 32.) || TV(res->alpha, 162., 18.) )
			 && TV(res->tau, 128., 26.) );*/
	return ( 
		((TNT(res->alpha, -180.0, -150.) || TNT(res->alpha, 165., 180.0) )
		&& TV(res->tau, 125., 20.))
		||
		((TNT(res->alpha, -180.0, -110.) || TNT(res->alpha, 140., 180.0) )
		&& TV(res->tau, 132., 12.)));
}
/****************************************************************************************/
/*	test d'assignement par angles							*/
/****************************************************************************************/
int	test_alpha(Residu *res)
{

/*	return (TV(res->dis14, 5.4, 0.6) && TV(res->dis15, 5.1, 0.6));*/
		return (TNT(res->dis14, 4.75, 5.55) && TNT(res->dis15, 5.75, 6.45));
}
/****************************************************************************************/
/*	test d'assignement par distance							*/
/****************************************************************************************/
int	test_beta(Residu *res)
{

	/*return (TV(res->dis14, 9.95, 0.95) && TV(res->dis13, 6.70, 0.70));*/

	return (TV(res->dis14, 9.99, 0.99) && TV(res->dis15, 12.0, 1.30));
}
/****************************************************************************************/
/*	test d'assignement par distance							*/
/****************************************************************************************/
int	test_consa(Residu *res)
{

	return ( (TV(res->alpha, 50., 25.) && TNT(res->tau, 79, 100.))
		||  (TV(res->alpha, 51., 13.) && TNT(res->tau, 77, 106.))
		|| (TNT(res->dis14, 4.75, 5.55) && TNT(res->dis15, 5.75, 6.45))
		);

}
/****************************************************************************************/
/*	test d'assignement 								*/
/****************************************************************************************/
int	test_consb(Residu *res)
{

	/*return ( 
		((TNT(res->alpha, -180.0, -160.) || TNT(res->alpha, 165., 180.0) )
		&& TV(res->tau, 125., 20.))
		||
		((TNT(res->alpha, -180.0, -110.) || TNT(res->alpha, 140., 180.0) )
		&& TV(res->tau, 135., 12.))
		|| (TV(res->dis14, 9.95, 0.95) && TV(res->dis13, 6.75, 0.75)));*/
	if (comp_cons == 1 || comp_dom == 1)
		        return (
                ((TNT(res->alpha, -180.0, -150.) || TNT(res->alpha, 165., 180.0) )
                && TV(res->tau, 126., 20.))
                ||
                ((TNT(res->alpha, -180.0, -110.) || TNT(res->alpha, 140., 180.0) )
                && TV(res->tau, 132., 12.))
                || (TV(res->dis14, 10.00, 1.00) &&
                        TNT(res->dis13, 6.00, 7.40) && /**/
                        TNT(res->dis15, 11.25, 13.55)
                        ));
	else
	return ( 
		((TNT(res->alpha, -180.0, -150.) || TNT(res->alpha, 165., 180.0) )
		&& TV(res->tau, 125., 20.))
		||
		((TNT(res->alpha, -180.0, -110.) || TNT(res->alpha, 140., 180.0) )
		&& TV(res->tau, 132., 12.))
		|| (TV(res->dis14, 9.95, 0.95) && 
			TV(res->dis13, 6.75, 0.75)
			/*TNT(res->dis15, 11.3, 13.6)*/
			));

}
/****************************************************************************************/
/*	detection de feuillet		 						*/
/****************************************************************************************/
int	test_feuillet(Residu *res, int n, Residu *first)
{
	int 	SBETA = 0;
	Residu 	*r = NULL, *rer = NULL;
	ResAtom at[2];

	SBETA = 0;
	for (rer = res; n > 0; rer = rer->next, n--){
		if (!get_named_atom("CA", rer, at+1))
			continue;
      		for (r = first->next; r && (r != res || r != res->prev || r != res->prev->prev); r = r->next) {
			if (!get_named_atom("CA", r, at) || 
					!(TNT(r->tau, 117., 145.) || test_consa(r)))
				continue;
			if (TNT(resatom_distca(at), 4.25, 5.25))
				SBETA++;
		}
		if (!r || !r->next || !r->next->next || !rer->next->next->next)
			continue;
      		for (r = rer->next->next->next; r; r = r->next) {
			if (!get_named_atom("CA", r, at) 
					|| !(TNT(r->tau, 117., 145.) || test_consa(r)))
				continue;
			if (TNT(resatom_distca(at), 4.25, 5.25))
				SBETA++;
		}
		/*fprintf(stderr,"SBETC:%3d", SBETA);*/
	}
	/*fprintf(stderr,"\n");*/
	return (SBETA > 4);
}
/****************************************************************************************/
/*	assignement 									*/
/****************************************************************************************/
void   anal_cons234(Residu *first_res)
{
	char	bulge = 'u', turn = 't', omega = 'w';
	float	aai = 79., aas = 99., dof = 0., doi = 0.0, dol = 0.0;
	int	n = 0, SBETA = 0, p = 0;
	Residu *r = NULL, *res = NULL, *rer = NULL;
	ResAtom at[2];

	fprintf(stderr,"\n\tAssignation des helices \n");
  	for (res = first_res->next; res; res = res->next) {		
	   if (test_consa(res)) {
	     	for (r = res->next, n = 2; r; r = r->next, n++) 
			if (!test_consa(r))
				break;
		if (r)
			if (TNT(r->dis14, 4.80, 5.45) && TNT(r->tau, aai, aas)){
				n++;
			}
		if (n > 4 /*|| (n==4 && TNT(r->tau, aai, aas))*/) {
			if (TNT(res->prev->dis14, 4.75, 5.45) && TNT(r->prev->tau, aai, aas)){
				res->prev->cons = alfa;
			}
	     		for (;res && n > 0; res = res->next, n--) {
				res->cons = alfa;
			}
			if (res && (TNT(res->dis14, 4.75, 5.45) && TNT(res->tau, aai, aas))){
				res->cons = alfa;
			}
		}
	   }
	}
	fprintf(stderr,"\n\tAssignation des brins \n");
  	for (res = first_res->next; res; res = res->next) {
		if (res->cons == alfa)
			continue;
	      if (test_consb(res)) {
		for (r = res->next, n = 1; r; r = r->next, n++) 
			 if (!test_consb(r) || r->cons == alfa)
				break;
		if (r && r->cons != alfa)
			if (TNT(r->dis13, 6.05, 7.10) || TNT(r->tau, 118., 145.)){
				n++;
			}
		if (n > 3 || (n == 3 && test_feuillet(res, n, first_res))) {
			if ( TNT(res->prev->dis13, 6.35, 7.35)/* ||  TV(res->tau, 133., 16.)*/)
				res->prev->cons = beta;
	     		for (; res && n > 0; res = res->next, n--) {
				res->cons = beta;
			}
			if (TNT(res->dis13, 6.35, 7.30) || TNT(res->tau, 118., 145.)){
				res->cons = beta;
                        }
		}
	      }
  	}
/*-------------------------------------------------------------------------------------*/
	if (comp_total){
		fprintf(stderr,"\n\t Assignation totale \n");

		for (res = first_res->next; res->next; res = res->next) {
			if (get_named_atom("CA", res, at)){
				res->x = at->x;
				res->y = at->y;
				res->z = at->z;
			}
			if (res->cons == beta || res->cons == alfa || res->next->cons == alfa)
				continue;
			if (!test_consa(res) && !test_consa(res->next)) {
				if (TV(res->dis13, 5.4, 0.5) && TV(res->prev->dis13, 5.4, 0.5)
						&& TV(res->prev->dis14, 4.9, 0.6)) {
						res->cons = turn;
						res->next->cons = turn;
				}
			}
			if (TV(res->dis13, 5.6, 0.5) && TV(res->prev->dis13, 5.6, 0.5)
					&& TV(res->prev->dis14, 6.5, 0.5) && TV(res->prev->dis15, 8.8, 0.9)){
					res->cons = bulge;
					res->next->cons = bulge;
			}
		}
		for (res = first_res->next; res->next; res = res->next) {
			if (res->cons == beta || res->cons == alfa)
				continue;
			dof = 10.0;
			for (r = res->next->next->next->next->next->next, n = 6; r->next && n < 18; r = r->next, n++) {
				dol = GET_DIST(res, r);
				if ( dol < 10.){
					/*fprintf(stderr,"%.1f %d %d %d\n", dol, n, res->num, r->num);*/
					rer = r;
					dof = dol;
				}
				if (r->cons == beta || r->cons == alfa)
					break;
			}
			if (n > 6 && dof < 10.)
				for (;res != rer->prev && n > 2;  res = res->next, n--)
					res->cons = omega;
		}
	}
/*-------------------------------------------------------------------------------------*/
}
/****************************************************************************************/
/*	assignement 									*/
/****************************************************************************************/
void   analyse_dist234(Residu *first_res)
{
	int	n = 0;
	Residu *r = NULL, *res = NULL;

  	for (res = first_res->next; res; res = res->next) {		/* distances */
	   if (test_alpha(res)) {
	     	for (r = res->next, n = 1; r; r = r->next, n++) 
			if (!test_alpha(r) && !TV(r->dis15, 6.5, 0.5))
				break;
		if (r)
			if (TV(r->dis13, 5.4, 0.55))
				n++;
		if (n > 4) {
			if (TV(res->prev->dis13, 5.4, 0.5))
				res->prev->cons = alfa;
	     		for (;res->next && n > 0; res = res->next, n--) 
				res->cons = alfa;
		}
	   }
	}
  	for (res = first_res->next; res; res = res->next) {
		if (res->cons == alfa)
			continue;
	      if (test_beta(res)) {
		for (r = res->next, n = 1; r; r = r->next, n++) 
			 if (!test_beta(r))
				break;
		if (r)
			if (TV(r->dis13, 6.7, 0.5))
				n++;
		if (n > 3) {
	      		if (TV(res->prev->dis13, 6.7, 0.5))
				res->prev->cons = beta;
	     		for (;res->cons != alfa && res->next && n > 0; res = res->next, n--) 
				res->cons = beta;
		}
	      }
	      else
			res->cons = coil;
	}
}
/****************************************************************************************/
/*	assignement 									*/
/****************************************************************************************/
void   anal_ang234(Residu *first_res)
{
	int	n = 0;
	Residu *r = NULL, *res = NULL;


  	for (res = first_res->next; res; res = res->next) {			/* angles */
	   if (test_ang_alpha(res)) {
	     	for (r = res->next, n = 1; r; r = r->next, n++) 
			if (!test_ang_alpha(r))
				break;
		if (r)
			if (TV(r->tau, 88, 12.))
				n++;
		if (n > 4) {
			if (TV(res->prev->tau, 88, 12.))
				res->prev->cons = alfa;
	     		for (;res->next && n > 0; res = res->next, n--) 
				res->cons = alfa;
		}
	   }
	}
  	for (res = first_res->next; res; res = res->next) {
	      if (res->cons == alfa)
			continue;
	      if (test_ang_beta(res)) {
		for (r = res->next, n = 1; r; r = r->next, n++) 
			 if (!test_ang_beta(r))
				break;
		if (r)
			if (TV(r->tau, 131., 23.))
				n++;
		if (n > 3){ 
	      		if (TV(res->prev->tau, 131., 23.))
				res->prev->cons = beta;
	     		for (; res->next && n > 0; res = res->next, n--) 
				res->cons = beta;
		}			
	      }
	      else
			res->cons = coil;
	}
}
/* -------------------------------------------------------------------- */
/* gestion (simplifiee) des erreurs					*/
/* imprime un message et arrete le massacre				*/
/* -------------------------------------------------------------------- */
void myerror(char *msg, int status)
{
	fprintf(stderr, " *error* %s\n", msg);
	if (status) exit(status);
}

/* -------------------------------------------------------------------- */
/* gestion (simplifiee) des informations (pas vraiment erreurs)		*/
/* imprime un message sur stderr					*/
/* -------------------------------------------------------------------- */
static void myInfoOnStderr(char *info)
{
	fprintf(stderr,"%s\n", info);
}

/* -------------------------------------------------------------------- */
/* utility : lower case a string					*/
/* -------------------------------------------------------------------- */
static char *low(char *str)
{
	char *s;

	for (s = str ; *s ; s++)
	   if (isupper(*s))
		*s = tolower(*s);

	return str;
}

/* -------------------------------------------------------------------- */
/* utility : trim spaces on the right of string				*/
/*	     (starting from pos n)					*/
/* -------------------------------------------------------------------- */
static char *trimr(char *str, int n)
{
	char *s;

	s = str + n;

	while ((s > str) && isspace(*s))
		*s-- = '\000';

	return str;
}


/* -------------------------------------------------------------------- */
/* creation d'un residu							*/
/* -------------------------------------------------------------------- */
static Residu *new_residu(void)
{
	Residu *r;

	if (! (r = NEW(Residu))) {
	   myerror("new_residu:: not enough memory", 0);
	   return NULL;
	}

	memset(r, 0, sizeof(Residu));

	r->nb_neighbours = 0;

	return r;
}

/* -------------------------------------------------------------------- */
/* copy resatom -> resatom						*/
/* -------------------------------------------------------------------- */
static ResAtom *resatom_cpy(ResAtom *dst, ResAtom *src)
{
	strcpy(dst->name, src->name);
	dst->x =  src->x;
	dst->y =  src->y;
	dst->z =  src->z;
	
	return dst;
}

/* -------------------------------------------------------------------- */
/* copy atom -> resatom							*/
/* -------------------------------------------------------------------- */
static ResAtom *atom_resatom_cpy(ResAtom *dst, Atom *src)
{
	strcpy(dst->name, src->name);
	dst->x =  src->x;
	dst->y =  src->y;
	dst->z =  src->z;
	
	return dst;
}

/*------------------------------------------------------------------------------*/
/* compute the distance between CA in Resatom at[0] and at[1]			*/
/*------------------------------------------------------------------------------*/
static double   resatom_distca( ResAtom *at)
{
	return( sqrt(   ((at+1)->x - at->x)*((at+1)->x - at->x) +
			((at+1)->y - at->y)*((at+1)->y - at->y) +
			((at+1)->z - at->z)*((at+1)->z - at->z)));
}

/* -------------------------------------------------------------------- */
/* adds an atom in residue (returns the associated, new 		*/
/* resatom)								*/
/* returns the Resatom (or NULL if it can't be allocated )		*/ 
/* -------------------------------------------------------------------- */
static ResAtom *put_atom_in_res(Atom *atom, Residu *res)
{
	ResAtom *ra = NULL;

	if (! (ra = NEW(ResAtom)))
	
	   myerror("put_atom_in_res:: not enough memory", 0);

	else {
	
	   atom_resatom_cpy(ra, atom);

	   ra->next = NULL;

	   if (! res->first_atom)
	      res->first_atom = ra;
	   else
	      res->last_atom->next = ra;

	   res->last_atom  = ra;
	}

	return ra;
}

/* -------------------------------------------------------------------- */
/* maintains the list of the neighbours of a residue			*/
/* returns the number of neighbours or -1 if too much neighbours	*/
/* -------------------------------------------------------------------- */
int set_residue_neighbours(Residu *res, Residu *first)
{	
	Residu	*r;

	res->nb_neighbours = 0;

	for (r = first; r; r = r->next) {
	   if (r == res)
	      continue;
	   /* d = GET_DIST(r, res); i*//* debug */
	   if (GET_DIST(r, res) <= DIST_MAX_BETWEEN_NEIGHBOUR_RESIDUE) {
	       if (++res->nb_neighbours >= MAX_NEIGHBOURS ) {
		   myerror("set_residue_neighbours:: trop de voisins", 4);
		   return -1;
	       }
	       else
		   res->neighbour[res->nb_neighbours-1] = r;
	   }
	}
	
	return res->nb_neighbours;
}

/* -------------------------------------------------------------------- */
/* find the geometric center of atoms in a residue			*/
/* and set the x, y, z field of the Residu structure			*/
/* return the number of atom found					*/
/* -------------------------------------------------------------------- */
int      set_residue_geocenter(Residu *r)
{
	ResAtom *a;
	int	n = 0;
	float	sx = 0.0, sy = 0.0, sz = 0.0, inv;

	for (a = r->first_atom; a != NULL; a = a->next) {
	   sx += a->x;
	   sy += a->y;
	   sz += a->z;
	   n++;
	}

	if (n > 0) {
		inv = 1./n;
	   r->x = sx*inv;
	   r->y = sy*inv;
	   r->z = sz*inv;
	}
	else
	   r->x = r->y = r->z = 0.;
	
	return n;
}

/* -------------------------------------------------------------------- */
/* get the maximum distance of an atom from the				*/
/* geometric center of the residue					*/
/* -------------------------------------------------------------------- */
float get_dist_max_from_geocenter(Residu *r)
{
	float d, dmax = -1.0;
	ResAtom	*a;

	for (a = r->first_atom; a != NULL; a = a->next) {
	   d = GET_DIST(r, a);
	   if (d > dmax) {
		dmax = d;
	   }
	}

	return dmax;
}
/* -------------------------------------------------------------------- */
/* extract atom (by name) from residu					*/
/* if found, copy atom into dst and returns atom			*/
/* else, returns NULL							*/
/* -------------------------------------------------------------------- */
static ResAtom *get_named_atom(char *name, Residu *r, ResAtom *dst)
{
	ResAtom *atm;

	for (atm = r->first_atom ; atm ; atm = atm->next)
	  if (! strcmp(name, atm->name)) {
		resatom_cpy(dst, atm);
		return atm;
	  }

	return NULL;

}
/*------------------------------------------------------------------------------*/
/* compute the distance between CA-1 and CA+3					*/
/*------------------------------------------------------------------------------*/
int  get_distca_1_5( Residu *r)
{
	ResAtom at[2];
	char	strerr[256];
						/* respectively: at[0] -> CA-1	*/
						/*               at[1] -> CA+3	*/
	r->dis14 = NO_ANGLE;
	r->dis15 = NO_ANGLE;

#define ERRORDIST(X,NX,S,Y,NY,CAR)	{ r->dis15 = NO_ANGLE;			\
					angle_error(X,NX,S,Y,NY,CAR,4);		\
					return 1; }

	if ( (! r->prev) || (r->num != r->prev->num + 1))
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_START_OF_CHAIN)

						/* find CA atom of prev residue	*/
	else if ( ! get_named_atom( "CA", r->prev, at)) /* n-1 */
	   ERRORDIST(r->prev->name, r->prev->num, "preceding", r->name, r->num, HAS_NO_CA_ATOM)

	if (( ! r->next) || (r->num != r->next->num - 1) )
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)

						/* find CA atom of next residue */
	else if ( ! get_named_atom( "CA", r->next, at+1))
	   ERRORDIST(r->next->name, r->next->num, "following", r->name, r->num, HAS_NO_CA_ATOM)

	r->dis13 = (float)resatom_distca(at); /* entre r->prev et r->next */

	if ( !r->next->next || (r->num != r->next->next->num - 2))
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)

	else if ( ! get_named_atom( "CA", r->next->next, at+1))	/* n+2 */
	   ERRORDIST(r->next->next->name, r->next->next->num, "following", r->name, r->num, HAS_NO_CA_ATOM)

	r->dis14 = (float)resatom_distca(at); /* entre r->prev et r->next->next */
									/* find CA atom of next residue */
	if ( !r->next->next->next || r->num != (r->next->next->next->num - 3) )
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)

	else if ( ! get_named_atom( "CA", r->next->next->next, at+1))	/* n+3 */
	   ERRORDIST(r->next->next->next->name, r->next->next->next->num, "following", r->name, r->num,
	   							HAS_NO_CA_ATOM)

	r->dis15 = (float)resatom_distca(at); /* entre r->prev et r->next->next->next */

	return 0;

#undef ERRORDIST

}
/*------------------------------------------------------------------------------*/
/* compute the distance between CA-1 and CA+2					*/
/*------------------------------------------------------------------------------*/
int  get_distca_1_4( Residu *r)
{
	ResAtom at[2];
	char	strerr[256];
						/* respectively: at[0] -> CA-1	*/
						/*               at[1] -> CA+2	*/
	r->dis14 = NO_ANGLE;

#define ERRORDIST(X,NX,S,Y,NY,CAR)	{ r->dis14 = NO_ANGLE;			\
					angle_error(X,NX,S,Y,NY,CAR,4);		\
					return 1; }

	if ( (! r->prev) || (r->num != r->prev->num + 1))

	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_START_OF_CHAIN)

						/* find CA atom of prev residue	*/
	else if ( ! get_named_atom( "CA", r->prev, at))

	   ERRORDIST(r->prev->name, r->prev->num, "preceding", r->name, r->num,
	   							HAS_NO_CA_ATOM)

	if (( ! r->next) || ( ! r->next->next) || (r->num != r->next->next->num - 2) )

	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)

						/* find CA atom of next residue */
	else if ( ! get_named_atom( "CA", r->next->next, at+1))

	   ERRORDIST(r->next->next->name, r->next->next->num, "following", r->name, r->num,
	   							HAS_NO_CA_ATOM)

	r->dis14 = (float)resatom_distca(at);

	return 0;

#undef ERRORDIST

}
/*------------------------------------------------------------------------------*/
/* compute the distance between CA-1 and CA+1					*/
/*------------------------------------------------------------------------------*/
int  get_distca_1_3( Residu *r)
{
	ResAtom at[2];
	char	strerr[256];
						/* respectively: at[0] -> CA-1	*/
						/*               at[1] -> CA+1	*/
	r->dis13 = 0.0;

#define ERRORDIST(X,NX,S,Y,NY,CAR)	{ r->dis13 = NO_ANGLE;			\
					angle_error(X,NX,S,Y,NY,CAR,4);		\
					return 1; }

	if ( (! r->prev) || (r->num != r->prev->num + 1))

	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_START_OF_CHAIN)

						/* find CA atom of prev residue	*/
	else if ( ! get_named_atom( "CA", r->prev, at))

	   ERRORDIST(r->prev->name, r->prev->num, "preceding", r->name, r->num,
	   							HAS_NO_CA_ATOM)

	if (( ! r->next) || (r->num != r->next->num - 1) )

	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)

						/* find CA atom of next residue */
	else if ( ! get_named_atom( "CA", r->next, at+1))

	   ERRORDIST(r->next->name, r->next->num, "following", r->name, r->num,
	   							HAS_NO_CA_ATOM)

	r->dis13 = (float)resatom_distca(at); /* entre r->prev et r->next */

	return 0;

#undef ERRORDIST

}
/*------------------------------------------------------------------------------*/
/* compute the peptide bond length if N et C available				*/
/*------------------------------------------------------------------------------*/
double get_distNC( Residu *r)
{
	float	dis = 0.;
	ResAtom at[2];
						/* respectively: at[0] -> C atome i	*/
						/*               at[1] -> N atome i+1	*/

#define ERRORDIST(X,NX,S,Y,NY,CAR)		{ r->dis12 = NO_ANGLE;		\
						angle_error(X,NX,S,Y,NY,CAR,3);	\
						return 1; }

	if (! r) {
	   return 0.;
	}
	/*else if ( ! get_named_atom( "C", r, at))
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, HAS_NO_CA_ATOM)

	if ( ! r->next) 
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)
	else if ( ! get_named_atom( "N", r->next, at+1))
	   ERRORDIST(r->next->name, r->next->num, "following", r->name, r->num,
	   						HAS_NO_CA_ATOM)*/
	 if (! r || ! get_named_atom( "C", r, at))
		return 0;
	if ( ! r->next || ! get_named_atom( "N", r->next, at+1))
		return 0;

	dis = resatom_distca(at);

	return dis;

#undef ERRORDIST
}
/*------------------------------------------------------------------------------*/
/* compute the distance between CA and CA+1					*/
/*------------------------------------------------------------------------------*/
int get_distca_1_2( Residu *r)
{
	ResAtom at[2];
						/* respectively: at[0] -> CA	*/
						/*               at[1] -> CA+1	*/

#define ERRORDIST(X,NX,S,Y,NY,CAR)		{ r->dis12 = NO_ANGLE;		\
						angle_error(X,NX,S,Y,NY,CAR,3);	\
						return 1; }
	r->dis12 = 0.0;
			r->ca = 1;

	if (! r) {
	   myerror("get_distca_1_2:: residue does not exist",0);
	   return 1;
	}
					/* find CA atom of current residue */
	else 	if ( ! get_named_atom( "CA", r, at)){
			r->ca = -1;
	   		ERRORDIST(r->name, r->num, NULL, NULL, 0, HAS_NO_CA_ATOM)
		}
	if (( ! r->next) || (r->num != r->next->num - 1) )
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)
					/* find CA atom of next residue */
	else if ( ! get_named_atom( "CA", r->next, at+1))
	   ERRORDIST(r->next->name, r->next->num, "following", r->name, r->num,
	   						HAS_NO_CA_ATOM)

	r->dis12 = (float)resatom_distca(at);	/* entre r et r->next */

	return 0;

#undef ERRORDIST
}

/* -------------------------------------------------------------------- */
/* put a warning message when an angle cannot be			*/
/* computed (somewhat verbose ?)					*/
/* -------------------------------------------------------------------- */
static void angle_error(char *nam1, int num1, char *spos,
		char *nam2, int num2, char smiss, int subroutine)
{
	char subroutine_name[20], smissmsg[50], strerr[256];

#define SUBCPY(X)	strcpy(subroutine_name, X); break

	switch(subroutine) {

	   case 0: SUBCPY("phi_psi   ");

	   case 1: SUBCPY("chi       ");

	   case 2: SUBCPY("alpha_tau ");

	   case 3: SUBCPY("distca_1_2");

	   case 4: SUBCPY("distca_1_3");

	}

#undef SUBCPY

#define SMISSCPY(X)	strcpy(smissmsg, X); break

	switch(smiss) {

/*	   case HAS_NO_CA_ATOM: SMISSCPY("has no CA atom");

	   case HAS_NO_CB_ATOM:SMISSCPY("has no CB atom");

	   case HAS_NO_N_ATOM: SMISSCPY("has no N atom");

	   case HAS_NO_CO_ATOM: SMISSCPY("has no CO atom");

	   case HAS_NO_CG_ATOM: SMISSCPY("has no CG(1), OG(1) or SG atom");

	   case HAS_NO_CD_ATOM: SMISSCPY("has no CD(1), OD(1), SD or ND(1) atom");

	   case HAS_NO_CE_ATOM: SMISSCPY("has no CE, CE(1), OE(1), NE or NE(1) atom");

	   case AT_START_OF_CHAIN: SMISSCPY("at start of chain");

	   case AT_END_OF_CHAIN: SMISSCPY("at end of chain");*/

	   case HAS_NO_CA_ATOM: SMISSCPY("pas de CA");

	   case HAS_NO_CB_ATOM:SMISSCPY("pas de CB");

	   case HAS_NO_N_ATOM: SMISSCPY("pas de N");

	   case HAS_NO_CO_ATOM: SMISSCPY("pas de CO");

	   case HAS_NO_CG_ATOM: SMISSCPY("pas de CG(1), OG(1) ou SG");

	   case HAS_NO_CD_ATOM: SMISSCPY("pas de CD(1), OD(1), SD ou ND(1)");

	   case HAS_NO_CE_ATOM: SMISSCPY("pas de CE, CE(1), OE(1), NE ou NE(1)");

	   case AT_START_OF_CHAIN: SMISSCPY("debut de chaine");

	   case AT_END_OF_CHAIN: SMISSCPY("fin de chaine");

	}

#undef SMISSCPY

	if (nam2) {

	   sprintf(strerr, "%s :: residu %s %4d %s residu %s %4d %s",
		subroutine_name, nam1, num1, spos,nam2, num2, smissmsg);

	   myInfoOnStderr(strerr);

	}

	else {

	   sprintf(strerr, "%s :: residu %s %4d %s",
		subroutine_name, nam1, num1, smissmsg);

	   myInfoOnStderr(strerr);

	}

}
/*----------------------------------------------------------------------*/
/* rebuild from internal coordinates					*/
/*----------------------------------------------------------------------*/
static ResAtom *atom_pos(ResAtom *ra, float dist, float angl, float dihd)
{
	double	xn,yn,zn,ax,ay,az,bx,by,bz,cx,cy,cz,a,b,c,pm;
	char	got[10];

	angl = angl * D2R;
	dihd = dihd * D2R;

	xn = dist*cos(angl);
	yn = dist*sin(angl)*cos(dihd);
	zn = -dist*sin(angl)*sin(dihd);

	ax = (ra+1)->x - (ra+2)->x;			/* calcul des composantes vectorielles	*/
	bx = (ra+1)->y - (ra+2)->y;
	cx = (ra+1)->z - (ra+2)->z;

	pm = sqrt(ax*ax + bx*bx + cx*cx);
	xn = xn/pm;

	a = (ra)->x - (ra+1)->x;
	b = (ra)->y - (ra+1)->y;
	c = (ra)->z - (ra+1)->z;

	az = bx*c - cx*b;
	bz = cx*a - ax*c;
	cz = ax*b - bx*a;

	pm = sqrt(az*az + bz*bz + cz*cz);
	zn = zn/pm;

	ay = bz*cx - cz*bx;
	by = cz*ax - az*cx;
	cy = az*bx - bz*ax;

	pm = sqrt(ay*ay + by*by + cy*cy);
	yn = yn/pm;

	(ra+3)->x = ax*xn+ay*yn+az*zn + (ra+2)->x;
	(ra+3)->y = bx*xn+by*yn+bz*zn + (ra+2)->y;
	(ra+3)->z = cx*xn+cy*yn+cz*zn + (ra+2)->z;

return ra;
}
/*----------------------------------------------------------------------*/
/* init of the rebuilt from internal coordinates			*/
/*----------------------------------------------------------------------*/
static ResAtom *init_pos(Residu *pre, float dist, float angl, float dihd)
{

	Residu	*cur_res,
		*cur_r;
	int	i;
	double  ang,dih;
	char	got[10],
		fil_pdb[30];
	
	ResAtom ra[4];

	printf("\n Quel nom de fichier de sortie [* %s *]?","pdbout.pdb");
		gets(got);
		sscanf(got,"%s",fil_pdb);		
	if ((pdbout = fopen(fil_pdb, "w")) == NULL) 	
		if ((pdbout = fopen("pdbout.pdb", "w")) == NULL) 
			exit(1);			
	
fprintf(pdbout,"REMARK	      Reconstruction a partir des coordonnees internes\n");

	cur_res = pre;
	get_named_atom("CA", pre, ra);
	get_named_atom("CA", pre->next, ra+1);
	get_named_atom("CA", pre->next->next, ra+2);

ang = angl * D2R;	/*	deg -> rad	*/
dih = dihd * D2R;

ra->x = 0.0;
ra->y = 0.0;
ra->z = 0.0;
fprintf(pdbout,"ATOM    %3d  CA  %s   %3d    %8.3f%8.3f%8.3f\n",cur_res->num,cur_res->name,cur_res->num,ra->x,ra->y,ra->z);
(ra+1)->x = dist;
(ra+1)->y = 0.0;
(ra+1)->z = 0.0;
cur_res = cur_res->next;
fprintf(pdbout,"ATOM    %3d  CA  %s   %3d    %8.3f%8.3f%8.3f\n",cur_res->num,cur_res->name,cur_res->num,(ra+1)->x,(ra+1)->y,(ra+1)->z);
(ra+2)->x = (ra+1)->x - dist * cos(ang);
(ra+2)->y = dist * sin(ang);
(ra+2)->z = 0.0;
cur_res = cur_res->next;
fprintf(pdbout,"ATOM    %3d  CA  %s   %3d    %8.3f%8.3f%8.3f\n",cur_res->num,cur_res->name,cur_res->num,(ra+2)->x,(ra+2)->y,(ra+2)->z);

	for (cur_r = cur_res->next,i=3 ; cur_r ; cur_r = cur_r->next, i++) {
		get_named_atom("CA", cur_r, ra+3);
		atom_pos(ra,cur_r->prev->dis12,cur_r->prev->tau, cur_r->prev->prev->alpha);
		fprintf(pdbout,"ATOM    %3d  CA  %s   %3d    %8.3f%8.3f%8.3f\n",cur_r->num,cur_r->name,cur_r->num,(ra+3)->x,(ra+3)->y,(ra+3)->z);
		ra[0] = ra[1];
		ra[1] = ra[2];
		ra[2] = ra[3];
	}

fprintf(pdbout,"TER     %3d \n",i);
fprintf(pdbout,"END\n");

return ra;
}

/* -------------------------------------------------------------------- */
/* computes the angle defined by the 3 points				*/
/* given in ra. returns angle in [-180, 180] deg.			*/
/* -------------------------------------------------------------------- */
static double atom_angle(ResAtom *ra)
{
	double	xa, ya, za, xb, yb, zb,
		xc, yc, zc, np1np2, ang;

				/* calcul des composantes vectorielles	*/

	xa = ra->x - (ra+1)->x;
	ya = ra->y - (ra+1)->y;
	za = ra->z - (ra+1)->z;

	xb = (ra+2)->x - (ra+1)->x;
	yb = (ra+2)->y - (ra+1)->y;
	zb = (ra+2)->z - (ra+1)->z;


				/* (produit des normes)			*/

	np1np2 = sqrt((xa*xa+ya*ya+za*za) * (xb*xb+yb*yb+zb*zb));

	if (np1np2 == 0) 
		return NO_ANGLE;

				/* produit vectoriel 1*2		*/

	xc =   ya*zb - yb*za;
	yc = - xa*zb + xb*za;
	zc =   xa*yb - xb*ya;
				/* arcsinus de norme du produit vect.	*/

	ang = asin(sqrt(xc*xc+yc*yc+zc*zc)/np1np2);

				/* ang is between -Pi/2 and Pi/2	*/
				/* we need to know the sign		*/
				/* of cosinus (produit scalaire)	*/
	
	if ((xa*xb+ya*yb+za*zb) <  0)			/* cos is < 0	*/
		ang = PI - ang;


	return ang * R2D;				/* rad -> deg	*/

}

/* -------------------------------------------------------------------- */
/* computes the dihedral angle defined by the 4 points			*/
/* given in ra. returns angle in [-180, 180] deg.			*/
/* -------------------------------------------------------------------- */
static double atom_dihed(ResAtom *ra)
{
	double	xa, ya, za, px1, py1, pz1,
		xb, yb, zb, px2, py2, pz2,
		xc, yc, zc, px3, py3, pz3,
		np1np2, dihed;

				/* calcul des composantes vectorielles	*/

	xa = (ra+1)->x - ra->x;
	ya = (ra+1)->y - ra->y;
	za = (ra+1)->z - ra->z;

	xb = (ra+2)->x - (ra+1)->x;
	yb = (ra+2)->y - (ra+1)->y;
	zb = (ra+2)->z - (ra+1)->z;

	xc = (ra+3)->x - (ra+2)->x;
	yc = (ra+3)->y - (ra+2)->y;
	zc = (ra+3)->z - (ra+2)->z;

					/* 1er produit vectoriel	*/

	px1 =   ya*zb - yb*za;
	py1 = - xa*zb + xb*za;
	pz1 =   xa*yb - xb*ya;

					/* 2eme produit vectoriel	*/

	px2 =   yb*zc - yc*zb;
	py2 = - xb*zc + xc*zb;
	pz2 =   xb*yc - xc*yb;

				/* (produit des normes)			*/

	np1np2 = sqrt((px1*px1+py1*py1+pz1*pz1) * (px2*px2+py2*py2+pz2*pz2));

	if (np1np2 == 0) 
		return NO_ANGLE;

				/* arccosinus du produit scalaire	*/
				/* des produits vectoriels 		*/


	dihed = acos((px1*px2+py1*py2+pz1*pz2)/np1np2);

				/* dihed is between 0 and Pi		*/
				/* we need the sign			*/
				/* produit vectoriel des produits vect.	*/

	px3 =   py1*pz2 - py2*pz1;
	py3 = - px1*pz2 + px2*pz1;
	pz3 =   px1*py2 - px2*py1;
	
				/* produit scalaire du 2eme vect	*/
				/* avec prod. vect. des prod. vect.	*/
	
	if ((px3*xb + py3*yb + pz3*zb) <  0)		/* sine is < 0	*/
		dihed = -dihed;


	return dihed * R2D;			/* rad -> deg	*/

}

/* -------------------------------------------------------------------- */
/* computes phi, psi and omega dihed. angles				*/
/* put result in r and returns 1 if all angles defined			*/
/* atoms coordinates are given in ra (see below)			*/
/* -------------------------------------------------------------------- */
static int compute_phipsi(Residu *r, ResAtom *ra,
		int do_phi, int do_psi, int do_ome)
{
	r->phi = (do_phi ? atom_dihed(ra)     : NO_ANGLE);
	r->psi = (do_psi ? atom_dihed(ra + 1) : NO_ANGLE);
	r->ome = (do_ome ? atom_dihed(ra + 2) : NO_ANGLE);

	return (    (r->phi != NO_ANGLE)
		 && (r->psi != NO_ANGLE)
		 && (r->ome != NO_ANGLE));
}

/* -------------------------------------------------------------------- */
/* computes chi1, chi2 and chi3 dihed. angles				*/
/* put result in r and returns 1 if all angles defined			*/	
/* atoms coordinates are given in ra (see below)			*/	
/* -------------------------------------------------------------------- */
static int compute_chis(Residu  *r, ResAtom *ra,
		int do_chi1, int do_chi2, int do_chi3)
{
	r->x1 = (do_chi1 ? atom_dihed(ra)     : NO_ANGLE);
	r->x2 = (do_chi2 ? atom_dihed(ra + 1) : NO_ANGLE);
	r->x3 = (do_chi3 ? atom_dihed(ra + 2) : NO_ANGLE);

	return (    (r->x1 != NO_ANGLE)
		 && (r->x2 != NO_ANGLE)
		 && (r->x3 != NO_ANGLE));
}

/* -------------------------------------------------------------------- */
/* check  phi, psi and omega computability, compute 			*/
/* them (put result in r) and return 1 if all angles			*/
/* are defined								*/
/* -------------------------------------------------------------------- */

#define CHECK_ANGLES 	if (! (dphi || dpsi || dome)) \
			   return compute_phipsi(r, ra, 0, 0, 0)

int get_phi_psi(Residu *r)
{
	int	dphi, dpsi, dome;	/* computability flags		*/
	ResAtom ra[6];			/* ra[0] -> CO-1		*/
					/* ra[1] -> NA  		*/
					/* ra[2] -> CA  		*/
					/* ra[3] -> CO  		*/
					/* ra[4] -> NA+1		*/
					/* ra[5] -> CA+1		*/

	if (! r) {
	   myerror("get_phi_psi:: residue does not exist",0);
	   return 0;
	}

	dphi = dpsi = dome = 1;		/* assume all is ok 		*/

	/* ------------------------------------------------------------ */
	/* first of all, we need CA and CO atoms of the current residue	*/
	/* for phi, psi and omega					*/
	/* ------------------------------------------------------------ */

	if (! get_named_atom("CA", r, ra + 2)) {
	   angle_error(r->name, r->num, NULL, NULL, 0, HAS_NO_CA_ATOM, 0);
	   dphi = dpsi = dome = 0;
	}

	CHECK_ANGLES;

	if (! get_named_atom("C", r, ra + 3)) {
	   angle_error(r->name, r->num, NULL, NULL, 0,HAS_NO_CO_ATOM, 0);
	   dphi = dpsi = dome = 0;
	}

	CHECK_ANGLES;

	/* ------------------------------------------------------------ */
	/* we need the N atom too (for phi and psi but not for omega)	*/
	/* ------------------------------------------------------------ */

	if (! get_named_atom("N", r, ra + 1)) {
	   angle_error(r->name, r->num, NULL, NULL, 0,HAS_NO_N_ATOM, 0);
	   dphi = dpsi = 0;
	}

	CHECK_ANGLES;

	/* ------------------------------------------------------------ */
	/* phi angle is undefined for the first residue of a chain	*/
	/* else, we need the CO atom of previous residue for phi	*/
	/* ------------------------------------------------------------ */
	
	if (! (r->prev && (r->num == r->prev->num + 1))) {
	   angle_error(r->name, r->num, NULL, NULL, 0, AT_START_OF_CHAIN, 0);
	   dphi = 0;
	}
	else if (! get_named_atom("C", r->prev, ra + 0)) {
	   angle_error(r->prev->name, r->prev->num, "preceding", 
		       r->name, r->num, HAS_NO_CO_ATOM, 0);
	   dphi = 0;
	}

	CHECK_ANGLES;

	/* ------------------------------------------------------------ */
	/* psi and omega are undefined for the last residue of a chain	*/
	/* else, we need the NA and CA atoms in next residue to compute	*/
	/* them								*/
	/* ------------------------------------------------------------ */
	
	if (! (r->next && (r->next->num == r->num + 1))) {
	   angle_error(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN, 0);
	   dpsi = dome = 0;
	}
	else if (! get_named_atom("N", r->next, ra + 4)) {
	   angle_error(r->next->name, r->next->num, "following", 
		   r->name, r->num, HAS_NO_N_ATOM, 0);
	   dpsi = dome = 0;
	}
	else if (! get_named_atom("CA", r->next, ra + 5)) {
	   angle_error(r->next->name, r->next->num, "following", 
		   r->name, r->num, HAS_NO_CA_ATOM, 0);
	   dome = 0;
	}

	CHECK_ANGLES;

	return compute_phipsi(r, ra, dphi, dpsi, dome);
}

#undef CHECK_ANGLES

/* -------------------------------------------------------------------- */
/* check  chi1, 2, 3 computability, compute them			*/
/* put result in r) and return 1 if all angles are			*/
/* defined								*/
/* -------------------------------------------------------------------- */

#define CHECK_ANGLES 	if (! (dchi1 || dchi2 || dchi3)) \
			   return compute_chis(r, ra, 0, 0, 0)

int get_chi(Residu *r)
{
	int	dchi1, dchi2, dchi3;	/* computability flags		*/	
	ResAtom ra[6];			/* ra[0] -> NA			*/
					/* ra[1] -> CA  		*/
					/* ra[2] -> CB  		*/
					/* ra[3] -> CG  		*/
					/* ra[4] -> NA+1		*/
					/* ra[5] -> CA+1		*/

	if (! r) {
	   myerror("get_chi:: residue does not exist",0);
	   return 0;
	}

	dchi1 = dchi2 = dchi3 = 1;	/* assume all is ok		*/

	/* ------------------------------------------------------------ */
	/* first, check residus for which chi's cannot be computed	*/
	/* (silent)							*/
	/* ------------------------------------------------------------ */

	if (strstr(sResiduNoChi1, r->name))
	   dchi1 = dchi2 = dchi3 = 0;
	else if (strstr(sResiduNoChi2, r->name))
	   dchi2 = dchi3 = 0;
	else if (strstr(sResiduNoChi3, r->name))
	   dchi3 = 0;

	CHECK_ANGLES;

	/* ------------------------------------------------------------ */
	/* we need NA atom of current residue for chi1			*/
	/* ------------------------------------------------------------ */

	if (dchi1 && (! get_named_atom("N", r, ra))) {
	   angle_error(r->name, r->num, NULL, NULL, 0, HAS_NO_N_ATOM, 1);
	   dchi1 = 0;
	}

	CHECK_ANGLES;

	/* ------------------------------------------------------------ */
	/* we need CA atom of current residue for chi1 and chi2		*/
	/* ------------------------------------------------------------ */

	if ((dchi1 || dchi2) && (! get_named_atom("CA", r, ra + 1))) {
	   angle_error(r->name, r->num, NULL, NULL, 0, HAS_NO_CA_ATOM, 1);
	   dchi1 = dchi2 = 0;
	}

	CHECK_ANGLES;

	/* ------------------------------------------------------------ */
	/* we need CB atom of current residue for chi1, chi2 and chi3	*/
	/* ------------------------------------------------------------ */

	if ((dchi1 || dchi2 || dchi3) &&
		(! get_named_atom("CB", r, ra + 2))) {
	   angle_error(r->name, r->num, NULL, NULL, 0, HAS_NO_CB_ATOM, 1);
	   dchi1 = dchi2 = dchi3 = 0;
	}

	CHECK_ANGLES;

	/* ------------------------------------------------------------ */
	/* we need CG(1) or OG(1) or SG atom of current residue 	*/
	/* for chi1, chi2 and chi3					*/
	/* ------------------------------------------------------------ */

	if ((dchi1 || dchi2 || dchi3) && 
	      (!( get_named_atom("CG" , r, ra + 3) /* ASP, ASN, TYR,	*/
						   /* PHE, GLU, GLN,	*/
						   /* LYS, LEU, TRP,	*/
						   /* PRO, MET, HIS	*/
	       || get_named_atom("CG1", r, ra + 3) /* VAL,ILE		*/
	       || get_named_atom("OG" , r, ra + 3) /* SER		*/
	       || get_named_atom("OG1", r, ra + 3) /* THR		*/
	       || get_named_atom("SG" , r, ra + 3) /* CYS		*/
	       ))) {
	   angle_error(r->name, r->num, NULL, NULL, 0, HAS_NO_CG_ATOM, 1);
	   dchi1 = dchi2 = dchi3 = 0;
	}

	CHECK_ANGLES;

	/* ------------------------------------------------------------ */
	/* we need CD(1) or equivalent atom of current residue 		*/
	/* for chi2 and chi3						*/
	/* ------------------------------------------------------------ */

	if ( (dchi2 || dchi3) &&
	      (!( get_named_atom("CD" , r, ra + 4)
	       || get_named_atom("CD1", r, ra + 4)
	       || get_named_atom("OD1", r, ra + 4) 	/* ASP, ASN 	*/
	       || get_named_atom("SD" , r, ra + 4) 	/* MET		*/
	       || get_named_atom("ND1", r, ra + 4) 	/* HIS 		*/
	       ))) {
	   angle_error(r->name, r->num, NULL, NULL, 0, HAS_NO_CD_ATOM, 1);
	   dchi2 = dchi3 = 0;
	}

	CHECK_ANGLES;

	/* ------------------------------------------------------------ */
	/* we need CE(1) or equivalent atom of current residue 		*/
	/* for chi3							*/
	/* ------------------------------------------------------------ */

	if (dchi3 &&
	      (!( get_named_atom("CE" , r, ra + 5)
	       || get_named_atom("CE1", r, ra + 5)
	       || get_named_atom("OE1", r, ra + 5) 	/* GLU,GLN	*/
	       || get_named_atom("N"  , r, ra + 5) 	/* PRO		*/
	       || get_named_atom("NE" , r, ra + 5) 	/* ARG		*/
	       || get_named_atom("NE1", r, ra + 5) 	/* TRP		*/
	       ))) {
	   angle_error(r->name, r->num, NULL, NULL, 0, HAS_NO_CE_ATOM, 1);
	   dchi3 = 0;
	}

	CHECK_ANGLES;

	return compute_chis(r, ra, dchi1, dchi2, dchi3);

}

#undef CHECK_ANGLES

/* -------------------------------------------------------------------- */
/* computes alpha dihed. angle and tau angle				*/
/* put result in r and returns 1 if all angles defined			*/	
/* atoms coordinates are given in ra (see below)			*/	
/* -------------------------------------------------------------------- */
static int compute_alpha_tau(Residu  *r, ResAtom *ra,
		int do_tau, int do_alpha)
{
	r->tau   = (do_tau   ? atom_angle(ra)  : NO_ANGLE);
	r->alpha = (do_alpha ? atom_dihed(ra)  : NO_ANGLE);

	return (    (r->tau != NO_ANGLE)
		 && (r->alpha != NO_ANGLE));
}

/* -------------------------------------------------------------------- */
/* computes the tau angle between CA-1,CA and CA+1			*/
/* and computes the alpha angle between CA-1,CA, CA+1 and CA+2		*/
/* returns what compute_alpha_tau returns				*/
/* find an angle : 1, cannot find an angle : 0				*/
/* -------------------------------------------------------------------- */
int	get_alpha_tau(Residu *r)
{
	int	dtau, dalpha;		/* computability flags		*/	
	ResAtom ra[4];			/* ra[0] -> CA-1		*/
					/* ra[1] -> CA  		*/
					/* ra[2] -> CA+1  		*/
					/* ra[3] -> CA+2  		*/

#define CHECK_ANGLES 	if (! (dtau || dalpha )) \
			   return compute_alpha_tau(r, ra, 0, 0)

	dtau = dalpha = 1;		/* assume all is ok		*/

	/* ------------------------------------------------------------ */
	/* verify that all residues exist				*/
	/* ------------------------------------------------------------ */
	if ( ! r) {
	   myerror("get_alpha_tau:: residue does not exist",0);
	   return 0;
	}
	else if ( ! (r->prev && (r->prev->num == r->num-1))) {
	   angle_error(r->name, r->num, NULL, NULL, 0, AT_START_OF_CHAIN, 2);
	   dtau = dalpha = 0;
	}
	else if ( ! (r->next && (r->next->num == r->num+1))) {
	   angle_error(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN, 2);
	   dtau = dalpha = 0;
	}
	else if(! ((r->next->next) &&
			(r->next->next->num == r->num+2))) {
	   angle_error(r->next->name, r->next->num, NULL, NULL, 0,
	   					AT_END_OF_CHAIN, 2);
	   dalpha = 0;
	}

	CHECK_ANGLES;

	/* here dtau is 1 */

	/* ------------------------------------------------------------ */
	/* we need CA atom of current residue for both tau and alpha	*/
	/* ------------------------------------------------------------ */
	if ( ! get_named_atom( "CA", r, ra+1) ) {
	   angle_error(r->name, r->num, NULL, NULL, 0, HAS_NO_CA_ATOM, 2);
	   dtau = dalpha = 0;
	}

	CHECK_ANGLES;

	/* ------------------------------------------------------------ */
	/* we need CA atom of previous residue for both tau and alpha	*/
	/* ------------------------------------------------------------ */

	if ( ! get_named_atom( "CA", r->prev, ra)) {
	   angle_error(r->prev->name, r->prev->num, NULL, NULL, 0,
	   						HAS_NO_CA_ATOM, 2);
	   dtau = dalpha = 0;
	}

	CHECK_ANGLES;

	
	/* ------------------------------------------------------------ */
	/* we need CA atom of next residue for both tau and alpha	*/
	/* ------------------------------------------------------------ */
	if ( ! get_named_atom( "CA", r->next, ra+2)) {
	   angle_error(r->next->name, r->next->num, NULL, NULL, 0,
	   					HAS_NO_CA_ATOM, 2);
	   dtau = dalpha = 0;
	}

	CHECK_ANGLES;

	/* here dtau is 1 */

	/* ------------------------------------------------------------ */
	/* we need CA atom of next->next residue for alpha		*/
	/* ------------------------------------------------------------ */

	if (dalpha && (! get_named_atom( "CA", r->next->next, ra+3))) {
	   angle_error(r->next->next->name, r->next->next->num,
	   				NULL, NULL, 0, HAS_NO_CA_ATOM, 2);
	   dalpha = 0;
	}

	return compute_alpha_tau(r, ra, dtau, dalpha);

#undef CHECK_ANGLES

}
/* -------------------------------------------------------------------- */
/* extract a pdb field from a pdb line					*/
/* field starts at start and has a length len				*/
/* -------------------------------------------------------------------- */
static char *pdb_field(char *line, int start, int len)
{
	static char buffer[MLL];

	strncpy(buffer, line + start, len);
	buffer[len] = '\000';

	return buffer;
}
/* -------------------------------------------------------------------- */
/* recherche les debuts de chaines					*/
/* -------------------------------------------------------------------- */
Residu	*recherche_deb(Residu *deb)
{
	Residu	*r = deb;

	for (; r->next; r = r->next){
		if (r->chain != r->next->chain)
			break;
	}
	if (r == deb){
		fprintf(stderr, "Probleme avec la chaine %c, aa %c %d\n", r->chain, deb->code, r->num);
		return deb->next;
	/*
	fprintf(stderr, "%c:%c ", deb->code, r->code);
	fprintf(stderr, "%3d/%3d\n", deb->num, r->num);
	*/
	}

return r;
}
/* -------------------------------------------------------------------- */
/* read the sequence to determine the type of oligomers			*/
/* -------------------------------------------------------------------- */
int	compare_chain(Residu *start, Residu *new)
{
	Residu	*r = start, *res = new;
	int 	sim = 0, n = 0, total = 0, i = 0;

	for (; r && res && i < 30000; res = res->next, r = r->next, i++){
		total++;
		if (r->code == res->code)
			sim++;
		if (r->chain != start->chain || res->chain != new->chain  )
			break;
	}
	if (total == 0)
		return 0;
	/*
fprintf(stderr, "%3d/%3d\n", sim, total);
*/

return TV((double)sim/total, 0.91, 0.1);
}
/* -------------------------------------------------------------------- */
/* check the repetition in the sequence 				*/
/* return 1 if similar sequences are found 				*/
/* -------------------------------------------------------------------- */
int 	check_repeat(Residu *first_r, Residu *new)
{

	Residu 	*start, *r, *res;
	int 	rep = 0;

	for (start = first_r; new->next && start->next != new && start; start = recherche_deb(start)) {
		/*fprintf(stderr, "%c:%c\n", new->code, start->code);*/
		if (compare_chain(start->next, new))
			return YES;
		else if (rep++ > 50)
			return NO;
	}

return NO;
}
/* -------------------------------------------------------------------- */
/* read an atom (in a pdb line)						*/
/* -------------------------------------------------------------------- */
static int get_atom(char *line, Atom *atom)

{
float	Standard = 1.00,
	Fix = 0.00;

	GET_FIELD( 6, 5, "%hd", &(atom->num));

	GET_FIELD(12, 4, "%s",  atom->name);

	GET_FIELD(17, 3, "%s",  atom->resn);

	/* on saute un caractere apres le residu	*/
	/* ce caractere indique la chaine		*/

	if (sscanf(pdb_field(line, 21, 1), "%c", &(atom->chain)) != 1)
	   atom->chain = NO_CHAIN;

	GET_FIELD(22, 4, "%hd",  &(atom->ires));

	if (sscanf(pdb_field(line, 26, 1), "%c", &(atom->inser)) != 1)
	   atom->inser = NO_INSER;

	GET_FIELD(30, 8, "%f",   &(atom->x));
	GET_FIELD(38, 8, "%f",   &(atom->y));
	GET_FIELD(46, 8, "%f",   &(atom->z));

/*	GET_FIELD(54, 6, "%f",   &(atom->localize));*/		/* COMPOSER */
	if (sscanf(pdb_field(line, 54, 6), "%f", &(atom->localize)) != 1)
	   atom->localize = Standard;
	if (sscanf(pdb_field(line, 60, 6), "%f", &(atom->tfactor)) != 1)
	   atom->tfactor = Fix;
/*	GET_FIELD(60, 6, "%f",   &(atom->tfactor));*/		/* COMPOSER */

	return 1;
}

#undef GET_FIELD
/************************************************************************/
/*									*/
/************************************************************************/
int	test_sel(char line[LINE_LENGTH])
{

	if (!strncmp(line, "HETATM", 6) && (!strncmp(line+17, "SEC", 3) || !strncmp(line+17, "MSE",3)) )   
		return 1;

return 0;

}
/* -------------------------------------------------------------------- */
/* reads residues and atoms from a pdb file return the first residue	*/
/* which is a dummy one	or NULL if fails				*/
/* -------------------------------------------------------------------- */
Residu	*readPdbResidues(FILE *f)
{
   char		line[LINE_LENGTH], key[LINE_LENGTH], nom[LINE_LENGTH],
   		desc[1024], old_inser = NO_INSER;
   float	resolution;
   Atom		atom;
   int		i, nb_residus_inseres = 0, total_residus_inseres = 0;
/* int		total_residus = 0; */
   Residu	*cur_res, *prev_res, *first_res;
 
						/* first res is a 	*/
						/* dummy res		*/
   if ((first_res = cur_res = new_residu()) == NULL)
   	return NULL;
   first_res->prev  = NULL;
   first_res->num = -32000;			/* improbable		*/

   *nom = *desc = '\000';
   resolution   =  0;

						/* -- read loop		 */
   while (fgets(line, LINE_LENGTH, f)) {

	sscanf(line, "%s", key);

	if (! strcmp(key, "HEADER")) {		/* - HEADER		*/
	   sscanf(line + 72, "%s", nom);
	   low(nom);
	}

	else if (! (  strcmp(key, "COMPND")
		   && strcmp(key, "SOURCE"))) {	/* - COMPBD || SOURCE	*/
	   trimr(line, 70);
	   strcat(desc, line + 10);
	}

	else if (! strcmp(key, "REMARK")) {	/* - REMARK	*/
	   if (! strncmp(line + 9, "2 RESOLUTION.", 13))
		sscanf(line + 22, "%f", &resolution); 
	}
	else if (! strcmp(key, "ENDMDL")) {		/* - ENDMDL		*/
	   break;
	}

	else if (! strcmp(key, "ATOM") || test_sel(line)) {  	/* - ATOM	*/

           if (! get_atom(line, &atom) )
           	continue;

	   if (   (strlen(atom.resn) < 3)	/* unknown		*/
	       || (! strstr(sResiduNames, atom.resn)))
		continue;

	   if (atom.ires + total_residus_inseres != cur_res->num
	           || atom.inser != old_inser) {	
	           				/* residue changes i.e.	*/
						/* res num changes	*/
						/* or insertion changes	*/

		/* total_residus++; */
	
		if (atom.inser != old_inser) {
						/* insertion changes	*/
		   if (atom.ires + total_residus_inseres == cur_res->num) {
		      nb_residus_inseres = 0;
		      total_residus_inseres++;
		   }
		   else if (atom.inser != NO_INSER)
		         nb_residus_inseres++;
		}
		else {
		   total_residus_inseres += nb_residus_inseres;
		   nb_residus_inseres = 0;
		}

						/* creation of new res	*/
		prev_res = cur_res;
	   	cur_res = new_residu();
	   	prev_res->next = cur_res;
	   	cur_res->prev  = prev_res;
	   	cur_res->num = atom.ires + total_residus_inseres;
	   	strcpy(cur_res->name, atom.resn);
	   	cur_res->pdbnum  = atom.ires;
	   	cur_res->chain   = atom.chain;
	   	cur_res->inser   = atom.inser;
	   	cur_res->next    = NULL;
		cur_res->nb_ca	= -1;
		cur_res->code = 'U';
		for (i = 0; i < 26; i++)
			if (strncmp(cur_res->name, THREE[i],3) == 0)
				cur_res->code = ONE[i];

	   	old_inser = atom.inser;

		if (cur_res->num == prev_res->num + 1)
	   	     cur_res->nrel = prev_res->nrel + 1;
	   	else
	   	     cur_res->nrel = 0;
	
	   	cur_res->cut = cur_res->num - prev_res->num;  /* 1 is ok */

	   }

	   if (! put_atom_in_res(&atom, cur_res))
	   	return NULL;

	}
   } /* end while get line */
   
   return first_res;

}
/* -------------------------------------------------------------------- */
/* PUB									*/
/* -------------------------------------------------------------------- */
static void do_help()
{
        printf("o-----------------------------------------------o\n");
        printf("|                                               |\n");
        printf("| p-sea :   Version 0.99 LMCP       Sept   96   |\n");
        printf("|                                               |\n");
        printf("o-----------------------------------------------o\n");
        printf("| synopsis : phi psi omega alpha tau chi(s)     |\n");
        printf("| and CA-1 CA+1 distance computing              |\n");
        printf("|                                               |\n");
        printf("| usage: sea pdb_file [out.sea]                 |\n");
        printf("|                                               |\n");
        printf("| ' ' : assign secondary structure              |\n");
        printf("|                                               |\n");
        printf("o-----------------------------------------------o\n");
        printf("| options:                                      |\n");
        printf("| -p  : computes phi, psi and omega             |\n");
        printf("| -a  : computes alpha,tau and CA_CA+1 distance |\n");
        printf("| -x  : computes chi1,chi2 and chi3             |\n");
        printf("| -d  : computes CA-1_CA+1 distance             |\n");
        printf("| -h  : displays this help                      |\n");
        printf("| -m  : assign secondary structure              |\n");
        printf("o-----------------------------------------------o\n");
        printf("| if for any reason (missing atoms, angle not   |\n");
        printf("| defined,...) the angle cannot be computed,    |\n");
        printf("| it is set to 500.0                            |\n");
        printf("|                                               |\n");
        printf("o-----------------------------------------------o\n");
        printf("| output              : on standard output      |\n");
        printf("| comments and errors : on standard error       |\n");
        printf("|                                               |\n");
        printf("o-----------------------------------------------o\n");
        printf("|                                               |\n");
        printf("| The ouput values are in order (on one line):  |\n");
        printf("| Atom_name Residue_name Residue_number [Phi \\ |\n");
        printf("| Psi Omega] [Alpha Tau CA_CA+1_Distance]    \\ |\n");
        printf("| [Chi1 Chi2 Chi3] [CA-1_CA+1_Distance]         |\n");
        printf("|                                               |\n");
        printf("o-----------------------------------------------o\n");


}


/*  Gilles Labesse
Laboratoire de Mineralogie-Cristallographie 
Universites Paris VI et Paris VII, CNRS URA 09 
Macromolecules Biologiques	    	Tel   : (33 1) 44 27 45 85
4, place Jussieu		       	Fax   : (33 1) 44 27 37 85
75252 Paris Cedex 05 FRANCE	    	Email : labesse@lmcp.jussieu.fr  */

/*  Joel Pothier
Atelier de BioInformatique
Institut CURIE
Section de Physique et Chimie	    	Tel   : (33 1) 42 34 67 60
11, rue Pierre et Marie Curie       	Fax   : (33 1) 40 51 06 36
75231 Paris Cedex 05 FRANCE	   	Email : jompo@radium.jussieu.fr  */

