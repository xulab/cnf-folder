/*******************************************************************************

                                  ESPEnergy.h
                                  ------

  ****************************************************************************
  *                                                                          *
  *   This library is free software; you can redistribute it and/or          *
  *   modify it under the terms of the GNU Lesser General Public             *
  *   License as published by the Free Software Foundation; either           *
  *   version 2.1 of the License, or (at your option) any later version.     *
  *                                                                          *
  *   This library is distributed in the hope that it will be useful,        *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *
  *   Lesser General Public License for more details.                        *
  *                                                                          *
  *   You should have received a copy of the GNU Lesser General Public       *
  *   License along with this library; if not, write to the Free Software    *
  *   Foundation, Inc.,                                                      *
  *   59 Temple Place, Suite 330, Boston, MA  02111-1307  USA                *
  *                                                                          *
  ****************************************************************************

  Copyright (C) 2005 The University of Chicago

  Authors:
  Feng Zhao

  Description:
  Energy function to calculate ESP energies given the information of the
  the residue, radius of gyration and number of Ca contacts information.

*******************************************************************************/

#ifndef __ESPENERGY_H__
#define __ESPENERGY_H__

#include <vector>
#include "ScoreMatrix.h"

//min_RG=7; max_RG=39; max_N=25
#define MIN_N 1
#define MAX_N 25
#define MIN_RG 9  // 7+2
#define MAX_RG 27 //(39-4)-(7+2)+1

class ESPEnergy
{
public:
	ESPEnergy();
	~ESPEnergy();
	
	double energy_coeff;

	void Init(string CfgName);
	double Energy(string aa0, int n, double rg);

private:
	double* values;
	void InitializeEnergy();
	int AANameToCode(string AAName);
	
	int LoadFromFile(string &fn);
	void SetDefConfig();
};

class NewESPEnergy
{
public:
	NewESPEnergy();
	~NewESPEnergy(){if (weights) delete weights;
		if (Gates_table)delete Gates_table;
		if (psp)delete psp;
	};
	
	void Init(string CfgName, vector<string>& psp);
	double Energy(int res, double rg, int at_count);

private:
	int NUM_PARAMS, NUM_GATES, WINDOW_SIZE, NUM_DIM, NUM_BIN;
	double* weights;
	double* Gates_table;
	double* psp;
	double* p_ref;
	int m_len;
	double* probTable;

	double getGateWeight(int gate, int offset);
	double getStateWeight(int bin,int offset);
	//double ComputeProb(int res, double rg, int bin);
	double ComputeProb(int res, int rg, int bin);
	double ComputeRefProb(int res, double rg, int bin);
	int CalculatePSP(vector<string>& psp_s);
	int getbin(int nContact);
	void ComputeGatesTable();
	double prepareProb();
};

class EPAD_ESP
{
public:
	EPAD_ESP();
	~EPAD_ESP();
	
	void Init(int seq_len, string prob_table_file);
	double Energy(int n, double perc_SA);

private:
	int seq_len;
	ScoreMatrix* energyMat;
	int LoadProbTable(string & sProbFile);
};

#endif
