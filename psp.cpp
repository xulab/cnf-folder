#include <ctype.h>
#include "psp.h"
#include "util.h"
#include "stringTokenizer.h"

// *********** BY TINA, May 20, 2003
//  for controling debug output
//#include "global_config.h"
// *********** BY TINA, May 20, 2003

#define PSP_LOGFILE	"PSP_error.log"

//extern Config * config;

PSPInfo::PSPInfo(Config* conf){
	config=conf;
}

PSPInfo::~PSPInfo(){
}


bool PSPInfo::Load(char* file,short* PSP_array,int* seqLen){

	FILE* PSP_logfp=fopen(PSP_LOGFILE,"ab");
	if (!PSP_logfp){
		fprintf(stderr,"can not open or create %s \n",PSP_LOGFILE);
		PSP_logfp=NULL	;
	}

	//printf("begin to load PSPFile %s\n",file);

	FILE* psp_fp=fopen(file,"r");
	if (!psp_fp) {
		if (PSP_logfp){
			fprintf(PSP_logfp,"can not open PSP file %s\n",file);
			fclose(PSP_logfp);
		}
		return false;
	}

	//parse file, the following parse result for blastpgp

	char buf[4096];

	int lastSeqNum=0;

	while(fgets(buf,4096,psp_fp)){


		char* head=trim(buf);
	//	printf("buf=%s\n",buf);
		if (!head) continue;
		if (strstr(head,"position-specific")) continue;
		if (strstr(head,"Standard")) break;
		if (!isdigit(head[0])){
            		if (config->debug_level> DEBUG_LEVEL3)
                		printf("head[0]=%c\n",head[0]);
		 	continue;
		}

		int seqNum;
		int resChar;
		int prob[20];
/*
		double left[2];
		int score[20];
*/

		StringTokenizer* strTokenizer=new StringTokenizer(head," \t\n\r,");

		char* token=strTokenizer->getNextToken();
		seqNum=atoi(token);
		token=strTokenizer->getNextToken();
		resChar=token[0];

		if ((*seqLen)!=-1 && seqNum>(*seqLen)){
			fprintf(PSP_logfp,"in PSP file %s meet one seqNum (%d) greater than the told seq length (%d)\n",file,seqNum,(*seqLen));
			break;
		}

		//do some check
		/*
		if (resChar!=sequences[seqNum-1]){
			fprintf(PSP_logfp,"possible inconsistent between  PSP file %s AA (%c) and seq file AA (%c) \n",file,resChar,sequences[seqNum-1]);
		}
		*/

		token=strTokenizer->getNextToken();

		int i=0;
		while(token && token[0]){

		//	printf("token=%s\n",token);
/*

			if (i<20)
				score[i]=atoi(token);
			else if (i<40)
				prob[i-20]=atoi(token);
			else left[i-40]=atof(token);
*/
			if (i>=20 && i<40)
				prob[i-20]=atoi(token);

			i++;
			token=strTokenizer->getNextToken();
		}

		if (strTokenizer) delete strTokenizer;


		
		short* PSP_base=PSP_array+(seqNum-1)*21;
		int sum=0;
		for(int j=0;j<20;j++){
			PSP_base[j]=(short)((prob[j]/100.0)*PROB_BASE);
			sum+=prob[j];
		}
		if (sum<=95){
			//reset
			for(int j=0;j<20;j++)
				PSP_base[j]=0;
			PSP_base[AA1Coding[AA2SUB[toupper(resChar)-'A']]]=(short)(1*PROB_BASE);
		}


		//for debug
/*
		printf("%d :",seqNum);
		for(int l=0;l<20;l++)
			printf("%d ",PSP_base[l]);
		printf("\n");
*/

		lastSeqNum++;
		
	}

	fclose(psp_fp);

	bool res=true;

	if ((*seqLen)==-1){
		*seqLen=lastSeqNum;
	}else if (lastSeqNum!=(*seqLen)){
		fprintf(PSP_logfp,"%s:inconsistent lastSeqNum(%d) and seqLen(%d)!,maybe meet error in reading PSP file,this PSP file is not used!\n",file,lastSeqNum,*seqLen);
		res=false;
	}

	if (PSP_logfp) fclose(PSP_logfp);
	return res;
}


char* trim(char* s){

	if (!s) return 0;

	char* tmp=s;


	while(tmp[0] && (tmp[0]==' ' || tmp[0]=='\t' || tmp[0]=='\r' || tmp[0]=='\n'))
		tmp++;

	if (!tmp[0]) return 0;

	return tmp;
}

// for test

/*

void main(int argc, char** argv){

	PSPInfo psp;

	short* PSP_array=new short[1500*21];

	int len=-1;

	if(!psp.Load(argv[1],PSP_array,&len)){
		fprintf(stderr,"load psp file err!\n");
	}

	printf("psp len=%d\n",len);
	
}
*/


