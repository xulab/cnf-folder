/*******************************************************************************

                                  DopeBeta.cpp
                                  --------

  Copyright (C) 2005 The University of Chicago

  Authors: 
  Andr�s Colubri

  Description:
  Plugin that calculates pairwise statistical potentials. In particular, it 
  can calculate DopeBeta, the statistical potential created by Min-yi Shen and 
  Andrej Sali at UCSF.
  This plugin can be configured to evaluate other statistical potentials by 
  providing the appropriate parameter file. 

*******************************************************************************/
#ifndef __DOPE_BETA_H__
#define __DOPE_BETA_H__

#include "DOPE.h"

// This class contains the binned energy values.
class DopeBeta: public DOPE
{
public:
	DopeBeta(){DOPE();SetDefConfig();};
	~DopeBeta() {};

	double calcAtomDOPE(int residue_id1, int atom1, int residue_id2, int atom2, double dist); 
	void Init(string cfgFile);

protected:
	void SetDefConfig();
	int ReadConfigFile(string cfgName);
	void trim2(string& str) {
		string::size_type pos = str.find_last_not_of(' ');
		if(pos != string::npos) {
			str.erase(pos + 1);
			pos = str.find_first_not_of(' ');
			if(pos != string::npos) 
				str.erase(0, pos);
		}
		else str.erase(str.begin(), str.end());
	};

	void SetAddAtoms(bool add_h, bool add_cm, bool add_sc, bool add_onlyCB) {
		add_hydrogens = add_h;
		add_centroids = add_cm;
		add_side_chains = add_sc;
		add_only_CB = add_onlyCB;
	};

public:
	// Parameters.
	double dist_cutoff;
	int num_bin;
	string dope_par_file, dope_dir;
	bool add_bonded;
	bool add_hydrogens, add_centroids, add_side_chains, add_only_CB, CB_CB_focus, fix_consensus;
	int min_chain_dist, max_chain_dist, min_CB_CB_dist;
	double energy_coeff, CB_CB_coefficient;
	//int dope_vec[200][200];

	bool Burial_renorm, analysis;
	double Penalty_coeff;
	double Burial_sphere_rad;
	int Burial_min_atcount, Burial_max_atcount;
};

#endif
