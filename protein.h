#ifndef _ZHOUPROTEIN
#define _ZHOUPROTEIN
#include "restype.h"
#include "protpl.h"
using namespace std;

class ZhouAtom{
protected:
	bool bfill;
	double x[3];
	string name;
	int type, aseq;
	vector <int> ineib;
//
	int iz[4]; double ag[3];
/*	int ia, ib, ic, chiral;
	double bond, angle1, angle2;*/
public:
	ZhouAtom(){bfill=false; type=-1;}
	ZhouAtom(int is, int it, string an);
	void setname(string an, int it){name=an; type=it;}
	void settype(int it){type = it;}
	void setint(int *it){for(int i=0; i<4; i++) iz[i] = it[i];}
	void setzcrd(double *at){for(int i=0; i<3; i++) ag[i] = at[i];}
	void setx(string scrd){ str2dat(scrd, 3, x); bfill = true; };
	void setx(const double *xt){for(int m=0; m<3; m++) x[m]=xt[m]; bfill=true;};
	void setx(double xx, double xy, double xz){x[0]=xx; x[1]=xy; x[2]=xz; bfill=true;};
	void addneib(int it);
	void clearNeib(){ineib.clear();}
	bool xyzatm1(vector <ZhouAtom> &ap);
	void settorsion(double at){ag[2] = at;}
//
	double *getx(){return x;};
	string getname(){return name;};
	bool filled(){return bfill;};
	int gettype(){return type;};
	vector <int> *getineib(){return &ineib;};
	int *getint(){return iz;}
	double *getzcrd(){return ag;}
	double gettorsion(){return ag[2];}
};
class ZhouProtein{
protected:
	Protpl *aatpl;
	string pdbnm;
	int nres, natm, nchain;
// residues-
	vector <string> rnam, seq0_str;
	vector <int> resid, seq0, ichain, chainID;
// res & atoms
	vector <int> rstart;
// atoms
	vector <int> pdbidx;	//pos of pdb, for decoys trj
	vector <int> rseq;		// natm
	vector <ZhouAtom> atoms;		//natm
public:
	ZhouProtein(){natm = nres = 0; pdbnm="";aatpl=NULL;};
	ZhouProtein(FILE *fp){rdpdb(fp); pdbnm="";aatpl=NULL;};
	ZhouProtein(const string fn);
	bool rdpdb(FILE*);
	bool addRes(string);
	bool addRes(string resname, int cid, int chain0, int ires);
	bool initAtom(string atomname, double x, double y, double z);
	bool initAtom(string);
	void initneib();
	void initAtomint(int ia);
	void initAtomzcrd0(int i0);
	void calAtomzcrd(int ia);
	void insertAtoms(int idx, int na);
	void eraseAtoms(int idx, int na);
	void resMutation(int ir, string rn);
	void addCB_GLY(int ir);
	void makexyz1(int ia){atoms[ia].xyzatm1(atoms);}
	void makexyz1(int i1, int i2){for(int i=i1; i<i2; i++) makexyz1(i);}
//
	void wrpdb(FILE*);
	void wrpdb(string fn);
	void xdiff(int ia, int ib, double *xd);
	double distance2(int ia,int ib);
	double angle(int,int,int);
	double torsion(int,int,int,int);
	bool iscomplete();
	bool refill();
	double calResclash(int ir);
	int getReschi(int ir, int *idx);
	int findatom(int ires, string an);
	int getnres(){return nres;};
};

extern const double radian;
void xdiff(const double *xa, const double *xb, double *xab);
double distance2(const double *xa, const double *xb);
double angle(const double *xa, const double *xb, const double *xc);
double torsion(const double *xa, const double *xb, const double *xc, const double *xd);
bool xyzatm(double *xi, double bond, const double *xa, double angle1, const double *xb, 
				double angle2, const double *xc, int chiral);
#endif
