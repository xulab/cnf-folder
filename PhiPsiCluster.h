#ifndef _CRF_CLUSTER_ANG_H_
#define _CRF_CLUSTER_ANG_H_

#include <vector>
#define MAX_NUM_CLUSTER 300

using namespace std;

class PhiPsiCluster
{
protected:
	int mNumCluster;
	vector<vector<double> > mCluster;

public:
	PhiPsiCluster(const char* aFileName);
     	~PhiPsiCluster();
	int getNumClusters() {return mNumCluster;};
	vector<vector<double> >& getClusters() { return mCluster;};
	vector<double>& getClusters(int index) {return mCluster[index]; };
};


#endif
