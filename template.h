
#ifndef _IP_TEMPLATE_H
#define _IP_TEMPLATE_H


#include <stdio.h>
#include <string.h>

#include "IP_constant.h"
#include "config.h"
#include "param.h"
#include "util.h"
//#include "CompressedGraph.h"
#include "stringTokenizer.h"

typedef struct {
	int leftPos; //left end of the contact
	int rightPos; // right end of the contact
	float dist;
}CONTACT;


typedef struct {
	short flag; //0 for a.a without structure
	short gapFlag; //1 for starting a gap;
	short residue; //coding of residue type (0-19)
	int cResidue; // char representation of this residue

	int resID; // residue number in PDB
	char extraChar; //extra char after resID in PDB
	short structType; //secondary structure type
	short solventType; //solvent accessibility type;
	short solvent;	//solvent accessibilty value
	float volSolvent;	//solvent accessibility defined by contact surface area
	float subunitSolvent;	//subunit solvent accessibility defined by contact surface area
				//in this version, I am not going to use it
	short secondaryStruct ; //order of its host secondary str
	float xPos, yPos, zPos; //C-beta coordinates
	float xPos2,yPos2,zPos2;	//C-alpha coordinates

	char buf[256];

	int contactNum;
	int sContactNum;
	int lContactNum;


} TemplateSingleton;

typedef float DIST_T;


typedef struct {

	DIST_T dist;

	//if VolContact is used, then these two fields should be used rather than dist
	//VolContact1 for contact volume from left to right
	//VolContact2 for contact volume from right to left
	float VolContact1;	
	float VolContact2;	

	short lCoreIndex; //the left core index of this link
	short lCoreOffset;//the left core offset of  this link
	short rCoreIndex; //the right core index of this link
	short rCoreOffset;//the right core offset of this link

	short lSeqIndex; //the left pos in the template sequence
	short rSeqIndex; //the right pos in the template sequence

} LINK;



typedef struct {
	int cut;
	int lCore;
	int rCore;
}CUT;

	
typedef struct {
	
	short loopSize; //the size of the loop to the left of the core
	short leftSeqPos; //the left pos in the template (inclusive)
	short rightSeqPos; //the right pos in the template (inclusive)
	short coreType;
	short len;

	int localLinkNum;
	LINK localLinks[MaxLocalLinkSize];

	int leftLinkNum;
	LINK leftLinks[MaxLeftLinkSize];

	int rightLinkNum;
	LINK rightLinks[MaxRightLinkSize];

} CORE;

typedef struct {
	int linkNum;
	int lCore;
	int rCore;
} COREINTERACTION;

class TEMPLATE {

public:
	TemplateSingleton templateSeq[MaxTemplateSize];
	char seq[MaxTemplateSize];

	CORE*	cores;
	int coreSize;

	int 	linkGraph[MaxCoreSize][MaxCoreSize];
	float   scoreGraph[MaxCoreSize][MaxCoreSize];

	int templateSize;
	char* templateName;

	Config* config;

	bool gapEdgeAdded;
	bool flag_pruneEdge;

	//volume contact matrix
	float* VolContact_array;
	float* VolContact[MaxTemplateSize];

	//position specific probability
	//short* PSP_array;
	//short* PSP[MaxTemplateSize];


	//position specific score matrix
	float* PSM_array;
	float* PSM[MaxTemplateSize];

/*
	//backbone hydrogen restraints
	bool HBondAvailable;
	int* HBonds[MaxTemplateSize];
*/

public:

	TEMPLATE(char* tName);
	virtual ~TEMPLATE();

/*
	bool IsHBondAvailable() { return HBondAvailable;};
	int Bond(int pos1, int pos2){
		return HBonds[pos1][pos2];
	};
*/

	//return true if the position tPos is within a core, false otherwise
	bool WithinCore(int tPos); 

	void calcNativePairScore(CParam* p);
	void calcMinCut();

	//short* getPSP(int tPos);
	float* getPSM(int tPos);

	char* getName();
	char getChainChar();
	CORE* getCore(int i);
	CORE* getCores();
	int getLinkGraph(int i,int j);
	int getCutLinkNum(int i,int j);
	double getScoreGraph(int i,int j);
	int getTemplateSize();
	int getCoreSize();
	TemplateSingleton* operator[] (int i);

	//getContent will load all information related to a template
	//it will also extract core, calc links and contacts
	void getContent();


	//Load will load all information of a template, but won't extract core, calc links and contacts
	void Load();

	char getAA(int i){ return templateSeq[i].cResidue;};

	//get the line record in the template file for residue res_index
	char* GetBuf(int res_index){ return templateSeq[res_index].buf;};
		
	
	void addGapEdge();	
	void deleteGapEdge();	
	void pruneEdge();
//	void pruneEdgeByPROSPECT();//prune edges according to the criteria of PROSPECT

	void setConfig(Config* conf);

	void printSeq();
	void printCore();	
	void printLink();
//	void outputLinkMap(int linkMap[MaxCoreSize][MaxCoreSize]);
	void outputLinkGraph();
	void print(FILE* fp);

	void statSolvent();
	void statLinks(); //just for statistic
	void calcLinks();
	void calcContacts();
	virtual void extractCore();// extract core struct from the template seqa
	void estimateMinLoopSize();


	int CoreIndexByResIndex(int resIndex);	//return the core index of this residue
	int GetCoreLen(int coreIndex);
	int GetCoreHeadPosition(int coreIndex);
	char* GetSequence() { return (char*) seq;};
	char GetStructTypeChar(int i);
	int GetStructType(int i);

	int getEdge(int i,int j);

	//void ReadProspectTree();

//	void findPivot();
	//float DistOf2AAs(int i, int j);
	float DistOf2AAs(int tPos1, int tPos2, int type=CB2CB);
	float VolumeContact(int i, int j);

	int getContactNum() { return contactNum;};
	CONTACT* getContactList() { return contactList;};

	void SetResidueFlag(int pos, short flag);
	short GetResidueFlag(int pos);

private:

	void addOneResidue(int i, StringTokenizer* s,char* buf);
	void setTemplate(); //preprocess


	void calcInternalLinks();
	void calcExternalLinks();

	int LoadPSMInfo(char* file,float* array);

	void LoadVolContact(char* file);

	//calculate the distance between two amino acids



	
private:

	CONTACT* contactList;
	int contactNum;

/*
	PTREENODE prospect_tree[MaxCoreSize];
	int prospect_treeSize;
	bool isProspectTreeAvailable;

*/


public:
	int ResidueIndexByResID(char* resID);
	int ResidueIndexByResID(int resID, char extraChar);


};

#define INVALIDCOOR(x) ((x)>=998)

#endif




