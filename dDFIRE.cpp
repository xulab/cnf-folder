#include "strtokenizer.h"
#include "dDFIRE.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static float eag[mpolar][matype][mbin][mabin2];
dDFIRE::dDFIRE()
{
	bsym=true;
	bag=true;
	datadir = "./config/";
	ert=0.01, epen1=10.*ert, epen2=2.*ert;
	p_coeff=0.5;
	q_coeff=0.5;
	pq_coeff=0.5;
}

void dDFIRE::Init(bool bsym, bool bag)
{
	this->bsym = bsym;
	this->bag = bag;

	rdpolar(); 
	initEobs();
	ReadConfigFile(datadir+"dDFIRE.cfg");
}

double dDFIRE::CalcEnergy(ZhouMolecule *mol, int start)
{
	double ener[20];
	mol->initneib();
	mol->score(ener, start);
	delete mol;
	return ener[0]+ener[1]*p_coeff+ener[2]*q_coeff+ener[3]*pq_coeff;
}

double dDFIRE::CalcEnergy(string pdbfile)
{
	double ener[20];
	ZhouMolecule *mol = new ZhouMolecule(pdbfile.c_str());
	mol->initneib();
	//mol->score(ener, bag);
	mol->score(ener, 0);
	delete mol;
	return ener[0];
}

int dDFIRE::ReadConfigFile(string CfgName)
{
	ifstream CfgFile;
	CfgFile.open(CfgName.c_str(), ios::in | ios::binary);
	string line, par, val;
	bool recog_line;

	while (true)
	{
		getline(CfgFile, line); 

		if ((line != "") && (line[0] != '#'))
		{
			strtokenizer strtokens(line,"=");
			int num=strtokens.count_tokens();
			if (num<2) continue;
			par = strtokens.token(0);
			val = strtokens.token(1);
			//cerr << par << "=" << val << endl;

			if (par == "P_COEFFICIENT") { p_coeff = atof(val.c_str());}
			if (par == "Q_COEFFICIENT") { q_coeff = atof(val.c_str());}
			if (par == "PQ_COEFFICIENT") { pq_coeff = atof(val.c_str());}
		}

		if (CfgFile.eof()) break;
	}
	CfgFile.close();
	return 0;
}

void dDFIRE::initEobs()
{
	double Nobs[matype][matype][mbin], Nobs0[matype][mbin], dat[matype];
	string fn = datadir + "dfire.1";
	FILE *fp=openfile(fn, "r");
	char str[6001], *strs, *stre;
	int i, j, k;
	cerr<<fn<<endl;
	for(i=0; i<mbin; i++){
		fgets(str,120,fp);
		if(strchr(str, ':') == NULL){fprintf(stderr, "Error, %s\n", str); exit(1);}
		for(j=0; j<nkind; j++){
			fgets(str,6000,fp); 
			int it = str2dat(str, dat);
			if(it != nkind) 
				cerr<<"Nobs:"<<str<<endl;
			for(k=0; k<nkind; k++) 
				Nobs[j][k][i] = dat[k];
		}
	}
	fgets(str,120,fp);
	for(i=0; i<mbin; i++){
		fgets(str,6000,fp); int it = str2dat(str, dat);
		if(it != nkind) cerr<<"Nobs0:"<<str<<endl;
		for(k=0; k<nkind; k++) Nobs0[k][i] = dat[k];
	}
	// symtry
	if(bsym){
		for(i=0; i<mbin; i++){
			for(j=0; j<nkind; j++)
			for(k=j+1; k<nkind; k++) Nobs[j][k][i] = Nobs[k][j][i] = Nobs[j][k][i] + Nobs[k][j][i];
		}
   	}
	for(j=0; j<nkind; j++)
		for(k=0; k<nkind; k++){
			for(i=0; i<nbin; i++){
				double e, r = bin2r(i), rc=bin2r(nbin-1);
				if( Nobs[k][j][i]==0 ) e = epen1;
				else {
					double ALPHA=1.61;
					e = -ert*log(Nobs[k][j][i] / (pow((r/rc),ALPHA) * Nobs[k][j][nbin-1]));
					e = min(e, epen1);
				}
				edfire[k][j][i] = e;
			}
		}
	if(! bag) return;
// edfire_av[mpolar][matype][mbin][mabin]
	fgets(str, 120, fp);
	initEag(fp, mabin, nbin, nkind, npolar, "av");
	for(int j=0; j<npolar; j++) for(int k=0; k<nkind; k++)
		for(int i=0; i<nbin; i++) for(int b1=0; b1<mabin; b1++)
		{ edfire_av[j][k][i][b1] = eag[j][k][i][b1]; }
// edfire_vv[mpolar][mpolar][mbin][mabin],
	fgets(str, 120, fp);
	initEag(fp, mabin, nbin, npolar, npolar, "vv");
	for(int j=0; j<npolar; j++) for(int k=0; k<npolar; k++)
		for(int i=0; i<nbin; i++) for(int b1=0; b1<mabin; b1++)
		{ 
			edfire_vv[j][k][i][b1] = eag[j][k][i][b1]; 
			//cerr << edfire_vv[j][k][i][b1] << " ";
		}
	fclose(fp);
	return;
}

void dDFIRE::initEag(FILE *fp, int nab, int nb, int nk, int np, string flag)
{
	char str[20001];
	bool bhf_mat;
	bhf_mat = (flag=="pp1" || flag=="vv");
	for(int i=0; i<mbin; i++){
		fgets(str, 120, fp);
		for(int j=0; j<np; j++){
			fgets(str, 20000, fp); int dat[nk][nab];
			int nt = str2dat(str, &dat[0][0]);
			if(nt != nk*nab) 
			{
				//cerr<<flag<<" "<<nt<<" "<<nk<<' '<<nab<<str; 
				exit(1);
			}
			for(int k=0; k<nk; k++)
			for(int b1=0; b1<nab; b1++){
				eag[j][k][i][b1] = dat[k][b1];
			}
		}
	}

	float Emean[nab];
	bzero(Emean, sizeof(Emean));
	for(int i=0; i<nb; i++)
	for(int j=0; j<np; j++)
	for(int k=0; k<nk; k++){
		if(bhf_mat && k>j) continue;
		if(i == nb-1) {
			for(int b1=0; b1<nab; b1++) Emean[b1] += eag[j][k][i][b1];
		}
		initEag1(nab, eag[j][k][i]);
		if(bhf_mat){
			for(int b1=0; b1<nab; b1++) eag[k][j][i][b1] = eag[j][k][i][b1];
		}
	}
	initEag1(nab, Emean);
	for(int i=0; i<nb; i++)
	for(int j=0; j<np; j++)
	for(int k=0; k<nk; k++)
	for(int b1=0; b1<nab; b1++){
		eag[j][k][i][b1] = min(epen2, eag[j][k][i][b1]+0.);
	}
}

void dDFIRE::initEag1(int ma, float *edim)
{
	double ds = sum(ma, edim) / float(ma);
	for(int b1=0; b1<ma; b1++){
		double dt = edim[b1], e = epen1;
		if(dt > 0) {
			e = -ert*log(dt / ds);
			e = min(e, epen1);
		}
		edim[b1] = e;
	}
}
double dDFIRE::bin2r(int i){
	return (i+0.5) * 0.5;
}
void dDFIRE::rdpolar()
{
	string fname = datadir + "amino.dat";
	cerr << "Init dDFIRE: reading " << fname << endl;
	FILE *fp=openfile(fname.c_str(), "r");
	char str[121], rn[4], an[4];
	initPRtypes0(0);
	fgets(str, 120, fp);
	nkind = npolar = 0;
	while(fgets(str, 120, fp) != NULL){
		if(strstr(str, "END") == str) break;
		if(strstr(str, "ATOM ") != str) continue;
		int it, ip;
		sscanf(str+4, "%s%s%d%d", rn, an, &it, &ip);
		int ia = defineAtype(rn, an);
		if(ia < 0) continue;
		if(it == 0){
			nkind ++;
			if(ip >= 1) npolar++;
		}
		iskind[ia] = nkind - 1;
		if(ip >= 1) ispolar[ia] = npolar - 1;
		else ispolar[ia] = -1;
	}
// planar
	int np = 0;
	while(fgets(str, 120, fp) != NULL){
		if(strstr(str, "END") == str) break;
		if(strstr(str, "PLN ") != str) continue;
		sscanf(str+4, "%s%s%s%s", aa_plane[np][0], aa_plane[np][1], aa_plane[np][2], aa_plane[np][3]);
		np++;
	}
	nplane = np;
	fprintf(stderr, "nkind: %d; npolar: %d; nplane: %d\n", nkind, npolar, nplane);
}

