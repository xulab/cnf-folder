/*******************************************************************************

                             RamaTypology.cpp
                             ----------------

  ****************************************************************************
  *                                                                          *
  *   This library is free software; you can redistribute it and/or          *
  *   modify it under the terms of the GNU Lesser General Public             *
  *   License as published by the Free Software Foundation; either           *
  *   version 2.1 of the License, or (at your option) any later version.     *
  *                                                                          *
  *   This library is distributed in the hope that it will be useful,        *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *
  *   Lesser General Public License for more details.                        *
  *                                                                          *
  *   You should have received a copy of the GNU Lesser General Public       *
  *   License along with this library; if not, write to the Free Software    *
  *   Foundation, Inc.,                                                      *
  *   59 Temple Place, Suite 330, Boston, MA  02111-1307  USA                *
  *                                                                          *
  ****************************************************************************

  Copyright (C) 2005 The University of Chicago

  Authors:
  Andres Colubri

  Description:
  Implements the structures and classes to handle Ramachandran types.

*******************************************************************************/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "RamaBasin.h"
#include "strtokenizer.h"

/************************ TPhiPsiBox implementation ***************************/

int PhiPsiBox::LoadFromStream(ifstream &stream)
{
    string line, par, val;
    while (stream.is_open())
    {
        getline(stream, line);

        if (line != "")
            if (line == "End section") break;
            else
            {
		strtokenizer strtokens(line,"=");
                int num=strtokens.count_tokens();
                if (num<2) continue;
                par = strtokens.token(0);
                val = strtokens.token(1);

                if (par == "Length[phi]") Length[PHI] = atof(val.c_str());
                if (par == "Length[psi]") Length[PSI] = atof(val.c_str());

                if (par == "Center[phi]") Corner[PHI] = atof(val.c_str()) - Length[PHI] / 2;
                if (par == "Center[psi]") Corner[PSI] = atof(val.c_str()) - Length[PSI] / 2;
            }

        if (stream.eof()) break;
    }
    //cerr << "Center("<<Corner[PHI]<<", "<<Corner[PSI]<<"); Length("<<Length[PHI]<<", "<<Length[PSI]<<")"<<endl;
    return 0;
}

/************************ TRamaBasin implementation ***************************/

int RamaBasin::LoadFromStream(ifstream &stream)
{
    string line, par, val, idx;
    while (stream.is_open())
    {
        getline(stream, line); 
        if (line != "")
            if (line == "End section") break;
            else
            {
                if (line == "Start bounding-box section") BoundBox.LoadFromStream(stream);

		strtokenizer strtokens(line,"=");
                int num=strtokens.count_tokens();
                if (num<2) continue;
                par = strtokens.token(0);
                val = strtokens.token(1);

                if (par == "Name") Name = val;
                if (par == "Code") Code = atoi(val.c_str());
            }

        if (stream.eof()) break;
    }
    //cerr << "Name="<< Name << "; Code=" << Code << endl;
    return 0;
}

/********************** TRamaTypology implementation **************************/

int RamaTypology::LoadFromStream(ifstream &stream)
{
    string line; 
    while (stream.is_open())
    {
        getline(stream, line); 
        if (line == "Start basin section")
        {
                // Adding new basin.
                BasinsList.push_back(RamaBasin());
                BasinsList[BasinsList.size() - 1].LoadFromStream(stream);
        }
        if (stream.eof()) break;
    }
    return 0;
}

int RamaTypology::PhiPsiToRBasin(double phi, double psi)
{
    int rb;
    for (rb = 0; rb < int(BasinsList.size()); rb++)
        if (BasinsList[rb].ContainsPhiPsi(phi, psi)) return BasinsList[rb].Code;//return rb;
    return 3; // return -1; //***Note: -1 means not in the helix or sheet basins, which is acturally 3
}

int RamaTypology::LoadFromFile(const string &FileName)
{
    ifstream IFile;
    IFile.open(FileName.c_str(), ios::in);

    cerr <<"RamaTypology::LoadFromFile("<< FileName <<")"<<endl;

    string line;
    while (true)
    {
        getline(IFile, line); 
	//cerr << line << endl;
        if (line == "Start typology section")
            LoadFromStream(IFile);

        if (IFile.eof()) break;
    }

    IFile.close();

    return 0;
}
