#ifndef _NEW_CONFIG_H
#define _NEW_CONFIG_H

#include "Constants.h"
#include <map>

#define DEBUG_LEVEL0	0	//no debug information
#define DEBUG_LEVEL1	1
#define DEBUG_LEVEL2	2
#define DEBUG_LEVEL3	3	//the most detailed debug information

class Config {
	private:
		string source; // the source file of this configuration

		//store all the pairs of (key, value)
		map<string, string> configs; 
		map<string, string> default_configs; 

		//for MPI
		int my_id;
		int num_procs;

	public:

		Config();
		Config(char* fileName);
		Config(string& fileName);
		~Config();

		void configure(); //default configuration

		//get the value of a given key, these functions return 0 if no value found, otherwise return 1
		// value stores the output
		int getValue(const string& key, string* value);
		int getValue(const string& key, double* value);
		int getValue(const string& key, int* value);
		int getValue(const string& key, bool* value);

		void printConfig(); 

	private:
		void Initialize();
		void PostProcess();
		void readConfiguration();
		int parseLine(char* buf, string& paramName, string& paramValue);
		void ProcessOneLine(char* line);

};


#endif

