/**
 Protein Folding Library.
 * Copyright (C) 2009 Andres Colubri.
 * Contact: andres.colubri 'AT' gmail.com
 *
 * This library was written at the Institute of Biophysical Dynamics at the University of Chicago.
 * Gordon Center for Integrated Science, W101. 929 East 57th Street, Chicago, IL 60637, USA.
 * Homepage: http://ibd.uchicago.edu/
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation with or without modifications and for any purpose and
 * without fee is hereby granted, provided that any copyright notices
 * appear in all copies and that both those copyright notices and this
 * permission notice appear in supporting documentation, and that the
 * names of the contributors or copyright holders not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific prior permission.
 * 
 * THE CONTRIBUTORS AND COPYRIGHT HOLDERS OF THIS SOFTWARE DISCLAIM ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE
 * CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT
 * OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOFTWARE.
 * 
 */

/**
 * DOPE-PW statisical potential function.
 * Authors: Joe DeBartolo, Glen Hocky (protlib2 port)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <string>
#include <iostream>

#include "dopepw.h"
#include "strtokenizer.h"

using namespace std;
using namespace BALL;

IndexValue numbins[NUMTERMS]={MAXBINNUM+1,MAXBINNUM+1,MAXBINNUM+1,MAXBINNUM+1,MAXBINNUM+1,MAXBINNUM+1,MAXBINNUM+1,MAXBINNUM+1};
FloatValue distancecutoffs[NUMTERMS]={15.0,15.0,15.0,30.0,30.0,30.0,30.0,30.0};
FloatValue binsizes[NUMTERMS]; 
FloatValue coeffs[NUMTERMS]={1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0};

static const char * aacodetoonelet[21]={" ","A","R","N","D","C","Q","E","G","H","I","L","K","M","F","P","S","T","W","Y","V"};
static const char * dopecodetolet[8]={"N","HN","CA","CB","HA2","HA1","C","O"};
//static const char * atcodetolet[10]={" ","N","CA","C","O","HN","HA1","CAP","CB","HA2"};
static const IndexValue convtodope[10]={-1,0,2,6,7,1,5,-1,3,4};

#define PI 3.141592653589793

dopepw_ac::dopepw_ac()
{
	pwvec=NULL;
	papvec=NULL;
	eevec=NULL;
	palign=NULL;
	apalign=NULL;
	hohbondvec=NULL;
	ohhbondvec=NULL;
	cocoalign=NULL;
	nhnhalign=NULL;
	nhcoalign=NULL;
	conhalign=NULL;
	niprop=NULL;

	pairenergies=NULL;
	paircoeffs=NULL;
	pairterms=NULL;
	pairrows=NULL;
	updatepairs=NULL;
	activepairs=NULL;

	secstrvec=NULL;
	continuityvec=NULL;

	bFirstTimeSetup=true;
	
	_num_HB=0;
}

int dopepw_ac::four_atom_angle(FloatValue *angle,ATOM& head1,ATOM& tail1,ATOM& head2,ATOM& tail2)
{
	Vector3 v,w;
	v=getPosition(head1)-getPosition(tail1);
	w=getPosition(head2)-getPosition(tail2);
	*angle=v.getAngle(w)*180/PI;
	return NO_ERROR;
}

int dopepw_ac::setup_pw_pair_vecs(void)
{
	int nres2=nres*nres;
	pwvec= new IndexValue[nres2]; memset(pwvec, 0, sizeof(IndexValue)*nres2);
	papvec= new IndexValue[nres2]; memset(papvec, 0, sizeof(IndexValue)*nres2);
	eevec= new IndexValue[nres2]; memset(eevec, 0, sizeof(IndexValue)*nres2);
	palign= new IndexValue[nres2]; memset(palign, 0, sizeof(IndexValue)*nres2);
	apalign= new IndexValue[nres2]; memset(apalign, 0, sizeof(IndexValue)*nres2);
	hohbondvec= new IndexValue[nres2]; memset(hohbondvec, 0, sizeof(IndexValue)*nres2);
	ohhbondvec= new IndexValue[nres2]; memset(ohhbondvec, 0, sizeof(IndexValue)*nres2);
	cocoalign= new IndexValue[nres2]; memset(cocoalign, 0, sizeof(IndexValue)*nres2);
	nhnhalign= new IndexValue[nres2]; memset(nhnhalign, 0, sizeof(IndexValue)*nres2);
	nhcoalign= new IndexValue[nres2]; memset(nhcoalign, 0, sizeof(IndexValue)*nres2);
	conhalign= new IndexValue[nres2]; memset(conhalign, 0, sizeof(IndexValue)*nres2);

	niprop= new IndexValue[natoms]; memset(niprop, 0, sizeof(IndexValue)*natoms);

	pairenergies= new FloatValue[natoms2];
	paircoeffs= new FloatValue[natoms2];
	pairrows= new IndexValue[natoms2];
	pairterms= new IndexValue[natoms2];

	updatepairs= new IndexValue[natoms2];
	activepairs= new IndexValue[natoms2];

	secstrvec= new IndexValue[nres];
	continuityvec= new IndexValue[nres];

	return NO_ERROR;
}

int dopepw_ac::delete_pw_pair_vecs(void)
{
	delete pwvec;
	delete papvec;
	delete eevec;
	delete palign;
	delete apalign;
	delete hohbondvec;
	delete ohhbondvec;
	delete cocoalign;
	delete nhnhalign;
	delete nhcoalign;
	delete conhalign;
	delete niprop;

	return NO_ERROR;
}

int dopepw_ac::set_all_pair_vecs(IndexValue position,IntValue integer)
{
	pwvec[position]=integer;
	papvec[position]=integer;
	eevec[position]=integer;
	palign[position]=integer;
	apalign[position]=integer;
	hohbondvec[position]=integer;
	ohhbondvec[position]=integer;
	cocoalign[position]=integer;

	nhnhalign[position]=integer;
	nhcoalign[position]=integer;
	conhalign[position]=integer;

	return NO_ERROR;
}

int dopepw_ac::init_dopepw(const char* cfg)
{
	FloatValue cutoff;
	debug=debug2=0;

	cerr << "        Initializing DOPE-PW..." << endl;
	cerr << "              Loading Parameter file..." << cfg << endl;

	string linehead, line, filename, val, filenames[8];
	ifstream CfgFile;
	CfgFile.open(cfg, ios::in | ios::binary);
	while (CfgFile.is_open() && !CfgFile.eof())
	{
		getline(CfgFile, line); trim2(line);
		if ((line != "") && (line[0] != '#'))
		{
			strtokenizer strtokens(line,"=");
			int num=strtokens.count_tokens();
			if (num<2) continue;
			linehead = strtokens.token(0);
			val = strtokens.token(1);
			trim2(linehead);
			trim2(val);

			char firstchar=linehead[0];
			if (firstchar=='#' || linehead=="")
				continue;
			else if (linehead=="PW_EE_cont_Energy parameter file")
				filenames[EECONT] = val;
			else if (linehead=="PW_HH_cont_Energy parameter file")
				filenames[HHCONT] = val;
			else if (linehead=="PW_CC_cont_Energy parameter file")
				filenames[CCCONT] = val;
			else if (linehead=="PW_0EEap_ncont_Energy parameter file")
				filenames[EEap] = val;
			else if (linehead=="PW_0EEp_ncont_Energy parameter file")
				filenames[EEp] = val;
			else if (linehead=="PW_0A_ncont_Energy parameter file")
				filenames[PW0Ancont] = val;
			else if (linehead=="PW_1A_ncont_Energy parameter file")
				filenames[PW1Ancont] = val;
			else if (linehead=="PW_2A_ncont_Energy parameter file")
				filenames[PW2Ancont] = val;
			else if (linehead=="Distance cutoff") {
				cutoff = atoi(val.c_str());
				distancecutoffs[HHCONT]=distancecutoffs[EECONT]=distancecutoffs[CCCONT]=cutoff;
				distancecutoffs[EEap]=distancecutoffs[EEp]=distancecutoffs[PW0Ancont]=cutoff;
			}
			else if (linehead=="Distance cutoff 1A2A") {
				cutoff = atoi(val.c_str());
				distancecutoffs[PW1Ancont]=distancecutoffs[PW2Ancont]=cutoff;
			}
			else if (linehead=="PW_EE_cont_Energy coefficient") coeffs[EECONT] = atof(val.c_str());
			else if (linehead=="PW_HH_cont_Energy coefficient") coeffs[HHCONT] = atof(val.c_str());
			else if (linehead=="PW_CC_cont_Energy coefficient") coeffs[CCCONT] = atof(val.c_str());
			else if (linehead=="PW_0EEap_ncont_Energy coefficient") coeffs[EEap] = atof(val.c_str());
			else if (linehead=="PW_0EEp_ncont_Energy coefficient") coeffs[EEp] = atof(val.c_str());
			else if (linehead=="PW_0A_ncont_Energy coefficient") coeffs[PW0Ancont] = atof(val.c_str());
			else if (linehead=="PW_1A_ncont_Energy coefficient") coeffs[PW1Ancont] = atof(val.c_str());
			else if (linehead=="PW_2A_ncont_Energy coefficient") coeffs[PW2Ancont] = atof(val.c_str());
			else if (linehead=="DEBUG") debug = atoi(val.c_str());
			else if (linehead=="DEBUG2") debug2 = atoi(val.c_str());
			else {
				//cerr << "UNKNOWN_PARAMETER_ERROR (init_debug):" << linehead << endl;
			}
		}
	}
	CfgFile.close();

	dopearraysize=convert_to_dopepw_pos(NUMTERMS,MAXAACODE,MAXATCODE,MAXAACODE,MAXATCODE,MAXBINNUM);
	dopeenergies = new FloatValue[dopearraysize];

	//check to make sure dope is loaded correctly
	IndexValue i,j,k,l,m;
	for (i=0;i<NUMTERMS;i++) {
		binsizes[i]=distancecutoffs[i]/numbins[i];
		filename = filenames[i];
		cerr << "  init par file: " << filename << endl;
		ifstream par_file;
		par_file.open(filename.c_str(), ios::in);
		while (!par_file.eof())
		{
			IndexValue count;
			IntValue indices[4];
			IntValue currentindex, index;
			FloatValue currentenergy;
			for (count=0; count<4; count++) {
				par_file >> index;
				indices[count] = index;
			}

			for (count = 4; count < 34; count++) {
				par_file >> currentenergy;
				currentindex=convert_to_dopepw_row(i,indices[0],indices[1],indices[2],indices[3])+ (count-4);           
				dopeenergies[currentindex]=currentenergy;
			}
		}
		par_file.close();
	}

	if (debug2){
		for (i=0;i<MAXAACODE;i++)
			for(j=0;j<MAXATCODE;j++)
				for(k=0;k<MAXAACODE;k++)
					for(l=0;l<MAXATCODE;l++){
						//printf("%i %i %i %i ",i,j,k,l);
						printf("%s(%i) %s(%i) %s(%i) %s(%i)",aacodetoonelet[i+1],i,dopecodetolet[j],j,aacodetoonelet[k+1],k,dopecodetolet[l],l);
						for(m=0;m<=MAXBINNUM;m++){
							printf(" %4.2f ",dopeenergies[convert_to_dopepw_row(EECONT,i,j,k,l)+m]);
						}
						printf("\n");
					}
	}

	cerr << "   dopepw_ac Done." << endl;
	return NO_ERROR;
}

int dopepw_ac::finish_dopepw(void)
{
	delete pairenergies;
	delete paircoeffs;
	delete pairterms;
	delete pairrows;
	delete updatepairs;
	delete activepairs;

	delete secstrvec;
	delete continuityvec;

	delete dopeenergies;

	delete_pw_pair_vecs();

	return NO_ERROR;
}

int dopepw_ac::set_system_for_dopepw(BoolValue *_distmask, vector<RESIDUE> *_residues, string _sequence)
{
	//distmask = _distmask;
	residues = _residues;
	sequence = _sequence;
	nres = _residues->size();
	natoms = 0;
	atoms.clear();
	for (int i=0; i<nres; i++){
		RESIDUE& r= residues->at(i);
		for (int j=0; j<(int)r.arrAtoms.size(); j++){
			ATOM& atom = r.arrAtoms[j];
			if (atom.name=="SG")
				continue;
			atom.idx=natoms;
			atoms.push_back(&atom);
			natoms ++;
		}
	}
	distances.resize(natoms,natoms);
	for (int i=0; i<natoms-1; i++)
		for (int j=i+1; j<natoms; j++)
			distances(i, j)=distances(j, i)=BBQ::dist(*atoms[i], *atoms[j]);//getPosition(*atoms[i]).getSquareDistance(getPosition(*(atoms[j])));

	return NO_ERROR;
}

int dopepw_ac::pre_dopepw_calc(void)
{
	double dif;
	struct timeval ts, te;
	//gettimeofday(&ts, 0);

	natoms2=natoms*natoms;
	if (pairenergies==NULL)
		setup_pw_pair_vecs();

	//if (bFirstTimeSetup)
	{
		bFirstTimeSetup=false;
		for (int i=0;i<natoms;i++)
			niprop[i]=AtomNameToCode(atoms[i]->name);
	}

	//set dope codes,set active pairs (i.e. pairs to add up later)
	nactivepairs=0;
	IndexValue resi,typei,resj,typej;
	for (int i=0;i<natoms-1;i++){
		//string atomnamei=atoms[i]->name;
		typei=niprop[i];
		resi=atoms[i]->res_idx; // tarts from 0
		for (int j=i+1;j<natoms;j++){
			//string atomnamej=atoms[j]->name;
			typej=niprop[j];
			resj=atoms[j]->res_idx; // starts from 0
			//BoolValue hydrogen = ( atomnamei== "HN" || atomnamei== "HA1" || atomnamej== "HN" ||atomnamej== "HA1");
			BoolValue hydrogen = ( typei== 1 || typej== 1);
			if (resj>resi+MINCD && !hydrogen) { //change this to reflect ii+n only
				activepairs[2*nactivepairs]=i;
				activepairs[2*nactivepairs+1]=j;
				nactivepairs++;
			}
			pairenergies[natoms*i+j]=0;
		}
	}

	//MUST set prop wash, find beta strands, set continuity
	FloatValue dist,sheetangle;
	IndexValue i,j,pairidx,res0,res1;

	//setting propwash
	//cerr << "pre_dopepw_calc:" << natoms << endl;
	nupdatepairs=0;
	for (i=0;i<natoms-1;i++){
		for (j=i+1;j<natoms;j++) {
			IndexValue res0=atoms[i]->res_idx;
			IndexValue res1=atoms[j]->res_idx;
			IndexValue cd=res1-res0;
			//if (distmask[distance_index(i,j,natoms)] && cd>MINCD){
			if (cd>MINCD){
				updatepairs[2*nupdatepairs]=i;
				updatepairs[2*nupdatepairs+1]=j;
				nupdatepairs++;
			}
		}
	}

	//gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
	//cerr << "	pre_dopepw_calc#1: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000 << " miliseconds." << endl;
	//gettimeofday(&ts, 0);

	ATOM nres0,cares0,cres0,ores0,nres1,cares1,cres1,ores1;
	ATOM naa0p1res0,caa0m1res0,cbres0,hnres0;
	ATOM naa1p1res1,caa1m1res1,cbres1,hnres1;
	FloatValue CO0_C01_angle,CO1_C10_angle;
	FloatValue COCOangle,CO0_CN01_angle,NH1_NC10_angle,NH0_NC01_angle, CO1_CN10_angle, HN0_CO1_angle,CO0_HN1_angle,NH_NH_angle;
	FloatValue CN0_N1_peptide_angle,NC1_C0_peptide_angle,NC0_C1_peptide_angle,CN1_N0_peptide_angle;
	for (res0=0;res0<nres;res0++) {
		RESIDUE residue0= residues->at(res0);
		BoolValue peptidecn0=true,peptidenc0=true,hascb0=true,hashn0=true;
		BoolValue peptidecn1=true,peptidenc1=true,hascb1=true,hashn1=true;

		nres0=residue0.getAtom("N");
		cares0=residue0.getAtom("CA");
		cres0=residue0.getAtom("C");
		ores0=residue0.getAtom("O");
		if(res0<nres-1) {
			RESIDUE res= residues->at(res0+1);
			naa0p1res0=res.getAtom("N");
			peptidecn0=(naa0p1res0.res_idx>=0);
		}
		if(res0>1) {
			RESIDUE res= residues->at(res0-1);
			caa0m1res0=res.getAtom("C");
			peptidenc0=(caa0m1res0.res_idx>=0);
		}

		cbres0=residue0.getAtom("CB");
		hnres0=residue0.getAtom("HN");
		hascb0=(cbres0.res_idx>=0);
		hashn0=(hnres0.res_idx>=0);
		for (res1=res0+MINCD;res1<nres;res1++) {
			RESIDUE residue1= residues->at(res1);
			pairidx=nres*res0+res1;
			//set_all_pair_vecs(pairidx,0);

			nres1=residue1.getAtom("N");
			cares1=residue1.getAtom("CA");
			cres1=residue1.getAtom("C");
			ores1=residue1.getAtom("O");

			cbres1=residue1.getAtom("CB");
			hnres1=residue1.getAtom("HN");
			hascb1=(cbres1.res_idx>=0);
			hashn1=(hnres1.res_idx>=0);
			if(hashn1) {
				dist=distances(ores0.idx,hnres1.idx); //cerr << "dist: " << dist << "; " << ores0.idx<<","<<hnres1.idx <<endl;
				if(dist<2.6 && dist > 1.7) { ohhbondvec[pairidx]=1; _num_HB ++; }
			}
			if(hashn0) {
				dist=distances(hnres0.idx,ores1.idx);
				if(dist<2.6 && dist > 1.7) { hohbondvec[pairidx]=1; _num_HB ++; }
			}
			//Determining if sheets are parallel or antiparallel
			four_atom_angle(&sheetangle,nres0,cres0,nres1,cres1);
			//printf("sheetangle: %i %i %f\n",res0,res1,sheetangle);
			if (sheetangle < 90.0) papvec[pairidx]=PARALLEL;
			else papvec[pairidx]=ANTIPARALLEL;

			four_atom_angle(&CO0_C01_angle,cres0,ores0,cres0,cres1);
			four_atom_angle(&CO1_C10_angle,cres1,ores1,cres1,cres0);
			FloatValue COCOpropwash = sqrt((CO0_C01_angle-90.0)*(CO0_C01_angle-90.0) + (CO1_C10_angle-90.0)*(CO1_C10_angle-90.0));
			four_atom_angle(&COCOangle,cres0,ores0,cres1,ores1);
			four_atom_angle(&CO0_CN01_angle,cres0,ores0,cres0,nres1);
			FloatValue OHpropwash = 0.0,HOpropwash = 0.0;
			if (hashn1) {
				four_atom_angle(&NH1_NC10_angle,nres1,hnres1,nres1,cres0);  //what if hnres1 not defined!!, added if though doesn't seem to be in original
				OHpropwash = sqrt ((CO0_CN01_angle-90.0)*(CO0_CN01_angle-90.0) + (NH1_NC10_angle-90.0)*(NH1_NC10_angle-90.0));
				four_atom_angle(&CO0_HN1_angle,cres0,ores0,nres1,hnres1); //BUG: this appears correct, joe's code has opposite direction for n and h 
			}
			if (hashn0) {
				four_atom_angle(&NH0_NC01_angle,nres0,hnres0,nres0,cres1); //what if hnres1 not defined!!, added if though doesn't seem to be in original
				four_atom_angle(&CO1_CN10_angle,cres1,ores1,cres1,nres0);
				HOpropwash = sqrt((NH0_NC01_angle-90.0)*(NH0_NC01_angle-90.0) + (CO1_CN10_angle-90.0)*(CO1_CN10_angle-90.0));
				//printf("%i %i NH0_NC01_angle:%f\n",res0,res1,NH0_NC01_angle);
				four_atom_angle(&HN0_CO1_angle,nres0,hnres0,cres1,ores1); //SAME BUG AS ABOVE
			}

			FloatValue NH0_NN01_angle,NH1_NN10_angle,NHNHpropwash = 0.0;
			if (hashn0 && hashn1) {
				four_atom_angle(&NH0_NN01_angle,nres0,hnres0,nres0,nres1);
				four_atom_angle(&NH1_NN10_angle,nres1,hnres1,nres1,nres0);
				NHNHpropwash=sqrt((NH0_NN01_angle-90.0)*(NH0_NN01_angle-90.0) + (NH1_NN10_angle-90.0)*(NH1_NN10_angle-90.0));
				four_atom_angle(&NH_NH_angle,nres0,hnres0,nres1,hnres1);
			}

			if(res1>1) {
				RESIDUE res= residues->at(res1-1);
				caa1m1res1=res.getAtom("C");
				peptidenc1=(caa1m1res1.res_idx>=0);
			}
			if(res1<nres-1) {
				RESIDUE res= residues->at(res1+1);
				naa1p1res1=res.getAtom("N");
				peptidecn1=(naa1p1res1.res_idx>=0);
			}
			
			if (peptidecn0 && peptidenc1) {
				four_atom_angle(&CN0_N1_peptide_angle,cres0,naa0p1res0,cres0,nres1);
				four_atom_angle(&NC1_C0_peptide_angle,nres1,caa1m1res1,nres1,cres0);
			}
			if (peptidenc0 && res0>1 && peptidecn1) {
				four_atom_angle(&NC0_C1_peptide_angle,nres0,caa0m1res0,nres0,cres1);
				four_atom_angle(&CN1_N0_peptide_angle,cres1,naa1p1res1,cres1,nres0);
			}
			if(COCOpropwash > 90.0 && COCOangle < 30.0) cocoalign[pairidx]=1;
			if(COCOpropwash > 90.0 && COCOangle < 30.0 && NHNHpropwash > 90.0 && NH_NH_angle < 30.0) palign[pairidx]=1;
			if(HOpropwash > 90.0 && CO0_HN1_angle >140.0 && OHpropwash > 90.0 && HN0_CO1_angle > 140.0 ) apalign[pairidx]=1;
			//printf("HOpropwash: %i %i %f %f %f %f %i %i %i p%i\n",res0,res1,HOpropwash,CO0_HN1_angle,OHpropwash,HN0_CO1_angle,hashn0,hashn1,apalign[pairidx], pairidx);
			if(NHNHpropwash > 90.0 && NH_NH_angle < 30.0) nhnhalign[pairidx]=1;
			if (HOpropwash > 90.0 && HN0_CO1_angle > 140.0) nhcoalign[pairidx]=1;
			if (OHpropwash > 90.0 && CO0_HN1_angle > 140.0) conhalign[pairidx]=1;

			if(hascb0 && hascb1) {
				FloatValue propwash,propwash0,propwash1;
				four_atom_angle(&propwash0,cares0,cbres0,cares0,cares1);
				four_atom_angle(&propwash1,cares1,cbres1,cares1,cares0);
				propwash=sqrt(((propwash0-90.0)*(propwash0-90.0))+((propwash1-90.0)*(propwash1-90.0)));
				if ((propwash>0.0) && (propwash <=40.0)) pwvec[pairidx]=0;
				else if ((propwash > 40 ) && (propwash <=70)) pwvec[pairidx]=1;
				else if (propwash > 70) pwvec[pairidx]=2;
			}
			//printf("conhalign: %i %i %i\n",res0,res1,conhalign[pairidx]);
		}
	}

	//gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
	//cerr << "	pre_dopepw_calc#2: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000 << " miliseconds." << endl;
	//gettimeofday(&ts, 0);

	IndexValue helixlength=0,helixiter=0;
	for(i=0;i<nres;i++){
		secstrvec[i]=COIL;
		FloatValue phi = (*residues)[i].phi;
		FloatValue psi = (*residues)[i].psi;
		if ( (phi<-40.0 && phi > -90.0 && psi <-20.0 && psi > -60.0) || (i>4 && ohhbondvec[nres*(i-4)+i]==1) )helixlength++; //check this
		else {
			if (helixlength>4) for (helixiter=i-helixlength;helixiter<i;helixiter++) secstrvec[helixiter]=HELIX;
			helixlength=0;
		}
	}
	//must check if got to end of sequence and still helix
	if(helixlength>4) for(helixiter=nres-1-helixlength;helixiter<nres-1;helixiter++) secstrvec[helixiter]=HELIX; 

	//cout << "Secseq:" << endl;
	//for(i=0;i<nres;i++) printf("%i",secstrvec[i]);
	//printf("\n");

	//FindBetaStrands()  //MAY NEED TO ADD !ANALYSIS CASES LATER
	IndexValue base = 0;
	for (res0=1;res0<nres;res0++)  //BUG! To make below run smoothely, skipped first res, problems?
	{
		base+=nres;
		for(res1=res0+5;res1<nres;res1++){
			pairidx=base+res1; //pairidx=residue_index(res0,res1);
			IndexValue res0res1m1=pairidx-1;//residue_index(res0,(res1-1));
			IndexValue res0res1p1=pairidx+1;//residue_index(res0,(res1+1));
			IndexValue res0p1res1=pairidx+nres; //residue_index((res0+1),res1);
			IndexValue res0m1res1=pairidx-nres; //residue_index((res0-1),res1);
			IndexValue res0m1res1p1=res0m1res1+1; //residue_index((res0-1),(res1+1));
			IndexValue res0m1res1m1=res0m1res1-1; //residue_index((res0-1),(res1-1));
			IndexValue res0p1res1p1=res0p1res1+1; //residue_index((res0+1),(res1+1));
			IndexValue res0p1res1m1=res0p1res1-1; //residue_index((res0+1),(res1-1));
			//printf("%i %i papvec:%i\n",res0,res1,papvec[pairidx]);
	            
			if (papvec[pairidx]==PARALLEL) {  
				if (( (hohbondvec[res0res1m1]==1 && ohhbondvec[res0res1p1]==1) ||
						(hohbondvec[res0p1res1]==1 && ohhbondvec[res0m1res1]==1) ) &&
						palign[pairidx]==1 && cocoalign[res0m1res1m1]==1 && nhnhalign[res0p1res1p1]==1 )
				{
					eevec[pairidx]=eevec[res0m1res1m1]=eevec[res0m1res1]=eevec[res0res1m1]=eevec[res0p1res1]=eevec[res0res1p1]=eevec[res0p1res1p1]=PARALLEL;
					secstrvec[res0]=secstrvec[res1]=secstrvec[res0+1]=secstrvec[res1-1]=secstrvec[res1+1]=STRAND;
					if(res0>0) secstrvec[res0-1]=STRAND;
				}
			}
			if (papvec[pairidx]==ANTIPARALLEL){
				if ( (ohhbondvec[res0m1res1p1]==1 && hohbondvec[res0p1res1m1]==1) &&
						apalign[pairidx]==1 && conhalign[res0m1res1p1]==1 && nhcoalign[res0p1res1m1]==1 )
				{
					eevec[pairidx]=eevec[res0m1res1p1]=eevec[res0m1res1]=
							eevec[res0res1m1]=eevec[res0p1res1]=eevec[res0res1p1]= eevec[res0p1res1m1]=ANTIPARALLEL;
					secstrvec[res0]=secstrvec[res1]=secstrvec[res0+1]=secstrvec[res1-1]=secstrvec[res1+1]=STRAND;
					if(res0>0) secstrvec[res0-1]=STRAND;
				}
			}
		}
	}

	//SetContinuity
	IndexValue ss0,ss1,flag=0;
	ss0=secstrvec[0];
	continuityvec[0]=flag;
	for(res0=0;res0<nres;res0++){
		ss1=secstrvec[res0];
		if (ss1!=ss0) flag++;
		continuityvec[res0]=flag;
		ss0=ss1;
	}

	//gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
	//cerr << "	pre_dopepw_calc#3: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000 << " miliseconds." << endl;
	//gettimeofday(&ts, 0);

	//Make term assignments, set reset doperow

	//	ALA,"", CYS,ASP,GLU,PHE,GLY,HIS,ILE,"", LYS,LEU, MET,ASN,"", PRO,GLN,ARG,SER,THR,"", VAL,TRP,"", TYR
	int arrResidueIdx[25]={0, -1, 4, 3, 6, 13, 7, 8, 9, -1, 11, 10, 12, 2, -1, 14, 5, 1, 15, 16, -1, 19, 17, -1, 18};
	// ALA, ARG, ASN, ASP, CYS, GLN, GLU, GLY, HIS, ILE, LEU, LYS, MET, PHE, PRO, SER, THR, TRP, TYR, VAL, HET
	//  0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20

	if (debug) printf("Update pairs: %i\n",nupdatepairs);
	for (i=0;i<nupdatepairs;i++){
		IndexValue atom0=updatepairs[2*i];
		IndexValue res0=atoms[atom0]->res_idx;
		IndexValue res0type=arrResidueIdx[sequence[res0]-'A'];
		IndexValue dopetype0=niprop[atom0];

		IndexValue atom1=updatepairs[2*i+1];
		IndexValue res1=atoms[atom1]->res_idx;
		IndexValue dopetype1=niprop[atom1];
		IndexValue res1type=arrResidueIdx[sequence[res1]-'A'];
		IndexValue term;

		IndexValue pairidx=atom0*natoms+atom1;
		IndexValue respairidx=res0*nres+res1;

		IndexValue cd=res1-res0;
		if (cd<MINCD) continue;

		BoolValue continuous = ( continuityvec[res0]==continuityvec[res1] );

		if ( continuous ){
			if (secstrvec[res0]==HELIX)  term=HHCONT;
			else if (secstrvec[res0]==STRAND) term=EECONT;
			else term=CCCONT;
		} 
		//after here, no continuous terms
		else if ( eevec[respairidx] == PARALLEL ) term = EEp;
		else if ( eevec[respairidx] == ANTIPARALLEL ) term = EEap;
		else if ( pwvec[respairidx] == 0 ) term=PW0Ancont;
		else if ( pwvec[respairidx] == 1 ) term=PW1Ancont;
		else if ( pwvec[respairidx] == 2 ) term=PW2Ancont;
		else {
			printf("Danger, fall through case for atoms (%i,%i)\n",atom0,atom1);
			exit(0);
		}

		pairrows[pairidx]=convert_to_dopepw_row(term,res0type,dopetype0,res1type,dopetype1);
		pairterms[pairidx]=term;
		paircoeffs[pairidx]=coeffs[term];

		pairidx=natoms*atom0+atom1;
	}

	//gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
	//cerr << "	pre_dopepw_calc#4: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000 << " miliseconds." << endl;
	//gettimeofday(&ts, 0);

	return NO_ERROR;
}

void dopepw_ac::print_pair_data(IndexValue at1,IndexValue at2){
	IndexValue res1=atoms[at1]->res_idx;
	IndexValue res2=atoms[at2]->res_idx;
	string at1type=atoms[at1]->name;
	string at2type=atoms[at2]->name;
	string res1type=(*residues)[res1].name;
	string res2type=(*residues)[res2].name;
	IndexValue dopecode1=niprop[at1];
	IndexValue dopecode2=niprop[at2];
	//for full detail, uncomment
	printf("%s:%s(%i) %s:%s(%i) %i %i",res1type.c_str(),at1type.c_str(),dopecode1,res2type.c_str(),at2type.c_str(),dopecode2,res1+1,res2+1);
}

int dopepw_ac::calc_dopepw_energy(FloatValue *energy)
{
	FloatValue totalenergy=0.0;
	FloatValue dist=0.0,r1,f1,f2;
	IndexValue i,pairidx,pairidxt,idx1,idx2;
	IntValue ri;
	double teste=0;
	//cerr << "nactivepairs=" << nactivepairs << "; nupdatepairs="<< nupdatepairs << endl; 
	for (i=0;i<nupdatepairs;i++){
		idx1=updatepairs[2*i];
		idx2=updatepairs[2*i+1];

		pairidx=natoms*idx1+idx2;
		pairidxt=natoms*idx2+idx1;

		IndexValue doperow=pairrows[pairidx];
		IndexValue term = pairterms[pairidx];

		//string atomtype1=atoms[idx1]->name, atomtype2=atoms[idx2]->name;
		IndexValue atomtype1 = niprop[idx1], atomtype2 = niprop[idx2];
		FloatValue coeff=paircoeffs[pairidx];

		IndexValue res1=atoms[idx1]->res_idx;
		IndexValue res2=atoms[idx2]->res_idx;
		IndexValue cd=res2-res1;

		dist=distances(idx1,idx2);
		//BoolValue hydrogen = ( atomtype1 == "HN" || atomtype1 == "HA1" || atomtype2 == "HN" ||atomtype2 == "HA1");
		//BoolValue cb = ( atomtype1 == "CB" || atomtype2 == "CB");
		BoolValue hydrogen = ( atomtype1 == 1 ||atomtype2 == 1);
		BoolValue cb = ( atomtype1 == 3 || atomtype2 == 3);
		if ((term == PW0Ancont || term==PW1Ancont || term==PW2Ancont) && !cb && dist> 10.00)
			pairenergies[pairidx]=0.0;
		else if (0< dist && dist<distancecutoffs[term] && !hydrogen) {
			FloatValue epair;
			r1 = dist/binsizes[term] - 0.5;
			ri = (int)floor(r1);
			f1 = r1 - ri;
			f2 = 1.0 - f1;
			f1=(ri==MAXBINNUM)?0:f1;           
			if (ri>0) {
				epair=f2*dopeenergies[doperow+ri] + f1*dopeenergies[doperow+ri+1];
			} else {
				epair = dopeenergies[doperow];
			}

			if (term==EEp || term==HHCONT || term==CCCONT || term==EECONT ){
				if (epair > 0.0 && dist > 6.0) epair=0.0;
				if (epair<0.0) epair*=coeff;
			}
			if (term==EEap){
				if (epair > 0.0 && dist > 6.0) epair=0.0;
				if (epair<0.0 && cd>7) epair*=coeff;
			}
			if (term == PW0Ancont || term==PW1Ancont || term==PW2Ancont ) {
				if (!cb)  {
					//if (dist> 10.00) epair=0.0;
					if (dist > 4.0 && epair >0.0 ) epair=0.0;
					if (epair<0 && cd < 7 ) epair = 0.0;
					if (epair <0) epair *= coeff;
				} else {
					if(dist > 4.00 && epair > 0.0 && cd < 5) epair=0;
					if (dist>8 && epair > 0 ) epair = 0.0;
					if (epair <0 && cd <7) epair = 0.0;
				}
			}
			pairenergies[pairidx]=epair;
		}
		else pairenergies[pairidx]=0.0;
	}
	for (i=0;i<nactivepairs;i++){
		idx1=activepairs[2*i];
		idx2=activepairs[2*i+1];
		pairidx=natoms*idx1+idx2;
		FloatValue energy=pairenergies[pairidx];
		totalenergy+=energy;
	}
	*energy=totalenergy;
	return NO_ERROR;
}
