/*******************************************************************************

                                  TSP1.h
                                  ------

  ****************************************************************************
  *                                                                          *
  *   This library is free software; you can redistribute it and/or          *
  *   modify it under the terms of the GNU Lesser General Public             *
  *   License as published by the Free Software Foundation; either           *
  *   version 2.1 of the License, or (at your option) any later version.     *
  *                                                                          *
  *   This library is distributed in the hope that it will be useful,        *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *
  *   Lesser General Public License for more details.                        *
  *                                                                          *
  *   You should have received a copy of the GNU Lesser General Public       *
  *   License along with this library; if not, write to the Free Software    *
  *   Foundation, Inc.,                                                      *
  *   59 Temple Place, Suite 330, Boston, MA  02111-1307  USA                *
  *                                                                          *
  ****************************************************************************

  Copyright (C) 2005 The University of Chicago

  Authors:
  Andres Colubri

  Description:
  PL plugin to calculate local phi, psi energies given the information of the
  first neighbor residues (Torsional Statistical Potential with 1st neighbor
  information).

*******************************************************************************/

#ifndef __TSP1ENERGY_H__
#define __TSP1ENERGY_H__

const int AA_BOC = -3;
const int AA_EOC = -2;
const int AA_ANY = -1;
const int AA_ALA = 0;
const int AA_ARG = 1;
const int AA_ASN = 2;
const int AA_ASP = 3;
const int AA_CYS = 4;
const int AA_GLN = 5;
const int AA_GLU = 6;
const int AA_GLY = 7;
const int AA_HIS = 8;
const int AA_ILE = 9;
const int AA_LEU = 10;
const int AA_LYS = 11;
const int AA_MET = 12;
const int AA_PHE = 13;
const int AA_PRO = 14;
const int AA_SER = 15;
const int AA_THR = 16;
const int AA_TRP = 17;
const int AA_TYR = 18;
const int AA_VAL = 19;
const int AA_HET = 20;

class TSP1Energy
{
public:
	TSP1Energy();
	~TSP1Energy();
	
	int tsp1_mode;
	double energy_coeff;

	void Init(string CfgName);
	int AALetterToCode(char AALetter, bool AcptHET=false, bool AcptEXT=false);
	
	double operator () (string aa0, double phi, double psi) {
		int iaa0=AALetterToCode(*(aa0.c_str())); 
		return Energy(iaa0, AA_ALA, AA_ALA, phi, psi); 
	}
	double operator () (string aa0, string aa1, double phi, double psi) { 
		int iaa0=AALetterToCode(*(aa0.c_str())); 
		int iaa1=AALetterToCode(*(aa1.c_str())); 
		return Energy(iaa0, iaa1, AA_ALA, phi, psi); 
	}
	double operator () (string aa0, string aa1, string aa2, double phi, double psi) { 
		int iaa0=AALetterToCode(*(aa0.c_str())); 
		int iaa1=AALetterToCode(*(aa1.c_str())); 
		int iaa2=AALetterToCode(*(aa2.c_str())); 
		return Energy(iaa0, iaa1, iaa2, phi, psi); 
	}

	double operator () (int aa0, double phi, double psi) { return Energy(aa0, AA_ALA, AA_ALA, phi, psi); }
	double operator () (int aa0, int aa1, double phi, double psi) { return Energy(aa0, aa1, AA_ALA, phi, psi); }
	double operator () (int aa0, int aa1, int aa2, double phi, double psi) { return Energy(aa0, aa1, aa2, phi, psi); }

private:
	int n_bins;
	double bin_size, inv_bin_size, angle0;
	double **values[20][20][20];
	
	int AANameToCode(string AAName);
	int idx(double angle) { return int(inv_bin_size * (angle + angle0)); }
	
	void Resize(int n);

	void InitializeMemory(int n);
	void ReleaseMemory();
	void InitializeEnergy();
	double Energy(int aa0, int aa1, int aa2, double phi, double psi);
	
	int LoadFromFile(string &fn, int mode);
	void SetDefConfig();
	int ReadConfigFile(string CfgName);
	
	// Calculates the plane given by three points (x1, y1, z1), (x2, y1, z2), (x2, y2, z3):
	//                             (x3,y3)
	//                              /  |
	//                             /   |
	//                            /    |
	//                           /     |
	//                          /      |
	//                         /       |
	//                        /        |
	//                    (x1,y1)---(x2,y2)
	inline double PlaneLower(double x1, double y1, double z1, double z2, double z3, double inv_dx, double inv_dy, double x, double y)
	{   
		return z1 + inv_dx * (z2 - z1) * (x - x1) + inv_dy * (z2 - z3) * (y1 - y);
	}
	// Calculates the plane given by three points (x1, y1, z1), (x2, y2, z2), (x1, y2, z3):
	//                   (x3,y3)---(x2,y2)
	//                      |        /
	//                      |       /
	//                      |      /
	//                      |     /
	//                      |    /
	//                      |   / 
	//                      |  / 
	//                    (x1,y1)
	inline double PlaneUpper(double x1, double y1, double z1, double z2, double z3, double inv_dx, double inv_dy, double x, double y)
	{   
		return z1 + inv_dx * (z3 - z2) * (x1 - x) + inv_dy * (z3 - z1) * (y - y1);
	}

	inline double LinearInterpolation(double f0, double f1, double f2, double f3, 
					double x0, double y0, double x, double y, double inv_dx, double inv_dy)
	{
		double f_x = (inv_dx / inv_dy) * (x - x0) + y0;
		// Selecting interpolation region.
		if (y <= f_x) return PlaneLower(x0, y0, f0, f1, f2, inv_dx, inv_dy, x, y); // Region I
		else return PlaneUpper(x0, y0, f0, f2, f3, inv_dx, inv_dy, x, y); // Region II
	}

	void trim2(string& str) {
                string::size_type pos = str.find_last_not_of(' ');
                if(pos != string::npos) {
                        str.erase(pos + 1);
                        pos = str.find_first_not_of(' ');
                        if(pos != string::npos) 
                                str.erase(0, pos);
                }
                else str.erase(str.begin(), str.end());
        };


private:
	string tsp1_par_file;
	int num_bin;
};

#endif
