#include "molecule.h"
ZhouMolecule::ZhouMolecule(const char *fn):ZhouProtein(){
	Rcut0=mbin*0.5, Rcut=nbin*0.5, dRcut=0.5;
	chID = toupper(fn[4]); //fn[4]='\0';
	if(chID == '0') chID = ' ';
	FILE *fp = openfile(fn, "r");
	bzero(syms, sizeof(syms)); syms[0][0][0] = syms[0][1][1] = syms[0][2][2] = 1.;
//	initReplica(fp);
	rdpdb(fp);
	fclose(fp);
}
void ZhouMolecule::initcrd(){
	bmiss.resize(natm); x0.clear();
	for(int i=0; i<natm; i++){
		bmiss[i] = true;
		if( atoms.at(i).filled() ) bmiss[i] = false;
		double *xp = atoms[i].getx();
		for(int m=0; m<3; m++){
			if(i==0) xmax[m] = xmin[m] = xp[m];
			if(xp[m] > xmax[m]) xmax[m] = xp[m];
			if(xp[m] < xmin[m]) xmin[m] = xp[m];
		}
		x0.push_back(xp);
	}
}
bool ZhouMolecule::initcrd(int isym, int ix, int iy, int iz){
	double xs[3], xt[3], xmax2[3], xmin2[3];
	xn.resize(natm);
	for(int m=0; m<3; m++) xs[m] = ix*cvec[0][m] + iy*cvec[1][m] + iz*cvec[2][m];
	for(int i=0; i<natm; i++){
		double *xp = atoms[i].getx();
		for(int m=0; m<3; m++){
			double *rp = &syms[isym][m][0];
			xt[m] = rp[0]*xp[0] + rp[1]*xp[1] + rp[2]*xp[2] + rp[3] + xs[m];
			if(i==0) xmax2[m] = xmin2[m] = xt[m];
			if(xt[m] > xmax2[m]) xmax2[m] = xt[m];
			if(xt[m] < xmin2[m]) xmin2[m] = xt[m];
		}
		xn[i] = xt;
	}
	for(int m=0; m<3; m++){
		if(xmin2[m]-xmax[m]>Rcut0 || xmin[m]-xmax2[m]>Rcut0) return true;
	}
	return false;
}
bool ZhouMolecule::rdtrj(FILE *fp){
	int na; fread(&na, sizeof(int), 1, fp);
	if(ferror(fp)){fprintf(stderr, "Error traj file\n"); exit(1);}
	if(feof(fp)) return false;
	int ix[na*3];
	fread(ix, sizeof(int), na*3, fp);
	bool ball=false;
	if(na==(int)pdbidx.size()) ball=true;
	else if(natm!=na-1 && natm!=na) {
		fprintf(stderr, "different number of atoms: %d %d %d\n", natm, na, pdbidx.size());
		return false;
	}
	int n=0;
	for(int i=0; i<(int)pdbidx.size(); i++){
		int it=pdbidx.at(i);
		if(it<0) continue;
		double x[3];
		for(int m=0; m<3; m++){
			if(ball) x[m] = ix[i*3+m]/1000.;	// for Luo decoys
			else x[m] = ix[n*3+m]/1000.;
		}
		atoms.at(it).setx(x);
		n++;
	}
	return true;
}
void ZhouMolecule::initReplica(FILE *fp){
	char str[121];
	int irep;
	double *rp;
	double leng[3], ag[3];
	nsym = 0; irep = 0;
	while(fgets(str, 120, fp) != NULL){
		if(strstr(str, "SMTRY1") == str+13){
			rp = &syms[nsym][0][0];
			sscanf(str+23, "%lf%lf%lf%lf", rp, rp+1, rp+2, rp+3);
			fgets(str, 120, fp);
			rp = &syms[nsym][1][0];
			sscanf(str+23, "%lf%lf%lf%lf", rp, rp+1, rp+2, rp+3);
			fgets(str, 120, fp);
			rp = &syms[nsym][2][0];
			sscanf(str+23, "%lf%lf%lf%lf", rp, rp+1, rp+2, rp+3);
			nsym++;
		} else if(strstr(str, "CRYST1") == str){
			sscanf(str+7, "%lf%lf%lf%lf%lf%lf", leng, leng+1, leng+2, ag, ag+1, ag+2);
			irep = strtol(str+66, NULL, 10);
			if(irep != nsym) printf("Warning, diff symt: %d %d\n", irep, nsym);
		} else if(strstr(str, "ATOM ") == str){
			fseek(fp, -strlen(str), 1); break;
		}
	}
	if(irep < 1) fprintf(stdout, "Warning, No CRYST dat found");
	if(nsym < 1) fprintf(stdout, "Warning, No SYMTRY");
//
	bzero(cvec, sizeof(cvec));
	for(int m=0; m<3; m++) ag[m] *= acos(-1.) / 180.;
	cvec[0][0] = 1.;
	cvec[1][0] = cos(ag[2]); cvec[1][1] = sin(ag[2]);
	cvec[2][0] = cos(ag[1]); cvec[2][1] = (cos(ag[0]) - cos(ag[1])*cos(ag[2]))/sin(ag[2]);
	cvec[2][2] = sqrt(1 - dot_product(2, cvec[2], cvec[2]));
	for(int i=0; i<3; i++){
		for(int m=0; m<3; m++) cvec[i][m] *= leng[i];
//		printf("%f %f %f\n", cvec[i][0], cvec[i][1], cvec[i][2]);
	}
}
