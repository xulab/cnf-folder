#COMPILER = g++43
COMPILER = g++ 

# paths of all the necessary components of the projects defined in this makefile
JHOME=~fzhao/mcs/Folding
CRFSampler_HOME=${JHOME}/CRFSampler-CNF
FlexCRF_HOME=${JHOME}/FlexCRFs-0.3-CNF

GSL_HOME=${JHOME}/GSL
vonAPI_HOME=${JHOME}/vonmisesAPI
BALL_HOME=${JHOME}/BALL-1.2
BBQ_HOME=${JHOME}/BBQ

# paths for all the header files
INCL_DIR  = -I /usr/include
INCL_DIR += -I ${FlexCRF_HOME}/include -I ${CRFSampler_HOME}/include 
INCL_DIR += -I ${GSL_HOME}/include
INCL_DIR += -I ${vonAPI_HOME}/include
INCL_DIR += -I ${BALL_HOME}/include
INCL_DIR += -I ${BBQ_HOME}/ann/ann_1.1/include

# paths for all the necessary libraries
LIBRARY2 = -L ${CRFSampler_HOME}/lib -lCRFSampler
LIBRARY2 += -L ${FlexCRF_HOME}/lib -lFlexCRFs_rv
LIBRARY2 += -L ${vonAPI_HOME}/lib -lAngles
#LIBRARY2 += -L ${vonAPI_HOME}/lib/lib64 -lAngles
LIBRARY2 += -L ${GSL_HOME}/lib -lgsl  -lgslcblas 
LIBRARY2 += -L ${BALL_HOME}/lib -lBALL
LIBRARY2 += -L ${BBQ_HOME}/ann/ann_1.1/lib -lANN

# all the object files for ProteinFolder
ProteinFolder_OBJS = ProteinFolder.o \
	PhiPsiCluster.o FBCluster.o KentParamFileParser.o Kent.o \
	SimpPDB.o newPSEA.o DOPE.o stringTokenizer.o DopeX.o Contact.o strtokenizer.o\
	newutil.o bbq.o RamaBasin.o SideChain_util.o BMKhbond.o TSP1Energy.o ESPEnergy.o DopeBack.o DopeBeta.o \
	dDFIRE.o restype.o protein.o xyzatm.o protpl.o molecule.o DOPE_PW.o dopepw.o \
	newconfig.o StructureBuilder.o ScoringFunction.o ConformationOptimizer.o EPAD.o

# compiler flags
CFLAGS = -m32 -fPIC -fexceptions -DNDEBUG -W
#CFLAGS = -fPIC -fexceptions -DNDEBUG -W
CFLAGS += -I/usr/local/include ${INCL_DIR} 
CFLAGS += -O3 -s
#CFLAGS += -s -g
#CFLAGS += -Wall 

# link options and libraries to be linked
ProteinFolder_LIB = -static ${LIBRARY2} -lm

# targets to be compiled
all: ProteinFolder

clean:
	rm -f *.o core 

ProteinFolder: $(ProteinFolder_OBJS) 
	$(COMPILER) -m32 -o $@ $(ProteinFolder_OBJS) $(ProteinFolder_LIB) 

# how the object files are generated
%.o:%.cpp
	$(COMPILER) $(CFLAGS)  -c $<

%.o:%.cc
	$(COMPILER) $(CFLAGS)  -c $<

