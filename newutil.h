#ifndef _NEW_UTIL_H
#define _NEW_UTIL_H

#include <string>

using namespace std;

//remove the spaces at the two ends of the string
void trim(string& str);

#endif
