
#ifndef _CONFIG_H
#define _CONFIG_H

#include "IP_constant.h"
//#include <vector.h>

#define MaxCmdLen	4096

#define CONFIGFILE	"RAPTOR.conf"

#define TEMP_PATH_STR		"TemplatePath"
#define TEMP_TYPE_STR		"TemplateType"
#define PART_PATH_STR		"PartitionPath"
#define PART_TYPE_STR		"PartitionType"
#define COMPRESS_PATH_STR	"CompressPath"
#define COMPRESS_PATH_STR2	"CompressPath2"
#define PARAM_PATH_STR		"ParamPath"
#define SEQ_PATH_STR		"SeqPath"
#define PSP_PATH_STR		"PSPPath"
#define PSM_PATH_STR		"PSMPath"
#define SS_PATH_STR		"SSPath"
#define OUTPUT_PATH_STR		"OutputPath"
#define SARF_PATH_STR		"SarfPath"
#define MODEL_PATH_STR		"ModelPath"
#define PDB_PATH_STR		"PDBPath"
#define TEMP_BOND_PATH_STR	"TempBondPath"
#define SEQ_BOND_PATH_STR	"SeqBondPath"
#define TEMP_VOLUME_PATH_STR	"TempVolPath"
#define REQUEST_PATH_STR	"RequestPath"
#define FINISHED_PATH_STR	"FinishedPath"
#define PPI_PATH_STR		"PPIPath"

#define MODELLER_PATH_STR	"ModellerProgram"
#define MODELLER_OUTPATH_STR	"ModellerOutDir"
#define MODELLER_NUM_STR	"NumModels"
#define USE_MODELLER_STR	"CallModeller"

#define MAXLOOPSIZE_STR		"MaxLoopSize"
#define MAXSKIPSEQSIZE_STR	"MaxSkipSeqSize"

#define LOOPFINDER_STR		"LoopFinder"
#define USE_SS_STR		"UseSS"
#define USE_CONTACT_STR		"UseContactCapacity"
#define USE_PSP_STR		"UsePSP"
#define USE_PSM_STR		"UsePSM"
#define USE_VOL_STR		"UseVolume"
#define USE_DIST_DEPEND_CONTACT_STR	"UseDistanceDependentContact"
#define USE_NMR_STR		"UseNMR"

#define CALC_ZSCORE_STR 	"CalcZScore"
#define CALC_STRICT_ZSCORE_STR 	"CalcStrictZScore"

#define NO_SOLVENT_STR		"NoSolventType"


#define FILTERCUTOFF_STR	"FilterCutoff"
#define FILTERSINGLETON_STR	"FilterSingleton"
#define FILTERMUTATION_STR	"FilterMutate"
#define FITFILTER_STR		"FitFilter"
#define SSFILTER_STR		"SSFilter"

#define USE_END_GAP_STR		"UseEndGap"
#define USE_SPECIAL_GAP_STR	"UseSpecialEndGap"

#define THREAD_RESULT_FILE_STR	"ThreadResultFile"
#define THREAD_ALIGN_FILE_STR   "ThreadAlignFile"
#define WEIGHT_FILE_STR		"WeightFile"
#define THREAD_XML_FILE_STR	"ThreadXMLFile"


#define SVM_PARAM_STR			"SVMParams"
#define SVM_STANDARD_PARAM_FILE_STR	"SVMStandardParamFile"
#define SVM_ESTIMATOR_STR		"SVMEstimator"

#define OUT_PS_STR		"PrintPS"
#define OUT_DETAIL_RES_STR	"OutputDetailedResult"
#define OUT_BACKBONE_STR	"OutputBackbone"

#define ZSCORE_CUTOFF_STR	"ZScoreCutoff"
#define ZSCORE_CUTOFF_PLUS_STR	"ZScoreCutoffPlus"
#define MAX_THREAD_TIME_STR	"MaxThreadTime"

#define PROCESS_NUM_STR		"ProcessNum"
#define COMPUTE_TYPE_STR	"ComputeType"


#define MIN_CORE_LEN_STR	"MinCoreLen"
#define MIN_EX_LINK_NUM_STR	"MinExLinkNum"

#define MIN_PAIR_DIST_STR	"MinPairDist"
#define MAX_CONTACT_DIST_STR	"MaxContactDist"
#define PPI_MAX_CONTACT_DIST_STR	"PPIMaxContactDist"

#define LP_METHOD_STR	"LPMethod"
#define DEBUG_LEVEL_STR	"debug_level"

#define PSP_PROGRAM_STR		"PSP_program"
#define SS_PROGRAM_STR		"SSpred_program"
#define BTHREAD_PROGRAM_STR	"bThread_program"
#define ESTIMATE_PROGRAM_STR	"estimate_program"
#define BLAST_PROGRAM_STR	"blastpgp"
#define BLAST_DB_STR		"blastdb"

#define ALIGNMENT_TYPE_STR	"AlignmentType"

#define RAPTOR_ADM_STR		"RAPTORadm"

#define TEMP_RANK_METHOD_STR	"TempRankMethod"
#define DETAILED_SCORE_RANK_STR	"OutDetailedScoreRank"
#define OUT_ALIGN_MODEL_STR	"OutAlignmentModel"


#define DEBUG_LEVEL0	0	//no debug information
#define DEBUG_LEVEL1	1
#define DEBUG_LEVEL2	2
#define DEBUG_LEVEL3	3	//the most detailed debug information


#define GLOBAL_ALIGNMENT	1
#define GLOBAL_LOCAL		2

#define SKIP_PSP_STR	"SkipPSPGeneration" //skip the calculation of PSP
#define SKIP_SS_STR	"SkipSSGeneration"	//skip the calculation of SS

#define TEMPLATE_LIST_STR	"templateList"
#define TOP_TEMPLATE_LIST_STR	"topTemplateList"
#define TASK_LIST_STR		"taskList"
#define TOP_TASK_LIST_STR		"topTaskList"
#define TOP_TEMPLATE_NUM_STR	"NumOfTopTemplates"

#define EQUAL(s,a)	(strcasecmp((s),(a))==0)

#define MAX_SPEC_LINE_NUM	300
#define MAX_SESSION_NUM		10
#define SESSION_START_STR	"SESSION_START"
#define SESSION_END_STR		"SESSION_END"

typedef struct {
	char* bufs[MAX_SPEC_LINE_NUM];
	int lineNum;
}SESSION; //each flow chart corresponds to a  RAPTOR session

class Config {

	
	public:

		char sourceFile[MaxLineSize];	//the source file for this configuration

    		// *******BY TINA, May 8, 2003
		// We only need ONE env var: RAPTOR_HOME
        	const static char RAPTOR_HOME_STRING [16];	
        	char RAPTOR_HOME [MAX_STRING_LEN];
        	char RAPTOR_HOME_VAR [16];
    		// *******BY TINA, May 8, 2003

		char RAPTORadm[256];	//email address of RAPTOR administrator

		char processID[256]; //just for distributed computing
					//it is used for identify its own 
				    //it is null when no distributed computing

		int ProcessNum; //the total process number

		int LPMethod;

		//algorithm
		char ComputeType[MaxLineSize];

		char ThreadResultFile[MaxLineSize];
		char ThreadAlignFile[MaxLineSize];
		char WeightFile[MaxLineSize];
		char ThreadXMLFile[MaxLineSize];

		FILE* err_log_fp;
		FILE* cpu_log_fp;

		char TemplatePath[MaxLineSize];
		char TemplateType[MaxLineSize];
		char PartitionPath[MaxLineSize];
		char PartitionType[MaxLineSize];
		char ComRootDir[MaxLineSize];
		char ComRootDir2[MaxLineSize];
		char ParamPath[MaxLineSize];
		char SeqPath[MaxLineSize];
		char PSPPath[MaxLineSize];
		char PSMPath[MaxLineSize];
		char SSPath[MaxLineSize];
		char TempBondPath[MaxLineSize];
		char SeqBondPath[MaxLineSize];
		char TempVolPath[MaxLineSize];	//path to the template volume contact file
		char OutputPath[MaxLineSize];
		char SarfPath[MaxLineSize];
		char ModelPath[MaxLineSize];
		char PDBPath[MaxLineSize];
		char PPIPath[MaxLineSize];	//path to the protein-protein interaction file

		char request_dir[MaxLineSize];
		char finished_dir[MaxLineSize];

		char Modeller[MaxLineSize]; //the path to Modeller program
		char ModellerOut[MaxLineSize];	//the Modeller output dir 
		int NumModels;	//the number of models generated from each template
		int CallModeller; //call modeller or not


		float weightPPI;	//weight for protein-protein interaction
		float weightSingleton;
		float weightPair;
		float weightLoopGap;
		float weightGapPenalty;
		float weightMutation;
		float weightSStruct; //weight for secondary structure prediction
		float weightContactCapacity;

		float	GAPBasePenalty;
		float	GAPIncPenalty;

		float weightNMR;	//weight for NMR constraints
		//float weightNMRPenalty;	//penalty for NMR constraints
		//float weightNMRsingle;	//if hbond is unavailable, then use this parameter when NMR is required

		int MinCoreSize; //the min core size required for filtering by singleton score

		int MaxLoopSize;
		int MaxSkipSeqSize;

		float MaxContactDist;
		float PPIMaxContactDist; //the max contact dist for protein-protein interaction
		//float DistCutOff;
		float MinPairDist;

		bool	UseGapAlign;
		bool 	UseEndGap; //Use head and tail gap
		bool    UseSpecialGap;

		bool NoSolventType;

		bool	LevelOneRemoval;
		bool	LevelTwoRemoval;

		float 	FirstCut;
		float 	SecondCut;

		int 	ConserveLoopSize;
		int 	LoopMinSeqSize;
		float	LoopConfidence;
		int	LoopVariation;
		

		bool FitFilter;
		bool SSFilter;

		float FilterCutOff;
		float	FilterSingleton;
		float	FilterMutate;

		float	MinSSFilterFlag;
		float	MinSSFilterSize;
		float	MinSSFilterLevel;
		float   SingleSSFilterLevel;


		bool SkipPSPGeneration;
		bool SkipSSGeneration;

		bool LoopFinder;

		bool UsePSP; 	//use position specific scoring system
		bool UsePSM; 	//use position specific scoring system
		bool UseSS;	//use secondary structure
		bool UseContactCapacity;	//use secondary structure
		bool CalcZScore;
		bool StrictZScore;
	//	bool UseNMRbond;
		bool UseVolume;	//if set to true, then disable all others
		bool UseDistanceDependentContact; //if set to true, then use distance dependent contacts

		int alignment_type;	//global, global-local?

		int CoreGrayZone;

		char SVMEstimator[MaxLineSize]; //the  svm estimator path
		char SVMParams[MaxLineSize] ;//parameters for svm estimator
					//include the SVM model and params
		char SVMStandardParamFile[MaxLineSize];


		bool PrintPS;

		float zScoreCutoff;
		float zScoreCutoffPlus;

		int MinCoreLen;
		int MinExLinkNum;

		int debug_level;
		int MaxThreadTime; //unit is second

    		int TempRankMethod;
		int DetailedScoreRank;
		bool OutputDetailedResult;
		int OutAlignmentModel;
		bool OutputBackbone;


		int  NumOfTopTemplates;
		char templateList[MaxLineSize];
		char topTemplateList[MaxLineSize];
		char taskList[MaxLineSize];
		char topTaskList[MaxLineSize];

		//blast program information
		char blast_program[MaxCmdLen];
		char blast_db[MaxLineSize];

		//a wrapper program to call secondary structure prediction
		//two parameters: first is the seq path, second is result path
		char SSpred_program[MaxCmdLen];

		//a wrapper program to call batch threading
		char bThread_program[MaxCmdLen];

		//a wrapper program to calculate position specific profile
		//of a target sequence
		//two parameters: first is the seq path, 
		//second is the result path
		char PSP_program[MaxCmdLen];

		//estimate program, used for rank template
		char estimate_program[MaxCmdLen];
	private:
		SESSION sessions[MAX_SESSION_NUM];
		int sessionNum;
		//vector<SESSION> sessions;


		SESSION common_session;

	public:

		Config();
		Config(char* fileName);
		~Config();

		void configure(); //default configuration


		void closeLogFile();
		void writeErrorLog(char* s);
		void writeCPUTime(char* t);
		void setTemplatePath(char* path,char* type);
		void setPartitionPath(char* path,char* type);
		void setSeqPath(char* path);
		void setPSPPath(char* path);
		void setPath(char* path, char* value);
		void setWeight(char* fileName);

		//int EnablePairWise(bool flag);
		int EnablePSM(bool flag);


		void SetProcessID(char* id);
		void SetProcessID(int id);

		//void SetXMLOutputFile(char* seqName,char* id);
		char* GetXMLOutputFile();

		void printConfig(char *msg = ""); 

		void ProcessOneSession(SESSION* s);
		void ProcessOneSession(int index);
		int GetSessionNum(){return sessionNum;};
		void WriteSession(char* file, int index=-1); //output the content of a configuration file
			//if index==-1, then only the common session will be outputed
	private:


		//read a configuration file, this file must be named 
		//IPThread.conf and is in the current work directory

		//currently , this configuration file only support very
		// limited setting, this class needed to improved to support
		//all setting

		void readConfiguration();

		int parseLine(char* buf, char* o1, char* o2);

		void ProcessOneLine(char* line);

		void SetBool(bool* a, char* value);

		//do some postprocessing after reading configuration
		void PostProcess();
		void Initialize();

};

//parse a line into two substrings
//if just one token, return 1
//otherwise return 2
//the fist toekn is stored into o1
//others are all sored into o2
//for example: if buf="aa bb -i cc -o"
//then o1=aa, o2="bb -i cc -o"

//int parseLine(char* buf, char* o1, char* o2);


#endif

