#ifndef _GEN_SEQ_FRAG_H
#define _GEN_SEQ_FRAG_H
#include "seq.h"
#define T_DOMAIN	"domain"
#define T_FSSP		"fssp"

#define INTEGER_SOLUTION	true
#define LINEAR_SOLUTION		false

#define DEF_ALGORITHM ALG_IP	
#define DEF_SOLUTION_TYPE	INTEGER_SOLUTION
#define DEF_TEMPLATE_TYPE	T_DOMAIN 

typedef struct _OPTIONS{
	char algorithm[1024]; //algorithm used

	char second_file[1024];  //secondary prediction result file
			//if null, then no secondary structure information

	char out_file[1024];  //output file 
	bool sol_type;		//if true, only linear programming, otherwise
				//integer programming

	char template_type[1024];

	char template_name[1024];
	char seq_name[1024];

	//if true, then the threading result is used for weight training
	bool isParameterStudy;
	
}OPTIONS;
#endif
