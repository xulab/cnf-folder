/*******************************************************************************

                                   DOPE.h
                                   ------

  ****************************************************************************
  *                                                                          *
  *   This library is free software; you can redistribute it and/or          *
  *   modify it under the terms of the GNU Lesser General Public             *
  *   License as published by the Free Software Foundation; either           *
  *   version 2.1 of the License, or (at your option) any later version.     *
  *                                                                          *
  *   This library is distributed in the hope that it will be useful,        *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *
  *   Lesser General Public License for more details.                        *
  *                                                                          *
  *   You should have received a copy of the GNU Lesser General Public       *
  *   License along with this library; if not, write to the Free Software    *
  *   Foundation, Inc.,                                                      *
  *   59 Temple Place, Suite 330, Boston, MA  02111-1307  USA                *
  *                                                                          *
  ****************************************************************************

  Copyright (C) 2005 The University of Chicago

  Authors:
  Andres Colubri

  Description:
  Plugin that calculates pairwise statistical potentials. In particular, it
  can calculate DOPE, the statistical potential created by Min-yi Shen and
  Andrej Sali at UCSF.
  This plugin can be configured to evaluate other statistical potentials by
  providing the appropriate parameter file.

*******************************************************************************/

#ifndef __DOPE_PW_H__
#define __DOPE_PW_H__

#include "DOPE.h"

// This class contains the binned energy values.
class DopePW: public DOPE
{
public:
	DopePW();
	~DopePW();
	
	int ReadConfigFile(string cfgName);
	void SetDefConfig();
		
	double *PW_EE_cont_Energy;
	double *PW_HH_cont_Energy;
	double *PW_CC_cont_Energy;
	
	double *PW_0EEap_ncont_Energy;
	double *PW_0EEp_ncont_Energy;
	double *PW_0A_ncont_Energy;
	double *PW_1A_ncont_Energy;
	double *PW_2A_ncont_Energy;
	
	double PW_EE_cont_Energy_coefficient;
	double PW_HH_cont_Energy_coefficient;
	double PW_CC_cont_Energy_coefficient;
	
	double PW_0EEap_ncont_Energy_coefficient;
	double PW_0EEp_ncont_Energy_coefficient;
	double PW_0A_ncont_Energy_coefficient;
	double PW_1A_ncont_Energy_coefficient;
	double PW_2A_ncont_Energy_coefficient;
	
	bool PW_EE_cont_Energy_steric_only;
	bool PW_HH_cont_Energy_steric_only;
	bool PW_CC_cont_Energy_steric_only;
	
	bool PW_0EEap_ncont_Energy_steric_only;
	bool PW_0EEp_ncont_Energy_steric_only;
	bool PW_0A_ncont_Energy_steric_only;
	bool PW_1A_ncont_Energy_steric_only;
	bool PW_2A_ncont_Energy_steric_only;
	
	//double DOPEenergy(int aa1, int at1, int aa2, int at2, int bin, int cd, int ss1, int ss2, int id, double dist);
	double DOPEenergy(string res_id0, int seq0, string atom0, string res_id1, int seq1, string atom1, double dist, int ss0, int ss1, int seq_len);
	void Init(string cfgFile);
	double PropWashEnergy(string res_id0, int seq0, string atom0, string res_id1, int seq1, string atom1, double distance);
	
	int pap_vec[200][200];
	int dope_vec[200][200];
	int ON_hbond_vec[200][200];
	int NO_hbond_vec[200][200];
	vector<int> continuity_vec;
	string secseq;
	vector<vector<int> > ESP_energy;

	int ESP(string aa_name, int i){
		int aa = AANameToCode(aa_name);
		if (aa>=0 && aa<(int)ESP_energy.size() && i>=0 && i<3)
			return ESP_energy[aa][i];
		else
		{
			//cerr << "ERROE!! Trying to access invalid index of DopeX:ESP at [" << aa<<","<<i<<"] with " << a_char <<endl;
			//exit(0);
			return 0;
		}
	};
	
public:	
	bool add_backbone, add_hydrogens, add_centroids, add_side_chains, add_only_CB, CB_CB_focus, use_energy_cap, fix_consensus;
		
	string PW_EE_cont_Energy_par_file;
	string PW_HH_cont_Energy_par_file;
	string PW_CC_cont_Energy_par_file;
	
	string PW_0EEap_ncont_Energy_par_file;
	string PW_0EEp_ncont_Energy_par_file;
	string PW_0A_ncont_Energy_par_file;
	string PW_1A_ncont_Energy_par_file;
	string PW_2A_ncont_Energy_par_file;
	string esp_par_file;
	bool calc_SC_energy;

	bool add_bonded;
	int min_chain_dist, max_chain_dist, min_CB_CB_dist;
	double energy_coeff, CB_CB_coefficient, dist_cutoff_ncont;

	double Penalty_coeff;
	double Burial_sphere_rad;
};

class DopePW_RAMA: public DopePW
{
public:
	//DopePW_RAMA();
	//~DopePW_RAMA();
	
	//void Init(string cfgFile);
	
	string dope_par_file, dope_dir, esp_par_file, EECDis3_par_file;
	double energy_cap;
	
	bool Burial_renorm, analysis, use_secseq;
	int Burial_min_atcount, Burial_max_atcount;
	
	vector<int> CB_burial;
	vector<int> HB_burial;
	vector<int> H_bond;

	int pw_vec[200][200];
	int pap_vec[200][200];
	int EE_vec[200][200];
	int OH_hbond_vec[200][200];
	int HO_hbond_vec[200][200];
	int COCO_align[200][200];
	int NHNH_align[200][200];
	int NHCO_align[200][200];
	int CONH_align[200][200];
	int P_align[200][200];
	int AP_align[200][200];
	int continuity_vec[200];
	int SecStr_vec[200];
	
	double DOPEenergy(string res0, string atom0, string res1, string atom1, double dist, bool CB1, bool CB2, int cd);
	double DOPE_PW_continuous(string res0, string atom0, string res1, string atom1, double distance, int SecStr_val);
	double DOPE_PW_strand(string res0, string atom0, string res1, string atom1, double distance, int EE_val);
	double DOPE_PW_0(string res0, string atom0, string res1, string atom1, double distance, bool CB, int cd);
	double PropWashEnergy(string res0, string atom0, string res1, string atom1, double distance, bool CB, int cd, int pw_val);
	void FindBetaStrands(int seq_length);
	void SetContinuity(int seq_length);
};

#endif
