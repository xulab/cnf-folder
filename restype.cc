#include "restype.h"
#include <stdio.h>
#include <string.h>

const char rnam3_std[][4]={"ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU",
							"MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR"};
const char rnam1_std[] = "ACDEFGHIKLMNPQRSTVWY";
vector <Restype> PRtype;
int nrestype=20;
int resDefine(string rn0){
	string rn = rn0;
	if(rn=="HSD" || rn=="HSE") rn = "HIS";
	for(int i=0; i<nrestype; i++){
		if(rn==rnam3_std[i]) return i;
	}
	if(DEBUG>0) fprintf(stderr, "Unrecognized residue name: %s\n", rn.c_str());
	return -1;
}
int resDefine1(char rn){
	for(int i=0; i<nrestype; i++){
		if(rn==rnam1_std[i]) return i;
	}
	if(DEBUG>0) fprintf(stderr, "Unrecognized residue name: %c\n", rn);
	return -1;
}
int defineAtype(string rn, string an){
	int rid = resDefine(rn), pos;
	if(rid < 0) return -1;
	int type = PRtype.at(rid).define(an, pos);
	if(DEBUG>0 && type<0) fprintf(stderr, "Unkown atom: %s %s\n", rn.c_str(), an.c_str());
	return type;
}
void initPRtypes0(int flag){
	for(int i=0; i<nrestype; i++){
		PRtype.push_back(Restype(i, rnam3_std[i]));
		string rn(rnam3_std[i]);

		Restype *res = &PRtype[i];
		res->addatom("N"); res->addatom("CA"); res->addatom("C"); res->addatom("O");
//		if(flag==2){res->addatom("CB"); continue;}				// only main+Cb
//		else if(flag==1 && rn!="PRO") res->addatom("H");		// mainchain H included
//
		if(rn != "GLY") res->addatom("CB");
		if(flag!=0 && rn!="GLY" && rn!="ALA") res->addatom("HB1");
//
		if(rn == "VAL"){
			res->addatom("CG1"); res->addatom("CG2");
		} else if(rn == "LEU"){
			res->addatom("CG"); res->addatom("CD1"); res->addatom("CD2"); 
		} else if(rn == "ILE"){
			res->addatom("CG1"); res->addatom("CG2"); res->addatom("CD1");
		} else if(rn == "SER"){
			res->addatom("OG");
		} else if(rn == "THR"){
			res->addatom("OG1"); res->addatom("CG2");
		} else if(rn == "CYS"){
			res->addatom("SG");
		}else if(rn == "PRO"){
			res->addatom("CG"); res->addatom("CD");
			res->addneib("CD", "N");
		}else if(rn == "PHE"){
			res->addatom("CG"); res->addatom("CD1"); res->addatom("CD2"); res->addatom("CE1");
			res->addatom("CE2"); res->addatom("CZ");
		}else if(rn == "TYR"){
			res->addatom("CG"); res->addatom("CD1"); res->addatom("CD2"); res->addatom("CE1");
			res->addatom("CE2"); res->addatom("CZ"); res->addatom("OH"); 
		}else if(rn == "TRP"){
			res->addatom("CG"); res->addatom("CD1"); res->addatom("CD2"); res->addatom("NE1");
			res->addatom("CE2"); res->addatom("CE3"); res->addatom("CZ2"); res->addatom("CZ3");
			res->addatom("CH2");
			res->addneib("CE3", "CD2");
		}else if(rn == "HIS"){
			res->addatom("CG"); res->addatom("ND1"); res->addatom("CD2"); res->addatom("CE1");
			res->addatom("NE2");
			res->addneib("NE2", "CE1");
		}else if(rn == "ASP"){
			res->addatom("CG"); res->addatom("OD1"); res->addatom("OD2");
		}else if(rn == "ASN"){
			res->addatom("CG"); res->addatom("OD1"); res->addatom("ND2");
		}else if(rn == "GLU"){
			res->addatom("CG"); res->addatom("CD"); res->addatom("OE1"); res->addatom("OE2");
		}else if(rn == "GLN"){
			res->addatom("CG"); res->addatom("CD"); res->addatom("OE1"); res->addatom("NE2");
		}else if(rn == "MET"){
			res->addatom("CG"); res->addatom("SD"); res->addatom("CE");
		}else if(rn == "LYS"){
			res->addatom("CG"); res->addatom("CD"); res->addatom("CE"); res->addatom("NZ");
		}else if(rn == "ARG"){
			res->addatom("CG"); res->addatom("CD"); res->addatom("NE"); res->addatom("CZ");
			res->addatom("NH1"); res->addatom("NH2");
		}
	}
}
void Restype::addatom(const string an){
	static int ia=0;
	atypes.push_back(Atomtype(ia++, an));
	if(an=="CA") addneib(an,"N");
	else if(an=="C") addneib(an, "CA");
	else if(an=="O") addneib(an, "C");
	else if(an=="H") addneib(an, "N");
	else if(an=="CB") addneib(an, "CA");
	else if(an=="HB1") addneib(an, "CB");
	else if(an.size()>1){
//
		string str;
		if(an.at(1) == 'G') str="*B";
		else if(an.at(1) == 'D') str="*G";
		else if(an.at(1) == 'E') str="*D";
		else if(an.at(1) == 'Z') str="*E";
		else if(an.at(1) == 'H') str="*Z";
		if(an.size()>=3) str += an.substr(2);
		addneib(an, str);
	}
}
void Restype::addneib(const string an1, const string an2){
	int i1, i2;
	vector <int> ilist;
	i1 = i2 = -1;
	for(int i=0; i<(int)atypes.size(); i++){
		if(atypes.at(i).isname(an2)) ilist.push_back(i);
		else if(atypes.at(i).isname(an1)) {i1=i; break;}
	}
	i2 = ilist.size();
	if(i1<0 || i2<=0) {
/*		for(int i=0; i<atypes.size(); i++) cerr<<*atypes.at(i).getname()<<' ';
		cerr<<"Error: "<<name<<' '<<an1<<' '<<an2<<endl;*/
		return;
	}
	for(int i=0; i<(int)ilist.size(); i++){
		i2 = ilist.at(i);
		neibs[0].push_back(atypes.at(i1).getname());
		neibs[1].push_back(atypes.at(i2).getname());
	}
}
int Restype::define(string an, int &pos){
	for(int i=0; i<(int)atypes.size(); i++){
		if(atypes.at(i).getname()==an) {
			pos = i;
			return atypes.at(i).gettype();
		}
	}
	return -1;
}
bool Atomtype::isname(const string an){
	int is, ie, flag;
	if(an[0]=='*'){
		is=1; ie = min(name.size(), an.size()) - is;
		ie = max(ie, 1);
		flag = name.compare(is, ie, an, is, ie);
	} else flag = name.compare(an);
	return (flag == 0);
}
void initPRtypes(int flag){
	if(flag==0 || flag==1) {initPRtypes0(flag); return;}
	FILE *fp=openfile(string(getenv("DATADIR")) + "aa20.int", "r");
	PRtype.resize(20);
	int rtype=-1, atype=0;
	char str[121], rn[4];
	while(fgets(str, 120, fp) != NULL){
		if(strchr(str, '#') == str) continue;
		if(strstr(str, "RES") == str){		// add residue
			sscanf(str+4, "%s", rn);
			rtype = resDefine(rn);
			if(rtype < 0) continue;
			PRtype[rtype].setRestype(rn); continue;
		}
		if(rtype<0) continue;
		Restype *res = &PRtype[rtype];
		if(strstr(str, "ATOM") == str){		// add atoms
			char an1[5], an2[5];
			sscanf(str+4, "%s%s", an1, an2);
			if(an1[0]=='H' && an2[0]=='C') continue;
			if(DEBUG>2) cout<<rnam3_std[rtype]<<' '<<an1<<' '<<an2<<endl;
			res->addatom(atype++, an1);
			continue;
		}
	}
}
