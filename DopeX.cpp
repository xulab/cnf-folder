/*******************************************************************************
                                  DopeX-Back-AllAtom.cpp
                                  ---------------------
  Copyright (C) 2005 The University of Chicago
  Authors: 
  Min-yi Shen, original code
  Andres Colubri, coversion to C++
  James Fitzgerald, backbone dependence
  Description:
  PL plugin to calculate Min-yi's Statistical Potential for Fold
  Recognition. Backbone Dependent and allows for all atom treatment.
*******************************************************************************/
#include <cstddef> // Needed to use NULL.
#include <fstream>
#include <cstring>
#include <cmath>
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "DopeX.h"
#include "strtokenizer.h"

using namespace std;

#define INF_CHAIN 10000
DopeX::DopeX()
{
	max_atom_code=9;
	n_bins = 60;
	SetDefConfig();
	InitializeMemory(60, EECDis1Energy);
	InitializeEnergy(EECDis1Energy);
	InitializeMemory(60, EECDis2Energy);
	InitializeEnergy(EECDis2Energy);
	InitializeMemory(60, EECDis3Energy);
	InitializeEnergy(EECDis3Energy);
	InitializeMemory(60, EECDis4Energy);
	InitializeEnergy(EECDis4Energy);
	InitializeMemory(60, EECDgt4p_CABpEnergy);
	InitializeEnergy(EECDgt4p_CABpEnergy);
	InitializeMemory(60, EECDgt4p_CABapEnergy);
	InitializeEnergy(EECDgt4p_CABapEnergy);
	InitializeMemory(60, EECDgt4p_CAB0Energy);
	InitializeEnergy(EECDgt4p_CAB0Energy);
	InitializeMemory(60, EECDgt4ap_CABpEnergy);
	InitializeEnergy(EECDgt4ap_CABpEnergy);
	InitializeMemory(60, EECDgt4ap_CABapEnergy);
	InitializeEnergy(EECDgt4ap_CABapEnergy);
	InitializeMemory(60, EECDgt4ap_CAB0Energy);
	InitializeEnergy(EECDgt4ap_CAB0Energy);
	InitializeMemory(60, AllOtherCDgt4_CAB0Energy);
	InitializeEnergy(AllOtherCDgt4_CAB0Energy);
	InitializeMemory(60, AllOtherCDgt4_CABpEnergy);
	InitializeEnergy(AllOtherCDgt4_CABpEnergy);
	InitializeMemory(60, AllOtherCDgt4_CABapEnergy);
	InitializeEnergy(AllOtherCDgt4_CABapEnergy);
	InitializeMemory(60, HHCDis1Energy);
	InitializeEnergy(HHCDis1Energy);
	InitializeMemory(60, HHCDis2Energy);
	InitializeEnergy(HHCDis2Energy);
	InitializeMemory(60, HHCDis3Energy);
	InitializeEnergy(HHCDis3Energy);
	InitializeMemory(60, HHCDis4Energy);
	InitializeEnergy(HHCDis4Energy);
	InitializeMemory(60, AllOtherCDis1Energy);
	InitializeEnergy(AllOtherCDis1Energy);
	InitializeMemory(60, AllOtherCDis2Energy);
	InitializeEnergy(AllOtherCDis2Energy);
	InitializeMemory(60, AllOtherCDis3Energy);
	InitializeEnergy(AllOtherCDis3Energy);
	InitializeMemory(60, AllOtherCDis4Energy);
	InitializeEnergy(AllOtherCDis4Energy);
}

DopeX::~DopeX()
{
	ReleaseMemory(EECDis1Energy);
	ReleaseMemory(EECDis2Energy);
	ReleaseMemory(EECDis3Energy);
	ReleaseMemory(EECDis4Energy);
	ReleaseMemory(EECDgt4p_CABpEnergy);
	ReleaseMemory(EECDgt4p_CABapEnergy);
	ReleaseMemory(EECDgt4p_CAB0Energy);
	ReleaseMemory(EECDgt4ap_CABpEnergy);
	ReleaseMemory(EECDgt4ap_CABapEnergy);
	ReleaseMemory(EECDgt4ap_CAB0Energy);
	ReleaseMemory(AllOtherCDgt4_CAB0Energy);
	ReleaseMemory(AllOtherCDgt4_CABpEnergy);
	ReleaseMemory(AllOtherCDgt4_CABapEnergy);
	ReleaseMemory(HHCDis1Energy);
	ReleaseMemory(HHCDis2Energy);
	ReleaseMemory(HHCDis3Energy);
	ReleaseMemory(HHCDis4Energy);
	ReleaseMemory(AllOtherCDis1Energy);
	ReleaseMemory(AllOtherCDis2Energy);
	ReleaseMemory(AllOtherCDis3Energy);
	ReleaseMemory(AllOtherCDis4Energy);
}

void DopeX::SetDefConfig()
{
    // Setting defaults.
    dist_cutoff = 15.0;
    num_bin = 30;

	EECDis1Energy_par_file = " ";
    EECDis2Energy_par_file = " ";
    EECDis3Energy_par_file = " ";
    EECDis4Energy_par_file = " ";
    EECDgt4p_CABpEnergy_par_file = " ";
    EECDgt4p_CABapEnergy_par_file = " ";
    EECDgt4p_CAB0Energy_par_file = " ";
    EECDgt4ap_CABpEnergy_par_file = " ";
    EECDgt4ap_CABapEnergy_par_file = " ";
    EECDgt4ap_CAB0Energy_par_file = " ";
    AllOtherCDgt4_CAB0Energy_par_file = " ";
    AllOtherCDgt4_CABpEnergy_par_file = " ";
    AllOtherCDgt4_CABapEnergy_par_file = " ";
    HHCDis1Energy_par_file = " ";
    HHCDis2Energy_par_file = " ";
    HHCDis3Energy_par_file = " ";
    HHCDis4Energy_par_file = " ";
    AllOtherCDis1Energy_par_file = " ";
    AllOtherCDis2Energy_par_file = " ";
    AllOtherCDis3Energy_par_file = " ";
    AllOtherCDis4Energy_par_file = " ";

    EECDis1Energy_coefficient = 1.0;
    EECDis2Energy_coefficient = 1.0;
    EECDis3Energy_coefficient = 1.0;
    EECDis4Energy_coefficient = 1.0;
    EECDgt4p_CABpEnergy_coefficient = 1.0;
    EECDgt4p_CABapEnergy_coefficient = 1.0;
    EECDgt4p_CAB0Energy_coefficient = 1.0;
    EECDgt4ap_CABpEnergy_coefficient = 1.0;
    EECDgt4ap_CABapEnergy_coefficient = 1.0;
    EECDgt4ap_CAB0Energy_coefficient = 1.0;
    AllOtherCDgt4_CAB0Energy_coefficient = 1.0;
    AllOtherCDgt4_CABpEnergy_coefficient = 1.0;
    AllOtherCDgt4_CABapEnergy_coefficient = 1.0;
    HHCDis1Energy_coefficient = 1.0;
    HHCDis2Energy_coefficient = 1.0;
    HHCDis3Energy_coefficient = 1.0;
    HHCDis4Energy_coefficient = 1.0;
    AllOtherCDis1Energy_coefficient = 1.0;
    AllOtherCDis2Energy_coefficient = 1.0;
    AllOtherCDis3Energy_coefficient = 1.0;
    AllOtherCDis4Energy_coefficient = 1.0;

    esp_par_file = "";

    dope_par_file = "";
    dope_dir = "";

    add_bonded = false;

    add_hydrogens = false;
    add_centroids = false;
    add_side_chains = true;
    add_only_CB = false;
    CB_CB_focus = false;
    fix_consensus = false;

    analysis = false;

    Burial_renorm = false;
    Penalty_coeff = 0.0;
    Burial_sphere_rad = 7.0;
    Burial_min_atcount = 5;
    Burial_max_atcount = 20;

    min_chain_dist = 1;
    min_CB_CB_dist = 1;
    max_chain_dist = INF_CHAIN;

    energy_coeff = 1.0;
    CB_CB_coefficient = 1.0;

	energy_cap = -100000.0;
}

int DopeX::ReadConfigFile(string cfgName)
{
	cerr << "DopeX::ReadConfigFile("<< cfgName<<")"<<endl;
	ifstream CfgFile;
	CfgFile.open(cfgName.c_str(), ios::in | ios::binary);
	string line, par, val;
	bool recog_line;

	while (true)
	{
		getline(CfgFile, line); trim2(line);

		if ((line != "") && (line[0] != '#'))
		{
			strtokenizer strtokens(line,"=");
			int num=strtokens.count_tokens();
			if (num<2) continue;
			par = strtokens.token(0);
			val = strtokens.token(1);
			trim2(par);
			trim2(val);
			//cerr << par << "=" << val << endl;
			recog_line = false;

			if (par == "EECDis1 parameter file") { EECDis1Energy_par_file = val; recog_line = true; }
			if (par == "EECDis2 parameter file") { EECDis2Energy_par_file = val; recog_line = true; }
			if (par == "EECDis3 parameter file") { EECDis3Energy_par_file = val; recog_line = true; }
			if (par == "EECDis4 parameter file") { EECDis4Energy_par_file = val; recog_line = true; }
			if (par == "EECDgt4p_CABp parameter file"){EECDgt4p_CABpEnergy_par_file = val; recog_line = true; }
			if (par == "EECDgt4p_CABap parameter file"){EECDgt4p_CABapEnergy_par_file = val; recog_line = true; }
			if (par == "EECDgt4p_CAB0 parameter file") { EECDgt4p_CAB0Energy_par_file = val; recog_line = true; }
			if (par == "EECDgt4ap_CABp parameter file"){EECDgt4ap_CABpEnergy_par_file = val; recog_line = true; }
			if (par == "EECDgt4ap_CABap parameter file") { EECDgt4ap_CABapEnergy_par_file = val; recog_line = true; }
			if (par == "EECDgt4ap_CAB0 parameter file"){EECDgt4ap_CAB0Energy_par_file = val; recog_line = true; }
			if (par == "HHCDis1 parameter file") { HHCDis1Energy_par_file = val; recog_line = true; }
			if (par == "HHCDis2 parameter file") { HHCDis2Energy_par_file = val; recog_line = true; }
			if (par == "HHCDis3 parameter file") { HHCDis3Energy_par_file = val; recog_line = true; }
			if (par == "HHCDis4 parameter file") { HHCDis4Energy_par_file = val; recog_line = true; }
			if (par == "AllOtherCDis1 parameter file") { AllOtherCDis1Energy_par_file = val; recog_line = true; }
			if (par == "AllOtherCDis2 parameter file") { AllOtherCDis2Energy_par_file = val; recog_line = true; }
			if (par == "AllOtherCDis3 parameter file") { AllOtherCDis3Energy_par_file = val; recog_line = true; }
			if (par == "AllOtherCDis4 parameter file") { AllOtherCDis4Energy_par_file = val; recog_line = true; }
			if (par == "AllOtherCDgt4_CABp parameter file"){AllOtherCDgt4_CABpEnergy_par_file = val; recog_line = true; }
			if (par == "AllOtherCDgt4_CABap parameter file"){AllOtherCDgt4_CABapEnergy_par_file = val; recog_line = true; }
			if (par == "AllOtherCDgt4_CAB0 parameter file") { AllOtherCDgt4_CAB0Energy_par_file = val; recog_line = true; }
			if (par == "EECDis1 coefficient"){EECDis1Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "EECDis2 coefficient"){EECDis2Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "EECDis3 coefficient"){EECDis3Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "EECDis4 coefficient"){EECDis4Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "EECDgt4p_CABp coefficient"){EECDgt4p_CABpEnergy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "EECDgt4p_CABap coefficient"){EECDgt4p_CABapEnergy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "EECDgt4p_CAB0 coefficient"){EECDgt4p_CAB0Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "EECDgt4ap_CABp coefficient"){EECDgt4ap_CABpEnergy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "EECDgt4ap_CABap coefficient"){EECDgt4ap_CABapEnergy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "EECDgt4ap_CAB0 coefficient"){EECDgt4ap_CAB0Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "HHCDis1 coefficient"){HHCDis1Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "HHCDis2 coefficient"){HHCDis2Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "HHCDis3 coefficient"){HHCDis3Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "HHCDis4 coefficient"){HHCDis4Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "AllOtherCDis1 coefficient") { AllOtherCDis1Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "AllOtherCDis2 coefficient") { AllOtherCDis2Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "AllOtherCDis3 coefficient") { AllOtherCDis3Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "AllOtherCDis4 coefficient") { AllOtherCDis4Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "AllOtherCDgt4_CABp coefficient") { AllOtherCDgt4_CABpEnergy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "AllOtherCDgt4_CABap coefficient") { AllOtherCDgt4_CABapEnergy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "AllOtherCDgt4_CAB0 coefficient") { AllOtherCDgt4_CAB0Energy_coefficient = atof(val.c_str()); recog_line = true; }
			if (par == "ESP file") { esp_par_file = val; recog_line = true; }

			if (par == "BURIAL RENORMALIZATION") { Burial_renorm = (val=="yes"); recog_line = true; }
			if (par == "PENALTY COEFFICIENT") { Penalty_coeff = atof(val.c_str()); recog_line = true; }
			if (par == "BURIAL SPHERE RADIUS") { Burial_sphere_rad = atof(val.c_str()); recog_line = true; }
			if (par == "BURIAL MIN ATOM COUNT") { Burial_min_atcount = atoi(val.c_str()); recog_line = true; }
			if (par == "BURIAL MAX ATOM COUNT") { Burial_max_atcount = atoi(val.c_str()); recog_line = true; }

			if (par == "ANALYSIS") { analysis = (val=="yes"); recog_line = true; }

			if (par == "Distance cutoff") { dist_cutoff = atof(val.c_str()); recog_line = true; }

			if (par == "Number of bins")
			{
				num_bin = atoi(val.c_str());
				Resize(num_bin, EECDis1Energy);
				Resize(num_bin, EECDis2Energy);
				Resize(num_bin, EECDis3Energy);
				Resize(num_bin, EECDis4Energy);
				Resize(num_bin, EECDgt4p_CABpEnergy);
				Resize(num_bin, EECDgt4p_CABapEnergy);
				Resize(num_bin, EECDgt4p_CAB0Energy);
				Resize(num_bin, EECDgt4ap_CABpEnergy);
				Resize(num_bin, EECDgt4ap_CABapEnergy);
				Resize(num_bin, EECDgt4ap_CAB0Energy);
				Resize(num_bin, AllOtherCDgt4_CAB0Energy);
				Resize(num_bin, AllOtherCDgt4_CABpEnergy);
				Resize(num_bin, AllOtherCDgt4_CABapEnergy);
				Resize(num_bin, HHCDis1Energy);
				Resize(num_bin, HHCDis2Energy);
				Resize(num_bin, HHCDis3Energy);
				Resize(num_bin, HHCDis4Energy);
				Resize(num_bin, AllOtherCDis1Energy);
				Resize(num_bin, AllOtherCDis2Energy);
				Resize(num_bin, AllOtherCDis3Energy);
				Resize(num_bin, AllOtherCDis4Energy);
				recog_line = true;
			}
			if (par == "Parameter file") { dope_par_file = val; recog_line = true; }

			if (par == "Add bonded atoms") { add_bonded = (val=="yes"); recog_line = true; }
	
			if (par == "Add hydrogen atoms") { add_hydrogens = (val=="yes"); recog_line = true; }
			if (par == "Add side-chain centroids") { add_centroids = (val=="yes"); recog_line = true; }
			if (par == "Add side-chain atoms") { add_side_chains = (val=="yes"); recog_line = true; }
			if (par == "Add beta-carbons only") { add_only_CB = (val=="yes"); recog_line = true; }
			if (par == "Minimize CB_CB only") { CB_CB_focus = (val=="yes"); recog_line = true; }
			if (par == "FIX CONSENSUS") { fix_consensus = (val=="yes"); recog_line = true; }
			if (par == "DOPE FIX DIR") { dope_dir = val; recog_line = true; }

			if (par == "Minimum chain distance") { min_chain_dist = atoi(val.c_str()); recog_line = true; }
			if (par == "Maximum chain distance") { max_chain_dist = atoi(val.c_str()); recog_line = true; }

			if (par == "Minimum CB_CB distance") { min_CB_CB_dist = atoi(val.c_str()); recog_line = true; }

			if (par == "Energy coefficient") { energy_coeff = atof(val.c_str()); recog_line = true; }
			if (par == "CB_CB coefficient") { CB_CB_coefficient = atof(val.c_str()); recog_line = true; }
		}

		if (CfgFile.eof()) break;
	}
	CfgFile.close();
	return 0;
}

void DopeX::Init(string cfgFile)
{
	ReadConfigFile(cfgFile);
	//SetAddAtoms(add_hydrogens, add_centroids, add_side_chains, add_only_CB);
	//LoadFromFile(dope_par_file);
	cerr << "DopeX::Init max_atom_code="<<max_atom_code<< " n_bins="<<n_bins<<" num_bin="<<num_bin<<endl;
	LoadFromFile(EECDis1Energy_par_file, EECDis1Energy);
	LoadFromFile(EECDis2Energy_par_file, EECDis2Energy);
	LoadFromFile(EECDis3Energy_par_file, EECDis3Energy);
	LoadFromFile(EECDis4Energy_par_file, EECDis4Energy);
	LoadFromFile(HHCDis1Energy_par_file, HHCDis1Energy);
	LoadFromFile(HHCDis2Energy_par_file, HHCDis2Energy);
	LoadFromFile(HHCDis3Energy_par_file, HHCDis3Energy);
	LoadFromFile(HHCDis4Energy_par_file, HHCDis4Energy);
	LoadFromFile(AllOtherCDis1Energy_par_file, AllOtherCDis1Energy);
	LoadFromFile(AllOtherCDis2Energy_par_file, AllOtherCDis2Energy);
	LoadFromFile(AllOtherCDis3Energy_par_file, AllOtherCDis3Energy);
	LoadFromFile(AllOtherCDis4Energy_par_file, AllOtherCDis4Energy);
	LoadFromFile(EECDgt4p_CABpEnergy_par_file, EECDgt4p_CABpEnergy);
	LoadFromFile(EECDgt4p_CABapEnergy_par_file, EECDgt4p_CABapEnergy);
	LoadFromFile(EECDgt4p_CAB0Energy_par_file, EECDgt4p_CAB0Energy);
	LoadFromFile(EECDgt4ap_CABpEnergy_par_file, EECDgt4ap_CABpEnergy);
	LoadFromFile(EECDgt4ap_CABapEnergy_par_file, EECDgt4ap_CABapEnergy);
	LoadFromFile(EECDgt4ap_CAB0Energy_par_file, EECDgt4ap_CAB0Energy);
	LoadFromFile(AllOtherCDgt4_CABpEnergy_par_file, AllOtherCDgt4_CABpEnergy);
	LoadFromFile(AllOtherCDgt4_CABapEnergy_par_file, AllOtherCDgt4_CABapEnergy);
	LoadFromFile(AllOtherCDgt4_CAB0Energy_par_file, AllOtherCDgt4_CAB0Energy);

	memset(ESP_energy, 0, sizeof(int)*3*(20+1));
	string e0_str, e1_str, e2_str;
	int n=0;
	int e0, e1, e2;
	ifstream par_file;
	par_file.open(esp_par_file.c_str(), ios::in);
	cerr << "start loading esp.par files: "<< esp_par_file << endl;
	while (!par_file.eof()&&par_file.is_open())
	{
		par_file >> e0_str;
		par_file >> e1_str;
		par_file >> e2_str;
		//cerr << e0_str << " " << e1_str << " " << e2_str << endl;
		e0 = atoi(e0_str.c_str());
		e1 = atoi(e1_str.c_str());
		e2 = atoi(e2_str.c_str());
		ESP_energy[3*n]=e0;
		ESP_energy[3*n+1]=e1;
		ESP_energy[3*n+2]=e2;
		n++;
	}
	par_file.close();
}

double DopeX::DOPEenergy(int aa1, int at1, int aa2, int at2, int bin, int cd, int od, int pap, int ss1, int ss1_1, int ss1_2, int ss1_3, int ss2)
{
	double e_pair=0.0, coef=0.0;
	if (-1 < bin && bin < n_bins)
	{
		int idx = bin+n_bins*(at2+(max_atom_code+1)*(aa2+20*(at1+(max_atom_code+1)*aa1)));//[aa1][at1][aa2][at2][bin];
		//cerr << idx << ";";
		if (cd <= 4)
		{
			switch (cd)
			{
			case 1:
				return 0.00;
				//if ((ss1 == 1) && (ss2 == 1)) return EECDis1Energy[aa1][at1][aa2][at2][bin];
				//else if ((ss1 == 0) && (ss2 == 0)) return HHCDis1Energy[aa1][at1][aa2][at2][bin];
				//else return AllOtherCDis1Energy[aa1][at1][aa2][at2][bin];
			case 2:
				if ((ss1 == 1) && (ss1_1 == 1) && (ss2 == 1))
				{
					//cerr << "EECDis2 " << bin <<". ";
					e_pair = EECDis2Energy[idx];
					coef = EECDis2Energy_coefficient;
				}
				else if ((ss1 == 0) && (ss1_1 == 0) && (ss2 == 0))
				{
					//cerr << "HHCDis2 " << bin <<". ";
					e_pair = HHCDis2Energy[idx];
					coef = HHCDis2Energy_coefficient;
				}
				else
				{
					//cerr << "AllOtherCDis2 " << bin <<". ";
					e_pair = AllOtherCDis2Energy[idx];
					coef = AllOtherCDis2Energy_coefficient;
				}
				break;
			case 3:
				if ((ss1 == 1) && (ss1_1 == 1) && (ss1_2 == 1) && (ss2 == 1))
				{
					//cerr << "EECDis3 " << bin <<". ";
					e_pair = EECDis3Energy[idx];
					coef = EECDis3Energy_coefficient;
				}
				else if ((ss1 == 0) && (ss1_1 == 0) && (ss1_2 == 0) && (ss2 == 0))
				{
					//cerr << "HHCDis3 " << bin <<". ";
					e_pair = HHCDis3Energy[idx];
					coef = HHCDis3Energy_coefficient;
				}
				else
				{
					//cerr << "AllOtherCDis3 " << bin <<". ";
					e_pair = AllOtherCDis3Energy[idx];
					coef = AllOtherCDis3Energy_coefficient;
				}
				break;
			case 4:
				if ((ss1 == 1) && (ss1_1 == 1) && (ss1_2 == 1) && (ss1_3 == 1) && (ss2 == 1))
				{
					//cerr << "EECDis4 " << bin <<". ";
					e_pair = EECDis4Energy[idx];
					coef = EECDis4Energy_coefficient;
				}
				else if ((ss1 == 0) && (ss1_1 == 0) && (ss1_2 == 0) && (ss1_3 == 0) && (ss2 == 0))
				{
					//cerr << "HHCDis4 " << bin <<". ";
					e_pair = HHCDis4Energy[idx];
					coef = HHCDis4Energy_coefficient;
				}
				else
				{
					//cerr << "AllOtherCDis4 " << bin <<". ";
					e_pair = AllOtherCDis4Energy[idx];
					coef = AllOtherCDis4Energy_coefficient;
				}
				break;
			}
		}
		else
		{
			if ((ss1 == 1) && (ss2 == 1) && (pap == 1))
			{
				switch (od)
				{
				case 0:
					//cerr << "EECDgt4p_CAB0 " << bin <<". ";
					e_pair = EECDgt4p_CAB0Energy[idx];
					coef = EECDgt4p_CAB0Energy_coefficient;
					break;
				case 1:
					//cerr << "EECDgt4p_CABp " << bin <<". ";
					e_pair = EECDgt4p_CABpEnergy[idx];
					coef = EECDgt4p_CABpEnergy_coefficient;
					break;
				case 2:
					//cerr << "EECDgt4p_CABap" << bin <<". ";
					e_pair = EECDgt4p_CABapEnergy[idx];
					coef = EECDgt4p_CABapEnergy_coefficient;
					break;
				}
			}
			else if ((ss1 == 1) && (ss2 == 1) && (pap == 2))
			{
				//cerr << "EECDgt4ap_ " << bin <<". ";
				switch (od)
				{
				case 0:
					e_pair = EECDgt4ap_CAB0Energy[idx];
					coef = EECDgt4ap_CAB0Energy_coefficient;
					break;
				case 1:
					e_pair = EECDgt4ap_CABpEnergy[idx];
					coef = EECDgt4ap_CABpEnergy_coefficient;
					break;
				case 2:
					e_pair = EECDgt4ap_CABapEnergy[idx];
					coef = EECDgt4ap_CABapEnergy_coefficient;
					break;
				}
			}
			else if ((ss1 == 1) && (ss2 == 1) && (pap == 0)) 
				return 0.0;
			else if (!((ss1 == 1) && (ss2 == 1)))
			{
				//cerr << "AllOtherCDgt4ap_ " << bin <<". ";
				switch (od)
				{
				case 0:
					e_pair = AllOtherCDgt4_CAB0Energy[idx];
					coef = AllOtherCDgt4_CAB0Energy_coefficient;
					break;
				case 1:
					e_pair = AllOtherCDgt4_CABpEnergy[idx];
					coef = AllOtherCDgt4_CABpEnergy_coefficient;
					break;
				case 2:
					e_pair = AllOtherCDgt4_CABapEnergy[idx];
					coef = AllOtherCDgt4_CABapEnergy_coefficient;
					break;
				}
			}
		}
		if (e_pair > 0.0) 
		//if (e_pair < 0.0) 
			return e_pair;
		else
			return coef * e_pair;
	}
	else return 0.0;
}

// Search the DopeX table to get the energy for 2 atoms
//double DopeX::calcAtomDOPE(string residue_id1, string atom1, string residue_id2, string atom2, double dist, int cd, int od, int pap, char ss1, char ss1_1, char ss1_2, char ss1_3, char ss2) 
double DopeX::calcAtomDOPE(string residue_id1, string atom1, string residue_id2, string atom2, double dist, int cd, int od, int pap, int ss1, int ss1_1, int ss1_2, int ss1_3, int ss2) 
{
	//int nss1 = SSNCode(ss1);
	//int nss1_1 = SSNCode(ss1_1);
	//int nss1_2 = SSNCode(ss1_2);
	//int nss1_3 = SSNCode(ss1_3);
	//int nss2 = SSNCode(ss2);
	double e_pair = 0;

	if (0.0<dist && dist<dist_cutoff && cd >1)
	{
		int aa0 = GetResidueCode(residue_id1, 0);
		int at0 = GetAtomCode(atom1, 0);
		int aa1 = GetResidueCode(residue_id2, 0);
		int at1 = GetAtomCode(atom2, 0);
		if (aa0==-1 || aa1==-1|| at0==-1|| at1==-1)
			return 0;    
		// The distance lies between middle points of bins ri and ri + 1.
		double bin_size = dist_cutoff / num_bin;
		double r1 = dist / bin_size - 0.5;
		int ri = int(r1);
		
		// Linear interpolation between the energy value at those bins.
		double f1 = r1 - ri;
		double f2 = 1.0 - f1;
		//cerr <<"DopeX("<<residue_id1<<" "<<atom1<<" "<<residue_id2<<" "<<atom2<<","<<","<<od<<","<<pap<<","<<nss1<<","<<nss1_1<<","<<nss1_2<<","<<nss1_3<<","<<nss2<<")";
		if (0 < ri)
		{
			if (ri>=n_bins)
				return 0;
			// When ri = n_bins - 1, StatPot(ri + 1) gives 0, so we are interpolating
			// between the last energy value stored in the table and 0, which is ok.
			//double e1 = Energy[aa0][at0][aa1][at1][ri];
			//double e1 = DOPEenergy(aa0, at0, aa1, at1, ri, cd, od, pap, nss1, nss1_1, nss1_2, nss1_3, nss2);
			double e1 = DOPEenergy(aa0, at0, aa1, at1, ri, cd, od, pap, ss1, ss1_1, ss1_2, ss1_3, ss2);
			double e2=0;
			if (ri+1<n_bins)
			{
				//e2 = Energy[aa0][at0][aa1][at1][ri+1];
				//e2 = DOPEenergy(aa0, at0, aa1, at1, ri+1, cd, od, pap, nss1, nss1_1, nss1_2, nss1_3, nss2);
				e2 = DOPEenergy(aa0, at0, aa1, at1, ri+1, cd, od, pap, ss1, ss1_1, ss1_2, ss1_3, ss2);
			}
			e_pair = f2 * e1 + f1 * e2;
			//cerr <<"ri="<<ri<<"; f1="<<f1<<"; e1="<<e1<<"; e2="<<e2<<endl;
		}
		else
		{
			// e_pair = Energy[aa0][at0][aa1][at1][0];
			//e_pair = DOPEenergy(aa0, at0, aa1, at1, 0, cd, od, pap, nss1, nss1_1, nss1_2, nss1_3, nss2);
			e_pair = DOPEenergy(aa0, at0, aa1, at1, 0, cd, od, pap, ss1, ss1_1, ss1_2, ss1_3, ss2);
		}
		//if (e_pair!=0)
		//	cerr <<"; e_pair="<<e_pair<<endl;
	}

	//cerr <<"; e_pair="<<e_pair<<endl;
	return e_pair;
}
