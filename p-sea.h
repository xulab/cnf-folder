#ifndef _P_SEA
#define _P_SEA

#include "SimpPDB.h"


#define NO_ANGLE	500	/* invalid angle				*/
#define NO_CHAIN	' '	/* ' ' == pas de chaines		*/
#define NO_INSER	' '	/* ' ' == pas d'insertion		*/

#define ATOMNAME	6	/* length + 1 of atom name		*/
#define RESNAME		4	/* length + 1 of residue name	*/

						/* maximum distance for two		*/
						/* residues to be neighbours 	*/
#define DIST_MAX_BETWEEN_NEIGHBOUR_RESIDUE  5
#define MAX_NEIGHBOURS	10	/* maximum neighbour for a residue	*/


/* -------------------------------------------------------------------- */
/* structures															*/
/* -------------------------------------------------------------------- */

typedef struct {			/* -- atome (long desc)		*/
	float		x, y, z;	/* cartesian coordinates	*/
	float		localize;	/* occupancy			*/
	float		tfactor;	/* temperature factor		*/
	short		num;		/* atom number in pdb file	*/
	short		ires;		/* numero du residu		*/
	char		name[ATOMNAME];	/* atom name			*/
	char		resn[RESNAME];	/* atom residue name		*/
	char		chain;		/* chaine de la proteine	*/
	char		inser;		/* residu insere		*/
} Atom;

typedef struct _resAtom {		/* -- atom (short desc)		*/
	float		x, y, z;	/* cartesian coordinates	*/
	struct _resAtom	*next;		/* next in list			*/
	struct _resAtom	*prev;		/* next in list			*/
	char		name[ATOMNAME];	/* atom name			*/
} ResAtom;

typedef struct _residu{				/* -- residue 					*/
	ResAtom		*first_atom;			/* first atom in residue		*/
	ResAtom		*last_atom;			/* last atom in residue			*/
	float		phi, psi, ome;			/* phi, psi & omega dihed.		*/
	float		alpha, tau;			/* alpha, tau (Levitt 1976 )		*/
	float		x1, x2, x3;			/* chi1, ch2 & chi3 dihed.		*/
	float		x, y, z;			/* geometric center			*/
	float           dis15;          		/* distance between CA-1 & CA+3		*/
	float           dis14;          		/* distance between CA-1 & CA+2		*/
	float           dis13;          		/* distance between CA-1 & CA+1		*/
	float           dis12;          		/* distance between CA & CA+1		*/

	struct _residu	*neighbour[MAX_NEIGHBOURS]; 		/* array of neighbours		*/
	struct _residu	*prev;					/* previous in list		*/
	struct _residu	*next;					/* next in list			*/
/*	struct _residu	*nhbonded;		*/		/* nh-cobonded neighbours	*/
/*	struct _residu	*cobonded;		*/		/* co-nhbonded neighbours	*/

	short		nb_neighbours;	/* number of neighbours		*/
	short		num;		/* number in the whole molecule	*/
	short		pdbnum;		/* number in the pdb file	*/
	short		cut;		/* if previous residu is far	*/
	short		nrel;		/* relative number in a segment	*/
	short		dummy;		/* for structure alignment	*/
	short		hydro;		/* hydrophobicity		*/
	short		nb_ca;		/* nb calpha voisins		*/
	short		mer;		/* oligomere			*/

	char		name[RESNAME];	/* residue name	(3 letters)		*/
	char		code;		/* residue name	(1 letter)		*/
	char		cons;		/* structure secondaire du residu 	*/
	char		chain;		/* chaine de la proteine		*/
	char		inser;		/* residu insere			*/
	char		ca;		/* residu insere			*/
} Residu;

/* -------------------------------------------------------------------- */
/* prototypes           												*/
/* -------------------------------------------------------------------- */

void	myerror(char *msg, int status);
int	set_residue_geocenter(Residu *r);
int	set_residue_neighbours(Residu *res, Residu *first);
float	get_dist_max_from_geocenter(Residu *r);
Residu	*readPdbResidues(FILE *f);
Residu	*buildResidue(SimPDB*f);
Residu  *new_residu();
ResAtom	*put_atom_in_res(Atom *atom, Residu *res);
int	get_phi_psi(Residu *r);
int	get_chi(Residu *r);
int	get_alpha(Residu *r);
int     get_distca_1_3( Residu *r);
int     get_distca_1_2( Residu *r);
int     get_distca_1_5( Residu *r);
int	get_alpha_tau(Residu *r);

void setCoord(Residu* res, double * stru, int len);
void setCoord(Residu* res, double ** stru, int len);

char     analyse_dist( Residu *r);


#endif
