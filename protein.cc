#include "protein.h"

int DEBUG = 2;
const double PI=acos(-1.), radian=180.0/PI;
ZhouProtein::ZhouProtein(const string fn){
	pdbnm = fn;
	FILE *fp=openfile(fn, "r");
	rdpdb(fp); fclose(fp);
	aatpl=NULL;
}
bool ZhouProtein::rdpdb(FILE *fp){
	char str[121], st[10];
	string line, s0, s1;
	int ires, oires, chain0=0;
//
	if(PRtype.size()==0) initPRtypes(0);
//
	oires=-9999;
	natm = nres = 0;
	bool bres = false;
	while(fgets(str,120,fp) != NULL){
		line = str;
		if(line.substr(0,3) == "END") break;
		if(line.substr(0,3) == "TER") chain0++;
		if(line.substr(0,4) != "ATOM") continue;
		if(line.at(12)=='H' || line.at(13)=='H') {pdbidx.push_back(-1); continue;}

		s1 = line.substr(17,10); ires=strtol(str+22,NULL,10); sscanf(str+22, "%s", st);
		if(s0!=s1 || oires!=ires){
			if(DEBUG>0 && ires-oires!=1 && oires!=-9999 && !ichain.empty() && chain0==ichain.back()){
				fprintf(stderr, "Warning, Residue seqens not adjacent: %d %d\n", oires, ires);
			}
			bres = addRes( line.substr(17,3) );
			if(bres){
				seq0.push_back(ires); seq0_str.push_back(st);
				ichain.push_back(chain0);
				int cid = str[21]; if(cid == ' ') cid = '0';
				chainID.push_back(cid);
			}
			s0 = s1; oires = ires;
		}
		if(! bres) pdbidx.push_back(-1);
		else {
			if(! initAtom(line)) pdbidx.push_back(-1);
		}
	}
	return true;
}
// require to call "initPRtypes" first
bool ZhouProtein::addRes(string rn){
	int rid = resDefine(rn);
	if(rid<0) return false;
	resid.push_back(rid);
	rnam.push_back(rn);
	if(rid >= 20) rid = resDefine("ALA"); 			// treat hetro residue as ALA
	vector <Atomtype> *ap = PRtype.at(rid).getatypes();
	for(int i=0; i<(int)ap->size(); i++){
		string an = ap->at(i).getname();
		int it = ap->at(i).gettype();
		atoms.push_back(ZhouAtom(natm, it, an));
		rseq.push_back(nres);
		natm ++;
	}
	if(nres == 0) rstart.push_back(0);
	rstart.push_back(natm);
	nres ++;
	return true;
}
bool ZhouProtein::addRes(string resname, int cid, int chain0, int ires)
{
	if(PRtype.size()==0) {
		initPRtypes(0);
		natm = nres = 0;
	}
	bool bres = addRes(resname);
	if(bres){
		seq0.push_back(ires); 
		char tmp[10];
		sprintf(tmp, "%d", ires);
		seq0_str.push_back(string(tmp));
		ichain.push_back(chain0);
		chainID.push_back(cid);
		//cerr<< ires << "::" << chain0 << "::" << cid << endl;
	} else {
		cerr << "ERROR: fail to addres " << resname << ires << endl;
		pdbidx.push_back(-1);
	}
	return true;
}

bool ZhouProtein::initAtom(string atomname, double x, double y, double z)
{
	//if(PRtype.size()==0) initPRtypes(0);
	if(atomname == "OXT" || atomname == "SG" || atomname == "HN")
		return false;
	else if(atomname=="CD" && rnam.at(nres-1)=="ILE") 
		atomname="CD1";
	for(int i=rstart[nres-1]; i<rstart[nres]; i++) {
		if(atomname == atoms[i].getname()) {
			atoms[i].setx(x,y,z);
			pdbidx.push_back(i);
			return true;
		}
	}
	//if(DEBUG>0)
	//	fprintf(stderr, "Warning, inconsistent atoms: %s %s\n", rnam[nres-1].c_str(), atomname.c_str());
	return false;
}

bool ZhouProtein::initAtom(string line){
	string an = trimstr(line.substr(13,3)), an1=line.substr(12,1), an2=line.substr(16,1);
	if(an.substr(0,1) == "H"){
		if(an1=="1" || an1=="2" || an1=="3") an += an1;
		else if(an2=="1" || an2=="2" || an2=="3") an += an2;
	}
	if(an == "OXT") return false;
	else if(an=="CD" && rnam.at(nres-1)=="ILE") an="CD1";
	for(int i=rstart[nres-1]; i<rstart[nres]; i++){
		if(an == atoms[i].getname()){
			atoms[i].setx(line.substr(30, 24));
			pdbidx.push_back(i);
			return true;
		}
	}
	if(DEBUG>0)
		fprintf(stderr, "Warning, inconsistent atoms: %s %s\n", rnam[nres-1].c_str(), an.c_str());
	return false;
}
int ZhouProtein::findatom(int ir, string an){
	if(ir>=nres || ir<0) return -1;
	for(int i=rstart.at(ir); i<rstart.at(ir+1); i++){
		if(atoms.at(i).getname() == an) return i;
	}
	fprintf(stderr, "Fail to find atom: %d, %s\n", ir, an.c_str());
	return -1;
}
void ZhouProtein::wrpdb(string fn){
	FILE *fp = openfile(fn, "w");
	wrpdb(fp); fclose(fp);
}
void ZhouProtein::wrpdb(FILE *fp){
	for(int i=0; i<(int)atoms.size(); i++){
		string fmt1("ATOM%7d  %-4s%-4sA%4d    %8.3f%8.3f%8.3f\n");
		ZhouAtom *ap = &atoms.at(i);
		if(! ap->filled()) continue;
		int ir = rseq.at(i); double *x=ap->getx();
		fprintf(fp, fmt1.c_str(), i+1, ap->getname().c_str(), rnam.at(ir).c_str(), ir+1,
							*x, x[1], x[2]);
	}
}
void ZhouProtein::xdiff(int ia, int ib, double *xd){
	void xdiff(const double *xa, const double *xb, double *xab);
	xdiff(atoms[ia].getx(), atoms[ib].getx(), xd);
}
double ZhouProtein::distance2(int ia, int ib){
	double distance2(const double *xa, const double *xb);
	return distance2(atoms[ia].getx(), atoms[ib].getx());
}
double ZhouProtein::angle(int ia, int ib, int ic){
	double angle(const double *xa, const double *xb, const double *xc);
	return angle(atoms[ia].getx(), atoms[ib].getx(), atoms[ic].getx());
}
double ZhouProtein::torsion(int ia, int ib, int ic, int id){
	double torsion(const double *xa, const double *xb, const double *xc, const double *xd);
	return torsion(atoms[ia].getx(), atoms[ib].getx(), atoms[ic].getx(), atoms[id].getx());
}
bool ZhouProtein::iscomplete(){
	bool bcomp=true;
	for(int ir=0; ir<nres; ir++){
		int id = resid.at(ir);
		if(id < 0){
			if(ir!=0 && ir!=nres-1) bcomp = false;
			fprintf(stderr, "Error residue type: %s %d", rnam[ir].c_str(), ir);
			continue;
		}
		Restype *rp = &PRtype.at(id);
		vector <Atomtype> *ap=rp->getatypes();
		for(int i=0; i<(int)ap->size(); i++){
			bool finish=false;
			string at = ap->at(i).getname();
			for(int ia=rstart[ir]; ia<rstart[ir+1]; ia++){
				if(atoms.at(ia).getname() == at) {finish=true; break;}
			}
			if(! finish){
				if(ir!=0 && ir!=nres-1) bcomp = false;
				fprintf(stderr, "Lost atoms: %s %d %s", rnam[ir].c_str(), seq0[ir], at.c_str());
			}
		}
	}
	return bcomp;
}
void ZhouProtein::initneib(){
	//cerr <<  "ZhouProtein::initneib() " << natm << ", " << nres << endl;
	for(int i=0; i<natm; i++) atoms[i].clearNeib();
	for(int ir=0; ir<nres; ir++){
		int id = resid.at(ir);
		if(id < 0) continue;
		if(ir>0) {
			int i1=rstart[ir-1]+2, i2=rstart[ir];
			atoms.at(i2).addneib(i1);		//N --- C
			atoms.at(i1).addneib(i2);
		}
//
		Restype *rp = &PRtype.at(id);
		vector <string> *neib = rp->getneibs();
		for(int i=0; i<(int)neib->size(); i++){
			int i1=-1, i2=-1;
			for(int ia=rstart[ir]; ia<rstart[ir+1]; ia++){
				if(atoms.at(ia).getname() == neib[0].at(i)) i1=ia;
				else if(atoms.at(ia).getname() == neib[1].at(i)) i2=ia;
				if(i1>=0 && i2>=0) break;
			}
			atoms.at(i1).addneib(i2);
			atoms.at(i2).addneib(i1);
		}
	}
}
// Att! aseq afterwards not changed
void ZhouProtein::eraseAtoms(int idx, int na){
	if(idx<=0 || na<=0) die("Error erase: %d %d\n", idx, na);
	for(int ir=rseq[idx]+1; ir<=nres; ir++) rstart[ir] -= na;
	rseq.erase(rseq.begin()+idx, rseq.begin()+idx+na);
	atoms.erase(atoms.begin()+idx, atoms.begin()+idx+na);
	natm -= na;
}
void ZhouProtein::insertAtoms(int idx, int na){
	if(idx<=0 || na<=0) die("Error insert: %d %d\n", idx, na);	// in front
	for(int ir=rseq[idx-1]+1; ir<=nres; ir++) rstart[ir] += na;
	rseq.insert(rseq.begin()+idx, na, rseq[idx-1]);
	atoms.insert(atoms.begin()+idx, na, ZhouAtom());
	natm += na;
}
void ZhouProtein::initAtomint(int i0){
	int ir = rseq[i0];
	vector<int> *np0 = atoms[i0].getineib();
	int ia, ib, ic, chiral;
	ia = ib = ic = -1; chiral=0;
	string rn = rnam[ir];
	if(np0->at(0) < i0) {
		ia = np0->at(0);
// in PRO, to replace N
		if(rn=="PRO" && atoms[i0].getname()=="CD") ia = findatom(ir, "CG");

		vector<int> *np1 = atoms[ia].getineib();
		if(np1->at(0) < i0) {
			ib = np1->at(0);
			if(np1->at(1) < i0) {ic = np1->at(1); chiral = 1;}
			else {
				vector<int> *np2 = atoms[ib].getineib();
				ic = np2->at(0);
				if(ic == ia) {
					if(np2->size()>1 && np2->at(1)<ia) ic = np2->at(1);
					else ic = -1;
				}
			}
		}
	}
	int iz[] = {ia, ib, ic, chiral};
	atoms[i0].setint(iz);
}
void ZhouProtein::initAtomzcrd0(int i0){
	int *iz = atoms[i0].getint();
	int ia=iz[0], ib=iz[1], ic=iz[2], chiral=iz[3];
	if(aatpl == NULL) aatpl = new Protpl();
//
	string rn = rnam[ rseq[i0] ];
	int idx[4]; double ag[3];
	for(int i=0; i<4; i++) idx[i] = -1;
	if(ic<0 || rseq[ic]==rseq[i0]) {
		idx[0] = aatpl->findatom_tpl(rn, atoms[i0].getname());
		if(ia >= 0) idx[1] = aatpl->findatom_tpl(rn, atoms[ia].getname());
		if(ib >= 0) idx[2] = aatpl->findatom_tpl(rn, atoms[ib].getname());
		if(ic >= 0) idx[3] = aatpl->findatom_tpl(rn, atoms[ic].getname());
//
	} else if (rseq[ic] != rseq[i0]) {		// main-chain atoms
		string rn1 = "ALA", rn2 = "CYS";		// adjacent residues in template
		idx[0] = aatpl->findatom_tpl(rn2, atoms[i0].getname());
		if(rseq[ia] == rseq[i0]) idx[1] = aatpl->findatom_tpl(rn2, atoms[ia].getname());
		else idx[1] = aatpl->findatom_tpl(rn1, atoms[ia].getname());
		if(rseq[ib] == rseq[i0]) idx[2] = aatpl->findatom_tpl(rn2, atoms[ib].getname());
		else idx[2] = aatpl->findatom_tpl(rn1, atoms[ib].getname());
		idx[3] = aatpl->findatom_tpl(rn1, atoms[ic].getname());
	}
	aatpl->calint_tpl(idx, chiral, ag);
//
	if(ia < 0) chiral = 10;
	else if(ib < 0) chiral = 11;
	else if(ic < 0) chiral = 12;
	atoms[i0].setzcrd(ag);
}
void ZhouProtein::calAtomzcrd(int ia){
	int *iz = atoms[ia].getint();
	double *ag = atoms[ia].getzcrd();
	if(iz[0] < 0) return;
	ag[0] = sqrt( distance2(ia, iz[0]) );
	if(iz[1] < 0) return;
	ag[1] = angle(ia, iz[0], iz[1]);
	if(iz[2] < 0) return;
	ag[2] = torsion(ia, iz[0], iz[1], iz[2]);
	if(iz[3] != 0) {
		iz[3] = 1;
		if( ag[2] > 0) iz[3] = -1;
		ag[2] = angle(ia, iz[0], iz[2]);
	}
}
void ZhouProtein::addCB_GLY(int ir){
	if(rnam[ir] != "GLY") die("not GLY: %d %s\n", ir, rnam[ir].c_str()); 
	int ib = findatom(ir, "O") + 1;
	insertAtoms(ib, 1);
	atoms[ib].setname("CB", -1);
//
	if(aatpl == NULL) aatpl = new Protpl();
	string rn="ALA"; int idx[4], chiral; double ag[4];
	idx[0] = aatpl->findatom_tpl(rn, "CB"); idx[1] = aatpl->findatom_tpl(rn, "CA");
	idx[2] = aatpl->findatom_tpl(rn, "N"); idx[3] = aatpl->findatom_tpl(rn, "C");
	aatpl->calint_tpl(idx, chiral, ag);
	atoms[ib].setzcrd(ag);
//
	int it[4];
	it[0] = findatom(ir, "CA"); it[1] = findatom(ir, "N");
	it[2] = findatom(ir, "C"); it[3] = chiral;
	atoms[ib].setint(it);
	atoms[ib].xyzatm1(atoms);
}
int ZhouProtein::getReschi(int ir, int *idx){
	string rn = rnam[ir];
	int nchi=0, mchi=4;
	if(rn=="PHE" || rn=="TYR" || rn=="TRP" || rn=="HIS") mchi = 2;
	for(int i=rstart[ir]+5; i<rstart[ir+1]; i++){
		int *iz = atoms[i].getint();
		if(iz[3] != 0) continue;
		idx[nchi++] = i;
		if(nchi >= mchi) break;
	}
	return nchi;
}
bool ZhouProtein::refill(){
	initneib();
	for(int i=0; i<natm; i++){
		if( atoms.at(i).filled() ) continue;
		initAtomint(i);
		initAtomzcrd0(i);
		if(! atoms[i].xyzatm1(atoms)) return false;
	}
	return true;
}
double ZhouProtein::calResclash(int ir){
	double nclash=0.;
	for(int i=0; i<natm; i++){
		if(rseq[i] != ir) continue;
		for(int j=0; j<natm; j++){
			if(ir<0 && i<j) continue;
			if(rseq[i] == rseq[j]) continue;
			double r = sqrt( distance2(i, j) );
			if(r < 2.) nclash ++;
		}
	}
	return nclash;
}
//
ZhouAtom::ZhouAtom(int is, int it, string an){
	aseq=is; type=it; name=an;
	bfill=false;
}
void ZhouAtom::addneib(int ia){
	for(int i=0; i<(int)ineib.size(); i++){
		if(ia == ineib[i]) {fprintf(stderr, "Duplicate neib: %d\n", ia); exit(1);}
		else if(ia > ineib[i]) continue;
		ineib.insert(ineib.begin()+i, ia); return;
	}
	ineib.push_back(ia);
}
bool ZhouAtom::xyzatm1(vector <ZhouAtom> &ap){
	bool xyzatm(double *xi, double *xa, double *xb, double *xc, double *ag, int chiral);
	double *xa, *xb, *xc;
	xa = xb = xc = NULL;
	if(iz[0] >= 0){
		if(! ap[iz[0]].filled()) return false;
		xa = ap.at(iz[0]).getx();
	}
	if(iz[1] >= 0){
		if(! ap[iz[1]].filled()) return false;
		xb = ap.at(iz[1]).getx();
	}
	if(iz[2] >= 0){
		if(! ap[iz[2]].filled()) return false;
		xc = ap.at(iz[2]).getx();
	}
	bfill = xyzatm(x, xa, xb, xc, ag, iz[3]);
	return bfill;
}
