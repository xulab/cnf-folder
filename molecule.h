#ifndef _ZHOU_MOLECULE
#define _ZHOU_MOLECULE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "protein.h"
#include "xvec.h"
#include "dfire.h"
#include "ScoreMatrix.h"

static float edfire[matype][matype][mbin],
	edfire_av[mpolar][matype][mbin][mabin], edfire_vv[mpolar][mpolar][mbin][mabin];

class ZhouMolecule:public ZhouProtein{
	int chID, nsym;
	double syms[100][3][4], cvec[3][3];
	double xmax[3], xmin[3];
	vector <Xvec> x0, xn, xvec0, xvec1, xpln0[4], xpln1[4];
	vector <int> polar_atm, plane_atm;		// [natm]
	vector <int> iplane_pro[4]; // type, 3 atoms
	vector <bool> bmiss;
	double Rcut0, Rcut, dRcut;

	ScoreMatrix res_energy, res_energy1i, res_energy1j, res_energy2;

public:
	ZhouMolecule(){ 
		Rcut0=mbin*0.5, Rcut=nbin*0.5, dRcut=0.5; chID = ' ';
		bzero(syms, sizeof(syms)); syms[0][0][0] = syms[0][1][1] = syms[0][2][2] = 1.;
	}
	ZhouMolecule(const char *fn);
	//ZhouMolecule(const char *fn):Protein(fn){};
	void initcrd(); 
	//void buildvec();
	//void init_planes(); void build_planes();
	bool initcrd(int isym, int ix, int iy, int iz);

	void initReplica(FILE *fp);
	bool rdtrj(FILE*);
	//void score(double*);
	//void score1(double *); void score2(double*); //void score3(double*);

	int angle2bin(double ag){
		double PI = acos(-1.), radian=180./PI;
   		static double agbin[mabin] = {-2./3,-1./3, 0., 1/3., 2/3., 1.};
		if(fabs(ag)-1.>1.0e-5){
			fprintf(stderr, "Warning, error angle: %f\n", ag);
		}
		for(int i=0; i<mabin; i++)
			if(ag<=agbin[i]) return i;
		return mabin-1;
	}

	int r2bin(double r){
		if(r >= Rcut0) return -1;
		else return int(r/0.5);
	}
  void score(double *ener, int start){
	int nn=0; nsym = 1;
	initcrd(); initcrd(0, 0, 0, 0); buildvec();
	xvec0.clear();
	for(int i=0; i<(int)xvec1.size(); i++) xvec0.push_back( xvec1[i] );

	bzero(ener, 20*sizeof(*ener));
	score1(ener, start); 
	//extern bool bag;
	//if(bag) score2(&ener[1]); //score3(&ener[4]);
	    score2(&ener[1], start); //score3(&ener[4]);
	//cout<<sum(4, ener)<<'\t';
	//prtdim(4, ener);
	//cout<<sum(4, ener)*0.01<<endl;
  }
  void buildvec(){
	polar_atm.resize(natm);
	for(int i=0; i<natm; i++) polar_atm[i] = -1;
	xvec1.clear(); int np=0;
	for(int i=0; i<natm; i++){
		if(bmiss[i]) continue;
		int ta = atoms.at(i).gettype();
		if(ta<0 || ispolar[ta]<0) continue;
		vector <int> *inb = atoms.at(i).getineib();
		if(inb->size() > 2) {fprintf(stderr,"Warning, Error polar atoms");}
		Xvec pvec1 = 0.; bool bmiss2=false;
		for(int jj=0; jj<(int)inb->size(); jj++){
			int j=inb->at(jj);
			if(! atoms.at(j).filled()) {bmiss2=true; break;};
			Xvec xd = xn[j] - xn[i];
			xd.normalize();
			pvec1 += xd;
		}
		if(bmiss2) pvec1 = 0.;
		else {
			pvec1.normalize();
			polar_atm[i] = np;
		}
		xvec1.push_back( pvec1 );
		np++;
	}
  }
  void score1(double *ener, int start){
	if (start==0)
		res_energy.resize(rseq.size(), rseq.size());
	int irt = 0;
	double eres[nres]; bzero(eres, sizeof(eres));
	int nch_atm=0; chID = chainID.at(0);
	for(int i=0; i<natm; i++){
		if(chID != chainID.at( rseq[i] )) continue;
		int ta = atoms.at(i).gettype();
		if(ta < 0) continue;
		if( bmiss[i] ) continue;
		nch_atm ++;
		//printf("test Atoms: %d %d\n", natm, nch_atm);
		int ka = iskind[ta];
		bool bfar_res = false;		// far away from the res.
		int old_seq = -1;
		for(int j=0; j<natm; j++){
		  if(rseq[j]>=start && rseq[i]>=start){
			if(bfar_res && rseq[j]==old_seq) continue;
			bfar_res = false;
			if( bmiss[j] ) continue;
			int tb = atoms.at(j).gettype();
			if(tb < 0) continue;
			int kb = iskind[tb];
			bool bchain = (irt==0 && chainID.at(rseq[i])==chainID.at(rseq[j]));
			if( bchain && i>=j) continue;	// same chain

			old_seq = rseq[j];
			bool bfar=false;
			Xvec xd = xn[j] - x0[i];
			for(int m=0; m<3; m++){
				if(fabs(xd[m]) > Rcut0) {
					if(fabs(xd[m]) > Rcut0 + 10.) bfar_res = true;
					bfar=true; break;
				}
			}
			if(bfar) continue;
			double r = sqrt(xd * xd);
			if(irt>0 && r<2.) { //printf("%d %d %d %f\n", irt, seq0[rseq[i]], seq0[rseq[j]], r);
			} else if(r < 1.){ //fprintf(stderr, "%d %d %f\n", seq0[rseq[i]], seq0[rseq[j]], r);
			}
			if(r > Rcut0 + 10.) bfar_res= true;

			int b=r2bin(r); if(b<0) continue;
			if(bchain && abs(rseq[j]-rseq[i])<1) continue;
			if(r >= Rcut-0.5/2) continue;
			/*
			int b1 = b + 1; double r0 = bin2r(b), dt = (r - r0) / 0.5;
			if(b == 0) b1 = b;
			else if(r < r0) {b1 = b - 1; dt = -dt;}
			double e = (1-dt)*edfire[ka][kb][b] +  dt*edfire[ka][kb][b1];
			*/
			double e = edfire[ka][kb][b];
			//printf("%d %d %d %f\n", ka, kb, b, e);
			res_energy(rseq[i], rseq[j])=e;
		  }
		  double e = res_energy(rseq[i], rseq[j]);
		  *ener += e;
		  eres[ rseq[i] ] += e; eres[ rseq[j] ] += e;
		  //printf("%d %d %d %f %f\n", ka, kb, b, edfire[ka][kb][b], e);
		}
	}
  }
  void score2(double *ener, int start){
	if (start==0){
		res_energy1i.resize(rseq.size(), rseq.size());
		res_energy1j.resize(rseq.size(), rseq.size());
		res_energy2.resize(rseq.size(), rseq.size());
	}
	int irt = 0;
	buildvec();
	for(int i=0; i<natm; i++){
		if(bmiss[i]) continue;
		if(chID != chainID.at( rseq[i] )) continue;
		int ta = atoms.at(i).gettype();
		if(ta < 0) continue;
		int ka = iskind[ta];
		int pa = ispolar[ ta ];
		int iv = polar_atm[i];

		bool bfar_res = false;			// far away from the res.
		int old_seq = -1;
		for(int j=0; j<natm; j++){
			if(bfar_res && rseq[j]==old_seq) continue;
			int jv = polar_atm[j];
			if(iv<0 && jv<0) continue;

		  if(rseq[j]>=start && rseq[i]>=start){
			bfar_res = false;
			if(bmiss[j]) continue;
			int tb = atoms.at(j).gettype();
			if(tb < 0) continue;
			int pb = ispolar[ tb ];
			int kb = iskind[tb];
			bool bchain = (irt==0 && chainID.at(rseq[i])==chainID.at(rseq[j]));
			//if(bchain && pa>=0 && pb>=0 && i<=j) continue;		// polar in same chain
			if(bchain && i<=j) continue;
			if(bchain && abs(rseq[j]-rseq[i])<1) continue;
			//  calculate the r
			old_seq = rseq[j];
			bool bfar=false;
			Xvec xd = xn[j] - x0[i];
			for(int m=0; m<3; m++){
				if(fabs(xd[m]) > Rcut0+0.5) {
					if(fabs(xd[m]) > Rcut0+0.5 + 10.) bfar_res = true;
					bfar=true; break;
				}
			}
			if(bfar) continue;
			double r = xd.normalize();
			if(irt>0 && r<2.) ; //printf("%d %d %f\n", seq0[rseq[i]], seq0[rseq[j]], r);
			if(r > Rcut0+0.5 + 10.) bfar_res= true;

			int b = r2bin(r);
			if(b < 0) continue;
			if(b >= nbin) continue;
			double ag1, ag2, ag3;
			int ba1, ba2, ba3;
			if(iv >= 0){
				ag2 = xvec0[iv] * xd; ba2=angle2bin(ag2);
				double e = edfire_av[pa][kb][b][ba2];
		  		res_energy1i(rseq[i], rseq[j])=e;
			}
			if(jv >= 0) {
				ag3 = -xvec1[jv] * xd; ba3 = angle2bin(ag3);
				double e = edfire_av[pb][ka][b][ba3];
		  		res_energy1j(rseq[i], rseq[j])=e;
			}
			if(iv>=0 && jv>=0){
				ag1 = xvec0[iv] * xvec1[jv]; ba1=angle2bin(ag1);
				res_energy2(rseq[i], rseq[j])=edfire_vv[pa][pb][b][ba1];
			}
		  }
		  if(iv >= 0){
			double e = res_energy1i(rseq[i], rseq[j]);
		  	if(jv>=0) ener[1] += e;
		  	else ener[0] += e;
		  }
		  if(jv >= 0) {
			double e = res_energy1j(rseq[i], rseq[j]);
			if(iv>=0) ener[1] += e;
			else ener[0] += e;
		  }
		  if(iv>=0 && jv>=0)
		  	ener[2] += res_energy2(rseq[i], rseq[j]);
		}
	}
  }
};
#endif
