/*******************************************************************************
Created by Feng Zhao referencing from : DOPE-Back-AllAtom.h of Copyright (C) 2005 The University of Chicago

  Authors: 
  Min-yi Shen, original code
  Andr�s Colubri, coversion to C++
  James Fitzgerald

  Description:
  PL plugin to calculate Min-yi's Statistical Potential for Fold
  Recognition. Backbone Dependent

*******************************************************************************/

#ifndef __DOPE_BACK_H__
#define __DOPE_BACK_H__

#include "DOPE.h"

// This class contains the binned energy values.
class DopeBack: public DOPE
{
public:
	DopeBack();
	~DopeBack();

	void Init(const char* cfgFileName);
	double PairEnergy(string aa0, int rb0, string at0, string aa1, int rb1, string at1, int cd, double dist, int interaction = 0);

	double energy_coeff, interaction_coeff[4];
	bool add_bonded, use_DM;

protected:
	int n_bins;
	double *SheetSheetEnergy[20][16][20][16][3];
	double *SheetHelixEnergy[20][16][20][16][2];
	double *HelixHelixEnergy[20][16][20][16];
	double *Energy1[20][16][20][16];
	double *Energy2[20][16][20][16];
	double *Energy4[20][16][20][16];
	double *OtherEnergy[20][16][20][16];
	
	void InitializeMemory(int n);
	void ReleaseMemory();
	void InitializeEnergy();
	double PairEnergyFromTable(int aa1, int rb1, int at1, int aa2, int rb2, int at2, int bin);
	void SetDefConfig();
	int ReadConfigFile(const char* FileName);
	int LoadFromFile(string fn, int fn_num);
	void Resize(int n);
	int GetDOPEcode(int aa, const string &at);
	int GetAtomCode(int aa, string atom_str, int insert_flag=1);
	
	// Amino Acid Names
	int ALA, ARG, ASN, ASP, CYS, GLN, GLU, GLY, HIS, ILE, LEU, LYS, MET, PHE, PRO, SER, THR, TRP, TYR, VAL;

private:
	// Parameters.
	double dist_cutoff;
	int num_bin;
	string sheet_sheet_par_file, sheet_helix_par_file, helix_helix_par_file, other_par_file, back_cent_file;
	bool add_backbone, add_hydrogens, add_centroids, add_rb_centroids, add_side_chains, add_only_CB;
	int min_chain_dist, max_chain_dist;
	bool update_codes, print_profile;
};

#endif
