/* *
 * The C Protein Folding Library.
 * Copyright (C) 2009 Andres Colubri.
 * Contact: andres.colubri 'AT' gmail.com
 *
 * This library was written at the Institute of Biophysical Dynamics at the University of Chicago.
 * Gordon Center for Integrated Science, W101. 929 East 57th Street, Chicago, IL 60637, USA.
 * Homepage: http://ibd.uchicago.edu/
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation with or without modifications and for any purpose and
 * without fee is hereby granted, provided that any copyright notices
 * appear in all copies and that both those copyright notices and this
 * permission notice appear in supporting documentation, and that the
 * names of the contributors or copyright holders not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific prior permission.
 * 
 * THE CONTRIBUTORS AND COPYRIGHT HOLDERS OF THIS SOFTWARE DISCLAIM ALL
 % WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE
 * CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT
 * OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOFTWARE.
 * 
 */

/**
 * DOPE-CB statisical potential function.
 * Authors: Min-yi Shen (original creator of DOPE), Andres Colubri (protlib implementation), Glen Hocky (protlib2 port)
 *
 */

#ifndef __DOPE_PW_AC_H__
#define __DOPE_PW_AC_H__

#include <stdio.h>
#include <vector>
#include "BALL/KERNEL/protein.h"
#include <BALL/MATHS/vector3.h>

#include "bbq.h"
#include "ScoreMatrix.h"

using namespace std;
using namespace BALL;

#define convert_to_dopepw_row(term,aa0,at0,aa1,at1) (768000*term+aa0*38400+aa1*1920+at0*240+at1*30)
#define convert_to_dopepw_pos(term,aa0,at0,aa1,at1,bin) (768000*term+aa0*38400+aa1*1920+at0*240+at1*30+bin)
#define residue_index(res0,res1) (nres*res0+res1)

#define MAXBINNUM 29
#define MAXATCODE 7
#define MAXAACODE 19
#define NUMTERMS  8

#define MINCD 2      //sets minimum chain distance to consider calculating

#define EECONT  0
#define HHCONT  1
#define CCCONT  2
#define EEap    3
#define EEp     4
#define PW0Ancont 5
#define PW1Ancont 6
#define PW2Ancont 7

#define HELIX 0
#define STRAND 1
#define COIL 2

#define PARALLEL 1
#define ANTIPARALLEL 2

#define NO_ERROR 0

typedef int IndexValue;
typedef int IntValue;
typedef double FloatValue;
typedef bool BoolValue;

// modified from Andres Colubri's version of dope_pw
class dopepw_ac
{
public:
	dopepw_ac();
	int init_dopepw(const char* cfg);
	int set_system_for_dopepw(BoolValue *_distmask, vector<RESIDUE> *_residues, string _sequence);
	int pre_dopepw_calc(void);
	int calc_dopepw_energy(FloatValue *energy);
	int finish_dopepw(void);

	double _num_HB;

protected:
	int four_atom_angle(FloatValue *angle,ATOM& head1,ATOM& tail1,ATOM& head2,ATOM& tail2);
	int setup_pw_pair_vecs(void);
	int delete_pw_pair_vecs(void);
	int set_all_pair_vecs(IndexValue position,IntValue integer);
	void print_pair_data(IndexValue at1,IndexValue at2);

	Vector3 getPosition(ATOM a){ //cerr << a.name << "("<<a.x<<", "<<a.y<<", "<<a.z<<")"<<endl; 
		return Vector3(a.x, a.y, a.z); 
	};

	void trim2(string& str) {
		string::size_type pos = str.find_last_not_of(' ');
		if(pos != string::npos) {
			str.erase(pos + 1);
			pos = str.find_first_not_of(' ');
			if(pos != string::npos) 
				str.erase(0, pos);
		}
		else str.erase(str.begin(), str.end());
	};
	// Atom codes ------------------------------------------------------------------
	//enum{MC_N = 0, MC_CA = 1, MC_C = 2, MC_O = 3, MC_HN = 4, MC_HA1 = 5, MC_CAP = 6, SC_CB = 7, SC_HA2 = 8};
	IndexValue get_atom_type(string atomname){
		if (atomname=="N")
			return 0; //MC_N 
		else if (atomname=="CA")
			return 1; //MC_CA 
		else if (atomname=="C")
			return 2; //MC_C 
		else if (atomname=="O")
			return 3; //MC_O 
		else if (atomname=="HN")
			return 4; //MC_HN
		else if (atomname=="CB")
			return 7; //MC_CB 
		else
			return 5;
	};

	int AALetter2Code(char a_char){
		int arrResidueIdx[26]={  0, -1, 4, 3,  6, 13,  7,  8,  9, -1, 11, 10, 12, 
					 2, -1, 14, 5, 1, 15, 16, -1, 19, 17, -1, 18, 20};
		/*	ALA	A	0
				B	-1
			CYS	C	4
			ASP	D	3
			GLU	E	6
			PHE	F	13
			GLY	G	7
			HIS	H	8
			ILE	I	9
				J	-1
			LYS	K	11
			LEU	L	10
			MET	M	12
			ASN	N	2
				O	-1
			PRO	P	14
			GLN	Q	5
			ARG	R	1
			SER	S	15
			THR	T	16
				U	-1
			VAL	V	19
			TRP	W	17
				X	-1
			TYR	Y	18
			HET	Z	20
		*/
		int idx = a_char-'A';
		if (idx>=0 && idx<26)
			return arrResidueIdx[idx];
		else
			return -1;
	};
	
	int AtomNameToCode(string name){
		string arrAtomName[8]={"N","HN","CA","CB","","","C","O"}; //2,3,0,6,7,1::CA,CB,N,C,O,HN
		for (int i=0; i<8; i++)
			if (arrAtomName[i]==name)
				return i;
		return -1; //UNK;
	};

	bool bFirstTimeSetup;

	vector<ATOM*> atoms;	// Array of atom data.
	ScoreMatrix distances;	// Array of atom pair-wise distances.
	//BoolValue *distmask;	// Array of distance masking values.  
	IndexValue natoms;		// Total number of atoms.

	string sequence;
	vector<RESIDUE> *residues;    // Array of residue data.
	IndexValue nres;                 // Total number of residues.

	IndexValue natoms2,dopearraysize;

	FloatValue * dopeenergies;
	FloatValue * pairenergies;
	FloatValue * paircoeffs;

	IndexValue * pairrows;
	IndexValue * pairterms;
	IndexValue * activepairs;
	IndexValue nactivepairs;
	IndexValue * updatepairs;
	IndexValue nupdatepairs;

	//pair arrays
	IndexValue * pwvec;
	IndexValue * papvec;
	IndexValue * eevec;
	IndexValue * palign;
	IndexValue * apalign;
	IndexValue * hohbondvec;
	IndexValue * ohhbondvec;
	IndexValue * cocoalign;
	IndexValue * nhnhalign;
	IndexValue * nhcoalign;
	IndexValue * conhalign;
	IndexValue * niprop;

	BoolValue debug,debug2;

	IndexValue * secstrvec;  //array to hold secondary structure numbers. H<->0, E<->1, Other<->2
	IndexValue * continuityvec;
};

#endif
