#ifndef _FB_CLUSTER_ANG_H_
#define _FB_CLUSTER_ANG_H_

#include "Kent.h"
#include <vector>

using namespace std;

class FB5ClusterNode
{
public:
    	double beta;
    	double kappa;
    	double gamma1[3];
    	double gamma2[3];
    	double gamma3[3];
    	double theta;
    	double tau;
    	int index;
};

class FB5Cluster
{
protected:
	int mNumCluster;

public:
	vector<FB5ClusterNode> mCluster;
     	vector<KentDensity* > mDens;
     	vector<KentSampler* > mSampler;
     	double rev2[3];
    
     	double** mSecProb;
     	double** mSeqProb;

public:
	FB5Cluster(const char* aFileName);
     	~FB5Cluster();
     	double getProb(double theta, double tau, int i);
     	void sample(double* rev, int i);
	int getNumClusters() { return mNumCluster;};
};
#endif
