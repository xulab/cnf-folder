#include <BALL/STRUCTURE/rotamerLibrary.h>
#include <BALL/SYSTEM/file.h>
#include <BALL/STRUCTURE/fragmentDB.h>
#include <BALL/STRUCTURE/residueChecker.h>
#include <BALL/KERNEL/system.h>
#include <BALL/STRUCTURE/geometricProperties.h>
#include <BALL/STRUCTURE/defaultProcessors.h>
#include <BALL/FORMAT/PDBFile.h>
#include <sys/times.h>
#include <iostream>
#include <BALL/SYSTEM/timer.h>
#include <set>

#include "SideChain_util.h"
#include "stringTokenizer.h"

extern String atomRadiusFile;

using namespace BALL;
using namespace std;

const double ADJ_DISTANCE_LB=2.0;
const double ADJ_DISTANCE_UB=4.2;

#define VALIDADJDISTANCE(d) ((d>ADJ_DISTANCE_LB)&&(d<ADJ_DISTANCE_UB))

void WritePositions(Residue& r){
	for(AtomIterator ai=r.beginAtom();ai!=r.endAtom();ai++)
	{
		if (SideChainAtom(*ai) || BackBoneAtom(*ai)) {
			cout<<ai->getName()<<"\t"<<ai->getPosition()<<"\t radius="<<ai->getRadius()<<endl;
		}
	}
}

pair<int, int> CalculateTorsionAngles(BasicAtomTriple& previous, BasicAtomTriple& current, BasicAtomTriple& next)
{
	const Atom* last_C=previous.C;
	const Atom* N=current.N;
	const Atom* CA=current.CA;
	const Atom* C=current.C;
	const Atom* next_N=next.N;

	Vector3 a12(N->getPosition()-last_C->getPosition());
	Vector3 a23(CA->getPosition()-N->getPosition());
	Vector3 a34(C->getPosition()-CA->getPosition());
	Vector3 a45(next_N->getPosition()-C->getPosition());

	Vector3 n13(a12 % a23);
	Vector3 n24(a23 % a34);
	Vector3 n35(a34 % a45);

	if (n13 == Vector3::getZero() || n24 == Vector3::getZero() || n35==Vector3::getZero())
	{
		cerr<<"illegal positions!"<<endl;
		return pair<int,int>(-360, -360);
	}
	n13.normalize();
	n24.normalize();
	n35.normalize();

	Vector3 cross_n13_n24(n13 % n24);
	float direction = cross_n13_n24 * a23;
	float scalar_product = n13 * n24;

	if (scalar_product > 1.0)
		scalar_product=1.0;
	else if (scalar_product< -1.0)
		scalar_product=-1.0;
	float phi=acos(scalar_product);
	if (direction<0.0)
		phi*=-1;

	Vector3 cross_n24_n35(n24%n35);
	direction=cross_n24_n35* a34;
	scalar_product = n24*n35;

	if (scalar_product > 1.0)
		scalar_product=1.0;
	else if (scalar_product< -1.0)
		scalar_product=-1.0;
	float psi=acos(scalar_product);
	if (direction<0.0)
		psi*=-1;

	int i_phi=(int)roundf(phi*180/Constants::PI/10);
	i_phi=i_phi*10;
	int i_psi=(int)roundf(psi*180/Constants::PI/10);
	i_psi=i_psi*10;

	return pair<int,int>(i_phi,i_psi);
}

bool ParseField(char* field, vector<pair<int, int> > & fieldList)
{
	if (!field || field[0]==0){
		cerr<<"WARNING: field is empty, something might be wrong!"<<endl;
		return false;
	}

	//begin to parse field
	for (int i=0;i<(int)strlen(field);i++){
		if (!isdigit(field[i]) && field[i]!=',' && field[i]!='-' && !isspace(field[i])){
			cerr<<"field contains illegal character, it can contain only digits and , -\n";
			return false;
		}
	}

	StringTokenizer* strTokens=new StringTokenizer(field,"\n");
	char* token=0;
	int low,high;
	while(token=strTokens->getNextToken()){
		int ret=sscanf(token,"%d-%d",&low,&high);
		if (ret<=0 || ret>=3) continue;
		if (ret==1){
			fieldList.push_back(pair<int,int>(low,low));
		}else if (ret==2){
			fieldList.push_back(pair<int,int>(low,high));
		}
	}

	if (strTokens) delete strTokens;

	return true;
}

bool SideChainAtom(const Atom& a){
	String name=a.getName();
	name.setCompareMode(String::CASE_INSENSITIVE);
	if (name=="C" || name=="CA" || name=="N" || name=="O") return false;
	if (name=="CB") return false; //here we also exclude CB atom
	if (name.size()<1) return false;
	if (name[0]=='H') return false;
	if (name.size()>1 && String::isDigit(name[0]) && name[1]=='H') return false;
	return true;
}

bool BackBoneAtom(const Atom& a){
	String name=a.getName();
	name.setCompareMode(String::CASE_INSENSITIVE);
	if (name=="C" || name=="CA" || name=="N" || name=="O") return true;
	if (name=="CB") return true; //here we also include CB atom
	return false;
}

double DihedralAngle(const Vector3 &v1, const Vector3 &v2, const Vector3 &v3, const Vector3 &v4)
{
	Vector3 v12(v1-v2), v43(v4-v3), z(v2-v3);
	double u, v, r;
		
	Vector3 p(z % v12); // Cross Product
	Vector3 x(z % v43);
	Vector3 y(z % x);
	
	u = x * x; // Dot Product
	v = y * y;
	//cerr << "("<<x.x<<","<<x.y<<","<<x.z<<")="<< u <<"*("<<y.x<<","<<y.y<<","<<y.z<<")="<<v;
		
	r = 360.0;
	if (u <= 0.0 || v <= 0.0) return r;
		
	u = (p * x) / sqrt(u);
	v = (p * y) / sqrt(v);
	//cerr << "("<<p.x<<","<<p.y<<","<<p.z<<")="<< u <<"="<<v<< endl;
	if (u != 0.0 || v != 0.0) return (atan2(v, u) * 180 / 3.14159265358979);
	
	return r;
}


double getAngle(const Vector3 &v1, const Vector3 &v2, const Vector3 &v3)
{
	Vector3 v12(v1-v2), v23(v2-v3);
	v12.normalize();
	v23.normalize();

	float scalar_product = v12 * v23;
	if (scalar_product > 1.0)
		scalar_product=1.0;
	else if (scalar_product< -1.0)
		scalar_product=-1.0;
	float angle=acos(scalar_product);
	return angle;
}
