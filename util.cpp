#include "util.h"

#include "IP_constant.h"

#include <ctype.h>
#include <math.h>

int rand_node_compare(const void* d1, const void* d2){
	RAND_NODE* a1=(RAND_NODE*)d1;
	RAND_NODE* a2=(RAND_NODE*)d2;

	if (a1->number<a2->number) return -1;
	if (a1->number>a2->number) return 1;
	return 0;
}

void ToolKit::ShuffleArray(int* src, int* dest, int len){
	RAND_NODE* nodes=new RAND_NODE[len];
	for(int i=0;i<len;i++){
		nodes[i].number=random();
		nodes[i].data=src+i;
	}
	qsort(nodes,len,sizeof(RAND_NODE),rand_node_compare);
	for(int j=0;j<len;j++)
		dest[j]=*((int*)nodes[j].data);
}

int ToolKit::isEmpty(char* s){
	if (!s) return 1;
	if (!s[0]) return 1;

	int i=0;
	while(s[i] && isspace(s[i])) i++;

	if (!s[i]) return 1;

	return 0;
}

int ToolKit::isAA(char a){

	char tmp=toupper(a);

	if (tmp>'Z' || tmp<'A')	return 0;

	if (AA2SUB[tmp-'A']<0 || AA2SUB[tmp-'A']>=NONAMINO)	return 0;

	return 1;
}


int ToolKit::round(float x){
	int c=(int)ceil(x);
	int f=(int)floor(x);

	if (c-x>x-f)	return f;
	else return c;
}

void ToolKit::allege(bool b, char* message){
	if (!b){
		fprintf(stderr,message);
	}
}

float ToolKit::sumOfColumn(float* m, int c, int rowSize, int colSize){

	float sum=0;

	for(int r=0;r<rowSize;r++)
		sum+=m[r*colSize+c];
	return sum;

}

float ToolKit::sumOfRow(float* m, int r, int rowSize, int colSize){
	float sum=0;
	
	for(int c=0;c<colSize;c++)
		sum+=m[r*colSize+c];
	return sum;

}

float ToolKit::sumOfMatrix(float* m, int rowSize, int colSize){
	float sum=0;

	for(int r=0;r<rowSize;r++)
		for(int c=0;c<colSize;c++)
			sum+=m[r*colSize+c];
	return sum;

}

void ToolKit::sort(double* list, int size, bool flag){
	if (flag)
		qsort(list,size,sizeof(double),ToolKit::GEdouble);	
	else 
		qsort(list,size,sizeof(double),ToolKit::LEdouble);	
}
void ToolKit::sort(int* list, int size, bool flag){
	if (flag)
		qsort(list,size,sizeof(int),ToolKit::GEint);	
	else 
		qsort(list,size,sizeof(int),ToolKit::LEint);	
}
int ToolKit::GEdouble(const void * p1, const void * p2){
	double* d1=(double*)p1;
	double* d2=(double*)p2;
	return (*d1)>=(*d2);
}

int ToolKit::LEdouble(const void* p1, const void *p2){
	double* d1=(double*)p1;
	double* d2=(double*)p2;
	return (*d1)<=(*d2);
}
int ToolKit::LEint(const void* p1, const void *p2){
	int* d1=(int*)p1;
	int* d2=(int*)p2;
	return (*d1)<=(*d2);
}
int ToolKit::GEint(const void * p1, const void * p2){
	int* d1=(int*)p1;
	int* d2=(int*)p2;
	return (*d1)>=(*d2);
}

unsigned long pow3(int x){
	if (x<0) return 1;
	if (x>10) return pow3(x-10)*POW3[10];
	else return POW3[x];
}




unsigned long pow2(int x){

	if (x<-1){
		perror("pow incorrect!");
		exit(-1);
	}
	if (x==-1) return 0;
	if (x<16)	return POW2[x];
	else return POW2[15]*pow2(x-15);

}

unsigned long pow(int x,int y){
	if (y<0){
		perror("pow incorrect!");
		exit(-1);
	}

	if (y==0)	return 1;


	int a=x;
	int res=1;
	int x1=y;

	while(x1>0){
		if (x1%2) res*=a;	
		a^=2;
		x1/=2;
	}



	return res;





}


char ToolKit::AlignSymbol (int res1, int res2)
  {
    if (res1 == res2) return '|';
    else if ((res1 == 3  && res2 == 6)  || (res1 == 6  && res2 == 3))
      return ':';
    else if ((res1 == 1  && res2 == 11) || (res1 == 11 && res2 == 1))
      return ':';
    else if ((res1 == 9  || res1 == 10) && (res2 == 9  || res2 == 10))
      return ':';
    else if ((res1 == 12 || res1 == 19) && (res2 == 12 || res2 == 19))
      return ':';
    else if ((res1 == 17 || res1 == 18) && (res2 == 17 || res2 == 18))
      return ':';
    else if ((res1 == 1 || res1 == 11  || res1 == 8) && 
             (res2 == 1 || res2 == 11  || res2 == 8))
      return '.';
    else if ((res1 == 13 || res1 == 17  || res1 == 18) && 
             (res2 == 13 || res2 == 17  || res2 == 18))
      return '.';
    else if ((res1 == 9  || res1 == 10  || res1 ==12   || res1 == 19) && 
             (res2 == 9  || res2 == 10  || res2 ==12   || res2 == 19))
      return '.';
    else if ((res1 == 0  || res1 == 14  || res1 == 15  || res1 == 16) && 
             (res2 == 0  || res2 == 14  || res2 == 15  || res2 == 16))
      return '.';
    else if ((res1 == 5 && res2 == 6)   || (res1 == 6  && res2 == 5))
      return '.';
    else if ((res1 == 2 && res2 == 3)   || (res1 == 3  && res2 == 2))
      return '.';
    else return ' ';
  }

/*
int Match2Seq(char* str1, int len1, char* str2, int len2, int* pos_align){

#define MAX_LEN	10000

	float GAPBasePenalty=11;
	float GAPIncPenalty=1;

	float GAPPenalty=GAPBasePenalty+GAPIncPenalty;

	int tLen=len1;
	int sLen=len2;


	float * scoreMatrix[MAX_LEN];
	float * deleteMatrix[MAX_LEN];
	float * insertMatrix[MAX_LEN];

	float* score_array=new float[(tLen+1)*(sLen+1)];
	float* delete_array=new float[(tLen+1)*(sLen+1)];
	float* insert_array=new float[(tLen+1)*(sLen+1)];

	int p;
	for(p=0;p<tLen+1;p++){
		scoreMatrix[p]=score_array+p*(sLen+1);
		deleteMatrix[p]=delete_array+p*(sLen+1);
		insertMatrix[p]=insert_array+p*(sLen+1);
	}

	//scoreMatrix[t][s], insertMatrix[t][s], deleteMatrix[t][s] means
	//when the t-th temp AA is aligned to s-th seq AA, count starting from 1


	short* scoreAlign[MAX_LEN];
	short* deleteAlign[MAX_LEN];
	short* insertAlign[MAX_LEN];

	short* score_align=0;
	short* delete_align=0;
	short* insert_align=0;

	short* final_table;

	score_align=new short[(tLen+1)*(sLen+1)];
	delete_align=new short[(tLen+1)*(sLen+1)];
	insert_align=new short[(tLen+1)*(sLen+1)];
	for(p=0;p<tLen+1;p++){
		scoreAlign[p]=score_align+p*(sLen+1);
		deleteAlign[p]=delete_align+p*(sLen+1);
		insertAlign[p]=insert_align+p*(sLen+1);
	}


	scoreMatrix[0][0]=deleteMatrix[0][0]=insertMatrix[0][0]=0;


	int i;
	for(i=1;i<=tLen;i++)
	{
		scoreMatrix[i][0]=0;
		insertMatrix[i][0]=MAXALIGNSCORE;
		deleteMatrix[i][0]=MAXALIGNSCORE;
	}

	for(i=1;i<=sLen;i++)
	{
		scoreMatrix[0][i]=0;
		deleteMatrix[0][i]=MAXALIGNSCORE;
		insertMatrix[0][i]=MAXALIGNSCORE;
	}

	float minscore=MAXALIGNSCORE-1;
	int tFinalLen=0;
	int sFinalLen=0;

	int t;
	for(t=1;t<=tLen;t++)
		for(int s=1;s<=sLen;s++)
		{

			float d1=deleteMatrix[t-1][s]+GAPIncPenalty;
			float s1=scoreMatrix[t-1][s]+GAPPenalty;
			float i1=insertMatrix[t-1][s]+GAPPenalty;

			if (d1<s1 && d1<i1){
				deleteMatrix[t][s]=d1;
				deleteAlign[t][s]=DELETE_ACTION;
			}else if(s1<=i1){
				deleteMatrix[t][s]=s1;
				deleteAlign[t][s]=ALIGN_ACTION;
			}else{
				deleteMatrix[t][s]=i1;
				deleteAlign[t][s]=INSERT_ACTION;
			}

			float d2=deleteMatrix[t][s-1]+GAPPenalty;
			float s2=scoreMatrix[t][s-1]+GAPPenalty;
			float i2=insertMatrix[t][s-1]+GAPIncPenalty;

			if (i2<s2 && i2<d2){
				insertMatrix[t][s]=i2;
				insertAlign[t][s]=INSERT_ACTION;
			}else if(s2<=d2){
				insertMatrix[t][s]=s2;
				insertAlign[t][s]=ALIGN_ACTION;
			}else{
				insertMatrix[t][s]=d2;
				insertAlign[t][s]=DELETE_ACTION;
			}


			scoreMatrix[t][s]=MIN(scoreMatrix[t-1][s-1],MIN(insertMatrix[t-1][s-1],deleteMatrix[t-1][s-1]))+fitnessScore[s][t];




			if (scoreMatrix[t-1][s-1]<=MIN(insertMatrix[t-1][s-1],deleteMatrix[t-1][s-1]))
				scoreAlign[t][s]=ALIGN_ACTION;
			else if (insertMatrix[t-1][s-1]<deleteMatrix[t-1][s-1])
				scoreAlign[t][s]=INSERT_ACTION;
			else 	scoreAlign[t][s]=DELETE_ACTION;


			float min=MIN3(scoreMatrix[t][s],insertMatrix[t][s],deleteMatrix[t][s]);
			

			if (minscore>min){
				minscore=min;
				tFinalLen=t;
				sFinalLen=s;

				if (scoreMatrix[t][s]<=insertMatrix[t][s] && scoreMatrix[t][s]<=deleteMatrix[t][s])
				{
					final_table=score_align;
				}else if(insertMatrix[t][s]<deleteMatrix[t][s]){
					final_table=insert_align;
				}else final_table=delete_align;

			}

		}
	

	// backtrace

	int tAlign[MAX_LEN];
	int sAlign[MAX_LEN];

	for(int l=tFinalLen+1;l<=tLen;l++)
		tAlign[l-1]=-1;
	for(int k=sFinalLen+1;k<=sLen;k++)
		sAlign[k-1]=-1;

	short *table=final_table;
	t=tFinalLen;
	int s=sFinalLen;
	int action;

	while(t>0 && s>0){
		action=table[t*(sLen+1)+s];

		//in the following statements, t-1 or s-1 just for 
		//array index

		if (table==score_align && action==ALIGN_ACTION){
			// t is aligned to s
			tAlign[t-1]=s-1;
			sAlign[s-1]=t-1;

			t--;
			s--;
			continue;
		}
		if (table==score_align && action==INSERT_ACTION){
			table=insert_align;
			continue;
		}
		if (table==score_align && action==DELETE_ACTION){
			table=delete_align;
			continue;
		}
		if (table==delete_align && action==ALIGN_ACTION){
			tAlign[t-1]=-1;
			t--;
			table=score_align;
			continue;
		}

		if (table==delete_align && action==DELETE_ACTION){
			tAlign[t-1]=-1;
			t--;
			continue;
		}
		if (table==delete_align && action==INSERT_ACTION){
			tAlign[t-1]=-1;
			t--;
			table=insert_align;
			continue;
		}

		if (table==insert_align && action==ALIGN_ACTION){
			sAlign[s-1]=-1;
			s--;
			table=score_align;
			continue;
		}

		if (table==insert_align && action==INSERT_ACTION){
			sAlign[s-1]=-1;
			s--;
			continue;
		}
		if (table==insert_align && action==DELETE_ACTION){
			sAlign[s-1]=-1;
			s--;
			table=delete_align;
			continue;
		}

	}
	if(t>0){
		for(int i=1;i<=t;i++)
			tAlign[i-1]=-1;
	}else if(s>0){
		for(int i=1;i<=s;i++)
			sAlign[i-1]=-1;
	}


	//get total score
	//score=scoreMatrix[tFinalLen][sFinalLen];

	if (score_array) delete []score_array;
	if (delete_array) delete []delete_array;
	if (insert_array) delete []insert_array;

	if (score_align) delete []score_align;
	if (insert_align) delete []insert_align;
	if (delete_align) delete []delete_align;


	memcpy(pos_align,tAlign,sizeof(int)*len1);

	int count=0;
	for(i=0;i<len1;i++)
		count+=(tAlign[i]>=0);

	return count;

	
}

void main(){
	char s1[256];
	char s2[256];

	sprintf(s1,"%s","ABCDEFG");
	sprintf(s2,"%s","ACDEFH");

	int s3[256];

	int ret=Match2Seq(s1,strlen(s1),s2,strlen(s2),s3);

	printf("ret=%d\n",ret);
	for(int i=0;i<strlen(s1);i++){
		printf(" %d ",s3[i]);
	}
	
}
*/
