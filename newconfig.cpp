#include <stdlib.h>
#include <iostream>
#include <string>

#include "newconfig.h"
#include "strtokenizer.h"

using namespace std;

Config::Config(){
	source.clear();
	configs.clear();
	my_id=-1;
	num_procs=1;

	//set default configuration here
}

Config::Config(char* fileName){
	Config();
	source=fileName;
}

Config::Config(string& fileName){
	Config();
	source=fileName;
}

Config::~Config(){
}

void Config::Initialize(){

}

void Config::configure(){

	//printf("begin to initialize the config object!\n");
	Initialize();

	//printf("begin to read the config file!\n");
	readConfiguration();

	//printf("begin to postprocess after reading the config file!\n");
	PostProcess();
	//printf("finish postprocessing after reading the config file!\n");


}


//do some postprocess after reading the configuration file
void Config::PostProcess(){
}

void Config::readConfiguration(){

	FILE* fp=0;
	if (source.empty()){ 
		cerr<<"WARNING:the configuration file is empty!"<<endl;
		return;
	}else {
		fp=fopen(source.c_str(),"r");
		if (!fp){
            		cerr<<"WARNING: cannot open the configuration file "<<source<<" for read!"<<endl;
			return;
		}
	}

	char buf[4096];

	while(fgets(buf,4096,fp)){
		char* s=buf;
		while(s[0] && isspace(s[0])) s++;
		if (!s[0]) continue;
		if (s[0]=='#') continue; //comments
		if (s[0]=='[' || s[0]=='<') continue; //tags
		ProcessOneLine(buf);
	}
	fclose(fp);

}

void Config::ProcessOneLine(char* buf){

	string paramName;
	string paramValue;

	int ret=parseLine(buf, paramName, paramValue);
	if (ret<2 || ret==EOF) return;
	configs[paramName]=paramValue;

}


//int parseLine(char* buf, char* paramName, char* paramValue){
int Config::parseLine(char* buf, string& paramName, string& paramValue){

	strtokenizer tokenizer(buf," =\t\r\n");
	if (tokenizer.count_tokens()!=2) return 0;
	paramName=tokenizer.token(0);
	paramValue=tokenizer.token(1);
	return 2;
}

int Config::getValue(const string& key, string* value){
	map<string, string>::iterator iter=configs.find(key);
	if (iter==configs.end()) {
		//find default settings
		iter=default_configs.find(key);
		if (iter==default_configs.end()) {
			return 0;
		}
	}
	*value=iter->second;
	return 1;
}

int Config::getValue(const string& key, double* value){
	map<string, string>::iterator iter=configs.find(key);
	if (iter==configs.end()) {
		//find default settings
		iter=default_configs.find(key);
		if (iter==default_configs.end()) {
			return 0;
		}
	}
	//insert code here to check if iter->second is a numeric value, if not return 0

	*value=atof(iter->second.c_str());
	return 1;
}

int Config::getValue(const string& key, int* value){
	map<string, string>::iterator iter=configs.find(key);
	if (iter==configs.end()) {
		//find default settings
		iter=default_configs.find(key);
		if (iter==default_configs.end()) {
			return 0;
		}
	}
	//insert code here to check if iter->second is an integer value, if not return 0


	*value=atoi(iter->second.c_str());
	return 1;
}
int Config::getValue(const string& key, bool* value){
	map<string, string>::iterator iter=configs.find(key);
	if (iter==configs.end()) {
		//find default settings
		iter=default_configs.find(key);
		if (iter==default_configs.end()) {
			return 0;
		}
	}
	
	//insert code here to check if iter->second is a boolean value, if not return 0


	if (iter->second=="true" || iter->second=="yes") *value=1;
	else if(iter->second=="false" || iter->second=="no") *value=0;
	return 1;
}


// for debug
void Config::printConfig()
{
}


 /* for test
int main()
{
    Config *config = new Config(); 
    config->configure();
    config->printConfig();
    }//
 */
