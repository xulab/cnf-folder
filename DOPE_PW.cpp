/*******************************************************************************

                                  DOPE.cpp
                                  --------

  ****************************************************************************
  *                                                                          *
  *   This library is free software; you can redistribute it and/or          *
  *   modify it under the terms of the GNU Lesser General Public             *
  *   License as published by the Free Software Foundation; either           *
  *   version 2.1 of the License, or (at your option) any later version.     *
  *                                                                          *
  *   This library is distributed in the hope that it will be useful,        *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *
  *   Lesser General Public License for more details.                        *
  *                                                                          *
  *   You should have received a copy of the GNU Lesser General Public       *
  *   License along with this library; if not, write to the Free Software    *
  *   Foundation, Inc.,                                                      *
  *   59 Temple Place, Suite 330, Boston, MA  02111-1307  USA                *
  *                                                                          *
  ****************************************************************************

  Copyright (C) 2005 The University of Chicago

  Authors:
  Andres Colubri
  slight mods by Joe D.

  Description:
  Plugin that calculates pairwise statistical potentials. In particular, it
  can calculate DOPE, the statistical potential created by Min-yi Shen and
  Andrej Sali at UCSF.
  This plugin can be configured to evaluate other statistical potentials by
  providing the appropriate parameter file.

*******************************************************************************/

#include <cstddef> // Needed to use NULL.
#include <fstream>
#include <cstring>
#include <cmath>
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
using namespace std;

#include "DOPE_PW.h"
#include "strtokenizer.h"

/************************** DopePW implementation***************************/
#define UNK -1
#define INF_CHAIN 10000

DopePW::DopePW()
{
	max_atom_code=7;
	n_bins = 30;
	SetDefConfig();
    InitializeMemory(30, PW_EE_cont_Energy);
    InitializeEnergy(PW_EE_cont_Energy);
    InitializeMemory(30, PW_HH_cont_Energy);
    InitializeEnergy(PW_HH_cont_Energy);
    InitializeMemory(30, PW_CC_cont_Energy);
    InitializeEnergy(PW_CC_cont_Energy);

    InitializeMemory(30, PW_0EEap_ncont_Energy);
    InitializeEnergy(PW_0EEap_ncont_Energy);
    InitializeMemory(30, PW_0EEp_ncont_Energy);
    InitializeEnergy(PW_0EEp_ncont_Energy);
    InitializeMemory(30, PW_0A_ncont_Energy);
    InitializeEnergy(PW_0A_ncont_Energy);
    InitializeMemory(30, PW_1A_ncont_Energy);
    InitializeEnergy(PW_1A_ncont_Energy);
    InitializeMemory(30, PW_2A_ncont_Energy);
    InitializeEnergy(PW_2A_ncont_Energy);
}

DopePW::~DopePW()
{
    ReleaseMemory(PW_EE_cont_Energy);
    ReleaseMemory(PW_HH_cont_Energy);
    ReleaseMemory(PW_CC_cont_Energy);

    ReleaseMemory(PW_0EEap_ncont_Energy);
    ReleaseMemory(PW_0EEp_ncont_Energy);
    ReleaseMemory(PW_0A_ncont_Energy);
    ReleaseMemory(PW_1A_ncont_Energy);
    ReleaseMemory(PW_2A_ncont_Energy);
}

void DopePW::SetDefConfig()
{
    // Setting defaults.
    dist_cutoff = 15.0;
    n_bins = 30;

    dist_cutoff_ncont = 30.0;

    PW_EE_cont_Energy_par_file = "";
    PW_HH_cont_Energy_par_file = "";
    PW_CC_cont_Energy_par_file = "";

    PW_0EEap_ncont_Energy_par_file = "";
    PW_0EEp_ncont_Energy_par_file = "";
    PW_0A_ncont_Energy_par_file = "";
    PW_1A_ncont_Energy_par_file = "";
    PW_2A_ncont_Energy_par_file = "";

    PW_EE_cont_Energy_coefficient = 1.0;
    PW_HH_cont_Energy_coefficient = 1.0;
    PW_CC_cont_Energy_coefficient = 1.0;

    PW_0EEap_ncont_Energy_coefficient = 1.0;
    PW_0EEp_ncont_Energy_coefficient = 1.0;
    PW_0A_ncont_Energy_coefficient = 1.0;
    PW_1A_ncont_Energy_coefficient = 1.0;
    PW_2A_ncont_Energy_coefficient = 1.0;


    PW_EE_cont_Energy_steric_only = false;
    PW_HH_cont_Energy_steric_only = false;
    PW_CC_cont_Energy_steric_only = false;

    PW_0EEap_ncont_Energy_steric_only = false;
    PW_0EEp_ncont_Energy_steric_only = false;
    PW_0A_ncont_Energy_steric_only = false;
    PW_1A_ncont_Energy_steric_only = false;
    PW_2A_ncont_Energy_steric_only = false;

    esp_par_file = "";

    add_bonded = false;

    add_backbone = true;
    add_hydrogens = false;
    add_centroids = false;
    add_side_chains = true;
    add_only_CB = false;
    CB_CB_focus = false;

    calc_SC_energy = false;

    min_chain_dist = 1;
    min_CB_CB_dist = 1;
    max_chain_dist = INF_CHAIN;

    energy_coeff = 1.0;
    CB_CB_coefficient = 1.0;

    secseq = "";
}

int DopePW::ReadConfigFile(string cfgName)
{
	cerr << "DopePW::ReadConfigFile("<< cfgName<<")"<<endl;
	ifstream CfgFile;
	CfgFile.open(cfgName.c_str(), ios::in | ios::binary);
	string line, par, val;
	bool recog_line;

	while (true)
	{
		getline(CfgFile, line); trim2(line);
	
		if ((line != "") && (line[0] != '#'))
		{
			strtokenizer strtokens(line,"=");
			int num=strtokens.count_tokens();
			if (num<2) continue;
			par = strtokens.token(0);
			val = strtokens.token(1);
			trim2(par);
			trim2(val);
			//cerr << par << " = " << val << endl;
			recog_line = false;

			if (par == "Distance cutoff") { dist_cutoff = atof(val.c_str()); recog_line = true; }
            if (par == "Distance cutoff non-continuous") { dist_cutoff_ncont=atof(val.c_str()); recog_line=true; }

            if (par == "Number of bins")
            {
            	n_bins = atoi(val.c_str());
	    		Resize(n_bins, PW_EE_cont_Energy);
	    		Resize(n_bins, PW_HH_cont_Energy);
	    		Resize(n_bins, PW_CC_cont_Energy);
	
	    		Resize(n_bins, PW_0EEap_ncont_Energy);
	    		Resize(n_bins, PW_0EEp_ncont_Energy);
	    		Resize(n_bins, PW_0A_ncont_Energy);
	    		Resize(n_bins, PW_1A_ncont_Energy);
	    		Resize(n_bins, PW_2A_ncont_Energy);

                recog_line = true;
            }

            if (par == "PW_EE_cont_Energy parameter file") { PW_EE_cont_Energy_par_file = val; recog_line = true; }
            if (par == "PW_HH_cont_Energy parameter file") { PW_HH_cont_Energy_par_file = val; recog_line = true; }
            if (par == "PW_CC_cont_Energy parameter file") { PW_CC_cont_Energy_par_file = val; recog_line = true; }
            if (par == "PW_0EEap_ncont_Energy parameter file") { PW_0EEap_ncont_Energy_par_file = val; recog_line = true; }
            if (par == "PW_0EEp_ncont_Energy parameter file") { PW_0EEp_ncont_Energy_par_file = val; recog_line = true; }
            if (par == "PW_0A_ncont_Energy parameter file") { PW_0A_ncont_Energy_par_file = val; recog_line = true; }
            if (par == "PW_1A_ncont_Energy parameter file") { PW_1A_ncont_Energy_par_file = val; recog_line = true; }
            if (par == "PW_2A_ncont_Energy parameter file") { PW_2A_ncont_Energy_par_file = val; recog_line = true; }

            if (par == "PW_EE_cont_Energy coefficient") { PW_EE_cont_Energy_coefficient = atof(val.c_str()); recog_line = true; }
            if (par == "PW_HH_cont_Energy coefficient") { PW_HH_cont_Energy_coefficient = atof(val.c_str()); recog_line = true; }
            if (par == "PW_CC_cont_Energy coefficient") { PW_CC_cont_Energy_coefficient = atof(val.c_str()); recog_line = true; }
            if (par == "PW_0EEap_ncont_Energy coefficient") { PW_0EEap_ncont_Energy_coefficient = atof(val.c_str()); recog_line = true; }
            if (par == "PW_0EEp_ncont_Energy coefficient") { PW_0EEp_ncont_Energy_coefficient = atof(val.c_str()); recog_line = true; }
            if (par == "PW_0A_ncont_Energy coefficient") { PW_0A_ncont_Energy_coefficient = atof(val.c_str()); recog_line = true; }
            if (par == "PW_1A_ncont_Energy coefficient") { PW_1A_ncont_Energy_coefficient = atof(val.c_str()); recog_line = true; }
            if (par == "PW_2A_ncont_Energy coefficient") { PW_2A_ncont_Energy_coefficient = atof(val.c_str()); recog_line = true; }

            if (par == "PW_EE_cont_Energy steric_only") { PW_EE_cont_Energy_steric_only = (val=="yes"); recog_line = true; }
            if (par == "PW_HH_cont_Energy steric_only") { PW_HH_cont_Energy_steric_only = (val=="yes"); recog_line = true; }
            if (par == "PW_CC_cont_Energy steric_only") { PW_CC_cont_Energy_steric_only = (val=="yes"); recog_line = true; }
            if (par == "PW_0EEap_ncont_Energy steric_only") { PW_0EEap_ncont_Energy_steric_only = (val=="yes"); recog_line = true; }
            if (par == "PW_0EEp_ncont_Energy steric_only") { PW_0EEp_ncont_Energy_steric_only = (val=="yes"); recog_line = true; }
            if (par == "PW_0A_ncont_Energy steric_only") { PW_0A_ncont_Energy_steric_only = (val=="yes"); recog_line = true; }
            if (par == "PW_1A_ncont_Energy steric_only") { PW_1A_ncont_Energy_steric_only = (val=="yes"); recog_line = true; }
            if (par == "PW_2A_ncont_Energy steric_only") { PW_2A_ncont_Energy_steric_only = (val=="yes"); recog_line = true; }


            if (par == "ESP file") { esp_par_file = val; recog_line = true; }

            if (par == "Add bonded atoms") { add_bonded = (val=="yes"); recog_line = true; }

            if (par == "PENALTY COEFFICIENT") { Penalty_coeff = atof(val.c_str()); recog_line = true; }
            if (par == "BURIAL SPHERE RADIUS") { Burial_sphere_rad = atof(val.c_str()); recog_line = true; }

            if (par == "Add backbone atoms") { add_backbone = (val=="yes"); recog_line = true; }
            if (par == "Add hydrogen atoms") { add_hydrogens = (val=="yes"); recog_line = true; }
            if (par == "Add side-chain centroids") { add_centroids = (val=="yes"); recog_line = true; }
            if (par == "Add side-chain atoms") { add_side_chains = (val=="yes"); recog_line = true; }
            if (par == "Add beta-carbons only") { add_only_CB = (val=="yes"); recog_line = true; }
            if (par == "Minimize CB_CB only") { CB_CB_focus = (val=="yes"); recog_line = true; }

            if (par == "Calculate side-chain energy") { calc_SC_energy = (val=="yes"); recog_line = true; }

            if (par == "Minimum chain distance") { min_chain_dist = atoi(val.c_str()); recog_line = true; }
            if (par == "Maximum chain distance") { max_chain_dist = atoi(val.c_str()); recog_line = true; }

            if (par == "Minimum CB_CB distance") { min_CB_CB_dist = atoi(val.c_str()); recog_line = true; }

            if (par == "Energy coefficient") { energy_coeff = atof(val.c_str()); recog_line = true; }
            if (par == "CB_CB coefficient") { CB_CB_coefficient = atof(val.c_str()); recog_line = true; }
        }

        if (CfgFile.eof()) break;
    }
    CfgFile.close();
    return 0;
}

void DopePW::Init(string cfgFile)
{
	ReadConfigFile(cfgFile);
	LoadFromFile(PW_EE_cont_Energy_par_file, PW_EE_cont_Energy);
	LoadFromFile(PW_HH_cont_Energy_par_file, PW_HH_cont_Energy);
	LoadFromFile(PW_CC_cont_Energy_par_file, PW_CC_cont_Energy);
	LoadFromFile(PW_0EEap_ncont_Energy_par_file, PW_0EEap_ncont_Energy);
	LoadFromFile(PW_0EEp_ncont_Energy_par_file, PW_0EEp_ncont_Energy);
	LoadFromFile(PW_0A_ncont_Energy_par_file, PW_0A_ncont_Energy);
	LoadFromFile(PW_1A_ncont_Energy_par_file, PW_1A_ncont_Energy);
	LoadFromFile(PW_2A_ncont_Energy_par_file, PW_2A_ncont_Energy);

	secseq = "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE";

	if (esp_par_file.size()>0)
	{
		string e0_str, e1_str, e2_str;
		int e0, e1, e2;
		vector<int> ESP_energy_AA;
	
		ifstream par_file;
		par_file.open(esp_par_file.c_str(), ios::in);
	    if (!par_file.fail())
	    {
			while (true)
			{
				par_file >> e0_str;
				par_file >> e1_str;
				par_file >> e2_str;
		
				if (par_file.eof()) break;
			
				//cerr << e0_str << " " << e1_str << " " << e2_str << endl;
		
				ESP_energy_AA.assign(3, 0);
		
				e0 = atoi(e0_str.c_str());
				e1 = atoi(e1_str.c_str());
				e2 = atoi(e2_str.c_str());
		
				ESP_energy_AA[0] = e0;
				ESP_energy_AA[1] = e1;
				ESP_energy_AA[2] = e2;
			
				ESP_energy.push_back(ESP_energy_AA);
			}
			par_file.close();
		}
	}
}

double DopePW::DOPEenergy(string res_id0, int seq0, string atom0, string res_id1, int seq1, string atom1, double distance, int ss0, int ss1, int seq_len)
{
	double r1, f1, f2, bin_size, pw_coef=0.0;
	double e_pair=0;
	int ri, pap, cont_SecStr;
	bool continuous= false, use_attractive= false;
	
	if (0.0 >= distance || distance >= dist_cutoff) {
		//cerr << distance<< ">=" << dist_cutoff << ": so return 0" << endl;
		e_pair = 0.0;
		return e_pair;
	}

	int aa0 = GetResidueCode(res_id0, 0);
	int c0 = GetAtomCode(atom0, 0);
	int aa1 = GetResidueCode(res_id1, 0);
	int c1 = GetAtomCode(atom1, 0);
	if (aa0==-1 || aa1==-1 || c0==-1|| c1==-1)  // c0 and c1 are the atom code for CA, C, O, N, H and CB etc.
	{
		//cerr << "wrong amino acid code" << endl;
		return 0;    
	}
	bool CB0=(atom0=="CB"), CB1=(atom1=="CB"); 
	int cd = abs(seq0-seq1);

	if (ss0 > 1) cont_SecStr = 2;
	else cont_SecStr = ss0;

	pap = pap_vec[seq0][seq1];
	if (continuity_vec[seq0] == continuity_vec[seq1]) continuous = true;

	bin_size = dist_cutoff / n_bins; 

	// The distance lies between middle points of bins ri and ri + 1.
	r1 = distance / bin_size - 0.5;
	ri = int(r1);

	// Linear interpolation between the energy value at those bins.
	f1 = r1 - ri;
	f2 = 1.0 - f1;

	if (0 >= ri) {
		e_pair = 10.0;
		//cerr << "ri is less than or equal to zero: " << ri << endl;
		return e_pair;
	}

	int idx = ri+n_bins*(c1+(max_atom_code+1)*(aa1+20*(c0+(max_atom_code+1)*aa0)));//[idx];
	//cerr << "DOPEenergy: c_S="<<cont_SecStr<<"("<<n_bins<<","<<max_atom_code<<"); pap="<<pap<<"; ss="<<ss0<<"-"<<ss1<<endl;
	//cerr << res_id0<<","<<seq0<<","<<atom0<<","<<res_id1<<","<<seq1 <<","<<atom1 <<","<<ri<<","<< distance<< endl;
	// When ri = n_bins - 1, DopePW(ri + 1) gives 0, so we are interpolating
	// between the last energy value stored in the table and 0, which is ok.
	//cerr << "continuous=" << continuous<<"; ss="<<ss0<<"-"<<ss1<< " ";
	if (continuous) {
		switch (cont_SecStr)
		{
		case 0:
			e_pair = f2 * PW_HH_cont_Energy[idx] + f1 * PW_HH_cont_Energy[idx + 1];
			pw_coef= PW_HH_cont_Energy_coefficient;
			//cerr << "PW_HH_cont_Energy="<<PW_HH_cont_Energy[idx]<<"; "<<PW_HH_cont_Energy[idx + 1]<< endl;
			break;
		case 1:
			e_pair = f2 * PW_EE_cont_Energy[idx] + f1 * PW_EE_cont_Energy[idx + 1];
			pw_coef= PW_EE_cont_Energy_coefficient;
			//cerr << "PW_EE_cont_Energy="<<PW_EE_cont_Energy[idx]<<"; "<<PW_EE_cont_Energy[idx + 1]<< endl;
			break;
		case 2:
			e_pair = f2 * PW_CC_cont_Energy[idx] + f1 * PW_CC_cont_Energy[idx + 1];
			pw_coef= PW_CC_cont_Energy_coefficient;
			//cerr << "PW_CC_cont_Energy="<<PW_CC_cont_Energy[idx]<<"; "<<PW_CC_cont_Energy[idx + 1]<< endl;
			break;
		}
		if (distance > 10.0) e_pair = 0.00;
		if (distance > 6.00 && e_pair > 0.0) e_pair = 0.0;
		if (e_pair < 0.0) e_pair = e_pair * pw_coef;
		//if (distance >= 7.0) e_pair = 0.0;
		e_pair = 0.0;
	} else if (ss0 == 1 && ss1 == 1) {
		switch (pap)
		{
		case 0:
			cerr << "Not found pap. ";
			break;
		case 1:
			e_pair = f2 * PW_0EEp_ncont_Energy[idx] + f1 * PW_0EEp_ncont_Energy[idx + 1];
			pw_coef= PW_0EEp_ncont_Energy_coefficient;
			//cerr << "PW_0EEp_ncont_Energy="<<PW_0EEp_ncont_Energy[idx]<<"; "<<PW_0EEp_ncont_Energy[idx+1]<< endl;
			use_attractive = false;
			if (e_pair < 0.0) {
				//cerr << "pap=1, EE:" << secseq[seq0]<<secseq[seq1]<<endl;
				if (seq0>0 && seq0<seq_len-1 && seq1>0 && seq1<seq_len-1
					&&((cd>4&&secseq[seq0]=='A'&&secseq[seq1]=='A')||(cd>2&&(secseq[seq0]!='A'||secseq[seq1]!='A'))))
				{
					if (ON_hbond_vec[seq0-1][seq1]==1 || NO_hbond_vec[seq0+1][seq1]==1 
						|| NO_hbond_vec[seq0][seq1-1]==1 || ON_hbond_vec[seq0][aa1+1]==1
				   		|| NO_hbond_vec[seq0+1][seq1-1]==1 || ON_hbond_vec[seq0-1][seq1+1]==1 
						|| (ON_hbond_vec[seq0][seq1]==1 && NO_hbond_vec[seq0][seq1]==0) 
				    	|| (ON_hbond_vec[seq0][seq1]==0 && NO_hbond_vec[seq0][seq1]==1))
					{
						use_attractive = true; //cerr << "use_attractive"<<endl;
					}
					if (!use_attractive) e_pair = 0.0;
				}
				else e_pair = 0.0;
			}
			break;
		case 2:
			e_pair = f2 * PW_0EEap_ncont_Energy[idx] + f1 * PW_0EEap_ncont_Energy[idx + 1];
			pw_coef= PW_0EEap_ncont_Energy_coefficient;
			//cerr << "PW_0EEap_ncont_Energy="<<PW_0EEap_ncont_Energy[idx]<<"; "<<PW_0EEap_ncont_Energy[idx + 1]<< endl;
			use_attractive = false;
			if (e_pair<0.0 && cd>4) {
				//cerr << "pap=2, EE:" << seq0<<"-"<<atom0<<":"<<seq1<<"-"<<atom1<<endl;
				
				if (ON_hbond_vec[seq0][seq1]==1 || NO_hbond_vec[seq0][seq1]==1)
					use_attractive = true;
				if (seq0>0 && seq0<seq_len-1 && seq1>0 && seq1<seq_len-1) {
 					if ((ON_hbond_vec[seq0+1][seq1-1]==1 || NO_hbond_vec[seq0+1][seq1-1]==1)
						&& (ON_hbond_vec[seq0-1][seq1+1]==1 || NO_hbond_vec[seq0-1][seq1+1]==1))
					{
						use_attractive = true;
					}
				}
				if (seq0>0 && seq0<seq_len-1 && seq1>1 && seq1<seq_len-2) {
				    if (((ON_hbond_vec[seq0-1][seq1]==1 || NO_hbond_vec[seq0-1][seq1]==1) 
						&& (ON_hbond_vec[seq0+1][seq1-2]==1 || NO_hbond_vec[seq0+1][seq1-2]==1))
				    	|| ((ON_hbond_vec[seq0+1][seq1]==1 || NO_hbond_vec[seq0+1][seq1]==1)
						&& (ON_hbond_vec[seq0-1][seq1+2]==1 || NO_hbond_vec[seq0-1][seq1+2]==1))) 
					{
						use_attractive = true;
					}
				}
				if (seq0>1 && seq0<seq_len-2 && seq1>0 && seq1<seq_len-1) {
					if (((ON_hbond_vec[seq0][seq1+1]==1 || NO_hbond_vec[seq0][seq1+1]==1)
						&& (ON_hbond_vec[seq0+2][seq1-1]==1 || NO_hbond_vec[seq0+2][seq1-1]==1))
					|| ((ON_hbond_vec[seq0][seq1-1]==1 || NO_hbond_vec[seq0][seq1-1]==1)
						&& (ON_hbond_vec[seq0-2][seq1+1]==1 || NO_hbond_vec[seq0-2][seq1+1]==1)))
					{
						use_attractive = true;
					}
				}
				if (!use_attractive) e_pair = 0.0;
			}
			break;
		}
		if (distance > 6.00 && e_pair > 0.0) e_pair = 0.0;
		if (e_pair < 0.0) e_pair = e_pair * pw_coef;
		//e_pair = 0.0;
	}
//*
	else {
		e_pair = f2 * PW_0A_ncont_Energy[idx] + f1 * PW_0A_ncont_Energy[idx + 1];
		if (!CB1 && !CB0) {
			if (distance > 10.00) e_pair = 0.0;
			if (distance > 4.00 && e_pair > 0.0) e_pair = 0.0;
			if (e_pair < 0.0 && cd < 7) e_pair = 0.0;
			if (e_pair < 0.0) e_pair = e_pair * PW_0A_ncont_Energy_coefficient;
		} else {
			if (distance > 4.00 && e_pair > 0.0 && cd < 5) e_pair = 0.0;
			if (e_pair < 0.0 && cd < 7) e_pair = 0.0;
			if (distance > 8.00 && e_pair > 0.0) e_pair = 0.0;
		}
		e_pair = 0.0;
	}
//*/

	return e_pair;
}

double DopePW::PropWashEnergy(string res_id0, int seq0, string atom0, string res_id1, int seq1, string atom1, double distance)
{
	double r1, f1, f2, bin_size;
	double e_pair=0.0, pw_coef=0.0;
	int ri, PropWash;

	int aa0 = GetResidueCode(res_id0, 0);
	int c0 = GetAtomCode(atom0, 0);
	int aa1 = GetResidueCode(res_id1, 0);
	int c1 = GetAtomCode(atom1, 0);
	if (aa0==-1 || aa1==-1 || c0==-1|| c1==-1)  // c0 and c1 are the atom code for CA, C, O, N, H and CB etc.
		return 0;    
	bool CB0=(atom0=="CB"), CB1=(atom1=="CB"); 
	int cd = abs(seq0-seq1);
	PropWash = dope_vec[seq0][seq1];

	if (0.0<distance && distance<dist_cutoff_ncont) {
		bin_size = dist_cutoff_ncont / n_bins; 

		// The distance lies between middle points of bins ri and ri + 1.
		r1 = distance / bin_size - 0.5;
		ri = int(r1);

		// Linear interpolation between the energy value at those bins.
		f1 = r1 - ri;
		f2 = 1.0 - f1;

		if (0 < ri) {
			int idx = ri+n_bins*(c1+(max_atom_code+1)*(aa1+20*(c0+(max_atom_code+1)*aa0)));//[idx];
			//cerr << "PropWash="<<PropWash<<"("<<n_bins<<","<<max_atom_code<<"); cd="<<cd<<"; CBs="<<CB0<<"-"<<CB1<<endl;
			//cerr << res_id0<<","<<seq0<<","<<atom0<<","<<res_id1<<","<<seq1 <<","<<atom1 <<","<<ri<<","<< distance<< endl;
			// When ri = n_bins - 1, DopePW(ri + 1) gives 0, so we are interpolating
			// between the last energy value stored in the table and 0, which is ok.
			switch (PropWash)
			{
/*
			case 0:
				e_pair = f2 * PW_0A_ncont_Energy[idx] + f1 * PW_0A_ncont_Energy[idx + 1];
				pw_coef = PW_0A_ncont_Energy_coefficient;
				break;
*/			case 1:
				e_pair = f2 * PW_1A_ncont_Energy[idx] + f1 * PW_1A_ncont_Energy[idx + 1];
				pw_coef = PW_1A_ncont_Energy_coefficient;
				//cerr << "PW_1A_ncont_Energy="<<PW_1A_ncont_Energy[idx]<<"; "<<PW_1A_ncont_Energy[idx + 1]<< endl;
				break;
			case 2:
				e_pair = f2 * PW_2A_ncont_Energy[idx] + f1 * PW_2A_ncont_Energy[idx+ 1];
				pw_coef = PW_2A_ncont_Energy_coefficient;
				//cerr << "PW_2A_ncont_Energy="<<PW_2A_ncont_Energy[idx]<<"; "<<PW_2A_ncont_Energy[idx + 1]<< endl;
				break;
			}
			if (!CB1 && !CB0) {
				if (distance > 10.00) e_pair = 0.0;
				if (distance > 4.00 && e_pair > 0.0) e_pair = 0.0;
				if (e_pair < 0.0 && cd < 7) e_pair = 0.0;
				if (e_pair < 0.0) e_pair = e_pair * pw_coef;
			} else {
				if (distance > 4.00 && e_pair > 0.0 && cd < 5) e_pair = 0.0;
				if (distance > 8.00 && e_pair > 0.0) e_pair = 0.0;
				if (e_pair < 0.0 && cd < 7) e_pair = 0.0;
			}
		}
	}

	e_pair = 0.0;
	return e_pair;

}

double DopePW_RAMA::DOPE_PW_continuous(string res0, string atom0, string res1, string atom1, double distance, int SecStr_val)
{
	double r1, f1, f2, bin_size;
	double e_pair=0;
	int ri;
	
	int aa0 = GetResidueCode(res0, 0);
	int c0 = GetAtomCode(atom0, 0);
	int aa1 = GetResidueCode(res1, 0);
	int c1 = GetAtomCode(atom1, 0);
	if (aa0==-1 || aa1==-1 || c0==-1|| c1==-1)  // c0 and c1 are the atom code for CA, C, O, N, H and CB etc.
		return 0;

	if ((0.0 < distance) && (distance < dist_cutoff)) {
		bin_size = dist_cutoff / n_bins; 
		
		// The distance lies between middle points of bins ri and ri + 1.
		r1 = distance / bin_size - 0.5;
		ri = int(r1);
		
		// Linear interpolation between the energy value at those bins.
		f1 = r1 - ri;
		f2 = 1.0 - f1;
		
		if (0 < ri) {
			int idx = ri+n_bins*(c1+(max_atom_code+1)*(aa1+20*(c0+(max_atom_code+1)*aa0)));//[idx];
			// When ri = n_bins - 1, StatPot(ri + 1) gives 0, so we are interpolating
			// between the last energy value stored in the table and 0, which is ok.
			//switch (SecStr_vec[aa0])
			switch (SecStr_val)
			{
			case 0:
				e_pair = f2 * PW_HH_cont_Energy[idx] + f1 * PW_HH_cont_Energy[idx + 1];
				if (distance > 6.00 && e_pair > 0.0) e_pair = 0.0;
				if (e_pair < 0.0) e_pair = e_pair * PW_HH_cont_Energy_coefficient;
				break;
			case 1:
				e_pair = f2 * PW_EE_cont_Energy[idx] + f1 * PW_EE_cont_Energy[idx + 1];
				if (distance > 6.00 && e_pair > 0.0) e_pair = 0.0;
				if (e_pair < 0.0) e_pair = e_pair * PW_EE_cont_Energy_coefficient;
				break;
			case 2:
				e_pair = f2 * PW_CC_cont_Energy[idx] + f1 * PW_CC_cont_Energy[idx + 1];
				if (distance > 6.00 && e_pair > 0.0) e_pair = 0.0;
				if (e_pair < 0.0) e_pair = e_pair * PW_CC_cont_Energy_coefficient;
				break;
			}
		} else e_pair = 10.0;
	} else e_pair = 0.0;
//cerr << "DOPE_PW_continuous("<<res0<<","<<atom0<<"),("<<res1<<","<<atom1<<"),"<<ri<<"~"<<distance<<": "<<e_pair<<endl;
	return e_pair;
}

double DopePW_RAMA::DOPE_PW_strand(string res0, string atom0, string res1, string atom1, double distance, int EE_val)
{
	double r1, f1, f2, bin_size;
	double e_pair=0;
	int ri;
	
	int aa0 = GetResidueCode(res0, 0);
	int c0 = GetAtomCode(atom0, 0);
	int aa1 = GetResidueCode(res1, 0);
	int c1 = GetAtomCode(atom1, 0);
	if (aa0==-1 || aa1==-1 || c0==-1|| c1==-1)  // c0 and c1 are the atom code for CA, C, O, N, H and CB etc.
		return 0;
	
	if ((0.0 < distance) && (distance < dist_cutoff)) {
		bin_size = dist_cutoff / n_bins; 
		
		// The distance lies between middle points of bins ri and ri + 1.
		r1 = distance / bin_size - 0.5;
		ri = int(r1);
		
		// Linear interpolation between the energy value at those bins.
		f1 = r1 - ri;
		f2 = 1.0 - f1;
		
		int cd = aa1 - aa0;
		
		if (0 < ri) {
			int idx = ri+n_bins*(c1+(max_atom_code+1)*(aa1+20*(c0+(max_atom_code+1)*aa0)));//[idx];
			//switch (EE_vec[aa0][aa1])
			switch (EE_val)
			{
			case 1:
//cerr << endl<<PW_0EEp_ncont_Energy[idx]<<"##"<<PW_0EEp_ncont_Energy[idx + 1];
				e_pair = f2 * PW_0EEp_ncont_Energy[idx] + f1 * PW_0EEp_ncont_Energy[idx + 1];
				if (distance > 6.00 && e_pair > 0.0) e_pair = 0.0;
				if (e_pair < 0.0) e_pair = e_pair * PW_0EEp_ncont_Energy_coefficient;
				break;
			case 2:
				e_pair = f2 * PW_0EEap_ncont_Energy[idx] + f1 * PW_0EEap_ncont_Energy[idx + 1];
				if (distance > 6.00 && e_pair > 0.0) e_pair = 0.0;
				if (e_pair < 0.0 && cd > 7) e_pair = e_pair * PW_0EEap_ncont_Energy_coefficient;
				break;
			}
		}
		else e_pair = 10.0;
	}
	else e_pair = 0.0;
	
//cerr << "****DOPE_PW_strand("<<res0<<","<<atom0<<c0<<"),(" <<res1<<"," <<atom1<<c1<<"),<"<<EE_val<<","<<ri<<","<<distance<<">: " << e_pair <<endl;
	return e_pair;
}

double DopePW_RAMA::DOPE_PW_0(string res0, string atom0, string res1, string atom1, double distance, bool CB, int cd)
{
	double r1, f1, f2, bin_size;
	double e_pair;
	int ri;
	
	int aa0 = GetResidueCode(res0, 0);
	int c0 = GetAtomCode(atom0, 0);
	int aa1 = GetResidueCode(res1, 0);
	int c1 = GetAtomCode(atom1, 0);
	if (aa0==-1 || aa1==-1 || c0==-1|| c1==-1)  // c0 and c1 are the atom code for CA, C, O, N, H and CB etc.
		return 0;

	if ((0.0 < distance) && (distance < dist_cutoff)) {
		bin_size = dist_cutoff / n_bins; 

		// The distance lies between middle points of bins ri and ri + 1.
		r1 = distance / bin_size - 0.5;
		ri = int(r1);

		// Linear interpolation between the energy value at those bins.
		f1 = r1 - ri;
		f2 = 1.0 - f1;

		if (0 < ri) {
			int idx = ri+n_bins*(c1+(max_atom_code+1)*(aa1+20*(c0+(max_atom_code+1)*aa0)));//[idx];
			e_pair = f2 * PW_0A_ncont_Energy[idx] + f1 * PW_0A_ncont_Energy[idx + 1];
			if (!CB) {
				if (distance > 10.00) e_pair = 0.0;
				if (distance > 4.00 && e_pair > 0.0) e_pair = 0.0;
				if (e_pair < 0.0 && cd < 7) e_pair = 0.0;
				if (e_pair < 0.0) e_pair = e_pair * PW_0A_ncont_Energy_coefficient;
			} else {
				if (distance > 4.00 && e_pair > 0.0 && cd < 5) e_pair = 0.0;
				if (e_pair < 0.0 && cd < 7) e_pair = 0.0;
				if (distance > 8.00 && e_pair > 0.0) e_pair = 0.0;
			}
		}
		else e_pair = 10.0;
	}
	else e_pair = 0.0;

//cerr << "DOPE_PW_0("<<res0<<","<<atom0<<"),(" <<res1<<"," <<atom1<<"),"<<ri<<"~"<<distance<<": " << e_pair <<endl;
	return e_pair;
}

double DopePW_RAMA::PropWashEnergy(string res0, string atom0, string res1, string atom1, double distance, bool CB, int cd, int pw_val)
{
	double r1, f1, f2, bin_size;
	double e_pair=0;
	int ri=0;
	
	int aa0 = GetResidueCode(res0, 0);
	int c0 = GetAtomCode(atom0, 0);
	int aa1 = GetResidueCode(res1, 0);
	int c1 = GetAtomCode(atom1, 0);
//cerr << "PropWashEnergy("<<res0<<","<<atom0<<"),(" <<res1<<"," <<atom1<<")," <<distance<< aa0<<c0<<aa1<<c1 <<endl;
	if (aa0==-1 || aa1==-1 || c0==-1|| c1==-1)  // c0 and c1 are the atom code for CA, C, O, N, H and CB etc.
		return 0;

	if ((0.0 < distance) && (distance < dist_cutoff_ncont)) {
		bin_size = dist_cutoff_ncont / n_bins;

		// The distance lies between middle points of bins ri and ri + 1.
		r1 = distance / bin_size - 0.5;
		ri = int(r1);

		// Linear interpolation between the energy value at those bins.
		f1 = r1 - ri;
		f2 = 1.0 - f1;

		if (0 < ri) {
			int idx = ri+n_bins*(c1+(max_atom_code+1)*(aa1+20*(c0+(max_atom_code+1)*aa0)));//[idx];
			// When ri = n_bins - 1, StatPot(ri + 1) gives 0, so we are interpolating
			// between the last energy value stored in the table and 0, which is ok.
			//switch (pw_vec[aa0][aa1])
			switch (pw_val)
			{
				case 1:
					e_pair = f2 * PW_1A_ncont_Energy[idx] + f1 * PW_1A_ncont_Energy[idx + 1];
					if (!CB) {
						if (distance > 10.00) e_pair = 0.0;
						if (distance > 4.00 && e_pair > 0.0) e_pair = 0.0;
						if (e_pair < 0.0 && cd < 7) e_pair = 0.0;
						if (e_pair < 0.0) e_pair = e_pair * PW_1A_ncont_Energy_coefficient;
					} else {
						if (distance > 4.00 && e_pair > 0.0 && cd < 5) e_pair = 0.0;
						if (distance > 8.00 && e_pair > 0.0) e_pair = 0.0;
						if (e_pair < 0.0 && cd < 7) e_pair = 0.0;
					}
					break;
				case 2:
					e_pair = f2 * PW_2A_ncont_Energy[idx] + f1 * PW_2A_ncont_Energy[idx + 1];
					if (!CB) {
						if (distance > 10.00) e_pair = 0.0;
						if (distance > 4.00 && e_pair > 0.0) e_pair = 0.0;
						if (e_pair < 0.0 && cd < 7) e_pair = 0.0;
						if (e_pair < 0.0) e_pair = e_pair * PW_2A_ncont_Energy_coefficient;
					} else {
						if (distance > 4.00 && e_pair > 0.0 && cd < 5) e_pair = 0.0;
						if (distance > 8.00 && e_pair > 0.0) e_pair = 0.0;
						if (e_pair < 0.0 && cd < 7) e_pair = 0.0;
					}
					break;
			}
		}
		else e_pair = 10.0;
	}
	else e_pair = 0.0;

//cerr << "PropWashEnergy("<<res0<<","<<atom0<<"),(" <<res1<<"," <<atom1<<"),"<<ri<<"," <<distance<<"~" <<dist_cutoff_ncont<<": " << e_pair <<endl;
	return e_pair;
}

void DopePW_RAMA::FindBetaStrands(int seq_length)
{
	for (int aa0 = 0; aa0 < seq_length; aa0++)
	for (int aa1 = aa0 + 5; aa1 < seq_length; aa1++) {
		switch (pap_vec[aa0][aa1])
		{
		case 0:
			break;
		case 1:
			if (((HO_hbond_vec[aa0][aa1-1]==1 && OH_hbond_vec[aa0][aa1+1]==1) || (HO_hbond_vec[aa0+1][aa1]==1 && OH_hbond_vec[aa0-1][aa1]==1)) && P_align[aa0][aa1]==1 && COCO_align[aa0-1][aa1-1]==1 && NHNH_align[aa0+1][aa1+1]==1)
			{ 
				EE_vec[aa0][aa1] = EE_vec[aa0-1][aa1-1] = EE_vec[aa0-1][aa1] = EE_vec[aa0][aa1-1] = EE_vec[aa0+1][aa1] = EE_vec[aa0][aa1+1] = EE_vec[aa0+1][aa1+1] = 1;
				SecStr_vec[aa0] = SecStr_vec[aa1] = SecStr_vec[aa0-1] = SecStr_vec[aa0+1] = SecStr_vec[aa1-1] = SecStr_vec[aa1+1] = 1;
			}
			break;
		case 2:
			if (OH_hbond_vec[aa0-1][aa1+1]==1 && HO_hbond_vec[aa0+1][aa1-1]==1 && CONH_align[aa0-1][aa1+1]==1 && NHCO_align[aa0+1][aa1-1]==1 && AP_align[aa0][aa1]==1)
			{
				EE_vec[aa0][aa1] = EE_vec[aa0-1][aa1+1] = EE_vec[aa0-1][aa1] = EE_vec[aa0][aa1-1] = EE_vec[aa0+1][aa1] = EE_vec[aa0][aa1+1] = EE_vec[aa0+1][aa1-1] = 2;
				SecStr_vec[aa0] = SecStr_vec[aa1] = SecStr_vec[aa0 - 1] = SecStr_vec[aa0 + 1] = SecStr_vec[aa1 - 1] = SecStr_vec[aa1 + 1] = 1;
			}
			break;
		}
	}
}

void DopePW_RAMA::SetContinuity(int seq_length)
{
	int ss0, ss1, aa0, cont_flag = 0;

	ss0 = SecStr_vec[0];
	continuity_vec[0] = cont_flag;
	for (aa0 = 1; aa0 < seq_length; aa0++) {
		ss1 = SecStr_vec[aa0];
		if (ss1 != ss0) 
			cont_flag++;
		continuity_vec[aa0] = cont_flag;
		ss0 = ss1;
	}
}

