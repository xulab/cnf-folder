/*******************************************************************************

                                 BMKhbond.h
                                 ----------

  ****************************************************************************
  *                                                                          *
  *   This library is free software; you can redistribute it and/or          *
  *   modify it under the terms of the GNU Lesser General Public             *
  *   License as published by the Free Software Foundation; either           *
  *   version 2.1 of the License, or (at your option) any later version.     *
  *                                                                          *
  *   This library is distributed in the hope that it will be useful,        *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *
  *   Lesser General Public License for more details.                        *
  *                                                                          *
  *   You should have received a copy of the GNU Lesser General Public       *
  *   License along with this library; if not, write to the Free Software    *
  *   Foundation, Inc.,                                                      *
  *   59 Temple Place, Suite 330, Boston, MA  02111-1307  USA                *
  *                                                                          *
  ****************************************************************************

  Copyright (C) 2005 The University of Chicago

  Authors:
  Andres Colubri

  Description:
  PL plugin to calculate the Hydrogen Bond potential from Kortemme,
  Morozov & Baker.

*******************************************************************************/

#ifndef __BMKHBOND_H__
#define __BMKHBOND_H__

// Interaction types.
const int HELIX_HELIX = 0;
const int STRAND_STRAND = 1;
const int OTHER = 2;

inline double LinearInterpolation(double f0, double f1, double x0, double x, double inv_dx)
{
    double y = inv_dx * (x - x0);
    return f1 * y + f0 * (1 - y);
}

class TDeltaEnergy
{
public:
    bool InRange(double delta);
    double Energy(int int_type, double delta);
    double Minimum(int int_type);
    int LoadFromFile(int int_type, string &fn);

    double operator () (int int_type, double delta) { return Energy(int_type, delta); }
private:
    double values[3][32];

    int idx(double delta) { return int(20 * (delta - 1.425)); }
};

class TChiEnergy
{
public:
    double Energy(int int_type, double chi);
    double Minimum(int int_type);
    int LoadFromFile(int int_type, string &fn);

    double operator () (int int_type, double chi) { return Energy(int_type, chi); }
private:
    double values[3][72];

    int idx(double chi) { return int(0.2 * (chi + 177.5)); }
};

class TAngleEnergy
{
public:
    double Energy(int int_type, double angle);
    double Minimum(int int_type);
    int LoadFromFile(int int_type, string &fn);

    double operator () (int int_type, double angle) { return Energy(int_type, angle); }
private:
    double values[3][36];

    int idx(double angle) { return int(0.2 * (angle - 2.5)); }
};

class TBMKhbond
{
public:
    TBMKhbond();
    ~TBMKhbond();

    void SetDefConfig();
    int ReadConfigFile(string CfgName);

	void Init(string CfgName);

	// compute the pair energy between a Backbone CO of residue1 and a Backbone NH of residue2.
	// ChainDist is the chain distance between two atoms
	// delta is the euclid distance between two atoms
	// theta = VectAngle(NH, OH);
	// psi = VectAngle(HO, CO);
	// chi = DihedralAngle(CA_1->Pos, C_1->Pos, O_1->Pos, H_2->Pos);
	// CO_burial= CO_burial(id1, aa1), NH_burial= NH_burial(id2, aa2)
	double BMKHBPairEnergy(int ss1, int ss2, int ChainDist, double delta, double theta, double psi, double chi, 
							double CO_burial, double NH_burial);

	void trim2(string& str) {
		string::size_type pos = str.find_last_not_of(' ');
		if(pos != string::npos) {
			str.erase(pos + 1);
			pos = str.find_first_not_of(' ');
			if(pos != string::npos) 
				str.erase(0, pos);
		}
		else str.erase(str.begin(), str.end());
	};

	double HBond_Min_Energy;
	bool Burial_renorm;
	double Energy_coeff, Penalty_coeff, Burial_sphere_rad;
	int Burial_min_atcount, Burial_max_atcount;

	double Min_delta, Max_delta;
	int MinChainDist;

private:
    int IntType(int ss1, int ss2);

    double Min_theta, Max_theta;
    double Min_psi, Max_psi;
    bool Cap_energy_to_zero;
    string Delta_helix_parfile, Delta_strand_parfile, Delta_other_parfile;
    string Chi_helix_parfile, Chi_strand_parfile, Chi_other_parfile;
    string Theta_helix_parfile, Theta_strand_parfile, Theta_other_parfile;
    string Psi_helix_parfile, Psi_strand_parfile, Psi_other_parfile;
    string SecStr_plugin, SecStr_config;
    double Delta_coeff, Theta_coeff, Psi_coeff, Chi_coeff;

    TDeltaEnergy delta_energy;
    TChiEnergy chi_energy;
    TAngleEnergy theta_energy, psi_energy;
};

#endif
