/*******************************************************************************

                                  DOPE.cpp
                                  --------

  Copyright (C) 2005 The University of Chicago

  Authors: 
  Andr�s Colubri

  Description:
  Plugin that calculates pairwise statistical potentials. In particular, it 
  can calculate DOPE, the statistical potential created by Min-yi Shen and 
  Andrej Sali at UCSF.
  This plugin can be configured to evaluate other statistical potentials by 
  providing the appropriate parameter file. 

*******************************************************************************/
#ifndef __DOPE_H__
#define __DOPE_H__

#include <stdio.h>
#include <cstddef> // Needed to use NULL.
#include <fstream>
#include <cstring>
#include <cmath>
#include <string>
#include <iostream>
#include <map>
#include <vector>

using namespace std;

// map of residues' name [string -> int]
typedef map<string, int> mapRESstr2int;

// map of residues' name [int -> string]
typedef map<int, string> mapRESint2str;

// map of atoms' name [string -> int]
typedef map<string, int> mapATOMstr2int;

// map of atoms' name [int -> string]
typedef map<int, string> mapATOMint2str;

//#define MAX_ATOM_CODE 36
#define MAX_ATOM_CODE 7

// This class contains the binned energy values.
class DOPE
{
public:
	DOPE();
	~DOPE() { ReleaseMemory(Energy); };

	int LoadFromFile(string fn){
		InitializeMemory(30, Energy);
		InitializeEnergy(Energy);
		return LoadFromFile(fn, Energy, true);
	};
	int LoadFromFile(string fn, double *Energy, bool bSymmetry=false);//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1]);

	double operator () (int aa1, int at1, int aa2, int at2, int bin){
		int idx = n_bins*(at2+(max_atom_code+1)*(aa2+20*(at1+(max_atom_code+1)*aa1)));
		if ((-1 < bin) && (bin < n_bins))
			return Energy[bin+idx];//[aa1][at1][aa2][at2][bin];
		else
			return 0.0;
	};

	void Resize(int n, double *&Energy)//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1]) 
	{
		ReleaseMemory(Energy);
		InitializeMemory(n, Energy);
		InitializeEnergy(Energy);
	};

	double calcAtomDOPE(string residue_id1, string atom1, string residue_id2, string atom2, double dist); 

	int AALetter2Code(char a_char){
		int arrResidueIdx[26]={  0, -1, 4, 3,  6, 13,  7,  8,  9, -1, 11, 10, 12, 
					 2, -1, 14, 5, 1, 15, 16, -1, 19, 17, -1, 18, 20};
		/*	ALA	A	0
				B	-1
			CYS	C	4
			ASP	D	3
			GLU	E	6
			PHE	F	13
			GLY	G	7
			HIS	H	8
			ILE	I	9
				J	-1
			LYS	K	11
			LEU	L	10
			MET	M	12
			ASN	N	2
				O	-1
			PRO	P	14
			GLN	Q	5
			ARG	R	1
			SER	S	15
			THR	T	16
				U	-1
			VAL	V	19
			TRP	W	17
				X	-1
			TYR	Y	18
			HET	Z	20
		*/
		int idx = a_char-'A';
		if (idx>=0 && idx<26)
			return arrResidueIdx[idx];
		else
			return -1;
	};
	
	void trim2(string& str) {
		string::size_type pos = str.find_last_not_of(' ');
		if(pos != string::npos) {
			str.erase(pos + 1);
			pos = str.find_first_not_of(' ');
			if(pos != string::npos) 
				str.erase(0, pos);
		}
		else str.erase(str.begin(), str.end());
	};

	int AANameToCode(string AAName){
		string arrResidueName[21]={"ALA","ARG","ASN","ASP","CYS","GLN","GLU","GLY","HIS","ILE",
				"LEU","LYS","MET","PHE","PRO","SER","THR","TRP","TYR","VAL","HET"};
		//string arrResidueName[20]={"ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU",
		//                          "MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR"};
		for (int i=0; i<21; i++)
			if (arrResidueName[i]==AAName)
				return i;
		return -1; //UNK;
	};

	bool BondedAtoms(int aa0, string code0, int aa1, string code1, int chain_dist){
		if (chain_dist <= 1 && chain_dist != 0)
		{
			// All the atom we use are backbone atoms
			if (aa1 < aa0)
				return ((code1.compare("CA")==0 || code1.compare("C")==0) && (code0.compare("N")==0 || code0.compare("CA")==0)); 
			else
				return ((code0.compare("CA")==0 || code0.compare("C")==0) && (code1.compare("N")==0 || code1.compare("CA")==0)); 
		}
		
		return false;
	}

	static int SSNCode(char code){
		switch (code)
		{
		case 'H':
			return 0;
		case 'E':
			return 1;
		case 'S':
			return 2;
		case 'B':
			return 3;
		case 'G':
			return 4;
		case 'T':
			return 5;
		case 'N':
			return 6;
		default:
			return 2;
		}
	};
	
	static string AACode(string AA){
		if (AA == "A") return "ALA";
		else if (AA == "C") return "CYS";
		else if (AA == "D") return "ASP";
		else if (AA == "E") return "GLU";
		else if (AA == "F") return "PHE";
		else if (AA == "G") return "GLY";
		else if (AA == "H") return "HIS";
		else if (AA == "I") return "ILE";
		else if (AA == "K") return "LYS";
		else if (AA == "L") return "LEU";
		else if (AA == "M") return "MET";
		else if (AA == "N") return "ASN";
		else if (AA == "P") return "PRO";
		else if (AA == "Q") return "GLN";
		else if (AA == "R") return "ARG";
		else if (AA == "S") return "SER";
		else if (AA == "T") return "THR";
		else if (AA == "V") return "VAL";
		else if (AA == "W") return "TRP";
		else if (AA == "Y") return "TYR";
		else return "?";
	};
	
	int max_atom_code;
	int n_bins;

protected:
	double *Energy;//[20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1][];
	// Parameters.
	double dist_cutoff;

	void InitializeMemory(int n, double *&Energy);//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1]);
	void ReleaseMemory(double *&Energy);//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1]);
	void InitializeEnergy(double *Energy);//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1]);
	int GetResidueCode(string aa_str, int insert_flag=0);
	int GetResidueCode(int aa);
	int GetAtomCode(string atom_str, int insert_flag=0);
	int GetAtomCode(int atom);

	mapRESstr2int * pres_s2i;
	mapRESint2str * pres_i2s;
	int resMap[26];
	mapATOMstr2int * patom_s2i;
	mapATOMint2str * patom_i2s;
	int atomMap[10];
};

/*************************** Function implementations *************************/
/*
void plgSetDefConfig()
{
    // Setting defaults.
    dist_cutoff = 15.0;
    num_bin = 30;

    min_chain_dist = 1;
    max_chain_dist = INF_CHAIN;

    energy_coeff = 1.0;
}
*/

#endif

