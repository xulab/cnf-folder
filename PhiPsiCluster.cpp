#include "PhiPsiCluster.h"
#include <fstream>
#include <iostream>
#include "strtokenizer.h"
#include <values.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

PhiPsiCluster::PhiPsiCluster(const char* aFileName)
{
   
	ifstream input(aFileName);
   	if(input.bad())
   	{
      		cerr<<"ERROR: Can not find cluster file "<<aFileName<<endl;
      		return;
   	}

   	char buf[4096];
   	int clusterID;
	int maxClusterID=MININT;

	mCluster=vector<vector<double> >(MAX_NUM_CLUSTER);
   	mNumCluster=0;

	//if mode=0, then it is cluster for Phi and Psi pairs
	//otherwise for a single angle

   	int mode=0;
   	while(!input.eof())
   	{
     		input.getline(buf, 4096);
		strtokenizer tokenizer(buf," :,\r\t\n");
		int numTokens=tokenizer.count_tokens();
		if (numTokens==6){
			mode=0;
		}else if(numTokens==3){
			mode=1;
		}else continue;
     
		vector<double> tmp;
		if (mode==0) tmp=vector<double>(5);
		else tmp=vector<double>(2);

    		clusterID=atoi(tokenizer.token(0).c_str());
		if (clusterID > maxClusterID)	maxClusterID=clusterID;

		if (clusterID>=(int)mCluster.size())	mCluster.resize(clusterID+1);
	
		if (mode==0){
			tmp[0]=atof(tokenizer.token(1).c_str()); 
			tmp[1]=atof(tokenizer.token(2).c_str()); 
			tmp[2]=atof(tokenizer.token(3).c_str()); 
			tmp[3]=atof(tokenizer.token(4).c_str());    
			tmp[4]=atof(tokenizer.token(5).c_str()); 
		}else{
			tmp[0]=atof(tokenizer.token(1).c_str()); 
			tmp[1]=atof(tokenizer.token(2).c_str()); 
		}
		mCluster[clusterID]=tmp;
     		mNumCluster++;

   	}//while

	if (maxClusterID!=mNumCluster){
		cerr<<"ERROR: Invalid content in the PhiPsi cluster file "<<aFileName<<endl;
		cerr<<"\tPossible reason: cluster IDs are not starting from 0 and not consecutive!"<<endl;
		return;
	}

	//shall we resize mCluster?
	//mCluster.resize(mNumCluster);
}
PhiPsiCluster:: ~PhiPsiCluster()
{
}

