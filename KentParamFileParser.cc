#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>

#include "KentParamFileParser.h"
#include "FBCluster.h"

using namespace std;

ParseFB5::ParseFB5(ifstream& input)
{
   char buf[4096];
   //trim off the first two lines
   input.getline(buf, 4096);
   input.getline(buf, 4096);
}

//FB5ClusterNode*
void ParseFB5::newNode(FB5ClusterNode& rev)
{
   rev.index=node;
   rev.beta=beta;
   rev.kappa=kappa;
   
   memcpy(rev.gamma1, &mGamma[0][0], 3*sizeof(double));
   memcpy(rev.gamma2, &mGamma[1][0], 3*sizeof(double));
   memcpy(rev.gamma3, &mGamma[2][0], 3*sizeof(double));
   rev.tau=tau;
   rev.theta=theta;
}
void ParseFB5::writeNode(ofstream& output)
{
  output<<node<<" "<<beta<<" "<<kappa<<endl;
  output<<mGamma[0][0]<<" "<<mGamma[0][1]<<" "<<mGamma[0][2]<<endl;
  output<<mGamma[1][0]<<" "<<mGamma[1][1]<<" "<<mGamma[1][2]<<endl;
  output<<mGamma[2][0]<<" "<<mGamma[2][1]<<" "<<mGamma[2][2]<<endl;
  output<<theta<<" "<<tau<<endl;
}

bool ParseFB5::parseNode(ifstream& input)
{
   char buf[4096];
   char* token;
   while(!input.eof()) {
     input.getline(buf, 4096); 
     token=strtok(buf, " ");
     if(strcmp(token, "<H")==0) {
        token=strtok(NULL, " h=\">");
	node=atoi(token);
     } else if(strcmp(token, "<Beta>")==0) {
	token=strtok(NULL, " ");
	beta=atof(token);
     } else if(strcmp(token, "<Kappa>")==0) {
	token=strtok(NULL, " ");
	kappa=atof(token);
     } else if(strcmp(token, "<Gamma")==0) {
	token=strtok(NULL, " n=\">");
	int index=atoi(token); 
        token=strtok(NULL, " n=\">");
	mGamma[index][0]=atof(token);
	token=strtok(NULL, " ");
	mGamma[index][1]=atof(token);
	token=strtok(NULL, " ");
	mGamma[index][2]=atof(token);
     } else if(strcmp(token, "<Mean_Theta_Tau>")==0) {
        token=strtok(NULL, " ");
	theta=atof(token);
        token=strtok(NULL, " ");
	tau=atof(token);     
     } else if(strcmp(token, "</H>")==0)
       return true;
     else if(strcmp(token, "</FB5_Emission>")==0)
       return false;
   }
}
/*
ParseFB5::parseTrans(ifstream& input, int numNode, double** trans)
{
    char buf[400];
    char* token; 
    input.getline(buf, 400);
    input.getline(buf, 400);
    for(int i=0; i<numNode; i++) {
       //trim off the first line
       input.getline(buf, 400);
       for(int j=0; j<numNode; j++) {
          input.getline(buf, 400);
	  token=strtok(buf, ">");
	  token=strtok(NULL, " ");
	  trans[i][j]=atof(token);
       }
       input.getline(buf, 400);
    }
}
*/
void ParseFB5::parseSecProb(ifstream& input, int numNode, double** secProb)
{
    char buf[400];
    char* token; 
    input.getline(buf, 400);
    input.getline(buf, 400);
    for(int i=0; i<numNode; i++) {  
        input.getline(buf, 400);
        for(int j=0; j<3; j++) {	
          input.getline(buf, 400);
	  //cout<<buf<<endl;
          token=strtok(buf, ">");
	  token=strtok(NULL, " ");
	  secProb[i][j]=atof(token);
        }
        input.getline(buf, 400);
    }
}
void ParseFB5::parseSeqProb(ifstream& input, int numNode, double** seqProb)
{
    char buf[400];
    char* token; 
    input.getline(buf, 400);
    input.getline(buf, 400);
    for(int i=0; i<numNode; i++) {  
      input.getline(buf, 400);
      for(int j=0; j<20; j++) {
        input.getline(buf, 400);
	//cout<<buf<<endl;
        token=strtok(buf, ">");
	token=strtok(NULL, " ");
	seqProb[i][j]=atof(token);
      }
      input.getline(buf, 400);
    }
    input.getline(buf, 400);
}
/*
void ParseFB5::parseInitProb(ifstream& input, int numNode, double** initProb)
{  
    char buf[400];
    char* token;

    input.getline(buf, 400);
    input.getline(buf, 400);
    for(int i=0; i<numNode; i++) {  
        input.getline(buf, 400);
	token=strtok(buf, ">");
	token=strtok(NULL, " ");
	initProb[i]=atof(token);
      }    
    }   
}*/
