#include <cstdlib>
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <values.h>

#include <BALL/KERNEL/protein.h>
#include <BALL/KERNEL/atomContainer.h>
//#include <BALL/CONCEPT/textPersistenceManager.h>
#include <BALL/FORMAT/PDBFile.h>
#include <BALL/KERNEL/residue.h>

#include "newconfig.h"
#include "ConformationOptimizer.h"
#include "StructureBuilder.h"

using namespace std;
using namespace BALL;

#define MODEL_PHIPSI	1
#define MODEL_THETATAU	2

#define ALG_GREEDY	1
#define ALG_MCMC	2
#define ALG_CALCLSP	3

#define SCORE_RADIUS 	1
#define SCORE_DOPE 	2
#define SCORE_DOPE_BACK 3
#define SCORE_BMK 	4
#define SCORE_TSP 	5
#define SCORE_ESP	6
#define SCORE_DOPE_BETA 7

#define DEF_CONFIG_FILE	"proteinFolder.conf"
#define DEF_OUT_DIR	"./"

void Usage(){
	cerr<<"Usage: calcLSP[-m model ] [-a algorithm] [-e scoring] [-t temperature] [-s] [-r random_seed] [-f config_file] [-o out_dir] proteinName"<<endl;
	cerr<<"\tmodel: protein representation model: thetatau or phipsi (def=thetatau)"<<endl;
	cerr<<"\talgorithm: conformation optimization algorithm: calcLSP, greedy or Monte Carlo(MCMC) (def=greedy)"<<endl;
	cerr<<"\tscoring: the scoring function: by radius or a true scoring function (def=radius)"<<endl;
	cerr<<"\ttemperature: the absolute temperature used for DOPE. Default value is 273.0 "<<endl;
	cerr<<"\t-s: this option enables the MCMC search to use Simulated Annealing " <<endl;
	cerr<<"\trandom_seed: the seed for random generator. If not specified, the program will generate the seed from system state. This seed will also be used to name the result file."<<endl;
	cerr<<"\tconfig_file: configuration file name (def=proteinFolder.conf)"<<endl;
	cerr<<"\tout_dir: output directory (def=./)"<<endl;
	cerr<<"\tproteinName: protein name, its corresponding sequence file and ss file are proteinName.seq and proteinName.newss, respectively."<<endl;
	cerr<<"\n\tNote: use [-d decoy_dir] and [-p pattern] for calcLSP"<<endl;
	cerr<<"\tBuilt Date: 20080506"<<endl;
}

Config* config=0;
double temperature_DOPE = 273.0;

vector<RESIDUE> g_backbone;

// read the pdb file to get the structure of a protein and fill into <protein>
// return -1 if not succeed
int GetConformation(string pdb_file, vector<RESIDUE> &g_backbone)
{
	g_backbone.clear();
	Protein* protein = new Protein;
	// read the pdb protein file
        PDBFile* pdb=0;
	try{
		pdb = new PDBFile(pdb_file, std::ios::in);
	}catch(Exception::FileNotFound){
	  cerr<<"Can not find protein file "<<pdb_file <<endl;
	  return -1;
	}

	*pdb >> *protein;
	//cerr << "protein data read with " << protein->countChains() << " chains; ";
	//cerr << protein->countResidues() << " residues; " << protein->countPDBAtoms() << " atoms\n";

	int nRes=0;
	// retrieve the residue and atoms from the Protein structure and insert into the bbq acceptable structure
	for(ResidueIterator resIter=protein->beginResidue(); resIter!=protein->endResidue(); resIter++){
		RESIDUE res;
		res.name= resIter->getFullName(Residue::NO_VARIANT_EXTENSIONS);
		if (res.name.size()<3)
			break; //continue;
		Vector3 pos;
		for(AtomIterator at=resIter->beginAtom();at!=resIter->endAtom();at++){
			pos = at->getPosition();
			ATOM atom;
			atom.name = at->getName();

			atom.x = pos.x;
			atom.y = pos.y;
			atom.z = pos.z;

			res.addAtom( atom);
			//cerr << "BALLatom=" << atom.name << "(" << atom.x << ", " << atom.y << ", " << atom.z << ")\n";
		}
		res.seq = nRes;
		nRes++;
		
		if (res.getAtom("CA").name=="")
			break;

		ATOM C, CB, N, O;
		C.name = "C";
		CB.name = "CB";
		O.name = "O";
		N.name = "N";
		if (res.getAtom("CB").name=="")
			res.addAtom(CB);
		if (res.getAtom("C").name=="")
			res.addAtom(C);
		if (res.getAtom("O").name=="")
			res.addAtom(O);
		if (res.getAtom("N").name=="")
			res.addAtom(N);

		g_backbone.push_back(res);
	}
	
	if (protein) delete protein;
	if (pdb) delete pdb;

	return 1;
}

int main(int argc, char** argv)
{
	if(argc<2)
	{
		Usage();
		exit(-1);
	}
	
  
	int model=MODEL_THETATAU;
	int algorithm=ALG_GREEDY;
	int scoring=SCORE_RADIUS;
	string configFile=DEF_CONFIG_FILE;
	string outdir=DEF_OUT_DIR;
	string lsp_dir="", pattern="";
	unsigned int randomSeed=0;

	extern char* optarg;
	extern int optind;
	
	char c=0;
	int bUseSimulatedAnnealing = 0;
	while((c=getopt(argc,argv,":m:a:e:r:f:o:t:s:d:p:"))!=EOF){
		//      printf("c=%d EOF=%d\n",c,EOF);
		switch(c){
		case 'm':
			if (strcasecmp(optarg,"phipsi")==0)
				model=MODEL_PHIPSI;
			break;
		
		case  'a':
			if (strcasecmp(optarg,"MCMC")==0)
				algorithm=ALG_MCMC;
			if (strcasecmp(optarg,"calcLSP")==0)
				algorithm=ALG_CALCLSP;
			break;
		case 'e':
			if (strcasecmp(optarg, "dope")==0)
				scoring=SCORE_DOPE;
			else if (strcasecmp(optarg, "dope-back")==0)
				scoring=SCORE_DOPE_BACK;
			else if (strcasecmp(optarg, "bmk")==0)
				scoring=SCORE_BMK;
			else if (strcasecmp(optarg, "tsp")==0)
				scoring=SCORE_TSP;
			else if (strcasecmp(optarg, "esp")==0)
				scoring=SCORE_ESP;
			else if (strcasecmp(optarg, "dopebeta")==0)
				scoring=SCORE_DOPE_BETA;
			break;
		case 'r':
			randomSeed=atoi(optarg);
			break;
		case 'f':
			configFile=optarg;		
			break;
		case 'o':
			outdir=optarg;
			break;
		case 't':
			temperature_DOPE = atof(optarg);
			break;
		case 's':
			bUseSimulatedAnnealing = 1;
			break;
		case 'd':
			lsp_dir = optarg;
			break;
		case 'p':
			pattern = optarg;
			break;
		default:
			Usage();
			exit(-1);
		}
	}

	string proteinName;

	if (optind<argc) proteinName=argv[optind];

	//set random seed
	if (randomSeed <=0 ) {
		//here we donot use time() to get the random seed, instead we use the system kernel state to generate a seed
		ifstream in("/dev/urandom",ios::in);
		in.read((char*)&randomSeed, sizeof(unsigned)/sizeof(char));
//		cout<<randomSeed<<endl;
		in.close();

		unsigned id=getpid();
		randomSeed=randomSeed*randomSeed+id*id;
	}

	//read configuration file
	config=new Config(configFile);
	config->configure();
        
	//generate three major objects
	ConformationOptimizer* conformationOptimizer=0;
	StructureBuilder* structureBuilder=0;

	if (model==MODEL_THETATAU){
		conformationOptimizer=new ThetaTauOptimizer();
		structureBuilder=new ThetaTauBuilder();
	}else if (model==MODEL_PHIPSI){
		conformationOptimizer=new PhiPsiOptimizer();
		structureBuilder=new PhiPsiBuilder();
	}

	conformationOptimizer->setProteinName(proteinName);
	structureBuilder->setProteinName(proteinName);
	conformationOptimizer->setStructureBuilder(structureBuilder);
	
	cerr << "checkpoint 4\n";
	//Initialize has to be called before other actions
	conformationOptimizer->Initialize();

	cerr << "checkpoint 5\n";
/////////////////////// Feng Test ///////////////////////
	for (int i=0; i<2; i++)
	{
		//here we donot use time() to get the random seed, instead we use the system kernel state to generate a seed
		ifstream in("/dev/urandom",ios::in);
		in.read((char*)&randomSeed, sizeof(unsigned)/sizeof(char));
	//	cout<<randomSeed<<endl;
		in.close();
	
		unsigned id=getpid();
		randomSeed=randomSeed*randomSeed+id*id;
		//we can set the random seed at only the main function
		srand(randomSeed);
		srandom(randomSeed);
		srand48(randomSeed);
	
		//optimize the conformation
		if (algorithm==ALG_GREEDY)
			conformationOptimizer->GreedySearch();
		else if (algorithm==ALG_MCMC)
			conformationOptimizer->MCMCSearch(bUseSimulatedAnnealing);
		else if (algorithm==ALG_CALCLSP)
		{
 			vector<string> seq;
			if  ( BBQ::lookup(seq, lsp_dir.c_str(), pattern.c_str())==0)
			{
				cerr << "no *." << pattern << " file found under the directory: " << outdir << "\n\r";
		    		return 0;
			}
			cerr << "totally " << seq.size() << " files." << endl;
			for (int n=0; n<seq.size(); n++)
			{
				cout << "Protein file: " << seq[n];
				string fname = lsp_dir + "/" + seq[n];
				GetConformation( fname, g_backbone);
				cerr << "Protein file: " << seq[n];
                       		conformationOptimizer->calcLSP(g_backbone);
			}
			break;
		}

		//save the resulting PDB file
		char resultPDBfile[4096];
		sprintf(resultPDBfile,"%s/%s%u.pdb",outdir.c_str(),proteinName.c_str(),randomSeed);
		string pdbfile(resultPDBfile);
		conformationOptimizer->writeDecoy(pdbfile);
	}
///////////////Feng Test //////////////////

	//free memory
	if (conformationOptimizer) delete conformationOptimizer;
	//if (scoringFunction) delete scoringFunction;
	if (structureBuilder) delete structureBuilder;
	if (config) delete config;
}
