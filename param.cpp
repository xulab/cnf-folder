
#include <stdlib.h>
#include <string.h>

#include "param.h"
#include "IP_constant.h"
#include "util.h"
#include "stringTokenizer.h"

CParam::CParam(){
	singletonIsReady=false;
	mutationIsReady=false;
	pairInteractionIsReady=false;


	memset(pairInteraction,0,sizeof(int)*ResidueTypes*ResidueTypes);
	memset(mutation,0,sizeof(int)*ResidueTypes*ResidueTypes);
	memset(singleton,0,sizeof(int)*ResidueTypes*3*3);

	memset(solventAcc,0,sizeof(float)*ResidueTypes*MaxSolventIntervalNum*3);
	memset(solventBound,0,sizeof(float)*MaxSolventIntervalNum);
	memset(contactCapacity,0,sizeof(float)*MaxContactNum*ResidueTypes*StructTypes);
	memset(sContactCapacity,0,sizeof(float)*MaxContactNum*ResidueTypes*StructTypes);
	memset(lContactCapacity,0,sizeof(float)*MaxContactNum*ResidueTypes*StructTypes);
	memset(maxContactNum,0,sizeof(int)*StructTypes);
	memset(maxsContactNum,0,sizeof(int)*StructTypes);
	memset(maxlContactNum,0,sizeof(int)*StructTypes);

	memset(volumeContact,0,sizeof(float)*ResidueTypes*ResidueTypes);
	memset(volumeSolvent,0,sizeof(float)*ResidueTypes);
}

CParam::~CParam(){
	singletonIsReady=false;
	mutationIsReady=false;
	pairInteractionIsReady=false;
}


//void CParam::InputPairTable(char* filename,int flag){
void CParam::InputPairTable(char* filename,int dest[ResidueTypes][ResidueTypes]){

	FILE* fp;



	fp=fopen(filename,"r");

	if (fp==0){
		//report err
		fprintf(stderr,"can not open file %s ",filename);
		return;
	}

	char s[MaxLineSize];

	int k=0;


	while(fgets(s,MaxLineSize,fp) && k<ResidueTypes){
		StringTokenizer strToken(s," \t\n\r");
		char* nextToken=strToken.getNextToken();
		if (strncasecmp(nextToken,"REM",3)!=0){
			int j=0;


			while((nextToken=strToken.getNextToken())!=0 && j<=k)
			{

		//		printf("token=%s ",nextToken);

/*
				if (flag==MUTATION)
					mutation[k][j]=-atoi(nextToken);
				else pairInteraction[k][j]=atoi(nextToken);
*/
				dest[k][j]=atoi(nextToken);

				j++;
			}
			k++;


		}

	}

	fclose(fp);


	for(k=0;k<ResidueTypes;k++)
		for(int j=0;j<k;j++)
/*
			if (flag==MUTATION)
				mutation[j][k]=mutation[k][j];
			else	pairInteraction[j][k]=pairInteraction[k][j];
*/
			dest[j][k]=dest[k][j];





}

void CParam::InputPairInteraction(char* filename,int flag){

	pairInteractionIsReady=false;

	//InputPairTable(filename,PAIR_INTERACTION);
	if (flag==DISTINDEPENDENT){
		InputPairTable(filename,pairInteraction);
		for(int i=0;i<ResidueTypes;i++)
			for(int j=0;j<ResidueTypes;j++)
				pairInteraction[i][j]=ToolKit::round(0.1*pairInteraction[i][j]);
	}else if(flag==DISTDEPEND5){
		InputPairTable(filename,pairInteraction5);
		for(int i=0;i<ResidueTypes;i++)
			for(int j=0;j<ResidueTypes;j++)
				pairInteraction5[i][j]=ToolKit::round(0.1*pairInteraction5[i][j]);
	}else if(flag==DISTDEPEND7){
		InputPairTable(filename,pairInteraction7);
		for(int i=0;i<ResidueTypes;i++)
			for(int j=0;j<ResidueTypes;j++)
				pairInteraction7[i][j]=ToolKit::round(0.1*pairInteraction7[i][j]);
	}else if(flag==DISTDEPEND9){
		InputPairTable(filename,pairInteraction9);
		for(int i=0;i<ResidueTypes;i++)
			for(int j=0;j<ResidueTypes;j++)
				pairInteraction9[i][j]=ToolKit::round(0.1*pairInteraction9[i][j]);
	}else if(flag==DISTDEPEND11){
		InputPairTable(filename,pairInteraction11);
		for(int i=0;i<ResidueTypes;i++)
			for(int j=0;j<ResidueTypes;j++)
				pairInteraction11[i][j]=ToolKit::round(0.1*pairInteraction11[i][j]);
	}else if(flag==DISTDEPEND13){
		InputPairTable(filename,pairInteraction13);
		for(int i=0;i<ResidueTypes;i++)
			for(int j=0;j<ResidueTypes;j++)
				pairInteraction13[i][j]=ToolKit::round(0.1*pairInteraction13[i][j]);
	}else if(flag==PPIINTERACTION){
		InputPairTable(filename,PPIpairInteraction);
		for(int i=0;i<ResidueTypes;i++)
			for(int j=0;j<ResidueTypes;j++)
				PPIpairInteraction[i][j]=ToolKit::round(0.1*PPIpairInteraction[i][j]);
	}

	pairInteractionIsReady=true;
}



void CParam::InputMutation(char* filename){
	mutationIsReady=false;
	//InputPairTable(filename,MUTATION);
	InputPairTable(filename,mutation);
	mutationIsReady=true;

}

void CParam::InputSingleton(char* filename){
	singletonIsReady=false;
	FILE* fp;
	fp=fopen(filename,"r");
	if(fp==0){
		fprintf(stderr,"cannot open file %s for read",filename);
			return;
	}

	char s[MaxLineSize];
	int k=0;
	while(fgets(s,MaxLineSize,fp) && k<ResidueTypes){
		StringTokenizer strToken(s," \t\n\r");
		char * nextToken=strToken.getNextToken();
		if (strncmp(nextToken,"REM",3)!=0){
			int j=0;
			while(j<9 && (nextToken=strToken.getNextToken())!=0)
			{
				*(&singleton[k][0][0]+j)=ToolKit::round(100*atof(nextToken));
				j++;
			}
			k++;

		}

	}
	fclose(fp);

	singletonIsReady=true;

}

void CParam::InputSolvent(char* filename){
	solventIsReady=false;

	FILE* fp=fopen(filename,"r");
	if (!fp){
		fprintf(stderr,"can not open %s for read\n",filename);
		return ;
	}

	solventIntervalNum=0;

	char buf[2048];

	int ssType=0;

	int aaType=0;

	while(fgets(buf,2048,fp)){
		if (isEmpty(buf)) continue;
		if (strstr(buf,"#")) continue;

		if (strncasecmp(buf,"PARTITION",9)==0){
			char tmp[256];
			float* a=solventBound;
			int ret=sscanf(buf,"%s%f%f%f%f%f%f%f%f%f%f",tmp,a,a+1,a+2,a+3,a+4,a+5,a+6,a+7,a+8,a+9);
			solventIntervalNum=ret-1;
			continue;
		}

		if (strncasecmp(buf,"HELIX",5)==0){
			ssType=HELIX;
			aaType=0;
			continue;
		}

		if (strncasecmp(buf,"LOOP",4)==0){
			ssType=LOOP;
			aaType=0;
			continue;
		}
		if (strncasecmp(buf,"SHEET",5)==0){
			ssType=SHEET;
			aaType=0;
			continue;
		}

		float a[MaxSolventIntervalNum];

		sscanf(buf,"%f%f%f%f%f%f%f%f%f%f",a,a+1,a+2,a+3,a+4,a+5,a+6,a+7,a+8,a+9);
		for(int i=0;i<solventIntervalNum;i++)
			solventAcc[aaType][i][ssType]=a[i]*100;

		aaType++;

		
	}

	fclose(fp);

	solventIsReady=true;

	//debug
/*
	for(int k=0;k<3;k++){
		for(int i=0;i<20;i++){
			for(int j=0;j<solventIntervalNum;j++)
				printf("%f\t",solventAcc[i][j][k]);
			printf("\n");
		}
	}
*/
		
}

void CParam::InputContactCapacity(char* filename){

	contactCapacityIsReady=false;

	FILE* fp=fopen(filename,"r");
	if (!fp){
		fprintf(stderr,"can not open %s for read\n",filename);
		return ;
	}


	memset(maxContactNum,0,sizeof(int)*StructTypes);
	memset(maxsContactNum,0,sizeof(int)*StructTypes);
	memset(maxlContactNum,0,sizeof(int)*StructTypes);

	int ss=LOOP;
	int type=CONTACT_T_GENERAL;

	char buf[4096];
	while(fgets(buf,4096,fp)){
		if (isEmpty(buf)) continue;
		if (strstr(buf,"#")) continue;
		if (strncasecmp(buf,"HELIX",5)==0){
			ss=HELIX;
			continue;
		}
		if(strncasecmp(buf,"SHEET",5)==0){
			ss=SHEET;
			continue;
		}
		if (strncasecmp(buf,"LOOP",4)==0){
			ss=LOOP;
			continue;
		}
		if (strncasecmp(buf,"GENERAL",7)==0){
			type=CONTACT_T_GENERAL;
			continue;
		}
		if (strncasecmp(buf,"SHORT",5)==0){
			type=CONTACT_T_SHORT;
			continue;
		}
		if (strncasecmp(buf,"LONG",4)==0){
			type=CONTACT_T_LONG;
			continue;
		}

		float a[20];
		int ret=sscanf(buf,"%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f",a,a+1,a+2,a+3,a+4,a+5,a+6,a+7,a+8,a+9,a+10,a+11,a+12,a+13,a+14,a+15,a+16,a+17,a+18,a+19);

		if (ret<20){
			fprintf(stderr,"fatal error, not enough data in one line in reading file %s\n%s",filename,buf);
			exit(-1);
		}

//		printf("type=%d,buf=%s",type,buf);

		if (type==CONTACT_T_GENERAL){
			for(int i=0;i<ResidueTypes;i++)
				contactCapacity[maxContactNum[ss]][i][ss]=a[i];
			maxContactNum[ss]++;
		}else if(type==CONTACT_T_LONG){
			for(int i=0;i<ResidueTypes;i++)
				lContactCapacity[maxlContactNum[ss]][i][ss]=a[i];
			maxlContactNum[ss]++;
		}else{
			for(int i=0;i<ResidueTypes;i++)
				sContactCapacity[maxsContactNum[ss]][i][ss]=a[i];
			maxsContactNum[ss]++;
		}
	}

	fclose(fp);

	//for debug


	if (config->debug_level>=2){
		printf("\ncontactCapacity:\n");
		for(int j=0;j<3;j++){
			for(int c=0;c<maxContactNum[j];c++){
				for(int i=0;i<ResidueTypes;i++)
					printf(" %.1f",contactCapacity[c][i][j]);
				printf("\n");
			
			}
		
			printf("\n");
		}
	}


	contactCapacityIsReady=true;
}


int CParam::getPairInteraction(int i,int j,float dist){
	ToolKit::allege(pairInteractionIsReady,"please input interaction param first!");
	if (i<0 || i>=ResidueTypes || j<0 || j>=ResidueTypes)
	{
		//fprintf(stderr,"getPairInteraction param:i=%d,j=%d\n",i,j);
		return 0;
		//exit(-1);
	}

	if (config->UseDistanceDependentContact)
	{
		if (dist<=5){
			return pairInteraction5[i][j];
		}else if(dist<=7){
			return pairInteraction7[i][j];
		}else if(dist<=9){
			return pairInteraction9[i][j];
		}else if(dist<=11){
			return pairInteraction11[i][j];
		}else if(dist<=13){
			return pairInteraction13[i][j];
		}else return 0;
	}
	else return pairInteraction[i][j];
}
int CParam::getPPIPairInteraction(int i, int j, float dist){
	if (i<0 || i>=ResidueTypes) return 0;
	if (j<0 || j>=ResidueTypes) return 0;
	return PPIpairInteraction[i][j];
}

int CParam::getMutation(int i,int j){

	ToolKit::allege(mutationIsReady,"please input mutation param first!");

	if (i>=ResidueTypes || j>=ResidueTypes || i<0 || j<0)
	{
		//fprintf(stderr,"beyond bound at getMutation!");
		return 0;
		//exit(-1);
	}
	return mutation[i][j];
}
float CParam::getSolvent(int residue,int ss,int solventType,int solvent){

	ToolKit::allege(singletonIsReady, "please input singleton param first!");

	if (residue<0 || residue>=ResidueTypes || ss>=3|| ss<0 || solventType>=3 || solventType <0)
	{
		//fprintf(stderr,"getSingleton param:%d,%d,%d,%d\n",residue,ss,solventType,solvent);
		return 0;
		//exit(-1);
	}

	float percent=solvent*1.0/maxAcc[residue];
	int i=0;
	for(i=0;i<solventIntervalNum;i++)
		if (percent<=solventBound[i]) break;
	if (i==solventIntervalNum) i--;

	return solventAcc[residue][i][ss];

}

int CParam::getSingleton(int residue,int ss,int solventType,int solvent){

	ToolKit::allege(singletonIsReady, "please input singleton param first!");

	if (residue<0 || residue>=ResidueTypes || ss>=3|| ss<0 || solventType>=3 || solventType <0)
	{
		//fprintf(stderr,"getSingleton param:%d,%d,%d,%d\n",residue,ss,solventType,solvent);
		return 0;
		//exit(-1);
	}


	return singleton[residue][ss][solventType];

}
float CParam::getContactCapacity(int contact_num,int residue,int ss,int type){
	ToolKit::allege(contactCapacityIsReady,"please input contact capacity param first!");

	if (residue<0 || residue>=ResidueTypes || ss>=3 || ss<0 || contact_num<0)
		return 0;

	int cNum=contact_num;


	if (type==CONTACT_T_SHORT){
		if (maxsContactNum<=0) return 0;
		if (cNum>=maxsContactNum[ss])
			cNum=maxsContactNum[ss]-1;
		return sContactCapacity[cNum][residue][ss];
	}
	else if (type==CONTACT_T_LONG){
		if (maxlContactNum<=0) return 0;
		if (cNum>=maxlContactNum[ss])
			cNum=maxlContactNum[ss]-1;
		return lContactCapacity[cNum][residue][ss];
	}
	else{
		if (maxContactNum<=0) return 0;
		if (cNum>=maxContactNum[ss])
			cNum=maxContactNum[ss]-1;
		return contactCapacity[cNum][residue][ss];
	}
}
void CParam::print(){

	printf("\npair interaction:\n");
	printPairInteraction();

	printf("\nmutation:\n");
	printMutation();

	printf("\nsingleton:\n");
	printSingleton();

}

void CParam::printPairInteraction(){
	ToolKit::allege(pairInteractionIsReady,"please input interaction param first!");
	for(int i=0;i<ResidueTypes;i++){
		printf("\n");
		for(int j=0;j<=i;j++)
			printf(" %d",pairInteraction[i][j]);
	}
}

void CParam::printMutation(){
	ToolKit::allege(mutationIsReady,"please input mutation param first!");
	for(int i=0;i<ResidueTypes;i++){
		printf("\n");
		for(int j=0;j<=i;j++)
			printf(" %d",mutation[i][j]);
	}
}

void CParam::printSingleton(){

	ToolKit::allege(singletonIsReady, "please input singleton param first!");
	for(int i=0;i<ResidueTypes;i++){
		printf("\n");
		for(int j=0;j<3;j++)
			for(int k=0;k<3;k++)
				printf(" %d",singleton[i][j][k]);
	}

}

void CParam::CalcAver(){
	for(int i=0;i<ResidueTypes;i++){
		int j,k;

		//pairInteraction
		pairInteractionAver[i]=0;
		for(j=0;j<ResidueTypes;j++)
			pairInteractionAver[i]+=pairInteraction[i][j];
		pairInteractionAver[i]/=ResidueTypes;

		//singleton
		singletonAver[i]=0;
		for(j=0;j<3;j++)
			for(k=0;k<3;k++)
				singletonAver[i]+=singleton[i][j][k];
		singletonAver[i]/=9;

		//mutation
		mutationAver[i]=0;
		for(j=0;j<ResidueTypes;j++)
			mutationAver[i]+=mutation[i][j];
		mutationAver[i]/=ResidueTypes;
		
	}

	//for debug
	
	if (config->debug_level>=DEBUG_LEVEL3){
		int k;
		for(k=0;k<ResidueTypes;k++)
			printf("mutationAver[%d]=%d\n",k,mutationAver[k]);
		for(k=0;k<ResidueTypes;k++)
			printf("singletonAver[%d]=%d\n",k,singletonAver[k]);
	}

}

int CParam::getPairInteractionAver(int i){
	if (i>=0 || i<ResidueTypes) return pairInteractionAver[i];
	else return 0;
}
int CParam::getSingletonAver(int i){
	if (i>=0 || i<ResidueTypes) return singletonAver[i];
	else return 0;
}

int CParam::getMutationAver(int i){
	if (i>=0 || i<ResidueTypes) return mutationAver[i];
	else return 0;

}

float CParam::getVolumeSolvent(int residue){
	if (residue<0 || residue>=ResidueTypes) return 0;
	return volumeSolvent[residue];
}

float CParam::getVolumeContact(int lResidue,int rResidue){
	if (lResidue<0 || lResidue>=ResidueTypes) return 0;
	if (rResidue<0 || rResidue>=ResidueTypes) return 0;
	return volumeContact[lResidue][rResidue];
}

void CParam::InputVolumeContact(char* file){
	FILE* fp=fopen(file,"r");
	if (!fp){
		fprintf(stderr,"WARNING: cannot open %s for read!\n",file);
		return;
	}
	char buf[8192];
	
	//skip the first two lines
	fgets(buf,8192,fp);
	fgets(buf,8192,fp);

	int line=0;
	while(fgets(buf,8192,fp)){
		float a[ResidueTypes+2];
		char AA[10];
		int ret=sscanf(buf,"%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f",AA,a,a+1,a+2,a+3,a+4,a+5,a+6,a+7,a+8,a+9,a+10,a+11,a+12,a+13,a+14,a+15,a+16,a+17,a+18,a+19,a+20,a+21);
		if (ret<ResidueTypes+2){
			fprintf(stderr,"something is wrong with %s\n",file);
			exit(-1);
		}
		char code=AA1Coding[AA2SUB[AA[0]-'A']];
		volumeSolvent[code]=a[0];
		for(int i=1;i<=21;i++){
			char othercode=AA1Coding[i];
			volumeContact[code][othercode]=a[i];
		}
		line++;

		if (line>=20) break;
		
	}
	fclose(fp);
}
