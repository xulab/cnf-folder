#include <BALL/STRUCTURE/rotamerLibrary.h>
#include <BALL/STRUCTURE/fragmentDB.h>

using namespace BALL;
using namespace std;

typedef struct _BasicAtomTriple{
	const Atom* CA;
	const Atom* C;
	const Atom* N;
}BasicAtomTriple;


//calculate the phi and psi angles simutaneously
pair<int, int> CalculateTorsionAngles(BasicAtomTriple& previous, BasicAtomTriple& myself, BasicAtomTriple& next);

bool ParseField(char* field, vector<pair<int, int> >& fieldList);
void WritePositions(Residue& r);
bool SideChainAtom(const Atom& a);
bool BackBoneAtom(const Atom& a);
double DihedralAngle(const Vector3 &v1, const Vector3 &v2, const Vector3 &v3, const Vector3 &v4);
double getAngle(const Vector3 &v1, const Vector3 &v2, const Vector3 &v3);
