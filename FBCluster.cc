#include "FBCluster.h"
#include <fstream>
#include <iostream>
#include "KentParamFileParser.h"

using namespace std; 

//the caller needs to allocate memory to rev
void polar2xyz(double theta,double tau,double* rev) {
	rev[0]=cos(theta);
	rev[1]=sin(theta)*cos(tau);
	rev[2]=sin(theta)*sin(tau);
}

FB5Cluster::FB5Cluster(const char* aFileName) { 
	ifstream input(aFileName);
   	if(!input) {
      		cerr<<"Can not find cluster file "<<aFileName<<endl;
      		exit(0);
   	}
   	mNumCluster=0;
   	ParseFB5 fb(input);
   	while(fb.parseNode(input)) {
		FB5ClusterNode node;
		fb.newNode(node);
		mCluster.push_back(node);
   	}
	mNumCluster=mCluster.size(); 
	cerr <<"Read in "<<mNumCluster<<" FB clusters"<<endl;
 
   	for(int i=0; i<mNumCluster; i++) {
/*
		cerr <<"cluster "<<i<<endl;
		cerr<<"kapa="<<mCluster[i].kappa<<" beta="<<mCluster[i].beta<<endl;
		cerr<<"gamm1="<<mCluster[i].gamma1[0]<<" "<<mCluster[i].gamma1[1]<<" "<<mCluster[i].gamma1[2]<<endl;
		cerr<<"gamm2="<<mCluster[i].gamma2[0]<<" "<<mCluster[i].gamma2[1]<<" "<<mCluster[i].gamma2[2]<<endl;
		cerr<<"gamm3="<<mCluster[i].gamma3[0]<<" "<<mCluster[i].gamma3[1]<<" "<<mCluster[i].gamma3[2]<<endl;
*/ 
     		KentDensity* tmp=new KentDensity(mCluster[i].kappa, mCluster[i].beta, mCluster[i].gamma1, mCluster[i].gamma2, mCluster[i].gamma3);
		mDens.push_back(tmp);
   	}

   	for(int i=0; i<mNumCluster; i++) {
     		KentSampler* tmp=new KentSampler(mCluster[i].kappa, mCluster[i].beta, mCluster[i].gamma1, mCluster[i].gamma2,mCluster[i].gamma3);
		mSampler.push_back(tmp);
   	}
}

FB5Cluster:: ~FB5Cluster() {
	for(int i=0; i<mNumCluster; i++) {
		if (mDens[i]) delete mDens[i];
		if (mSampler[i]) delete mSampler[i];
   	}
}

//calculate the probability of a given pair of theta and tau in a  cluster
double FB5Cluster::getProb(double theta, double tau, int i) {
	double d[3];
   	polar2xyz(theta, tau, d);
  	return mDens[i]->density(d, true); 
}

//sample a unit vector from cluster i
void FB5Cluster::sample(double* rev, int i) {
	mSampler[i]->sample(rev);
}

