/*******************************************************************************

                                 TSP1.cpp
                                 --------

  ****************************************************************************
  *                                                                          *
  *   This library is free software; you can redistribute it and/or          *
  *   modify it under the terms of the GNU Lesser General Public             *
  *   License as published by the Free Software Foundation; either           *
  *   version 2.1 of the License, or (at your option) any later version.     *
  *                                                                          *
  *   This library is distributed in the hope that it will be useful,        *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *
  *   Lesser General Public License for more details.                        *
  *                                                                          *
  *   You should have received a copy of the GNU Lesser General Public       *
  *   License along with this library; if not, write to the Free Software    *
  *   Foundation, Inc.,                                                      *
  *   59 Temple Place, Suite 330, Boston, MA  02111-1307  USA                *
  *                                                                          *
  ****************************************************************************

  Copyright (C) 2005 The University of Chicago

  Authors:
  Andres Colubri

  Description:
  PL plugin to calculate local phi, psi energies given the information of the
  first neighbor residues (Torsional Statistical Potential with 1st neighbor
  information).

*******************************************************************************/

#include <cstddef> // Needed to use NULL.
#include <string>
#include <cmath>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

#include "TSP1Energy.h"
#include "strtokenizer.h"

/************************** TSP1Energy implementation ***************************/

TSP1Energy::TSP1Energy()
{
	SetDefConfig();
	InitializeMemory(36);
	InitializeEnergy();
}

TSP1Energy::~TSP1Energy()
{
	ReleaseMemory();
}

double TSP1Energy::Energy(int aa0, int aa1, int aa2, double phi, double psi)
{
	int i, j;
	if ((phi <= -angle0) || (angle0 <= phi) || (psi <= -angle0) || (angle0 <= psi)) {
		if (phi <= -angle0) i = 0;
		else if (angle0 <= phi) i = n_bins - 1;
	    	else i = idx(phi);
	
		if (psi <= -angle0) j = 0;
		else if (angle0 <= psi) j = n_bins - 1;
	    	else j = idx(psi);
	
		return values[aa0][aa1][aa2][i][j];
	} else {
		i = idx(phi);
		j = idx(psi);
	
		// Rotating counter-clockwise around the bin.
		double v0 = values[aa0][aa1][aa2][i][j];
		double v1 = values[aa0][aa1][aa2][i + 1][j];
		double v2 = values[aa0][aa1][aa2][i + 1][j + 1];
		double v3 = values[aa0][aa1][aa2][i][j + 1];
	
		double phi0 = -angle0 + bin_size * i;
		double psi0 = -angle0 + bin_size * j;
	
		return LinearInterpolation(v0, v1, v2, v3, phi0, psi0, phi, psi, inv_bin_size, inv_bin_size);
	}
}

int TSP1Energy::AANameToCode(string AAName)
{
        string arrResidueName[21]={"ALA","ARG","ASN","ASP","CYS","GLN","GLU","GLY","HIS","ILE",
				   "LEU","LYS","MET","PHE","PRO","SER","THR","TRP","TYR","VAL","HET"};
        //string arrResidueName[20]={"ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU",
        //                          "MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR"};
        for (int i=0; i<21; i++)
                if (arrResidueName[i]==AAName)
                        return i;
        return -1; //UNK;
}


// Aminoacid names -------------------------------------------------------------

const char BOC_LTR = '<';
const char EOC_LTR = '>';
const char ANY_LTR = 'X';
const char ALA_LTR = 'A';
const char ARG_LTR = 'R';
const char ASN_LTR = 'N';
const char ASP_LTR = 'D';
const char CYS_LTR = 'C';
const char GLN_LTR = 'Q';
const char GLU_LTR = 'E';
const char GLY_LTR = 'G';
const char HIS_LTR = 'H';
const char ILE_LTR = 'I';
const char LEU_LTR = 'L';
const char LYS_LTR = 'K';
const char MET_LTR = 'M';
const char PHE_LTR = 'F';
const char PRO_LTR = 'P';
const char SER_LTR = 'S';
const char THR_LTR = 'T';
const char TRP_LTR = 'W';
const char TYR_LTR = 'Y';
const char VAL_LTR = 'V';
const char HET_LTR = 'Z';

int TSP1Energy::AALetterToCode(char AALetter, bool AcptHET, bool AcptEXT)
{
	if (AcptEXT) {
	  if (AALetter == BOC_LTR) return AA_BOC;
	  if (AALetter == EOC_LTR) return AA_EOC;
	  if (AALetter == ANY_LTR) return AA_ANY;
	}
	if (AALetter == ALA_LTR) return AA_ALA;
	if (AALetter == ARG_LTR) return AA_ARG;
	if (AALetter == ASN_LTR) return AA_ASN;
	if (AALetter == ASP_LTR) return AA_ASP;
	if (AALetter == CYS_LTR) return AA_CYS;
	if (AALetter == GLN_LTR) return AA_GLN;
	if (AALetter == GLU_LTR) return AA_GLU;
	if (AALetter == GLY_LTR) return AA_GLY;
	if (AALetter == HIS_LTR) return AA_HIS;
	if (AALetter == ILE_LTR) return AA_ILE;
	if (AALetter == LEU_LTR) return AA_LEU;
	if (AALetter == LYS_LTR) return AA_LYS;
	if (AALetter == MET_LTR) return AA_MET;
	if (AALetter == PHE_LTR) return AA_PHE;
	if (AALetter == PRO_LTR) return AA_PRO;
	if (AALetter == SER_LTR) return AA_SER;
	if (AALetter == THR_LTR) return AA_THR;
	if (AALetter == TRP_LTR) return AA_TRP;
	if (AALetter == TYR_LTR) return AA_TYR;
	if (AALetter == VAL_LTR) return AA_VAL;
	if (AcptHET) {
	  if (AALetter == HET_LTR) return AA_HET;
	}
	return -1; //UNK;
}

int TSP1Energy::LoadFromFile(string &fn, int mode)
{
	ifstream par_file;
	char aa0_ltr, aa1_ltr, aa2_ltr;
	int aa0, aa1, aa2;
	int i, j;
	double energy;
	
	par_file.open(fn.c_str());
	while (true) {
	  aa0_ltr = aa1_ltr = aa2_ltr = 'A';
	  par_file >> aa0_ltr;
	  if (2 <= mode) par_file >> aa1_ltr;
	  if (3 <= mode) par_file >> aa2_ltr;
	
	  if (par_file.eof()) break;
	
	  aa0 = AALetterToCode(aa0_ltr);
	  aa1 = AALetterToCode(aa1_ltr);
	  aa2 = AALetterToCode(aa2_ltr);
	
	  for (i = 0; i < n_bins; i++)
	      for (j = 0; j < n_bins; j++) {
	          par_file >> energy;
	          values[aa0][aa1][aa2][i][j] = energy;
	      }
	}
	par_file.close();
	
	return 0;
}

void TSP1Energy::Resize(int n)
{
    ReleaseMemory();
    InitializeMemory(n);
    InitializeEnergy();
}

void TSP1Energy::InitializeMemory(int n)
{
	n_bins = n;
	if (0 < n_bins) {
	  bin_size = 360.0 / double(n_bins);
	  inv_bin_size = 1.0 / bin_size; 
	  angle0 = 180.0 - 0.5 * bin_size;
	}
	
	int aa0, aa1, aa2, bin0;
	for (aa0 = 0; aa0 < 20; aa0++)
	  for (aa1 = 0; aa1 < 20; aa1++)
	      for (aa2 = 0; aa2 < 20; aa2++)
	          if (0 < n_bins) {
	              values[aa0][aa1][aa2] = new double*[n_bins];
	              for (bin0 = 0; bin0 < n_bins; bin0++) values[aa0][aa1][aa2][bin0] = new double[n_bins];
	          } else values[aa0][aa1][aa2] = NULL;
}

void TSP1Energy::ReleaseMemory()
{
	if (0 < n_bins) {
	  int aa0, aa1, aa2, bin0;
	  for (aa0 = 0; aa0 < 20; aa0++)
	      for (aa1 = 0; aa1 < 20; aa1++)
	          for (aa2 = 0; aa2 < 20; aa2++) {
	              for (bin0 = 0; bin0 < n_bins; bin0++) delete[] values[aa0][aa1][aa2][bin0];
	              delete[] values[aa0][aa1][aa2];
	          }
	}
}

void TSP1Energy::InitializeEnergy()
{
	int aa0, aa1, aa2, bin0, bin1;
	for (aa0 = 0; aa0 < 20; aa0++)
	  for (aa1 = 0; aa1 < 20; aa1++)
	      for (aa2 = 0; aa2 < 20; aa2++)
	          for (bin0 = 0; bin0 < n_bins; bin0++)
	              for (bin1 = 0; bin1 < n_bins; bin1++) values[aa0][aa1][aa2][bin0][bin1] = 0.0;
}

void TSP1Energy::SetDefConfig()
{
	tsp1_par_file = "";
	energy_coeff = 1.0;
	num_bin = 36;
	tsp1_mode = 3;
}

int TSP1Energy::ReadConfigFile(string CfgName)
{
	ifstream CfgFile;
	CfgFile.open(CfgName.c_str(), ios::in | ios::binary);
	string line, par, val;
	
	while (CfgFile.is_open()) {
		getline(CfgFile, line);
		if (line != "" && line[0] != '#') {
			strtokenizer strtokens(line,"=");
			int num=strtokens.count_tokens();
			if (num<2) continue;
			par = strtokens.token(0);
			val = strtokens.token(1);
			trim2(par);
			trim2(val);
			//cerr << par << "=" << val << endl;
			if (par == "Potential mode") { tsp1_mode = atoi(val.c_str()); }
			if (par == "Number of bins") {
	   			num_bin = atoi(val.c_str());
	   			Resize(num_bin);
			}
			if (par == "Parameter file") { tsp1_par_file = val; }
			if (par == "Energy coefficient") { energy_coeff = atof(val.c_str()); }
		}
	
		if (CfgFile.eof()) break;
	}
	return 0;
}

void TSP1Energy::Init(string CfgName)
{
	ReadConfigFile(CfgName);
	LoadFromFile(tsp1_par_file, tsp1_mode);
}
