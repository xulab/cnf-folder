// *******BY TINA, May 23, 2003
// for mkdir
#ifdef WIN32
#include <direct.h>
#else
#include <sys/stat.h>
#endif
#include <errno.h>
// *******BY TINA, May 23, 2003

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
using namespace std;
//#include <strings.h>
#ifndef WIN32
#include <unistd.h>
#endif



#include "IP_constant.h"
#include "config.h"
#include "stringTokenizer.h"

const char Config::RAPTOR_HOME_STRING [] = "CRF_HOME"; 

Config::Config(){
    	// read it once, use everywhere!
    	RAPTOR_HOME[0] = 0; 

    	sprintf(RAPTOR_HOME, "%s", getenv(RAPTOR_HOME_STRING));
    	//sprintf(RAPTOR_HOME, "%s", "Never used it ");
    	if (RAPTOR_HOME[0] == 0) 
    	{
       		fprintf(stderr, "Please specify environmental variable RAPTOR_HOME before running RAPTOR.\n");
        	exit (-1);
    	}else{
	//	 printf("RAPTOR_HOME=%s\n",RAPTOR_HOME);
	}

    	sprintf(RAPTOR_HOME_VAR, "$%s", RAPTOR_HOME_STRING);
    	// *******BY TINA, May 8, 2003
	processID[0]=0;
	//setPath(TempBondPath,"./");
	//setPath(SeqBondPath,"./");
	sourceFile[0]=0;

//	printf("finish creating a config object!\n");

}

Config::Config(char* fileName){
    	// read it once, use everywhere!
    	cout<<"I reach the start of config"<<endl;
        RAPTOR_HOME[0] = 0; 

    	sprintf(RAPTOR_HOME, "%s", getenv(RAPTOR_HOME_STRING));
    	if (RAPTOR_HOME[0] == 0) 
    	{
       		fprintf(stderr, "Please specify environmental variable RAPTOR_HOME before running RAPTOR.\n");
        	exit (-1);
    	}else{
		// printf("RAPTOR_HOME=%s\n",RAPTOR_HOME);
	}

    	sprintf(RAPTOR_HOME_VAR, "$%s", RAPTOR_HOME_STRING);
	processID[0]=0;
	sprintf(sourceFile,"%s",fileName);

//	printf("finish creating a config object with file %s!\n",fileName);
}

Config::~Config(){
	//free sessions
	for(int i=0;i<sessionNum;i++){
		for(int j=0;j<sessions[i].lineNum;j++)
			if (sessions[i].bufs[j]) delete sessions[i].bufs[j];
	}
	for(int j=0;j<common_session.lineNum;j++)
		if (common_session.bufs[j]) delete common_session.bufs[j];
}

void Config::Initialize(){

	sessionNum=0;
	common_session.lineNum=0;

	CallModeller=0;
	ModellerOut[0]=0;
	Modeller[0]=0;
	NumModels=5;

	OutputBackbone=true;

	RAPTORadm[0]=0;

	NoSolventType=false;

	PrintPS=false;
	OutputDetailedResult=false;

	ProcessNum=10;
	sprintf(ComputeType,"%s","np");


	//flag_np=flag_nopair;

	//LoopFinder=LOOPFINDER;
	//LoopFinder=true;
	LoopFinder=false;

	UsePSP=true;
	UsePSM=true;
	UseSS=true;
	UseContactCapacity=false;
	//UseNMRbond=false;
	UseVolume=false;
	UseDistanceDependentContact=false;

	alignment_type=GLOBAL_LOCAL;

	CalcZScore=true;
	StrictZScore=true;

	CoreGrayZone=COREGRAYZONE;

	MinCoreSize=MINCORESIZE;
	MinCoreLen=MINCORELEN;
	MinExLinkNum=MINEXLINKNUM;
	//MaxLoopSize=MAXLOOPSIZE;
	MaxLoopSize=25;
	MaxSkipSeqSize=MAXSKIPSEQSIZE;
	MaxContactDist=MAXCONTACTDIST;
	PPIMaxContactDist=4.5;

	MinPairDist=MINPAIRDIST;
	UseGapAlign=USEGAPALIGN;
	UseEndGap=USEENDGAP;
	UseSpecialGap=false;
	LevelOneRemoval=LEVELONEREMOVAL;
	LevelTwoRemoval=LEVELTWOREMOVAL;
	FirstCut=FIRSTCUT;
	SecondCut=SECONDCUT;
	ConserveLoopSize=CONSERVELOOPSIZE;
	LoopMinSeqSize=LOOPMINSEQSIZE;
	LoopConfidence=LOOPCONFIDENCE;
	LoopVariation=LOOPVARIATION;
		
	FilterCutOff=FILTERCUTOFF;
	FilterSingleton=FILTERSINGLETON;
	FilterMutate=FILTERMUTATE;

	FitFilter=true;
	SSFilter=true;
	
	FilterCutOff=-10;
	FilterSingleton=10;
	FilterMutate=10;

	MinSSFilterFlag=MINSSFILTERFLAG;
	MinSSFilterSize=MINSSFILTERSIZE;
	MinSSFilterLevel=MINSSFILTERLEVEL;
	SingleSSFilterLevel=SINGLESSFILTERLEVEL;

	//DistCutOff=MaxContactDist*MaxContactDist;

	
	weightMutation=0;
	weightSingleton=0;
	weightPair=0;
	weightGapPenalty=0;
	weightLoopGap=0;
	weightSStruct=0;
	weightContactCapacity=0;

	weightNMR=0;
	//weightNMRsingle=0;
	//weightNMRPenalty=0;

	weightPPI=0;

	sprintf(ThreadResultFile,"IPThread_result.dat");
	sprintf(ThreadAlignFile,"IPThread_align.dat");

	SVMEstimator[0]=0;
	SVMParams[0]=0;
	SVMStandardParamFile[0]=0;

	zScoreCutoff=-2;
	zScoreCutoffPlus=1;

	MaxThreadTime=3600; //default  1 hours



	err_log_fp=fopen(ErrorLogFile,"ab");
	if (!err_log_fp){
		fprintf(stderr,"WARNING:can not open error log file for write!\n");
	}
	cpu_log_fp=fopen(CPULogFile,"ab");
	if (!cpu_log_fp){
		fprintf(stderr,"WARNING:can not open CPU log file for write!\n");
	}



	GAPBasePenalty=weightLoopGap*OPENGAPPENALTY;
	GAPIncPenalty=weightLoopGap*ONEINDELPENALTY;

	debug_level=DEBUG_LEVEL0;

	TempRankMethod=RankBySVM;
	DetailedScoreRank=0;

	LPMethod=LPBarrier;

	//setPath(PDBPath,"PDB");

	SkipPSPGeneration=false;
	SkipSSGeneration=false;

	templateList[0]=0;
	topTemplateList[0]=0;
	taskList[0]=0;
	topTaskList[0]=0;
	NumOfTopTemplates=0;
	OutAlignmentModel=0;
}

void Config::configure(){

	//printf("begin to initialize the config object!\n");
	Initialize();

	//printf("begin to read the config file!\n");
	readConfiguration();

	//printf("begin to postprocess after reading the config file!\n");
	PostProcess();
	//printf("finish postprocessing after reading the config file!\n");


}

void Config::WriteSession(char* file, int index){
	//first write the common session
	FILE* fp=fopen(file,"wb");
	if (!fp){
		fprintf(stderr,"cannot open %s for write!\n",file);
		exit(-1);
	}
	for (int i=0;i<common_session.lineNum;i++)
		fprintf(fp,"%s\n",common_session.bufs[i]);
	//then output the session content
	if (index<0 || index>=sessionNum) {
		fclose(fp);
		return;
	}
	for (int j=0;j<sessions[index].lineNum;j++){
		fprintf(fp,"%s\n",sessions[index].bufs[j]);
	}
	fclose(fp);
	
}


//do some postprocess after reading the configuration file
void Config::PostProcess(){
	if(!UseSS) LoopFinder=false;

	//postprocess, the following codes must be executed after reading the configuration file
/*
	if (UseNMRbond){
		MaxContactDist=MAXNMRCONTACTDIST;
	}else{
		MaxContactDist=MAXCONTACTDIST;
	}
*/
	//DistCutOff=MaxContactDist*MaxContactDist;

	if (access(WeightFile,R_OK)==0)	setWeight(WeightFile);

    	if (OutputPath[0] != 0 && access(SeqPath,R_OK)!=0)
    	{
    	    // first create the OUTPUT directory, if it doesn't exist
       		//int fd;
#ifdef WIN32
        	//fd = _mkdir(OutputPath);
        	_mkdir(OutputPath);
#else
		mkdir(OutputPath, 0755);
/*
        	if ((fd = mkdir(OutputPath, 0755 )) && errno == EACCES)
        	{
            		fprintf(stderr, "Failed to create output file directory %s.\n", OutputPath);
        	}
*/
#endif 
    	}//if

    	if (finished_dir[0] != 0 && access(SeqPath,R_OK)!=0)
    	{
       		 // first create the FINISHED directory, if it doesn't exist
 //      		 int fd;
#ifdef WIN32
       		 //fd = _mkdir(finished_dir);
       		 _mkdir(finished_dir);
#else
		mkdir(finished_dir,0755);
/*
        	if ((fd = mkdir(finished_dir, 0755 )) && errno == EACCES)
        	{
        	    fprintf(stderr, "Failed to create finished sequence file directory %s.\n", finished_dir);
        	}
*/
#endif 
    	}//if

	if (SeqPath[0]!=0 && access(SeqPath,R_OK)!=0)
	{
#ifdef WIN32
		_mkdir(SeqPath);
#else
		mkdir(SeqPath,0755);
#endif
	}
}

void Config::SetProcessID(char* id){
	sprintf(processID,"%s",id);
}

void Config::SetProcessID(int id){
	sprintf(processID,"%d",id);
}


void Config::closeLogFile(){
	if (err_log_fp)
		fclose(err_log_fp);

	if (cpu_log_fp)
		fclose(cpu_log_fp);
}

void Config::writeErrorLog(char* s){
	if (err_log_fp)
		fprintf(err_log_fp,"%s\n",s);
		fflush(err_log_fp);
	}

void Config::writeCPUTime(char* t){
	if (cpu_log_fp)
		fprintf(cpu_log_fp,"%s\n",t);
	}

void Config::setTemplatePath(char* path,char* type){
	memcpy(TemplatePath,path,sizeof(char)*MaxLineSize);
	memcpy(TemplateType,type,sizeof(char)*MaxLineSize);
}

void Config::setPartitionPath(char* path,char* type){
	memcpy(PartitionPath,path,sizeof(char)*MaxLineSize);
	memcpy(PartitionType,type,sizeof(char)*MaxLineSize);
}

void Config::setSeqPath(char* path){
	setPath(SeqPath,path);
}
void Config::setPSPPath(char* path){
	setPath(PSPPath,path);
}
int Config::EnablePSM(bool flag){
	UsePSM=flag;
	return flag;
}
	
void Config::setWeight(char* fileName){

	FILE* fp=fopen(fileName,"r");
	if (!fp){
		fprintf(stderr,"fatal error:can not open weight file %s for read!\n",fileName);
		exit(-1);
	}


	char buf[4096];
	while(fgets(buf,4096,fp)){
		char paramName[256];
		float paramValue;
		char paramValueStr[MaxLineSize];
		char tmpStr[MaxLineSize];

	//	printf("param buf=%s\n",buf);
		for(int i=0;i<strlen(buf);i++)
			if(buf[i]=='=') buf[i]=' ';

		int ret=sscanf(buf,"%s%s",paramName,tmpStr);
		if (ret<2 || ret==EOF) continue;

		if (strncasecmp(tmpStr,RAPTOR_HOME_VAR, strlen(RAPTOR_HOME_VAR))==0){
			sprintf(paramValueStr,"%s%s",RAPTOR_HOME, tmpStr+strlen(RAPTOR_HOME_VAR));
		}else sprintf(paramValueStr,"%s",tmpStr);

		if (EQUAL(paramName,SVM_PARAM_STR)){
			sprintf(SVMParams,"%s",paramValueStr);
			continue;
		}else if(EQUAL(paramName,SVM_STANDARD_PARAM_FILE_STR)){
			sprintf(SVMStandardParamFile,"%s",paramValueStr);
			continue;
		}else paramValue=atof(paramValueStr);

	//	printf("paramName=%s, paramValue=%f\n",paramName,paramValue);

		if (EQUAL(paramName,"weightMutation")){
			weightMutation=paramValue;
			continue;
		}
		if (EQUAL(paramName,"weightSingleton")){
			weightSingleton=paramValue;
			continue;
		}
		if (EQUAL(paramName,"weightPair")){
			weightPair=paramValue;
			continue;
		}
		if(EQUAL(paramName,"weightLoopGap")){
			weightLoopGap=paramValue;
			continue;
		}
		if (EQUAL(paramName,"weightGapPenalty")){
			weightGapPenalty=paramValue;
			continue;
		}
		if (EQUAL(paramName,"weightSStruct")){
			weightSStruct=paramValue;
			continue;
		}
		if (EQUAL(paramName,"weightContactCapacity")){
			weightContactCapacity=paramValue;
			continue;
		}
/*
		if (EQUAL(paramName,"weightPSM")){
			weightPSM=paramValue;
			continue;
		}
*/
		
		if (EQUAL(paramName,"weightNMR")){
			weightNMR=paramValue;
			continue;
		}

		if (EQUAL(paramName,"weightPPI")){
			weightPPI=paramValue;
			continue;
		}

/*
		if (EQUAL(paramName,"weightNMRsingle")){
			weightNMRsingle=paramValue;
			continue;
		}

		if (EQUAL(paramName,"weightNMRPenalty")){
			weightNMRPenalty=paramValue;
			continue;
		}
*/
	}

	fclose(fp);

	//for debug
	
	if (debug_level>=DEBUG_LEVEL2)
		printf("wM=%.2f, wSingle=%.2f,wPair=%.2f,wLoop=%.2f,wGap=%.2f,wSS=%.2f,wNMR=%.2f\n",weightMutation,weightSingleton,weightPair,weightLoopGap,weightGapPenalty,weightSStruct,weightNMR);

	//recalculate some values

	GAPBasePenalty=weightLoopGap*OPENGAPPENALTY;
	GAPIncPenalty=weightLoopGap*ONEINDELPENALTY;

} 	

/*
int Config::EnablePairWise(bool flag){
	if (flag){
		setWeight(PWWeightFile);
	}else setWeight(NPWeightFile);
	return 1;
}
*/

void Config::readConfiguration(){

	FILE* fp=0;
	if (sourceFile[0]!=0){ //if the configuration file is given, then use the given file
				//otherwise search in the following two default directory
		fp=fopen(sourceFile,"r");
		if (!fp){
			fprintf(stderr,"cannot open configuration file %s for read!\n",sourceFile);
			exit(-1);
		}
	}else {
		fp=fopen(CONFIGFILE,"r");
		if (!fp){
    			// *******BY TINA, May 8, 2003
    			// first read the conf file in working directory, if it's not
    			// present, try the standard one in $RAPTOR_HOME/data/parameters/
        		if (debug_level> DEBUG_LEVEL0)
            			fprintf(stderr,"configuration file not found in current directory. Attempting %s/data/parameters/%s...\n", RAPTOR_HOME, CONFIGFILE);
        		char temp[1280];
        		sprintf(temp, "%s/data/parameters/%s", RAPTOR_HOME, CONFIGFILE);
        		fp  = fopen(temp, "r");
        		if (!fp)
        		{
            			fprintf(err_log_fp,"WARNING:configuration file not found in either the curent or RAPTOR's installed directory!\n");
            			return;
        		}//if
    			// *******BY TINA, May 8, 2003
			sprintf(sourceFile,"%s",temp);
		}else sprintf(sourceFile,"%s",CONFIGFILE);

	}

	char buf[1024];

	while(fgets(buf,1024,fp)){
		char* s=buf;
		while(s[0] && isspace(s[0])) s++;
		if (!s[0]) continue;
		if (s[0]=='#') continue; //comments
		if (s[0]=='[' || s[0]=='<') continue; //tags


		if (strncasecmp(s,SESSION_START_STR,strlen(SESSION_START_STR))==0){
			//start to read information for one session
			char buf2[1024];

			SESSION* session=sessions+sessionNum;
			session->lineNum=0;

			while(fgets(buf2,1024,fp)){
				char* s=buf2;
				while(s[0] && isspace(s[0])) s++;
				if (!s[0]) continue;
				if (s[0]=='#') continue; //comments
				if (s[0]=='[' || s[0]=='<') continue; //tags
				if (strncasecmp(s,SESSION_END_STR,strlen(SESSION_END_STR))==0){
					sessionNum++;
					break;
				}
				session->bufs[session->lineNum]=new char[strlen(s)+1];
				sprintf(session->bufs[session->lineNum],"%s",s);
				session->lineNum++;
				
			}
		} else {
			common_session.bufs[common_session.lineNum]=new char[strlen(buf)+1];
			sprintf(common_session.bufs[common_session.lineNum],"%s",buf);
			common_session.lineNum++;
			ProcessOneLine(buf);
		}
	}
	fclose(fp);

	//printf("in total there are %d sessions\n",sessionNum);
}

void Config::ProcessOneLine(char* buf){

	char paramName[4096];
	char paramValue[4096];

	int ret=parseLine(buf, paramName, paramValue);

	if (ret<2 || ret==EOF) return;

	int len=strlen(paramValue);

	if (EQUAL(paramName, RAPTOR_ADM_STR)){
		sprintf(RAPTORadm,"%s",paramValue);
	}else if (EQUAL(paramName,TEMP_PATH_STR)){
		setPath(TemplatePath,paramValue);
	}else if (EQUAL(paramName,TEMP_TYPE_STR)){
		sprintf(TemplateType,"%s",paramValue);
	}else if (EQUAL(paramName,PART_PATH_STR)){
		setPath(PartitionPath,paramValue);
	}else if (EQUAL(paramName,PART_TYPE_STR)){
		sprintf(PartitionType,"%s",paramValue);
	}else if (EQUAL(paramName,PARAM_PATH_STR)){
		setPath(ParamPath,paramValue);
	}else if (EQUAL(paramName,PPI_PATH_STR)){
		setPath(PPIPath,paramValue);
	}else if (EQUAL(paramName,SEQ_PATH_STR)){
		setPath(SeqPath,paramValue);
	}else if (EQUAL(paramName,PSM_PATH_STR)){
		setPath(PSMPath,paramValue);
	}else if (EQUAL(paramName,PSP_PATH_STR)){
		setPath(PSPPath,paramValue); 
	}else if (EQUAL(paramName,TEMP_BOND_PATH_STR)){
		setPath(TempBondPath,paramValue);
	}else if (EQUAL(paramName,SEQ_BOND_PATH_STR)){
		setPath(SeqBondPath,paramValue);
	}else if (EQUAL(paramName,TEMP_VOLUME_PATH_STR)){
		setPath(TempVolPath,paramValue);
	}else if (EQUAL(paramName,OUTPUT_PATH_STR)){
		setPath(OutputPath,paramValue); 
	}else if (EQUAL(paramName,PDB_PATH_STR)){
		setPath(PDBPath,paramValue);
	}else if (EQUAL(paramName,SS_PATH_STR)){
		setPath(SSPath,paramValue);
	}else if (EQUAL(paramName,SARF_PATH_STR)){
		setPath(SarfPath,paramValue);
	}else if(EQUAL(paramName,COMPRESS_PATH_STR)){
		setPath(ComRootDir,paramValue);
	}else if(EQUAL(paramName,COMPRESS_PATH_STR2)){
		setPath(ComRootDir2,paramValue);

	}else if(EQUAL(paramName,LOOPFINDER_STR)){
		SetBool(&LoopFinder,paramValue);

	}else if(EQUAL(paramName,USE_SS_STR)){
		SetBool(&UseSS, paramValue);

	}else if(EQUAL(paramName,USE_CONTACT_STR)){
		SetBool(&UseContactCapacity,paramValue);

	}else if(EQUAL(paramName,USE_PSM_STR)){
		SetBool(&UsePSM,paramValue);

	}else if(EQUAL(paramName,USE_PSP_STR)){
		SetBool(&UsePSP,paramValue);
/*
	}else if(EQUAL(paramName,USE_NMR_STR)){
		if (EQUAL(paramValue,"Yes"))
			UseNMRbond=true;
		else UseNMRbond=false;
*/
	}else if(EQUAL(paramName,USE_VOL_STR)){
		if (EQUAL(paramValue,"Yes"))
			UseVolume=true;
		else UseVolume=false;
	}else if(EQUAL(paramName,USE_DIST_DEPEND_CONTACT_STR)){
		if (EQUAL(paramValue,"Yes"))
			UseDistanceDependentContact=true;
		else UseDistanceDependentContact=false;
	}else if(EQUAL(paramName,CALC_STRICT_ZSCORE_STR)){
		if (EQUAL(paramValue,"Yes"))
			StrictZScore=true;
		else StrictZScore=false;
	}else if(EQUAL(paramName,CALC_ZSCORE_STR)){
		if (EQUAL(paramValue,"Yes"))
			CalcZScore=true;
		else CalcZScore=false; 
	}else if(EQUAL(paramName,MAXLOOPSIZE_STR)){
		MaxLoopSize=atoi(paramValue);
	}else if(EQUAL(paramName,FILTERCUTOFF_STR)){
		FilterCutOff=atof(paramValue);
	}else if(EQUAL(paramName,FILTERSINGLETON_STR)){
		FilterSingleton=atof(paramValue);
	}else if(EQUAL(paramName,FILTERMUTATION_STR)){
		FilterMutate=atof(paramValue);
	}else if(EQUAL(paramName,ZSCORE_CUTOFF_STR)){
		zScoreCutoff=atof(paramValue);
	}else if(EQUAL(paramName,ZSCORE_CUTOFF_PLUS_STR)){
		zScoreCutoffPlus=atof(paramValue);
	}else if(EQUAL(paramName,THREAD_RESULT_FILE_STR)){
		sprintf(ThreadResultFile,"%s",paramValue);
	}else if(EQUAL(paramName,THREAD_XML_FILE_STR)){
		sprintf(ThreadXMLFile,"%s",paramValue);
	}else if(EQUAL(paramName,THREAD_ALIGN_FILE_STR)){
		sprintf(ThreadAlignFile,"%s",paramValue);
	}else if(EQUAL(paramName,WEIGHT_FILE_STR)){
		sprintf(WeightFile,"%s",paramValue);
	}else if(EQUAL(paramName,FITFILTER_STR)){
		if (EQUAL(paramValue,"Yes"))
			FitFilter=true;
		else FitFilter=false;
	}else if(EQUAL(paramName,SSFILTER_STR)){
		if (EQUAL(paramValue,"Yes"))
			SSFilter=true;
		else SSFilter=false;
	}else if(EQUAL(paramName,USE_END_GAP_STR)){
		if (EQUAL(paramValue,"Yes"))
			UseEndGap=true;
		else UseEndGap=false;
	}else if(EQUAL(paramName,USE_SPECIAL_GAP_STR)){
		if (EQUAL(paramValue,"Yes"))
			UseSpecialGap=true;
		else UseSpecialGap=false;
	}else if(EQUAL(paramName,MODEL_PATH_STR)){
		setPath(ModelPath,paramValue);
	}else if(EQUAL(paramName,OUT_PS_STR)){
		if (EQUAL(paramValue,"Yes"))
			PrintPS=true;
		else PrintPS=false;
	}else if(EQUAL(paramName,OUT_DETAIL_RES_STR)){
		if (EQUAL(paramValue,"Yes"))
			OutputDetailedResult=true;
		else OutputDetailedResult=false;
	}else if(EQUAL(paramName,OUT_BACKBONE_STR)){
		SetBool(&OutputBackbone,paramValue);
	}else if(EQUAL(paramName,MAX_THREAD_TIME_STR)){
		MaxThreadTime=atoi(paramValue);
	}else if(EQUAL(paramName,PROCESS_NUM_STR)){
		ProcessNum=atoi(paramValue);
		if (ProcessNum<0) ProcessNum=10;
	}else if (EQUAL(paramName,COMPUTE_TYPE_STR)){
		sprintf(ComputeType,"%s",paramValue);
	}else if (EQUAL(paramName,MIN_CORE_LEN_STR)){
		MinCoreLen=atoi(paramValue);
	}else if (EQUAL(paramName,MIN_EX_LINK_NUM_STR)){
		MinExLinkNum=atoi(paramValue);
	}else if (EQUAL(paramName,MIN_PAIR_DIST_STR)){
		MinPairDist=atof(paramValue);
	}else if (EQUAL(paramName,MAX_CONTACT_DIST_STR)){
		MaxContactDist=atof(paramValue);
	}else if (EQUAL(paramName,PPI_MAX_CONTACT_DIST_STR)){
		PPIMaxContactDist=atof(paramValue);
	}else if (EQUAL(paramName,NO_SOLVENT_STR)){
		if (EQUAL(paramValue,"Yes"))
			NoSolventType=true;
		else NoSolventType=false;
	}else if (EQUAL(paramName, LP_METHOD_STR)){
		if (EQUAL(paramValue,"dualSimplex"))
			LPMethod=LPDualSimplex;
		else if(EQUAL(paramValue, "primalSimplex"))
			LPMethod=LPPrimalSimplex;
		else if(EQUAL(paramValue, "eitherSimplex"))
			LPMethod=LPEitherSimplex;
		else if (EQUAL(paramValue, "Smart"))
			LPMethod=LPSmart;
		else
			LPMethod=LPBarrier;
	}else if(EQUAL(paramName,ALIGNMENT_TYPE_STR)){
		if (EQUAL(paramValue,"global"))
			alignment_type=GLOBAL_ALIGNMENT;
		else alignment_type=GLOBAL_LOCAL;
	}else if (EQUAL(paramName,DEBUG_LEVEL_STR)){
		debug_level=atoi(paramValue);
		if (debug_level<0)
			debug_level=0;
	}else if (EQUAL(paramName,PSP_PROGRAM_STR)){
		sprintf(PSP_program,"%s",paramValue);
	}else if (EQUAL(paramName,SS_PROGRAM_STR)){
		sprintf(SSpred_program,"%s",paramValue);
	}else if (EQUAL(paramName,BTHREAD_PROGRAM_STR)){
		sprintf(bThread_program,"%s",paramValue);
	}else if (EQUAL(paramName,ESTIMATE_PROGRAM_STR)){
		sprintf(estimate_program,"%s",paramValue);
	}else if (EQUAL(paramName,BLAST_PROGRAM_STR)){
		sprintf(blast_program,"%s",paramValue);
	}else if (EQUAL(paramName,BLAST_DB_STR)){
		sprintf(blast_db,"%s",paramValue);
	}else if (EQUAL(paramName,REQUEST_PATH_STR)){
		setPath(request_dir,paramValue);
	}else if (EQUAL(paramName,FINISHED_PATH_STR)){
		setPath(finished_dir,paramValue);
	}else if (EQUAL(paramName, TEMP_RANK_METHOD_STR)){
		if (EQUAL(paramValue,"SVM"))
			TempRankMethod=RankBySVM;
		else if(EQUAL(paramValue,"ZScore"))
			TempRankMethod=RankByZRaw;
		else if (EQUAL(paramValue, "RawScore"))
			TempRankMethod=RankByRawScore;
		else
			TempRankMethod=RankBySVM;
	}else if (EQUAL(paramName, DETAILED_SCORE_RANK_STR)){
		if (EQUAL(paramValue,"yes"))
			DetailedScoreRank=1;
	}else if (EQUAL(paramName, MODELLER_PATH_STR)){
		sprintf(Modeller,"%s",paramValue);
	}else if (EQUAL(paramName, MODELLER_OUTPATH_STR)){
		setPath(ModellerOut,paramValue);
	}else if (EQUAL(paramName, MODELLER_NUM_STR)){
		NumModels=atoi(paramValue);
	}else if (EQUAL(paramName, USE_MODELLER_STR)){
		if (EQUAL(paramValue,"yes"))
			CallModeller=1;
		else CallModeller=0;
	}else if (EQUAL(paramName,SKIP_PSP_STR)){
		SetBool(&SkipPSPGeneration,paramValue);
	}else if (EQUAL(paramName,SKIP_SS_STR)){
		SetBool(&SkipSSGeneration,paramValue);
	}else if (EQUAL(paramName,TEMPLATE_LIST_STR)){
		sprintf(templateList,"%s",paramValue);
	}else if (EQUAL(paramName,TOP_TEMPLATE_LIST_STR)){
		sprintf(topTemplateList,"%s",paramValue);
	}else if (EQUAL(paramName,TASK_LIST_STR)){
		sprintf(taskList,"%s",paramValue);
	}else if (EQUAL(paramName,TOP_TASK_LIST_STR)){
		sprintf(topTaskList,"%s",paramValue);
	}else if (EQUAL(paramName,TOP_TEMPLATE_NUM_STR)){
		NumOfTopTemplates=atoi(paramValue);
		if (NumOfTopTemplates<0) NumOfTopTemplates=0;
	}else if (EQUAL(paramName,OUT_ALIGN_MODEL_STR)){
		OutAlignmentModel=atoi(paramValue);
	}else if (EQUAL(paramName,SVM_ESTIMATOR_STR)){
		sprintf(SVMEstimator,"%s",paramValue);
	}

}

void Config::ProcessOneSession(int index){
	if (index<0 || index>=sessionNum) return;
	ProcessOneSession(sessions+index);
}

void Config::ProcessOneSession(SESSION* session){
	if (debug_level>=DEBUG_LEVEL1)
		printf("processing one single session...\n");
	for(int i=0;i<session->lineNum;i++)
		ProcessOneLine(session->bufs[i]);
	//insert code here to do postprocessing
	PostProcess();
}


void Config::SetBool(bool* a, char* value){
	if (EQUAL(value,"Yes"))
		*a=true;
	else *a=false;
}

void Config::setPath(char* path, char* value){
	if (value[strlen(value)-1]==FSEP){
		sprintf(path,"%s",value);
	}else{
		sprintf(path,"%s%c",value,FSEP);
	}
    if (debug_level>=DEBUG_LEVEL2)
        printf("set path:%s\n",path);
}

//int parseLine(char* buf, char* paramName, char* paramValue){
int Config::parseLine(char* buf, char* paramName, char* paramValue){
	paramName[0]=0;
	paramValue[0]=0;
	StringTokenizer* strTokens=new StringTokenizer(buf," \t\r\n");
	char* token=strTokens->getNextToken();
	if (!token){ 
		if (strTokens) delete strTokens;
		return 0;
	}
	sprintf(paramName,"%s",token);
	token=strTokens->getNextToken();
	if (!token){
		if (strTokens) delete strTokens;
		return 1;
	}

    char temp[MAX_STRING_LEN];

	do{
    // *******BY TINA, May 21, 2003
    // detect env var RAPTOR_HOME and do substitution
    /*
        if (!strncasecmp(token, Config::RAPTOR_HOME_VAR,
                    strlen(Config::RAPTOR_HOME_VAR)))
                    */
        if (!strncasecmp(token, RAPTOR_HOME_VAR,
                    strlen(RAPTOR_HOME_VAR)))
        {
            temp[0] = 0;
            //char temp[MAX_STRING_LEN];
            // first replace RAPTOR_HOME_VAR with its value
            //strcpy(temp, Config::RAPTOR_HOME);
            strcpy(temp, RAPTOR_HOME);
            // then concatenate the remaining substring
            //strcat(temp, &(token[strlen(Config::RAPTOR_HOME_VAR)]));
            strcat(temp, &(token[strlen(RAPTOR_HOME_VAR)]));

            strcat(paramValue,temp);
        }//if
    // *******BY TINA, May 21, 2003
        else
        {
            strcat(paramValue,token);
        }//else
		token=strTokens->getNextToken();
		if (!token)	break;
		strcat(paramValue," ");
	}while(1);

	if (strTokens) delete strTokens;
	return 2;
}

char* Config::GetXMLOutputFile(){
	return ThreadXMLFile;
}

/*
void Config::SetXMLOutputFile(char* seqName,char* id){
	sprintf(xmlOutputFile,"%s.xml.%s",seqName,id);
}
*/


    // *******BY TINA, May 21, 2003
void Config::printConfig(char * msg)
{
    fprintf(stdout, "*********%s\n", msg);
    fprintf(stdout, "ParamPath: %s\n", ParamPath);
    fprintf(stdout, "TemplatePath: %s\n", TemplatePath);
    fprintf(stdout, "SeqPath: %s\n", SeqPath);
    fprintf(stdout, "bThread_program: %s\n", bThread_program);
    fprintf(stdout, "PartitionPath: %s\n", PartitionPath);
    fprintf(stdout, "TemplateType: %s\n", TemplateType);
    fprintf(stdout, "debugLevel: %d\n", debug_level);
    fprintf(stdout, "SSpred_program: %s\n", SSpred_program);
    fprintf(stdout, "PSP_program: %s\n", PSP_program);
    fprintf(stdout, "*********\n");

}//
    // *******BY TINA, May 21, 2003

 /*
int main()
{
    Config *config = new Config(); 
    config->configure();
    config->printConfig();
    }//
 */
