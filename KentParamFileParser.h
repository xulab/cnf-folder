#ifndef PARSE_FB5
#define PARSE_FB5

#include <iostream>
#include <fstream>
using namespace std;
class FB5ClusterNode;

class ParseFB5
{ 
public:
   ParseFB5(ifstream& input);
   ParseFB5(char* input);   
protected:
   double mGamma[3][3];
   int node; 
   double beta;
   double kappa;
   double theta;
   double tau;
   
public:
   void parseSeqProb(ifstream& input, int numNode, double** seqProb);
   void parseSecProb(ifstream& input, int numNode, double** secProb);
   bool parseNode(ifstream& input);
   void writeNode(ofstream& output);
   void newNode(FB5ClusterNode& rev);
};
#endif
