#include "stringTokenizer.h"

StringTokenizer::StringTokenizer(char* s,char* delimiter){
	str=new char[strlen(s)+1];
	strncpy(str,s,strlen(s));
	str[strlen(s)]=0;

	this->delimiter=new char[strlen(delimiter)+1];
	strncpy(this->delimiter,delimiter,strlen(delimiter));
	this->delimiter[strlen(delimiter)]=0;

	pointer=0;

	token=new char[strlen(s)+1];

}

StringTokenizer::~StringTokenizer(){
	if (str)	delete str;
	if (token)	delete token;
	if (delimiter)	delete delimiter;

}

void StringTokenizer::rewind(){
	pointer=0;
}

char* StringTokenizer::getNextToken(){

	int len=strlen(str);

	while(pointer<len && isDelimiter(str[pointer]))
		pointer++;

	if (pointer>=(int)strlen(str))	return 0;

	int index=pointer;

	while(pointer<len && !isDelimiter(str[pointer]) )
		pointer++;

	strncpy(token,str+index,pointer-index);
	token[pointer-index]=0;

	return token;
		
}

int StringTokenizer::isDelimiter(char c){
	int i=0;
	while(i<(int)strlen(delimiter) && toupper(c)!=toupper(delimiter[i]))
		i++;
	if (i<(int)strlen(delimiter))	return 1;
	else return 0;

}

int isEmpty(char* s){
	int i=0;
	while (s[i]  && isspace(s[i])) i++;

	if (!s[i]) return 1;
	return 0;
}

// *********** BY TINA, May 13, 2003
// this is to emulate more fully the Java counterpart
int StringTokenizer::countTokens()
{
    int backup_pointer = pointer;
    int numTokens = 0;

    while (getNextToken())
    {
        numTokens ++;
    }//while

    pointer = backup_pointer;
    return numTokens; 
}//int
// *********** BY TINA, May 13, 2003
