#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include "EPAD.h"
#include "strtokenizer.h"

NN_Sequence::NN_Sequence(int length_seq, EPAD* pModel, string name, int label_flag)
{
	m_pModel = pModel;
	p_name= name;
	this->length_seq = length_seq;
	this->label_flag=label_flag;
	length_contact=length_seq*length_seq;
	dist_pred=new ScoreMatrix(length_seq, length_seq, "dist_pred");
	dist_pred->Fill(0);
	backbone_probs=new double[5*5*TOTAL_COND_LABEL*length_seq*length_seq];
	memset(backbone_probs, 0, sizeof(double)*5*5*TOTAL_COND_LABEL*length_seq*length_seq);

	IdxMap=new int[length_seq*length_seq];
	memset(IdxMap, -1, sizeof(int)*length_contact);
	rIdxMap=new int[length_seq*length_seq];
	centerMap=new int[length_seq*length_seq];
	IdxMapCenter=new int[length_seq];
	IdxMapCofactor=new int[length_seq];
	for (int i=0; i<length_seq; i++)
		IdxMapCofactor[i]=IdxMapCenter[i]=-1;
	backbone_prob_flags=new ScoreMatrix(length_seq, length_seq, "backbone_prob_flags");

	forward=NULL;
	gates=NULL;
	arrVi=NULL;
	Partition=NULL;
	obs_feature = new Score*[length_seq];
	for(int i=0;i<length_seq;i++)
		obs_feature[i] = new Score[m_pModel->dim_one_pos];
	_features=NULL;
	buffer2=NULL;
	isbuffered2=NULL;
	isbuffered_output_center=NULL;
	output_center=NULL;
	isbuffered_output_cofactor=NULL;
	output_cofactor=NULL;
	predicted_label=NULL;
	predicted_label8=NULL;
	memset(sum_energy, 0, sizeof(double)*30);
	memset(sum_energy_cd, 0, sizeof(double)*99);
	ss3=new int[length_seq];
	predRg=2.2*pow(length_seq, 0.38);
	betaVsAlpha=0;
}

NN_Sequence::~NN_Sequence(){
	delete forward;
	delete Partition;

	delete dist_pred;
	delete IdxMap; 
	delete ss3;
}

void NN_Sequence::ClearBuffer1(){
	delete rIdxMap;
	delete centerMap;
	delete predicted_label;
	delete predicted_label8;
}

void NN_Sequence::ClearBuffer(){
	//(*isbuffered).Fill(0);
	(*isbuffered2).Fill(0);
	delete arrVi;
	delete gates;
	delete _features;
	delete buffer2;
	delete isbuffered2;

	for(int i=0;i<length_seq;i++)
		delete obs_feature[i];
	delete obs_feature;
	delete IdxMapCenter;
	delete IdxMapCofactor;
	delete isbuffered_output_center;
	delete output_center;
	delete isbuffered_output_cofactor;
	delete output_cofactor;
}
double NN_Sequence::get_backbone_probs(int ai, int aj, int label, int i, int j)
{
	return backbone_probs[(((ai*5+aj)*TOTAL_COND_LABEL+label)*length_seq+i)*length_seq+j];
}
void NN_Sequence::set_backbone_probs(int ai, int aj, int label, int i, int j, double p)
{
	backbone_probs[(((ai*5+aj)*TOTAL_COND_LABEL+label)*length_seq+i)*length_seq+j]=p;
}

int NN_Sequence::MAP(int top, int nMaxContact, int nMaxNonContact){ // Posterior Decoding (Marginal Probability Decoder)
	int nContact=0, nNonContact=0;
	int num_labels=m_pModel->num_labels;
	int* buffer_t=new int[length_contact];
	int* buffer_nt=new int[length_contact];
	memset(pred, 0, sizeof(int)*20);
	for(int t=length_contact-1; t>=0;t--){
		(*forward)(num_labels, t)=log(exp((*forward)(0,t))+exp((*forward)(1,t))+exp((*forward)(2,t))+exp((*forward)(3,t))); // a big state for dist<8, assuming using s12 model
		(*forward)(num_labels+1, t)=log(exp((*forward)(num_labels-2,t))+exp((*forward)(num_labels-1,t)));
		int bestState[10];
		memset(bestState, 0, sizeof(int)*10); 
		Score bestScore[10], bestScoreS8=(*forward)(num_labels, t);
		for (int k=0; k<=top; k++) bestScore[k]=-99999;
		for(int i=0; i< num_labels; i++){
			for (int k=0; k<=top; k++){
				if ((*forward)(i,t)>bestScore[k]){
					for (int l=top; l>=k+1; l--)
						bestScore[l]=bestScore[l-1], bestState[l]=bestState[l-1];
					bestScore[k]=(*forward)(i,t);
					bestState[k]=i;
					break;
				}
			}
		}
		//if (bestScoreS8>=bestScore[0] && bestScoreS8>(*forward)(num_labels+1, t)) // for f_34
		if (bestScoreS8>=bestScore[0]) // f_318, PI_f52
			pred[num_labels]++, predicted_label8[t]=1;
		predicted_label[t]=bestState[0]; 
		if (predicted_label[t]!=0){
			for (int k=0; k<=top; k++)
				if (bestState[k]==0)
					predicted_label[t]=0;
		}

		if (predicted_label[t]==0){
			buffer_t[nContact]=t;
			nContact++;
			//int x=centerMap[t], y=rIdxMap[t];
			//printf(" &&& (%4d,%4d)\t", x, y);
			//for (int i=0; i<m_pModel->num_labels; i++){
			//	printf("\t%6.2f ", exp((*forward)(i,t)-Partition[t]));
			//}
			//printf("\n");
		}
		if (predicted_label[t]==num_labels-1){
			buffer_nt[nNonContact]=t;
			nNonContact++;
		}
		pred[predicted_label[t]]++;
	}
	cerr << " ** ** Found " << nContact << " contacts, need " << nMaxContact << " and " << nNonContact << " noncontacts, needed " << nMaxNonContact << "." << endl;
	cerr << " ** ** Others:"; for (int i=0; i<num_labels-1; i++) cerr << pred[i] << ", "; cerr << pred[num_labels] << endl;
	if (nContact>nMaxContact){
		int* bestPos=new int[nMaxContact];
		memset(bestPos, 0, sizeof(int)*nMaxContact);
		Score* bestScore=new Score[nMaxContact];
		for (int k=0; k<nMaxContact; k++) bestScore[k]=-9999;
		for (int i=0; i<nContact; i++) {
			int t=buffer_t[i];
			//int x=centerMap[t], y=rIdxMap[t]; printf("(%3d,%3d):%4.2f\t", x, y, exp((*forward)(0,t)-Partition[t]));
			int label=predicted_label[t];
			for (int k=0; k<nMaxContact; k++){
				if ((*forward)(label, t)>=bestScore[k]){
					for (int l=nMaxContact-1; l>=k+1;l--)
						bestScore[l]=bestScore[l-1], bestPos[l]=bestPos[l-1];
					bestScore[k]=(*forward)(label,t);
					bestPos[k]=t;
					break;
				}
			}
		}
		//printf("\n");
		for (int i=0; i<nContact; i++)
			//predicted_label[buffer_t[i]]=num_labels-1;
			predicted_label[buffer_t[i]]=1;
		for (int i=0; i<nMaxContact; i++){
			int t=bestPos[i];
			predicted_label[t]=0;
			/*
			   int x=centerMap[t], y=rIdxMap[t]; printf(" &&& (%4d,%4d)\t", x, y);
			   for (int i=0; i<num_labels; i++){ printf("\t%6.2f ", exp((*forward)(i,t)-Partition[t])); }
			   printf("\n");
			// */
		}
		nContact=nMaxContact;
		delete bestPos;
		delete bestScore;
	}

	if (nMaxNonContact>0) {
		int* bestPos=new int[nMaxNonContact];
		memset(bestPos, 0, sizeof(int)*nMaxNonContact);
		Score* bestScore=new Score[nMaxNonContact];
		for (int k=0; k<nMaxNonContact; k++) bestScore[k]=-9999;
		for (int i=0; i<nNonContact; i++) {
			int t=buffer_nt[i];
			//int x=centerMap[t], y=rIdxMap[t]; printf("(%3d,%3d):%4.2f\t", x, y, exp((*forward)(0,t)-Partition[t]));
			int label=predicted_label[t];
			for (int k=0; k<nMaxNonContact; k++){
				if ((*forward)(label, t)>=bestScore[k]){
					for (int l=nMaxNonContact-1; l>=k+1;l--)
						bestScore[l]=bestScore[l-1], bestPos[l]=bestPos[l-1];
					bestScore[k]=(*forward)(label,t);
					bestPos[k]=t;
					break;
				}
			}
		}
		//printf("\n");
		for (int i=0; i<nNonContact; i++)
			predicted_label[buffer_nt[i]]=-1;
		for (int i=0; i<nMaxNonContact; i++){
			predicted_label[bestPos[i]]=num_labels-1;
			//cerr << bestPos[i] << " ";
		}
		nNonContact=nMaxNonContact;
		delete bestPos;
		delete bestScore;

	}

	delete buffer_t;
	delete buffer_nt;
	ClearBuffer();

	return nContact; // number of predictions on label 0;
}

void NN_Sequence::CalcPartition()
{
	if (forward==NULL)
		forward = new ScoreMatrix(m_pModel->num_labels+2,length_contact, "forward");
	if (Partition==NULL) 
		Partition = new Score[length_contact];
	for(int t=0;t<length_contact;t++){
		Partition[t] = (Score)LogScore_ZERO;
		for(int i=0; i< m_pModel->num_labels; i++){
			int state = i;
			Score score = ComputeScore(state,t);
			(*forward)(i,t)=score;
			LogScore_PLUS_EQUALS(Partition[t], score);
			//cerr << "	Partition(" << t << "," << state << ")=" << score << " : " << Partition[t] << endl;
		}
	} //cerr << endl;

	/*
	   cerr << "***************** probability matrix *********************" << endl;
	   int num_labels=8;
	   for (int t=0; t<length_contact; t++){
	   int x=centerMap[t], y=rIdxMap[t];
	   cerr << "PP("<<x<<","<<y<< "): ";
	   for (int j=0; j<num_labels; j++){
	   double p=(*forward)(j,t)-Partition[t];
	   cerr << exp(p)<<" ";
	   }
	   cerr << endl;
	   }
	//exit(1);
	// */
}

Score NN_Sequence::GetGateOutput(int gate, int pos){
	Score output = 0;
	//num_gates2
	for(int j=0;j<m_pModel->num_gates2;j++){
		output+=m_pModel->GetWeightG(gate,j)*GetGateOutput2(j,pos);
	}
	return Gate(output);
}

Score NN_Sequence::GetGateOutput2(int gate2, int pos){
	Score output = 0;
	if((*isbuffered2)(gate2,pos)>0.5) return (*buffer2)(gate2,pos);
	//dim_features
	int dim_features=m_pModel->dim_features;
	int window_size=m_pModel->window_size;
	int center=centerMap[pos];
	int cofactor=rIdxMap[pos];
	if ((*isbuffered_output_center)(gate2, center)>0.5)
		output=(*output_center)(gate2, center);
	else {
		for (int j=0; j<window_size/2; j++){
			Score w=m_pModel->GetWeightG2(gate2,j);
			Score f=getFeatures(pos, j);
			output+=w*f;
			//if (proc_id==0) cerr << j << "," << w << ", " << f << endl;
		}
		for(int j=window_size/2*2;j<dim_features/2+window_size/2-1;j++){
			Score w=m_pModel->GetWeightG2(gate2,j);
			Score f=getFeatures(pos, j);
			output+=w*f;
		}
		(*output_center)(gate2, center)=output;
		(*isbuffered_output_center)(gate2,center)=1;
	}
	if ((*isbuffered_output_cofactor)(gate2, cofactor)>0.5)
		output+=(*output_cofactor)(gate2, cofactor);
	else {
		double o=0;
		for (int j=window_size/2; j<window_size/2*2; j++){
			Score w=m_pModel->GetWeightG2(gate2,j);
			Score f=getFeatures(pos, j);
			o+=w*f;
			//if (proc_id==0) cerr <<  j << "," <<w << ", " << f << endl;
		}
		for(int j=dim_features/2+window_size/2-1;j<dim_features-2;j++){
			Score w=m_pModel->GetWeightG2(gate2,j);
			Score f=getFeatures(pos, j);
			o+=w*f;
		}
		(*output_cofactor)(gate2, cofactor)=o;
		(*isbuffered_output_cofactor)(gate2,cofactor)=1;

		output+=o;
		//if (abs(w*f)>100) cerr << "	GetGate2: w="<< w << ", f=" << f << endl;
	} //cerr << "	GetGate2(" << gate2 << ", " << pos << ")=" << output << endl;
	output+=m_pModel->GetWeightG2(gate2,dim_features-2)*getFeatures(pos, dim_features-2);
	output+=m_pModel->GetWeightG2(gate2,dim_features-1)*getFeatures(pos, dim_features-1);

	(*isbuffered2)(gate2,pos) = 1;
	(*buffer2)(gate2,pos) = Gate(output);
	return (*buffer2)(gate2,pos);
}

void NN_Sequence::ComputeGates()
{
	if (gates==NULL){
		gates = new ScoreMatrix(m_pModel->num_gates,length_contact, "gates");
		output_center = new ScoreMatrix(m_pModel->num_gates2,length_seq, "output_center");
		isbuffered_output_center = new ScoreMatrix(m_pModel->num_gates2,length_seq, "isbuffered_output_center");
		output_cofactor = new ScoreMatrix(m_pModel->num_gates2,length_seq, "output_cofactor");
		isbuffered_output_cofactor = new ScoreMatrix(m_pModel->num_gates2,length_seq, "isbuffered_output_cofactor");
	}
	gates->resize(m_pModel->num_gates, length_contact);
	gates->Fill(LogScore_ZERO);
	output_center->resize(m_pModel->num_gates2, length_seq);
	output_center->Fill(0);
	isbuffered_output_center->resize(m_pModel->num_gates2, length_seq);
	isbuffered_output_center->Fill(0);
	output_cofactor->resize(m_pModel->num_gates2, length_seq);
	output_cofactor->Fill(0);
	isbuffered_output_cofactor->resize(m_pModel->num_gates2, length_seq);
	isbuffered_output_cofactor->Fill(0);
	isbuffered2->Fill(0);
	for (int pos=0;pos<length_contact; pos++) {
		for (int k=0;k<m_pModel->num_gates;k++) 
			(*gates)(k,pos) = GetGateOutput(k,pos);
	}
}

void NN_Sequence::ComputeVi()
{
	if (arrVi==NULL)
		arrVi = new ScoreMatrix(m_pModel->num_labels,length_contact, "arrVi");
	arrVi->resize(m_pModel->num_labels, length_contact);
	arrVi->Fill(LogScore_ZERO);
	int num_gates = m_pModel->num_gates;
	for (int pos=0;pos<length_contact; pos++)
		for (int l=0; l<m_pModel->num_labels; l++){
			int currState = l;
			int weightLStart = m_pModel->iGateStart + currState*num_gates;
			Score score = 0;
			for(int k=0;k<m_pModel->num_gates;k++) {
				Score output = (*gates)(k,pos);
				score += m_pModel->weights[weightLStart++]*output;
			}
			(*arrVi)(currState, pos) = score; //cerr << "	ComputeVi(" << currState << ", " << pos << ")=" << score << endl;
		}
}

Score NN_Sequence::ComputeScore(int currState, int pos){
	//compute num_gates neurons' output
	double vi = (*arrVi)(currState, pos);
	Score score = vi;
	return score;
}

void NN_Sequence::makeFeatures(int contact_DIST){
	//obs_feature: first 3 columns corresponding to C, H and E
	double alphaCnt=0, betaCnt=0;
	for (int i=0; i<length_seq; i++){
		if (obs_feature[i][0]>obs_feature[i][1] && obs_feature[i][0]>obs_feature[i][2])
			ss3[i]=0;
		else if (obs_feature[i][1]>obs_feature[i][0] && obs_feature[i][1]>obs_feature[i][2])
			ss3[i]=1, alphaCnt++;
		else 
			ss3[i]=2, betaCnt++;
	}
	betaVsAlpha=betaCnt/alphaCnt;

	int df = length_contact*2;//m_pModel->dim_features;
	//cerr << "df=" << df << ", len_contact=" << length_contact << ", gaps_size=" << gaps.size();
	_features = new int[df];

	int totalContact=0;
	//cerr << " window_size=" << m_pModel->window_size << ", len_seq=" << length_seq << endl;
	//build features for local windows	
	//for(int t=window_size/2;t<length_seq-window_size/2;t++){
	for(int t=max(0, m_pModel->startRes-1);t<min(m_pModel->endRes, length_seq)-contact_DIST;t++){
		//int x=centerMap[t], y=rIdxMap[t];
		if (m_pModel->residue_flag[t]==0)
			continue;
		//cerr << "	=== " << t << " : ";
		//for(int u=t+8;u<length_seq-window_size/2;u++){
		for(int u=t+contact_DIST;u<min(m_pModel->endRes, length_seq);u++){
			//if (u-t>=12) continue;
			//if (u-t>=24) continue;
			if (m_pModel->residue_flag[u]==0)
				continue;
			//cerr <<u<<", ";
			IdxMap[u*length_seq+t]=IdxMap[t*length_seq+u]=totalContact;
			centerMap[totalContact]=t;
			IdxMapCenter[t]=totalContact;
			rIdxMap[totalContact]=u;
			IdxMapCofactor[u]=totalContact;
			int pivot = totalContact*2;//m_pModel->dim_features;
			totalContact++;
			int offset1 = t-m_pModel->window_size/2;
			int offset2 = u-m_pModel->window_size/2;
			_features[pivot++]=offset1;
			_features[pivot++]=offset2;
		} //cerr << endl;

		if (label_flag==0)
			continue;

		// This is the extra part for the Contact Number
		// For predicting the contact number between residue t and all the residues from t+contact_DIST to u
		for(int u=t-contact_DIST;u>=0;u--){
			if (m_pModel->residue_flag[u]==0)
				continue;
			//cerr <<u<<", ";
			IdxMap[t*length_seq+u]=totalContact;
			centerMap[totalContact]=t;
			IdxMapCenter[t]=totalContact;
			rIdxMap[totalContact]=u;
			IdxMapCofactor[u]=totalContact;
			int pivot = totalContact*2;
			totalContact++;
			int offset1 = t-m_pModel->window_size/2;
			int offset2 = u-m_pModel->window_size/2;
			_features[pivot++]=offset1;
			_features[pivot++]=offset2;
		} //cerr << endl;
	}
	length_contact=totalContact; 
	buffer2 = new ScoreMatrix(m_pModel->num_gates2,length_contact, "buffer2"); //buffer = new ScoreMatrix(num_gates,length_contact);
	isbuffered2 = new ScoreMatrix(m_pModel->num_gates2,length_contact, "isbuffered2"); //isbuffered = new ScoreMatrix(num_gates,length_contact);
	(*isbuffered2).Fill(0); //(*isbuffered).Fill(0);
	predicted_label = new int[length_contact];	
	predicted_label8 = new int[length_contact];	
}

Score NN_Sequence::getFeatures(int pos, int i){
	int dim_features=m_pModel->dim_features;
	int window_size= m_pModel->window_size;
	int pivot=pos*2;
	int offset1 = _features[pivot];
	int offset2 = _features[pivot+1];
	int iif=i-window_size/2*2;
	int idx = iif / m_pModel->dim_one_pos;
	Score ret=1;
	if (i<window_size/2*2){
		offset1+=window_size/2;
		offset2+=window_size/2;
		//if (proc_id==0) cerr << "offset1="<<offset1<<", offset2="<<offset2<<endl;
		if (label_flag==1) { // for contact number prediction, need to check both ends
			if ((offset1<window_size/2 && i==offset1) || (offset2<window_size/2 && i==offset2))
				return 1;
			else if ((length_seq-offset1==window_size/2*2-i && offset1>=length_seq-window_size/2)
					|| (length_seq-offset2==window_size/2*2-i && offset2>=length_seq-window_size/2)){
				//if (proc_id==0) cerr << length_seq << "-" << offset2 << "="<<window_size/2*2<<"-" <<i << endl; 
				return 1;
			} else 
				return 0;
		} else {
			if (offset1<window_size/2 && i==offset1)
				return 1;
			else if (length_seq-offset2==window_size/2*2-i && offset2>=length_seq-window_size/2){
				//if (proc_id==0) cerr << length_seq << "-" << offset2 << "="<<window_size/2*2<<"-" <<i << endl; 
				return 1;
			} else 
				return 0;
		}
	} else if (idx<window_size){
		if (offset1+idx<0 || offset1+idx>=length_seq) return 0; // Secondary Structure
		ret= obs_feature[offset1+idx][iif%m_pModel->dim_one_pos];
	} else if (i<dim_features-4){
		idx-=m_pModel->window_size;
		//cerr << idx << " - " << i << "*"<<m_pModel->dim_features <<" : " << i%m_pModel->dim_one_pos<<" : " << length_seq << " : " << offset1 << "  : " << offset2;
		if (offset2+idx<0 || offset2+idx>=length_seq) return 0; // Secondary Structure
		ret= obs_feature[offset2+idx][iif%m_pModel->dim_one_pos];
		//cerr << " : " << ret << endl;
	} else if (i==dim_features-3)
                ret = predRg/10;
	else if (i==dim_features-2)
		ret = abs(offset2-offset1);
	else if (i==dim_features-1)
		ret = abs(offset2-offset1+0.000001)/length_seq;

	return ret;
}

// func=0: contact prediction for top L/5, 1-top L/10, 2-top 5; 3-energy function for FM, 4-energy function for TB; 5-contact number
void NN_Sequence::PredictDistances(int func) 
{
	int contact_DIST=6;
	contact_DIST= 12;

	cerr << "Making features..." << endl;
	makeFeatures(contact_DIST);
	cerr << "	--------- Got len_contact=" << length_contact << endl;
	ComputeGates();
	ComputeVi();
	CalcPartition();
	int pred_positive=0, top=0, TOP_most=2;
	if (m_pModel->startRes<0)
		m_pModel->startRes=1;
	if (m_pModel->endRes<1)
		m_pModel->endRes=length_seq;
	int totalRes=min(m_pModel->endRes, length_seq) -m_pModel->startRes+1;
	int needed, nonContact=-1;;
	TOP_most=1;
	needed=length_contact;
	while (pred_positive<needed && top<TOP_most)
		pred_positive=MAP(top++, needed, nonContact);

	for(int i=0;i<m_pModel->num_labels;i++) for(int j=0;j<m_pModel->num_labels;j++) { confusion[i][j]=0;}

	for(int t=0; t<length_contact;t++) {
		int x=centerMap[t], y=rIdxMap[t];
		int label=predicted_label[t];
		(*dist_pred)(x,y)=(*dist_pred)(y,x)=label;
	}
	ClearBuffer1();
}
double EPAD::logReferenceState(double r, double Rg, double lastDist)
{
	int idx=(int)(r*10);
	if (log_ref[idx]!=0)
		return log_ref[idx];
	double a=sqrt(5.0/3.0)*Rg, a2=a*a;
	double n_r_a=3*r*r*(r-2*a)*(r-2*a)*(r+4*a)/(16*a2*a2*a2); // p(r|a) when r<14
	if (r>lastDist){ // 1- p(r<lastDist)
		double R=lastDist/a, R3=R*R*R; n_r_a=1-R3*(R3-18*R+32)/32;
	}
	log_ref[idx]=log(n_r_a);
	return log_ref[idx];
}

string arrResidueNames[20]={"ALA", "CYS","ASP","GLU","PHE","GLY","HIS","ILE", "LYS","LEU",
	"MET","ASN", "PRO","GLN","ARG","SER","THR", "VAL","TRP", "TYR"};
double EPAD::CalculateConditionalPairEnergyS12(int i, int j, double dist, double Rg, int func, int ri, int rj, int ai, int aj)
{
	double dist_bak=dist;
	int obs_label=min(TOTAL_COND_LABEL-1, max(0, (int)((dist-2.5)*2)));
	int idx=testData->IdxMap[i*testData->length_seq+j];
	//cerr << "    structure(" << i << "," << j << ")=" << obs_label << " : " << (*testData->dist_pred)(i,j) << "; " << idx << endl;
	if (idx==-1 || i>=testData->length_seq || j>=testData->length_seq) // not a valid pair: the terminal residue are not valid if with window_size/2
		return 0;

	if (model_typ==1 && (testData->ss3[i]==0 || testData->ss3[j]==0)) return 0;

	if (dist>15) dist=15.4;

	double dist_cutoff=14.0;
	if (testData->length_seq>=95)
		dist_cutoff=15.2;
	else if (testData->length_seq>=70)
		dist_cutoff=14.6;
	double energy=0;
	double log_prob=testData->get_backbone_probs(ai,aj,obs_label,i, j);
	if (log_prob>-1000 && dist>2.5){
		double log_rf=logReferenceState(dist, testData->predRg, dist_cutoff); // for f_318, PI_f_52
		energy=-log_prob + log_rf;
		if (energy>forbidden_val)
			energy=forbidden_val;
		if (log_prob<-1000 || log_prob>0)
			fprintf(stderr, "\tEnergy=%f=-log(%f)+%f; %f=get_backbone_probs(%d, %d, %d, %d, %d) : Residues: (%d, %d)\n", energy, exp(log_prob), log_rf, log_prob, ai,aj,obs_label,i, j, ri, rj);
	}
	if (dist_bak>=16){ // For folding only
/*
		if (energy>=0) return -0.002;
		else if (dist_bak<18) return energy/10;
		else return energy/20;
*/
		if (energy>=0) {
			if (dist_bak<18) return energy*1.02;
			else if (dist_bak<22) return energy*1.05;
			else return energy*1.1;
		} else {
			if (dist_bak<18) return energy/10;
			else if (dist_bak<25) return energy/20;
			else return 0.01;
		}
	}
	double wPoorLabel=(testData->pred[0]+testData->pred[1]+testData->pred[2]+testData->pred[3]+testData->pred[4]+testData->pred[7]+testData->pred[8]+testData->pred[9]+testData->pred[10])*100.0/testData->length_seq;
	double wPoorLabel0=testData->pred[num_labels]*100.0/testData->length_seq;
	double wPoorLabel5=(testData->pred[5]+testData->pred[6]+testData->pred[7])*100.0/testData->length_contact;
	double wPoorLabel8=(testData->pred[8]+testData->pred[9]+testData->pred[10])*100.0/testData->length_seq;
	//if (obs_label>=16 && obs_label<=24 || obs_label==5 || obs_label==10 ||obs_label==9 ||obs_label==11 || obs_label==4 || obs_label==25&&wPoorLabel==0&&testData->length_seq<110) //for PI_f_52
	//	energy=0;
	//if (obs_label>=17 && obs_label<=22 || obs_label>=8 && obs_label<=11 || obs_label==4) //for PI_f_52
	//if (testData->length_seq>145 && testData->length_seq<350){
	if (testData->length_seq>400){
		testData->sum_energy[(int)((dist-2.5)*2)]+=energy;
		return energy;
	}
	//if (obs_label>=23&&obs_label<=24&&testData->length_seq>=141&&testData->length_seq<280) energy=0;
	//else 
	    if (obs_label>=20&&obs_label<=22 || obs_label>=8&&obs_label<=10 || obs_label>=0&&obs_label<=4 || obs_label==25&&wPoorLabel==0&&testData->length_seq<=75) //for PI_f_52
		if (func!=10) // ScoringByDopeProfileNN::FUNC_CONTACT_DISTRIBUTION_ENERGY
			energy=0; // Optimizing correlation
	testData->sum_energy[(int)((dist-2.5)*2)]+=energy;
	testData->sum_energy_cd[min(95, abs(i-j))]+=energy;

	return energy;
}

double EPAD::CalculatePairEnergyS12(int i, int j, double dist, double Rg, int func, int ri, int rj)
{
	double dist_bak=dist;
	int cd=abs(i-j);
	if (testData->length_seq>65 && cd<12)
		return 0;

	if (model_typ==1 && (testData->ss3[i]==0 || testData->ss3[j]==0)) return 0;

	int obs_label;
	int label = (int)((dist-3)*1);
	int label1 = (int)((dist-4)*1); // 12 states model
	if (label1>0 && label<12)
		obs_label=label1;
	else if (label>=0 && label<12)
		obs_label=0, gold_positive++;
	else 
		obs_label=num_labels-1;
	int idx=testData->IdxMap[i*testData->length_seq+j];
	//cerr << "    structure(" << i << "," << j << ")=" << obs_label << " : " << (*testData->dist_pred)(i,j) << "; " << idx << endl;
	if (idx==-1 || i>=testData->length_seq || j>=testData->length_seq) // not a valid pair: the terminal residue are not valid if with window_size/2
		return 0;
//* FUNC_CONTACT_PAIRWISE_ENERGY=9
	if (func!=9 && (*testData->backbone_prob_flags)(i,j)<0.5 && cond_weight>0){
		//cerr.setf(ios::fixed,ios::floatfield);
		//cerr.precision(4);
		double sum[5][5];
		//for(int dj=0; dj<num_labels; dj++){
		for(int dj=0; dj<TOTAL_COND_LABEL; dj++){
			memset(sum, 0, sizeof(double)*5*5);
			int st=0, end=5;
			for(int di=0; di<num_labels; di++){
				double prob=exp((*testData->forward)(di,idx)-testData->Partition[idx]);
				for (int a1=st; a1<end; a1++)
					for (int a2=st; a2<end; a2++)
						sum[a1][a2]+=prob*prob_cond[ri][a1][rj][a2][di][dj];
			}
			for (int a1=st; a1<end; a1++)
				for (int a2=st; a2<end; a2++){
					double log_prob=-10000;
					if (sum[a1][a2]>0)
						log_prob=log(sum[a1][a2]);
					testData->set_backbone_probs(a1,a2,dj,i, j,log_prob);

					bool DEBUG=true;
					if (DEBUG)
						continue;
					cerr << "    cond_prob " << dj <<" ::<" << ri << "," << a1 <<";" << rj << "," << a2 << ">,<"<< i << "," << j <<">:: "<< sum[a1][a2] << "=" ;
					if (sum[a1][a2]>0)
						for(int di=0; di<num_labels; di++){
							double prob=exp((*testData->forward)(di,idx)-testData->Partition[idx]);
							cerr << prob<<"*"<<prob_cond[ri][a1][rj][a2][di][dj] << "+";
						}
					cerr << endl;
					log_prob=testData->get_backbone_probs(a1,a2,dj,i, j);
					fprintf(stderr, "***%f=get_backbone_probs(%d, %d, %d, %d, %d)\n", exp(log_prob), a1,a2,dj,i, j);
				}
			//cerr << testData->get_backbone_probs(2,2,dj,i, j) << " ";
		} //cerr << endl; exit(0);
		(*testData->backbone_prob_flags)(i,j)=(*testData->backbone_prob_flags)(j, i)=1;
	}
	//return 0;
// */
	int pred_label=(int)(*testData->dist_pred)(i,j);
	testData->confusion[pred_label][obs_label]++;
	switch (func)
	{
	case 3: // FUNC_CONTACT_ENERGY_FM
		if (dist>14) dist=max(14.5, 1.28*testData->predRg); // This is for ab initio ranking
		if (dist<4) dist=3;
		break;
	case 4: // FUNC_CONTACT_ENERGY_TB
	default:
		//if (dist>14) dist=14.5; // This is for template-base ranking f_34
		if (dist>15) dist=15.4; // This is for template-base ranking f_318
		//else if (dist<8) dist=5; // This is for template-base ranking
		else if (dist<4) dist=4; // This is for template-base ranking
		else dist=(int) dist;
		break;
	}

	double dist_cutoff=14.2;
	//double dist_cutoff=12.0+0.1*(testData->length_seq-40); // For sequence length <70
	if (testData->length_seq>=120)
		dist_cutoff=14.8;
	else if (testData->length_seq>=90)
		dist_cutoff=15.2;
	else if (testData->length_seq>=70)
		dist_cutoff=14.4;
	double log_rf=logReferenceState(dist, testData->predRg, dist_cutoff); // for f_318, PI_f_52

	if (cd>=6 && cd<=21){ // from 19
		if (testData->length_seq<=65)
			log_rf=logReferenceStateNearCD[cd-6][obs_label];
		else if (testData->length_seq<120)
			log_rf=logReferenceStateNearCD1[cd-6][obs_label];
		else if (testData->length_seq<200)
			log_rf=logReferenceStateNearCD2[cd-6][obs_label];
		else 
			log_rf=logReferenceStateNearCD3[cd-6][obs_label];
	}

	double energy, Partition=testData->Partition[idx], forward=(*testData->forward)(obs_label,idx);
	energy=-forward + Partition + log_rf; //if (dist<8 && energy>0) energy=0;
	if (energy>forbidden_val)
		energy=forbidden_val;
	if (dist_bak>=16){ // For folding only
		if (energy>=0) {
			if (dist_bak<18) return energy*1.02;
			else if (dist_bak<21) return energy*1.04;
			else if (dist_bak<testData->predRg*sqrt(5/3)*2) return energy*1.06; // 1.1
			else return energy*1.1;
		} else {
			if (dist_bak<18) return energy/10;
			else if (dist_bak<21) return energy/20;
			else if (testData->betaVsAlpha<2.5){ // for significently alpha proteins, limit more on the size
			//else if (testData->betaVsAlpha<2.5){ // for small proteins or significently alpha proteins, limit more on the size
				if (dist_bak<testData->predRg*sqrt(5/3)*2) return 0.2; // 0.04->0.1->0.2
				else if (testData->length_seq<70) return 0.9; //0.15->0.5->0.9; small mainly alpha proteins 
				else return 0.7;
			} else { // Beta should be looser than alpha
				if (dist_bak<testData->predRg*sqrt(5/3)*2) return 0.1;
				else return 1.1; //0.15->0.5->0.4
			}
		}
	}
/* 
	if (testData->length_seq>145 && testData->length_seq<300 || testData->length_seq>400){
		testData->sum_energy[(int)dist]+=energy;
		return energy;
	}

	double wPoorLabel=(testData->pred[0]+testData->pred[1]+testData->pred[2]+testData->pred[3]+testData->pred[4]+testData->pred[7]+testData->pred[8]+testData->pred[9]+testData->pred[10])*100.0/testData->length_seq;
	double wPoorLabel0=testData->pred[num_labels]*100.0/testData->length_seq;
	double wPoorLabel5=(testData->pred[5]+testData->pred[6]+testData->pred[7])*100.0/testData->length_contact;
	double wPoorLabel8=(testData->pred[8]+testData->pred[9]+testData->pred[10])*100.0/testData->length_seq;
	switch (obs_label)
	{
	case 0:
	case 3:
	case 4:
		energy=0; //for PI_f_52
		break;
	case 2:
		break;
	case 10:
		if (testData->length_seq<60) energy=0;
		break;
	case 5:
	case 6:
	case 7:
		if (wPoorLabel5<0.06) energy=0; //for PI_f_52
		break;
	case 8:
	case 9:
		if (testData->length_seq<60) energy=0;
		if (wPoorLabel8<0.01) energy=0; //for PI_f_52
		break; // f_318, PI_f_52
	case 11: //num_labels-1:
		//if (wPoorLabel0<5) energy=-energy; // best for f_34
		//if (wPoorLabel0<6) energy=0;  // for f_318
		if ((wPoorLabel<1 || wPoorLabel<1) && testData->length_seq <110) energy=-energy;  // for PI_f_52
		break;
	}
// */
	testData->sum_energy[(int)dist]+=energy;

	return energy;
}

void EPAD::reset(int s_Res, int e_Res, vector<int>& r_flag)
{
	positive=gold_positive=0;
	for(int i=0;i<num_labels;i++) for(int j=0;j<num_labels;j++) { testData->confusion[i][j]=0;}
	
	//cerr << "EPAD::reset( " << s_Res << ", " << e_Res << "); r_flag=" << (int)r_flag.size() << endl;
	if (s_Res>-1 && e_Res>-1){
		startRes=s_Res;
		endRes=e_Res;
		int tot=min((int)residue_flag.size(), (int)r_flag.size());
		for (int i=0; i<tot; i++){
			residue_flag[i]=r_flag[i];
			//cerr << i << ":" << residue_flag[i] << ", ";
		}
		for (int i=tot; i<(int)residue_flag.size(); i++)
			residue_flag[i]=0;
	}
}

void EPAD::printConfusionMatrix(double Rg)
{
	FILE* fp=stderr;
/*
	fprintf(fp, "\nConfusion Matrix: min(%d,%d)-%d+1=%d\n", endRes, testData->length_seq, startRes, min(endRes, testData->length_seq) -startRes+1);
	int* gold=new int[num_labels+1];
	memset(gold, 0, sizeof(int)*(num_labels+1));
	for(int i=0;i<num_labels;i++){
		int pred=0;
		if (i==0) fprintf(fp, "#");
		else if (i==num_labels-1)
			fprintf(fp, "~");
		else fprintf(fp, " ");
		for(int j=0;j<num_labels;j++){
			fprintf(fp, "%6d ", testData->confusion[i][j]);
			pred +=testData->confusion[i][j];
			gold[j]+=testData->confusion[i][j];
		}
		fprintf(fp, "| %6d %6.2f\n", pred, testData->confusion[i][i]*100.0/pred);
	}
	fprintf(fp, "-----------------------------------------------------------------------------------\n^");
	for (int i=0; i<num_labels; i++)
		fprintf(fp, "%6d ", gold[i]);
	fprintf(fp, "\n@");
	for (int i=0; i<num_labels; i++)
		fprintf(fp, "%6.2f ", testData->confusion[i][i]*100.0/gold[i]);
	delete gold;
	fprintf(fp, "\nEC");
	for (int i=6; i<95; i++) fprintf(fp, "%6.2f ", testData->sum_energy_cd[i]);
	fprintf(fp, "\n");
// */
	fprintf(fp, "\nEE");
	for (int i=0; i<28; i++) fprintf(fp, "%6.2f ", testData->sum_energy[i]);
	fprintf(fp, "R%6.2f\n", Rg);
	memset(testData->sum_energy, 0, sizeof(double)*30);
	memset(testData->sum_energy_cd, 0, sizeof(double)*99);
}

int EPAD::ReadConfigFile(string cfgName)
{
	ifstream CfgFile;
	CfgFile.open(cfgName.c_str(), ios::in | ios::binary);
	string line, par, val;

	while (!CfgFile.eof()){
		getline(CfgFile, line); trim2(line);
		if ((line != "") && (line[0] != '#')) {
			strtokenizer strtokens(line,"=");
			int num=strtokens.count_tokens();
			if (num<2) continue;
			par = strtokens.token(0);
			val = strtokens.token(1);
			trim2(par);
			trim2(val); 
			//cerr << par << "=" << val << endl;

			if (par == "Parameter file") { model_file = val; } 
			if (par == "CONDITIONAL_BACKBONE_WEIGHT") { cond_weight = atof(val.c_str()); }
			if (par == "Forbidden Value") { forbidden_val = atof(val.c_str()); }
			if (par == "config_param") { config = val; }
			if (par == "MODEL_TYPE") { if (val=="TEMPLATED_BASED") model_typ=1; }
		}
	}
	CfgFile.close();
	cerr << "EPAD config: model_typ=" << model_typ << "; Forbidden_Value=" << forbidden_val << endl;
	return 0;
}

void EPAD::Initialize(string config_file, string input_f)
{
	double refNearCD[]={
		0.00499,0.02355,0.02355,0.01634,0.03617,0.31484,0.18850,0.06153,0.04827,0.04105,0.03745,0.20378,
		0.00769,0.01668,0.01484,0.01570,0.03065,0.04766,0.38579,0.07744,0.05545,0.06098,0.04928,0.23784,
		0.00376,0.01814,0.01693,0.01084,0.02390,0.04127,0.05443,0.08120,0.34561,0.06793,0.05675,0.27923,
		0.00554,0.01707,0.01165,0.01210,0.02736,0.03381,0.04116,0.04545,0.05156,0.07519,0.35369,0.32542,
		0.00532,0.01653,0.01676,0.01260,0.02092,0.02347,0.04000,0.03780,0.04624,0.05249,0.07769,0.65017,
		0.00639,0.01454,0.01407,0.01419,0.02128,0.02921,0.03417,0.03003,0.04079,0.05427,0.05948,0.68157,
		0.00484,0.01500,0.01513,0.01029,0.01936,0.02626,0.04090,0.03509,0.03957,0.04925,0.05179,0.69252,
		0.00434,0.01363,0.01363,0.00979,0.02404,0.03024,0.03135,0.03445,0.04263,0.05044,0.04759,0.69786,
		0.00571,0.01168,0.01257,0.00978,0.02540,0.02984,0.03581,0.03505,0.04063,0.04800,0.04825,0.69727,
		0.00521,0.01068,0.01276,0.01159,0.02174,0.03073,0.04180,0.04310,0.03776,0.04154,0.04987,0.69323,
		0.00334,0.01162,0.01069,0.01269,0.02191,0.03433,0.03967,0.03794,0.04502,0.05130,0.04849,0.68301,
		0.00288,0.01070,0.00960,0.01234,0.02139,0.03758,0.03854,0.04292,0.04416,0.05732,0.05252,0.67005,
		0.00225,0.01042,0.01000,0.01282,0.02268,0.03691,0.04367,0.04325,0.04691,0.04832,0.05522,0.66756,
		0.00376,0.00898,0.00912,0.01390,0.02201,0.03823,0.04387,0.04822,0.04561,0.05227,0.04692,0.66710,
		0.00298,0.01043,0.00953,0.01177,0.02443,0.03843,0.04305,0.04677,0.04737,0.05333,0.05199,0.65991,
		0.00291,0.00951,0.00935,0.01119,0.02408,0.03788,0.04846,0.04539,0.05168,0.05199,0.05398,0.65358,
		0.00237,0.00775,0.01091,0.01201,0.02213,0.04189,0.04647,0.04932,0.04900,0.05280,0.05248,0.65286,
		0.00212,0.00832,0.01044,0.01044,0.02283,0.04061,0.04551,0.04681,0.05350,0.05040,0.05921,0.64981,
		0.00303,0.00893,0.00893,0.01027,0.02223,0.03605,0.04144,0.05373,0.04716,0.05727,0.05626,0.65471,
		0.00348,0.00853,0.00801,0.01289,0.02316,0.02734,0.04249,0.05102,0.04858,0.05972,0.06077,0.65401,
		0.00306,0.00901,0.00919,0.01153,0.02108,0.03243,0.04053,0.04918,0.05044,0.06377,0.05314,0.65664,
		0.00261,0.00579,0.00989,0.01624,0.02464,0.02968,0.04256,0.04405,0.05544,0.06011,0.06104,0.64794,
		0.00213,0.00755,0.00871,0.01317,0.02091,0.03854,0.04764,0.04376,0.04764,0.06410,0.06081,0.64504,
		0.00342,0.00664,0.01046,0.01106,0.02293,0.03641,0.04144,0.05592,0.05552,0.05492,0.05532,0.64595,
		0.00272,0.00691,0.01151,0.00963,0.02428,0.04144,0.04584,0.04416,0.05023,0.05860,0.06090,0.64378};

	double refNearCD1[]={
		0.00711,0.02248,0.02544,0.02067,0.04002,0.20959,0.16730,0.07044,0.05493,0.04762,0.04334,0.29105,
		0.00587,0.01798,0.01762,0.01778,0.03333,0.05286,0.25378,0.08435,0.06073,0.07127,0.05107,0.33337,
		0.00295,0.01920,0.01737,0.01355,0.02625,0.04596,0.06142,0.07783,0.21487,0.07513,0.06376,0.38170,
		0.00656,0.01404,0.01404,0.01433,0.02930,0.03437,0.04592,0.05066,0.06118,0.07511,0.21803,0.43647,
		0.00657,0.01426,0.01607,0.01234,0.02325,0.02917,0.04269,0.04343,0.05002,0.05952,0.07602,0.62665,
		0.00529,0.01531,0.01565,0.01311,0.02279,0.02704,0.03648,0.03948,0.04459,0.05636,0.06656,0.65734,
		0.00425,0.01609,0.01569,0.01142,0.01959,0.02593,0.03761,0.03955,0.04515,0.05332,0.05877,0.67262,
		0.00503,0.01380,0.01349,0.01222,0.02108,0.02702,0.03600,0.03754,0.04368,0.05320,0.05435,0.68259,
		0.00596,0.01202,0.01276,0.01186,0.02144,0.02676,0.03667,0.03974,0.04314,0.04808,0.05162,0.68995,
		0.00468,0.01296,0.01281,0.01144,0.02060,0.02782,0.03625,0.04022,0.04317,0.04819,0.05174,0.69014,
		0.00394,0.01257,0.01256,0.01216,0.02016,0.02873,0.03603,0.03951,0.04307,0.04991,0.05049,0.69088,
		0.00375,0.01199,0.01191,0.01157,0.02062,0.02896,0.03773,0.03953,0.04480,0.04902,0.05146,0.68866,
		0.00441,0.01058,0.01127,0.01135,0.02069,0.03064,0.03883,0.03974,0.04469,0.04906,0.05363,0.68511,
		0.00366,0.01064,0.01106,0.01071,0.01986,0.03121,0.04033,0.04034,0.04686,0.05137,0.05241,0.68153,
		0.00332,0.00986,0.01017,0.01045,0.01997,0.03273,0.03960,0.04432,0.04671,0.05138,0.05529,0.67620,
		0.00347,0.00943,0.00978,0.01059,0.01934,0.03366,0.04027,0.04477,0.04779,0.05311,0.05578,0.67199,
		0.00339,0.00853,0.00950,0.01032,0.02020,0.03516,0.04036,0.04463,0.04832,0.05359,0.05547,0.67053,
		0.00281,0.00818,0.00942,0.01103,0.01915,0.03524,0.04088,0.04464,0.04906,0.05677,0.05768,0.66514,
		0.00309,0.00754,0.00865,0.01050,0.01864,0.03585,0.04272,0.04515,0.04997,0.05514,0.05853,0.66422,
		0.00310,0.00748,0.00936,0.00970,0.01853,0.03391,0.04238,0.04596,0.05068,0.05920,0.05856,0.66115,
		0.00288,0.00722,0.00834,0.01047,0.01901,0.03497,0.04101,0.04649,0.05139,0.05718,0.05981,0.66123,
		0.00247,0.00704,0.00865,0.01008,0.01831,0.03457,0.04346,0.04734,0.04968,0.05761,0.06074,0.66003,
		0.00277,0.00659,0.00788,0.01089,0.01920,0.03502,0.04189,0.04526,0.05096,0.05739,0.06143,0.66073,
		0.00301,0.00663,0.00850,0.01051,0.01862,0.03370,0.04413,0.04437,0.04824,0.05759,0.06243,0.66228,
		0.00347,0.00688,0.00820,0.01035,0.01892,0.03389,0.04022,0.04526,0.05029,0.05809,0.06024,0.66418};

	double refNearCD2[]={ 
		0.00718,0.02026,0.02358,0.02225,0.04024,0.19309,0.16030,0.07131,0.05858,0.05067,0.04666,0.30587,
		0.00518,0.01702,0.01670,0.01748,0.03181,0.05171,0.23221,0.08312,0.06338,0.07142,0.05269,0.35729,
		0.00263,0.01721,0.01613,0.01371,0.02555,0.04360,0.05696,0.07487,0.19757,0.07570,0.06622,0.40984,
		0.00547,0.01320,0.01258,0.01322,0.02665,0.03338,0.04468,0.04919,0.05720,0.07218,0.20002,0.47225,
		0.00529,0.01265,0.01410,0.01177,0.02207,0.02742,0.03920,0.04269,0.04762,0.05758,0.07289,0.64672,
		0.00463,0.01309,0.01287,0.01256,0.02089,0.02562,0.03437,0.03682,0.04183,0.05335,0.06253,0.68146,
		0.00349,0.01402,0.01337,0.01118,0.01851,0.02394,0.03335,0.03592,0.04133,0.04984,0.05528,0.69977,
		0.00470,0.01204,0.01154,0.01083,0.01872,0.02381,0.03275,0.03519,0.04055,0.04754,0.05069,0.71162,
		0.00531,0.01076,0.01159,0.00962,0.01854,0.02313,0.03301,0.03613,0.03895,0.04474,0.04754,0.72068,
		0.00409,0.01127,0.01149,0.01029,0.01825,0.02392,0.03198,0.03470,0.03802,0.04425,0.04762,0.72411,
		0.00321,0.01146,0.01142,0.01088,0.01766,0.02434,0.03228,0.03461,0.03726,0.04383,0.04663,0.72642,
		0.00361,0.01074,0.01080,0.01022,0.01825,0.02482,0.03277,0.03469,0.03797,0.04376,0.04668,0.72569,
		0.00452,0.00998,0.01028,0.01035,0.01788,0.02516,0.03289,0.03590,0.03837,0.04341,0.04695,0.72429,
		0.00371,0.01063,0.01068,0.01068,0.01768,0.02580,0.03313,0.03593,0.03858,0.04401,0.04689,0.72227,
		0.00427,0.00987,0.01019,0.01037,0.01815,0.02709,0.03385,0.03604,0.03958,0.04464,0.04725,0.71870,
		0.00364,0.00942,0.01062,0.01018,0.01728,0.02851,0.03566,0.03627,0.04022,0.04511,0.04752,0.71559,
		0.00343,0.00894,0.00976,0.01074,0.01892,0.02769,0.03553,0.03770,0.04074,0.04668,0.04913,0.71075,
		0.00358,0.00853,0.00987,0.01007,0.01798,0.02838,0.03705,0.03914,0.04170,0.04705,0.04956,0.70709,
		0.00332,0.00820,0.00888,0.01058,0.01849,0.03001,0.03649,0.03977,0.04219,0.04829,0.05060,0.70320,
		0.00289,0.00783,0.00979,0.01021,0.01799,0.03004,0.03788,0.04000,0.04256,0.04993,0.05024,0.70063,
		0.00348,0.00746,0.00867,0.01031,0.01802,0.03143,0.03839,0.04066,0.04259,0.04876,0.05156,0.69867,
		0.00278,0.00761,0.00950,0.01017,0.01787,0.03077,0.03809,0.04077,0.04429,0.05100,0.05135,0.69579,
		0.00315,0.00738,0.00842,0.01011,0.01864,0.03103,0.03759,0.04159,0.04503,0.05011,0.05244,0.69451,
		0.00303,0.00706,0.00888,0.01000,0.01793,0.03105,0.03910,0.04113,0.04386,0.04989,0.05305,0.69501,
		0.00274,0.00693,0.00852,0.01040,0.01805,0.03061,0.03887,0.04147,0.04376,0.04940,0.05205,0.69720};
 
	double refNearCD3[]={ 
		0.00638,0.01874,0.02320,0.02443,0.04513,0.19282,0.17244,0.07790,0.06444,0.05568,0.04884,0.27002,
		0.00472,0.01403,0.01592,0.01908,0.03296,0.05702,0.23346,0.09156,0.06874,0.07613,0.05707,0.32931,
		0.00269,0.01330,0.01411,0.01410,0.02740,0.04686,0.06193,0.08165,0.19516,0.08230,0.07053,0.38997,
		0.00419,0.01020,0.01121,0.01403,0.02617,0.03438,0.04790,0.05413,0.06108,0.07800,0.19726,0.46144,
		0.00406,0.00993,0.01209,0.01208,0.02118,0.02893,0.04022,0.04512,0.04974,0.06116,0.07736,0.63814,
		0.00354,0.00978,0.01105,0.01224,0.02057,0.02648,0.03420,0.03768,0.04478,0.05483,0.06542,0.67942,
		0.00268,0.01018,0.01083,0.01089,0.01747,0.02484,0.03335,0.03708,0.04281,0.05052,0.05704,0.70230,
		0.00321,0.00911,0.00949,0.01014,0.01731,0.02352,0.03225,0.03597,0.04136,0.04854,0.05200,0.71710,
		0.00346,0.00823,0.00915,0.00960,0.01648,0.02304,0.03162,0.03607,0.04099,0.04568,0.04877,0.72691,
		0.00314,0.00827,0.00879,0.00907,0.01640,0.02329,0.03158,0.03534,0.03943,0.04511,0.04749,0.73207,
		0.00286,0.00831,0.00901,0.00975,0.01645,0.02280,0.03057,0.03407,0.03877,0.04468,0.04740,0.73532,
		0.00294,0.00817,0.00895,0.00963,0.01647,0.02383,0.03131,0.03403,0.03817,0.04350,0.04732,0.73569,
		0.00351,0.00781,0.00857,0.00974,0.01679,0.02497,0.03169,0.03458,0.03880,0.04411,0.04681,0.73261,
		0.00302,0.00802,0.00935,0.00993,0.01684,0.02574,0.03276,0.03542,0.03980,0.04418,0.04684,0.72809,
		0.00325,0.00810,0.00918,0.01031,0.01787,0.02626,0.03417,0.03648,0.03974,0.04435,0.04629,0.72401,
		0.00344,0.00832,0.00978,0.01061,0.01835,0.02702,0.03467,0.03709,0.04029,0.04429,0.04704,0.71910,
		0.00392,0.00878,0.01028,0.01105,0.01869,0.02771,0.03501,0.03696,0.04022,0.04522,0.04682,0.71535,
		0.00418,0.00940,0.01095,0.01126,0.01875,0.02814,0.03520,0.03710,0.04047,0.04512,0.04709,0.71237,
		0.00452,0.00956,0.01084,0.01163,0.01931,0.02808,0.03563,0.03751,0.04106,0.04534,0.04696,0.70955,
		0.00408,0.00929,0.01123,0.01188,0.02009,0.02851,0.03572,0.03854,0.04110,0.04497,0.04656,0.70803,
		0.00423,0.00885,0.01081,0.01215,0.02026,0.02871,0.03627,0.03808,0.04117,0.04492,0.04662,0.70793,
		0.00395,0.00876,0.01088,0.01187,0.01977,0.02846,0.03600,0.03866,0.04058,0.04487,0.04736,0.70885,
		0.00388,0.00842,0.01044,0.01179,0.01960,0.02796,0.03554,0.03784,0.04072,0.04517,0.04732,0.71131,
		0.00380,0.00802,0.01011,0.01143,0.01893,0.02725,0.03487,0.03807,0.04032,0.04449,0.04690,0.71582,
		0.00342,0.00752,0.00966,0.01104,0.01843,0.02654,0.03420,0.03643,0.04018,0.04529,0.04718,0.72011};

	logReferenceStateNearCD.resize(30-6+1, 12);
	logReferenceStateNearCD1.resize(30-6+1, 12);
	logReferenceStateNearCD2.resize(30-6+1, 12);
	logReferenceStateNearCD3.resize(30-6+1, 12);
	for (int i=6; i<=30; i++){
		for (int j=0; j<12; j++){
			logReferenceStateNearCD(i-6,j)=log(refNearCD[(i-6)*12+j]);
			logReferenceStateNearCD1(i-6,j)=log(refNearCD1[(i-6)*12+j]);
			logReferenceStateNearCD2(i-6,j)=log(refNearCD2[(i-6)*12+j]);
			logReferenceStateNearCD3(i-6,j)=log(refNearCD3[(i-6)*12+j]);
			//cerr << logReferenceStateNearCD(i-6,j) << ":" << logReferenceStateNearCD1(i-6,j) << " ";
		} 
		//cerr << endl;
	}   

	ReadConfigFile(config_file);

	positive_energy_cutoff=1.2;
	dope_weight=1;
	for (int i=0; i<30; i++)
		refScale[i]=1.0;
	istringstream si(config.c_str());
	si >> label_flag >> refScale[4] >> refScale[5] >> refScale[6] >> dope_weight >> positive_energy_cutoff;
	//cerr << "Params: " << config << endl;
	//cerr << "Params: " << label_flag << " " << refScale[4] <<" " << refScale[5] <<" " << refScale[6] <<" " << dope_weight <<" " << positive_energy_cutoff << endl;
	
	ifstream fin(model_file.c_str()); // no file header
	char buf[10240];
	string tmp;
	int data;
	for (int i=0; i<8; i++) {
		if (!fin.getline(buf,10240))
			break;
		else {
			istringstream si(buf);
			si >> tmp >> data;
			if (tmp=="num_params:")
				num_params = data;
			else if (tmp=="num_gates:")
				num_gates = data;
			else if (tmp=="num_gates2:")
				num_gates2 = data;
			else if (tmp=="window_size:")
				window_size = data;
			else if (tmp=="dim_features:")
				dim_features = data;
			else if (tmp=="num_labels:")
				num_labels = data;
			else if (tmp=="dim_one_pos:")
				dim_one_pos = data;
			cerr << "\t" << tmp << "::::"<<data<<endl;
		}
	}
	iGateStart = 0;
	iGate2Start = iGateStart+num_labels*num_gates;
	iFeatureStart = iGate2Start+num_gates*num_gates2;
	num_params = iFeatureStart + dim_features*num_gates2;
	weights = new double[num_params];
	for(int i=0;i<num_params;i++)
		fin >> weights[i];
	fin.close();

	positive=gold_positive=0;

	cerr <<"num_params = " << num_params << endl;
	LoadData(input_f);

	startRes=1;
	endRes=testData->length_seq;
	residue_flag.clear();
	for (int i=0; i<endRes; i++)
		residue_flag.push_back(1);

//*
	double prob;
	ifstream fcond("config/cond_all_atomCa1");
	memset( prob_cond, 0, sizeof(double)*20*5*20*5*TOTAL_LABEL*TOTAL_COND_LABEL);
	while (fcond.getline(buf,10240)){
		istringstream si(buf);
		int ri, rj, ati, atj;
		si >> ri >> ati >> rj >> atj;
		for (int r=0; r<TOTAL_LABEL; r++){
			for (int r2=0; r2<TOTAL_COND_LABEL; r2++){
				si >> prob;
				prob_cond[rj][atj][ri][ati][r][r2]=prob/100;
				prob_cond[ri][ati][rj][atj][r][r2]=prob/100;
				//cerr << prob_cond[ri][ati][rj][atj][r][r2] << " ";
			}
		}
		//cerr << endl;
	}
	fcond.close();
// */
	cerr << "   " << testData <<" num_params="<<num_params<<"; nnz_param="<<nnz_param<<"; num_labels="<<num_labels<<"; dim_features=" <<dim_features<<endl;
}

void EPAD::LoadData(string input)
{
	ifstream trn_in(input.c_str());
	cerr << " gateStart="<<iGateStart<<"; ifs="<<iFeatureStart << " - Load Data: "<< dim_one_pos << endl;

	int bypass=0;
	int length_s, len_c;
	double tmp;
	string stmp;
	// construct a new sequence
	trn_in >> length_s >> len_c;
	getline(trn_in, stmp);
	cerr << " *** " << input << ":"<<length_s << ", " << stmp <<endl;
	
	NN_Sequence *seq = new NN_Sequence(length_s, this, stmp, label_flag);
	for(int j=0;j<length_s;j++){
		for(int k=0;k<bypass;k++)
			trn_in >> tmp;
		for(int k=0;k<dim_one_pos-1;k++){
			trn_in >> tmp;
			seq->obs_feature[j][k] = tmp;
			//cerr << seq->obs_feature[j][k]<<" ";
		}
		//cerr << endl;
		seq->obs_feature[j][dim_one_pos-1] = 1;
	}
	
        //obs_feature: first 3 columns corresponding to C, H and E; 4th column is the NEFF value for each position
	NEFFs=new double[length_s];
	NEFF=0;
	for (int i=0; i<length_s; i++){
		NEFFs[i]=seq->obs_feature[i][3];
		//cerr << "\tNEFFs[" << i << "]=" << NEFFs[i] << endl;
		NEFF+=NEFFs[i];
	}
	NEFF=NEFF/length_s+1; 
	cerr << "NEFF=" << NEFF << endl;

	testData=seq;
	trn_in.close();

	nnz_param=num_labels*num_gates + num_gates*num_gates2 + dim_features*num_gates2;
	int ltest=testData->length_seq;
	cerr << "Data: num_data="<< ltest << "; num_gates="<<num_gates<<"; num_gates2="<< num_gates2<<endl;
	//cerr << " num_params="<<num_params<<"; nnz_param="<<nnz_param<<"; num_labels="<<num_labels<<"; dim_features=" <<dim_features<<endl;
}
