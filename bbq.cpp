#include "bbq.h"
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "strtokenizer.h"

using namespace std;

double BBQ::innerProduct(ATOM a, ATOM b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

ATOM BBQ::crossProduct(ATOM a, ATOM b)
{
    ATOM c;
    c.x = a.y*b.z-a.z*b.y;
    c.y = a.z*b.x-a.x*b.z;
    c.z = a.x*b.y-a.y*b.x;
    return c;
}

void BBQ::normalize(ATOM &a)
{
    double r = sqrt(innerProduct(a,a));
    a.x/=r; a.y/=r; a.z/=r;
}

double BBQ::dist(ATOM a, ATOM b)
{
    ATOM c; 
    c.x = a.x-b.x;
    c.y = a.y-b.y;
    c.z = a.z-b.z;
    return sqrt(innerProduct(c,c));
}

int BBQ::key(int x,int y,int z)
{
    int flag = 0;
    if(z < 0) flag =1, z=-z;
    x-=20,y-=20,z-=20;
    int index = x + y*64 + 64*64*z;
    if(flag) index += 0x80000;
    return index;
}

// use kd-tree to find the index of segment
int BBQ::calc_index(ATOM seg[4])
{
    double R13 = dist(seg[0],seg[2]);
    double R24 = dist(seg[1],seg[3]);
    double R14 = dist(seg[0],seg[3]);
    
    ATOM v12,v23,v34;
    v12.x = seg[1].x - seg[0].x;
    v12.y = seg[1].y - seg[0].y;
    v12.z = seg[1].z - seg[0].z;
    
    v23.x = seg[2].x - seg[1].x;
    v23.y = seg[2].y - seg[1].y;
    v23.z = seg[2].z - seg[1].z;
    
    v34.x = seg[3].x - seg[2].x;
    v34.y = seg[3].y - seg[2].y;
    v34.z = seg[3].z - seg[2].z;
    
    //calculate the sign of R14 
    if(innerProduct(crossProduct(v12,v23),v34) < 0) R14 = -R14;
    
    //calculate the R-coordinates
    int x,y,z,index;
    int flag = 0;
       
    x = (int) (R24/0.2), y = (int) (R13/0.2), z = (int) (R14/0.2);
    // read query points
    queryPt[0] = x, queryPt[1] = y, queryPt[2] = z;
    
    //debug
//    cout << key(x,y,z);

    kdTree->annkSearch( // search
                   queryPt, // query point
                   k, // number of near neighbors
                   nnIdx, // nearest neighbors (returned)
                   dists, // distance (returned)
                   eps); // error bound
 
    index = key((int)dataPts[nnIdx[0]][0],(int)dataPts[nnIdx[0]][1],(int)dataPts[nnIdx[0]][2]);
//    cout << "\t" << index << endl;
    return index;
}

ATOM BBQ::calc_global_position(ATOM p)
{
     ATOM r;
     r.name = p.name;
     r.x = innerProduct(p,versor_t[0]);
     r.y = innerProduct(p,versor_t[1]);
     r.z = innerProduct(p,versor_t[2]);
     //cout << r.x << " " << r.y  << " " << r.z << endl;
     return r;
}

BBQ::BBQ(string database)
{
	mDatabase = database;
     	k = 1;
     	dim = 3;
     	eps = 0;
     	maxPts = 6000;
     
     	queryPt = annAllocPt(dim); // allocate query point
     	dataPts = annAllocPts(maxPts, dim); // allocate data points
     	nnIdx = new ANNidx[k]; // allocate near neigh indices
	dists = new ANNdist[k]; // allocate near neighbor dists
	nPts = 0; // read data points

	ifstream dbIn(mDatabase.c_str());
	int index;
	string c;
	ATOM CA, C, CB, O, N;
	while(dbIn >> index) {
		//cerr << index << "--" << nPts << endl;
		table[nPts].seq = index;
		table_index[index] = nPts;
		CA.name = "CA";
		CB.name = "CB";
		O.name = "O";
		N.name = "N";
		C.name = "C";
            	dbIn >> c >> C.x >> C.y >> C.z 
	                >> c >> O.x >> O.y >> O.z 
         	        >> c >> N.x >> N.y >> N.z 
         	        >> c >> CB.x >> CB.y >> CB.z 
               		>> table[nPts].freq;
		table[nPts].addAtom(CA);
		table[nPts].addAtom(C);
		table[nPts].addAtom(O);
		table[nPts].addAtom(CB);
		table[nPts].addAtom(N);
           	int rx,ry,rz,ind;
           	ind = index;
           	rx = (ind%64) + 20;
           	ind/=64;
           	ry = (ind%64) + 20;
           	ind/=64;
           	if(ind > 100) rz = -(ind - 128 + 20);
           	else rz = (ind +20);          
           	dataPts[nPts][0] = rx;
           	dataPts[nPts][1] = ry;
           	dataPts[nPts][2] = rz;
           	nPts++;
   	}
  	//cerr << "Done with reading BBQ table\n";   
	kdTree = new ANNkd_tree( // build search structure
                                dataPts, // the data points
                                nPts, // number of points
                                dim); // dimension of space
  	//cerr << "Done with BBQ Constructor\n";   
}

// build the local coordinate system
void BBQ::buildLCS(ATOM seg[4])
{
/*     versor[0].x = (seg[2].x-seg[1].x);
     versor[0].y = (seg[2].y-seg[1].y);
     versor[0].z = (seg[2].z-seg[1].z);
     normalize(versor[0]);
     
     versor[1].x = (seg[2].x-seg[0].x);
     versor[1].y = (seg[2].y-seg[0].y);
     versor[1].z = (seg[2].z-seg[0].z);
     versor[1] = crossProduct(versor[0],versor[1]);
     normalize(versor[1]);
     
     versor[2] = crossProduct(versor[0],versor[1]);
     normalize(versor[2]);
*/
    ATOM v12,v23;
    v12.x = seg[1].x - seg[0].x;
    v12.y = seg[1].y - seg[0].y;
    v12.z = seg[1].z - seg[0].z;

    normalize(v12);
    //cout << "v12" << v12.x << " " << v12.y << " " << v12.z << endl;
 
    v23.x = seg[2].x - seg[1].x;
    v23.y = seg[2].y - seg[1].y;
    v23.z = seg[2].z - seg[1].z;
    
    normalize(v23);

    //cout << "v23" << v23.x << " " << v23.y << " " << v23.z << endl;

    versor[0].x = v12.x + v23.x;
    versor[0].y = v12.y + v23.y;
    versor[0].z = v12.z + v23.z;
    
    versor[1].x = v12.x - v23.x;
    versor[1].y = v12.y - v23.y;
    versor[1].z = v12.z - v23.z;
    
    //for(int i = 0; i < 3; i++)
//	cout << versor[i].x << " " << versor[i].y << " " << versor[i].z << endl;

    normalize(versor[0]);
    normalize(versor[1]);
    versor[2] = crossProduct(versor[0],versor[1]);
    //normalize(versor[2]);
//    for(int i = 0; i < 3; i++)
//	cout << versor[i].x << " " << versor[i].y << " " << versor[i].z << endl;
          
     versor_t[0].x = versor[0].x;
     versor_t[0].y = versor[1].x;
     versor_t[0].z = versor[2].x;
     versor_t[1].x = versor[0].y;
     versor_t[1].y = versor[1].y;
     versor_t[1].z = versor[2].y;
     versor_t[2].x = versor[0].z;
     versor_t[2].y = versor[1].z;
     versor_t[2].z = versor[2].z;
}    
// generate the backbone
void BBQ::generate(vector<RESIDUE> &backbone)
{
   ATOM seg[4];
   int len_p = backbone.size();
   ATOM CA, C, N, O, CB, tmp, trans;
   for(int i = 0; i < len_p; i++) {
	//cerr << "#" << i << "-th iteration of generate()\n";

       	getSeg(seg,i,backbone);
       	int index = calc_index(seg);
       	//cerr << "index = " << index << ": " << table_index[index] << endl;
       	buildLCS(seg);

       	//CA = backbone[i].getAtom("CA");
       	CA = backbone[i].getAtom(0);

       	int nC= backbone[i].getAtomIndex("C");
	//tmp = table[table_index[index]].getAtom("C");
	tmp = table[table_index[index]].getAtom(4);
	trans = calc_global_position(tmp);
	//backbone[i].arrAtoms[nC] = calc_global_position(tmp);
       	//backbone[i].arrAtoms[nC].x += CA.x;
       	//backbone[i].arrAtoms[nC].y += CA.y;
       	//backbone[i].arrAtoms[nC].z+= CA.z;
       	backbone[i].arrAtoms[nC].x = CA.x + trans.x;
       	backbone[i].arrAtoms[nC].y = CA.y + trans.y;
       	backbone[i].arrAtoms[nC].z = CA.z + trans.z;
       
       	int nN= backbone[i].getAtomIndex("N");
	if (nN!=-1){
		//tmp = table[table_index[index]].getAtom("N");
		tmp = table[table_index[index]].getAtom(3);
		trans = calc_global_position(tmp);
		//cerr << tmp.name<<nN<<":"<<backbone[i].getAtomIndex(3)<<"("<<tmp.x<<")";
       		//backbone[i].arrAtoms[nN] = calc_global_position(tmp);
       		backbone[i].arrAtoms[nN].x = CA.x + trans.x;
       		backbone[i].arrAtoms[nN].y = CA.y + trans.y;
       		backbone[i].arrAtoms[nN].z = CA.z + trans.z;
	}
       
       	int nO= backbone[i].getAtomIndex("O");
	//tmp = table[table_index[index]].getAtom("O");
	tmp = table[table_index[index]].getAtom(2);
	trans = calc_global_position(tmp);
       	//backbone[i].arrAtoms[nO] = calc_global_position(tmp);
       	backbone[i].arrAtoms[nO].x = CA.x + trans.x;
       	backbone[i].arrAtoms[nO].y = CA.y + trans.y;
       	backbone[i].arrAtoms[nO].z = CA.z + trans.z;
       
       	int nCB= backbone[i].getAtomIndex("CB");
	if (nCB!=-1) {
		//tmp = table[table_index[index]].getAtom("CB");
		tmp = table[table_index[index]].getAtom(1);
		trans = calc_global_position(tmp);
       		//backbone[i].arrAtoms[nCB] = calc_global_position(tmp);
       		backbone[i].arrAtoms[nCB].x = CA.x + trans.x;
       		backbone[i].arrAtoms[nCB].y = CA.y + trans.y;
       		backbone[i].arrAtoms[nCB].z = CA.z + trans.z;
	}
/*
	for (int j=0; j<backbone[i].arrAtoms.size(); j++) {
		ATOM a = backbone[i].arrAtoms[j];
      		cerr << a.name << "(" << a.x << ", " << a.y << ", " << a.z << ") :: ";
	}
	cerr << i << endl;
*/
   }

	//cerr << " Move N to Head" << endl;
   for(int i = len_p-1; i>0; i--) {
       	int nN= backbone[i].getAtomIndex("N");
	if (nN!=-1){
       		//ATOM N= backbone[i-1].getAtom("N");
       		ATOM N= backbone[i-1].getAtom(3);
		N.res_idx = i;
       		backbone[i].arrAtoms[nN] = N;
	}
   }
	//cerr << "Delete head N" << endl;
   backbone[0].deleteAtom("N");
	//cerr << "Delete Gly Cb" << endl;
   for(int i = 0; i < len_p; i++) {
	if(backbone[i].name == "GLY"){
            backbone[i].deleteAtom("CB");	
            if (i==0) backbone[i].deleteAtom("N");	
	}
   }   
}
	
// get the quadrilateral	    
void BBQ::getSeg(ATOM seg[4], int pos, vector<RESIDUE>& backbone)
{
     ATOM CA, CA1, CA2, tmp;
     int len = backbone.size();
     if(pos == 0) {
	 //cerr << "getSeg: backbone[0] has " << backbone[0].atom_map.size() << " atoms." << endl ;
       	 //CA= backbone[0].getAtom("CA");
       	 CA= backbone[0].getAtom(0);
       	 //CA1= backbone[2].getAtom("CA");
       	 CA1= backbone[2].getAtom(0);
       	 //CA2= backbone[1].getAtom("CA");
       	 CA2= backbone[1].getAtom(0);
         seg[0].x = CA.x - CA1.x + CA2.x; 
         seg[0].y = CA.y - CA1.y + CA2.y; 
         seg[0].z = CA.z - CA1.z + CA2.z; 
         for(int i = 1; i < 4; i++) {
		//tmp = backbone[i-1].getAtom("CA");
		tmp = backbone[i-1].getAtom(0);
             	seg[i] = tmp;
	 }
         return;
     }
     if(pos == len-1) {
       	 //CA= backbone[pos-2].getAtom("CA");
       	 CA= backbone[pos-2].getAtom(0);
       	 //CA1= backbone[pos].getAtom("CA");
       	 CA1= backbone[pos].getAtom(0);
       	 //CA2= backbone[pos-1].getAtom("CA");
       	 CA2= backbone[pos-1].getAtom(0);
         seg[3].x = CA.x - CA1.x + CA2.x; 
         seg[3].y = CA.y - CA1.y + CA2.y; 
         seg[3].z = CA.z - CA1.z + CA2.z; 
	 //tmp = backbone[len-2].getAtom("CA");
	 tmp = backbone[len-2].getAtom(0);
         seg[0] = tmp;
	 //tmp = backbone[len-1].getAtom("CA");
	 tmp = backbone[len-1].getAtom(0);
         seg[1] = tmp;
         seg[2] = seg[3];
         return;
     }
     if(pos == len-2) {
       	 //CA= backbone[pos+1].getAtom("CA");
       	 CA= backbone[pos+1].getAtom(0);
       	 //CA1= backbone[pos-1].getAtom("CA");
       	 CA1= backbone[pos-1].getAtom(0);
       	 //CA2= backbone[pos].getAtom("CA");
       	 CA2= backbone[pos].getAtom(0);
	 //cerr << "getSeg: CA(" << CA.x << ", " << CA.y << ", " << CA.z << ")"<< endl ;
         seg[3].x = CA.x - CA1.x + CA2.x; 
         seg[3].y = CA.y - CA1.y + CA2.y; 
         seg[3].z = CA.z - CA1.z + CA2.z; 
	 //tmp = backbone[pos-1].getAtom("CA");
	 tmp = backbone[pos-1].getAtom(0);
         seg[0] = tmp;
	 //tmp = backbone[pos].getAtom("CA");
	 tmp = backbone[pos].getAtom(0);
         seg[1] = tmp;
	 //tmp = backbone[pos+1].getAtom("CA");
	 tmp = backbone[pos+1].getAtom(0);
         seg[2] = tmp;
         return;
     }
     for(int i = 0; i < 4; i++) {
	 //tmp = backbone[pos-1+i].getAtom("CA");
	 tmp = backbone[pos-1+i].getAtom(0);
         seg[i] = tmp;
	 //seg[i] = backbone[pos-1+i].CA;
     }
}

BBQ::~BBQ()
{
	delete kdTree;
}

int BBQ::lookup(vector<string>& files, const char* dir, const char *arg)
{
    DIR *dirp;
    struct dirent *dp;

    if ((dirp = opendir(dir)) == NULL) {
        perror("couldn't open directory");
        return 0;
    }

    cerr << "Looking up " << arg << " files under the directory " << dir << endl;
    do {
        errno = 0;
        if ((dp = readdir(dirp)) != NULL) {
            //cerr << "Got file " << dp->d_name << endl;
            char* p =  strstr(dp->d_name, arg);
            //cerr << "strstr position is " << (int)p << endl;
            if (p!= 0)
            {   
                //*p = '\0';
            	//cerr << dp->d_name << endl;
                files.push_back(dp->d_name);
                continue;
            }
            //else if (strcmp(dp->d_name, ".")==0 || strcmp(dp->d_name, "..")==0)
            //    continue;
            //else
            //    break;
        }
    } while (dp != NULL);

    //cerr <<"finish reading directory"<<endl;
    if (errno != 0)
        cerr <<"error reading directory"<<endl;
    (void) closedir(dirp);
    if (files.size()>0)
        return 1;
    else
        return 0;
}

/*for test
void Usage(){
        cerr<<"Usage: BBQ [-o output_dir] [-d input_dir] pdb_file"<<endl;
	cerr<<"\t-d read input files from the input dir. under this circumstance, the pdf_file is the pattern string of the input files, e.g. tor for *.tor files."<<endl;
        cerr<<"\tpdb_file: tor file of one protein."<<endl;
	cerr<<"\t-o output_dir: the directory that contains all the result files(in pdb format). The default is the current directory."<<endl;
}

int main(int argc, char** argv)
{
        if(argc<2)
        {
                Usage();
                exit(-1);
        }

        string pdb_file;
	string dir="."; // the dir that contains the tor files
	string output_dir="."; // the directory to store the result files
        extern char* optarg;
        extern int optind;
	int bUseDir = 0;
        char c=0;
        while((c=getopt(argc,argv,"d:o:"))!=EOF){
                switch(c){
                case 'd':
			bUseDir = 1;
			dir = optarg;
			cerr << "dir=" << dir << endl;
                        break;
		case 'o':
			output_dir = optarg;
			cerr << "output_dir=" << output_dir << endl;
			break;
                default:
                        Usage();
                        exit(-1);
                }
        }

 	if (optind<argc) 
          	pdb_file=argv[optind];

        //pdb_file+=optarg;
        cerr << "pdb_file is '" << pdb_file << "', dir is '" << dir << "' and the output_dir is '" << output_dir << "'\n";

    	vector<RESIDUE> bb;
    	char buf[4096];
    
    	int index;
    	double x,y,z;
    	string str, residue;

	vector<string> seq;
	if (bUseDir)
	{
		if  ( BBQ::lookup(seq, dir.c_str(), pdb_file.c_str())==0)
        	{
        	        cerr << "no " << pdb_file << " file found under the directory: " << dir << "\n\r";
                	return 0;
        	}
	}
	else
		seq.push_back(pdb_file);

	for (int n=0; n<seq.size(); n++)
	{
		string fname = seq[n];
		cerr << "read from file: " << fname << endl;
    		//freopen("1ew0a.tor","r",stdin);
		ifstream idataf((dir + "/" + fname).c_str());

		bb.clear();

		// The following code is for pdb format with only CA atoms
		string line;
		while (getline(idataf, line))
        	{       
                	//cerr << "line: " << line << "\n";
                        RESIDUE	res;
                	strtokenizer stok(line," \t");
                	if (stok.count_tokens()<9) 
                        	continue; //wrong format
                
     			ATOM CA, C, CB, N, O;
                	res.name = stok.token(3);
                	//cerr << "res_name= " << res.name << "\n";
        		CA.name = "CA";
        		C.name = "C";
        		CB.name = "CB";
       		 	O.name = "O";
       	 		N.name = "N";
                	CA.x = atof(stok.token(5).c_str());
                	CA.y = atof(stok.token(6).c_str());
                	CA.z = atof(stok.token(7).c_str());
			res.addAtom(CA);
			res.addAtom(CB);
			res.addAtom(C);
			res.addAtom(O);
			res.addAtom(N);
                	//cerr << "addAtom <" << CA.x << ", " << CA.y << ", " << CA.z << ">\n";
                	bb.push_back(res);
        	}
// / * This part is for *.tor file format
    		while(cin >> index)
    		{
    			RESIDUE res;
       			cin >> res.name >> str>>str>>str>>str>>str>>str>>str>>x>>y>>z>>str;
        		CA.name = "CA";
        		C.name = "C";
        		CB.name = "CB";
       		 	O.name = "O";
       	 		N.name = "N";
			//cout << N.name << endl;
			CA.x = x, CA.y = y, CA.z = z;
			res.addAtom(CA);
			res.addAtom(CB);
			res.addAtom(C);
			res.addAtom(O);
			res.addAtom(N);
		       	bb.push_back(res);
    		}
// * /
		//cerr << "totally " << bb.size() << "residues read\n";
		BBQ bbq;
	    	bbq.generate(bb);

    		//freopen((output_dir + "/_" + fname).c_str(),"w+",stdout);
		ofstream ofile((output_dir + "/_" + fname).c_str());

    		char tmp[100];
    		for(int i = 0; i < bb.size(); i++)
    		{
     			ATOM CA, C, CB, N, O;
       		 	CA = bb[i].getAtom("CA");
       	 		C = bb[i].getAtom("C");
       	 		if(bb[i].name!="GLY") CB = bb[i].getAtom("CB");
       	 		N = bb[i].getAtom("N");
       	 		O = bb[i].getAtom("O");
//ATOM      1  N   GLU     1      17.722   7.327 -18.903  1.00  0.69
//ATOM    124  CB  GLY    25      -7.994  16.735  10.469  0.00  0.00
	    		if(i!=0){
			    sprintf(tmp, "ATOM%7d  N   %s%6d    %8.3f%8.3f%8.3f%6.2f%6.2f\n", i*5, bb[i].name.c_str(), i+1,  N.x,  N.y,  N.z, 0, 0);
	    		    ofile << tmp;
			}
			sprintf(tmp, "ATOM%7d  CA  %s%6d    %8.3f%8.3f%8.3f%6.2f%6.2f\n", i*5+1, bb[i].name.c_str(), i+1, CA.x, CA.y, CA.z, 0, 0);
	    		ofile << tmp;
	    		sprintf(tmp, "ATOM%7d  C   %s%6d    %8.3f%8.3f%8.3f%6.2f%6.2f\n", i*5+2, bb[i].name.c_str(), i+1, C.x, C.y, C.z, 0, 0);
	    		ofile << tmp;
	    		sprintf(tmp, "ATOM%7d  O   %s%6d    %8.3f%8.3f%8.3f%6.2f%6.2f\n", i*5+3, bb[i].name.c_str(), i+1,  O.x,  O.y,  O.z, 0, 0);
	    		ofile << tmp;
			if(bb[i].name!="GLY"){
	    		    sprintf(tmp, "ATOM%7d  CB  %s%6d    %8.3f%8.3f%8.3f%6.2f%6.2f\n", i*5+4, bb[i].name.c_str(), i+1, CB.x, CB.y, CB.z, 0, 0);
	    		    ofile << tmp;
	    		}
			//cerr << "write to file: " << output_dir + "/_" + fname << ": " << tmp;
    		}
	}
    	return 0;
}
*/
