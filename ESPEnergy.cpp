/*******************************************************************************

                                 TSP1.cpp
                                 --------

  ****************************************************************************
  *                                                                          *
  *   This library is free software; you can redistribute it and/or          *
  *   modify it under the terms of the GNU Lesser General Public             *
  *   License as published by the Free Software Foundation; either           *
  *   version 2.1 of the License, or (at your option) any later version.     *
  *                                                                          *
  *   This library is distributed in the hope that it will be useful,        *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *
  *   Lesser General Public License for more details.                        *
  *                                                                          *
  *   You should have received a copy of the GNU Lesser General Public       *
  *   License along with this library; if not, write to the Free Software    *
  *   Foundation, Inc.,                                                      *
  *   59 Temple Place, Suite 330, Boston, MA  02111-1307  USA                *
  *                                                                          *
  ****************************************************************************

  Copyright (C) 2005 The University of Chicago

  Authors:
  Feng Zhao

  Description:
  Energy function to calculate ESP energies given the information of the
  the residue, radius of gyration and number of Ca contacts information.

*******************************************************************************/

#include <cstddef> // Needed to use NULL.
#include <string>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <string.h>

using namespace std;

#include "ESPEnergy.h"
#include "strtokenizer.h"

/************************** ESPEnergy implementation ***************************/

ESPEnergy::ESPEnergy()
{
	SetDefConfig();
	InitializeEnergy();
}

ESPEnergy::~ESPEnergy()
{
	delete values;
}

double ESPEnergy::Energy(string aa, int n, double rg) 
{
	int iaa=AANameToCode(aa); 
	int irg = int(rg)-MIN_RG;
	if (n==0)
		cerr << "ESP:Energy("<<iaa<<", "<<n<<", "<<irg<<")"<<endl;
	int base_idx=(iaa*MAX_N +n-1)*MAX_RG;
	if (iaa>=0 && n>=MIN_N && n<=MAX_N) {
		if (irg>=0 && irg<MAX_RG-1)
			return values[base_idx+irg];
		else if (irg==-2 || irg==-1)
			return values[base_idx];
		else if (irg==MAX_RG-1 || irg==MAX_RG)
			return values[base_idx+MAX_RG-2];
		else if (irg==MAX_RG+1 || irg==MAX_RG+2 || irg==MAX_RG+3)
			return values[base_idx+MAX_RG-1];
		else
			return 0;
	} else
		return 0;
}

int ESPEnergy::AANameToCode(string AAName)
{
	string arrResidueName[20]={"ALA","CYS","ASP","GLU","PHE","GLY","HIS","ILE","LYS","LEU",
                	          "MET","ASN","PRO","GLN","ARG","SER","THR","VAL","TRP","TYR"};
        for (int i=0; i<20; i++)
                if (arrResidueName[i]==AAName)
                        return i;
        return -1; //UNK;
}

int ESPEnergy::LoadFromFile(string &fn)
{
	ifstream par_file;
	string aa0_str;
	double energy;
	int n;
	
	cerr << "ESP loadfile " << fn << endl;
	par_file.open(fn.c_str());
	while (true) {
		par_file >> aa0_str;
		if (par_file.eof()) break;
		int aa0 = AANameToCode(aa0_str);
		par_file >> n;
		//cerr << aa0_str<<"("<<aa0 << ") " << n << " ";
		for (int r = 0; r < MAX_RG; r++) {
	          par_file >> energy;
	          values[(aa0*MAX_N +n-1)*MAX_RG+r] = energy;
		  //cerr << energy << "("<<r<<") ";
		}
		//cerr << endl;
	}
	cerr << "read esp done." << endl;
	par_file.close();
	
	return 0;
}

void ESPEnergy::InitializeEnergy()
{
	int size = 20*MAX_N*MAX_RG;
	values = new double[size];
/*
	for (int aa = 0; aa < 20; aa++)
		for (int n = 0; n < MAX_N; n++)
			for (int rg = 0; rg < MAX_RG; rg++) 
				values[aa][n][rg] = 0.0;
*/
	memset(values, 0, sizeof(double)*size);
}

void ESPEnergy::SetDefConfig()
{
	energy_coeff = 1.0;
}

void ESPEnergy::Init(string CfgName)
{
	LoadFromFile(CfgName);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////// New ESP_ENERGY ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

NewESPEnergy::NewESPEnergy()
{
	NUM_PARAMS = 0; //const int NUM_PARAMS = NUM_BIN*(NUM_BIN + NUM_GATES + 1) + NUM_DIM*NUM_GATES;
	NUM_GATES =0;
	WINDOW_SIZE = 0;
	NUM_DIM = 0;
	weights=NULL;
	Gates_table= NULL;
	psp=NULL;
	p_ref=NULL;
	m_len = 0;
	NUM_BIN = 20;
	probTable=NULL;
}

void NewESPEnergy::Init(string CfgName, vector<string>& psp_s){
	ifstream fin(CfgName.c_str());
	if(!fin.is_open()){
		cerr << "ERROR: Cannot find the params file for NewESPEnergy: " << CfgName << endl;
		return;
	}

	int data;
	char buf[102400];
	string tmp;
	for (int i=0; i<5; i++) {
		if (!fin.getline(buf,102400))
			break;
		else {
			istringstream si(buf);
			si >> tmp >> data;
			if (tmp=="num_params:")
				NUM_PARAMS = data;
			else if (tmp=="num_gates:")
				NUM_GATES = 30;
			else if (tmp=="window_size:")
				WINDOW_SIZE = 1;
			else if (tmp=="dim_features:")
				NUM_DIM = 101;
		}
	}

	m_len = psp_s.size();
	cerr << "psp file has totally lines of " << m_len << endl;
	if (weights)
		delete weights;
	if (Gates_table)
		delete Gates_table;
	if (psp)
		delete psp;
	if (p_ref)
		delete p_ref;
	weights= new double[NUM_PARAMS];
	Gates_table = new double[m_len*NUM_GATES];
	psp = new double[m_len*20];
	p_ref = new double[NUM_BIN*MAX_RG];
	if (!fin.getline(buf,102400))
		return;
	istringstream sii(buf);
	for(int i=0;i<NUM_PARAMS;i++){
		sii >> weights[i];
	}
	for (int i=1; i<NUM_BIN; i++) {
		if (!fin.getline(buf,102400))
			break;
	        istringstream si(buf);
		si >> data;
		for (int r=0; r<MAX_RG; r++) {
			si >> p_ref[i*MAX_RG+r];
			//cerr << p_ref[i*MAX_RG+r] << " ";
		}
		//cerr << endl;
	}
	fin.close();
	CalculatePSP(psp_s);
	ComputeGatesTable();
	prepareProb();
}

int NewESPEnergy::getbin(int nContact){
	int bin = nContact;
	if(nContact>20) bin=20;
	else if (nContact<1) bin=1;
	bin--;
	//cerr << bin << " " << nContact << endl;
	return bin;
}

int NewESPEnergy::CalculatePSP(vector<string>& psp_s){
	int tmp;
	for(int i=0;i<m_len;i++){
		istringstream sin(psp_s[i].c_str());
		for(int j=0;j<20;j++){
			sin >> tmp;
			psp[i*20+j]=tmp/100.0;
		}
	}
	return 1;
}

void NewESPEnergy::ComputeGatesTable()
{
	for(int gate=0;gate<NUM_GATES;gate++)
		for(int x=2;x<m_len-2;x++) {
			double score = 0;
			int pivot = 0;
			for(int offset=-2;offset<=2;offset++)
				for(int i=0;i<20;i++)
					(score+=psp[(x+offset)*20+i]*getGateWeight(gate,pivot)),pivot++;

			Gates_table[gate*m_len+x] = score;
		}
}

double NewESPEnergy::prepareProb()
{
	if (probTable==NULL){
		probTable=new double[m_len*(MAX_RG-MIN_RG+1)*NUM_BIN];
		memset(probTable, 0, sizeof(double)*m_len*(MAX_RG-MIN_RG+1)*NUM_BIN);
	}
	double* score = new double[NUM_BIN];
	double* p=new double[NUM_BIN*NUM_GATES];
    for (int rg=0; rg<MAX_RG; rg++){
	for (int res=2; res<=m_len-3; res++){
		memset(score, 0, sizeof(double)*NUM_BIN);
		memset(p, 0, sizeof(double)*NUM_BIN*NUM_GATES);
		double Z = 0;
		for(int b=0;b<NUM_BIN;b++){
			double tmp = 0;
			for(int g=0;g<NUM_GATES;g++){
				double s = Gates_table[g*m_len+res] + rg/100.0*getGateWeight(g,NUM_DIM-1);
				p[b*NUM_GATES+g] = 1.0/(1.0+exp(-s));
				tmp += p[b*NUM_GATES+g]*getStateWeight(b,g);
			}
			Z+=exp(tmp);
			score[b] = exp(tmp);
		}
/*
		double max = 0;
		int idx = 0;
		for(int b=0;b<NUM_BIN;b++){
			double tmp = 0;
			for(int g=0;g<NUM_GATES;g++){
				tmp += p[b*NUM_GATES+g]*getStateWeight(b,g);
			}
			if(tmp>max) max=tmp,idx=b;
		}
*/
		for(int b=0;b<NUM_BIN;b++)
			probTable[rg*MAX_RG*m_len+res*m_len+b]= score[b]/Z;
	}
    }
	delete p;
	delete score;
}

double NewESPEnergy::getGateWeight(int gate, int offset){ // here, offset means which feature
	return weights[NUM_BIN*(NUM_BIN+NUM_GATES+1) + gate*NUM_DIM +offset];
}

double NewESPEnergy::getStateWeight(int bin,int offset){ // here, offset means which gate
	return weights[NUM_BIN*(NUM_BIN+1)+bin*NUM_GATES + offset];
}
//double NewESPEnergy::ComputeProb(int res, double rg, int bin){
double NewESPEnergy::ComputeProb(int res, int rg, int bin){
	if(res<2 || res>m_len-3) return -1;
	//return probTable[rg*(MAX_RG-MIN_RG+1)*m_len+res*m_len+bin];
//*
	double score = 0;
	double Z = 0;
	double* p=new double[NUM_BIN*NUM_GATES];
	for(int b=0;b<NUM_BIN;b++){
		//int b = bin;
		double tmp = 0;
		for(int g=0;g<NUM_GATES;g++){
			double score = Gates_table[g*m_len+res] + rg*getGateWeight(g,NUM_DIM-1);
			p[b*NUM_GATES+g] = 1.0/(1.0+exp(-score));
			tmp += p[b*NUM_GATES+g]*getStateWeight(b,g);
		}
		Z+=exp(tmp);
		if(b==bin) score = exp(tmp);
	}
	cerr << "ESP: " << score/Z << " vs. " << probTable[rg*(MAX_RG-MIN_RG+1)*m_len+res*m_len+bin] << endl;
	//return score/Z;
//*/
/*
	double max = 0;
	int idx = 0;
	for(int b=0;b<NUM_BIN;b++){
		//int b = bin;
		double tmp = 0;
		for(int g=0;g<NUM_GATES;g++){
			tmp += p[b*NUM_GATES+g]*getStateWeight(b,g);
		}
		if(tmp>max) max=tmp,idx=b;
	}
	delete p;
*/
	return probTable[rg*MAX_RG*m_len+res*m_len+bin];
}

double NewESPEnergy::ComputeRefProb(int res, double rg, int bin){
	double score = 0;
	if(res<2 || res>m_len-3) return -1;
	double Z = 0;
	double* p=new double[NUM_BIN*NUM_GATES];
	for(int b=0;b<NUM_BIN;b++){
		//int b = bin;
		double tmp = 0;
		for(int g=0;g<NUM_GATES;g++){
			double score = Gates_table[g*m_len+res] + rg*getGateWeight(g,NUM_DIM-1);
			p[b*NUM_GATES+g] = 1.0/(1.0+exp(-score));
			tmp += p[b*NUM_GATES+g]*getStateWeight(b,g);
		}
		Z+=exp(tmp);
		if(b==bin) score = exp(tmp);
	}
	double max = 0;
	int idx = 0;
	for(int b=0;b<NUM_BIN;b++){
		//int b = bin;
		double tmp = 0;
		for(int g=0;g<NUM_GATES;g++){
			tmp += p[b*NUM_GATES+g]*getStateWeight(b,g);
		}
		if(tmp>max) max=tmp,idx=b;
	}
	delete p;
	return score/Z;
}
double NewESPEnergy::Energy(int res, double rg, int at_count){
	int k = getbin(at_count);
	int irg = int(rg)-MIN_RG;
	if (k==0 || res < 0 || k<MIN_N || k>MAX_N || irg>MAX_RG) {
		//cerr << "NewESP:Energy("<<res<<", "<<k<<", "<<irg<<")"<<endl;
		return 0; // penalty energy is set to 0
	}
	//double p = ComputeProb(res, irg/100.0,k);
	double p = ComputeProb(res, irg,k);
	//cerr << m_len << "; k=" << k << "; p=" << p << "; p_ref=" << p_ref[k*MAX_RG+irg] << endl;
	double tmp=0;
	if(p>0)
		tmp = -log(p/p_ref[k*MAX_RG+irg]);
	double energy = tmp;//min(tmp, epen);
	return energy;	
}

/************************** EPAD_ESP implementation ***************************/

#define NUM_LABELS 3

EPAD_ESP::EPAD_ESP()
{
	seq_len=0; 
	energyMat=0;
}

EPAD_ESP::~EPAD_ESP()
{
	delete energyMat;
}

double EPAD_ESP::Energy(int n, double perc_SA) 
{
	if (n==0)
		cerr << "EPAD_ESP:Energy("<<n << "," << perc_SA <<")"<<endl;
	if (n>=0 && n<seq_len) {
		if (perc_SA<10) return (*energyMat)(n, 0);
		else if (perc_SA<42) return (*energyMat)(n, 1);
		else return (*energyMat)(n, 2);
	} else
		return 0;
}

int EPAD_ESP::LoadProbTable(string & sProbFile)
{
	ifstream prob_file;
	double energy, prob;
	int n;
	double ref[]= {0.352412, 0.310499, 0.337089};
	
	cerr << "EPAD_ESP load prob table " << sProbFile << endl;
	prob_file.open(sProbFile.c_str());
	while (!prob_file.eof()) {
		prob_file >> n;
		//cerr << aa0_str<<"("<<aa0 << ") " << n << " ";
		for (int r = 0; r < NUM_LABELS; r++) {
			prob_file >> prob;
			(*energyMat)(n, r) = -log(prob/ref[r]);
			//cerr << energy << "("<<r<<") ";
		}
		//cerr << endl;
	}
	cerr << "read esp done." << endl;
	prob_file.close();
	
	return 0;
}

void EPAD_ESP::Init(int seq_len, string prob_table_file)
{
	energyMat = new ScoreMatrix(seq_len, NUM_LABELS, "EPAD_ESP");
	LoadProbTable(prob_table_file);
}
