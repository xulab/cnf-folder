#ifndef _D_DFIRE_H
#define _D_DFIRE_H
#include "molecule.h"
#include "misc.h"
#include "ScoreMatrix.h"

/*
const int mbin=30, matype=167, mabin=6, mabin2=mabin*2, mpolar=78, mplane=20;
*/
class dDFIRE
{
public:
	dDFIRE();
	void Init(bool bsym=true, bool bag=true);
	double CalcEnergy(ZhouMolecule *mol, int start);
	double CalcEnergy(string pdbfile);

protected:
	int ReadConfigFile(string CfgName);
	void initEobs();
	void initEag(FILE *fp, int nab, int nb, int nk, int np, string flag);
	void initEag1(int ma, float *edim);
	void rdpolar();
	double bin2r(int i);

public:
	bool bsym; // symmetric?
	bool bag;  // use angle?
	string datadir;
	char aa_plane[30][4][5];
	int npolar, nkind, nplane;
	double ert, epen1, epen2;
	double p_coeff, q_coeff, pq_coeff;

	ScoreMatrix res_energy;
};

#endif

