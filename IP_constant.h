#ifndef _IP_CONSTANT_H
#define _IP_CONSTANT_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>

/*
#if defined(SGICC)
#ifndef _BOOL
typedef unsigned int bool;
const int false=0;
const int true=1;
//#define false 0
//#define true 1
#endif
#endif
*/

#define ErrorLogFile	"IPThread_error.log"
#define CPULogFile	"IPThread_cpu.log"
#define OUTOFTIME_FILE	"IPThread_out_of_time.log"

#ifndef WIN32
#define FSEP	'/'
#define FSEPSTR "/"
#else
#define FSEP '\\'
#define FSEPSTR "\\"
#endif

#define MAX_RESID_LEN	6

#define MaxTemplateSize	1500 //upper limit of template size
#define MaxSeqSize	1200
#define MaxCoreSize	90  //upper limit of number of cores
#define ResidueTypes	20	//20 different aa types
#define SolventTypes	3 	//3 types of solvent accessibilities
#define StructTypes	3	//3 types of secondary structures


#define MaxNodeSize	MaxCoreSize

//for three kinds of secondary strucuture
//in order to be consistent with the singleton parameter, u can't change the value

#define HELIX	0
#define SHEET	1
#define LOOP	2

//for solvent types
//in order to be consistent with the singleton parameter, u can't change the value

#define BURIED		0
#define INTERMEDIATE	1
#define EXPOSED		2
#define NO_ASSIGNMENT	100


#define NONAMINO	20  //the 21-st amino acid

#define MaxLineSize	1024 //the max length of one line from the input file

// *********** BY TINA, May 13, 2003
#define MAX_STRING_LEN 256
// *********** BY TINA, May 13, 2003

// *********** BY TINA, May 20, 2003
// global variable
/*
class Config;

Config * config;
*/
// *********** BY TINA, May 20, 2003

//the max accessibilities of each kind of AA

const	int maxAcc[21]={101,242,171,161,127,190,195,73,193,174,182,200,192,214,127,122,144,259,235,148,-1};

//the conversion of AA to subScript
const 	int AA2SUB[26]={0,20,1,2,3,4,5,6,7,20,8,9,10,11,20,12,13,14,15,16,20,17,18,20,19,20};

//AA1Coding ???

const char AA1Coding[21]={0,4,3,6,13,7,8,9,11,10,12,2,14,5,1,15,16,19,17,18,20};

char* const AA3Coding[26]={"ALA","XXX","CYS","ASP","GLU","PHE","GLY","HIS","ILE","XXX","LYS","LEU","MET","ASN","XXX","PRO","GLN","ARG","SER","THR","XXX","VAL","TRP","XXX","TYR","XXX"};

unsigned int const POW2[31]={1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,1<<16,1<<17, 1<<18,1<<19,1<<20,1<<21,1<<22,1<<23,1<<24,1<<25,1<<26,1<<27,1<<28,1<<29,1<<30};

const int POW3[11]={1,3,9,27,81,243,729,2187,6561,19683,59049};

//when algin one domain to one sequence, almost all cores must be aligned
//domainAlignLenLowerBound[i] is the least align length when the number of cores is i.
const int domainAlignLenLowerBound[45]={0,1,2,3,4,5,6,6,7,7,8,9,10,10,11,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40};


const short MaxFileNameLength=2048;	//the maximum file name or path length

		
const float BuryCore1=10.25; //the first cutoff for the solvent access
const float BuryCore2=42.9; //the second cuttoff for the solvent access


const int SeqEntry=0;
const int CoorEntry=1;
const int CoreEntry=4;
const int PPICoreEntry=CoreEntry+1;

const int MaxLocalLinkSize=200;
const int MaxLeftLinkSize=200;
const int MaxRightLinkSize=200;


//const int MAGNIFYFACTOR=100;


const float MAXCOOR=999; //the maximum coordinates of residues
const float MAXALIGNSCORE=1e31;



//#define ProspectPath "/home/j3xu/prospect/"
//#define SeqPath "/home/j3xu/prospect/ProteinSequences/"
//#define SeqPath "/home/j3xu/prospect/demo/test0/"


const float Error=0.002;
const float WEIGHTMUTATE=1.0;
const float WEIGHTGAPPENALTY=1.0;
const float WEIGHTSINGLETON=0.24;
const float WEIGHTPAIR=0.46;
const float WEIGHTLOOPGAP=14.5;
const float WEIGHTNMR=300;


const int  MaxTemplateNum=5000;
const int MaxTemplateNameLength=20;



const float NPWEIGHTSINGLETON=0.50;
const float NPWEIGHTPAIR=0.0;
const float NPWEIGHTLOOPGAP=6.75;
const float NPWEIGHTGAPPENALTY=1.0;
//const float NPWEIGHTGAPPENALTY=1.25;


const int MINCORESIZE=6;
const int MINCORELEN=4;
const int MINEXLINKNUM=3;

const int MINSSFILTERFLAG=1;
const int  MINSSFILTERSIZE=3;
const float MINSSFILTERLEVEL=0.6;

const float SINGLESSFILTERLEVEL=8;

const int MAXLOOPSIZE=15;
const int MAXSKIPSEQSIZE	=30;
const int CONSERVELOOPSIZE=0;

const float FILTERCUTOFF=0.0;
const float FILTERSINGLETON=0.1;
const float FILTERMUTATE=0.1;

const float MINPAIRDIST=4;
const float OPENGAPPENALTY=10.8;
const float ONEINDELPENALTY=0.6;

const float GapOpenPenalty=3.631;
const float ElongGapPenalty=1.493;

const float LEVELONEREMOVAL=0;
const float LEVELTWOREMOVAL=0;
const float FIRSTCUT=8;
const float SECONDCUT=4;

const int FILTERCOMPLEXITY=3;
const float MAXCONTACTDIST=7.0;
const float MAXNMRCONTACTDIST=7.0;	//contact distance used for NMR
const float NMRReward=7.0;




//const bool USEGAPALIGN=false;
const bool USEGAPALIGN=true;
const bool USEENDGAP=true; //use head and tail gap

const int LOOPVARIATION=10;
const int LOOPMINSEQSIZE=100;
const float LOOPCONFIDENCE=9;


const int COREGRAYZONE=2;

const float BESTWEIGHTSSTRUCT=4.7;

const int MINALIGNEDSEQSIZE=60;

const bool CLIQUEFLAG=true; //use clique inequality as the cutting plane


const double MINDISTOf2CORES=11.621*pow(0.25,0.359)+0.5;

const int MaxDegree=15;

typedef float SCORE_T;
typedef short DOMAIN_T;

#define PROB_BASE	100.0

#define LOOPFINDER	1

const float PSM_aver[21]={6.4661,13.0051,11.824,13.9727,25.1246,11.04,11.2611,16.757,14.2443,13.6784,14.1105,10.9099,12.1047,17.5259,19.3949,7.7492,7.6928,27.4168,15.3683,10.9346,0};

const int LPPrimalSimplex=0;
const int LPDualSimplex=1;
const int LPEitherSimplex=2;
const int LPBarrier=3;
//for small problem, using simplex, for big problem, use barrier
const int LPSmart=4; 

//template rank method
const int RankBySVM=1;
const int RankByZRaw=2;
const int RankByRawScore=3;

#define ALIGN_ACTION	1
#define INSERT_ACTION	2
#define DELETE_ACTION	3


//for NMR constraint
#define CB2CB	1 	//bond between two CB atoms
#define CA2CA	2	//bond between two CA atoms
#define CB2CA	4	//bond between CB and CA
#define CA2CB	CB2CA


#define NOCOREALIGNBITS	1	//disable using coreAlignBits

#define Num_Mandatory	12
const char* const Mandatory_Lines []={"HEADER", "TITLE","KEYWDS","EXPDTA","AUTHOR","REVDAT","CRYST1","ORIGX1","SCALE","MASTER","TER   ","END   "};

#define ALG_NOCORE	"nc"
#define ALG_DP		"np"
#define ALG_NP		"np"
#define ALG_IP		"ip"
#define ALG_NMR		"nmr"
#define ALG_NMR_NOCORE	"ncNMR"
#define ALG_GLOBAL	"global"	//this is only used for protein-protein interaction  prediction
#define ALG_DC		"dc"
#define ALG_SMART	"smart"
#define ALG_TD		"td"	//do threading via tree decomposition

#endif

