#include "ScoringFunction.h"

#include <BALL/MATHS/vector3.h>
#include <BALL/KERNEL/protein.h>
#include <BALL/KERNEL/residue.h>
#include <vector>
#include <values.h>
#include <iostream>
#include <fstream>

#include "SideChain_util.h"
#include "strtokenizer.h"

using namespace std;
using namespace BALL;

#define SQUARE(x) ((x)*(x))
#define PI 3.14159265358979

/******ScoringFunction is the base class for all the scoring methods *********/
ScoringFunction::ScoringFunction(){
}

ScoringFunction::~ScoringFunction(){
}

int ScoringFunction::Init(string dat_file1){
  return 0;
}

int ScoringFunction::Init(string dat_file1, string dat_file2){
  return 0;
}

int ScoringFunction::Init(string dat_file1, int* data){
  return 0;
}

int ScoringFunction::Init(string dat_file1, vector<string>& data){
  return 0;
}

// we assume the native score to be the minimum float when nothing is known
// about this protein
double ScoringFunction::estimateNativeScore(string& sequence, string& ss){
  return MINFLOAT;
}

double ScoringFunction::evaluate(
        string& sequence, string& ss, vector<Vector3 >& structure){
  return 0;
}
double ScoringFunction::evaluate(string& sequence, string& ss, Protein* protein){
  return 0;
}
double ScoringFunction::evaluate(vector<RESIDUE >& structure, int start, int end){
  return 0;
}
double ScoringFunction::evaluate(string& ss, vector<RESIDUE >& structure){
  return 0;
}
double ScoringFunction::evaluate(vector<RESIDUE >& structure){
  return 0;
}
double ScoringFunction::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start){
  return 0;
}

//calculate the radius of a given protein
double ScoringFunction::CalculateRadius(vector<Vector3 >& structure){

  double dist=0;
  for(int i=0; i<(int)structure.size();i++)
    for(int j=0; j<i; j++)
      dist+=structure[i].getSquareDistance(structure[j]);
  dist/=(SQUARE(structure.size())-structure.size());
  dist=sqrt(dist);
  return dist;
}

void ScoringFunction::MassCenter(vector<RESIDUE >& prot)
{
  int n = 0;
  for(int aa1=0; aa1<(int)prot.size(); aa1++) {
    RESIDUE res1=prot[aa1];
    for(int i=0; i<(int)res1.arrAtoms.size();i++) {
      string at1 = res1.arrAtoms[i].name;
      ATOM atom = res1.arrAtoms[i];

      n++;
      massCenter+=getPosition(atom);
    }
  }
  if (0 < n) massCenter/=n;
}

void ScoringFunction::radial(vector<RESIDUE >& prot)
{
  double total_dist=0, seq_size=0, ave_dist, total_dev, total_dist_sqr = 0.0;
  double* dist = new double[prot.size()];
  for(int aa1=0; aa1<(int)prot.size(); aa1++) {
    RESIDUE res1=prot[aa1];
    //ATOM Ca=res1.getAtom("CA");
    ATOM Ca=res1.getAtom(0);
    dist[aa1] = sqrt(massCenter.getSquareDistance(getPosition(Ca)));
    total_dist += dist[aa1];
    total_dist_sqr += dist[aa1] * dist[aa1];
    seq_size += 1.0;
  }

  rg = sqrt(total_dist_sqr / seq_size);
  ave_dist = total_dist / seq_size;
  total_dev = 0.0;
  for(int aa1=0; aa1<(int)prot.size(); aa1++)
    total_dev += (dist[aa1] - ave_dist) * (dist[aa1] - ave_dist);

  ru = sqrt(total_dev / (seq_size - 1.0));
  delete dist;
}

// the ratio of the Rg of the non-polar Cb atoms to the Rg of the polar Cb
// atoms, is called burial ratio(Br)
void ScoringFunction::burial_ratio(vector<RESIDUE >& prot)
{
  int phob_count = 0, phil_count = 0;
  double dist = 0.0, pair_count = 0.0, total_phobic=0, total_philic=0;
  for(int aa1=0; aa1<(int)prot.size(); aa1++) {
    RESIDUE res1=prot[aa1];
    //ATOM Cb=res1.getAtom("CB");
    ATOM Cb=res1.getAtom(1);
    if (Cb.name=="")
      continue;
    double dist = massCenter.getSquareDistance(getPosition(Cb));
    if (isHydroPhobe(res1.name)){
      total_phobic += dist * dist;
      phob_count += 1;
    } else {
      total_philic += dist * dist;
      phil_count += 1;
    }
  }

  br = sqrt(total_phobic / phob_count) / sqrt(total_philic / phil_count);
}

/****************** ScoringByRadius ***************************/
ScoringByRadius::ScoringByRadius(){
}

ScoringByRadius::~ScoringByRadius(){
}

double ScoringByRadius::estimateNativeScore(string& sequence, string& ss){
  //here we assume that the protein is gobular, so that we can use 
  // 2.2*L^0.38 to estimate its radius where L is the number of amino
  // acids in this protein
  return 2.2*pow(sequence.size(), 0.38);
}

//calculate the radius of a given protein
double ScoringByRadius::evaluate(
        string& sequence, string& ss, vector<Vector3 >& structure){
  double dist=0;
  for(int i=0; i<(int)structure.size();i++){
    for(int j=0; j<i; j++) {
      dist+=structure[i].getSquareDistance(structure[j]);
    }
  }
  dist/=(SQUARE(structure.size())-structure.size());
  dist=sqrt(dist);
  return dist;
}

double ScoringByRadius::evaluate(vector<RESIDUE >& structure)
{
  vector<Vector3 > g_backbone;
  for (int i=0; i<(int)structure.size(); i++) {
    //ATOM CA = structure[i].getAtom("CA"); 
    ATOM CA = structure[i].getAtom(0);
    g_backbone.push_back(Vector3(CA.x, CA.y, CA.z));
  } 
  string ss="", seq="";
  return evaluate(seq, ss, g_backbone);
}

double ScoringByRadius::evaluate(string& sequence, string& ss, Protein* protein)
{
  vector<Vector3 > structure;
  return evaluate(sequence, ss, structure);
}

double ScoringByRadius::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(structure);
}

/****************** ScoringByContact ***************************/
ScoringByContact::ScoringByContact(){
  mpContact=NULL;
  mName = "contact";
}

ScoringByContact::~ScoringByContact(){
}

int ScoringByContact::Init(string dat_file1, int* data)
{
  mpContact=(Contact*) data;
}

double ScoringByContact::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  double energy=0;
  double total_pair=mpContact->nFolded+0.0001;
  for(int i=0; i<(int)mpContact->bpList.size(); i++){
    Bipartite bp=mpContact->bpList[i];
    if (bp.folded1==false || bp.folded2==false || bp.min_err>11)
      continue;
    for (int j=0; j<=bp.tail1-bp.head1; j++){
      int p1=bp.head1+j,p2=bp.head2+j;
      double predicted_dist=8;//mpContact->contactMap(p1,p2);
      //ATOM CA1 = structure[p1].getAtom("CA"); 
      ATOM CA1 = structure[p1].getAtom(0);
      //ATOM CA2 = structure[p2].getAtom("CA"); 
      ATOM CA2 = structure[p2].getAtom(0); 
      Vector3 ca1(CA1.x, CA1.y, CA1.z), ca2(CA2.x, CA2.y, CA2.z);
      double dist=sqrt(ca1.getSquareDistance(ca2));
      double err = max(0.0, abs(predicted_dist-dist)-3); // give no penalty if the error is less than 2A
      energy+=err*err;
      total_pair++;
    }
  }
  return energy/total_pair*0.0015*50;
}

/****************** ScoringByDOPE *******************************/
ScoringByDOPE::ScoringByDOPE(){
  calc_pw=false;
}

ScoringByDOPE::~ScoringByDOPE(){
}

// This function is necessary for initialization, 'coz DOPE need to read
// the dope.par for computation 
int ScoringByDOPE::Init(string dat_file1)
{
  return dope.LoadFromFile(dat_file1);
}

double ScoringByDOPE::hbonds(vector<RESIDUE >& structure)
{
  double dist, pw, NH_NC_angle, CO_CN_angle, energy = 0.0,
         phi0, psi0, phi1, psi1;
  ATOM N_atom, H_atom, O_atom, C_atom;
  Vector3 NH, CO, NC, CN;

  for(int aa0=0; aa0<(int)structure.size()-3; aa0++){
    RESIDUE res0 = structure[aa0];
    for(int aa1=aa0+3; aa1<(int)structure.size(); aa1++) {
      int cd = aa1 - aa0;
      if ((cd < 5) && (aa0 < 6 || aa0 > (int)structure.size() - 6)) 
        continue;
      RESIDUE res1 = structure[aa1];
      if ((res0.phi<0.0 && res0.phi>-170.0 && res0.psi<50.0 && res0.psi>-120.0 
            || res1.phi<0.0 && res1.phi>-170.0 && res1.psi<50.0 && res1.psi>-120.0)
          && cd < 5) 
        continue;
      ATOM H0 = res0.getAtom("HN");
      ATOM O1 = res1.getAtom("O"); // Backbone CO found.
      if (H0.name!=""){
        double delta= BBQ::dist(O1, H0);
        if ( delta>1.7 && delta<2.6) {
          N_atom = res0.getAtom("N");
          H_atom = res0.getAtom("HN");
          NH = getPosition(H_atom) - getPosition(N_atom);

          O_atom = res1.getAtom("O");
          C_atom = res1.getAtom("C");
          CO = getPosition(O_atom) - getPosition(C_atom);

          NC = getPosition(C_atom) - getPosition(N_atom);
          CN = getPosition(N_atom) - getPosition(C_atom);

          NH_NC_angle = NH.getAngle(NC)*180/PI;
          CO_CN_angle = CO.getAngle(CN)*180/PI;

          pw = sqrt((NH_NC_angle - 90.0) * (NH_NC_angle - 90.0)
                  + (CO_CN_angle - 90.0) * (CO_CN_angle - 90.0));
          
          if (pw > 90.0) energy -= pw;
        }
      }
      ATOM O = res0.getAtom("O"); // Backbone CO found.
      ATOM H1 = res1.getAtom("HN");
      if (H1.name!=""){
        double delta= BBQ::dist(O, H1);
        if ( delta>1.7 && delta<2.6) {
          N_atom = res1.getAtom("N");
          H_atom = res1.getAtom("HN");
          NH = getPosition(H_atom) - getPosition(N_atom);

          O_atom = res0.getAtom("O");
          C_atom = res0.getAtom("C");
          CO = getPosition(O_atom) - getPosition(C_atom);

          NC = getPosition(C_atom) - getPosition(N_atom);
          CN = getPosition(N_atom) - getPosition(C_atom);

          NH_NC_angle = NH.getAngle(NC)*180/PI;
          CO_CN_angle = CO.getAngle(CN)*180/PI;

          pw = sqrt((NH_NC_angle - 90.0) * (NH_NC_angle - 90.0)
                  + (CO_CN_angle - 90.0) * (CO_CN_angle - 90.0));
          
          if (pw > 90.0) energy -= pw;
        }
      }
    }
  }
  return energy;
}

double ScoringByDOPE::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(structure);
}

double ScoringByDOPE::evaluate(vector<RESIDUE >& structure)
{
  return evaluate(structure, 0, -1);
}

/* For the case when we want to evaluate the decoys using DOPE_PW, we need to
   minimize a E_radial in the beginning.
     E_radial=100*R_u * R_g * B_r + E_(DOPE-repulsive)
   where E_(DOPE-repulsive) is sum of the positive DOPE terms:
   Each MCSA simulation is repeated using Eradial until the Br is less than 
   0.80. We cap the minimum value of Ru at 2.5 A, since it is very easy for
   the chain to fold into a ring structure with an Ru close to 0. The 
   multiplied radial terms have a coefficient of 100 so that their combined
   magnitude is significant relative to the repulsive part of DOPE
*/
double ScoringByDOPE::evaluate(vector<RESIDUE >& structure, int start, int end)
{
// Calculate DOPE energy of the protein by adding up the energy between each
// pairs of the atoms

////////// For dope_pw ////////////////////
  calc_pw = true;
  if (calc_pw){
    MassCenter(structure);
    radial(structure); //RG+RU
    burial_ratio(structure); //BR
  }
///////////////////////////////////////////

  if (end<start)
    end = structure.size();
  double energy = 0.0;
  for(int i=0; i<(int)structure.size()-1; i++)
  {
    double residue_energy = 0.0;
    for(int j=i+1; j<(int)structure.size(); j++)
    {
      double e_pair = calcResidueDOPE(structure[i], structure[j]);
      if (calc_pw){
        if (e_pair == 10.0) e_pair = 1000.0;
        if (e_pair > 0.0) residue_energy += e_pair;
      } else {
        residue_energy += e_pair;
      }
    }
    energy +=residue_energy;
  }

  if (calc_pw){
      if (ru < 2.5) ru = 2.5;
    if (br < 0.80 && rg < 12.0) energy += hbonds(structure);
    energy += (100.0 * br * ru * rg);
  }

  return energy;
}

#define MAX_RESIDUE_DISTANCE 25.0

// Calculate Dope energy between 2 residues
double ScoringByDOPE::calcResidueDOPE( RESIDUE& res1, RESIDUE &res2)
{
  string atom1, atom2;
  double energy = 0;
  for(int i=0; i<(int)res1.arrAtoms.size();i++){
    atom1 = res1.arrAtoms[i].name;

    for(int j=0; j<(int)res2.arrAtoms.size();j++){
      atom2 = res2.arrAtoms[j].name;
      double dist= BBQ::dist(res1.arrAtoms[i], res2.arrAtoms[j]);
      //cerr << "distance = " << dist << endl;
      if (dist>MAX_RESIDUE_DISTANCE)
        return energy;

      double e_pair = dope.calcAtomDOPE(res1.name, atom1, res2.name, atom2, dist);
      energy += e_pair;
    }
  }       
  return energy;
}

/****************** ScoringByEPAD *******************************/
ScoringByEPAD::ScoringByEPAD(){
  mName="EPAD";
  startRes=-1;
  endRes=-1;
  predicted=false;
  indicator=0; // CA atoms
  //mfunc=FUNC_CONTACT_L5;
  //mfunc=FUNC_CONTACT_L10;
  mfunc=FUNC_CONTACT_ENERGY_TB; // For Rosetta
  //mfunc=FUNC_CONTACT_ENERGY_FM; // For old decoy sets like fisa, lattice, etc.
  //mfunc=FUNC_CONTACT_NUMBER_MORE;
  //mfunc=FUNC_CONTACT_NUMBER;
  //mfunc=FUNC_CONTACT_MAP_RECOVER;
  //mfunc=FUNC_CONTACT_MAP_ALL;
}

ScoringByEPAD::~ScoringByEPAD(){
}

int ScoringByEPAD::Init(string config_file, string dat_file)
{
  model.Initialize(config_file, dat_file);
  model.reset(startRes, endRes, residue_flag);
  NEFF=model.NEFF;
  if (startRes>-1 && endRes>-1 || mfunc==FUNC_CONTACT_DISTRIBUTION){
    model.testData->PredictDistances(mfunc);
    predicted=true;
  }
  return 1;
}

double ScoringByEPAD::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(structure);
}

double ScoringByEPAD::evaluate(vector<RESIDUE >& structure)
{
  return evaluate(structure, 0, -1);
}

double ScoringByEPAD::evaluate(vector<RESIDUE >& structure, int start, int end)
{
  int len=structure.size();
  if (bLongFormat)
    pointEnergies.resize(len);
  if (end<start)
    end = structure.size();
  endRes=end;

  model.reset(startRes, endRes, residue_flag);
  if (!predicted) {
    model.residue_flag.clear();
    for (int i=0; i<(int)structure.size(); i++){
      if (structure[i].name=="")
        model.residue_flag.push_back(0);
      else
        model.residue_flag.push_back(1);
    }
    model.testData->PredictDistances(mfunc);
    predicted=true;
  }

  int contact_dist_min=6, cd=0;
  vector<Vector3 > g_backbone;
  for (int i=0; i<len; i++) {
    //ATOM CA = structure[i].getAtom("CA"); 
    ATOM CA = structure[i].getAtom(0);
    g_backbone.push_back(Vector3(CA.x, CA.y, CA.z));
  } 
  double rg = CalculateRadius(g_backbone);
  double energy = 0.0;
  for(int i=0; i<len-1; i++) {
    if (structure[i].seq==-1) continue;
    ATOM atom_i;
    if (indicator==0)
      atom_i = structure[i].getAtom(0);
    else if (structure[i].name.compare("GLY")==0)
      atom_i = structure[i].getAtom(0);
    else
      atom_i = structure[i].getAtom(1);
    
    double residue_energy = 0.0;
    for(int j=i+contact_dist_min; j<len; j++) {
      if (structure[i].flag==-1 && structure[j].flag==-1) continue;
      //if (i<4 && j>len-4) continue;
      cd = abs(i-j); //chainDist
      if (model.model_typ==1){
        if (cd>=180&&len>=250&&len<340 || cd>=110&&len>=150&&len<250
            || cd>=70&&len>=109&&len<150 || cd>=50&&len>=80&&len<109
            || cd>=35&&len>55&&len<=65 || cd>=11&&len<40) 
          continue; // For moulder
      }

      double dist;
      ATOM atom_j;
      if (indicator==0)
        atom_j=structure[j].getAtom(0);
      else if (structure[j].name.compare("GLY")==0)
        atom_j=structure[j].getAtom(0);
      else
        atom_j=structure[j].getAtom(1);
      
      dist= BBQ::dist(atom_i, atom_j);//+0.8;
      //if (dist>MAX_RESIDUE_DISTANCE)
      //  break;
      if (model.model_typ==1){
        if (dist>15.1&&len>100&&len<105 || dist>27&&len>=105&&len<160
            || dist>41&&len>=160&&len<210 || dist>41&&len>=210&&len<300
            || dist>50&&len>=300&&len<400)
          continue; // For Moulder
      }

      int ri = ResidueIndex(structure[i].name_idx),
          rj = ResidueIndex(structure[j].name_idx);
      double e_pair = model.CalculatePairEnergyS12(
                structure[i].seq, structure[j].seq, dist, rg, mfunc, ri, rj);
      residue_energy += e_pair;
    }
    energy +=residue_energy;
    if (bLongFormat)
      pointEnergies[i]=residue_energy*0.0015*50;
  }

  double energyB = 0.0;
  double energyA = 0.0;
  energy=energy*0.0015*50;
//*
  for(int i=0; i<len-1; i++) {
    if (structure[i].seq==-1 || model.cond_weight==0) continue;
    RESIDUE resi=structure[i];
    double residue_energyA = 0.0;
    double residue_energyB = 0.0;
    for(int j=i+contact_dist_min; j<(int)structure.size(); j++) {
      cd = abs(i-j); //chainDist
      RESIDUE resj=structure[j];
      for(int ai=0; ai<(int)resi.arrAtoms.size();ai++){
        if (ai>4)
          continue;
        ATOM atom_i = resi.arrAtoms[ai];
        int atom1=atom_i.name_idx;
        for(int aj=0; aj<(int)resj.arrAtoms.size();aj++){
          if (aj>4)
            continue;
          ATOM atom_j = resj.arrAtoms[aj];
          int atom2=atom_j.name_idx;
          //if (!(atom_i.name_idx==0&&atom_j.name_idx==0 || (atom_i.name_idx==2||atom_i.name_idx==3)&&atom_j.name_idx==2)) // use Ca-Ca, and N-O, O-O
          //if (!((atom1==2||atom1==3)&&atom2==2)) // use Ca-Ca, and N-O, O-O
          //if (!((atom1==0||atom1==1||atom1==2)&&atom2==0 || atom1==1&&atom2==4 || atom1==4&&atom2==1 ||(atom1==1||atom1==2||atom1==3)&&atom2==2))
          if (!(atom1==2&&atom2==0 || atom1==2&&atom2==1 || (atom1==0||atom1==1||atom1==2||atom1==3)&&atom2==2))
            continue;
          double dist = BBQ::dist(atom_i, atom_j);
          if (dist>MAX_RESIDUE_DISTANCE)
            break;
          int ri=ResidueIndex(structure[i].name_idx), rj=ResidueIndex(structure[j].name_idx);
          double e_pair=model.CalculateConditionalPairEnergyS12(structure[i].seq,structure[j].seq,dist,rg,mfunc,ri,rj,atom1,atom2);
          //if (atom1==0 && atom2==0) residue_energyA += e_pair;
          //else
            residue_energyB += e_pair;
          if (mfunc==FUNC_CONTACT_DISTRIBUTION_ENERGY) {
            cout << structure[i].name << " " << atom_i.name << " " 
                 << structure[j].name << " " << atom_j.name << " " 
                 << structure[i].seq <<" "<< structure[j].seq << " " << dist << " ";
            for (double d=2.8; d<15.5; d+=0.5){
              double e = model.CalculateConditionalPairEnergyS12(
                  structure[i].seq,structure[j].seq,d,rg,mfunc,ri,rj,atom1,atom2);
              cout << e*0.0015*50 << " ";
            }
            cout << endl;
          }
        }
      }
    }
    energyB += residue_energyB;
    //energyA +=residue_energyA
    if (bLongFormat)
      pointEnergies[i]=(residue_energyA+residue_energyB*model.cond_weight)*0.0015;
  }
  //energyB=energyB*0.0015;
  //energyB=energyB*0.0015*10; // good for 1A conditional prob. NNs12Rg3_model.r0.010000_G100g40_w15_f_312
  //energyB=energyB*0.0015*7; // 7; good for 0.5A conditional prob. NNs12Rg3_model.r0.010000_G100g40_w15_f_312
  //energyB=energyB*0.0015*7.5; // 7.5; good for 0.5A conditional prob. NNs12Rg2_model.r0.010000_G100g40_w15_f_34: 14.14/29/0.395/2.15 with cond_prob NO
  energyA=0; //energyA*0.0015*0.01; // 0.5; Ca-Ca cond energy. Good for 0.5A conditional prob. NNs12Rg2_model.r0.010000_G100g40_w15_f_34: 14.14/29/0.395/2.15 with cond_prob NO
  //energyB=energyB*0.0015*11; // 11 ; good for 0.5A conditional prob. NNs12Rg1_model.r0.010000_G100g40_w15_f_318: 12.57/25/0.398/1.99 with cond_prob NO
  //energyB=energyB*0.0015*35; // 35 ; good for 0.5A conditional prob. PIcnRg1_model.r0.010000_G100g40_w15_f_52: 14.38/26/0.41/2.02 with cond_prob (Ca/Cb/O/N)-O + O-(Ca/Cb)
  energyB=energyB*0.0015*model.cond_weight; // 35 ; THREADING good for 0.5A conditional prob. PIcnRg1_model.r0.010000_G100g40_w15_f_52: 14.38/26/0.41/2.02 with cond_prob (Ca/Cb/O/N)-O + O-(Ca/Cb)
  //cerr << " DPN energy=" << energy << "; energyB=" << energyB << endl;
// */  
  //model.printConfusionMatrix(rg);
  mHBEnergy=energyB;
  return energy+energyA+energyB;
}

int ScoringByEPAD::ResidueIndex(int aa)
{
  string letters = "A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V  Z";
  //                0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20
  int r_idx[26] = { 0, -1, 4, 3, 6, 13, 7, 8, 9, -1, 11, 10, 12, 2, -1, 14,
                    5, 1, 15, 16, -1, 19, 17, -1, 18, 20};
  return r_idx[aa];
}

/****************** ScoringByDopeBeta *******************************/
ScoringByDopeBeta::ScoringByDopeBeta(){
  mBurialPenalty = 0;
  mName = "dopebeta";
  CB_burial= NULL;
  NB_burial= NULL;
}

ScoringByDopeBeta::~ScoringByDopeBeta(){
  if (CB_burial) delete CB_burial;
  if (NB_burial) delete NB_burial;
}

// This function is necessary for initialization, 'coz DOPE need to read
// the dope.par for computation 
int ScoringByDopeBeta::Init(string dat_file1)
{
  dopeB.Init(dat_file1);
  Burial_renorm = dopeB.Burial_renorm;
  Burial_min_atcount = dopeB.Burial_min_atcount;
  Burial_max_atcount = dopeB.Burial_max_atcount;
  Burial_sphere_rad = dopeB.Burial_sphere_rad;
  Penalty_coeff = dopeB.Penalty_coeff;
  CB_CB_focus=dopeB.CB_CB_focus;
  min_chain_dist=dopeB.min_chain_dist;
  max_chain_dist = dopeB.max_chain_dist;
  min_CB_CB_dist=dopeB.min_CB_CB_dist;
  CB_CB_coefficient=dopeB.CB_CB_coefficient;

  return 1;
}

void ScoringByDopeBeta::CalculateBurial(vector<RESIDUE >& structure)
{
  int bins = structure.size();
  if (CB_burial==NULL){
    CB_burial= new double[bins+1];
    NB_burial= new double[bins+1];
  }
  memset(CB_burial, 0, sizeof(double)*(bins+1));
  memset(NB_burial, 0, sizeof(double)*(bins+1));

  int at_count, at_count_hb = 0, ChainDist;
  int max_at_count = Burial_min_atcount + Burial_max_atcount;
  vector<Vector3> cbs;
  for (int i=0; i<(int)structure.size(); i++) {
    //ATOM CA = structure[i].getAtom("CA"); 
    ATOM CA = structure[i].getAtom(0); 
    cbs.push_back(Vector3(CA.x, CA.y, CA.z));
  } 
  double dist;
  for(int aa1=0; aa1<(int)structure.size(); aa1++) {
    at_count = 0;
    at_count_hb = 0;

    RESIDUE res1=structure[aa1];
    for(int i=0; i<(int)res1.arrAtoms.size();i++) {
      ATOM atom1=res1.arrAtoms[i];
      //string at1 = atom1.name;
      int at1 = atom1.name_idx;
      Vector3 atom1V3(atom1.x, atom1.y, atom1.z);
      //if (at1=="CB" && res1.name.compare("GLY")!=0) {
      if (at1==1 && res1.name.compare("GLY")!=0) {
        //ATOM CB1 = res1.arrAtoms[i];
        for(int aa2=0; aa2<(int)structure.size(); aa2++) {
          if (abs(aa1-aa2)<=5) // we need longer chain distance(>5)
            continue;
          if (structure[aa1].flag==-1 && structure[aa2].flag==-1) continue;
          //if (aa1!=aa2 && at2=="CB") {
          if (aa1!=aa2) {
            dist = atom1V3.getDistance(cbs[aa2]);
            if (dist <= Burial_sphere_rad) at_count++;
            // We reached the maximum count, no need to keep counting.
            if (max_at_count <= at_count) break; 
          }
        }
      }
      //if (at1=="O" || at1 == "N") {
      if (at1==2 || at1 ==3) {
        // Counting how many atoms surround atom1.
        for(int aa2=0; aa2<(int)structure.size(); aa2++) {
          if (structure[aa1].flag==-1 && structure[aa2].flag==-1) continue;
          ChainDist = abs(aa1-aa2);
          if (aa1!=aa2 && ChainDist>5) // we need longer chain distance(>5)
          {
            dist = atom1V3.getDistance(cbs[aa2]);
            if (dist <= Burial_sphere_rad) at_count_hb++;
            // We reached the maximum count, no need to keep counting.
            if (max_at_count <= at_count_hb) break; 
          }
        }
        // We reached the maximum count, no need to keep counting.
        if (max_at_count <= at_count) break;
      }
    }
    CB_burial[aa1]= at_count;
    NB_burial[aa1]= at_count_hb;
  }
}

int ScoringByDopeBeta::PenaltyIndex(string AAName)
{
  string arrResidueName[21]=
        {"ALA","ARG","ASN","ASP","CYS","GLN","GLU","GLY","HIS","ILE",
         "LEU","LYS","MET","PHE","PRO","SER","THR","TRP","TYR","VAL","HET"};
  for (int i=0; i<21; i++)
    if (arrResidueName[i]==AAName)
      return PenaltyIndex(i);
  return 0; //UNK;
}

int ScoringByDopeBeta::PenaltyIndex(int aa)
{
  string letters = "ARNDCQEGHILKMFPSTWYVZ";
  switch (aa)
  {
  case 0:
    return 8;
  case 1:
    return 6;
  case 2:
    return 7;
  case 3:
    return 7;
  case 4:
    return 9;
  case 5:
    return 6;
  case 6:
    return 5;
  case 7:
    return 100;
  case 8:
    return 6;
  case 9:
    return 10;
  case 10:
    return 9;
  case 11:
    return 6;
  case 12:
    return 9;
  case 13:
    return 9;
  case 14:
    return 7;
  case 15:
    return 6;
  case 16:
    return 7;
  case 17:
    return 8;
  case 18:
    return 8;
  case 19:
    return 10;
  }
  return 0;
}

double ScoringByDopeBeta::CalculatePenalty(vector<RESIDUE >& structure)
{
  double penalty= 0.0;
  for(int aa=0; aa<(int)structure.size(); aa++) {
    int idx = PenaltyIndex(structure[aa].name);
    if (CB_burial[aa] >= idx) {
      penalty += 1.0;
    }
  }
  return penalty;
}

double ScoringByDopeBeta::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(structure, start, 0);
}

double ScoringByDopeBeta::evaluate(vector<RESIDUE >& structure, int start, int end)
{
  double dif;
  struct timeval ts, te;
  //gettimeofday(&ts, 0);

  MassCenter(structure);
  radial(structure); //RG+RU
  burial_ratio(structure); //BR

  // gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
  // cerr << "    DopeBeta_RG+BR: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000
  //      << " miliseconds." << endl;
  //gettimeofday(&ts, 0);

// Calculate DopeBeta energy of the protein by adding up the energy among each
// the pairs of the atoms between all the residues
  vector<AtomArray> allBackBone;
  for (int i=0; i<(int)structure.size(); i++) {
    AtomArray arrAtom;
    for(int j=0; j<(int)structure[i].arrAtoms.size();j++){
      ATOM atom = structure[i].arrAtoms[j];
      arrAtom.push_back(Vector3(atom.x, atom.y, atom.z));
    }
    allBackBone.push_back(arrAtom);
  } 
  double energy = 0.0;
  for(int i=0; i<(int)structure.size()-1; i++) {
    double residue_energy = 0.0;
    for(int j=i+1; j<(int)structure.size(); j++) {
      if (structure[i].flag==-1 && structure[j].flag==-1) continue;
      int chainDist = abs(i-j);
      if (chainDist<min_chain_dist || chainDist > max_chain_dist) continue;
      residue_energy += calcResidueDOPE(structure[i], structure[j], allBackBone[i], allBackBone[j],chainDist);
    }
    energy +=residue_energy;
  }

  //gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
  //cerr << "    DopeBeta_energy: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000
  //     << " miliseconds." << endl;
  //gettimeofday(&ts, 0);
  if (0.0 < Penalty_coeff) {
    double penalty = 0.0;
    CalculateBurial(structure);
    penalty = CalculatePenalty(structure);
    mBurialPenalty = penalty;
    energy += Penalty_coeff * penalty;
  }

  //gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
  //cerr << "    DopeBeta_penalty: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000
  //     << " miliseconds." << endl;
  //gettimeofday(&ts, 0);

  energy=energy*0.0015*50;
  return energy;
}

// Calculate Dope energy between 2 residues
double ScoringByDopeBeta::calcResidueDOPE( 
        RESIDUE& res1, RESIDUE &res2, AtomArray& arr1, AtomArray& arr2, int cd)
{
  //cerr << "residue_id1=" << res1.name << res1.arrAtoms.size() << "residue_id2="
  //     << res2.name << res2.arrAtoms.size() << endl;
  //string atom1, atom2;
  int atom1, atom2;
  double energy = 0;
  for(int i=0; i<(int)res1.arrAtoms.size();i++){
    //atom1 = res1.arrAtoms[i].name;
    atom1 = res1.arrAtoms[i].name_idx;
    //if (atom1!="CA" && atom1!="CB" && atom1!="O" && atom1!="N" && atom1!="C")
    if (atom1>4)
      continue;

    for(int j=0; j<(int)res2.arrAtoms.size();j++){
      //atom2 = res2.arrAtoms[j].name;
      atom2 = res2.arrAtoms[j].name_idx;
      //if (atom2!="CA" && atom2!="CB" && atom2!="O" && atom2!="N" && atom2!="C")
      if (atom2>4)
        continue;
      //double dist= BBQ::dist(res1.arrAtoms[i], res2.arrAtoms[j]);
      double dist = arr1[i].getDistance(arr2[j]);
      //dist=sqrt(dist);
      //cerr<<"atom2="<<res2.arrAtoms[j].name<<atom2<<"("
      //    <<res2.arrAtoms[j].x<<","<<res2.arrAtoms[j].y<<","<<res2.arrAtoms[j].z
      //    <<"); distance="<<dist<<endl;
      if (dist>MAX_RESIDUE_DISTANCE)
        return energy;

      double e_pair = dopeB.calcAtomDOPE(
                         res1.name_idx, atom1, res2.name_idx, atom2, dist);
      bool add_energy = false;
      if (CB_CB_focus) {
        if ((cd >= min_CB_CB_dist) || (e_pair > 0.00)) add_energy = true;
        else add_energy = false;
      }
      else add_energy = true;
  
      if (add_energy && e_pair<0.00 && CB_CB_focus 
          // atom1== "CB" && atom2 == "CB"
          && atom1==1 && atom2 ==1 && cd>= min_CB_CB_dist)
      {
        e_pair = e_pair * CB_CB_coefficient;
      }
    
      if (add_energy)
        energy += e_pair;
    }
  }
  return energy;
}

/****************** ScoringByDOPEBack *******************************/
ScoringByDOPEBack::ScoringByDOPEBack(){
  interaction_coeff[0] = 1.0;
  interaction_coeff[1] = 1.0;
  interaction_coeff[2] = 1.0;
  interaction_coeff[3] = 1.0;
}

ScoringByDOPEBack::~ScoringByDOPEBack(){
}

// This function is necessary for initialization, 'coz DOPE need to read
// the dope.par for computation 
int ScoringByDOPEBack::Init(string dat_file1)
{
  dope_back.Init(dat_file1.c_str());
  interaction_coeff[0] = dope_back.interaction_coeff[0];
  interaction_coeff[1] = dope_back.interaction_coeff[1];
   interaction_coeff[2] = dope_back.interaction_coeff[2];
  interaction_coeff[3] = dope_back.interaction_coeff[3];
  cerr << "interaction_coeff:" << interaction_coeff[0]<<";"
       << interaction_coeff[1] << ";"<<interaction_coeff[2]<<";"
       << interaction_coeff[3] << endl;
  energy_coeff = dope_back.energy_coeff;
  add_bonded = dope_back.add_bonded;
  return 1;
}

double ScoringByDOPEBack::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(structure);
}

double ScoringByDOPEBack::evaluate(vector<RESIDUE >& structure)
{
// Calculate DOPE energy of the protein by adding up the energy among each
// the pairs of the atoms between all the residues

  double e_0 = 0, e_1 = 0, e_2 = 0, e_3 = 0, e_4 = 0, e_5 = 0, 
         e_6 = 0, e_7 = 0, e_8 = 0, e_9 = 0;
  double sheet_sheet = 0, sheet_helix = 0, helix_helix = 0, other = 0,
         total_energy = 0;
  if (interaction_coeff[0] != 0) {
    e_0 = INTenergy(structure, 0);
    e_1 = INTenergy(structure, 1);
    e_2 = INTenergy(structure, 2);
  }
  if (interaction_coeff[1] != 0) {
    e_3 = INTenergy(structure, 3);
    e_4 = INTenergy(structure, 4);
  }
  if (interaction_coeff[2] != 0) {
    e_5 = INTenergy(structure, 5);
  }
  if (interaction_coeff[3] != 0) {
    e_6 = INTenergy(structure, 6);
    e_7 = INTenergy(structure, 7);
    e_8 = INTenergy(structure, 8);
    e_9 = INTenergy(structure, 9);
  }
  sheet_sheet = interaction_coeff[0] * (e_0 + e_1 + e_2);
  sheet_helix = interaction_coeff[1] * (e_3 + e_4);
  helix_helix = interaction_coeff[2] * e_5;
  other = interaction_coeff[3] * (e_6 + e_7 + e_8 + e_9);
  total_energy = sheet_sheet + sheet_helix + helix_helix + other;
  return total_energy;
}

double ScoringByDOPEBack::INTenergy(vector<RESIDUE >& structure, int interaction)
{
  // Calculating total energy.
  double e_total = 0.0;
  for(int i=0; i<(int)structure.size()-1; i++) {
    for(int j=i+1; j<(int)structure.size(); j++) {
      double residue_energy = calcResidueDOPEBack(
                                structure[i], structure[j], interaction);
      if (residue_energy!=0.0)
        e_total += residue_energy;
    }
  }
  return energy_coeff * e_total;
}

// Calculate Dope energy between 2 residues
double ScoringByDOPEBack::calcResidueDOPEBack( 
        RESIDUE& res0, RESIDUE &res1, int interaction)
{
  double energy = 0;
  string at0, at1;
  
  int rb0 = res0.ramaBasin;
  int rb1 = res1.ramaBasin;
  for(int i=0; i<(int)res0.arrAtoms.size();i++){
    ATOM C = res1.getAtom("C");
    ATOM CA = res0.getAtom("CA");
    if (C.x==0 && C.y==0 && C.z==0)
      continue;
    at0 = res0.arrAtoms[i].name;

    for(int j=0; j<(int)res1.arrAtoms.size();j++){
      ATOM C = res1.getAtom("C");
      ATOM CA = res1.getAtom("CA");
      if (C.x==0 && C.y==0 && C.z==0)
        continue;
      at1 = res1.arrAtoms[j].name;

      double dist= BBQ::dist(res0.arrAtoms[i], res1.arrAtoms[j]);
      if ( dist<=0.0) {
        continue;
      }
      if (dist>=MAX_RESIDUE_DISTANCE)
        return energy;
      int cd = abs(res0.seq-res1.seq);
      if ( add_bonded && dope_back.BondedAtoms(res0.seq, at0, res1.seq, at1, cd))
        continue;
      energy+= dope_back.PairEnergy(res0.name, rb0, at0, res1.name, rb1,
                                    at1, cd, dist, interaction);
    }
  }       
  return energy;
}

/****************** ScoringByDopeX *******************************/
ScoringByDopeX::ScoringByDopeX(){
  mBurialPenalty = 0;
  mName = "dopeX";
}

ScoringByDopeX::~ScoringByDopeX(){
}

// This function is necessary for initialization, 'coz DOPE need to read
// the dope.par for computation 
int ScoringByDopeX::Init(string dat_file1)
{
  dopeX.Init(dat_file1);
  Burial_renorm = dopeX.Burial_renorm;
  Burial_min_atcount = dopeX.Burial_min_atcount;
  Burial_max_atcount = dopeX.Burial_max_atcount;
  Burial_sphere_rad = dopeX.Burial_sphere_rad;
  Penalty_coeff = dopeX.Penalty_coeff;
  CB_CB_focus=dopeX.CB_CB_focus;
  min_chain_dist=dopeX.min_chain_dist;
  max_chain_dist = dopeX.max_chain_dist;
  min_CB_CB_dist=dopeX.min_CB_CB_dist;
  CB_CB_coefficient=dopeX.CB_CB_coefficient;
  energy_coeff = dopeX.energy_coeff;
  return 1;
}

double ScoringByDopeX::CalculateBurial(string& sequence, vector<RESIDUE >& structure)
{
  //cerr << sequence << endl;
  int at_count, at_count_hb = 0, ChainDist;
  int max_at_count = Burial_min_atcount + Burial_max_atcount;
  memset(dope_vec, 0, sizeof(int)*(2000*2000));
  memset(pap_vec, 0, sizeof(int)*(2000*2000));

  double dist, Penalty=0, CA_CA_dist, ESP_angle, sheet_angle;
  for(int aa1=0; aa1<(int)structure.size(); aa1++) {
    at_count = 0;
    at_count_hb = 0;

    RESIDUE res1=structure[aa1];
    for(int i=0; i<(int)res1.arrAtoms.size();i++) {
      string at1 = res1.arrAtoms[i].name;
      if (at1=="CB") {
        ATOM CB1 = res1.arrAtoms[i];
        ATOM CA1 = res1.getAtom("CA");
        ATOM C1 = res1.getAtom("C");
        ATOM N1 = res1.getAtom("N");
        if ((C1.x==0 && C1.y==0 && C1.z==0) || (N1.x==0 && N1.y==0 && N1.z==0))
          continue;

        Vector3 N1C1(getPosition(C1)-getPosition(N1));
        Vector3 A1B1(getPosition(CB1)-getPosition(CA1));

        for(int aa2=0; aa2<(int)structure.size(); aa2++) {
          ChainDist = abs(aa1-aa2);
          if (ChainDist < min_CB_CB_dist)
            continue;
          RESIDUE res2=structure[aa2];
          ATOM CB2 = res2.getAtom("CB");// string at2 = res2.arrAtoms[j].name;
          ATOM CA2 = res2.getAtom("CA");
          ATOM C2 = res2.getAtom("C");
          ATOM N2 = res2.getAtom("N");
          if ((CB2.x==0 && CB2.y==0 && CB2.z==0) || (N2.x==0 && N2.y==0 && N2.z==0))
            continue;
          CA_CA_dist = BBQ::dist(CA1,CA2);
          Vector3 A2B2(getPosition(CB2)-getPosition(CA2));
          Vector3 N2C2(getPosition(C2)-getPosition(N2));

          string at2 = CB2.name;
          if (aa1!=aa2 && at2=="CB") {
            dist = BBQ::dist(CB1, CB2); // Calculates the distance.

            sheet_angle = N1C1.getAngle(N2C2)*180/PI;
            ESP_angle = A1B1.getAngle(A2B2)*180/PI;
            if ((dist <= Burial_sphere_rad) && ((ESP_angle < 90.00)))
            {
              if (dopeX.ESP(sequence[aa2], 1) == 1) 
                at_count+= dopeX.ESP(sequence[aa2], 2);
              dope_vec[aa1*2000+aa2] = 1;
            }
            if ((dist <= Burial_sphere_rad) && ((ESP_angle >= 90.00)
                                            && (CA_CA_dist - dist > 0.0))) 
            {
              dope_vec[aa1*2000+aa2] = 2;
            }
            if (sheet_angle < 60.0) pap_vec[aa1*2000+aa2] = 1;
            if (sheet_angle > 120.0) pap_vec[aa1*2000+aa2] = 2;

            if (max_at_count <= at_count) break;
          }
        
          if (at_count > dopeX.ESP(sequence[aa1],0)) 
            Penalty += 1.0;
        }
      }
      if (at1=="O" || at1 == "N") {
        ATOM CB1 = res1.getAtom("CB");
        ATOM C1 = res1.getAtom("C");
        ATOM O1 = res1.getAtom("O");
        ATOM H1 = res1.getAtom("HN");
        ATOM N1 = res1.getAtom("N");
        if ((H1.name==""&&H1.x==0 && H1.y==0 && H1.z==0)
             || (N1.name=="" && N1.x==0 && N1.y==0 && N1.z==0))
          continue;
        Vector3 CO1(getPosition(O1)-getPosition(C1));
        Vector3 NH1(getPosition(H1)-getPosition(N1));

        // Counting how many atoms surround atom1.
        for(int aa2=0; aa2<(int)structure.size(); aa2++) {
          ChainDist = abs(aa1-aa2);
          if (ChainDist <= min_CB_CB_dist)
            continue;

          RESIDUE res2=structure[aa2];
          ATOM CA2 = res2.getAtom("CA");
          ATOM CB2 = res2.getAtom("CB");
          string at2 = CB2.name;
          if (aa1!=aa2 && at2=="CB") {
            double HB_CA_dist = BBQ::dist(res1.arrAtoms[i],CA2);
            dist = BBQ::dist(CB1, CB2); // Calculates the distance.
            Vector3 A2B2(getPosition(CB2)-getPosition(CA2));

            if (at1=="O") 
              ESP_angle = CO1.getAngle(A2B2)*180/PI;
            else
              ESP_angle = NH1.getAngle(A2B2)*180/PI;
        
            dist = BBQ::dist(res1.arrAtoms[i], CB2); // Calculates the distance.
            if (dist<=Burial_sphere_rad && dist-HB_CA_dist<0.0
                                        && (ESP_angle<60.0||ESP_angle>120))
            {
              at_count_hb += dopeX.ESP(sequence[aa2], 2);
            }
            //maximum count reached, no need to keep counting.
            if (max_at_count<=at_count_hb) break;
          }
        }
        // maximum count reached, no need to keep counting
        if (max_at_count <= at_count) break; 
      }
    }
    if (at_count_hb > 14) {
      Penalty += 1.0;
    }
  }
  return Penalty;
}

double ScoringByDopeX::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  double energy = 0.0;
  double penalty = CalculateBurial(sequence, structure);
  for(int i=0; i<(int)structure.size()-1; i++)
  {
    RESIDUE res1 = structure[i];
    double residue_energy = 0.0;
    for(int j=i+1; j<(int)structure.size(); j++)
    {
      int cd = abs(i-j);
      if (cd<min_chain_dist || cd > max_chain_dist) continue;
 
      RESIDUE &res2 = structure[j];

      string atom1, atom2;
      for(int at1=0; at1<(int)res1.arrAtoms.size();at1++){
        atom1 = res1.arrAtoms[at1].name;

        for(int at2=0; at2<(int)res2.arrAtoms.size();at2++){
          atom2 = res2.arrAtoms[at2].name;
          if (atom1=="HN" || atom2=="HN" || atom1=="SG" || atom2=="SG")
            continue;
          double dist= BBQ::dist(res1.arrAtoms[at1], res2.arrAtoms[at2]);

          int od = dope_vec[i*2000+j];
          int pap = pap_vec[i*2000+j];
          int ss1 = DOPE::SSNCode(ss[i]); //structure[i].getSecondaryStructure();
          int ss1_1 = DOPE::SSNCode(ss[i+1]); //structure[i+1].getSecondaryStructure();
          int ss1_2 = -1;
          if (i<(int)structure.size()-2)
            ss1_2 = DOPE::SSNCode(ss[i+2]); //structure[i+2].getSecondaryStructure();
          int ss1_3 = -1;
          if (i<(int)structure.size()-3)
            ss1_3 = DOPE::SSNCode(ss[i+3]); //structure[i+3].getSecondaryStructure();
          int ss2 = DOPE::SSNCode(ss[j]); //structure[j].getSecondaryStructure();

          double e_pair=dopeX.calcAtomDOPE(
                           sequence.substr(i,1), atom1, 
                           sequence.substr(j,1), atom2,
                           dist, cd, od, pap, ss1, ss1_1, ss1_2, ss1_3, ss2);
          bool add_energy = false;
          if (CB_CB_focus) {
            if ((cd >= min_CB_CB_dist) || (e_pair > 0.00)) add_energy = true;
            if ((atom1=="CB" && atom2=="CB" && cd >= 5 
                             && (e_pair > 1.00 || e_pair < 0.00)) 
                || e_pair > 3.00)
              add_energy = true;
            else 
              add_energy = false;
          } else
            add_energy = true;

          if (add_energy && e_pair < 0.00 && CB_CB_focus 
                && atom1 == "CB" && atom2 == "CB" && cd >= min_CB_CB_dist 
                && dope_vec[i*2000+j] == 1 && dopeX.ESP(sequence[i], 1)== 1 
                && dopeX.ESP(sequence[j], 1) == 1)
          {
            e_pair = e_pair * CB_CB_coefficient;
          }
          if (dist >= 15.0) add_energy = false;
          if (add_energy) 
            residue_energy += e_pair;
        }
      }
    }
    energy +=residue_energy;
  }
  if (0.0 < Penalty_coeff) 
    mBurialPenalty = penalty;

  return energy_coeff * energy;
}

/****************** ScoringByDopePW *******************************/
ScoringByDopePW::ScoringByDopePW(){
  mName = "dopePW";
  mHBEnergy = 0;
  mBurialPenalty=0;
}

ScoringByDopePW::~ScoringByDopePW(){
  dpw_ac.finish_dopepw();
}

int ScoringByDopePW::Init(string dat_file1)
{
  estimated_rg = 1000;
  dpw_ac.init_dopepw(dat_file1.c_str());
  return 1;
}

double ScoringByDopePW::CalculateBurial(
        string& sequence, string ss, vector<RESIDUE >& prot)
{
  int ChainDist;
  double dist, Penalty=0.0, sheet_angle, prop_wash_angle1, 
         prop_wash_angle2, prop_wash;
  int ii, jj;
  for (ii = 0; ii < (int)prot.size(); ii++)
    for (jj = 0; jj < (int)prot.size(); jj++) {
        dopePW.dope_vec[ii][jj] = 0;
        dopePW.pap_vec[ii][jj] = 0;
        dopePW.ON_hbond_vec[ii][jj] = 0;
        dopePW.NO_hbond_vec[ii][jj] = 0;
    }
    
  //Fill out the continuity vector
  int cont_flag = 0;
  dopePW.continuity_vec.assign(prot.size(), 0);
  int ss0 = DOPE::SSNCode(ss[0]); //prot[0].getSecondaryStructure());
  if (ss0 > 1) ss0 = 2;
  for (ii = 1; ii < (int)prot.size(); ii++) {
    int ss1 = DOPE::SSNCode(ss[ii]);//prot[ii].getSecondaryStructure());
    if (ss1 > 1) ss1 = 2;
    if (ss1 != ss0) cont_flag++;
    dopePW.continuity_vec[ii] = cont_flag;
    ss0 = ss1;
  }
  for(int aa1=0; aa1<(int)prot.size(); aa1++) {
    RESIDUE res1=prot[aa1];
    ATOM CA1 = res1.getAtom("CA");
    ATOM O1 = res1.getAtom("O");
    ATOM N1 = res1.getAtom("N");
    ATOM C1 = res1.getAtom("C");
    if ((C1.x==0 && C1.y==0 && C1.z==0) || (O1.x==0 && O1.y==0 && O1.z==0)
                                        || (N1.x==0 && N1.y==0 && N1.z==0))
      continue;

    // Calculate for Ca1
    for(int aa2=0; aa2<(int)prot.size(); aa2++) {
      ChainDist = abs(aa1-aa2);
      if (ChainDist < min_CB_CB_dist)
        continue;
      RESIDUE res2=prot[aa2];
      ATOM CA2 = res2.getAtom("CA");
      ATOM N2 = res2.getAtom("N");
      ATOM C2 = res2.getAtom("C");
      if ((C2.x==0 && C2.y==0 && C2.z==0) || (N2.x==0 && N2.y==0 && N2.z==0))
        continue;

      // Calculate sheet angle to get pap_vec
      //Vector Angle calculations
      Vector3 N_C_1(getPosition(C1)-getPosition(N1));
      Vector3 N_C_2(getPosition(C2)-getPosition(N2));
      sheet_angle = N_C_1.getAngle(N_C_2)*180/PI;
      if (sheet_angle < 90.0) dopePW.pap_vec[aa1][aa2] = 1;
      else dopePW.pap_vec[aa1][aa2] = 2;

      // Now calculate prop wash
      Vector3 CA1_CA2(getPosition(CA2)-getPosition(CA1));
      Vector3 CA2_CA1(getPosition(CA1)-getPosition(CA2));
      if (res1.name!= "GLY" && res2.name!= "GLY") {
        ATOM CB1=res1.getAtom("CB");
        Vector3 CA_CB_1 = (getPosition(CB1)-getPosition(CA1));;
        ATOM CB2=res2.getAtom("CB");
        Vector3 CA_CB_2 = (getPosition(CB2)-getPosition(CA2));;
      
        prop_wash_angle1 = CA1_CA2.getAngle(CA_CB_1)*180/PI;
        prop_wash_angle2 = CA2_CA1.getAngle(CA_CB_2)*180/PI;

        //Calculating normalized PropWash
        prop_wash = sqrt((prop_wash_angle1 - 90.0) * (prop_wash_angle1 - 90.0) 
                       + (prop_wash_angle2 - 90.0) * (prop_wash_angle2 - 90.0));

        //Calculating CB-CB distances
        dist = BBQ::dist(CB2,CB1);

        //Assigning prop wash bins
        if (dist <= Burial_sphere_rad) {
          if (prop_wash>0.00 && prop_wash<=40.00)
            dopePW.dope_vec[aa1][aa2] = 0;
          else if (prop_wash>40.00 && prop_wash<=70.00)
            dopePW.dope_vec[aa1][aa2] = 1;
          else if (prop_wash>70.00)
            dopePW.dope_vec[aa1][aa2] = 2;
        }
      }
    }
         
    //calculate for O1
    for(int aa2=0; aa2<(int)prot.size(); aa2++) {
      ChainDist = abs(aa1-aa2);
      RESIDUE res2=prot[aa2];
      ATOM N2 = res2.getAtom("N");
      if (N2.x==0 && N2.y==0 && N2.z==0)
        continue;
      dist = BBQ::dist(N2,O1);
      if (dist < 3.2) {
        dopePW.ON_hbond_vec[aa1][aa2] = 1;
        dopePW.NO_hbond_vec[aa2][aa1] = 1;
      }
    }
  }
  mBurialPenalty = Penalty;
  Penalty = 0.0;
  return Penalty;
}

// ss is not actually used in this function, 'coz we use the secondary
// structure calculated from structure.
double ScoringByDopePW::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& prot, int start)
{
  double energy, dif;
  struct timeval ts, te;
  //gettimeofday(&ts, 0);

  MassCenter(prot);
  radial(prot); //RG+RU
  burial_ratio(prot); //BR
  if (rg>estimated_rg) return 100.0 * br * ru * rg;

  dpw_ac.set_system_for_dopepw(NULL, &prot, sequence);
  //gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
  //cerr << "set_system_for_dopepw: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000 << " miliseconds." << endl;
  //gettimeofday(&ts, 0);

  dpw_ac.pre_dopepw_calc();
  mHBEnergy = dpw_ac._num_HB;
  //gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
  //cerr << "pre_dopepw_calc: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000 << " miliseconds." << endl;
  //gettimeofday(&ts, 0); 
  dpw_ac.calc_dopepw_energy(&energy);
  //gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
  //cerr << "calc_dopepw_energy: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000 << " miliseconds." << endl;
  //gettimeofday(&ts, 0);
      
  return energy;

  int ss0, ss1;
  double e_total=0, e_pair, penalty = CalculateBurial(sequence, ss, prot);

  // Calculating total energy.
  for(int i=0; i<(int)prot.size()-1; i++) {
    RESIDUE res0 = prot[i];
        
    ss0 = DOPE::SSNCode(ss[i]); //prot[i].getSecondaryStructure();
    for(int j=i+1; j<(int)prot.size(); j++) {
      int cd = abs(i-j);
      if (cd<min_chain_dist || cd>max_chain_dist) continue;
 
      RESIDUE res1 = prot[j];
      ss1 = DOPE::SSNCode(ss[j]);//prot[j].getSecondaryStructure();
      bool EE = (ss0==1 && ss1==1);
      bool continuous = (dopePW.continuity_vec[i]==dopePW.continuity_vec[j]);

      string atom0, atom1;
      for(int at0=0; at0<(int)res0.arrAtoms.size();at0++){
        atom0 = res0.arrAtoms[at0].name;

        for(int at1=0; at1<(int)res1.arrAtoms.size();at1++){
          atom1 = res1.arrAtoms[at1].name;
          if (atom1=="HN" || atom0=="HN" || atom1=="SG" || atom0=="SG")
            continue;
          double dist= BBQ::dist(res1.arrAtoms[at1], res0.arrAtoms[at0]);
          if (add_bonded || !dopePW.BondedAtoms(i, atom0, j, atom1, cd)) {
            string aa0=sequence.substr(i,1);
            string aa1=sequence.substr(j,1);
            if (continuous || dopePW.dope_vec[i][j]<1
                           || (dopePW.dope_vec[i][j]==1 && EE))
              e_pair=dopePW.DOPEenergy(
                        aa0,i,atom0, aa1,j,atom1,dist, ss0,ss1, prot.size());
            else 
              e_pair=dopePW.PropWashEnergy(aa0,i,atom0,aa1,j,atom1,dist);
            e_total += e_pair;
          }
        }
      }
    }
  }
  e_total += penalty;
  return energy_coeff * e_total;
}

/****************** ScoringByDopePW *******************************/
ScoringByDopePW_RAMA::ScoringByDopePW_RAMA(){
  mName = "dopePW_RAMA";
  mBurialPenalty=0;
}

ScoringByDopePW_RAMA::~ScoringByDopePW_RAMA(){
}

int ScoringByDopePW_RAMA::Init(string dat_file1)
{
  dp.Init(dat_file1);
  add_bonded = dp.add_bonded;
  Burial_sphere_rad = dp.Burial_sphere_rad;
  Penalty_coeff = dp.Penalty_coeff;
  min_chain_dist=dp.min_chain_dist;
  max_chain_dist = dp.max_chain_dist;
  min_CB_CB_dist=dp.min_CB_CB_dist;
  CB_CB_coefficient=dp.CB_CB_coefficient;
  energy_coeff = dp.energy_coeff;
  return 1;
}


void ScoringByDopePW_RAMA::SetPropWash(vector<RESIDUE >& prot)
{
  bool has_CB0, has_CB1, has_HN0, has_HN1,
       peptideNC0, peptideNC1, peptideCN0, peptideCN1;
  ATOM N_0, CA_0, C_0, O_0, CB_0, N_1, CA_1, C_1, O_1, CB_1, HN_0, HN_1,
       Naa0p1_0, Caa0m1_0, Caa1m1_1, Naa1p1_1;
  Vector3 CA_CB_0, CA_CB_1, N_C_0, N_C_1, CA0_CA1, CA1_CA0, C_O_0,
          C_O_1, C0_C1, C1_C0, N_H_0, N_H_1, C0_N1, N1_C0, N0_C1, C1_N0,
          N0_N1, N1_N0, peptide_vectorNC0, peptide_vectorNC1,
          peptide_vectorCN0, peptide_vectorCN1;
  double dist, sheet_angle, prop_wash0, prop_wash1, prop_wash, CO0_C01_angle,
         CO1_C10_angle, COCO_propwash, CO_CO_angle, phi, psi, omega,
         HN0_CO1_angle, CO0_HN1_angle, CO0_CN01_angle, NH1_NC10_angle,
         OH_propwash, NH0_NC01_angle, CO1_CN10_angle, HO_propwash,
         NH0_NN01_angle=0, NH1_NN10_angle, NHNH_propwash, NH_NH_angle,
         CN0_N1_peptide_angle, NC1_C0_peptide_angle, NC0_C1_peptide_angle,
         CN1_N0_peptide_angle;
  int aa0, aa1, seq_length = prot.size(), helix_length = 0, helix_iter;

  for (aa0 = 0; aa0 < seq_length; aa0++) {
    dp.SecStr_vec[aa0] = 2;

    RESIDUE res0 = prot[aa0];
    phi = res0.phi;
    psi = res0.psi;
    if ((phi<-40.0 && phi>-90.0 && psi<-20.0 && psi>-60.0) 
         || (dp.OH_hbond_vec[aa0-4][aa0]==1))
    {
      helix_length++;
    } else {
      if (helix_length > 4) {
        for (helix_iter = aa0 - helix_length; helix_iter < aa0; helix_iter++) 
          dp.SecStr_vec[helix_iter] = 0;
      }
      helix_length = 0;
    }
    if (aa0 == seq_length - 1 && helix_length > 4) 
      for (helix_iter = aa0 - helix_length; helix_iter < aa0; helix_iter++) 
        dp.SecStr_vec[helix_iter] = 0;
    
    N_0 = res0.getAtom("N");
    CA_0 = res0.getAtom("CA");
    C_0 = res0.getAtom("C");
    O_0 = res0.getAtom("O");
    N_C_0=getPosition(N_0)-getPosition(C_0);
    C_O_0=getPosition(C_0)-getPosition(O_0);
    
    peptideCN0 = false;
    if (aa0 + 1 < seq_length) {
      Naa0p1_0 = prot[aa0 + 1].getAtom("N");
      peptide_vectorCN0 = getPosition(C_0)-getPosition(Naa0p1_0);
      peptideCN0 = true;
    }
    
    peptideNC0 = false;
    if (aa0 - 1 > 0) {
      Caa0m1_0 = prot[aa0 - 1].getAtom("C");
      peptide_vectorNC0 = getPosition(N_0)-getPosition(Caa0m1_0);
      peptideNC0 = true;
    }
    
    has_CB0 = false;
    CB_0= prot[aa0].getAtom("CB");
    if (CB_0.name.length()> 0) {
      CA_CB_0= getPosition(CA_0)-getPosition(CB_0);
      has_CB0 = true;
    }
    has_HN0 = false;
    HN_0 = prot[aa0].getAtom("HN");
    if (HN_0.name.length()> 0) {
      N_H_0= getPosition(N_0)-getPosition(HN_0);
      has_HN0 = true;
    } else {
      if (N_H_0.x==0 && N_H_0.y==0 && N_H_0.z==0)
        N_H_0.y=1.4013e-45;
    }
    
    for (aa1 = aa0 + 2; aa1 < seq_length; aa1++) {
      RESIDUE res1 = prot[aa1];
      dp.pw_vec[aa0][aa1] = 0;
      dp.pap_vec[aa0][aa1] = 0;
      dp.EE_vec[aa0][aa1] = 0;
      dp.P_align[aa0][aa1] = 0;
      dp.AP_align[aa0][aa1] = 0;
      dp.HO_hbond_vec[aa0][aa1] = 0;
      dp.OH_hbond_vec[aa0][aa1] = 0;
      dp.COCO_align[aa0][aa1] = 0;
      dp.NHNH_align[aa0][aa1] = 0;
      dp.NHCO_align[aa0][aa1] = 0;
      dp.CONH_align[aa0][aa1] = 0;
    
      N_1 = res1.getAtom("N");
      CA_1 = res1.getAtom("CA");
      C_1 = res1.getAtom("C");
      O_1 = res1.getAtom("O");
    
      has_CB1 = false;
      CB_1 = res1.getAtom("CB");
      if (CB_1.name.length()> 0) {
        CA_CB_1=getPosition(CA_1)-getPosition(CB_1);
        has_CB1 = true;
      }
      has_HN1 = false;
      HN_1 = res1.getAtom("HN");
      if (HN_1.name.length()> 0) {
        N_H_1=getPosition(N_1)-getPosition(HN_1);
        has_HN1 = true;
      }
    
      if (has_HN1) { 
        dist = BBQ::dist(O_0,HN_1);
        if (dist < 2.6 && dist > 1.7) 
          dp.OH_hbond_vec[aa0][aa1] = 1;
      }
      if (has_HN0) {
        dist = BBQ::dist(O_1,HN_0);
        if (dist < 2.6 && dist > 1.7) 
          dp.HO_hbond_vec[aa0][aa1] = 1;
      }
    
      // Determining if beta sheets are parallel or anti-parallel
      N_C_1=getPosition(N_1)-getPosition(C_1);
      sheet_angle = N_C_1.getAngle(N_C_0)*180/PI;
      if (sheet_angle < 90.0) dp.pap_vec[aa0][aa1] = 1;
      if (sheet_angle >= 90.0) dp.pap_vec[aa0][aa1] = 2;
    
      // Determining the orientation of hydrogen-bond donors and acceptors
      C_O_1=getPosition(C_1)-getPosition(O_1);
      C0_C1=getPosition(C_0)-getPosition(C_1);
      C1_C0=getPosition(C_1)-getPosition(C_0);
      CO0_C01_angle = C_O_0.getAngle(C0_C1)*180/PI;
      CO1_C10_angle = C_O_1.getAngle(C1_C0)*180/PI;
      COCO_propwash = sqrt((CO0_C01_angle - 90.0) * (CO0_C01_angle - 90.0)
                         + (CO1_C10_angle - 90.0) * (CO1_C10_angle - 90.0));
      CO_CO_angle = C_O_0.getAngle(C_O_1)*180/PI;
    
      cerr << "1"<<endl;
      C0_N1=getPosition(C_0)-getPosition(N_1);
      N1_C0=getPosition(N_1)-getPosition(C_0);
      CO0_CN01_angle = C_O_0.getAngle(C0_N1)*180/PI;
      NH1_NC10_angle = N_H_1.getAngle(N1_C0)*180/PI;
      OH_propwash = sqrt((CO0_CN01_angle - 90.0) * (CO0_CN01_angle - 90.0)
                       + (NH1_NC10_angle - 90.0) * (NH1_NC10_angle - 90.0));
    
      cerr << "2"<<endl;
      N0_C1=getPosition(N_0)-getPosition(C_1);
      C1_N0=getPosition(C_1)-getPosition(N_0);
      {
        NH0_NC01_angle = N_H_0.getAngle(N0_C1)*180/PI;
        CO1_CN10_angle = C_O_1.getAngle(C1_N0)*180/PI;
        HO_propwash = sqrt((NH0_NC01_angle - 90.0) * (NH0_NC01_angle - 90.0)
                         + (CO1_CN10_angle - 90.0) * (CO1_CN10_angle - 90.0));
      }
    
      N0_N1=getPosition(N_0)-getPosition(N_1);
      N1_N0=getPosition(N_1)-getPosition(N_0);
      if (has_HN0)
        NH0_NN01_angle = N_H_0.getAngle(N0_N1)*180/PI;
      NH1_NN10_angle = N_H_1.getAngle(N1_N0)*180/PI;
      NHNH_propwash = sqrt((NH0_NN01_angle - 90.0) * (NH0_NN01_angle - 90.0)
                         + (NH1_NN10_angle - 90.0) * (NH1_NN10_angle - 90.0));
      {
        HN0_CO1_angle = N_H_0.getAngle(C_O_1)*180/PI;
        NH_NH_angle = N_H_0.getAngle(N_H_1)*180/PI;
      }
      CO0_HN1_angle = C_O_0.getAngle(N_H_1)*180/PI;
    
      peptideNC1 = false;
      if (aa1 - 1 > 0) {
        Caa1m1_1 = prot[aa1 - 1].getAtom("C");
        peptide_vectorNC1=getPosition(N_1)-getPosition(Caa1m1_1);
        peptideNC1 = true;
      }
      peptideCN1 = false;
      if (aa1 + 1 < seq_length) {
        Naa1p1_1 = prot[aa1 + 1].getAtom("N");
        peptide_vectorCN1=getPosition(C_1)-getPosition(Naa1p1_1);
        peptideCN1 = true;
      }
    
      if (peptideCN0 && peptideNC1) {
        CN0_N1_peptide_angle = peptide_vectorCN0.getAngle(C0_N1)*180/PI;
        NC1_C0_peptide_angle = peptide_vectorNC1.getAngle(N1_C0)*180/PI;
      }
      if (peptideNC0 && peptideCN1) {
        NC0_C1_peptide_angle = peptide_vectorNC0.getAngle(N0_C1)*180/PI;
        CN1_N0_peptide_angle = peptide_vectorCN1.getAngle(C1_N0)*180/PI;
      }
    
      if (COCO_propwash > 90.0 && CO_CO_angle < 30.0 && NHNH_propwash > 90.0
                               && NH_NH_angle < 30.0) 
        dp.P_align[aa0][aa1] = 1;
      if (HO_propwash > 90.0 && CO0_HN1_angle > 140.0 && OH_propwash > 90.0
                             && HN0_CO1_angle > 140.0) 
        dp.AP_align[aa0][aa1] = 1;
      if (COCO_propwash > 90.0 && CO_CO_angle < 30.0) 
        dp.COCO_align[aa0][aa1] = 1;
      if (NHNH_propwash > 90.0 && NH_NH_angle < 30.0) 
        dp.NHNH_align[aa0][aa1] = 1;
      if (HO_propwash > 90.0 && HN0_CO1_angle > 140.0) 
        dp.NHCO_align[aa0][aa1] = 1;
      if (OH_propwash > 90.0 && CO0_HN1_angle > 140.0) 
        dp.CONH_align[aa0][aa1] = 1;
    
      CA0_CA1=getPosition(CA_0)-getPosition(CA_1);
      CA1_CA0=getPosition(CA_1)-getPosition(CA_0);
    
      if (has_CB0 && has_CB1) {
        prop_wash0 = CA_CB_0.getAngle(CA0_CA1)*180/PI;
        prop_wash1 = CA_CB_1.getAngle(CA1_CA0)*180/PI;
        prop_wash = sqrt((prop_wash0 - 90.0) * (prop_wash0 - 90.0)
                       + (prop_wash1 - 90.0) * (prop_wash1 - 90.0));
    
        if ((prop_wash > 0.00) && (prop_wash <= 40.00)) 
          dp.pw_vec[aa0][aa1] = 0;
        else if ((prop_wash > 40.00) && (prop_wash <= 70.00)) 
          dp.pw_vec[aa0][aa1] = 1;
        else if (prop_wash > 70.00) 
          dp.pw_vec[aa0][aa1] = 2;
      }
    }
  }
}

double ScoringByDopePW_RAMA::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& prot, int start)
{
  double e_total, e_pair, dist;
  bool CB, continuous, EE, HN0_HN1;
  int aa0, aa1, at0, at1, cd, seq_length = prot.size(), aat0, aat1, id0, id1;

  SetPropWash(prot);
  for (aa0 = 0; aa0 < seq_length; aa0++) cerr << dp.SecStr_vec[aa0];
  cerr << endl; 

  dp.FindBetaStrands(seq_length);
  for (aa0 = 0; aa0 < seq_length; aa0++) cerr << dp.SecStr_vec[aa0];
  cerr << endl; //exit(0);
  
  dp.SetContinuity(seq_length);

  // Calculating total energy.
  e_total = 0;
  for (aa0 = 0; aa0 < seq_length; aa0++) {
    RESIDUE res0 = prot[aa0];
    for(at0=0; at0<(int)res0.arrAtoms.size();at0++) {
      string atom0 = res0.arrAtoms[at0].name;
      for (aa1 = aa0 + 2; aa1 < seq_length; aa1++) {
        RESIDUE res1 = prot[aa1];
        for (at1 = 0; at1 < (int)res1.arrAtoms.size(); at1++) {
          ATOM atom=res1.arrAtoms[at1];
          string atom1 = atom.name;
          cd = abs(aa0-aa1);
          dist= BBQ::dist(res1.arrAtoms[at1], res0.arrAtoms[at0]);
          
          CB = (atom0 == "CB" || atom1 == "CB");
          continuous = (dp.continuity_vec[aa0] == dp.continuity_vec[aa1]);
          HN0_HN1 = (atom0 == "HN" || atom1 == "HN");
          string rname0=sequence.substr(aa0,1);
          string rname1=sequence.substr(aa1,1);
          if (dist >= 30.0 || HN0_HN1) 
            e_pair = 0;
          else if (!continuous && dp.pw_vec[aa0][aa1] > 0 && dp.EE_vec[aa0][aa1] == 0) 
            e_pair = dp.PropWashEnergy(
                   rname0, atom0, rname1, atom1, dist, CB, cd, dp.pw_vec[aa0][aa1]);
          else if (dist >= 15.0)
            e_pair = 0;
          else if (!continuous) {
            if (dp.EE_vec[aa0][aa1] > 0) 
              e_pair = dp.DOPE_PW_strand(
                   rname0, atom0, rname1, atom1, dist, dp.EE_vec[aa0][aa1]);
            else 
              e_pair = dp.DOPE_PW_0(rname0, atom0, rname1, atom1, dist, CB, cd);
          } else if (continuous && dist < 10.0) 
            e_pair = dp.DOPE_PW_continuous(
                   rname0, atom0, rname1, atom1, dist, dp.SecStr_vec[aa0]);
          else e_pair = 0;
          
          e_total += e_pair;
        }
      }
    }
    cerr << "e_total=" << e_total << endl;
  }
  cerr << endl;

  return energy_coeff * e_total;
}

/****************** ScoringByBMKhbond****************************/

ScoringByBMKHBBond::ScoringByBMKHBBond()
{
  Min_delta = 1.70;
  Max_delta = 2.60;
  MinChainDist = 5;
  mName = "BMK";
  mHBEnergy = 0;
  mBurialPenalty = 0;
  CO_burial= NULL;
  NH_burial= NULL;
}

ScoringByBMKHBBond::~ScoringByBMKHBBond()
{
    if (CO_burial) delete CO_burial;
    if (NH_burial) delete NH_burial;
}

// This function is necessary for initialization, 'coz BMK need to read
// the dope.par for computation 
int ScoringByBMKHBBond::Init(string dat_file1, int* data)
{
  bmk.Init(dat_file1.c_str());
  HBond_Min_Energy = bmk.HBond_Min_Energy;
  Burial_min_atcount = bmk.Burial_min_atcount;
  Burial_max_atcount = bmk.Burial_max_atcount;
  Burial_sphere_rad = bmk.Burial_sphere_rad;
  Penalty_coeff = bmk.Penalty_coeff;
  Energy_coeff = bmk.Energy_coeff;
  Min_delta = bmk.Min_delta;
  Max_delta = bmk.Max_delta;
  MinChainDist = bmk.MinChainDist;
  memcpy(mConfidence, data, 4096*sizeof(int));
  return 1;
}

// Calculate Dope energy between 2 residues
double ScoringByBMKHBBond::calcResidueBMKHBBond( 
        RESIDUE& res1, RESIDUE &res2, bool calculate_hbond, int ss1, int ss2)
{
  // ChainDist is the number of residues between two residues in the sequence
  int ChainDist = abs(res1.seq-res2.seq); 
  if (MinChainDist >= ChainDist)
    return 0;
  double energy = 0;
  //ATOM O = res1.getAtom("O"); // Backbone CO found.
  ATOM O = res1.getAtom(2); // Backbone CO found.
  //ATOM H2 = res2.getAtom("HN");
  ATOM H2 = res2.getAtom(5);
  //if (H2.name=="") {
  if (H2.name_idx==-1) {
    return 0;
  }
  double delta= BBQ::dist(O, H2);
  if ( delta<Min_delta || delta>Max_delta) return energy;

  // Getting bonds (assuming that there are not missing atoms!).
  //ATOM N2 = res2.getAtom("N");
  ATOM N2 = res2.getAtom(3);
  //ATOM C = res1.getAtom("C");
  ATOM C = res1.getAtom(4);
  //ATOM CA = res1.getAtom("CA");
  ATOM CA = res1.getAtom(0);
  if ((CA.x==0 && CA.y==0 && CA.z==0) || (C.x==0 && C.y==0 && C.z==0)
                                      || (N2.x==0 && N2.y==0 && N2.z==0))
    return 0;
  Vector3 CO(getPosition(O)-getPosition(C));
  Vector3 NH(getPosition(H2)-getPosition(N2));
  Vector3 HO(getPosition(O)-getPosition(H2));
  Vector3 OH(HO*(-1));
  double theta, psi, chi;
  theta = NH.getAngle(OH)*180/PI;
  psi = HO.getAngle(CO)*180/PI;
  chi = DihedralAngle(getPosition(CA), getPosition(C), 
                      getPosition(O), getPosition(H2));
  if (calculate_hbond && (ss1==STRAND && ss2==STRAND)) {
    double co_burial = 0;//CO_burial[res1.seq];
    double nh_burial = 0;//NH_burial[res2.seq];
    energy = bmk.BMKHBPairEnergy(ss1, ss2, ChainDist, delta, theta,
                                 psi, chi, co_burial, nh_burial);
  }

  return energy;
}

double ScoringByBMKHBBond::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  double dif;
  struct timeval ts, te;
  //gettimeofday(&ts, 0);

// Calculate DOPE energy of the protein by adding up the energy among each
// the pairs of the atoms between all the residues
  // Calculating total energy.
  double TotalEnergy, energy = 0;
    CalculateBurial(structure);
  //gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
  //cerr << "    BMK_burial: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000
  //     << " miliseconds." << endl;
  //gettimeofday(&ts, 0);

  // Calculating secondary structure.
  for(int i=0; i<(int)structure.size()-1; i++) {
    for(int j=i+1; j<(int)structure.size(); j++) {
      bool calculate_hbond = false;
      // exclude low confident helix
      if (!(ss[i]=='H'&&mConfidence[i]<7) && !(ss[j]=='H'&&mConfidence[j]<7))
        calculate_hbond = true;
      if (!calculate_hbond)
        continue;
      int ss1, ss2;
      switch (ss[i]) {
      case 'H':
        ss1=HELIX;
        break;
      case 'E':
        ss1=STRAND;
        break;
      default:
        ss1=COIL;
        break;
      }
      switch (ss[j]) {
      case 'H':
        ss2=HELIX;
        break;
      case 'E':
        ss2=STRAND;
        break;
      default:
        ss2=COIL;
        break;
      }

      double residue_energy = calcResidueBMKHBBond(
                   structure[i], structure[j], calculate_hbond, ss1, ss2);
      if (residue_energy!=0.0) {
        energy += residue_energy;
      }
    }
  }
  //gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
  //cerr << "    BMK_energy: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000 
  //     << " miliseconds." << endl;
  //gettimeofday(&ts, 0);

  TotalEnergy = Energy_coeff * energy * 0.0015 * 50;
  mHBEnergy = TotalEnergy;
  double penalty = HBpenalty(structure) * 0.0015 * 50;
  mBurialPenalty = penalty;
  if (0.0 < Penalty_coeff) TotalEnergy += Penalty_coeff * penalty;
  return TotalEnergy;
}

double ScoringByBMKHBBond::HBpenalty(vector<RESIDUE >& structure)
{
  double penalty, TotalPenalty = 0;
  for(int aa1=0; aa1<(int)structure.size(); aa1++) {
    penalty = NH_burial[aa1] + CO_burial[aa1];
    TotalPenalty += penalty;
  }
  return TotalPenalty * (-0.5) * HBond_Min_Energy;
}

void ScoringByBMKHBBond::CalculateBurial(vector<RESIDUE >& structure)
{
  int bins = structure.size();
    if (NH_burial== NULL) {
      CO_burial= new double[bins+1];
      NH_burial= new double[bins+1];
    }
    memset(CO_burial, 0, sizeof(double)*(bins+1));
    memset(NH_burial, 0, sizeof(double)*(bins+1));
  vector<Vector3 > cbs;
  for (int i=0; i<(int)structure.size(); i++) {
    //ATOM CA = structure[i].getAtom("CB"); 
    ATOM CB = structure[i].getAtom(1); 
    cbs.push_back(Vector3(CB.x, CB.y, CB.z));
  } 
  int at_count;
  int max_at_count = Burial_min_atcount + Burial_max_atcount;
  double dist, burial;
  for(int aa1=0; aa1<(int)structure.size(); aa1++) {
    RESIDUE res1=structure[aa1];
    for(int i=0; i<(int)res1.arrAtoms.size();i++) {
      //if (at1=="O" || at1 == "N") {
      ATOM atom1=res1.arrAtoms[i];
      int at1 = atom1.name_idx;
      Vector3 atom1V3(atom1.x, atom1.y, atom1.z);
      if (at1==2 || at1 ==3) {
        // Counting how many atoms surround atom1.
        at_count = 0;
        for(int aa2=0; aa2<(int)structure.size(); aa2++) {
          if (aa1!=aa2){
            dist = atom1V3.getDistance(cbs[aa2]);
            if (dist <= Burial_sphere_rad) at_count++;
          }
          // We reached the maximum count, no need to keep counting.
          if (max_at_count <= at_count) break;
        }
        // Calculating burial coefficient (0 = fully exposed, 1 = fully buried).
        if (at_count <= Burial_min_atcount) 
          burial = 0.0;
        else {
          burial = min(double(1.0),
                       double(at_count - Burial_min_atcount) / Burial_max_atcount);
        }
        //if (at1 == "O") CO_burial[aa1]= burial;
        if (at1 ==2) CO_burial[aa1]= burial;
        else NH_burial[aa1]= burial;
      }
    }
  }
}

/****************** ScoringByTSP1****************************/
ScoringByTSP1::ScoringByTSP1(){
}

ScoringByTSP1::~ScoringByTSP1(){
}

int ScoringByTSP1::Init(string dat_file1)
{
  tsp1.Init(dat_file1.c_str());
  return 1;
}

double ScoringByTSP1::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(structure);
}

double ScoringByTSP1::evaluate(vector<RESIDUE >& structure)
{
  double tot_energy = 0;
  for(int aa=0; aa<(int)structure.size(); aa++) {
    double res_energy=0.0;
    RESIDUE res=structure[aa];
    ATOM at = res.getAtom("CA");
    ATOM C = res.getAtom("C");
    if ((at.x==0 && at.y==0 && at.z==0) || (C.x==0 && C.y==0 && C.z==0))
      continue;
    if (tsp1.tsp1_mode == 1)
      res_energy = tsp1(res.name,res.phi, res.psi);
    else if (aa < (int)structure.size()-1) {
        RESIDUE res1=structure[aa+1];
      if (tsp1.tsp1_mode == 2)
        res_energy = tsp1(res.name, res1.name, res.phi, res.psi);
        else if (aa>0) {
        RESIDUE res0=structure[aa-1];
        res_energy = tsp1(res0.name, res.name, res1.name, res.phi, res.psi);
      }
    }
    tot_energy += res_energy;
  }
  return tsp1.energy_coeff * tot_energy;
}

/****************** ScoringByESP****************************/
ScoringByESP::ScoringByESP(){
}

ScoringByESP::~ScoringByESP(){
}

int ScoringByESP::Init(string dat_file1)
{
  esp.Init(dat_file1);
  return 1;
}

double ScoringByESP::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(structure);
}

double ScoringByESP::evaluate(vector<RESIDUE >& structure)
{
  double tot_energy = 0;
  vector<Vector3 > g_backbone;
  for (int i=0; i<(int)structure.size(); i++) {
    ATOM CA = structure[i].getAtom("CA"); 
    g_backbone.push_back(Vector3(CA.x, CA.y, CA.z));
  } 
  double rg = CalculateRadius(g_backbone);

  for(int aa1=0; aa1<(int)structure.size()-1; aa1++) {
    RESIDUE res=structure[aa1];
    ATOM CA1 = res.getAtom("CA");
    if (CA1.x==0 && CA1.y==0 && CA1.z==0)
      continue;

    // Count the contacts around CA1
    int at_count = 0;
    for(int aa2=0; aa2<(int)structure.size(); aa2++) {
      RESIDUE res2=structure[aa2];
      ATOM CA2 = res2.getAtom("CA");
      if (CA2.x==0 && CA2.y==0 && CA2.z==0)
        continue;
      if (abs(aa1-aa2)>1 && BBQ::dist(CA1, CA2)<=8.5) // Calculates the distance.
        at_count++;
    }
    double res_energy = esp.Energy(res.name, at_count, rg);
    if (res_energy>3) {
      cerr<<" "<<res.name<<" "<<aa1<<" :: "<<at_count<<" "<<rg<<" "<<res_energy<<endl;
    }
    tot_energy += res_energy;
  }
  return tot_energy;
}

/****************** ScoringByESP****************************/
ScoringByNewESP::ScoringByNewESP(){
}

ScoringByNewESP::~ScoringByNewESP(){
}

int ScoringByNewESP::Init(string dat_file1, vector<string>& data)
{
  mName = "ESP_NEW";
  new_esp.Init(dat_file1, data);
  return 1;
}

double ScoringByNewESP::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(structure, start, 0);
}

double ScoringByNewESP::evaluate(vector<RESIDUE >& structure, int start, int end)
{
  double tot_energy = 0;
  vector<Vector3 > g_backbone;
  for (int i=0; i<(int)structure.size(); i++) {
    //ATOM CA = structure[i].getAtom("CA"); 
    ATOM CA = structure[i].getAtom(0); 
    g_backbone.push_back(Vector3(CA.x, CA.y, CA.z));
  } 

  double energy = 0;
  for(int aa1=2; aa1<(int)structure.size()-2; aa1++) {
    // Count the contacts around CA1
    int at_count = 0;
    for(int aa2=0; aa2<(int)structure.size(); aa2++) {
      double dist=g_backbone[aa1].getDistance(g_backbone[aa2]);
      if (abs(aa1-aa2)>1 && dist<=8.5) // Calculates the distance.
        at_count++;
    }
    double res_energy = new_esp.Energy(aa1, rg, at_count);
    tot_energy += res_energy;
  }
  tot_energy=tot_energy*0.0015*50;
  return tot_energy;
}

/****************** ScoringByClashes****************************/
ScoringByClashes::ScoringByClashes(){
}

ScoringByClashes::~ScoringByClashes(){
}

//a native structure shouldn't have any steric clashes
double ScoringByClashes::estimateNativeScore(string& sequence, string& ss){
  return 0;
}

//calculate the number of steric clashes in a protein, only Ca atoms considered
double ScoringByClashes::evaluate(
        string& sequence, string& ss, vector<Vector3 >& structure){
  return (double)countClashes(structure);
}

double ScoringByClashes::evaluate(string& sequence, string& ss, Protein* protein){
  return (double)countClashes(protein);
}

//check if steric clashes exist in a protein, only Ca atoms considered
bool ScoringByClashes::checkClashes(vector<Vector3 >& structure){

  for(int i=0; i<(int)structure.size();i++){
    for(int j=0; j<i-1; j++) {
      double dist=structure[i].getDistance(structure[j]);

      //find a clash
      if (dist < CLASH_DISTANCE_CUTOFF) return true;
    }
  }

  //no clash found
  return false;
}

//calculate the number of steric clashes in a protein, only Ca atoms considered
int ScoringByClashes::countClashes(vector<Vector3 >& structure){

  int numClashes=0;

  for(int i=0; i<(int)structure.size();i++){
    for(int j=0; j<i-1; j++) {
      double dist=structure[i].getDistance(structure[j]);

      //find a clash
      if (dist < CLASH_DISTANCE_CUTOFF) numClashes++;
    }
  }

  return numClashes;
}

bool ScoringByClashes::checkClashes(Protein* protein){
  vector<Vector3 > structure;
  return checkClashes(structure);
}

int ScoringByClashes::countClashes(Protein* protein){
  vector<Vector3 > structure;
  return countClashes(structure);
}

/*************************************ScoringBySS*******************************/

//since we always want to minimize the score of a structure, 
//in this class we negate all the scores

double ScoringBySS::estimateNativeScore(string& sequence, string& ss){
  return -1.0;
}

double ScoringBySS::evaluate(
        string& sequence, string& ss, Protein* protein){
  vector<Vector3 > structure;
  return evaluate(sequence, ss, structure);
}

// calculate the consistency between ss and the secondary structure of the
// structural model *structure* the ss of the structural model is calculated
// by a software PSEA
double ScoringBySS::evaluate(
        string& sequence, string& ss, vector<Vector3 >& structure){
/*

  //convert structure to double* format
  double* structure_=new double[structure.size()*3];
  for(int i=0; i<structure.size(); i++){
    structure_[3*i+0]=structure[i][0];
    structure_[3*i+1]=structure[i][1];
    structure_[3*i+2]=structure[i][2];
  }

  //pseaResult stores the calculated SS by p-sea for the structural model
  Residu* pseaResult;

  //calculate ss by p-sea
  setCoord(pseaResult,structure_, structure.size());

  if (structure_) delete structure_;

  //evaluate consistency
  Residu* res;
  double score=0;
  int count=0;
  int i;

  for(i=0, res=pseaResult->next; i<structure.size(); i++, res=res->next)
  {
    if(toupper(ss[i])=='C') continue; // && sec[i]=='C') continue;
      count++;
    if(toupper(ss[i])==toupper(res->cons)) score+=1.0;
  }

  //free pseaResult ???

  return -score/count;
*/
  return 0; // Feng Zhao: faked for compilation
/*
 if((score/count)>mSSAcceptThreshold);
 {
   return true;
 }
 return false;
*/

}

/****************** ScoringByZhangStiff ***************************/
ScoringByZhangShort::ScoringByZhangShort(){
}

ScoringByZhangShort::~ScoringByZhangShort(){
}

int ScoringByZhangShort::Init(string dat_file1)
{
  ReadE13Comm("./config/CA13.comm");
  ReadE14Comm("./config/CA14.comm");
  ReadE14hComm("./config/CA14h.comm");
  ReadE14sComm("./config/CA14s.comm");
  ReadE15Comm("./config/CA15.comm");
  ReadE15hComm("./config/CA15h.comm");
  ReadE15sComm("./config/CA15s.comm");
  return 1;
}

bool ScoringByZhangShort::ReadE13Comm(string fname)
{
  string line, aa;
  ifstream commFile;
  cerr << "Reading CA13.comm file " << fname << endl;
  commFile.open(fname.c_str(), ios::in | ios::binary);

  for (int i=0; i<20; i++) {
    for (int j=0; j<20; j++) {
      getline(commFile, line); // bypass the first line
      getline(commFile, line);
      strtokenizer strtokens(line," ");
      int num=strtokens.count_tokens();
      if (num<2) continue;
      for (int l=0; l<num; l++) {
        e13[i][j][l] = atof(strtokens.token(l).c_str());
      }
    }
  }
  return true;
}

bool ScoringByZhangShort::ReadE14Comm(string fname)
{
  string line;
  ifstream commFile;
  cerr << "Reading CA14.comm file " << fname << endl;
  commFile.open(fname.c_str(), ios::in | ios::binary);
  for (int i=0; i<20; i++) {
    for (int j=0; j<20; j++) {
      getline(commFile, line); // bypass the first line
      for (int k=0; k<3; k++) {
        getline(commFile, line);
        strtokenizer strtokens(line," ");
        int num=strtokens.count_tokens();
        if (num<8) continue;
        for (int l=0; l<num; l++)
          e14[i][j][k*8+l] = atof(strtokens.token(l).c_str());
      }
    }
  }
  return true;
}

bool ScoringByZhangShort::ReadE14hComm(string fname)
{
  string line;
  ifstream commFile;
  cerr << "Reading CA14h.comm file " << fname << endl;
  commFile.open(fname.c_str(), ios::in | ios::binary);
  for (int i=0; i<20; i++) {
    for (int j=0; j<20; j++) {
      getline(commFile, line); // bypass the first line
      for (int k=0; k<3; k++) {
        getline(commFile, line);
        strtokenizer strtokens(line," ");
        int num=strtokens.count_tokens();
        if (num<8) continue;
        for (int l=0; l<num; l++)
          e14h[i][j][k*8+l] = atof(strtokens.token(l).c_str());
      }
    }
  }
  return true;
}

bool ScoringByZhangShort::ReadE14sComm(string fname)
{
  string line;
  ifstream commFile;
  cerr << "Reading CA14s.comm file " << fname << endl;
  commFile.open(fname.c_str(), ios::in | ios::binary);
  for (int i=0; i<20; i++) {
    for (int j=0; j<20; j++) {
      getline(commFile, line); // bypass the first line
      for (int k=0; k<3; k++) {
        getline(commFile, line);
        strtokenizer strtokens(line," ");
        int num=strtokens.count_tokens();
        if (num<8) continue;
        for (int l=0; l<num; l++) {
          string snum = strtokens.token(l);
          e14s[i][j][k*8+l] = atof(snum.c_str());
          //cerr << snum << "=" << e14s[i][j][k*8+l] << "\t";
        }
        //cerr << endl;
      }
    }
  }
  return true;
}

double ScoringByZhangShort::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
// Calculate the energy for Electrostatic interactions
  int arrResidueIdx[25]={1, -1, 3, 9, 13, 17, 0, 16, 6, -1, 12, 11,
        8, 10, -1, 7, 14, 15, 2, 5, -1, 4, 19, -1, 18};
  // GLY  ALA  SER  CYS  VAL  THR  ILE  PRO  MET  ASP  ASN  LEU  LYS  GLU  GLN  ARG  HIS  PHE  TYR  TRP
  //  0    1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18   19
  //{"ALA","", "CYS","ASP","GLU","PHE","GLY","HIS","ILE","", "LYS","LEU",
  //        "MET","ASN","", "PRO","GLN","ARG","SER","THR","", "VAL","TRP","", "TYR"};
  energy3 = 0.0;
  energy4 = 0.0;
  energy5 = 0.0;
  int p_len = structure.size();
  for(int i=0; i<p_len-2; i++) // Side Chain is added from 1 to p_len-2
  {
    string aai = structure[i].name;
    ATOM CAi = structure[i].getAtom("CA");
    // E13
    if (i<p_len-2) {
      int j=i+2;
      string aaj = structure[j].name;
      ATOM CAj = structure[j].getAtom("CA");
      double d_i_j = (Vector3(CAj.x, CAj.y, CAj.z)).getDistance(
                                         Vector3(CAi.x, CAi.y, CAi.z));
      if (d_i_j==0) {
        cerr<<"ERROR::";
        cerr << aai<<"["<<j<<"]("<<CAj.x<<","<<CAj.y<<","<<CAj.z<<")"
             << aai<<"["<<i<<"]("<<CAi.x<<","<<CAi.y<<","<<CAi.z<<")\n";
        continue;
      }
      int ai = arrResidueIdx[sequence[i]-'A'];
      int aj = arrResidueIdx[sequence[j]-'A'];
      double residue_energy = E13(ai, aj, d_i_j);
      energy3 +=residue_energy;
    }
    // E14
    if (i<p_len-3) {
      string aa3 = structure[i+3].name;
      ATOM CA1 = structure[i+1].getAtom("CA");
      ATOM CA2 = structure[i+2].getAtom("CA");
      ATOM CA3 = structure[i+3].getAtom("CA");
      Vector3 ai = Vector3(CAi.x, CAi.y, CAi.z);
      Vector3 ai1 = Vector3(CA1.x, CA1.y, CA1.z);
      Vector3 ai2 = Vector3(CA2.x, CA2.y, CA2.z);
      Vector3 ai3 = Vector3(CA3.x, CA3.y, CA3.z);
      double d_i_j = ai3.getDistance(ai);
      if (d_i_j==0) {
        cerr << "ERROR::";
        cerr << aa3<<"["<<i+3<<"]("<<CA3.x<<","<<CA3.y<<","<<CA3.z<<")"
             << aai<<"["<<i<<"]("<<CAi.x<<","<<CAi.y<<","<<CAi.z<<")\n";
        continue;
      }
      // Let a,b,c are the consecutive vectors of neighboring residues, and
      // d=a(x)b(.)c. d>0 corresponds to right-handed and d<0 to left-handed.
      Vector3 a = ai1-ai;
      Vector3 b = ai2-ai1;
      Vector3 c = ai3-ai2;
      double d = (a%b)*c;
      int chirality = -1;
      if (d>0)
        chirality = 1;
      int li = arrResidueIdx[sequence[i]-'A'];
      int l3 = arrResidueIdx[sequence[i+3]-'A'];
      double residue_energy = CalculateE14(li, l3, d_i_j, chirality, ss, i);
      energy4 +=residue_energy;
    }
    // E15
    if (i<p_len-4) {
      int j=i+4;
      string aaj = structure[j].name;
      ATOM CAj = structure[j].getAtom("CA");
      double d_i_j = (Vector3(CAj.x, CAj.y, CAj.z)).getDistance(
                                           Vector3(CAi.x, CAi.y, CAi.z));
      if (d_i_j==0) {
        cerr<<"ERROR::";
        cerr << aaj<<"["<<j<<"]("<<CAj.x<<","<<CAj.y<<","<<CAj.z<<")"
             << aai<<"["<<i<<"]("<<CAi.x<<","<<CAi.y<<","<<CAi.z<<")\n";
        continue;
      }
      int ai = arrResidueIdx[sequence[i]-'A'];
      int aj = arrResidueIdx[sequence[j]-'A'];
      double residue_energy = CalculateE15(ai, aj, d_i_j, ss, i);
      energy5 +=residue_energy;
    }
  }
  return energy3;
}

double ScoringByZhangShort::E13(int aai, int aaj, double r2)
{
  // For r13.comm, the two values correspond the potential of r(i,i+2)<6.03A, >6.03A.
  if (aai<0 || aai>19 || aaj<0 || aaj>19)
    return 0;
  if (r2<6.03) return e13[aai][aaj][0];
  else return e13[aai][aaj][1];
}

double ScoringByZhangShort::E14(int aai, int aaj, double r3)
{
  if (aai<0 || aai>19 || aaj<0 || aaj>19)
    return 0;
  r3+=12;
  if (r3<=1) return e14[aai][aaj][0];
  if (r3>=24) return e14[aai][aaj][23];
      
  //int n_bins = 24;
  int ri = int(r3)-1;

  // Test if we should using interpolation
  ri = int(r3+0.5)-1;
  return e14[aai][aaj][ri];
}

double ScoringByZhangShort::E14h(int aai, int aaj, double r3)
{
  if (aai<0 || aai>19 || aaj<0 || aaj>19)
    return 0;
  r3+=12;
  if (r3<=1) return e14h[aai][aaj][0];
  if (r3>=24) return e14h[aai][aaj][23];
      
  //int n_bins = 24;
  int ri = int(r3)-1;

  // Test if we should using interpolation
  ri = int(r3+0.5)-1;
  return e14h[aai][aaj][ri];
}

double ScoringByZhangShort::E14s(int aai, int aaj, double r3)
{
  if (aai<0 || aai>19 || aaj<0 || aaj>19)
    return 0;
  r3+=12;
  if (r3<=1) return e14s[aai][aaj][0];
  if (r3>=24) return e14s[aai][aaj][23];
  
  // Test if we should using interpolation
  int ri = int(r3+0.5)-1;
  return e14s[aai][aaj][ri];
}

double ScoringByZhangShort::CalculateE14(
        int aai, int aaj, double r3, int chirality, string ss, int i)
{
  if (ss[i]=='H' && ss[i+1]=='H' && ss[i+2]=='H' && ss[i+3]=='H')
    return E14h(aai, aaj, r3*chirality);
  if (ss[i]=='S' && ss[i+1]=='E' && ss[i+2]=='E' && ss[i+3]=='E')
    return E14s(aai, aaj, r3*chirality);
  else
    return E14(aai, aaj, r3*chirality);
}

/****************** ScoringFunctionByZhang ***************************/

bool ScoringFunctionByZhang::ReadContact3_mm(string fname)
{
  string line, aa;
  ifstream commFile;
  cerr << "Reading contact3.comm file " << fname << endl;
  commFile.open(fname.c_str(), ios::in | ios::binary);

  // e: pairwise potential
  
  // Read Parallel part
  getline(commFile, line); // bypass the first line
  int aa1=0;
  while (commFile.good() && aa1<20) {
    // Moves through the lines until the HEADER record is found.
    getline(commFile, line);
    aa = line;
    strtokenizer strtokens(line," ");
    int num=strtokens.count_tokens();
    if (num<21) continue;
    
    for (int i=1; i<21; i++) {
      string snum = strtokens.token(i);
      e[aa1][i-1][0] = atof(strtokens.token(i).c_str());
    }

    aa1 ++;
  }
  // Read MID part
  getline(commFile, line); // bypass the empty line
  getline(commFile, line); // bypass the first line
  aa1=0;
  while (commFile.good() && aa1<20) {
    // Moves through the lines until the HEADER record is found.
    getline(commFile, line);
    aa = line;
    strtokenizer strtokens(line," ");
    int num=strtokens.count_tokens();
    if (num<21) continue;
    
    aa = strtokens.token(0);
    for (int i=1; i<21; i++)
      e[aa1][i-1][1] = atof(strtokens.token(i).c_str());

    aa1 ++;
  }
  // Read Anti-Parallel part
  getline(commFile, line); // bypass the empty line
  getline(commFile, line); // bypass the first line
  //cerr << line << endl;
  aa1=0;
  while (commFile.good() && aa1<20) {
    // Moves through the lines until the HEADER record is found.
    getline(commFile, line);
    aa = line;
    strtokenizer strtokens(line," ");
    int num=strtokens.count_tokens();
    if (num<21) continue;
    
    aa = strtokens.token(0);
    for (int i=1; i<21; i++)
      e[aa1][i-1][2] = atof(strtokens.token(i).c_str());

    aa1 ++;
  }
  
  // R0: R_min=(1-0.17)R0; R_max=(1+0.17)R0
  
  // Read Parallel part
  getline(commFile, line); // bypass the empty line
  getline(commFile, line); // bypass the first line
  aa1=0;
  while (commFile.good() && aa1<20) {
    // Moves through the lines until the HEADER record is found.
    getline(commFile, line);
    aa = line;
    strtokenizer strtokens(line," ");
    int num=strtokens.count_tokens();
    if (num<21) continue;
    
    aa = strtokens.token(0);
    for (int i=1; i<21; i++) {
      string snum = strtokens.token(i);
      R0[aa1][i-1][0] = atof(strtokens.token(i).c_str());
    }

    aa1 ++;
  }
  // Read MID part
  getline(commFile, line); // bypass the empty line
  getline(commFile, line); // bypass the first line
  aa1=0;
  while (commFile.good() && aa1<20) {
    // Moves through the lines until the HEADER record is found.
    getline(commFile, line);
    aa = line;
    strtokenizer strtokens(line," ");
    int num=strtokens.count_tokens();
    if (num<21) continue;
    
    aa = strtokens.token(0);
    for (int i=1; i<21; i++)
      R0[aa1][i-1][1] = atof(strtokens.token(i).c_str());

    aa1 ++;
  }
  // Read Anti-Parallel part
  getline(commFile, line); // bypass the empty line
  getline(commFile, line); // bypass the first line
  //cerr << line << endl;
  aa1=0;
  while (commFile.good() && aa1<20) {
    // Moves through the lines until the HEADER record is found.
    getline(commFile, line);
    aa = line;
    strtokenizer strtokens(line," ");
    int num=strtokens.count_tokens();
    if (num<21) continue;
    
    aa = strtokens.token(0);
    for (int i=1; i<21; i++)
      R0[aa1][i-1][2] = atof(strtokens.token(i).c_str());

    aa1 ++;
  }
  return true;
}

bool ScoringFunctionByZhang::ReadE15Comm(string fname)
{
  string line, aa;
  ifstream commFile;
  cerr << "Reading CA15.comm file " << fname << endl;
  commFile.open(fname.c_str(), ios::in | ios::binary);

  for (int i=0; i<20; i++) {
    for (int j=0; j<20; j++) {
      getline(commFile, line); // bypass the first line
      for (int k=0; k<2; k++) {
        getline(commFile, line);
        strtokenizer strtokens(line," ");
        int num=strtokens.count_tokens();
        if (num<8) continue;
        for (int l=0; l<num; l++)
          e15[i][j][k*8+l] = atof(strtokens.token(l).c_str());
      }
    }
  }
  return true;
}

bool ScoringFunctionByZhang::ReadE15hComm(string fname)
{
  string line, aa;
  ifstream commFile;
  cerr << "Reading CA15.comm file " << fname << endl;
  commFile.open(fname.c_str(), ios::in | ios::binary);

  for (int i=0; i<20; i++) {
    for (int j=0; j<20; j++) {
      getline(commFile, line); // bypass the first line
      for (int k=0; k<2; k++) {
        getline(commFile, line);
        strtokenizer strtokens(line," ");
        int num=strtokens.count_tokens();
        if (num<8) continue;
        for (int l=0; l<num; l++)
          e15h[i][j][k*8+l] = atof(strtokens.token(l).c_str());
      }
    }
  }
  return true;
}

bool ScoringFunctionByZhang::ReadE15sComm(string fname)
{
  string line, aa;
  ifstream commFile;
  cerr << "Reading CA15.comm file " << fname << endl;
  commFile.open(fname.c_str(), ios::in | ios::binary);

  for (int i=0; i<20; i++) {
    for (int j=0; j<20; j++) {
      getline(commFile, line); // bypass the first line
      for (int k=0; k<2; k++) {
        getline(commFile, line);
        strtokenizer strtokens(line," ");
        int num=strtokens.count_tokens();
        if (num<8) continue;
        for (int l=0; l<num; l++)
          e15s[i][j][k*8+l] = atof(strtokens.token(l).c_str());
      }
    }
  }
  return true;
}

double ScoringFunctionByZhang::E15(int aai, int aaj, double r4)
{
  if (aai<0 || aai>19 || aaj<0 || aaj>19)
    return 0;
  if (r4<=1) return e15[aai][aaj][0];
  if (r4>=16) return e15[aai][aaj][15];
    
  // Test if we should using interpolation
  int ri = int(r4+0.5)-1;
  return e15[aai][aaj][ri];
}

double ScoringFunctionByZhang::E15h(int aai, int aaj, double r4)
{
  if (aai<0 || aai>19 || aaj<0 || aaj>19)
    return 0;
  if (r4<=1) return e15h[aai][aaj][0];
  if (r4>=16) return e15h[aai][aaj][15];
    
  int ri = int(r4+0.5)-1;
  return e15h[aai][aaj][ri];
}

double ScoringFunctionByZhang::E15s(int aai, int aaj, double r4)
{
  if (aai<0 || aai>19 || aaj<0 || aaj>19)
    return 0;
  if (r4<=1) return e15s[aai][aaj][0];
  if (r4>=16) return e15s[aai][aaj][15];
    
  //int n_bins = 16;
  int ri = int(r4)-1;

  // Test if we should using interpolation
  ri = int(r4+0.5)-1;
  return e15[aai][aaj][ri];
}

double ScoringFunctionByZhang::CalculateE15(
        int aai, int aaj, double r4, string ss, int i)
{
  if (ss[i]=='H' && ss[i+1]=='H' && ss[i+2]=='H' && ss[i+3]=='H' && ss[i+4]=='H')
    return E15h(aai, aaj, r4);
  if (ss[i]=='S' && ss[i+1]=='E' && ss[i+2]=='E' && ss[i+3]=='E' && ss[i+4]=='E')
    return E15s(aai, aaj, r4);
  else
    return E15(aai, aaj, r4);
}

/****************** ScoringByZhangStiff ***************************/
ScoringByZhangStiff::ScoringByZhangStiff(){
}

ScoringByZhangStiff::~ScoringByZhangStiff(){
}

double ScoringByZhangStiff::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(ss, structure);
}

double ScoringByZhangStiff::evaluate(string& ss, vector<RESIDUE >& structure)
{
// Calculate the energy for local conformational stiffness, i.e.
// adding up the lambda*(-I(i)*I(i+4)-U(i)*U(i+2)-Theta1(i))+Theta2(i)
// for all the residues
  double energy = 0.0;
  int p_len = structure.size();
  vector<Vector3> r, r4, r6, r7, I, U, V;
  r.resize(p_len-1);
  r4.resize(p_len-4);
  r6.resize(p_len-6);
  r7.resize(p_len-7);
  I.resize(p_len-1);
  U.resize(p_len-1);
  V.resize(p_len-1);
  vector<Vector3 > g_backbone;
  Vector3 center(0,0,0);
  int i;
  for(i=0; i<p_len; i++) {
    ATOM CAi = structure[i].getAtom("CA");
    g_backbone.push_back(Vector3(CAi.x, CAi.y, CAi.z));
    center+=Vector3(CAi.x, CAi.y, CAi.z);
    if (i==p_len-1)
      break;
    ATOM CAi1 = structure[i+1].getAtom("CA");
    r[i] = Vector3(CAi1.x, CAi1.y, CAi1.z)-Vector3(CAi.x, CAi.y, CAi.z);
    if (r[i].x==0&&r[i].y==0&&r[i].z==0)
      return 0;
    I[i] = r[i].normalize();
    if (i<p_len-4) {
      ATOM CAi4 = structure[i+4].getAtom("CA");
      r4[i] = Vector3(CAi4.x, CAi4.y, CAi4.z)-Vector3(CAi.x, CAi.y, CAi.z);
    }
    if (i<p_len-6) {
      ATOM CAi6 = structure[i+6].getAtom("CA");
      r6[i] = Vector3(CAi6.x, CAi6.y, CAi6.z)-Vector3(CAi.x, CAi.y, CAi.z);
    }
    if (i<p_len-7) {
      ATOM CAi7 = structure[i+7].getAtom("CA");
      r7[i] = Vector3(CAi7.x, CAi7.y, CAi7.z)-Vector3(CAi.x, CAi.y, CAi.z);
    }
  }
  center/=p_len;
  for(i=1; i<p_len-1; i++) {
    U[i] = (I[i-1]-I[i]).normalize();
    V[i] = (U[i]%I[i]).normalize();
  }

  double rg = CalculateRadius(g_backbone);
  
  for(i=0; i<p_len-1; i++) {
    double term1=0.0;
    double term2=0.0;
    
    if (i<p_len-5) // I[p_len-1] is invalid
      term1=I[i]*I[i+4];
    
    if (0<i && i<p_len-3) // U is valid from 1 to p_len-1
      term2=U[i]*U[i+2];
    
    double theta1=0.0, theta2=0.0, theta3=0.0;
    if (i<p_len-4) {
      if ((I[i]*I[i+2]<0 && I[i]*I[i+3]>0 && r4[i].getLength()<7.5)
        || (0<i && V[i]*V[i+1]<0 && V[i]*V[i+2]>0 && r4[i].getLength()>11.0))
      {
        theta1=1.0;
      }
    }

    if (i<p_len-7 && ss[i]=='H')
      theta2=abs(r7[i].getLength()-10.5);
    else if (i<p_len-6 && ss[i]=='E')
      theta2=abs(r6[i].getLength()-19.1);

    if (i<p_len-12 && r4[i]*r4[i+4]<0 && r4[i+4]*r4[i+8]>0 && r4[i]*r4[i+8]>0)
      theta3=1.0;

    double lambda = 0.5;
    ATOM CA = structure[i].getAtom("CA");
    double dist2center = center.getDistance(Vector3(CA.x, CA.y, CA.z));
    if (dist2center<rg)
      lambda = 1.0;

    double residue_energy = lambda*(-term1-term2-theta1) + theta2 + theta3;

    energy +=residue_energy;
  }

  return energy;
}

/****************** ScoringByZhangHB ***************************/
ScoringByZhangHB::ScoringByZhangHB(){
}

ScoringByZhangHB::~ScoringByZhangHB(){
}

double ScoringByZhangHB::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(ss, structure);
}

double ScoringByZhangHB::evaluate(string& ss, vector<RESIDUE >& structure)
{
// Calculate the energy for hydrogen bonds, i.e.
// adding up the -lambda'*(U(i)*U(j)*|V(i)*V(j)|*Theta4(i,j) for all the residues
  double energy = 0.0;
  int p_len = structure.size();
  vector<Vector3> r, I, U, V;
  r.resize(p_len-1);
  I.resize(p_len-1);
  U.resize(p_len-1);
  V.resize(p_len-1);
  int i;
  for(i=0; i<p_len-1; i++) {
    ATOM CAi = structure[i].getAtom("CA");
    ATOM CAi1 = structure[i+1].getAtom("CA");
    r[i] = Vector3(CAi1.x, CAi1.y, CAi1.z)-Vector3(CAi.x, CAi.y, CAi.z);
    if (r[i].x==0&&r[i].y==0&&r[i].z==0)
      return 0;
    I[i] = r[i].normalize();
  }
  for(i=1; i<p_len-1; i++) {
    U[i] = (I[i-1]-I[i]).normalize();
    V[i] = (U[i]%I[i]).normalize();
  }

  for(i=1; i<p_len-3; i++) {
    ATOM CAi = structure[i].getAtom("CA");
    for (int j=i+4; j<p_len-1; j++) {
      // H-bonds between extended-assigned and helical-assigned residues
      // and long-range H-bonds between helical-assigned residues are prohibited
      if ((ss[i]=='H' && ss[j]=='E') 
          || (ss[i]=='E' && ss[j]=='H') 
          || (ss[i]=='H' && ss[j]=='H' && abs(i-j)>4)
          || (ss[i]=='E' && ss[j]=='E' && abs(i-j)<4)
          )
        continue;

      ATOM CAj = structure[j].getAtom("CA");
      double U_mul=U[i]*U[j];
      double V_mul=abs(V[i]*V[j]);
      
      double theta4=0.0;
      double d_i_j = (Vector3(CAj.x, CAj.y, CAj.z)).getDistance(Vector3(CAi.x, CAi.y, CAi.z));
      Vector3 r_i_j=Vector3(CAj.x, CAj.y, CAj.z)-Vector3(CAi.x, CAi.y, CAi.z);
      if (d_i_j<5.8 && U_mul>0 && V_mul>0.43 && abs(r_i_j*V[i])/d_i_j>0.9 && abs(r_i_j*V[j])/d_i_j>0.9 ) {
        theta4=1.0;
      }
      
      double lambda=1;
      if (ss[i]=='H' || ss[i]=='E')
        lambda=1.5;

      double residue_energy = -lambda*U_mul*V_mul*theta4;
      energy +=residue_energy;
    }
  }
  return energy;
}

/****************** ScoringByZhangPair ***************************/
ScoringByZhangPair::ScoringByZhangPair(){
}

ScoringByZhangPair::~ScoringByZhangPair(){
}

int ScoringByZhangPair::Init(string dat_file1, string dat_file2)
{
  ReadContact3_mm(dat_file1);
  ReadE15Comm(dat_file2); //"./config/CA15.comm");
  return 1;
}

//calculate the score of a given structure
double ScoringByZhangPair::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  // long range pairwise interactions
  int arrResidueIdx[25]={1, -1, 3, 9, 13, 17, 0, 16, 6, -1, 12, 11,
        8, 10, -1, 7, 14, 15, 2, 5, -1, 4, 19, -1, 18};
  // GLY  ALA  SER  CYS  VAL  THR  ILE  PRO  MET  ASP  ASN  LEU  LYS  GLU  GLN  ARG  HIS  PHE  TYR  TRP
  //  0    1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18   19
  //{"ALA","", "CYS","ASP","GLU","PHE","GLY","HIS","ILE","", "LYS","LEU",
  //        "MET","ASN","", "PRO","GLN","ARG","SER","THR","", "VAL","TRP","", "TYR"};

  double energy = 0.0;
  int p_len = structure.size();
  vector<Vector3> r, r4, I, U;
  r.resize(p_len-1);
  r4.resize(p_len-4);
  I.resize(p_len-1);
  U.resize(p_len-1);
  int i;
  for(i=0; i<p_len-1; i++) {
    ATOM CAi = structure[i].getAtom("CA");
    ATOM CAi1 = structure[i+1].getAtom("CA");
    r[i] = Vector3(CAi1.x, CAi1.y, CAi1.z)-Vector3(CAi.x, CAi.y, CAi.z);
    if (r[i].x==0&&r[i].y==0&&r[i].z==0)
      return 0;
    I[i] = r[i].normalize();

    if (i<p_len-4) {
      ATOM CAi4 = structure[i+4].getAtom("CA");
      r4[i] = Vector3(CAi4.x, CAi4.y, CAi4.z)-Vector3(CAi.x, CAi.y, CAi.z);
    }
  }
  for(i=1; i<p_len-1; i++) {
    U[i] = (I[i-1]-I[i]).normalize();
  }

  for(i=2; i<p_len-4; i++) {
    int aai = arrResidueIdx[sequence[i]-'A'];
    ATOM SGi = structure[i].getAtom("SG");
    if (SGi.name=="")
      continue;
    for (int j=i+6; j<p_len-2; j++) {
      int aaj = arrResidueIdx[sequence[j]-'A'];
      int gamma = 0; //PARALLEL
      double UiMulUj = U[i]*U[j];
      if (UiMulUj<-0.5)
        gamma = 2; //ANTI-PARALLEL
      else if (UiMulUj<0.5)
        gamma = 1; // MID
      
      double Rmin = (1-0.17)*R0[aai][aaj][gamma]; // R_min=(1-0.17)R0
      double Rmax = (1+0.17)*R0[aai][aaj][gamma]; // R_max=(1+0.17)R0
      ATOM SGj = structure[j].getAtom("SG");
      if (SGj.name=="")
        continue;
      double s_i_j = (Vector3(SGj.x, SGj.y, SGj.z)).getDistance(Vector3(SGi.x, SGi.y, SGi.z));
      double residue_energy = 0;
      if (s_i_j<Rmin) {
        residue_energy = 4;
      } else if (s_i_j<Rmax) {
        int aai_p2 = arrResidueIdx[sequence[i-2]-'A'];
        int aai_n2 = arrResidueIdx[sequence[i+2]-'A'];
        int aaj_p2 = arrResidueIdx[sequence[j-2]-'A'];
        int aaj_n2 = arrResidueIdx[sequence[j+2]-'A'];
        double ei = CalculateE15(aai_p2, aai_n2, r4[i-2].getLength(), ss, i-2);
        double ej = CalculateE15(aaj_p2, aaj_n2, r4[j-2].getLength(), ss, j-2);
        double C_i_j = min(0.0, ei)* min(0.0, ej);
        residue_energy = e[aai][aaj][gamma] - C_i_j;
        if (abs(i-j)>5) //E0=E0-0.25 for |i-j|>5 to encourage long-range contacts.
          residue_energy -= 0.25; 
      }
      energy +=residue_energy;
    }
  }

  return energy;
}

/****************** ScoringByZhangBurial ***************************/
ScoringByZhangBurial::ScoringByZhangBurial(){
}

ScoringByZhangBurial::~ScoringByZhangBurial(){
}

double ScoringByZhangBurial::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(structure);
}

//calculate the score of a given structure
double ScoringByZhangBurial::evaluate(vector<RESIDUE >& structure)
{
  // Burial interactions. This potential represents the general propensity of
  // amino acids to be buried or exposed to solvent and is only applicable
  // to single-domain proteins
  // Eburial = ECa(Ai, ri/r0) + MUi*ESG(Ai)
  // MUi is the burial factor relative to hydrophobic core
  double energy = 0.0;
  int N = structure.size();
  //double r0=2.2*log(N)*exp(0.38);
  // Compute ri, radial distance of the ith Ca related to the protein center
  // we can approximate the center of mass using centeroid
  Vector3 center(0,0,0);
  vector<Vector3> ri;
  ri.resize(N);
  int i;
  for(i=0; i<N; i++) {
    ATOM CAi = structure[i].getAtom("CA");
    center+= Vector3(CAi.x, CAi.y, CAi.z);
  }
  center/=N;
  for(i=0; i<N; i++) {
    ATOM CAi = structure[i].getAtom("CA");
    ri[i] = center.getDistance(Vector3(CAi.x, CAi.y, CAi.z));
  }

  return energy;
}

/****************** ScoringByZhangElectro ***************************/
ScoringByZhangElectro::ScoringByZhangElectro(){
}

ScoringByZhangElectro::~ScoringByZhangElectro(){
}

double ScoringByZhangElectro::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(ss, structure);
}

double ScoringByZhangElectro::evaluate(string& ss, vector<RESIDUE >& structure)
{
// Calculate the energy for Electrostatic interactions
  double energy = 0.0;
  double k_inverse = 15.0;//15e-10;
  double k= 1/k_inverse;
  int p_len = structure.size();
  for(int i=1; i<p_len-3; i++) // Side Chain is added from 1 to p_len-2
  {
    string aa = structure[i].name;
    if (aa!="ASP" && aa!="GLU" && aa!="LYS" && aa!="ARG")
      continue;
    ATOM SGi = structure[i].getAtom("SG");
    if (SGi.name=="")
      continue;
    for (int j=i+6; j<p_len-1; j++) {
      string aa1 = structure[j].name;
      if (aa1!="ASP" && aa1!="GLU" && aa1!="LYS" && aa1!="ARG")
        continue;
      ATOM SGj = structure[j].getAtom("SG");
      if (SGj.name=="")
        continue;
      double s_i_j = (Vector3(SGj.x, SGj.y, SGj.z)).getDistance(
                                          Vector3(SGi.x, SGi.y, SGi.z));
      if (s_i_j==0)
        continue;
      double residue_energy = exp(-k*s_i_j)/s_i_j;
      energy +=residue_energy;
    }
  }

  return energy;
}

/****************** ScoringByZhangProfile ***************************/
ScoringByZhangProfile::ScoringByZhangProfile(){
  mCN = 0;
  m_nMinChainDistance = 5;
}

ScoringByZhangProfile::~ScoringByZhangProfile(){
}

int ScoringByZhangProfile::Init(string dat_file1, string dat_file2)
{
  ReadContact3_mm(dat_file1);
  Read_contact_profile_comm(dat_file2);
  return 1;
}

bool ScoringByZhangProfile::Read_contact_profile_comm(string fname)
{
  // The following is the code for reading the table. The contact cutoff is
  // soft core cutoff R_max as mentioned above.
  // potential=envir(#antiparalel contacts, #orthogonal, #parallel, aminoacid's type)

  string line, aa;
  ifstream commFile;
  commFile.open(fname.c_str(), ios::in | ios::binary);

  for (int i=0; i<20; i++) {
    getline(commFile, line); // bypass the first line
    for (int ia=0; ia<9; ia++) {
      for (int im=0; im<9; im++) {
        getline(commFile, line);
        strtokenizer strtokens(line," ");
        int num=strtokens.count_tokens();
        if (num<9) continue;
        for (int ip=0; ip<num; ip++) {
          string sv = strtokens.token(ip);
          V[ia][im][ip][i] = atof(sv.c_str());
        }
      }
      getline(commFile, line); // bypass an empty line after one block
    }
    
    getline(commFile, line); // bypass an empty line after one amino acid
  }
  return true;
}

//calculate the score of a given structure
double ScoringByZhangProfile::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  // The potential describing the contact environment of individual residues
  // Vi(Np, Na, Nv, A);
  
  // Residues are regarded as being in contact when the distance between
  // their side groups is below Rmin(Ai, Aj, Gamma i;j)

  int arrResidueIdx[25]={1, -1, 3, 9, 13, 17, 0, 16, 6, -1, 12, 11,
        8, 10, -1, 7, 14, 15, 2, 5, -1, 4, 19, -1, 18};
  // GLY  ALA  SER  CYS  VAL  THR  ILE  PRO  MET  ASP  ASN  LEU  LYS  GLU  GLN  ARG  HIS  PHE  TYR  TRP
  //  0    1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18   19
  //{"ALA","", "CYS","ASP","GLU","PHE","GLY","HIS","ILE","", "LYS","LEU",
  // "MET","ASN","", "PRO","GLN","ARG","SER","THR","", "VAL","TRP","", "TYR"};
  
  double energy = 0.0;
  int p_len = structure.size();
  vector<Vector3> r, I, U;
  vector<int> Na, Nm, Np;
  r.resize(p_len-1);
  I.resize(p_len-1);
  U.resize(p_len-1);
  Na.resize(p_len-1); // Anti-parallel
  Nm.resize(p_len-1); // perpendicular
  Np.resize(p_len-1); // Parallel
  int i;
  for(i=0; i<p_len-1; i++) {
    ATOM CAi = structure[i].getAtom("CA");
    ATOM CAi1 = structure[i+1].getAtom("CA");
    r[i] = Vector3(CAi1.x, CAi1.y, CAi1.z)-Vector3(CAi.x, CAi.y, CAi.z);
    if (r[i].x==0&&r[i].y==0&&r[i].z==0)
      return 0;
    I[i] = r[i].normalize();
  }
  for(i=1; i<p_len-1; i++) {
    Vector3 d(I[i-1]-I[i]);
    if (d.x==0&&d.y==0&&d.z==0)
      return 0;
    U[i] = d.normalize();
  }
  
  mCN = 0;
  for(i=1; i<p_len-1; i++) {
    int aai = arrResidueIdx[sequence[i]-'A'];
    ATOM SGi = structure[i].getAtom("SG");
    if (SGi.name=="")
      continue;
    for (int j=1; j<p_len-1; j++) {
      if (abs(i-j)<6)
        continue;
        
      int aaj = arrResidueIdx[sequence[j]-'A'];
      ATOM SGj = structure[j].getAtom("SG");
      if (SGj.name=="")
        continue;
      double s_i_j = (Vector3(SGj.x, SGj.y, SGj.z)).getDistance(Vector3(SGi.x, SGi.y, SGi.z));

      int gamma = 0;
      double UiMulUj = U[i]*U[j];
      if (UiMulUj<-0.5)
        gamma = 2;
      else if (UiMulUj<0.5)
        gamma = 1;

      if (s_i_j<(1+0.17)*R0[aai][aaj][gamma]) // contact
      {
        switch (gamma)
        {
        case 0:
          Np[i]++;
          break;
        case 1:
          Nm[i]++;
          break;
        case 2:
          Na[i]++;
          break;          
        }
        mCN ++;
      }
    }
  }
  mCN = abs(mCN -1.9*p_len);

  for(i=1; i<p_len-1; i++) {
    int aai = arrResidueIdx[sequence[i]-'A'];
    if (Na[i]>8 || Nm[i]>8 || Np[i]>8 || aai>19 || aai<0)
      continue;
    double residue_energy = V[Na[i]][Nm[i]][Np[i]][aai];
    energy +=residue_energy;
  }
  cerr << "energy = " << energy << endl;
  
  return energy;
}

/****************** ScoringByZhangCOCN ***************************/
ScoringByZhangCOCN::ScoringByZhangCOCN(){
  m_nMinChainDistance = 4;
}

ScoringByZhangCOCN::~ScoringByZhangCOCN(){
}

//calculate the score of a given structure
double ScoringByZhangCOCN::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  ScoringByZhangProfile::evaluate(sequence, ss, structure, start);
  return mCN;
}

/****************** ScoringBydDFIRE****************************/
ScoringBydDFIRE::ScoringBydDFIRE(){
}

ScoringBydDFIRE::~ScoringBydDFIRE(){
}

// This function is necessary for initialization
int ScoringBydDFIRE::Init(string dat_file1)
{
  ddfire.Init();
  return 1;
}

double ScoringBydDFIRE::evaluate(
        string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  return evaluate(structure, start, 0);
}

double ScoringBydDFIRE::evaluate(vector<RESIDUE >& structure, int start, int end)
{
  //cerr << "ScoringBydDFIRE::evaluate" << endl;
  //return ddfire.CalcEnergy("/home/fzhao/mcs/dfire-out/T04892324873817.pdb");

  ZhouMolecule *mol = new ZhouMolecule();
  for (int i=0; i<(int)structure.size(); i++) {
    RESIDUE res = structure[i];
    int cid=' ', chain0=0;
    mol->addRes(res.name, cid, chain0, i+1);
    for(int i=0; i<(int)res.arrAtoms.size();i++){
      ATOM atom = res.arrAtoms[i];
      mol->initAtom(atom.name, atom.x, atom.y, atom.z);
    }
  }
  return ddfire.CalcEnergy(mol, start);
/*
  double ener[20];
  mol->initneib();
  //mol->score(ener, ddfire.bag);
  mol->score(ener);
  delete mol;
  return ener[0];
*/
}

