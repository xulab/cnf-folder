#ifndef  _CONTACT_NN_H
#define _CONTACT_NN_H

#include "ScoreMatrix.h"

using namespace std;

#define DUMMY -1
#define _TOTAL_PROBS 13
#define TOTAL_LABEL 12
//#define TOTAL_COND_LABEL 12
#define TOTAL_COND_LABEL 26

class EPAD;

class NN_Sequence
{
public:
	NN_Sequence(int len, EPAD* pModel, string name, int label_flag);
	~NN_Sequence();

	void makeFeatures(int contact_DIST);
	
	// func=0: contact prediction for top L/5, 1-top L/10, 2-top 5; 3-energy function for FM, 4-energy function for TB; 5-contact number
	void PredictDistances(int func);

	void ComputeGates();
	void ComputeVi();
	int MAP(int top, int nMaxContact, int nMaxNonContact);
	void CalcPartition();

	Score **obs_feature;
	ScoreMatrix *forward;
	Score* Partition;
	ScoreMatrix* dist_pred;
	double* backbone_probs;
	inline double get_backbone_probs(int ai, int aj, int label, int i, int j);
	inline void set_backbone_probs(int ai, int aj, int label, int i, int j, double p);

	ScoreMatrix* backbone_prob_flags;
	vector<int> gaps;
	int length_contact;
	int *IdxMap, *rIdxMap, *centerMap, *IdxMapCofactor, *IdxMapCenter, *ss3;
	int confusion[40][40];
	int pred[40]; 
	double sum_energy[40];
	double sum_energy_cd[99];

	int length_seq;
	int label_flag; // 0- Ca; 1- contact number

	double predRg;
	double betaVsAlpha;

protected:
	EPAD* m_pModel;

	Score Gate(Score sum){ return (Score)1.0/(1.0+exp(-(double)sum)); }

	string p_name;

	int *_features;
	
	ScoreMatrix *buffer2;
	void ClearBuffer();
	void ClearBuffer1();

	ScoreMatrix *isbuffered2, *isbuffered_output_center, *output_center;
	ScoreMatrix *isbuffered_output_cofactor, *output_cofactor;

	int *predicted_label;
	int *predicted_label8;

	ScoreMatrix* gates;
	ScoreMatrix* arrVi;

	Score GetGateOutput(int gate,int pos);
	Score GetGateOutput2(int gate2,int pos);
	Score ComputeScore(int currState, int pos);
	Score getFeatures(int pos, int i);
};	

class EPAD
{
public:
	EPAD(){ model_typ=0; forbidden_val=2; cond_weight=1; memset(log_ref, 0, 100*sizeof(double));};
	~EPAD(){ delete testData; };

	void Initialize(string config_file, string input_f);
	double CalculateConditionalPairEnergyS12(int i, int j, double dist, double Rg, int func, int ri, int rj, int ai, int aj);
	double CalculatePairEnergyS12(int i, int j, double dist, double Rg, int func, int ri, int rj);
	void reset(int s_Res, int e_Res, vector<int>& r_flag);
	void printConfusionMatrix(double Rg);

	int num_labels;
	int num_states;
	int num_gates;
	int num_gates2;
	int dim_one_pos;
	int dim_features;
	int window_size;
	int num_params;
	int gold_positive, positive;
	int startRes, endRes;
	vector<int> residue_flag; // 0--residue is missed; 1-- residue is real. We won't predict contact for those missing residues
	int label_flag; // 0- Ca; 1- contact number
	double prob_14[20][20][_TOTAL_PROBS+2];
	double prob_cond[20][5][20][5][TOTAL_LABEL][TOTAL_COND_LABEL];
	int resMap[26];
	double refScale[30];
	double dope_weight, positive_energy_cutoff;
	double *NEFFs, NEFF;

	NN_Sequence* testData;

	int nnz_param;
	int iGateStart, iGate2Start, iFeatureStart;
	double* weights;

	// get the transition prob, num_states( for DUMMY ) + num_states*num_states
	inline Score GetWeightT(int leftState, int currLabel){return weights[num_labels * (leftState +1)+ currLabel];}
	// get the label-based weight, num_states*num_gates
	inline Score GetWeightL(int currState, int gate){ return weights[iGateStart+ currState*num_gates + gate];}
	// get the Gate weight
	inline Score GetWeightG(int gate, int gate2){ return weights[iGate2Start + gate*num_gates2 + gate2];}
	// the inner gate weight // num_gates2*dim_features
	inline Score GetWeightG2(int gate2, int dim){return weights[iFeatureStart+gate2*dim_features + dim];};

	string model_file;
	string config;
	int model_typ; // 0-- free modelling; 1-- template based
	double forbidden_val, cond_weight;

protected:
	ScoreMatrix logReferenceStateNearCD; // reference state for residue pairs with chain distance between 6 and 24 for sequences with length under 65
	ScoreMatrix logReferenceStateNearCD1; // reference state for residue pairs with chain distance between 6 and 24 for longer sequences (65<len<=120)
	ScoreMatrix logReferenceStateNearCD2; // reference state for residue pairs with chain distance between 6 and 24 for longer sequences (120<len<=200)
	ScoreMatrix logReferenceStateNearCD3; // reference state for residue pairs with chain distance between 6 and 24 for longer sequences (len>200)

	void LoadData(string input);
	double logReferenceState(double r, double Rg, double lastDist);
	double log_ref[1000];

	void trim2(string& str) {
		string::size_type pos = str.find_last_not_of(' ');
		if(pos != string::npos) {
			str.erase(pos + 1);
			pos = str.find_first_not_of(' ');
			if(pos != string::npos)
				str.erase(0, pos);
		}
		else str.erase(str.begin(), str.end());
	};
	int ReadConfigFile(string cfgName);
};

#endif
