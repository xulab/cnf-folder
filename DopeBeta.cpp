/*******************************************************************************
                                  DopeBeta-Back-AllAtom.cpp
                                  ---------------------
  Copyright (C) 2005 The University of Chicago
  Authors: 
  Min-yi Shen, original code
  Andres Colubri, coversion to C++
  James Fitzgerald, backbone dependence
  Description:
  PL plugin to calculate Min-yi's Statistical Potential for Fold
  Recognition. Backbone Dependent and allows for all atom treatment.
*******************************************************************************/
#include <cstddef> // Needed to use NULL.
#include <fstream>
#include <cstring>
#include <cmath>
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "DopeBeta.h"
#include "strtokenizer.h"

using namespace std;

#define INF_CHAIN 10000

void DopeBeta::SetDefConfig()
{
    // Setting defaults.
    dist_cutoff = 15.0;
    num_bin = 30;
    dope_par_file = "";
    dope_dir = "";

    add_bonded = false;

    add_hydrogens = false;
    add_centroids = false;
    add_side_chains = true;
    add_only_CB = false;
    CB_CB_focus = false;
    fix_consensus = false;

    analysis = false;

    Burial_renorm = false;
    Penalty_coeff = 0.0;
    Burial_sphere_rad = 7.0;
    Burial_min_atcount = 5;
    Burial_max_atcount = 20;

    min_chain_dist = 1;
    min_CB_CB_dist = 1;
    max_chain_dist = INF_CHAIN;

    energy_coeff = 1.0;
    CB_CB_coefficient = 1.0;
}

int DopeBeta::ReadConfigFile(string cfgName)
{
	ifstream CfgFile;
	CfgFile.open(cfgName.c_str(), ios::in | ios::binary);
	string line, par, val;
	bool recog_line;

	while (true) {
		getline(CfgFile, line); trim2(line);
		if ((line != "") && (line[0] != '#')) {
			strtokenizer strtokens(line,"=");
			int num=strtokens.count_tokens();
			if (num<2) continue;
			par = strtokens.token(0);
			val = strtokens.token(1);
			trim2(par);
			trim2(val);
			cerr << par << "=" << val << endl;
			recog_line = false;

			if (par == "BURIAL RENORMALIZATION") { Burial_renorm = (val=="yes"); recog_line = true; }
			if (par == "PENALTY COEFFICIENT") { Penalty_coeff = atof(val.c_str()); recog_line = true; }
			if (par == "BURIAL SPHERE RADIUS") { Burial_sphere_rad = atof(val.c_str()); recog_line = true; }
			if (par == "BURIAL MIN ATOM COUNT") { Burial_min_atcount = atoi(val.c_str()); recog_line = true; }
			if (par == "BURIAL MAX ATOM COUNT") { Burial_max_atcount = atoi(val.c_str()); recog_line = true; }

			if (par == "ANALYSIS") { analysis = (val=="yes"); recog_line = true; }

			if (par == "Distance cutoff") { dist_cutoff = atof(val.c_str()); recog_line = true; }

			if (par == "Number of bins") {
				num_bin = atoi(val.c_str());
				Resize(num_bin, Energy);
				recog_line = true;
			}
			if (par == "Parameter file") { dope_par_file = val; recog_line = true; }

			if (par == "Add bonded atoms") { add_bonded = (val=="yes"); recog_line = true; }
	
			if (par == "Add hydrogen atoms") { add_hydrogens = (val=="yes"); recog_line = true; }
			if (par == "Add side-chain centroids") { add_centroids = (val=="yes"); recog_line = true; }
			if (par == "Add side-chain atoms") { add_side_chains = (val=="yes"); recog_line = true; }
			if (par == "Add beta-carbons only") { add_only_CB = (val=="yes"); recog_line = true; }
			if (par == "Minimize CB_CB only") { CB_CB_focus = (val=="yes"); recog_line = true; }
			if (par == "FIX CONSENSUS") { fix_consensus = (val=="yes"); recog_line = true; }
			if (par == "DOPE FIX DIR") { dope_dir = val; recog_line = true; }

			if (par == "Minimum chain distance") { min_chain_dist = atoi(val.c_str()); recog_line = true; }
			if (par == "Maximum chain distance") { max_chain_dist = atoi(val.c_str()); recog_line = true; }

			if (par == "Minimum CB_CB distance") { min_CB_CB_dist = atoi(val.c_str()); recog_line = true; }

			if (par == "Energy coefficient") { energy_coeff = atof(val.c_str()); recog_line = true; }
			if (par == "CB_CB coefficient") { CB_CB_coefficient = atof(val.c_str()); recog_line = true; }
		}

		if (CfgFile.eof()) break;
	}
	CfgFile.close();
	return 0;
}

void DopeBeta::Init(string cfgFile)
{
	ReadConfigFile(cfgFile);
	SetAddAtoms(add_hydrogens, add_centroids, add_side_chains, add_only_CB);
	LoadFromFile(dope_par_file);
}

// Search the DopeBeta table to get the energy for 2 atoms
double DopeBeta::calcAtomDOPE(int residue_id1, int atom1, int residue_id2, int atom2, double dist) 
{
	double e_pair = 0;
	if ((0.0 < dist) && (dist < dist_cutoff)) {
		int aa0 = GetResidueCode(residue_id1);
		int at0 = GetAtomCode(atom1);
		int aa1 = GetResidueCode(residue_id2);
		int at1 = GetAtomCode(atom2);
		if (aa0==-1 || aa1==-1|| at0==-1|| at1==-1)
			return 0;    
		// The distance lies between middle points of bins ri and ri + 1.
		double r1 = 2.0 * (dist - 0.25);
		int ri = int(r1);
		
		// Linear interpolation between the energy value at those bins.
		double f1 = r1 - ri;
		double f2 = 1.0 - f1;
		//cerr <<"calcAtomDOPE(r_id1="<<residue_id1<<"; atom1="<<atom1<<"; r_id2="<<residue_id2<<"; atom2="<<atom2<<"; dist="<<dist<<endl;
		int idx = n_bins*(at1+(max_atom_code+1)*(aa1+20*(at0+(max_atom_code+1)*aa0)));//[aa0][at0][aa1][at1][0];
		if (0 < ri) {
			if (ri>=n_bins)
				return 0;
			// When ri = n_bins - 1, StatPot(ri + 1) gives 0, so we are interpolating
			// between the last energy value stored in the table and 0, which is ok.
			double e1 = Energy[ri+idx];//[aa0][at0][aa1][at1][ri];
			double e2=0;
			if (ri+1<n_bins)
				e2 = Energy[ri+1+idx];//[aa0][at0][aa1][at1][ri+1];
			e_pair = f2 * e1 + f1 * e2;
			//cerr <<"; ri="<<ri<<"; f1="<<f1<<"; e1="<<e1<<"; e2="<<e2<<endl;
		} else 
			e_pair = Energy[idx];//[aa0][at0][aa1][at1][0];
	}

	//cerr <<"; energy="<<e_pair<<endl;
	return e_pair;
}
