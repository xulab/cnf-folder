#ifndef NEW_KENT__
#define NEW_KENT__

#include <math.h>
#include <vector>
#include <BALL/MATHS/vector3.h>

using namespace std;
using namespace BALL;

#define SIGN(x) ((x)<0?-1:1)

//dot product of two vectors
double DOT(double* d1, double* d2);

//sample a unit vector from a given distribution
class KentSampler
{
public:
	//this five parameters define a distribution
    	KentSampler(double kappa, double beta, double* e1, double* e2, double* e3);
    	void sample(double* rev);

private:
    	double a;
    	double b;
    	double gamma;
    	double c2;
    	double lam1;
    	double lam2;
	vector<Vector3 > rho;
	
	void SetSeed();
};

class KentDensity
{
public:
	KentDensity(double kappa1, double beta1, double *e11, double *e22, double *e33);

	//calculate the probability of a unit vector in a give distribution if log_space is true, then return the log-likelihood
     	double density(double* x, bool log_space);

protected:
	double e1[3];
	double e2[3];
	double e3[3];

	double beta;
     	double kappa;
     	double log_c;
};

#endif
