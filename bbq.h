#ifndef _BBQ_H
#define _BBQ_H

#include <fstream>
#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <map>
#include <stack>
#include <ANN/ANN.h>

using namespace std;
#define HELIX	0
#define STRAND	1
#define COIL	2
#define TURN	2

class ATOM
{
public:
	string name;
	int name_idx; // CA=0; CB=1; O=2; N=3; C=4; HN=5; SG=6
	double x,y,z;
	int res_idx;
	int idx;

	ATOM(){x=y=z=0.0;name="";res_idx=-1;name_idx=-1;idx=-1;};
	ATOM(string aname, int i){
		this->name=aname;x=y=z=0.0;res_idx=i; name_idx=-1;
		string arrAtomName[7]={"CA", "CB", "O", "N", "C", "HN", "SG"};
		for (int n=0; n<7; n++){
			if (arrAtomName[n]==aname){
				name_idx=n;
				break;
			}
		}
	};
};

// map of atoms' name [string -> int]
typedef map<string, int > mapStr2ATOM;

class RESIDUE
{
public:
	RESIDUE(){ phi=0; psi=0;seq=0; flag=-1; seq=-1; name_idx=-1;
		for (int i=0; i<10; i++)
			atomMap[i]=-1;
	}
	//~RESIDUE(){
	//	cerr << "~RESIDUE()" << endl;
	//	for(mapStr2ATOM::iterator at1=atom_map.begin();at1!=atom_map.end();at1++)
	//		delete at1->second;
	//	atom_map.clear();
	//};
     //ATOM CA;
     //ATOM O;
     //ATOM N;
     //ATOM CB;
     //ATOM C;
	mapStr2ATOM atom_map;
	int freq;
	string name;
	int name_idx;
	double phi, psi;
	int ramaBasin;
	int seq;
	int flag;

	// return secondary structure
	int getSecondaryStructure() {
	    if ((-180 < phi) && (phi < -20) && (-90 < psi) && (psi < -10)) return HELIX;
	    if ((-180 < phi) && (phi < -20) && (((20 < psi) && (psi < 180)) || ((-170 < psi) && (psi < -180)))) return STRAND;
	    return COIL;
	}

	void addAtom(ATOM atom){
		if (getAtomIndex(atom.name)==-1) {
			// cerr << seq << "-ATOM::addAtom(" << atom.name << ") " << endl;
			// atom not found, insert it to the map
			atom.res_idx = seq;
			int n = arrAtoms.size();
			arrAtoms.push_back(atom);
			atom_map.insert(pair<string, int>(atom.name, n));
			string arrAtomName[7]={"CA", "CB", "O", "N", "C", "HN", "SG"};
			for (int i=0; i<7; i++){
				if (arrAtomName[i]==atom.name){
					atomMap[i]=n;
					break;
				}
			}
		}
	};
	
	int getAtomIndex(string aname){
		//cerr << "-" << aname << seq;
		mapStr2ATOM::iterator atomit;
		atomit = atom_map.find(aname);
		if (atomit == atom_map.end()) {
			//cerr << "N" << endl;
			return -1;
		} else {
			//cerr << "f" << endl;
			return atomit->second;
		}
	};
	int getAtomIndex(int at){ return atomMap[at];};

	void deleteAtom(string aname){
		int idx=-1;
		string arrAtomName[7]={"CA", "CB", "O", "N", "C", "HN", "SG"};
		for (int i=0; i<7; i++){
			if (arrAtomName[i]==aname){
				idx=atomMap[i];
				atomMap[i]=-1;
				break;
			}
		}
		for (int i=0; i<10; i++) {
			if (atomMap[i]>idx)
				atomMap[i]--;
		}
		mapStr2ATOM::iterator atomit;
		atomit = atom_map.find(aname);
		if (atomit != atom_map.end()) { // atom found, delete it from the map
		  arrAtoms[atomit->second].name="";
		  arrAtoms.erase(arrAtoms.begin()+atomit->second);
		  atom_map.erase(atomit);
		}
		for(mapStr2ATOM::iterator at1=atom_map.begin();at1!=atom_map.end();at1++)
			if (at1->second>idx)
				at1->second--;
	};

	ATOM getAtom(string aname){
		//cerr << "." << arrAtoms.size();
		int i = getAtomIndex(aname);
		//cerr << "*" << i;
		if (i!=-1)
			return arrAtoms[i];
		else
			return ATOM();
	};
	ATOM getAtom(int at){
		int i = atomMap[at];
		if (i!=-1)
			return arrAtoms[i];
		else
			return ATOM();
	};
	vector<ATOM> arrAtoms;
	int atomMap[10];
};

//
// Global variables, KD-tree
//
class BBQ{

private:
	int nPts; // actual number of data points
	ANNpointArray dataPts; // data points
	ANNpoint queryPt; // query point
	ANNidxArray nnIdx; // near neighbor indices
	ANNdistArray dists; // near neighbor distances
	ANNkd_tree* kdTree; // search structure
	
	int k ; // number of nearest neighbors
	int dim ; // dimension
	double eps ; // error bound
	int maxPts ; // maximum number of data points
	
	RESIDUE table[7000];
	int table_index[700000];
	
	ATOM versor[3];
	ATOM versor_t[3];
	
	string mDatabase;
	
public:
	static double innerProduct(ATOM a, ATOM b);
	ATOM crossProduct(ATOM a, ATOM b);
	void normalize(ATOM &a);
	int key(int x,int y,int z);
	int calc_index(ATOM seg[4]);
	ATOM calc_global_position(ATOM p);
	void buildLCS(ATOM seg[4]);
	void generate(vector<RESIDUE> &backbone);
	void getSeg(ATOM seg[4], int pos, vector<RESIDUE> &backbone);
	
	static double dist(ATOM a, ATOM b);
	static int lookup(vector<string>& files, const char* dir, const char *arg);
	
	BBQ(string database); // #define DATABASE "/home/jpeng/BBQ/data/BBQ.dat"
	
	~BBQ();
};

#endif
