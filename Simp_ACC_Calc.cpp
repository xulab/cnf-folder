#include <string>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <time.h>
#include "Acc_Surface.h"


//-------- WS_Simp_ACC_Calc ------//
//you need to prepare XYZ **mol data for input, where
//first dimension is the length of the input protein, and
//second dimension is N,CA,C,O,CB's coordinate;
//note that for GLY, CB should be EXACTLY the same as CA
void WS_Simp_ACC_Calc(XYZ **mol,int moln,int *acc)
{
	Acc_Surface acc_calc;
	acc_calc.AC_Calc_SolvAcc(mol,moln,acc);
}

//--------- main --------//
int main(int argc,char **argv)
{
}
