//this file defines a basic class to build a 3D structure given a set of angles
#ifndef _STRUCTURE_BUILDER
#define _STRUCTURE_BUILDER
#include <vector>
#include <string>
#include <BALL/KERNEL/protein.h>
#include <BALL/MATHS/vector2.h>
#include <BALL/MATHS/vector3.h>
#include <BALL/STRUCTURE/fragmentDB.h>

#include "Contact.h"
#include "CRFSampler.h"
#include "FBCluster.h"

using namespace std;
using namespace BALL;

class StructureBuilder
{
public:
	StructureBuilder();
	virtual ~StructureBuilder();
	void setProteinName(string& );
	//build structure from a set of angles
	virtual void build(string& seq, vector<Vector2 >& angles, vector<Vector3 > & structure);
	virtual void build(string& seq, vector<Vector2 >& angles, Protein** protein);
	//update the angles and rebuild structures, only  a segment is updated 
	virtual void build(string& seq, vector<Vector2 >& angles, int start, vector<Vector3 > & structure); 
	virtual void build(string& seq, vector<Vector2 >& angles, int start, Protein** protein);

	virtual void ApplyContactConstraint(vector<string>& labels, vector<Vector3 > & structure, Contact* pContact, CRFSampler* pCRF){};

	vector<double > presetBondLengths;

protected:
	string mProteinName;
	//save all the angles
	//vector<Vector2 > mAngles;
};

//build a structure from a given set of theta and tau angles
class ThetaTauBuilder:public StructureBuilder
{
public:
	//build structure from a set of angles
	virtual void build(string& seq, vector<Vector2 >& angles, vector<Vector3 > & structure);
	//update the angles and rebuild structures, only  a segment is updated 
	virtual void build(string& seq, vector<Vector2 >& angles, int start, vector<Vector3 > & structure); 
	//build structure from a set of unit vectors
	void build(string& seq, vector<Vector3 >& unitVectors, vector<Vector3 >& structure);
	//update the unit vectors and rebuild structures, only  a segment is updated 
	void build(string& seq, vector<Vector3 >& unitVectors, int start, vector<Vector3 >& structure);
	//build coordinates for p4
	void build(Vector3& unitVector, Vector3& p1, Vector3& p2, Vector3& p3, Vector3& p4, double bondLength);
	void build_final(string& seq, vector<Vector2 >& angles, vector<Vector3> & structure, vector<double> & bondlenList, vector<int> & m_aligned, vector<Vector3 > InitCAs);

	virtual void ApplyContactConstraint(vector<string>& labels, vector<Vector3 > & FBVec, vector<Vector3 > & structure, Contact* pContact, CRFSampler* pCRF, FB5Cluster* pFB5);

protected:
	//vector<Vector3> unitVectors; //save all the unit vectors
	double FoldBipartite(Bipartite& bp,vector<string>& labels, vector<Vector3 > & FBVec, vector<Vector3 > & structure, Contact* pContact, CRFSampler* pCRF, FB5Cluster* pFB5);
	double SearchAngleObj(Vector3& da, Vector3& bc, Vector3& u, double dist, Vector3& ret_ab, double kappaFB5);
	int CheckClash(vector<Vector3 > & structure, int pos, Vector3& new_ab);
	Vector3 rotate(double beta, Vector3& u, Vector3& v);
};

//build a structure from a given set of phi and psi angles
class PhiPsiBuilder:public StructureBuilder
{
public:
	PhiPsiBuilder();
	~PhiPsiBuilder();
	//build structure from a set of angles
	virtual void build(string& seq, vector<Vector2 >& angles, Protein** protein);

protected:
	FragmentDB* mFragmentDB;
};

//a helper function, not sure where to put
void thetaTau2XYZ(Vector2 & angle, Vector3 & unitVector);

//inverse a matrix in to out
void invert(double* in, double* out);
#endif
