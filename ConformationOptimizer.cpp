#include "ConformationOptimizer.h"
#include "newconfig.h"
#include "newutil.h"

//#include <omp.h>

#include <BALL/KERNEL/protein.h>
#include <BALL/KERNEL/residue.h>
#include <BALL/KERNEL/atomContainer.h>
#include <BALL/KERNEL/PDBAtom.h>
#include <BALL/KERNEL/atom.h>

#include <values.h>

#include <BALL/FORMAT/PDBFile.h>
#include <BALL/KERNEL/residue.h>
#include <BALL/STRUCTURE/geometricProperties.h>

#include "SideChain_util.h"
#include "rmsdp.h"

using namespace BALL;

extern Config* config;

double ProbT[100][100][100];
double Prob2[100][100];

ConformationOptimizer::ConformationOptimizer(){
  mStructureBuilder=0;  
  mScoringFunction=0;

  mProteinName.clear();
  mProteinSeq.clear();
  mSecStruSeq.clear();
  mPretagFile.clear();

  mCRFSampler=0;
  mLabel.clear();
  //mLabelSave.clear();
  mBestLabel.clear();
  mStructure.clear();
  //mStructureSave.clear();
  mBestStructure.clear();
  mNativeScore=0;
  mBBQ = NULL;
  weight_radius = 0.0;
  mMaxIteration = 10000;
  mSA_StartTemperature = 500;
  mSA_EndTemperature = 380;
  mHBEnergy=0;
  mBurialPenalty=0;
  mAnnealSteps=100;
  mStopEspSteps = 10000;
  mSimulationWeightAlpha=1;
  mSimulationWeightBeta=1;
  mSimulationWeightCoil=1;
  mStartingWeightedSimulationStep=0;
  mCurrentSimulationStep = 0;
  bMergePDB = false;
  
  NEFF=-1;
  mpContact=NULL;
}

ConformationOptimizer::ConformationOptimizer(string& proteinName){
  ConformationOptimizer();
  mProteinName=proteinName;
}

ConformationOptimizer::~ConformationOptimizer(){
  if (mCRFSampler) delete mCRFSampler;
  for(int i = 0; i < libLen; i++) {
    for(int j = 0; j < fragLen; j++) 
      delete  fragments[i][j];
    delete fragments[i];
  }
  delete fragments;
  if ((int)mStructureNative.size()==mProteinSize){
    for(int i = 0; i < mProteinSize; i++)
      delete fragNative[i];
    delete fragNative;
  }
}

void ConformationOptimizer::setProteinName(string& name){
  mProteinName=name;
}

void ConformationOptimizer::setContactConditions(string contact_file){
  cerr << "ConformationOptimizer::setContactConditions(" << contact_file << endl;
  mpContact = new Contact(contact_file, mProteinSize);
  for(int i = 0; i < libLen; i++) {
    // build the unit vectors for each cluster center
    Vector3 f1(fragments[i][1][0], fragments[i][1][1], fragments[i][1][2]);
    Vector3 f2(fragments[i][2][0], fragments[i][2][1], fragments[i][2][2]);
    Vector3 f3(fragments[i][3][0], fragments[i][3][1], fragments[i][3][2]);
    Vector3 f4(fragments[i][4][0], fragments[i][4][1], fragments[i][4][2]);
    Vector3 v=f3-f2;
    v=v/v.getLength();
    Vector3 a1a2=f2-f1;
    Vector3 f=v%a1a2;
    f=f/f.getLength();
    Vector3 a3a4=f4-f3;
    Vector3 u=a3a4/a3a4.getLength();
    double cos_theta=f*u;
    double sin_theta=sqrt(1-cos_theta*cos_theta)*(cos_theta/abs(cos_theta));
    double cos_tau=u*v;
    double sin_tau=sqrt(1-cos_tau*cos_tau)*(cos_tau/abs(cos_tau));
    Vector3 uvec(sin_theta*cos_tau, sin_theta*sin_tau, cos_theta);
    //cerr << "UnitVector " << i << ": " << uvec.x << "," << uvec.y << "," << uvec.z << endl;
    mpContact->unitVecs.push_back(uvec);
  }

  {
    ScoringFunction* sf = new ScoringByContact();
    sf->Init("",(int*)mpContact);
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, 75));
  }
}

void ConformationOptimizer::setStructureBuilder(StructureBuilder* builder){
  mStructureBuilder=builder;
}

void ConformationOptimizer::setScoringFunction(ScoringFunction* scorer){
  mScoringFunction=scorer;
}

void ConformationOptimizer::setScoringFunction()
{
  double weight;
  if (config->getValue(STR_ENERGY_WEIGHT_RADIUS, &weight) && weight>0) {
    //ScoringFunction* sf = mScoringFunction;
    //mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
    weight_radius = weight;
    cerr << "Radius weight = "<<weight_radius<<endl;
  }
  if (config->getValue(STR_ENERGY_WEIGHT_DOPE, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByDOPE();
    sf->Init("./config/dope.par");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_DOPE_BACK, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByDOPEBack();
    sf->Init("./config/dope-CB-back.cfg");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_BMK, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByBMKHBBond();
    sf->Init("./config/BMKhbond-B.cfg", mSSConfidenceSeq);
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_TSP, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByTSP1();
    sf->Init("./config/tsp1-3A.cfg");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_ESP, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByESP();
    sf->Init("./config/esp_fzhao.par");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_DOPE_BETA, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByDopeBeta();
    sf->Init("./config/dope-CB-analysis.cfg");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
    mScoringFunction = new ScoringByDOPE();
    mScoringFunction->Init("./config/dope.par");
  }
  if (config->getValue(STR_ENERGY_WEIGHT_DOPE_X, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByDopeX();
    sf->Init("./config/dope-OD.cfg");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_ZHANG_SHORT14, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByZhangShort();
    sf->Init("");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_ZHANG_SHORT15, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByZhangShort();
    sf->Init("");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_ZHANG_HB, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByZhangHB();
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_ZHANG_PAIR, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByZhangPair();
    sf->Init("./config/contact3.comm", "./config/CA15.comm");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_ZHANG_ELECTRO, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByZhangElectro();
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_ZHANG_PROFILE, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByZhangProfile();
    sf->Init("./config/contact3.comm", "./config/contact_profile.comm");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_ZHANG_COCN, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByZhangCOCN();
    sf->Init("./config/contact3.comm", "./config/contact_profile.comm");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_dDFIRE, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringBydDFIRE();
    sf->Init("");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
  }
  if (config->getValue(STR_ENERGY_WEIGHT_DOPE_PW, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByDopePW();
    //sf->Init("./config/dope-PW.cfg");
    sf->Init("./config/dope-PW-analysis.cfg");
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
    mScoringFunction = new ScoringByDOPE();
    mScoringFunction->Init("./config/dope.par");
  }
  if (config->getValue(STR_ENERGY_WEIGHT_EPAD, &weight) && weight>0) {
    ScoringFunction* sf = new ScoringByEPAD();
    sf->Init("./config/epad.cfg", additional_f);
    mScoringFunctionList.push_back(pair<ScoringFunction*, double>(sf, weight));
    NEFF = sf->NEFF;
  }

  cerr << "SetScoringFunctions Done:)"<<endl;
  if (mScoringFunction==NULL)
    mScoringFunction = new ScoringByRadius();
}

void ConformationOptimizer::Initialize(
        int randomSeed, int DEBUG_MODE, string additional_f)
{
  //mNumDecoyPerCycle=1000;
  //mSSAcceptThreshold=0.6;

  this->randomSeed = randomSeed;
  this->DEBUG_MODE= DEBUG_MODE;
  this->additional_f=additional_f;

  for(int i=0;i<100;i++)
    for(int j=0;j<100;j++)
      for(int k=0;k<100;k++)
        ProbT[i][j][k]=0, Prob2[i][j]=0;
  ifstream ProbIn("probT.txt");
  int x,y,z;
  double p;
  while(ProbIn >> x >> y >> z >> p){
    ProbT[x][y][z]=p;
  }
  ProbIn.close();
  ifstream ProbIn1("2probT.txt");
  while(ProbIn1 >> x >> y >> p){
    Prob2[x][y] = p;
    //cerr<<"--------"<<p<<endl;
  }
  ProbIn.close();
  
  //create a CRFSampler
  if (!bMergePDB)
    mCRFSampler=new CRFSampler();
  string crfModelDir;
  string crfOptionFile;
  string crfFeatureTemplateFile;
  cerr << "void ConformationOptimizer::Initialize()" << endl;

  //needs to check errors here
  int ret=config->getValue(STR_CRF_MODEL_DIR,&crfModelDir);
  ret &=config->getValue(STR_CRF_OPTION_FILE,&crfOptionFile);
  ret &=config->getValue(STR_CRF_FEATURE_TEMPLATE_FILE,&crfFeatureTemplateFile);

  ///////// add in the initialization of fragLen, libLen and interval
  ret &=config->getValue(STR_FRAGLEN, &fragLen);
  ret &=config->getValue(STR_LIBLEN, &libLen);
  ret &=config->getValue(STR_INTERVAL, &interval);
  ret &=config->getValue(STR_FRAGLIBFILE, &fragLibFile);

  if (!ret){
    cerr << "Fatal error: please make sure the CRF model setting is correct!"
         << endl;
    exit(0);
  }

  cerr << "read fragment lib: " << fragLibFile << endl;
  readLib();
  cerr << "crfModelDir=" << crfModelDir << "; crfOptionFile=" << crfOptionFile
       << "; crfFeatureTemplateFile=" << crfFeatureTemplateFile << "\n";
  if (mCRFSampler)
    mCRFSampler->InitSampler(crfModelDir, crfOptionFile,
                             crfFeatureTemplateFile);

  setScoringFunction();

  mBBQ = new BBQ(crfModelDir+"/BBQ.dat");
  mpRamaTyp = new RamaTypology;
  mpRamaTyp->LoadFromFile("./config/RamaTypology.par");

  ReadSideChainComm(crfModelDir+"/config/sidechain.comm");
  
  loadSequenceAndSS();
  
  cerr << "STR_PRETAG_PATH=" << STR_PRETAG_PATH << "\n";
  string pretagPath;
  ret = config->getValue(STR_PRETAG_PATH, &pretagPath);

  if (!ret){
    cerr << "Fatal error: please make sure the pretag path is correctly set!"
         << pretagPath <<endl;
    exit(0);

    //in this case, all the features of the CRF model can only be binary values
    if (mCRFSampler)
      mCRFSampler->Initialize(mProteinSeq, mSecStruSeq);
  } else {
    mPretagFile=pretagPath+"/"+mProteinName+STR_PRETAG_FILE_SUFFIX;
    //the pretag file can contain both binary or real-valued features
    if (mCRFSampler)
      mCRFSampler->Initialize(mProteinSeq, mSecStruSeq,mPretagFile);
  }
  cerr << "checkpoint. NEFF=" << NEFF << endl;
  if (mCRFSampler){
    mCRFSampler->UseAllowedSet(1);
  }
  mLabel= vector<string>(mProteinSize);
  //mLabelSave= vector<string>(mProteinSize);
  mBestLabel= vector<string>(mProteinSize);
  mStructure=vector<Vector3 >(mProteinSize);
  //mStructureSave=vector<Vector3 >(mProteinSize);
  mBestStructure=vector<Vector3 >(mProteinSize);
  
  ret=config->getValue(STR_MAXIMUM_ITERATION,&mMaxIteration);
  ret=config->getValue(STR_START_TEMPERATION,&mSA_StartTemperature);
  ret=config->getValue(STR_END_TEMPERATION,&mSA_EndTemperature);
  ret=config->getValue(STR_ANNEAL_STEPS,&mAnnealSteps);
  ret=config->getValue(STR_STOP_ESP_STEP,&mStopEspSteps);

  ret=config->getValue(STR_STARTING_WEIGHTED_SIMULATION_STEP,
                       &mStartingWeightedSimulationStep);
  ret=config->getValue(STR_SIMULATION_WEIGHT_ALPHA,&mSimulationWeightAlpha);
  ret=config->getValue(STR_SIMULATION_WEIGHT_BETA,&mSimulationWeightBeta);
  ret=config->getValue(STR_SIMULATION_WEIGHT_COIL,&mSimulationWeightCoil);

  int i=2, total=0;
  for ( ; i<mProteinSize-2; i++) {
    if (mSecStruSeq[i]=='H')
      total+=mSimulationWeightAlpha;
    else if (mSecStruSeq[i]=='E')
      total+=mSimulationWeightBeta;
    else 
      total+=mSimulationWeightCoil;
  }
  double prob=1.0/total, cdf=0.0;
  for (i=2; i<mProteinSize-2; i++) {
    mProbAsSegmentStart[i]=cdf;
    
    if (mSecStruSeq[i]=='H')
      cdf+=prob*mSimulationWeightAlpha;
    else if (mSecStruSeq[i]=='E')
      cdf+=prob*mSimulationWeightBeta;
    else 
      cdf+=prob*mSimulationWeightCoil;
  }

  int sizeNative = mStructureNative.size();
  if (sizeNative>0){
    fragNative= new double*[sizeNative];
    for(int j = 0; j < sizeNative; j++) {
      fragNative[j] = new double[3];
      fragNative[j][0] = mStructureNative[j].x;
      fragNative[j][1] = mStructureNative[j].y;
      fragNative[j][2] = mStructureNative[j].z;
    }
  }
  cerr << "ConformationOptimizer::Initialize() done. START_TEMPERATION=" 
       << mSA_StartTemperature << " MAXIMUM_ITERATION=" << mMaxIteration<<endl;
}

void ConformationOptimizer::printVi(int pos)
{
  mCRFSampler->printVi(pos);
}

int ConformationOptimizer::GetNativeConformation(string pdb_file)
{
  mStructureNative.clear();
  Protein* protein = new Protein;
  // read the pdb protein file
  PDBFile* pdb=0;
  try{
    pdb = new PDBFile(pdb_file, std::ios::in);
  }catch(Exception::FileNotFound){
    cerr<<"Can not find protein file "<<pdb_file <<endl;
    return -1;
  }

  *pdb >> *protein;
  cerr << "protein data read with " << protein->countChains() << " chains; "
       << protein->countResidues() << " residues.\n";

  ResidueIterator resIter=protein->beginResidue(); 
  string startID=resIter->getID();
  mNativeStart = atoi(startID.c_str()); cerr << "###startID=" << mNativeStart;
  for( ; resIter!=protein->endResidue(); resIter++){
    string res_name= resIter->getFullName(Residue::NO_VARIANT_EXTENSIONS);
    if (res_name.size()<3)
      break; //continue;
    for(AtomIterator at=resIter->beginAtom();at!=resIter->endAtom();at++){
      Vector3 pos = at->getPosition();
      string atom_name = at->getName();
      if (atom_name=="CA")
        mStructureNative.push_back(pos);
    }
  }

  if (protein) delete protein;
  if (pdb) delete pdb;

  cerr << "getConformation done." << endl;
  return 1;
}

int ConformationOptimizer::readPSPFile(string pspname, vector<string>& str_vec)
{
  cerr << "reading psp file: " << pspname << endl;
  str_vec.clear();
  ifstream idataf(pspname.c_str());
  if (!idataf.is_open()) {
    cerr << "Can't open file: " << pspname<< endl;
    return 0;
  }
  string line;
  getline(idataf, line);
  getline(idataf, line);
  getline(idataf, line);
  while (getline(idataf, line)) {
    if (line.length()<150)
      continue;
    //cerr << "line: " << line << "\n";
    //string psp = line.substr(6,2) + line.substr(71,80); // using the count
    //string psp = line.substr(11,60); // using the matrix PSSM
    string psp = line.substr(71,80);
    //cerr << "psp: " << psp << "\n";
    str_vec.push_back(psp);
  }
  return 1;
}

void ConformationOptimizer::loadSequenceAndSS(){
  //read in seq and SS
  string seqPath, ssPath;
  int ret=config->getValue(STR_SEQ_PATH, &seqPath);
  ret &=config->getValue(STR_SS_PATH, &ssPath);
  if (!ret){
    cerr << "Fatal error: please make sure the seq and SS paths are correctly set!"
         << endl;
    exit(0);
  }

  string seqFile=seqPath+"/"+mProteinName+STR_SEQ_FILE_SUFFIX;

  ifstream seqIn(seqFile.c_str());
  if (!seqIn){
    //report err
    cerr<<"failed to open sequence file "<<seqFile<<" for read!"<<endl;
    exit(0);
  }

  //a  protein sequence can be saved in a multiple-line file
  string tmpStr="";
  while(getline(seqIn, tmpStr)){
    trim(tmpStr);
    if (tmpStr.empty()) continue;
    if (tmpStr[0]=='>') continue;
    if (tmpStr[0]=='#') continue;
    mProteinSeq+=tmpStr;
    tmpStr.clear();
    //break;
  }
  seqIn.close();

  string ssFile=seqPath+"/"+mProteinName+STR_SS_FILE_SUFFIX;

  ifstream ssIn(ssFile.c_str());
  if (!ssIn){
    //report err
    cerr<<"failed to open secondary structure file "<<ssFile<<" for read!"<<endl;
    exit(0);
  }

  //a secondary structure sequence can be saved in a multiple-line file
  tmpStr.clear();
  while(getline(ssIn, tmpStr)){
    trim(tmpStr);
    if (tmpStr.empty()) continue;
    if (tmpStr[0]=='>') continue;
    if (tmpStr[0]=='#') continue;
    mSecStruSeq+=tmpStr;
    tmpStr.clear();
    //break;
  }

  ssIn.close();

  //for debug
  cerr<<"sequence=\n\t"<<mProteinSeq<<endl;
  cerr<< ssFile << ": secondary structure=\n\t"<<mSecStruSeq<<endl;

  //check
  if (mProteinSeq.size()!=mSecStruSeq.size()){
    cerr << "FATAL: the protein sequence and its secondary structure seq "
         << "do not have the same size!" << endl;
    exit(0);
  }
  mProteinSize=mProteinSeq.size();

  string horizFile=seqPath+"/"+mProteinName+STR_HORIZ_FILE_SUFFIX;
  ifstream horizIn(horizFile.c_str());
  if (!horizIn){
    //report err
    cerr << "failed to open secondary structure file " << horizFile 
         << " for read!" << endl;
    exit(0);
  }

  //a secondary structure sequence can be saved in a multiple-line file
  tmpStr.clear();
  memset(mSSConfidenceSeq, 0, 4096*sizeof(int));
  int seq=0;
  while(getline(horizIn, tmpStr)){
    trim(tmpStr);
    if (tmpStr.empty()) continue;
    if (tmpStr[0]=='>') continue;
    if (tmpStr[0]=='#') continue;
    if (strncmp(tmpStr.c_str(), "Conf: ", 6)==0) {
      char digit[2];
      memset(digit, 0, 2*sizeof(char));
      for (int i=6; i<(int)tmpStr.size(); i++) {
        strncpy(digit, &tmpStr[i], 1);
        mSSConfidenceSeq[seq++]=atoi(digit);
      }
    }  
    tmpStr.clear();
    //break;
  }
  cerr << "Confidence: \n\t";
  for (int i=0; i<(int)mSecStruSeq.size();i++)
    cerr << mSSConfidenceSeq[i];
  cerr << endl;
  horizIn.close();  
/* ESP is temporarily not used
  string pspname=seqPath+"/"+mProteinName+".psp";
  readPSPFile(pspname, mPSPSeq);
*/
}

void ConformationOptimizer::SampleASegment(int* start, int* end){
  //start and end can be at any positions
  *start=random()%mProteinSize;
  int segLen=random()%MAX_SEG_LENGTH;
  *end= (*start)+segLen;
  if ((*end) > mProteinSize-1) 
    *end=mProteinSize-1;
}

void ConformationOptimizer::BuildStructure(int start, int end){
  cerr << "ConformationOptimizer::BuildStructure(int start, int end) not "
       << "implemented"<<endl;
  exit(-1);
}

void ConformationOptimizer::SampleLabels(int start, int end){
//  cerr <<"ConformationOptimizer--SampleLabels(start,end)"<<endl;
  mLabel=mBestLabel;
     mCRFSampler->Sample(mLabel, start, end);
  mLabel[mProteinSize-1]="d0";
  mLabel[0] = "d0";
  mLabel[1] = "d0";
}


//this function is incorrect, please donot use it anymore 
void ConformationOptimizer::GreedySearch(string outdir, int numRepeats){
  ScoringBySS ssFilter;
  double ssAcceptThreshold=0;
  
  int ret=config->getValue(STR_SS_FILTER_THRESHOLD,&ssAcceptThreshold);
  if (!ret){
    cerr<<"cannot find value for ssAcceptThreshold!"<<endl;
    exit(-1);
  }

  vector<Vector3 > structureSave;
  double bestSSScore=MINFLOAT;
  
  for(int i=0; i< numRepeats; i++){
    GreedySearch(outdir);
      
    //ScoringFunction always assume that the smaller a score, the better.
    //However here the bigger the ssScore, the better.
    double ssScore=ssFilter.evaluate(mProteinSeq, mSecStruSeq, mBestStructure);
    ssScore*=-1.0;

    if (ssScore > bestSSScore){
      bestSSScore=ssScore;
      structureSave=mStructure;
    }
    if (bestSSScore > ssAcceptThreshold) break;
  }
  mStructure=structureSave;
}

double ConformationOptimizer::estimateNativeScore(string& sequence, string& ss)
{
  if ( mScoringFunction)
    // we only have radius now
    return mScoringFunction->estimateNativeScore(sequence, ss); 
  else return -10000000;

  double ret = 0;
  for (int i=0; i<(int)mScoringFunctionList.size(); i++) {
    ScoringFunction* sf = mScoringFunctionList[i].first;
    double weight = mScoringFunctionList[i].second;
    ret +=sf->estimateNativeScore(sequence, ss)*weight;
  }  
  return ret;
}
/*
double ConformationOptimizer::evaluate(string& sequence, string& ss,
                                       vector<Vector3 >& structure)
{
  if ( mScoringFunction)
    // we only have radius now
    return mScoringFunction->estimateNativeScore(sequence, ss);
  double ret = 0;
  for (int i=0; i<mScoringFunctionList.size(); i++)
  {
    ScoringFunction* sf = mScoringFunctionList[i].first;
    double weight = mScoringFunctionList[i].second;
    ret +=sf->evaluate(sequence, ss, structure)*weight;
  }
  return ret;
}

double ConformationOptimizer::evaluate(
              string& sequence, string& ss, Protein* protein)
{
  double ret = 0;
  for (int i=0; i<mScoringFunctionList.size(); i++)
  {
    ScoringFunction* sf = mScoringFunctionList[i].first;
    double weight = mScoringFunctionList[i].second;
    ret +=sf->evaluate(sequence, ss, protein)*weight;
  }
  return ret;
}
*/
double ConformationOptimizer::evaluate(
         string& sequence, string& ss, vector<RESIDUE >& structure, int start)
{
  double ret = 0;
  double dif;
  struct timeval ts, te;
  if (DEBUG_MODE){
    //gettimeofday(&ts, 0);
  }
  for (int i=0; i<(int)mScoringFunctionList.size(); i++) {
    ScoringFunction* sf = mScoringFunctionList[i].first;
    if (mCurrentSimulationStep>mStopEspSteps && sf->mName=="ESP_NEW")
      continue;
    double weight = mScoringFunctionList[i].second;
    double energy = sf->evaluate(sequence, ss, structure, start)*weight;
    //cerr << sf->mName <<" energy="<<energy/weight << "; weight="<<weight<<endl;
    if (mScoringFunction && (sf->mName=="dopePW" || sf->mName=="dopebeta")){
      new_rg = sf->rg;
      new_ru = sf->ru;
      new_br = sf->br;
      //mHBEnergy = energy;
      //mBurialPenalty = sf->mBurialPenalty*weight;

      // for small proteins, give more weight
      if (structure.size()<70 || NEFF>0&&NEFF<2.05)
        energy=energy*7; // 4->8->7
      mBurialPenalty = energy;
    }
    if (sf->mName=="BMK" || sf->mName=="DOPEX" || sf->mName=="dopePW") {
      mHBEnergy = sf->mHBEnergy*weight;
      //mBurialPenalty = sf->mBurialPenalty*weight;
    } else if (sf->mName=="contact"){
      mContactEnergy = energy;
    } else if (sf->mName=="EPAD"){
      if (NEFF>0 && NEFF<2.05)
        energy=0; //energy * 0.002;
      mEPADEnergy = energy;
      //energy for other backbone pairs than Ca-Ca
      //mContactEnergy = sf->mHBEnergy*weight;
    }
    // cerr << "\t\t" << sf->mName << " energy=" << energy 
    //      << "; Burial=" << mBurialPenalty << "; mHBEnergy=" << mHBEnergy
    //      <<"; weight="<<weight<<"; ret="<<ret << endl;
    if (DEBUG_MODE){
      //gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
      // cerr << "        Energy("<<sf->mName<<"): "
      //      << dif*1000+te.tv_usec/1000-ts.tv_usec/1000 << " miliseconds." 
      //      << endl;
      //gettimeofday(&ts, 0);
    }
    ret +=energy;

  }
  return ret;
}

void ConformationOptimizer::readLib(){
  // should add fragment in class def
  fragments = new double**[libLen];
  for(int i = 0; i < libLen; i++) {
    fragments[i] = new double*[fragLen];
    for(int j = 0; j < fragLen; j++) {
      fragments[i][j] = new double[3];
    }
  }
  char buf[4096];
  ifstream libin(fragLibFile.c_str());
  //debug
  if(!libin.is_open()) {
    cerr << " Can not open the fragment library!" << endl << endl;
    exit(1);
  }

  cerr << "============= start to read lib======" << endl;
  //skip header
  for(int i = 0; i < 20; i++)
    libin.getline(buf,4096);
  //read fragments
  for(int i = 0; i < libLen; i++){
    //while(libin.getline(buf,4096) && buf[0]!=' ');
    //for(int j = 0; j < 6; j++)
    //   libin.getline(buf,4096);

    for(int j = 0; j < fragLen; j++) {
      while(libin.getline(buf,4096) && (buf[0]=='*' || buf[3] =='-'));

      sscanf(buf, "%lf%lf%lf", &fragments[i][j][0], &fragments[i][j][1],
             &fragments[i][j][2]);
    }
  //  cerr << endl;
  }
  libin.close();
}

#define SQUARE(x) ((x)*(x))

// Given a structure, assign labels to this structure
void ConformationOptimizer::ReLabel(int start, int end)
{
  cerr << "ReLabel" << endl;
  double **frag = new double*[fragLen];
  for(int j = 0; j < fragLen; j++)
    frag[j] = new double[3];
  for (int i = start; i <= end; i++) {
    //cerr << "relabel " << i << "; fragLen=" << fragLen << "; end=" << end << endl;
    // Assign Label
    for(int j = 0; j < fragLen; j++) {
        // build the frag of length, whose center is the current target residue
        frag[j][0] = mStructure[i-2+j].x;
        frag[j][1] = mStructure[i-2+j].y;
        frag[j][2] = mStructure[i-2+j].z;
    }
    double min_rmsd = 10000.;
    int index = 0;
    //find the most similar fragments
    //cerr << "find the most similar fragments: libLen=" << libLen<< endl;
    for(int k = 0; k < libLen; k++) {
            double rmsd = 10000.;
      RMSD(fragments[k], frag, fragLen, &rmsd);
      if(rmsd < min_rmsd) {
        index = k;
        min_rmsd = rmsd;
      }
          }
          char buf[100];
          sprintf(buf,"f%d",index);
          mLabel[i] = buf;
    cerr << " " <<  buf <<"("<<min_rmsd << ") ";
  }
  for(int j = 0; j < fragLen; j++)
    delete frag[j];
  delete frag;
}
double ConformationOptimizer::calcLSP(vector<RESIDUE >& structure, bool relabel_only)
{
  int start=2;
  int end=mProteinSize-3;
  double lsp;  

  // Calculate Radius  
  vector<Vector3 > g_backbone;
  for (int i=0; i<(int)structure.size(); i++) {
    ATOM CA = structure[i].getAtom("CA"); 
    g_backbone.push_back(Vector3(CA.x, CA.y, CA.z));
  } 
  double dist=0;
  for(int i=0; i<(int)g_backbone.size();i++)
    for(int j=0; j<i; j++)
      dist+=g_backbone[i].getSquareDistance(g_backbone[j]);
  dist/=(SQUARE(g_backbone.size())-g_backbone.size());
  dist=sqrt(dist);

  for(int i = 0; i <(int)structure.size(); i++) {
    ATOM atm=structure[i].getAtom("CA");
    //cerr << i << ": atom " << atm.name <<endl;
    mStructure[i].x = atm.x;
    mStructure[i].y = atm.y;
    mStructure[i].z = atm.z;
  }
  mLabel[0]="d0";
  mLabel[1]="d0";
  mLabel[mProteinSize-2]="f5";
  mLabel[mProteinSize-1]="f5";
  ReLabel(start, end);
  if (relabel_only){
    return 0;
  }
  //cerr << "calc_LSP" << endl;
  double bestScore= 0;
  if (mCRFSampler){
    double logp=calc_RefLog_P();
    lsp = -mCRFSampler->calc_LSP(mLabel) + logp;  
    bestScore= dist + 0.0024*lsp; // Radius + LSP
    cerr << " score=" << bestScore << "; dist=" << dist 
         << "; lsp=" << lsp << "; logp="<<logp << endl;
  }
  return bestScore;
}

void ConformationOptimizer::GreedySearch(string outdir){

  int start=0;
  int end=mProteinSize-1;
  double lsp;  

  // find the first feasible conformation, i.e., a structural model without
  // steric clashes
  int minNumClashes=MAXINT;
  do
  {
    SampleLabels(start, end);
    SampleAngles(start, end);
    BuildStructure(start,end);

    int numClashes=countClashes();
    //cerr<<"numClashes="<<numClashes<<" minNumClashes="<<minNumClashes<<endl;
    if (numClashes < minNumClashes ){
      minNumClashes=numClashes;
      KeepStructure();
    }else{
      //Undo(start,end);
    }
    if (minNumClashes==0) break;

    //insert code here to random sample start and end positions
    SampleASegment(&start, &end);  

  } while(minNumClashes>0);

  vector<RESIDUE> protein0;
  GetConformation(mProteinSeq, mStructure, protein0);
  //double energy = calcEnergy2(protein0);
  protein0.clear();

  cerr <<"One conformation without clashes found! #"<<mMaxIteration<<endl;

  double mNativeScore=estimateNativeScore(mProteinSeq,mSecStruSeq); // for radius
  double scale = mNativeScore;
  //mNativeScore = scale - 0.005*mProteinSize;

  //lsp = -mCRFSampler->calc_LSP(mLabel) + calc_RefLog_P();  
     
  double bestScore=calcEnergy();// + 0.0024*lsp; // Radius + LSP
  //energy += bestScore * weight_radius;  
  
  //find the optimal conformation by minimizing the score
  int maxNumTrials=10000; // the number of trials that have no improvement at all

  int count=0, total_cnt=0;
  while(count<=maxNumTrials&&total_cnt<mMaxIteration) {
    // cerr << count<<" vs. " <<maxNumTrials << " and " 
    //      << total_cnt << " vs. " << mMaxIteration << endl;
    //insert code here to random sample start and end position
    SampleASegment(&start, &end);  
    
    SampleLabels(start, end);
    SampleAngles(start, end);
    BuildStructure(start,end);
    
    bool clash=detectClashes();
    bool recover=true;
    if(!clash)
    {
      //lsp = -mCRFSampler->calc_LSP(mLabel) + calc_RefLog_P();  
      //cerr << "LSP : " << lsp << endl;
      //score = score + (0.0024*lsp);  // Combine the two scores together
      
      // Instead we use energy functions to compute the new score
      vector<RESIDUE> protein;
      GetConformation(mProteinSeq, mStructure, protein);
      double score=calcEnergy(); // Radius
      //double new_energy = calcEnergy2(protein);
      protein.clear();
  
      //new_energy += score * weight_radius;  

      if(score < bestScore)
      //if(new_energy < energy)
      {
        cerr << " trial#"<<count<<";  bestScore="<<bestScore
             << "; newScore="<<score<<endl;
        recover=false;
        bestScore=score;
        KeepStructure();

        //reset the count
        count=0;
      }//if
      if (score < mNativeScore) break;
    }else{
      // cerr <<"steric clashes found in the structural model"<<endl;
    }
    if (!recover) continue;

    //rollback to the previous conformation
    //Undo(start, end);

    count++;
    total_cnt++;

  }//while
  for(int i =0; i<mProteinSize;i++)
    cout <<i<<":"<< mLabel[i] << " " ;
  cout << endl;
  cout<<"Find a structural model with score="<<bestScore<<endl;
  
  writeDecoy(outdir);
}

void ConformationOptimizer::KeepStructure(){
  cerr<<"KeepStructure() not implemnted for ConformationOptimizer!"<<endl;
  exit(-1);
}

void ConformationOptimizer::MCMCSearch(
           string outdir, int bSimulatedAnnealing, double AnnealFact)
{
  int start=0;
  int end=mProteinSize-1;

  mJumpFactor=1.0;
  mCurrentSimulationStep = 0;

  // The Following code in generating the initial potentially non-clashed
  // decoy is the same as it is in GreedySearch
  // This decoy will be the basebone in the Monte Carlo Search step.

  // find the first feasible conformation, i.e., a structural model without
  // steric clashes
  int minNumClashes=MAXINT;
  cerr.setf(ios::fixed,ios::floatfield);
  cerr.precision(4);
  cerr << "Inital Labels("<< mLabel.size()<<"): ";
  for (int i=0; i<(int)mLabel.size(); i++)
    cerr << mLabel[i] << "-" << mBestLabel[i] << " ";
  if ((int)refine_segments.size()>0){
    mBestLabel=mLabel;
    KeepStructure();
    for (int i=0; i<(int)refine_segments.size(); i++){
      start = refine_segments[i].first; end=refine_segments[i].second;
      cerr << endl << "\tsample segment:" << start<<"-"<<end<<endl;
      SampleLabels(start, end);
      SampleAngles(start, end);
      BuildStructure(start,end);
      KeepStructure();
    }
    //writeDecoy(outdir); exit(0);
  }

  do {
    SampleLabels(start, end);
    SampleAngles(start, end);
    BuildStructure(start,end);
    
    int numClashes=countClashes();
    if (numClashes < minNumClashes ){
      minNumClashes=numClashes;
      KeepStructure();
    }
    cerr << "numClashes=" << numClashes 
         << "; and minNumClashes=" << minNumClashes << endl;
    if (refine_segments.size()>0 && minNumClashes<=4)
      break;
    if (minNumClashes==0) break;
    
    //insert code here to random sample start and end positions
    SampleASegment(&start, &end);  
  
  } while(minNumClashes>0);

  cerr <<"One conformation without clashes found!"<<endl;

  //writeDecoy(outdir);
  ApplyContactConstraint();
  KeepStructure();
  cerr << "Finish constrains" << endl;
  //writeDecoy(outdir); 
  //cerr << "finish writing new structure" << endl;

  // Monte Carlo Search
  // Note that each conformation sampled is considered as a state
  vector<RESIDUE> protein0;
  GetConformation(mProteinSeq, mStructure, protein0);
  cerr << "Conformation built." << endl;
  double energy = calcEnergy2(protein0, 0);
  cerr << "First_Energy=" << energy << "; Burial=" << mBurialPenalty 
       << "; mHBEnergy=" << mHBEnergy << endl;
  protein0.clear();

  double mNativeScore=estimateNativeScore(mProteinSeq,mSecStruSeq);
  double scale = mNativeScore;
  mNativeScore = scale - 0.005*mProteinSize;

  //exit(0);
  //double lsp = -mCRFSampler->calc_LSP(mLabel) + calc_RefLog_P();  
  //double bestScore=calcEnergy(); //+ 0.0024*lsp; // Radius + LSP
  //cerr << "BestScore="<< bestScore<<"; weight_radius="<<weight_radius << endl;
  
  //energy += bestScore*weight_radius;

  //find the optimal conformation by minimizing the score
  double new_energy;
  // the number of trials that have no improvement at all
  int maxNumTrials=6000; //1000; 
  int count=1;

  double tcoeff  = 1;
  double Temp1, Temp0; 
  // Initial and final temperature. If the annealing mode is disabled,
  // Temp1 is the temperture througouth the entire simulation.
  Temp1 = mSA_StartTemperature; // exp(-10/kb/t1)=0.5==>t~72K
  //Temp0 = 380;
  Temp0 = mSA_EndTemperature;
  if (bSimulatedAnnealing) {
    // when perform Simulated Annealing, maxNumTrials stands for the total
    // steps allowed for the simulation according to Andres Colubi's paper,
    // 4000 steps is a converging step for optimal RMSD and DOPE
    if(refine_segments.size()==0) 
      maxNumTrials = mMaxIteration; //10000;
    else
      Temp1/=50;
  }
  cerr << "num of refine_segments=" << refine_segments.size() 
       << "; Start Temperature=" << Temp1 << endl;
  
  //maxNumTrials = 2500; // test only. remember to comment it out.

  double temp_annealing = Temp1;
  int lastCount=0;
  int lastFactorCount = 0;
  while(count<=maxNumTrials && (temp_annealing-Temp0)>Temp0/200) {
    //insert code here to random sample start and end position
    SampleASegment(&start, &end);  

    SampleLabels(start, end);
    SampleAngles(start, end);
    BuildStructure(start,end);

    if (bSimulatedAnnealing && (count % mAnnealSteps == 0)) {
      // The temperature is modified using the classic exponential schedule.
      // It is an decrease/increase whether Temp1 > Temp0 or Temp0 > Temp1.
      tcoeff *= AnnealFact;
      temp_annealing = (Temp1 - Temp0) * tcoeff + Temp0;
      cerr << "New temperature: " << temp_annealing 
           << "; mJumpFactor="<<mJumpFactor<<"; best Energy="<<energy <<endl;
      if ((count-lastCount)>mAnnealSteps*20 || mJumpFactor>512)
        break;
    }
    if (bSimulatedAnnealing && (count-lastFactorCount)>=mAnnealSteps*1) {
      mJumpFactor*=2;
      lastFactorCount = count;
    }
    int numClashes=countClashes();
    bool clash=detectClashes();
    bool recover=true;
    //double score;
    if(!clash || (refine_segments.size()>0 && numClashes <=minNumClashes)) {
      if(numClashes < minNumClashes){
        minNumClashes=numClashes;
        cerr << "minNumClashes decrease to " << minNumClashes << endl;
      }
      //lsp = -mCRFSampler->calc_LSP(mLabel) + calc_RefLog_P();  
      //cerr << "LSP : " << lsp << endl;
      //score = calcEnergy();// + (0.0024*lsp);  // radius + LSP

      // Manto Carlo sampling
      vector<RESIDUE> protein;
      // Adding heavy atoms, and H
      GetConformation(mProteinSeq, mStructure, protein);
      // Compute the DOPE, TSP, ESP energies
      new_energy = calcEnergy2(protein, start);
      protein.clear();
      
      //new_energy += score*weight_radius;

      // Accept the state with lower engergy
      // Otherwise, sample to accept the new state according to the
      // distribution of P(y)/P(y')
      // As is implemented in MCSacceptMove()
      double temperature = Temp1;
      if (bSimulatedAnnealing)
        temperature= temp_annealing;
      if (MCSacceptMove(energy, new_energy, temperature)) {
        cerr << " Riteration#"<<count<<"; HB="<<mHBEnergy
             << "; BurialPenalty="<<mBurialPenalty<<"; bestEnergy=" << energy
             <<"; newEnergy="<<new_energy<<"; ContactE=" << mContactEnergy 
             << "; EPAD=" << mEPADEnergy<<endl;
        //if (energy>new_energy)
        {
          energy = new_energy;
          lastCount = count;
          lastFactorCount = count;
          if (mJumpFactor>1.9)
            mJumpFactor/=2;
          else
            mJumpFactor = 1.0;
        }

        KeepStructure();

        if (!bSimulatedAnnealing) {
          recover=false;
          count=0; //reset the count
        }
      }
    }
    if (!recover) continue;

    count++;
    mCurrentSimulationStep = count;
    //cerr << "Running iteration #" << count << "; best_energy=" << energy
    //     << "; new_energy=" << new_energy << endl;
  }//while

  time_t tloc;
  time(&tloc);
  //cerr << "Find a structural model with energy=" << energy 
  //     << " after running " << count << "iterations. ";
  cout << "energy= " << energy <<"; HB="<<mHBEnergy
       << "; BMKPenalty="<<mBurialPenalty<<" ";
  cout << ctime(&tloc);// << endl;
  mCurrentSimulationStep = 0;
  writeDecoy(outdir);
}

// The acceptance rule for the MC simulation.
// return true for accepting the curr state
bool ConformationOptimizer::MCSacceptMove(
             double prev, double curr, double temperature)
{
  // /200 (this means the temperature could be 30K high);
  double diff = (prev - curr)/mJumpFactor;
  if (diff >= 0)
    return true;
  else { 
    // The Boltzman constant ( Ideal gas constant), in Kcal/(mol K).
    // #define Kb 0.00198576 
    // temperature = 1.0/(Kb * temper);
    //double e = exp(diff*temperature);
    double Kb = 0.00198576; 
    double e = exp(diff/(Kb*temperature));
    double r = drand48();
    //cerr << "probability=" << e << " and r= " << r << "; diff=" << diff<<endl;
    if (r < e)
      return true;
    else
      return false;
  }
}

class Conformer {
public:
  vector<Vector3 > Vec;
  vector<Vector3 > Structure;
  vector<string> Label;
  vector<Vector3 > mBestFBVec;
  double energy;
  double HBenergy, EPADEnergy;
  double burial, ru, br, rg;
  int segStart, segEnd;
  int a; // record how many round we sample the angle given the label
};

double ConformationOptimizer::calcRMSD()
{
  if (mStructureNative.size()==0)
    return 0;
  //cerr << "##calcRMSD";
  double **frag= new double*[mProteinSize];
  for(int j = 0; j < mProteinSize; j++)
    frag[j] = new double[3];
  int fragLen=min((int)mStructureNative.size(), mProteinSize-mNativeStart);
  //cerr << " fragLen=" << fragLen;
  for(int j = 0; j < fragLen; j++) {
    frag[j][0] = mBestStructure[mNativeStart-1+j].x;
    frag[j][1] = mBestStructure[mNativeStart-1+j].y;
    frag[j][2] = mBestStructure[mNativeStart-1+j].z;
  }
  double rmsd = 10000.;
  RMSD(fragNative, frag, fragLen, &rmsd);
  for(int j = 0; j < mProteinSize; j++)
    delete frag[j];
  delete frag;
  //cerr << " RMSD=" << rmsd << endl;
  return rmsd;
}

void ConformationOptimizer::ReplicaExchange(string outdir){
  //LoadInitStructure("/home/jpeng/MultiTemplate/Experiments/InitPDB/1ctfA.pdb");
  
  int  num_rep, num_sampling;
  double T_low,T_high; 
  
  num_rep = 20;

  double max_energy = 10000000;
  Conformer tmp, BestConformer;
  BestConformer.energy = max_energy;
  vector<Conformer> BestConformers;
  num_sampling = mAnnealSteps/8; // 30 times
  int Max_Iters = mMaxIteration/num_sampling;// /num_rep; //12000/20/30=20
  T_low = mSA_EndTemperature/200;
  T_high = mSA_StartTemperature/500;

  double estimatedRg=2.2*pow(mProteinSize, 0.38);

  vector<Conformer> Replica(num_rep);
  vector<double> Temp;
  
  double temperatures[]={0.04, 0.062, 0.082, 0.12, 0.22, 1.256, 6.912,
                         10.368, 15.552, 21.77, 30.48, 39.62, 51.51, 61.81,
                         74.18, 81.59, 88.12, 92.53, 97.15, 100.0};

  cerr.precision(5);
  cerr << "Inital Labels("<< mLabel.size()<<"): ";
  //cerr << mProteinSize << endl;
  //double init_e = calcEnergy();
  //cerr << "Init Energy = " << init_e << endl;
  cerr << "T_high=" << T_high << "; T_low=" << T_low 
       << "; estimated Rg=" << estimatedRg << endl;
  //Initialize the temperatures (rescaled), linear or exponential
  for(int i=0;i<num_rep;i++){
    //Temp.push_back(T_low + (T_high - T_low) * i / (num_rep - 1));
    Temp.push_back(temperatures[i]);
    cerr << "REP: " << i << "; Temperature=" << Temp[i] 
         << "; Energy="<< Replica[i].energy <<endl;
    Replica[i].Label.resize(mProteinSize);    
    Replica[i].Vec.resize(mProteinSize);    
    Replica[i].Structure.resize(mProteinSize);    
  }  
  int start = 0;
  int end = mProteinSize - 1;

/*
  int th_id, nthreads;
#pragma omp parallel private(th_id)
{
     th_id = omp_get_thread_num();
     printf("Hello World from thread %d\n", th_id);
     #pragma omp barrier
     if ( th_id == 0 ) {
       nthreads = omp_get_num_threads();
       printf("There are %d threads\n",nthreads);
     }
*/
  // Find the first batch of conformers
  // here we donot use time() to get the random seed, instead we use the
  // system kernel state to generate a seed
  struct timeval ts, te;
  unsigned id=getpid();
  ifstream in("/dev/urandom",ios::in);
  in.read((char*)&randomSeed, sizeof(unsigned)/sizeof(char));
  in.close();
  gettimeofday(&ts, 0);
  randomSeed=randomSeed*randomSeed+id*id+ts.tv_usec;
  srand(randomSeed);
  srandom(randomSeed);

  // initialize for each replica, Random Sampling
  start = 0;
  end = mProteinSize - 1;
  mBestLabel.resize(mProteinSize);    
  mBestFBVec.resize(mProteinSize);    
  mBestStructure.resize(mProteinSize);    
  //mBestFBVec = Replica[i].Vec;
  //mBestStructure = Replica[i].Structure;
  //mBestLabel = Replica[i].Label;

  int minNumClashes=MAXINT;
  do {
    SampleLabels(start, end);
    SampleAngles(start, end);
    BuildStructure(start,end);

    int numClashes=countClashes();
    if (numClashes < minNumClashes){
      minNumClashes=numClashes;
      KeepStructure();
    }
    if (minNumClashes==0) break;

    //insert code here to random sample start and end positions
    SampleASegment(&start, &end);
  } while(minNumClashes>0);

  vector<RESIDUE> protein;
  
  /********* Greedy search for burial_ratio<0.8 using DOPE *************/
  GetConformation(mProteinSeq, mStructure, protein); // Adding heavy atoms, and H
  double bestScore = calcEnergy(protein, 0);
  protein.clear();

  // the number of trials that have no improvement at all
  int maxNumTrials= 640; //900;
  int nMaxIteration = 8000;
  int count=0, total_cnt=0;
  double bestRg=1000;
  while(count<=maxNumTrials&&total_cnt<nMaxIteration) {
    //insert code here to random sample start and end position
    SampleASegment(&start, &end);      
    SampleLabels(start, end);
    SampleAngles(start, end);
    BuildStructure(start,end);
    
    bool clash=detectClashes();
    bool recover=true;
    if(!clash) {
      vector<RESIDUE> protein;
      GetConformation(mProteinSeq, mStructure, protein);
      double score=calcEnergy(protein, start); // RadiusDope
      protein.clear();

      if(score < bestScore) {
        fprintf(stderr,
                " trial#%d;  bestScore=%f; newScore=%f; br=%f; ru=%f; rg=%f\n",
                count, bestScore, score, mScoringFunction->br, 
                mScoringFunction->ru, mScoringFunction->rg);
        recover=false;
        bestScore=score;
        bestRg=mScoringFunction->rg;
        KeepStructure();

        count=0; //reset the count
      }//if
      if ( mScoringFunction->br < 0.8 && mScoringFunction->ru < 5 
                                      && mScoringFunction->rg < estimatedRg)
         break;
    }

    if (!recover) continue;

    count++;
    total_cnt++;
  }//while

  cerr << "#<<i" << " has inital RadialDope=" << bestScore
       << "; br=" << mScoringFunction->br << "; ru=" << mScoringFunction->ru
       <<"; rg=" << mScoringFunction->rg << endl;
  //cerr << "     RMSD="<<calcRMSD()<<endl;
    
  estimatedRg = bestRg;

  /* ************* End of greedy search on burial_ratio *********************/

  ApplyContactConstraint();
  KeepStructure();
  cerr << "Finish constrains\n";
  GetConformation(mProteinSeq, mStructure, protein); // Adding heavy atoms, and H
  cerr << "Conformation built.\n";
  double energy0 = calcEnergy2(protein, 0);
  protein.clear();

  for(int i=0;i<num_rep;i++){
    // Save the initial energy, labels and structure
    Replica[i].Vec = mBestFBVec;
    Replica[i].Structure = mBestStructure;
    Replica[i].Label = mBestLabel;
    Replica[i].energy = energy0; //calcEnergy();
    Replica[i].HBenergy = mHBEnergy;
    Replica[i].burial = mBurialPenalty;
  }
  cerr << num_rep << " Replicus: " <<Replica[0].energy
       << "; HB=" << mHBEnergy << "; BurialPenalty=" << mBurialPenalty << endl;
  if(Replica[0].energy < max_energy) BestConformers.push_back(Replica[0]);

  for (int i=0; i<(int)mScoringFunctionList.size(); i++){
    ScoringFunction* sf = mScoringFunctionList[i].first;
    sf->estimated_rg = estimatedRg+1;
  }

  //Replica Exchange!
  mCurrentSimulationStep = 0;
  double dif; //Max_Iters=10;
  for(int iter = 1; iter < Max_Iters; iter++){
    // Confomer Sampling for each replica
    if (iter%5==0){
      ifstream in("/dev/urandom",ios::in);
      in.read((char*)&randomSeed, sizeof(unsigned)/sizeof(char));
      in.close(); //use the system kernel state to generate a seed
      gettimeofday(&ts, 0);
      randomSeed=randomSeed*randomSeed+id*id+ts.tv_usec;
      srand(randomSeed); srandom(randomSeed); srand48(randomSeed);
    }

//#pragma omp parallel for
    if ((iter+1)%100==0)
      MAX_SEG_LENGTH=min(15, MAX_SEG_LENGTH+1);
    for(int i=0;i<num_rep;i++){
      //assign the Vec,Structure and Label
      mBestFBVec = Replica[i].Vec;
      mBestStructure = Replica[i].Structure;
      mBestLabel = Replica[i].Label;
      double old_energy = Replica[i].energy, oldHB=Replica[i].HBenergy,
             oldEPAD=Replica[i].EPADEnergy, oldBurial=Replica[i].burial,
             oldContactE=0;

      // Usual MC move: num_sampling steps
      int energyCmp=0, energyAcc=0;
         for(int j=0;j<num_sampling;j++){
        //gettimeofday(&ts, 0);
        
        SampleASegment(&start, &end);
        SampleLabels(start, end);
        if (DEBUG_MODE){
          //gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
          //cerr << "  Sample Segment and Label: "
          //     << dif*1000+te.tv_usec/1000-ts.tv_usec/1000 
          //     << " miliseconds. Length=" << end-start+1 << endl;
          //gettimeofday(&ts, 0);
        }
        bool clash;
        int a=0;
        for ( ; a < 40; a++) {
          SampleAngles(start, end);
          BuildStructure(start,end);
          clash=detectClashes();
          if (!clash) break;
        }
        if (DEBUG_MODE){
          //gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
          //cerr << "  Sample Angle+BuildStructure+detectClashes: " 
          //     << dif*1000+te.tv_usec/1000-ts.tv_usec/1000 << " miliseconds. #"
          //     << a << endl;
          //gettimeofday(&ts, 0);
        }
        if(!clash) {
          vector<RESIDUE> protein;
          // Adding heavy atoms, and H
          GetConformation(mProteinSeq, mStructure, protein);
          double new_energy = calcEnergy2(protein, start);
          protein.clear();

          energyCmp++;
          if (new_energy > old_energy) {
            //double E = asinh((new_energy-old_energy));
            double E = new_energy-old_energy, delta = exp( -E/Temp[i] );
            if(drand48() > delta) continue;
            Replica[i].segStart=start; Replica[i].segEnd=end; Replica[i].a=a;
          }
          mScoringFunction->rg = new_rg;
          mScoringFunction->ru = new_ru;
          mScoringFunction->br = new_br;
          KeepStructure();
          old_energy = new_energy;
          oldHB=mHBEnergy;
          oldBurial=mBurialPenalty, oldEPAD=mEPADEnergy;
          oldContactE=mContactEnergy;
          //cerr << "      num_sampling: " << j << "; Energy="<<new_energy <<endl;
          energyAcc++;
          if (DEBUG_MODE) {
            //gettimeofday(&te, 0); dif = difftime (te.tv_sec,ts.tv_sec);
            //cerr << "  CalcEnergy: " << dif*1000+te.tv_usec/1000-ts.tv_usec/1000
            //     << " miliseconds. #" << a << endl;
            //gettimeofday(&ts, 0);
          }
        }
      }
      // Save the energy, labels and structure
      Replica[i].Vec = mBestFBVec;
      Replica[i].Structure = mBestStructure;
      Replica[i].Label = mBestLabel;
      Replica[i].energy = old_energy;
      Replica[i].HBenergy = oldHB;
      Replica[i].burial = oldBurial;
      Replica[i].EPADEnergy=oldEPAD;
      Replica[i].br = mScoringFunction->br;
      Replica[i].rg = mScoringFunction->rg;
      Replica[i].ru = mScoringFunction->ru;
      if (BestConformer.energy>Replica[i].energy)
        BestConformer = Replica[i];
      if (iter%3)
        continue;
      fprintf(stderr,
              "  REP: %2d(%d:%d:%d:%d/%d);T=%5.2f; rg=%.2f;\tE(%.2f)="
                         "hb(%.2f)+Epad(%.2f)+Burial(%.2f)+ContactE(%.2f)\n",
              i, Replica[i].a, Replica[i].segEnd - Replica[i].segStart+1,
              end-start+1, energyAcc, energyCmp, Temp[i],
              Replica[i].rg, Replica[i].energy, Replica[i].HBenergy,
              oldEPAD, Replica[i].burial, oldContactE);
    }
    cerr << "Riteration#" << iter << "; HB=" << BestConformer.HBenergy
         << "; BurialPenalty=" << BestConformer.burial 
         << ";  best_energy=" << BestConformer.energy 
         << "; currentStep=" << mCurrentSimulationStep
         <<"; weighting#" << mStartingWeightedSimulationStep << endl;
    // Exchange!
    int offset = iter%2;
    for(int i=offset;i+1<num_rep;i+=2){
      // only exchange between neighboor replica
      double delta = (1./Temp[i+1]-1./Temp[i])
                    * (Replica[i].energy-Replica[i+1].energy);
      if(delta <= 0){ //swap
        tmp = Replica[i]; Replica[i]=Replica[i+1]; Replica[i+1]=tmp;
      }else{
        double rnd = drand48();
        if(rnd <= exp(-delta)){ //swap
          tmp = Replica[i]; Replica[i]=Replica[i+1]; Replica[i+1]=tmp;
        }
      }
    }
    mCurrentSimulationStep += num_sampling;
  }

  cerr << " BestConformers.size()=" << BestConformers.size()<< endl;
  for (int i=0; i<num_rep; i++){
    // Update the best energy, labels and structure
    for (vector<Conformer>::iterator it = BestConformers.begin();
                                     it < BestConformers.end(); it++)
    {
      if(Replica[i].energy < (*it).energy){
        BestConformers.insert(it, Replica[i]);
        if ((int)BestConformers.size()>num_rep/2)
          BestConformers.pop_back();
        break;
      } else if(Replica[i].energy == (*it).energy) break;
      else if (it==BestConformers.end() 
               && (int)BestConformers.size()<=num_rep/2)
        BestConformers.push_back(Replica[i]);
    }
  }
  cerr << "BestConformers.size()=" << BestConformers.size()<< endl;
  for (int i=0; i<(int)BestConformers.size(); i++){
  //for (int i=0; i<min(num_rep/6, (int)BestConformers.size()); i++){ // ###
    double diff = BestConformers[i].energy-BestConformers[0].energy;
    cerr << diff <<"=" << BestConformers[i].energy << "-" 
                       << BestConformers[0].energy << endl;
    // Hard to handle the number of decoys left, therefore set to num_rep/6
    // as in line ###
    if (i>0&&diff==0 || diff/abs(BestConformers[0].energy)>0.15) continue; 

    mBestFBVec = BestConformers[i].Vec;
    mBestStructure = BestConformers[i].Structure;
    mBestLabel = BestConformers[i].Label;
    mHBEnergy = BestConformers[i].HBenergy;
    mEnergy = BestConformers[i].energy;
    mScoringFunction->br = BestConformers[i].br;
    mScoringFunction->ru = BestConformers[i].ru;
    mScoringFunction->rg = BestConformers[i].rg;
    //double rmsd=calcRMSD();
    //if (rmsd>6) continue;
    cout<<i<<"#energy= " << mEnergy;
    writeDecoy(outdir);
  }
}

/* For Reference
static char sTab1[]    = {'A','C','D','E','F',
        'G','H','I','K','L',
        'M','N','P','Q','R',
        'S','T','V','W','Y',
        'E','B','Z','X'
       };
*/

string arrResidueName[25] =
   {"ALA","", "CYS","ASP","GLU","PHE","GLY","HIS","ILE","", "LYS","LEU",
    "MET","ASN","", "PRO","GLN","ARG","SER","THR","", "VAL","TRP","", "TYR"};

// GetConformation Interface
void ConformationOptimizer::GetConformation(
        string seq, vector<Vector3 > structure, vector<RESIDUE>& protein)
{
  for(int i = 0; i < (int)seq.size(); i++) {
    ATOM CA("CA",i), CB("CB",i), O("O",i), N("N",i), C("C",i);
    RESIDUE res;
    res.name = arrResidueName[seq[i]-'A'];
    res.name_idx=seq[i]-'A';
    res.seq = i;
    //cerr << "residue : " << i << " " << CA.name << CA.name_idx
    //     << C.name << C.name_idx << endl;
    
    CA.x= structure[i].x;
    CA.y= structure[i].y;
    CA.z= structure[i].z;
    
    res.addAtom(N);
    res.addAtom(CA);
    res.addAtom(CB);
    res.addAtom(O);
    res.addAtom(C);
    
    protein.push_back(res);
    //cerr << "residue=" << res.name << endl;
  }
  if (refine_segments.size()>0){
    int total_seg=refine_segments.size();
    int seg=random() % total_seg;
    int seg_start=refine_segments[seg].first;
    int seg_end=refine_segments[seg].second;
    for (int i=seg_start; i<=seg_end; i++)
      protein[i].flag=i;
  } else {
    for (int i=0; i<(int)seq.size(); i++)
      protein[i].flag=i;
  }
  //cerr << "GetConformation: " << protein.size() << " residues" << endl;
/*  
  for(int i = 0; i < (int)protein.size(); i++) {
    RESIDUE res=protein[i];
    cerr << i << "-" << res.name << ":";
    for(int j=0; j<(int)res.arrAtoms.size();j++)
      cerr << "  " << res.arrAtoms[j].name << res.arrAtoms[j].name_idx
           << "("<<res.arrAtoms[j].x << "," << res.arrAtoms[j].y
           << "," << res.arrAtoms[j].z<<")" << endl;
  }
*/
  mBBQ->generate(protein);
  int nLen=(int)protein.size();
  //for (int i=0; i<protein[0].arrAtoms.size(); i++)
  //  cerr << protein[0].arrAtoms[i].name<< ","; cerr<< "(o o)"<<endl;
  //cerr << "\t\tGetConformation: Totally " << nLen << " residues generated." << endl;

  Vector3 pos, CA1, CB1, O1, C0, C1, N1, N2;
  for(int i = 0; i < nLen; i++) {
    ATOM atm;

    //atm=protein[i].getAtom("CB");
    atm=protein[i].getAtom(1);
    CB1.set(atm.x, atm.y, atm.z);
    //fprintf(stderr, "%i-AAtom(%s)<%f,%f,%f>\n",
    //        i, atm.name.c_str(), atm.x, atm.y, atm.z);
    //atm=protein[i].getAtom("O");
    atm=protein[i].getAtom(2);
    O1.set(atm.x, atm.y, atm.z);

    //atm=protein[i].getAtom("CA");
    atm=protein[i].getAtom(0);
    if (atm.x==0 && atm.y==0 && atm.z==0)
      continue;
    //cerr << i << " " << protein[i].name << "- Atom(" << atm.name << ") < " 
    //     << atm.x << ", " << atm.y << ", " << atm.z << ">\n";
    CA1.set(atm.x, atm.y, atm.z);
    //atm=protein[i].getAtom("C");
    atm=protein[i].getAtom(4);
    if (atm.x==0 && atm.y==0 && atm.z==0)
      continue;
    //fprintf(stderr, "%i-BAtom(%s)<%f,%f,%f>\n",
    //        i, atm.name.c_str(), atm.x, atm.y, atm.z);
    C1.set(atm.x, atm.y, atm.z);

    if (i==0 && protein[i].name!="GLY") {
      ATOM N("N",i);
      //N.name_idx=2;
      ConstructTerminalN(C1, CB1, CA1, N.x, N.y, N.z);
      protein[i].addAtom(N);
    }

    if (i>0 && i<(int)protein.size()-1) {
      //atm=protein[i].getAtom("N");
      atm=protein[i].getAtom(3);
      //fprintf(stderr, "%i-CAtom(%s)<%f,%f,%f>\n",
      //        i, atm.name.c_str(), atm.x, atm.y, atm.z);
      N1.set(atm.x, atm.y, atm.z);

      //atm=protein[i-1].getAtom("C");
      atm=protein[i-1].getAtom(4);
      C0.set(atm.x, atm.y, atm.z);

      if (protein[i].name!="PRO") {
        ATOM H("HN",i);
        //H.name = "HN";
        //H.name_idx=5;
        ConstructNH(C0, N1, CA1, H.x, H.y, H.z);
        //cerr << "3ATOM";// <<i<< "-H(" << H.x << ", " << H.y << ", " << H.z << ")\n";
        protein[i].addAtom(H);
      }
    }
  }
/*
  for(int i = 0; i < (int)protein.size(); i++) {
    RESIDUE res=protein[i];
    cerr << i << "-" << res.name << ":" << endl;
    for(int j=0; j<(int)res.arrAtoms.size();j++){
      ATOM at=res.arrAtoms[j];
      cerr << "  " << j << ":" << res.getAtomIndex(at.name) << ":"
           << res.getAtomIndex(at.name_idx) << " " 
           << at.name << at.name_idx << "(" << at.x<<","<<at.y<<","<<at.z<<")"
           << endl;
    }
  }
  exit(0);
*/
  //cerr << "GetConformation: Done" << endl;
}
void ConformationOptimizer::writeDecoy(string& pdbfile){
  cerr<<"ConformationOptimizer::writeDecoy() not implemented yet!"<<endl;  
}

bool ConformationOptimizer::ConstructNH(
      Vector3 &C0, Vector3& N1, Vector3 &CA1, double &x, double &y, double &z)
{
  // Quick and dirty Hydrogen construction.
  Vector3 NAT1(C0-N1);
  Vector3 NAT2(CA1-N1);
  NAT1.normalize();
  NAT2.normalize();
  Vector3 NH(NAT1+NAT2);
  NH.normalize();
  //cerr << "NH <" << NH.x << ", " << NH.y << ", " << NH.z << ">\n";
  Vector3 H(N1 - NH);
  x = H.x; y = H.y; z = H.z;
  return true;
}

bool ConformationOptimizer::ReadSideChainComm(string fname)
{
  string line, aa;
  ifstream commFile;
  cerr << "Reading SideChain commFile file " << fname << endl;
  commFile.open(fname.c_str(), ios::in | ios::binary);
  Vector3 extended, compact;
  pair<Vector3, Vector3> locs;
  while (commFile.good())
  {
    // Moves through the lines until the HEADER record is found.
    getline(commFile, line);
    aa = line;
    if (commFile.eof()) break;
    
    getline(commFile, line);
    strtokenizer strtokens(line," ");
    int num=strtokens.count_tokens();
    if (num<6) continue;
    extended.x = atof(strtokens.token(0).c_str());
    extended.y = atof(strtokens.token(1).c_str());
    extended.z = atof(strtokens.token(2).c_str());
    compact.x = atof(strtokens.token(3).c_str());
    compact.y = atof(strtokens.token(4).c_str());
    compact.z = atof(strtokens.token(5).c_str());
    
    locs.first = extended;
    locs.second = compact;
    mSideChainCenters.insert(pair<string, pair<Vector3, Vector3> >(aa, locs));
  }
  return true;
}

// CAp is the position of the previous Ca; CAn is the position of the next Ca;
// aa is the amino acid type
// return the center of the side chain
bool ConformationOptimizer::ConstructSG(
        Vector3 &CAp, Vector3& CA, Vector3 &CAn, Vector3 &SG, string aa)
{
  // search in the CABS table from sidechain.comm
  mapSideChainCenter::iterator mapit = mSideChainCenters.find(aa);
  if (mapit == mSideChainCenters.end() || aa=="GLY")
    return false;
  
  Vector3 tSGextened, tSGcompact;
  tSGextened = mapit->second.first;
  tSGcompact = mapit->second.second;
  
  // Construct SideChain center based on Zhang Yang's sidechain.comm
  Vector3 edge1(CAp-CA);
  Vector3 edge2(CAn-CA);
  Vector3 versor[3];
  versor[0] = -edge1 + edge2;
  versor[1] = (-edge1) % edge2;
  //cerr << "7("<<edge1.x<<","<<edge1.y<<","<<edge1.z<<";"
  //     << edge2.x<<","<<edge2.y<<","<<edge2.z<<")";
  //cerr << "8("<<versor[0].getLength()<<","<<versor[1].getLength()<<")";
  versor[0].normalize();
  versor[1].normalize();
  //cerr << "9";
  versor[2] = versor[0]%versor[1]; // Normal vector of the plane
  
  // transpose the rotation matrix to get the new coordinates
  Vector3 versorX, versorY, versorZ;      
  versorX.x = versor[0].x;
  versorX.y = versor[1].x;
  versorX.z = versor[2].x;
  versorY.x = versor[0].y;
  versorY.y = versor[1].y;
  versorY.z = versor[2].y;
  versorZ.x = versor[0].z;
  versorZ.y = versor[1].z;
  versorZ.z = versor[2].z;
  double angle = acos(edge1*edge2/(edge1.getLength()*edge2.getLength()));
  //cerr << "angle=" << angle*180/3.14159 << endl;
  if (angle>105*3.14159/180) {
    //cerr << "extend(" << tSGextened.x << ", " << tSGextened.y  
    //     << ", " << tSGextened.z << ")" << endl;
    SG.x = tSGextened*versorX;
    SG.y = tSGextened*versorY;
    SG.z = tSGextened*versorZ;
  } else {
    //cerr << "compact(" << tSGcompact.x << ", " << tSGcompact.y  
    //     << ", " << tSGcompact.z << ")" << endl;
    SG.x = tSGcompact*versorX;
    SG.y = tSGcompact*versorY;
    SG.z = tSGcompact*versorZ;
  }
  SG=SG+CA;
  //cerr << "SG(" << SG.x << ", " << SG.y  << ", " << SG.z << ")" << endl;
  return true;
}

bool ConformationOptimizer::ConstructTerminalN(
         Vector3 &C, Vector3 &Cb, Vector3 &Ca, double &x, double &y, double &z)
{
  Vector3 AB(Cb-Ca); // V1
  Vector3 AC(C-Ca);  // V2
  AB.normalize();
  AC.normalize();
  Vector3 SN(AB%AC); // surface normal vector
  SN.normalize();
  //cerr << "SN ;" << SN.x << ", " << SN.y << ", " << SN.z << endl;
  //Vector3 ANN(NN-Ca); ANN.normalize();
  //cerr << "Angle <BAC ;" << acos(AB*ANN)*180/3.14 << ", " 
  //     << acos(AC*ANN)*180/3.14 << ", " << acos(AB*AC)*180/3.14159 << endl;
  
  double k = cos(109.47*3.14159/180), 
         l_NCa = 1.48; // square of the N-Ca bond length, average 1.46-1.48
  double x1=AB.x, y1=AB.y, z1=AB.z;
  double x2=AC.x, y2=AC.y, z2=AC.z;
  //cerr << "AB ;" << x1 << ", " << y1 << ", " << z1 << endl;
  //cerr << "AC ;" << x2 << ", " << y2 << ", " << z2 << endl<<endl;
  
  double dx= y2*x1-y1*x2;
  double ax= (y2-y1)*k/dx;
  double bx= (z2*y1-z1*y2)/dx;
  //cerr << "Xequation(" << dx << ")= " << bx << "z" << "+" << ax << endl;
  
  double dy= -y2*x1+y1*x2;
  double ay= (x2-x1)*k/dy;
  double by= (z2*x1-z1*x2)/dy;
  //cerr << "Yequation(" << dy << ")= " << by << "z" << "+" << ay << endl;
  
  double az= bx*bx + by*by+1;
  double bz= 2*(ax*bx+ay*by);
  double cz= ax*ax+ay*ay-1;
  //cerr << "Z ;" << az << "Z*Z+" << bz << "*Z+" << cz << endl;
  
  Vector3 AN;
  AN.z= (-bz+sqrt(bz*bz-4*az*cz))/2/az;
  AN.x= ax+bx*AN.z;
  AN.y= ay+by*AN.z;
  //cerr << "AN ;" << AN.x << ", " << AN.y << ", " << AN.z << "\n";
  
  if (SN*AN<0) {
    Vector3 N(Ca + AN*l_NCa);
    x = N.x; y = N.y; z = N.z;
    //cerr << "N ;" << x << ", " << y << ", " << z << ";" << SN*AN << "\n";
    //cerr << "Angle ;" << acos(AB*AN)*180/3.14159 <<  ";" 
    //     << acos(AN*AC)*180/3.14159 << endl;
  } else {
    AN.z= (-bz-sqrt(bz*bz-4*az*cz))/2/az;
    AN.x= ax+bx*AN.z;
    AN.y= ay+by*AN.z;
    //cerr << "AN ;" << AN.x << ", " << AN.y << ", " << AN.z << "\n";
    Vector3 N(Ca + AN*l_NCa);
    x = N.x; y = N.y; z = N.z;
    //cerr << "N1 ;" << x << ", " << y << ", " << z << ";" << SN*AN << "\n";
    //cerr << "Angle ;" << acos(AB*AN)*180/3.14159 <<  ";" 
    //     << acos(AN*AC)*180/3.14159 << endl;
  }
  
  return true;
}

double ConformationOptimizer::calc_RefLog_P(){
  //cerr << "calc_RefLog_P"<< endl;
  vector<int> labels;
  double prob;

  labels.resize(mProteinSize);
  for(int i=0; i<mProteinSize; i++)
  {
    labels[i] = atoi(mLabel[i].c_str()+1);
  }
  
  prob = Prob2[labels[2]][labels[3]];
  if(prob==0) prob = 0.0001;
  for(int i=4; i<mProteinSize; i++){
    double p = ProbT[labels[i-2]][labels[i-1]][labels[i]];
    //cerr << p <<"= ProbT["<<labels[i-2]<<"]["<<labels[i-1]<<"]["
    //     << labels[i]<<"]"<<endl;
    if(p==0) p = 0.01;
    prob*=p;
  }
  //cerr << " prob=" << prob<<endl;
  return log(prob);
}

/********************************ThetaTauOptimizer**************************************/
ThetaTauOptimizer::ThetaTauOptimizer()
  :ConformationOptimizer()
{
  mFB5Sampler=0;
  mFBVec.clear();
  //mFBVecSave.clear();
  mBestFBVec.clear();
}

ThetaTauOptimizer::ThetaTauOptimizer(string& proteinName)
  :ConformationOptimizer(proteinName)
{
  mFB5Sampler=0;
  mFBVec.clear();
  //mFBVecSave.clear();
  mBestFBVec.clear();
}

ThetaTauOptimizer::~ThetaTauOptimizer(){
  if (mFB5Sampler) delete mFB5Sampler;
}

void ThetaTauOptimizer::Initialize(
                  int randomSeed, int DEBUG_MODE, string additional_f)
{
  ConformationOptimizer::Initialize(randomSeed, DEBUG_MODE, additional_f);

  cerr <<"Reading cluster parameters from the file"<<endl;  
  //initialize FB5Sampler
  string thetaTauClusterFileName;
  int ret=config->getValue(STR_THETATAU_MODEL_PARAM_FILE, &thetaTauClusterFileName);
  if (!ret){
    //report err
    cerr<<"cannot find value for thetaTauClusterFileName in ThetaTauOptimizer"<<endl;
    exit(-1);
  }

  if (mCRFSampler){
     mFB5Sampler=new FB5Cluster(thetaTauClusterFileName.c_str());
  
    cerr<<"Setting constraints to the CRF Sampler..." << mCRFSampler <<endl;
  
    //add constraints to CRFSampler
    vector<string> headLabel;
        vector<string> midLabel;
        vector<string> tailLabel;
  
        headLabel.push_back("d0");
        tailLabel.push_back("d0");
    cerr << "mFB5Sampler.NumClusters=" << mFB5Sampler->getNumClusters() << endl;
    for(int i=0; i<mFB5Sampler->getNumClusters(); i++) {
      char label[100];
             sprintf(label, "f%d", i);
             midLabel.push_back(string(label));
             //cerr<<label<< " ";
        }
  
    mCRFSampler->setSequenceLength(mProteinSize);
  
        mCRFSampler->setAllowedStates(0, headLabel);
       mCRFSampler->setAllowedStates(1, headLabel);
    mCRFSampler->setAllowedStates(mProteinSize-1, tailLabel);
    cerr << "^*^^"<<endl;
        for (int i=2; i<= mProteinSize-1; i++)
            mCRFSampler->setAllowedStates(i, midLabel);
  }
  //initialize two vectors of unitVectors
  mFBVec=vector<Vector3 >(mProteinSize);
  //mFBVecSave=vector<Vector3 >(mProteinSize);
  mBestFBVec=vector<Vector3 >(mProteinSize);

  cerr << "^^ThetaTauOptimizer::Initialize finished."<<endl;
  //mCRFSampler->InitLSP();
}

void ThetaTauOptimizer::SampleASegment(int* start, int*end){
  if (refine_segments.size()>0){
    int total_seg=refine_segments.size();
    int seg=random() % total_seg;
    int seg_start=refine_segments[seg].first;
    int seg_end=refine_segments[seg].second;
    if (seg_start==0)
      *start=random()%(seg_end-seg_start-3)+2;
    else
      *start=random()%(seg_end-seg_start+1)+seg_start;
    int segLen=random() % min(MAX_SEG_LENGTH, seg_end-*start+1);
    *end= (*start) + segLen;
    //cerr << "SampleASegment("<<*start<<","<<*end<<") out of seg-"<< seg
    //     << "("<< seg_start<< "," << seg_end <<")"; 
    return;
  }

  //the first two positions and the last position shouldn't be sampled
  int segLen=random() % MAX_SEG_LENGTH;
  //cerr << "current Step =" << mCurrentSimulationStep 
  //     << "; weighting will start at " << mStartingWeightedSimulationStep << endl;
  if (mCurrentSimulationStep>mStartingWeightedSimulationStep) {
    int i=2;
    double total=0;
    for ( ; i<mProteinSize-segLen-1; i++){ 
      double sum_entropy=0;
      for (int j=0; j<segLen; j++)
        sum_entropy+=mCRFSampler->entropies[i+j];
      //total+=sum_entropy;
      mProbAsSegmentStart[i]=sum_entropy;
      total+=exp(mProbAsSegmentStart[i]);
      //cerr << "sumOfEntropy["<<i<<"]="<< sum_entropy << endl;
    }
/*
    double average=total/(mProteinSize-segLen/2-3);
    total=0;
    for (i=2; i<mProteinSize-segLen/2-1; i++) {
      mProbAsSegmentStart[i]-=average;
      total+=exp(mProbAsSegmentStart[i]);
    }    
    //cerr << "Total=" << total << "; segLen=" << segLen << endl;
*/
    double cdf=0.0;
    for (i=2; i<mProteinSize-segLen-1; i++) {
      double pdf=exp(mProbAsSegmentStart[i])/total;
      cdf+=pdf;
      mProbAsSegmentStart[i]=cdf;
      //cerr << "pdf[" << i << "]=" << pdf << endl;
    }

    double rand_p = drand48();
    for( int l=2; l <= mProteinSize-segLen-1; l++) {
      // mProbAsSegmentStart contains the cdf probability
      double p = mProbAsSegmentStart[l];
      if (rand_p>=p) {
        *start=l;
        if ( l == mProteinSize-segLen-1 || rand_p < mProbAsSegmentStart[l+1]+p){
          //cerr << "choose pdf[" << l << "]=" << mProbAsSegmentStart[l+1]-p << endl;
          break;
        }
      }
    }
  }
  else
    *start=random()%(mProteinSize-3)+2;

  //cerr << mSecStruSeq[*start];
  *end= (*start) + segLen;

  if ((*end) > (mProteinSize-2) )
    *end=mProteinSize-2;
}

void ThetaTauOptimizer::SampleAngles(int start, int end){
  //cerr <<"ThetatauOptimizer--SampleAngles()"<<endl;
  mFBVec=mBestFBVec;
  string tl=mLabel[mProteinSize-2];
  for(int i=start; i<=end; i++) {
    double tmpUnitVector[3];
    string & label=mLabel[i];
    if (label=="d0" && i==mProteinSize-2) label="f67";
    if (label=="d0"){
      if (i>=2 && i< mProteinSize-1){
        cerr << "Fatal error: an incorrect label d0 sampled for position "
             << i << endl;
        exit(0);
      }
      continue;
    }else if (label[0]!='f'){
      cerr << "Fatal error: an incorrect label " << label 
           << " sampled for position " << i << endl;
      exit(0);
    }

    int clusterIndex=atoi(label.c_str()+1);
    //cerr << "clusterIndex="<<clusterIndex<<endl;
    mFB5Sampler->sample(tmpUnitVector,clusterIndex);
    mFBVec[i]=Vector3(tmpUnitVector[0], tmpUnitVector[1], tmpUnitVector[2]);
/*
    if (fabs(mFBVec[i].getLength()-1)>0.010){
      cerr << "label="<<label<<" clusterIndex="<<clusterIndex
           << "  " << mFBVec[i]<<" length="<<mFBVec[i].getLength()<<endl;
      exit(-1);
    }
*/
     //cerr<<mFBVec[3*i+0]<<" "<<mFBVec[3*i+1]<<" "<<mFBVec[3*i+2]<<endl;;
  }
  mLabel[mProteinSize-2] = tl;
}

// Given a structure, calculate the unit vectors that represent the
// theta/tau angles based on the i-2, i-1 and i-th Ca atoms.
void ThetaTauOptimizer::ReAngle()
{
  cerr << "ReAngle" << endl;
  //Vector2 angle;
  initCAs.resize(mStructure.size());
  for (int i = 0; i < (int)mStructure.size(); i++) {
    initCAs[i].x=mStructure[i].x;
    initCAs[i].y=mStructure[i].y;
    initCAs[i].z=mStructure[i].z;
  }

  for (int i = 2; i < (int)mStructure.size()-1; i++) {
    Vector3 v34=mStructure[i+1]-mStructure[i];
    v34.normalize();
    Vector3 diff12=mStructure[i-2]-mStructure[i-1];
    Vector3 diff32=mStructure[i]-mStructure[i-1];

    Vector3 xAxis, yAxis, zAxis;
    xAxis=mStructure[i-1]-mStructure[i];
    xAxis.normalize();
    //zAxis equal to the cross product of two vectors
    zAxis=diff32 % diff12;
    zAxis.normalize();

    //yAxis is equal to the cross product of x and z
    yAxis=xAxis % zAxis;
    yAxis.normalize();

    mFBVec[i].x=v34*xAxis;
    mFBVec[i].y=v34*yAxis;
    mFBVec[i].z=v34*zAxis;
    mFBVec[i].normalize();
  }
}

bool ThetaTauOptimizer::detectClashes(){
  return ScoringByClashes::checkClashes(mStructure);
}

int ThetaTauOptimizer::countClashes(){
  return ScoringByClashes::countClashes(mStructure);
}

double ThetaTauOptimizer::calcEnergy(){
  //return evaluate(mProteinSeq, mSecStruSeq, mStructure);
  return mScoringFunction->evaluate(mProteinSeq, mSecStruSeq, mStructure);
}

double ThetaTauOptimizer::calcEnergy(vector<RESIDUE>& protein, int start)
{
  //cerr << " ThetaTauOptimizer::calcEnergy2: "<< protein.size() << endl;
  return mScoringFunction->evaluate(mProteinSeq, mSecStruSeq, protein, start);
}

double ThetaTauOptimizer::calcEnergy2(vector<RESIDUE>& protein, int start)
{
  //cerr << " ThetaTauOptimizer::calcEnergy2: "<< protein.size() << endl;
  return evaluate(mProteinSeq, mSecStruSeq, protein, start);
}

//this function shouldn't be used any more
void ThetaTauOptimizer::Undo(int start, int end){
/*
  //perform undo operations for local optimization purpse
  for(int i=start; i<=end; i++)
  {
           mLabel[i]=mLabelSave[i];
    mFBVec[i]=mFBVecSave[i];
  }
  mStructure=mStructureSave;      
*/
}

void ThetaTauOptimizer::BuildStructure(int start, int end){
  //cerr <<"ThetaTauOptimizer::BuildStructure(start,end)"<<endl;

  //the structureBuilder is an instance of ThetaTauBuilder
  ThetaTauBuilder* builder=dynamic_cast<ThetaTauBuilder*>(mStructureBuilder);
  builder->presetBondLengths=presetBondLengths;

  //save the structure
  //mStructureSave=mStructure;

  //copy partial structure from mBestStructure
  for(int i=0; i<=start; i++)
    mStructure[i]=mBestStructure[i];

  builder->build(mProteinSeq, mFBVec,start, mStructure);
}

void ThetaTauOptimizer::ApplyContactConstraint(){
  cerr <<"ThetaTauOptimizer::ApplyContactConstraint()"<<endl;

  ThetaTauBuilder* builder=dynamic_cast<ThetaTauBuilder*>(mStructureBuilder);
  builder->presetBondLengths=presetBondLengths;

  if (mpContact==NULL)
    return;
  builder->ApplyContactConstraint(mLabel, mFBVec, mStructure, mpContact,
                                  mCRFSampler, mFB5Sampler);
}

void ThetaTauOptimizer::KeepStructure(){
  mBestLabel=mLabel;
  mBestFBVec=mFBVec;
  mBestStructure=mStructure;
}

void ThetaTauOptimizer::mergeDecoy(
        string& fileN, vector<Vector2>& g_angle, 
        vector<double> & bondlenList, vector<Vector3>& initCAs)
{
  ThetaTauBuilder* builder=dynamic_cast<ThetaTauBuilder*>(mStructureBuilder);

  vector<int> m_aligned(initCAs.size());
  cerr << endl;
  for (int i=0; i<(int)initCAs.size(); i++){
    if (presetBondLengths[i]==-1)
      m_aligned[i]=0;
    else
      m_aligned[i]=1;
    cerr << m_aligned[i];
  }
  cerr << endl;
  mBestStructure.resize(initCAs.size());
  builder->build_final(mProteinSeq, g_angle, mBestStructure, bondlenList,
                       m_aligned, initCAs);

  string pdbfile(fileN);
  cout <<" file= "<<fileN<<endl;

  ofstream ofile(fileN.c_str(),ios::out);

  vector<RESIDUE> protein;
  GetConformation(mProteinSeq, mBestStructure, protein);
  int atm_seq=1;
  //REMARK   2 Energy = 10863.32
  ofile.width(19);  //record name, i.e., ATOM
  ofile<<"REMARK   2 Energy =";
  ofile.width(12);  //atom serial number
  ofile<<mEnergy << endl;
  if (mScoringFunction){
    ofile << "REMARK   2 br=" << mScoringFunction->br << "; rg=" 
          << mScoringFunction->rg << "; ru=" << mScoringFunction->ru << endl;
  }
  for(int i=0; i< (int)protein.size(); i++) {
    string residue = AA3Coding[toupper(mProteinSeq[i])-'A'];
    ATOM atm;
    atm=protein[i].getAtom("N");
    if (!(atm.x==0 && atm.y==0 && atm.z==0)) 
      writeDecoyAtom(ofile, i+1, residue, atm_seq++, "N ", atm.x, atm.y, atm.z);
    atm=protein[i].getAtom("CA");
    writeDecoyAtom(ofile, i+1, residue, atm_seq++, "CA", atm.x, atm.y, atm.z);
    atm=protein[i].getAtom("C");
    if (!(atm.x==0 && atm.y==0 && atm.z==0))
      writeDecoyAtom(ofile, i+1, residue, atm_seq++, "C ", atm.x, atm.y, atm.z);
    atm=protein[i].getAtom("O");
    if (!(atm.x==0 && atm.y==0 && atm.z==0))
      writeDecoyAtom(ofile, i+1, residue, atm_seq++, "O ", atm.x, atm.y, atm.z);
    atm=protein[i].getAtom("CB");
    if (!(atm.x==0 && atm.y==0 && atm.z==0))
      writeDecoyAtom(ofile, i+1, residue, atm_seq++, "CB", atm.x, atm.y, atm.z);
  }
  ofile.close();
}

void ThetaTauOptimizer::writeDecoy(string& outdir){
  //cerr <<"Wrting the result structural model to file "<<pdbfile<<endl;
  
  //here we donot use time() to get the random seed, instead we use the
  // system kernel state to generate a seed
  //ifstream in("/dev/urandom",ios::in);
  //in.read((char*)&randomSeed, sizeof(unsigned)/sizeof(char)); in.close();
  
  unsigned id=getpid();
  randomSeed=randomSeed*randomSeed+id*id;
  
  //save the resulting PDB file
  char resultPDBfile[4096];
  sprintf(resultPDBfile, "%s/%s%u.pdb",
          outdir.c_str(), mProteinName.c_str(), randomSeed);
  string pdbfile(resultPDBfile);
  cout <<" file= "<<pdbfile<<endl;

  ofstream ofile(pdbfile.c_str(),ios::out);

  vector<RESIDUE> protein;
  GetConformation(mProteinSeq, mBestStructure, protein);
  if (refine_segments.size()>0){
    vector<Vector2> g_angle(mBestStructure.size());
    vector<double> bondLenList;
    for(int i=0;i<(int)mBestStructure.size();i++){
      g_angle[i].x = acos(mBestFBVec[i][0]);
      g_angle[i].y = atan2(mBestFBVec[i][2],mBestFBVec[i][1]);
      if (i>0){
        Vector3 caL=mBestStructure[i-1];
        Vector3 ca =mBestStructure[i];
        double dl=sqrt((ca.x-caL.x)*(ca.x-caL.x) + (ca.y-caL.y)*(ca.y-caL.y)
                                                 + (ca.z-caL.z)*(ca.z-caL.z));
        bondLenList.push_back(dl);
      }
    }
    mergeDecoy(pdbfile, g_angle, bondLenList, initCAs);
    return;
  }
  int atm_seq=1;
  //REMARK   2 Energy = 10863.32
  ofile.width(19);  //record name, i.e., ATOM
  ofile<<"REMARK   2 Energy =";
  ofile.width(12);  //atom serial number
  ofile<<mEnergy;
  ofile<<"; HBEnergy =";
  ofile.width(12);  //atom serial number
  ofile<<mHBEnergy << endl;
  if (mScoringFunction) {
    ofile << "REMARK   2 br=" << mScoringFunction->br << "; rg=" 
          << mScoringFunction->rg << "; ru=" << mScoringFunction->ru << endl;
  }
  for (int i=0; i<(int)mBestLabel.size(); i++){
    if (i%20==0)
      ofile << endl << "REMARK   3 SAMPLED LABELS : ";
    ofile << mBestLabel[i] << " ";
  }
  ofile << endl;
  for(int i=0; i< (int)protein.size(); i++)
  {
    string residue = AA3Coding[toupper(mProteinSeq[i])-'A'];
    ATOM atm;
    atm=protein[i].getAtom("N");
    if (!(atm.x==0 && atm.y==0 && atm.z==0))
      writeDecoyAtom(ofile, i+1, residue, atm_seq++, "N ", atm.x, atm.y, atm.z);
    atm=protein[i].getAtom("CA");
    writeDecoyAtom(ofile, i+1, residue, atm_seq++, "CA", atm.x, atm.y, atm.z);
    atm=protein[i].getAtom("C");
    if (!(atm.x==0 && atm.y==0 && atm.z==0))
      writeDecoyAtom(ofile, i+1, residue, atm_seq++, "C ", atm.x, atm.y, atm.z);
    atm=protein[i].getAtom("O");
    if (!(atm.x==0 && atm.y==0 && atm.z==0))
      writeDecoyAtom(ofile, i+1, residue, atm_seq++, "O ", atm.x, atm.y, atm.z);
    atm=protein[i].getAtom("CB");
    if (!(atm.x==0 && atm.y==0 && atm.z==0))
      writeDecoyAtom(ofile, i+1, residue, atm_seq++, "CB", atm.x, atm.y, atm.z);
  }
  ofile.close();
}

void ThetaTauOptimizer::writeDecoyAtom(
       ofstream& ofile, int res_seq, string residue, int atm_seq, 
       string atom, double x, double y, double z)
{
  ofile.width(6);  //record name, i.e., ATOM
  ofile<<"ATOM  ";
  ofile.width(5);  //atom serial number
  ofile<<atm_seq;

  ofile.width(4); //atom name
  ofile<<atom;
  ofile.width(1);  //alternative location indicator
  ofile<<" ";
  
  ofile<<" "; //one extra space
  ofile.width(3); //residue name
  ofile<<residue;

  ofile<<" "; //one extra space
  ofile.width(1); //chain ID
  ofile<<" ";
  ofile.width(4); //residue sequence number
  ofile<<res_seq;
  ofile.width(1); //residue insertion code
  ofile<<" ";

  ofile<<"   "; //three extra spaces
  ofile.width(8); //x coordinates
  ofile.precision(3);
    ofile.setf(ios::fixed,ios::floatfield);   
  ofile<<x;
  ofile.width(8); //y 
  ofile.precision(3);
    ofile.setf(ios::fixed,ios::floatfield);   
  ofile<<y;
  ofile.width(8);
  ofile.precision(3);
    ofile.setf(ios::fixed,ios::floatfield);   
  ofile<<z;

  ofile.width(6); //occupancy
  ofile.precision(2);
    ofile.setf(ios::fixed,ios::floatfield);  
  ofile<<1.0;
  ofile.width(6);
  ofile.precision(2);
    ofile.setf(ios::fixed,ios::floatfield);  
  ofile<<0.0; //temperature factor

  ofile<<endl;
}

/*************************PhiPsiOptimizer****************************/
PhiPsiOptimizer::PhiPsiOptimizer()
  :ConformationOptimizer()
{
  mAngleSampler=0;
  mPhiPsiCluster=0;
  mPsiCluster=0;
  mPhiCluster=0;

  mAngles.clear();
  //mAngleSave.clear();
  mBestAngles.clear();

  mProtein=0;
  mBestProtein=0;
}

PhiPsiOptimizer::PhiPsiOptimizer(string& proteinName)
  :ConformationOptimizer(proteinName)
{
  mAngleSampler=0;
  mPhiPsiCluster=0;
  mPsiCluster=0;
  mPhiCluster=0;

  mAngles.clear();
  //mAngleSave.clear();
  mBestAngles.clear();

  mProtein=0;
  mBestProtein=0;
}

void PhiPsiOptimizer::Initialize(
           int randomSeed, int DEBUG_MODE, string additional_f)
{
  ConformationOptimizer::Initialize(randomSeed, DEBUG_MODE, additional_f);

  //initialize PhiPsi sampler parameters
  string phiPsiClusterFileName;
  string phiClusterFileName;
  string psiClusterFileName;

  int ret=config->getValue(STR_PHIPSI_MODEL_PARAM_FILE,&phiPsiClusterFileName);
  ret &=config->getValue(STR_PSI_MODEL_PARAM_FILE, &psiClusterFileName);
  ret &=config->getValue(STR_PHI_MODEL_PARAM_FILE, &phiClusterFileName);

  if (!ret){
    //report err
    cerr<<"cannot find values for phiPsiClusterFileName in PhiPsiOptimizer"<<endl;
    exit(0);
  }

      mPhiPsiCluster=new PhiPsiCluster(phiPsiClusterFileName.c_str());
      
      mPsiCluster=new PhiPsiCluster(psiClusterFileName.c_str());
  
      mPhiCluster=new PhiPsiCluster(phiClusterFileName.c_str());

  //add constraints to CRFsampler
  char label[100];

  vector<string> headLabel;
      vector<string> midLabel;
      vector<string> tailLabel;

       for(int i=0; i<mPsiCluster->getNumClusters(); i++) {
    sprintf(label, "h%d", i);
           headLabel.push_back(string(label));
      }

      for(int i=0; i<mPhiCluster->getNumClusters(); i++) {
           sprintf(label, "t%d", i);
           tailLabel.push_back(string(label));
      }

      for(int i=0; i<mPhiPsiCluster->getNumClusters(); i++) {
           sprintf(label, "m%d", i);
           midLabel.push_back(string(label));
           //cerr<<label<<endl;
      }

  mCRFSampler->setSequenceLength(mProteinSize);
      mCRFSampler->setAllowedStates(0,headLabel);
      mCRFSampler->setAllowedStates(mProteinSize-1,tailLabel);
      for (int i=1; i<mProteinSize-1; i++) {
          mCRFSampler->setAllowedStates(i, midLabel);
      }

  //for storage of the sampled angles
  mAngles=vector<Vector2 >(mProteinSize);
  //mAngleSave=vector<Vector2 >(mProteinSize);
  //mAngleNext=vector<Vector2 >(mProteinSize);
  mBestAngles=vector<Vector2 >(mProteinSize);

  //the angle sampler
  mAngleSampler=new MCsampler();
}

PhiPsiOptimizer::~PhiPsiOptimizer(){
  if (mAngleSampler) delete mAngleSampler;
  if (mPhiPsiCluster) delete mPhiPsiCluster;
  if (mPsiCluster) delete mPsiCluster;
  if (mPhiCluster) delete mPhiCluster;
}

void PhiPsiOptimizer::SampleAngles(int start, int end){

  mAngles=mBestAngles;

  double tmpAngles[2];
  for(int i=start; i<=end; i++) {
    //cerr<<i<<" "<<mIntLabel[i]<<endl;
    //mAngleSave[i]=mAngles[i];
  
    string& label=mLabel[i];
    int clusterIndex=atoi(label.c_str()+1);
    char clusterType=label[0];

    //all the angles are radian representation
    if(clusterType=='m') {
      //sample phi and psi for the middle residues
      mAngleSampler->sample2Dpdf(tmpAngles, 
                                 mPhiPsiCluster->getClusters(clusterIndex), 
                                 0);
      mAngles[i]=Vector2(tmpAngles[0],tmpAngles[1]);

    } else if(clusterType=='h') {
      //sample psi for the first residue
      mAngleSampler->sample1Dpdf(tmpAngles+1,
                                 mPsiCluster->getClusters(clusterIndex),
                                 0);
      mAngles[i][1]=tmpAngles[1];
    } else if(clusterType=='t') {
      //sample phi for the last residue
      mAngleSampler->sample1Dpdf(tmpAngles, 
                                 mPhiCluster->getClusters(clusterIndex), 
                                 0);
      mAngles[i][0]=tmpAngles[0];
    } else {
      //report error
      cerr << "Fatal error: unrecognized CRF labels sampled from the CRF model."
           << endl;
      exit(0);
    }
  }
}

void PhiPsiOptimizer::Undo(int start, int end){
/*
  //perform undo operations for local optimization purpse
  for(int i=start; i<=end; i++)
  {
    mLabel[i]=mLabelSave[i];
    mAngles[i]=mAngleSave[i];
  }

  // do we also need to rollback to the previous 3D structure ?
  // currently we donot need to do so because each time we build up the
  // whole structure from scratch
*/
}

bool PhiPsiOptimizer::detectClashes(){
  return ScoringByClashes::checkClashes(mStructure);
}

int PhiPsiOptimizer::countClashes(){
  return ScoringByClashes::countClashes(mStructure);
}

double PhiPsiOptimizer::calcEnergy(){
  return mScoringFunction->evaluate(mProteinSeq, mSecStruSeq, mStructure);
  //return evaluate(mProteinSeq, mSecStruSeq, mStructure);
}

void PhiPsiOptimizer::BuildStructure(int start, int end){
  //the structureBuilder now is an instance of PhiPsiBuilder
  PhiPsiBuilder* builder=dynamic_cast<PhiPsiBuilder*>(mStructureBuilder);

  if (mProtein) {
    delete mProtein; //free memory
    mProtein=0;
  }

  //Each time we build up the whole structure from scratch
  builder->build(mProteinSeq, mAngles, &mProtein);

  //assign values to mStructure also
  getCalphaPositions(mProtein, mStructure);
}

void PhiPsiOptimizer::KeepStructure(){
  mBestLabel=mLabel;
  mBestAngles=mAngles;
  mBestStructure=mStructure;

  //free memory allocated to mBestProtein
  if (mBestProtein) delete mBestProtein;
  mBestProtein=mProtein;

  // mProtein should be assigned to 0 in case the memory pointed to by 
  // mBestProtein is freed
  mProtein=0;
}

void PhiPsiOptimizer::writeDecoy(string& pdbfile){
  //ofstream out(pdbfile.c_str(),ios::out);
  //out << (*Protein);
  PDBFile ofile(pdbfile, std::ios::out);
  ofile.write(*mBestProtein);
  ofile.close();
}

/***********************************helper functions ********************************/

// the caller should make sure protein is not NULL and structure has enough 
// slots for the coordinates of Ca atoms
void getCalphaPositions(Protein* protein, vector<Vector3 >& structure){
  int i=0;
  for(ResidueIterator resIter = protein->beginResidue();
                      resIter!= protein->endResidue(); resIter++)
  {
    bool hasCA=false;
    for(AtomIterator at = resIter->beginAtom();
                     at!= resIter->endAtom(); at++, i++)
    {
      if (at->getName()!="CA") continue;
      structure[i]=at->getPosition();
      hasCA=true;
    }
    
    if (!hasCA){
      cerr << "Fatal error: the residue at position "<<i
           << " does not have a CA atom."<<endl;
      exit(0);
    }
  }
}

