//this file defines classes for all the scoring functions

#ifndef SCORINGFUNCTION
#define SCORINGFUNCTION

#include <vector>

#include "BALL/KERNEL/protein.h"
#include "BALL/MATHS/vector3.h"

#include "DOPE.h"
#include "DopeBeta.h"
#include "DopeBack.h"
#include "bbq.h"
#include "BMKhbond.h"
#include "TSP1Energy.h"
#include "ESPEnergy.h"
#include "DopeX.h"
#include "dDFIRE.h"
#include "DOPE_PW.h"
#include "ScoreMatrix.h"
#include "dopepw.h"
#include "Contact.h"
#include "EPAD.h"

using namespace std;
using namespace BALL;

// if the distance between two Ca atoms are less than this value, then there
// is a steric clashes
#define CLASH_DISTANCE_CUTOFF  4.0

//ScoringFunction is the base class of all the other scoring functions
class ScoringFunction
{
public:
  ScoringFunction();
  virtual ~ScoringFunction();

  virtual int Init(string dat_file1);
  virtual int Init(string dat_file1, string dat_file2);
  virtual int Init(string dat_file1, int* data);
  virtual int Init(string dat_file1, vector<string>& data);

  //estimate the score of the native structure of a protein
  virtual double estimateNativeScore(string& sequence, string& ss);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<Vector3 >& structure);
  virtual double evaluate(string& sequence, string& ss, Protein* protein);
  virtual double evaluate(vector<RESIDUE >& structure);
  virtual double evaluate(vector<RESIDUE >& structure, int start, int end);
  virtual double evaluate(string& ss, vector<RESIDUE >& structure);
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);

  // more evaluate functions to be added here for different representation
  // of a structure calculate the radius of a given protein

  double CalculateRadius(vector<Vector3 >& structure);
  Vector3 getPosition(ATOM a){
    //cerr << a.name << "("<<a.x<<", "<<a.y<<", "<<a.z<<")"<<endl; 
    return Vector3(a.x, a.y, a.z); 
  };

  bool isHydroPhobe(string aa) {
    //ALA 'A'; PHE 'F'; ILE 'I'; LEU 'L'; MET 'M'; VAL 'V'; TRP 'W'; TYR 'Y'; 
    string hydrophobes[] = 
             { "ALA", "PHE", "ILE", "LEU", "MET", "VAL", "TRP", "TYR"};
    for (int i=0; i<8; i++)
      if (aa==hydrophobes[i])
        return true;
    return false;
  }

  void MassCenter(vector<RESIDUE >& prot);
  void radial(vector<RESIDUE >& prot);
  void burial_ratio(vector<RESIDUE >& prot);
  double rg, ru, br, estimated_rg;
  Vector3 massCenter;

public:
  string mName;
  int bLongFormat, startRes, endRes, indicator;
  double NEFF;
  vector<double> pointEnergies;
  // 0--residue is missed; 1-- residue is real. 
  // We won't predict contact for those missing residues
  vector<int> residue_flag;
  double mHBEnergy;
  double mBurialPenalty;
};

// A very simple scoring function, which calculates the radius of a
// given structure
class ScoringByRadius: public ScoringFunction
{
public: 
  ScoringByRadius();
  ~ScoringByRadius();
  virtual double estimateNativeScore(string&, string& );

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<Vector3 >& structure);
  virtual double evaluate(string& sequence, string& ss, Protein* protein);
  virtual double evaluate(vector<RESIDUE >& structure);
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);
};

//a very simple scoring function, which calculates the radius of a given structure
class ScoringByContact: public ScoringFunction
{
public: 
  ScoringByContact();
  ~ScoringByContact();

  virtual int Init(string dat_file1, int* data);
  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);

protected:
  Contact* mpContact;
};

//this class uses the DOPE scoring function
class ScoringByDOPE: public ScoringFunction
{
public:
  ScoringByDOPE();
  ~ScoringByDOPE();

  virtual int Init(string dat_file1);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);
  virtual double evaluate(vector<RESIDUE >& structure);
  virtual double evaluate(vector<RESIDUE >& structure, int start, int end);

  double hbonds(vector<RESIDUE >& structure);

protected:
  // Calculate Dope energy between 2 residues, i.e. adding the energy
  // between all the possible pairs of atoms
  double calcResidueDOPE(RESIDUE& res1, RESIDUE &res2);
  
  // Modified version of A. Colubri's plugin that calculates pairwise
  // statistical DOPE potentials. 
  // A file named "dope.par" that record all the pairwise energies is
  // required by this plugin. 
  DOPE dope;

  bool calc_pw;
  ScoreMatrix res_energy;
};
  
typedef vector<Vector3 > AtomArray;

/*
#define FUNC_CONTACT_L5                 0
#define FUNC_CONTACT_L10                1
#define FUNC_CONTACT_TOP_5              2
#define FUNC_CONTACT_ENERGY_FM          3
#define FUNC_CONTACT_ENERGY_TB          4
#define FUNC_CONTACT_NUMBER_MORE        5
#define FUNC_CONTACT_NUMBER             6
#define FUNC_CONTACT_MAP_RECOVER        7
#define FUNC_CONTACT_MAP_ALL            8
#define FUNC_CONTACT_PAIRWISE_ENERGY  9
#define FUNC_CONTACT_DISTRIBUTION  10
*/
class ScoringByEPAD: public ScoringFunction
{
public:
  ScoringByEPAD();
  ~ScoringByEPAD();

  virtual int Init(string config_file, string dat_file);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss, vector<RESIDUE >& structure, int start);
  virtual double evaluate(vector<RESIDUE >& structure);
  virtual double evaluate(vector<RESIDUE >& structure, int start, int end);
  enum {FUNC_CONTACT_L5=0, FUNC_CONTACT_L10, FUNC_CONTACT_TOP_5,
        FUNC_CONTACT_ENERGY_FM, FUNC_CONTACT_ENERGY_TB,
        FUNC_CONTACT_NUMBER_MORE, FUNC_CONTACT_NUMBER,
        FUNC_CONTACT_MAP_RECOVER, FUNC_CONTACT_MAP_ALL,
        FUNC_CONTACT_PAIRWISE_ENERGY, FUNC_CONTACT_DISTRIBUTION_ENERGY,
        FUNC_CONTACT_DISTRIBUTION} mfunc;
  //int mfunc;

protected:
  int ResidueIndex(int aa);
  EPAD model;
  ScoreMatrix res_energy;
  bool predicted;
};

//this class uses the DOPE scoring function
class ScoringByDopeBeta: public ScoringFunction
{
public:
  ScoringByDopeBeta();
  ~ScoringByDopeBeta();

  virtual int Init(string dat_file1);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);
  virtual double evaluate(vector<RESIDUE >& structure, int start, int end);

  double mBurialPenalty;

protected:
  double calcResidueDOPE( RESIDUE& res1, RESIDUE &res2,
                          AtomArray& arr1, AtomArray& arr2, int cd);
  void CalculateBurial(vector<RESIDUE >& structure);
  double CalculatePenalty(vector<RESIDUE >& structure);
  int PenaltyIndex(string AAName);
  int PenaltyIndex(int aa);

  // Modified version of A. Colubri's plugin that calculates pairwise
  // statistical DOPE potentials. 
  // A file named "dope.par" that record all the pairwise energies is
  // required by this plugin. 
  DopeBeta dopeB;
  
  bool CB_CB_focus;
  int min_chain_dist, max_chain_dist, min_CB_CB_dist;
  double CB_CB_coefficient;

  bool Burial_renorm;
  double Penalty_coeff;
  double Burial_sphere_rad;
  int Burial_min_atcount, Burial_max_atcount;

  double* CB_burial;
  double* NB_burial;

  ScoreMatrix res_energy;
};

//this class uses the DOPE-Back scoring function
class ScoringByDOPEBack: public ScoringFunction
{
public:
  ScoringByDOPEBack();
  ~ScoringByDOPEBack();

  virtual int Init(string dat_file1);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);
  virtual double evaluate(vector<RESIDUE >& structure);

protected:
  double INTenergy(vector<RESIDUE >& structure, int interaction=0);
  // Calculate Dope energy between 2 residues, i.e. adding the energy
  // between all the possible pairs of atoms
  double calcResidueDOPEBack(RESIDUE& res0, RESIDUE &res1, int interaction=0);

  // Modified version of A. Colubri's plugin that calculates pairwise
  // statistical DOPE-Back potentials. 
  // A file named "dope-back.par" that record all the pairwise energies
  // is required by this plugin. 
  DopeBack dope_back;
  double interaction_coeff[4];
  double energy_coeff;
  bool add_bonded;
};

//this class uses the DOPE scoring function
class ScoringByDopeX: public ScoringFunction
{
public:
  ScoringByDopeX();
  ~ScoringByDopeX();

  virtual int Init(string dat_file1);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);

  double mBurialPenalty;

protected:
  // Calculate Dope energy between 2 residues, i.e. adding the energy
  // between all the possible pairs of atoms
  double calcResidueDOPE(RESIDUE& res1, RESIDUE &res2, int cd);
  double CalculateBurial(string& sequence, vector<RESIDUE >& structure);

  DopeX dopeX;
  
  bool CB_CB_focus;
  int min_chain_dist, max_chain_dist, min_CB_CB_dist;
  double energy_coeff, CB_CB_coefficient;

  int pap_vec[2000*2000];
  int dope_vec[2000*2000];

  bool Burial_renorm;
  double Penalty_coeff;
  double Burial_sphere_rad;
  int Burial_min_atcount, Burial_max_atcount;
};

//this class uses the DOPE scoring function
class ScoringByDopePW: public ScoringFunction
{
public:
  ScoringByDopePW();
  ~ScoringByDopePW();

  virtual int Init(string dat_file1);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& prot, int start);

  double mBurialPenalty;

protected:
  // Calculate Dope energy between 2 residues, i.e. adding the energy
  // between all the possible pairs of atoms
  double CalculateBurial(string& sequence, string ss, vector<RESIDUE >& prot);
  DopePW dopePW;
  dopepw_ac dpw_ac;
  
  bool add_bonded;
  int min_chain_dist, max_chain_dist, min_CB_CB_dist;
  double energy_coeff, CB_CB_coefficient;

  double Penalty_coeff;
  double Burial_sphere_rad;

  ScoreMatrix res_energy;
};

//this class uses the DopePW_RAMA scoring function
class ScoringByDopePW_RAMA: public ScoringFunction
{
public:
  ScoringByDopePW_RAMA();
  ~ScoringByDopePW_RAMA();

  // This function is necessary for initialization
  virtual int Init(string dat_file1);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& prot, int start);

  double mBurialPenalty;

protected:
  void SetPropWash(vector<RESIDUE >& prot);

  DopePW_RAMA dp;
  
  bool add_bonded;
  int min_chain_dist, max_chain_dist, min_CB_CB_dist;
  double energy_coeff, CB_CB_coefficient;

  double Penalty_coeff;
  double Burial_sphere_rad;
};

//this class uses the DOPE-Back scoring function
class ScoringByBMKHBBond: public ScoringFunction
{
public:
  ScoringByBMKHBBond();
  ~ScoringByBMKHBBond();

  // This function is necessary for initialization, 'coz we need to read
  // the config file for BMK and also need the confidence sequence for
  // the PSIPRED predicted secondary strctures
  virtual int Init(string dat_file1, int* data);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);

protected:
  // Calculate BMK energy between 2 residues, i.e. adding the energyi
  // between all the possible pairs of atoms
  double calcResidueBMKHBBond(RESIDUE& res1, RESIDUE &res2,
                          bool calculate_hbond, int ss1, int ss2);

  void CalculateBurial(vector<RESIDUE >& structure);
  double HBpenalty(vector<RESIDUE >& structure);

  TBMKhbond bmk;
  double Min_delta, Max_delta;

  int MinChainDist;

  int Burial_min_atcount, Burial_max_atcount;
  double Burial_sphere_rad, Penalty_coeff, Energy_coeff;
  double HBond_Min_Energy;
  bool Burial_renorm;

  double* CO_burial;
  double* NH_burial;

  int mConfidence[4096];

  ScoreMatrix res_energy;
};

//this class uses the DOPE-Back scoring function
class ScoringByTSP1: public ScoringFunction
{
public:
  ScoringByTSP1();
  ~ScoringByTSP1();

  virtual int Init(string dat_file1);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);
  virtual double evaluate(vector<RESIDUE >& structure);

protected:
  TSP1Energy tsp1;
};

//this class uses the ESP scoring function
class ScoringByESP: public ScoringFunction
{
public:
  ScoringByESP();
  ~ScoringByESP();

  virtual int Init(string dat_file1);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);
  virtual double evaluate(vector<RESIDUE >& structure);

protected:
  ESPEnergy esp;
};

//this class uses the ESP scoring function
class ScoringByNewESP: public ScoringFunction
{
public:
  ScoringByNewESP();
  ~ScoringByNewESP();

  virtual int Init(string dat_file1, vector<string>& data);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);
  virtual double evaluate(vector<RESIDUE >& structure, int start, int end);

protected:
  NewESPEnergy new_esp;

  ScoreMatrix res_energy;
};
//this class check the validity of a structure
//its evaluate function will return the number of steric clashes
class ScoringByClashes: public ScoringFunction
{
public:
  ScoringByClashes();
  ~ScoringByClashes();

  virtual double estimateNativeScore(string&, string&);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<Vector3 >& structure);
  virtual double evaluate(string& sequence, string& ss, Protein* protein);

  static bool checkClashes(vector<Vector3 >& structure);
  static int countClashes(vector<Vector3 >& structure);
  
  static bool checkClashes(Protein* protein);
  static int countClashes(Protein* protein);
};

// this class check the validity of a structure
// its evalute function will calculate the difference between the sequence's
// SS and the model'SS
class ScoringBySS: public ScoringFunction
{
public:
  virtual double estimateNativeScore(string&, string&);
  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<Vector3 >& structure);
  virtual double evaluate(string& sequence, string& ss, Protein* protein);
};

class ScoringFunctionByZhang: public ScoringFunction
{
public: 
  ScoringFunctionByZhang(){};
  ~ScoringFunctionByZhang(){};

  double e[20][20][3];  
  double R0[20][20][3];  
  bool ReadContact3_mm(string fname);
  
  double CalculateE15(int aai, int aaj, double r4, string ss, int i);

  double e15[20][20][16];
  double e15h[20][20][16];
  double e15s[20][20][16];
  bool ReadE15Comm(string fname);
  bool ReadE15hComm(string fname);
  bool ReadE15sComm(string fname);
  double E15(int aai, int aaj, double r4);
  double E15h(int aai, int aaj, double r4);
  double E15s(int aai, int aaj, double r4);
};

class ScoringByZhangShort: public ScoringFunctionByZhang
{
public: 
  ScoringByZhangShort();
  ~ScoringByZhangShort();

  // This function is necessary for initialization
  virtual int Init(string dat_file1);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);

  double getEnergy3(){return energy3;};
  double getEnergy4(){return energy4;};
  double getEnergy5(){return energy5;};

protected:
  double energy3, energy4, energy5;
  
  double e13[20][20][2];
  double e14[20][20][24];
  double e14h[20][20][24];
  double e14s[20][20][24];
  bool ReadE13Comm(string fname);
  bool ReadE14Comm(string fname);
  bool ReadE14hComm(string fname);
  bool ReadE14sComm(string fname);
  double E13(int aai, int aaj, double r2);
  double E14(int aai, int aaj, double r3);
  double E14h(int aai, int aaj, double r3);
  double E14s(int aai, int aaj, double r3);
  double CalculateE14(int aai, int aaj, double r3,
                      int chirality, string ss, int i);
};

// This is Zhang Yang's stiffness energy
class ScoringByZhangStiff: public ScoringFunction
{
public: 
  ScoringByZhangStiff();
  ~ScoringByZhangStiff();

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);
  virtual double evaluate(string& ss, vector<RESIDUE >& structure);
};

// Zhang Yang's HB energy
class ScoringByZhangHB: public ScoringFunction
{
public: 
  ScoringByZhangHB();
  ~ScoringByZhangHB();
  
  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);
  virtual double evaluate(string& ss, vector<RESIDUE >& structure);
};

class ScoringByZhangPair: public ScoringFunctionByZhang
{
public: 
  ScoringByZhangPair();
  ~ScoringByZhangPair();
  
  // This function is necessary for initialization
  virtual int Init(string dat_file1, string dat_file2);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);

protected:
  double e15[20][20][16];
  double E15(int aai, int aaj, double r4);
};

class ScoringByZhangBurial: public ScoringFunction
{
public: 
  ScoringByZhangBurial();
  ~ScoringByZhangBurial();
  
  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);
  virtual double evaluate(vector<RESIDUE >& structure);
};

// Zhang Yang's Electrostatic energy
class ScoringByZhangElectro: public ScoringFunction
{
public: 
  ScoringByZhangElectro();
  ~ScoringByZhangElectro();
  
  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);
  virtual double evaluate(string& ss, vector<RESIDUE >& structure);
};

class ScoringByZhangProfile: public ScoringFunctionByZhang
{
public: 
  ScoringByZhangProfile();
  ~ScoringByZhangProfile();
  
  // This function is necessary for initialization
  virtual int Init(string dat_file1, string dat_file2);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);

protected:
  int m_nMinChainDistance;
  double mCN;
  double V[9][9][9][20];  
  bool Read_contact_profile_comm(string fname);
};

class ScoringByZhangCOCN: public ScoringByZhangProfile
{
public: 
  ScoringByZhangCOCN();
  ~ScoringByZhangCOCN();
  
  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);

  double getContactNumber(){return mCN;};  
};

//this class uses the ESP scoring function
class ScoringBydDFIRE: public ScoringFunction
{
public:
  ScoringBydDFIRE();
  ~ScoringBydDFIRE();

  virtual int Init(string dat_file1);

  //calculate the score of a given structure
  virtual double evaluate(string& sequence, string& ss,
                          vector<RESIDUE >& structure, int start);
  virtual double evaluate(vector<RESIDUE >& structure, int start, int end);

protected:
  dDFIRE ddfire;
};

#endif
