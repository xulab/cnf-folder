#include "Kent.h"
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <BALL/MATHS/vector3.h>
#include "Constants.h"
using namespace BALL;
using namespace std;

double DOT(double* A, double* B) {
	 return A[0]*B[0]+A[1]*B[1]+A[2]*B[2];
}

void KentSampler::SetSeed() {
	unsigned int randomSeed=0;
	ifstream in("/dev/urandom",ios::in);
	in.read((char*)&randomSeed, sizeof(unsigned)/sizeof(char));
	in.close();

	unsigned id=getpid();
	randomSeed=randomSeed*randomSeed+id*id; 
	//we can set the random seed at only the main function
	srand48(randomSeed);
	srand(randomSeed);
}

KentSampler::KentSampler(double kappa, double beta, double* e1, double* e2, double* e3) {
 	a=4*(kappa-2*beta);
 	b=4*(kappa+2*beta);
 	gamma=(b-a)/2;
 	if (b>0) c2=0.5*b/(b-gamma);
 	else c2=0.0;
 
	lam1=sqrt(a+2*sqrt(gamma));
	lam2=sqrt(b); 
	rho=vector<Vector3 >(3);
	rho[0]=Vector3(e1[0],e2[0],e3[0]);
	rho[1]=Vector3(e1[1],e2[1],e3[1]);
	rho[2]=Vector3(e1[2],e2[2],e3[2]);
	//cout<<kappa<<" "<<beta<<endl;
	//cout<<rho[0][0]<<" "<<rho[0][1]<<" "<<rho[0][2]<<endl;
	//cout<<rho[1][0]<<" "<<rho[1][1]<<" "<<rho[1][2]<<endl;
	//cout<<rho[2][0]<<" "<<rho[2][1]<<" "<<rho[2][2]<<endl;
}	

//the caller should allocate at least 3 doubles to rev
void KentSampler::sample(double* rev) {
	double c1=1.0;
	while(true) {
		double v1=drand48(), v2=drand48(), u1=drand48(), u2=drand48();
		double x1=v1, x2=v2;
		 
		if((1-v1*(1-exp(-lam1)))>0 && lam1!=0) 
			x1=-log(1-v1*(1-exp(-lam1)))/lam1;
		if((1-v2*(1-exp(-lam2)))>0 && lam2!=0) 
			 x2=-log(1-v2*(1-exp(-lam2)))/lam2;
		
		if(x1*x1+x2*x2>=1) continue;
		double ratio1=exp(-0.5*(a*x1*x1+gamma*x1*x1*x1*x1)-c1+lam1*x1);
		//cout<<ratio1<<" "<<u1<<endl;
		if(u1>=ratio1) continue;
		double ratio2=exp(-0.5*(b*x2*x2-gamma*x2*x2*x2*x2)-c2+lam2*x2);
		//cout<<"U2: "<<u2<<" "<<ratio2<<endl;
		if(u2>=ratio2) continue;
		int sign1=SIGN(drand48()-0.5);
		int sign2=SIGN(drand48()-0.5);
		x1=x1*sign1;
		x2=x2*sign2;
		double theta=acos(1-2*(x1*x1+x2*x2));
		double phi=atan2(x2, x1);
		double ct=cos(theta);
		double st=sin(theta);
		double cp=cos(phi);
		double sp=sin(phi);
		double x=ct;
		double y=st*cp;
		double z=st*sp;
		rev[0]=(rho[0])[0]*x+(rho[0])[1]*y+(rho[0])[2]*z;
		rev[1]=(rho[1])[0]*x+(rho[1])[1]*y+(rho[1])[2]*z;
		rev[2]=(rho[2])[0]*x+(rho[2])[1]*y+(rho[2])[2]*z;
		break; 
	}
	//cout<<rev[0]<<" "<<rev[1]<<" "<<rev[2]<<endl;
}

KentDensity::KentDensity(double kappa1, double beta1, double *e11, double *e22, double *e33) {
	e1[0]=e11[0]; e1[1]=e11[1]; e1[2]=e11[2]; 
	e2[0]=e22[0]; e2[1]=e22[1]; e2[2]=e22[2]; 
	e3[0]=e33[0]; e3[1]=e33[1]; e3[2]=e33[2];
	beta=beta1;
	kappa=kappa1;
	log_c= log(2*PI)+kappa-log(sqrt((kappa-2*beta)*(kappa+2*beta)));
}

double KentDensity::density(double* x, bool log_space) {
	 double a=DOT(e2, x);
	 double b=DOT(e3, x);
	 double c=beta*(a*a-b*b);
	 double d=kappa*DOT(e1, x);
	 double ll=-log_c+d+c;
	 if(log_space)
		return ll;
	 else
		return exp(ll);
}
