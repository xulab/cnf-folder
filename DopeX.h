/*******************************************************************************

                                  DopeX.cpp
                                  --------

  Copyright (C) 2005 The University of Chicago

  Authors: 
  Andres Colubri

  Description:
  Plugin that calculates pairwise statistical potentials. In particular, it 
  can calculate DopeBeta, the statistical potential created by Min-yi Shen and 
  Andrej Sali at UCSF.
  This plugin can be configured to evaluate other statistical potentials by 
  providing the appropriate parameter file. 

*******************************************************************************/
#ifndef __DOPE_X_H__
#define __DOPE_X_H__

#include "DOPE.h"

using namespace std;

// This class contains the binned energy values.
class DopeX: public DOPE
{
public:
	DopeX();
	~DopeX();

	//double calcAtomDOPE(string residue_id1, string atom1, string residue_id2, string atom2, double dist, int cd, int od, int pap, char ss1, char ss1_1, char ss1_2, char ss1_3, char ss2);
	double calcAtomDOPE(string residue_id1, string atom1, string residue_id2, string atom2, double dist, int cd, int od, int pap, int ss1, int ss1_1, int ss1_2, int ss1_3, int ss2);
	void Init(string cfgFile);
	double DOPEenergy(int aa1, int at1, int aa2, int at2, int bin, int cd, int od, int pap, int ss1, int ss1_1, int ss1_2, int ss1_3, int ss2);

	int ESP(char a_char, int i){
		int aa = AALetter2Code(a_char);
		if (aa>=0 && aa<20 && i>=0 && i<3)
			return ESP_energy[aa*3+i];
		else
		{
			//cerr << "ERROE!! Trying to access invalid index of DopeX:ESP at [" << aa<<","<<i<<"] with " << a_char <<endl;
			//exit(0);
			return 0;
		}
	};

protected:
	void SetDefConfig();
	int ReadConfigFile(string cfgName);
	void SetAddAtoms(bool add_h, bool add_cm, bool add_sc, bool add_onlyCB) {
		add_hydrogens = add_h;
		add_centroids = add_cm;
		add_side_chains = add_sc;
		add_only_CB = add_onlyCB;
	};

public:
	// Parameters.
	double dist_cutoff;
	int num_bin;
	string dope_par_file, dope_dir, esp_par_file, EECDis3_par_file;
	bool add_bonded;
	bool add_hydrogens, add_centroids, add_side_chains, add_only_CB, CB_CB_focus, fix_consensus;
	int min_chain_dist, max_chain_dist, min_CB_CB_dist;
	double energy_coeff, CB_CB_coefficient, energy_cap;
	
	int ESP_energy[3*(20+1)];
	
	bool Burial_renorm, analysis;
	double Penalty_coeff;
	double Burial_sphere_rad;
	int Burial_min_atcount, Burial_max_atcount;
	
	double *EECDis1Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *EECDis2Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *EECDis3Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *EECDis4Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *EECDgt4p_CABpEnergy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *EECDgt4p_CABapEnergy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *EECDgt4p_CAB0Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *EECDgt4ap_CABpEnergy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *EECDgt4ap_CABapEnergy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *EECDgt4ap_CAB0Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *AllOtherCDgt4_CAB0Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *AllOtherCDgt4_CABpEnergy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *AllOtherCDgt4_CABapEnergy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *HHCDis1Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *HHCDis2Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *HHCDis3Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *HHCDis4Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *AllOtherCDis1Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *AllOtherCDis2Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *AllOtherCDis3Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	double *AllOtherCDis4Energy;//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1];
	
	double EECDis1Energy_coefficient;
	double EECDis2Energy_coefficient;
	double EECDis3Energy_coefficient;
	double EECDis4Energy_coefficient;
	double EECDgt4p_CABpEnergy_coefficient;
	double EECDgt4p_CABapEnergy_coefficient;
	double EECDgt4p_CAB0Energy_coefficient;
	double EECDgt4ap_CABpEnergy_coefficient;
	double EECDgt4ap_CABapEnergy_coefficient;
	double EECDgt4ap_CAB0Energy_coefficient;
	double AllOtherCDgt4_CAB0Energy_coefficient;
	double AllOtherCDgt4_CABpEnergy_coefficient;
	double AllOtherCDgt4_CABapEnergy_coefficient;
	double HHCDis1Energy_coefficient;
	double HHCDis2Energy_coefficient;
	double HHCDis3Energy_coefficient;
	double HHCDis4Energy_coefficient;
	double AllOtherCDis1Energy_coefficient;
	double AllOtherCDis2Energy_coefficient;
	double AllOtherCDis3Energy_coefficient;
	double AllOtherCDis4Energy_coefficient;
	
	string EECDis1Energy_par_file;
	string EECDis2Energy_par_file;
	string EECDis3Energy_par_file;
	string EECDis4Energy_par_file;
	string EECDgt4p_CABpEnergy_par_file;
	string EECDgt4p_CABapEnergy_par_file;
	string EECDgt4p_CAB0Energy_par_file;
	string EECDgt4ap_CABpEnergy_par_file;
	string EECDgt4ap_CABapEnergy_par_file;
	string EECDgt4ap_CAB0Energy_par_file;
	string AllOtherCDgt4_CAB0Energy_par_file;
	string AllOtherCDgt4_CABpEnergy_par_file;
	string AllOtherCDgt4_CABapEnergy_par_file;
	string HHCDis1Energy_par_file;
	string HHCDis2Energy_par_file;
	string HHCDis3Energy_par_file;
	string HHCDis4Energy_par_file;
	string AllOtherCDis1Energy_par_file;
	string AllOtherCDis2Energy_par_file;
	string AllOtherCDis3Energy_par_file;
	string AllOtherCDis4Energy_par_file;
};

#endif
