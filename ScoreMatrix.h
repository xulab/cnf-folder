#ifndef SCOREMATRIX_H
#define SCOREMATRIX_H

#include <cstdio>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <assert.h>
using namespace std;

typedef double Score;
typedef double LogScore;
const LogScore LogScore_ZERO = (LogScore) -2e20;
const LogScore LogScore_ZERO_REDUCED = LogScore_ZERO/2;


/* Computes log(x) */

inline LogScore Score_LOG (Score x){
  return log(x);
}

/* Computes exp(x) */

inline Score LogScore_EXP (LogScore x){

  return exp(x);
}

/* Computes log(exp(x) + 1) */

inline LogScore LogScore_LOOKUP (LogScore x){
  if (x > (LogScore) 50) return x;
  if (x < (LogScore) -50) return (LogScore) 0;
  return log(exp(x)+1);
}

/* Computes sum of two numbers in log space */

inline LogScore LogScore_ADD (LogScore x, LogScore y){
  if (x < y){ LogScore t = x; x = y; y = t; }
  if (y <= LogScore_ZERO_REDUCED) return x;
  return LogScore_LOOKUP(x-y) + y;
}

/* Computes sum of two numbers in log space */

inline void LogScore_PLUS_EQUALS (LogScore &x, LogScore y){
  if (x < y){ LogScore t = x; x = y; y = t; }
  if (y > LogScore_ZERO_REDUCED) 
    x = LogScore_LOOKUP(x-y) + y;
}

class ScoreMatrix {
public:
	int layers;
	int rows;
	int cols;
	Score default_value;
	Score *data;  
	string name;

public:
	ScoreMatrix (){data=0; rows=cols=0;default_value=0; name="";};
	ScoreMatrix (int rows, int cols, string n){ data=0; default_value=0; name=n; resize(rows, cols); }
	/* Copy constructor */
	ScoreMatrix (const ScoreMatrix &m) : rows(m.rows), cols(m.cols), default_value(m.default_value), data(m.data){}
	~ScoreMatrix(){delete data;}

	inline void resize(int rows, int cols, string n){ name=n; resize(rows, cols);}
	inline void resize(int rows, int cols){
		if (0==rows || 0==cols) {
			cerr << " ERROR: ScoreMatrix("<<name<<")-resize()" << rows << " rows; " << cols << " cols." << endl;
  			assert (0);
			//exit(0);
		}
		this->rows = rows; this->cols=cols;
		if (data) delete data;
		data = new Score[rows*cols];
		memset(data, 0, sizeof(Score)*rows*cols);
	}
/* Fill all matrix values */
	void Fill (const Score &value){ default_value=value; for (int i = 0; i < rows * cols; i++) data[i] = value;}
	inline Score &operator() (int row, int col){
		if (row>=rows || col>=cols) {
			cerr << "ERROR: ScoreMatrix("<<name<<")-&op() " << row << " vs. " << rows << "; " << col << " vs. " << cols<< endl;
  			assert (0);
			//exit(0);
		}
		if (row * cols + col<0) return default_value;
		return data[(row * cols + col)];
	}
	inline  Score operator() (int row, int col) const {
		if (row>=rows || col>=cols) {
			cerr << "ERROR: ScoreMatrix("<<name<<")-op()" << row << " vs. " << rows << "; " << col << " vs. " << cols<< endl;
  			assert (0);
			//exit(0);
		}
		if (row * cols + col<0) return default_value;
		return data[(row * cols + col)];
	}

	inline void set(int row, int col, Score score){
		if (row>=rows || col>=cols) {
			cerr << "ERROR: ScoreMatrix("<<name<<")-set " << row << " vs. " << rows << "; " << col << " vs. " << cols<< endl;
  			assert (0);
			//exit(0);
		}
		if (row * cols + col<0) return;
		data[(row * cols + col)] = score;
	}

	inline Score add(int row, int col, Score score){
		if (row>=rows || col>=cols) {
			cerr << "ERROR: ScoreMatrix("<<name<<")-add " << row << " vs. " << rows << "; " << col << " vs. " << cols<< endl;
  			assert (0);
			//exit(0);
		}
		if (row * cols + col<0) return default_value;
		data[(row * cols + col)] += score;
		return data[(row * cols + col)];
	}

	inline void divide(int row, int col, Score score){
		if (row>=rows || col>=cols) {
			cerr << "ERROR: ScoreMatrix("<<name<<")-divide " << row << " vs. " << rows << "; " << col << " vs. " << cols<< endl;
  			assert (0);
			//exit(0);
		}
		data[(row * cols + col)] /= score;
	}

	inline void clearRow(int row){ memset(data+row*cols, 0, sizeof(Score)*cols); }
	inline Score* operator[] (int row){ return &data[row*cols]; }

	inline void row_copy(int row, Score* dt){
		//for (int i=row*cols, j=0; j<cols; i++, j++)
		//	data[i]=dt[j];
		memcpy(&data[row*cols], dt, sizeof(Score)*cols);
	}

	inline void row_divide(int row, Score scale){
		for (int i=row*cols, j=0; j<cols; i++, j++)
			data[i]/=scale;
	}

	inline Score row_normalize(int row){
		Score scale=0;
		for (int i=row*cols, j=0; j<cols; i++, j++)
			scale+=data[i];
		for (int i=row*cols, j=0; j<cols; i++, j++){
			if (data[i]==0) continue;
			data[i]/=scale;
		}
		return scale;
	}
};

#endif
