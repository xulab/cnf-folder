#ifndef _PRO_TPL
#define _PRO_TPL
#include "misc.h"
class Protpl{
	static const int mres=25, matm=500;
	int nres, natm, rstart[mres];
	double x[matm][3];
	string anam[matm], rnam[mres];
public:
	Protpl();
	int findatom_tpl(string rn, string an);
	void calint_tpl(int *idx, int &chiral, double *ag);
};
#endif
