#include <math.h>
#include <stack>

#include "StructureBuilder.h"
#include "Constants.h"
#include "CRFSampler.h"
#include "ScoringFunction.h"

#include "BALL/STRUCTURE/peptideBuilder.h"
#include "BALL/STRUCTURE/peptides.h"
#include "BALL/KERNEL/protein.h"
#include "BALL/STRUCTURE/fragmentDB.h"

using namespace BALL;
using namespace std;

#define CA_BOND_LENGTH	3.8

StructureBuilder::StructureBuilder(){
	mProteinName.clear();
}


StructureBuilder::~StructureBuilder(){
}

void StructureBuilder::setProteinName(string& name){
	mProteinName=name;
}

void StructureBuilder::build(string& seq, vector<Vector2 >& angles,vector<Vector3 >& structure){
	cerr<<"function StructureBuilder::build(string& seq, vector<Vector2 >& angles,vector<Vector3 >& structure) not implemented yet!"<<endl;
	exit(-1);
}
void StructureBuilder::build(string& seq, vector<Vector2 >& angles,Protein** protein){
	cerr<<"function StructureBuilder::build(string& seq, vector<Vector2 >& angles,Protein** protein) not implemented yet!"<<endl;
	exit(-1);
}
void StructureBuilder::build(string& seq, vector<Vector2 >& angles,int start, vector<Vector3 >& structure){
	cerr<<"function StructureBuilder::build(string& seq, vector<Vector2 >& angles,int start, vector<Vector3 >& structure) not implemented yet!"<<endl;
	exit(-1);
}
void StructureBuilder::build(string& seq, vector<Vector2 >& angles,int start, Protein** protein){
	cerr<<"function StructureBuilder::build(string& seq, vector<Vector2 >& angles,int start, Protein** protein) not implemented yet!"<<endl;
	exit(-1);
}


//************************function for ThetaTauBuilder************************************/

//please note that the first and the last residue do not have a valid angle pair
//for the second residue, we have to randomly set the theta angle
//the caller has to make sure that angles and structure have the same size
void ThetaTauBuilder::build(string& seq, vector<Vector2 >& angles, vector<Vector3 >& structure){
	if (seq.size() != angles.size() || seq.size()!=structure.size()){
		cerr<<"inconsistency between protein seq size and structural size"<<endl;
		exit(-1);
	}

	//convert each angle pair to a Vector3
	vector<Vector3 > unitVectors(angles.size());
	for(int i=0; i< (int)angles.size();i++){
		thetaTau2XYZ(angles[i],unitVectors[i]);
	} 
	build(seq, unitVectors, 0, structure);
}

void ThetaTauBuilder::build(string& seq, vector<Vector2 >& angles, int start, vector<Vector3 >& structure){
	if (seq.size() != angles.size() || seq.size()!=structure.size()){
		cerr<<"inconsistency between protein seq size and structural size"<<endl;
		exit(-1);
	}

	//convert each angle pair to a Vector3
	vector<Vector3 > unitVectors(angles.size());
	for(int i=0; i< (int)angles.size();i++){
		thetaTau2XYZ(angles[i],unitVectors[i]);
	}
	build(seq, unitVectors, start, structure);
}
//build structure from a set of unit vectors, each vector reresent the direction of a Ca-Ca bond
//the caller has to make sure that unitVectors and structure have the same size
void ThetaTauBuilder::build(string& seq, vector<Vector3 >& unitVectors, vector<Vector3 >& structure){
	build(seq, unitVectors, 0, structure);
	//copy structure back
}

void ThetaTauBuilder::ApplyContactConstraint(vector<string>& labels, vector<Vector3 > & FBVec, vector<Vector3 > & structure, Contact* pContact, CRFSampler* pCRF, FB5Cluster* pFB5)
{
	pContact->resetFoldFlag();
	stack<Bipartite > bpStack;
	for(int i=0; i<(int)pContact->bpList.size(); i++){
		Bipartite bp=pContact->bpList[i];
		//cerr << "Searching for new segment: " << bp.head1 << "," << bp.tail2 << endl;
		bpStack.push(bp);
		while (!bpStack.empty() && pContact->isClosedBipartite(bp)){
			cerr << "Need to Fold segment: (" << bp.head1 << "," << bp.tail2 << ")" << endl;
			double err=FoldBipartite(bp, labels, FBVec, structure, pContact, pCRF, pFB5);
			err=FoldBipartite(bp, labels, FBVec, structure, pContact, pCRF, pFB5);
			cerr << "***********pContact->setBipartiteClose(bp). err=" << err << endl;
			pContact->setBipartiteClose(bp, err);
			bpStack.pop();
			if (!bpStack.empty()) {
				bp=bpStack.top();
				//cerr << "Searching for new segment: " << bp.head1 << "," << bp.tail2 << endl;
			}
		}
	}
	//for (int i=0; i<(int)structure.size(); i++)
		//cerr << "ATOM    "<<i<<"  CA  GLY    "<<i<<"  " << structure[i].x << "  " << structure[i].y << "  " << structure[i].z << endl; 
}

// return the error
double ThetaTauBuilder::FoldBipartite(Bipartite& bp, vector<string>& labels, vector<Vector3 > & FBVec, vector<Vector3 > & structure, Contact* pContact, CRFSampler* pCRF, FB5Cluster* pFB5)
{
	int seqLen=structure.size();
	int p1=bp.tail1, p2=bp.tail2;
	if (bp.head1>bp.head2){
		p1=bp.head2;
		p2=bp.head1;
	}
	double predicted_dist = 8;//pContact->contactMap(p1, p2);
	//cerr << "            seqLen=" << seqLen<< endl;
	int* changeFlags= new int[seqLen];
	memset(changeFlags, 0, sizeof(int)*seqLen);
	vector<string> possibleLabels;
	double min_err=100;
	for (int i=p1+1; i<p2; i++){
		if (pContact->isFolded(i)) 
			continue;
		changeFlags[i]++;

		Vector3 dc=structure[p2]-structure[p1];
		min_err = abs(dc.getLength()-predicted_dist);
		//cerr << " FoldBipartite at " << i << ": (initial label is " << labels[i] << ") and cur_err=" << min_err << endl;
		possibleLabels.clear();
		pCRF->getPossibleLabels(i, labels, possibleLabels);
		if (possibleLabels.size()==0) continue;

		Vector3 ab, best_ab, old_ab=structure[i+1]-structure[i];
		string best_label=labels[i];
		int clIdx=atoi(best_label.c_str()+1); 
		double best_kappa=pFB5->mCluster[clIdx].kappa;
		bool ab_changed = false;
		Vector3 da=structure[i]-structure[p1];
		Vector3 prev_bond=structure[i]-structure[i-1];
		Vector3 bc=structure[p2]-structure[i+1];
		for (int j=0; j<(int)possibleLabels.size(); j++){
			string label=possibleLabels[j];
			int clusterIndex=atoi(label.c_str()+1);
			double kappaFB5=pFB5->mCluster[clusterIndex].kappa;
			Vector3 u=pContact->unitVecs[clusterIndex];
			ab=old_ab;
			double err = SearchAngleObj(da, bc, u, predicted_dist, ab, kappaFB5);
			double angle= acos(prev_bond*ab);
			if (abs(angle)<3.14159/10) // we don't allow abrupt turns
				continue;
			int clash=CheckClash(structure, i, ab);
			//cerr<<"\t" << label << ": <" << u.x << "," << u.y << "," << u.z << ">, d="<<predicted_dist<<", err=" << err;
			//cerr<<"\tresult |ab|=|<" << ab.x <<","<< ab.y << ","<<ab.z<<">|="<<ab.getLength()<< " with " << clash << " clashes." << endl;

			if (err<min_err && clash==0){
				min_err=err;
				best_ab=ab;
				best_label=label;
				best_kappa=kappaFB5;
				ab_changed=true;
			}
		}
		//cerr << "\tmin_err="<< min_err << endl;
		// translate the structure according to the new ab
		if (ab_changed){
			Vector3 trans = best_ab-old_ab;
			//cerr << "@"<< i << ": label("<<labels[i] <<"-->"<<best_label<<":"<<best_kappa<<"); ("<<trans.x << "," << trans.y << "," << trans.z << ")\tmin_err="<< min_err << endl;
			for (int k=i+1; k<seqLen; k++)
				structure[k]+=trans;
			labels[i]=best_label;
			//cerr <<"  Norm <"<<FBVec[i].x<<","<<FBVec[i].y<<","<<FBVec[i].z<<"> to |<"<<best_ab.x<<","<<best_ab.y<<","<<best_ab.z<<">|="<<best_ab.getLength()<<endl;
			best_ab.normalize();
			FBVec[i]=best_ab;
		}
		if (min_err < 1)
			break;

		if (ab_changed && changeFlags[i]<3)
			i=max(p1+1, i-4);
	}
	delete changeFlags;
	return min_err;
}

int ThetaTauBuilder::CheckClash(vector<Vector3 > & structure, int pos, Vector3& new_ab)
{
	Vector3 old_ab=structure[pos+1]-structure[pos];
	Vector3 trans = new_ab-old_ab;
	vector<Vector3 > st(structure.size());
	for (int i=0; i< (int) structure.size(); i++){
		Vector3 res=structure[i];
		if (i>pos)
			res+=trans;
		st[i]=res;
	}
	//int clash=ScoringByClashes::countClashes(st);
	int clash=ScoringByClashes::checkClashes(st);
	return clash;
}

// rotate vector v with angle beta around the vector u
Vector3 ThetaTauBuilder::rotate(double beta, Vector3& u, Vector3& v)
{
	u.normalize();
	double s=sin(beta);
	double c=cos(beta);
	double t=1-c;
	double x=u.x;
	double y=u.y;
	double z=u.z;

	Vector3 R[3];
	R[0].x=t*x*x+c;		R[1].x= t*x*y-s*z;	R[2].x= t*x*z+s*y; 
	R[0].y=t*x*y+s*z;	R[1].y= t*y*y+c;	R[2].y= t*y*z-s*x; 
	R[0].z=t*x*z-s*y;	R[1].z= t*y*z+s*x;	R[2].z= t*z*z+c;

	return Vector3(v*R[0], v*R[1], v*R[2]);
}

double ThetaTauBuilder::SearchAngleObj(Vector3& da, Vector3& bc, Vector3& u, double dist, Vector3& ret_ab, double kappaFB5)
{
	double bondlength=ret_ab.getLength();
	double beta=acos(u.z);
	double alpha=atan2(u.x, u.y);
	Vector3 R[3], Z(0,0,1), f=u%Z; f.normalize();
	//cerr << "@beta="<<beta<<"\talpha="<<alpha<<"; |u|=|("<<u.x<<","<<u.y<<","<<u.z<< ")|="<<u.getLength()<<"; f=("<<f.x<<","<<f.y<<","<<f.z<<")"<<endl;
	//R[0].x=cos(beta); 		R[1].x=0; 		R[2].x=sin(beta);
	//R[0].y=sin(alpha)*sin(beta); 	R[1].y=cos(alpha); 	R[2].y=-sin(alpha)*cos(beta);
	//R[0].z=-cos(alpha)*sin(beta); 	R[1].z=sin(alpha); 	R[2].z=cos(alpha)*sin(beta);
	
	//cerr << "Matrx:" << endl << "\t" << R[0].x << " " << R[0].y << "  " << R[0].z<<endl; 
	//cerr <<"\t" << R[1].x <<"  "<< R[1].y << "  " << R[1].z << endl;
	//cerr << "\t" << R[2].x <<"  "<< R[2].y << "  " << R[2].z << endl; 
	double best_theta=0.01, best_tau=0, min_err=99999; 
	double ub=2*3.14159/kappaFB5; // kappa=(4.08, 739.7)
	for (double theta=0.01; theta<ub; theta+=0.015){
		for (double tau=0; tau<2*3.14159; tau+=0.1){
			//cerr << "##theta=" << theta << ", cos=" << cos(theta) << ", sin=" << sin(theta) << "; ##tau=" << tau << ", cos=" << cos(tau) << ", sin=" << sin(tau) << endl;
			Vector3 obp(cos(tau)*sin(theta),sin(tau)*sin(theta),0); //cerr << "\t|obp|=|("<< obp.x << "," << obp.y << "," << obp.z << ")|=" << obp.getLength();
			Vector3 obz=rotate(alpha, Z, obp);	//cerr << "\t|obz|=|("<< obz.x << "," << obz.y << "," << obz.z << ")|=" << obz.getLength();
			Vector3 ob=rotate(beta, f, obz); 	//cerr << "\t|ob|=|("<<ob.x << "," << ob.y << "," << ob.z << ")|=" << ob.getLength();
			Vector3 ao=u*cos(theta);		//cerr << "\t|ao|=|("<<ao.x << "," << ao.y << "," << ao.z << ")|=" << ao.getLength() << "; cos="<<ao*ob<< endl;
			Vector3 ab=(ao+ob)*bondlength; 		//cerr << "; ab=("<<ab.x << "," << ab.y << "," << ab.z << ")";
			Vector3 dc= da+ab+bc;			//cerr << "; dc=("<<dc.x << "," << dc.y << "," << dc.z << ")"<< endl;
			double e=abs(dc.getLength()-dist);
			if (min_err > e){
				best_theta=theta;
				best_tau=tau;
				min_err = e;
				ret_ab=ab;
			}
		}
	//exit(0);
	}
	return min_err;
}

//helper functions
//the caller needs to make sure that unitVectors and structure have the same size
//start is the starting position where the structure should be rebuilt from
//the coordinate of the (start+1)-th Ca atom will be rebuilt
void ThetaTauBuilder::build(string& seq, vector<Vector3 >& unitVectors, int start, vector<Vector3> & structure){
	int start_=start;
	double bondLength=CA_BOND_LENGTH;
	if (start <=2){
		if ((int)presetBondLengths.size()>start && presetBondLengths[start]>0)
			bondLength=presetBondLengths[start];

		//fix the positions of the first three Ca atoms
		//assume the second residue is on the origin of the coordinate system
		structure[1]=Vector3(0,0,0);

		//set the first unitVector as (1,0,0)
		unitVectors[0]=Vector3(1,0,0);
		structure[0]=structure[1]-unitVectors[0]*bondLength;

		//randomly pick up one theta for the second residue
		//thetaPool is defined in Constants.h

		double theta=thetaPool[random() % thetaPoolSize];
		theta*=PI;
		theta/=180;

		//calculate the direction of the second unitVector	
		unitVectors[1].x=cos(PI-theta);
		unitVectors[1].y=sin(PI-theta);
		unitVectors[1].z=0;

		structure[2]=structure[1]+unitVectors[1]*bondLength;

		start_=2;
	}

	int end_=structure.size()-2;

	for(int i=start_; i<= end_; i++){
		if ((int)presetBondLengths.size()>end_ && presetBondLengths[i]>0)
			bondLength=presetBondLengths[i];
		else
			bondLength=CA_BOND_LENGTH;
		//cerr<<"Building coordinates for "<<i+1<<"-th Ca atom. bondLength="<<bondLength<<endl;
		//cerr << "unitVector="<<unitVectors[i]<<endl;
		build(unitVectors[i], structure[i-2],structure[i-1],structure[i],structure[i+1], bondLength);
	}
}

//build the coordinates of the fourth Ca atoms from the previous three and the unitVector
void ThetaTauBuilder::build(Vector3& unitVector, Vector3& p1, Vector3& p2, Vector3& p3, Vector3& p4, double bondLength){

	if (unitVector.getLength()<0.01){
		cerr << "p1="<<p1 <<endl;
		cerr << "p2="<<p2<<endl;
		cerr << "p3="<<p3<<endl;
		cerr << "unitVector="<<unitVector<<endl;
		exit(-1);
	}

	Vector3 xAxis, yAxis, zAxis;

	try {
	xAxis=p2-p3;
	xAxis.normalize();

	Vector3 diff12=p1-p2;
	Vector3 diff32=p3-p2;

	//zAxis equal to the cross product of two vectors
	zAxis=diff32 % diff12;
	zAxis.normalize();

	//yAxis is equal to the cross product of x and z
	yAxis=xAxis % zAxis;
	yAxis.normalize();
	}catch(Exception::DivisionByZero){
		cerr << "p1="<<p1 <<endl;
		cerr << "p2="<<p2<<endl;
		cerr << "p3="<<p3<<endl;
		exit(-1);
	}

	double in[9];
	double out[9];

	//assign x,y, z to in
	in[0]=xAxis.x;
	in[1]=xAxis.y;
	in[2]=xAxis.z;

	in[3]=yAxis.x;
	in[4]=yAxis.y;
	in[5]=yAxis.z;

	in[6]=zAxis.x;
	in[7]=zAxis.y;
	in[8]=zAxis.z;

	//out is the inverse of in
	invert(in, out);

	//asign out to out1, out2 and out3
	Vector3 out1(out[0],out[1],out[2]);
	Vector3 out2(out[3],out[4],out[5]);
	Vector3 out3(out[6],out[7],out[8]);

	p4.x=unitVector*out1;
	p4.y=unitVector*out2;
	p4.z=unitVector*out3;

	p4*=bondLength;

	//add p3 to p4 to get the positions of p4
	p4+=p3;

}

void ThetaTauBuilder::build_final(string& seq, vector<Vector2>& angles, vector<Vector3>& structure, vector<double>& bondlenList, vector<int>& m_aligned, vector<Vector3> InitCAs){
        int head=0, tail=0;
        for(int i=0;i<(int)m_aligned.size();i++)
                if(m_aligned[i]==1){
                        head = i;
                        break;
                }
        for(int i=m_aligned.size()-1;i>=0;i--)
                if(m_aligned[i]==1){
                        tail = i;
                        break;
                }

        Vector2 angle;
        double ca_bond_len;

        for(int i=head;i<=tail;i++) structure[i]=InitCAs[i];

        vector<Vector3 > uVh(angles.size());
        vector<Vector3 > uVt(angles.size());
        for(int i=1; i< (int)angles.size();i++){
		//cerr << i <<"\t" << angles[i].x << "\t" << angles[i].y << "==" << bondlenList[i] << endl;
                thetaTau2XYZ(angles[i],uVt[i]);
                angle.x = angles[i].x;
                angle.y = angles[i+1].y;
                thetaTau2XYZ(angle,uVh[i]);
        }
	//cerr << endl;

        for(int i=tail;i<=(int)structure.size()-2;i++){
                if(m_aligned[i]) ca_bond_len=bondlenList[i];
                else ca_bond_len=3.8;
                build(uVt[i], structure[i-2],structure[i-1],structure[i],structure[i+1],ca_bond_len);
        }

        for(int i=head;i>=1;i--){
                if(m_aligned[i-1]) ca_bond_len=bondlenList[i-1];
                else ca_bond_len=3.8;
                build(uVh[i], structure[i+2],structure[i+1],structure[i],structure[i-1],ca_bond_len);
        }
}


//**********************functions for PhiPsiBuilder***************************************/
PhiPsiBuilder::PhiPsiBuilder(){
	StructureBuilder();
	mFragmentDB=new FragmentDB("");
}

PhiPsiBuilder::~PhiPsiBuilder(){
	if (mFragmentDB) delete mFragmentDB;
}

//need to make sure all the angles have a consistent representation
void PhiPsiBuilder::build(string& seq, vector<Vector2 >& angles, Protein** protein){
	//save angles
	//construct a sequence of amino acid descriptor
	vector<Peptides::AminoAcidDescriptor> AAdescriptor(angles.size());

	for(int i=0;i<(int)angles.size(); i++){
		AAdescriptor[i].setAminoAcidType(seq[i]);
		AAdescriptor[i].setPhi(Angle(angles[i].x));
		AAdescriptor[i].setPsi(Angle(angles[i].y));
	}

	Peptides::PeptideBuilder builder(AAdescriptor);
	builder.setProteinName(mProteinName);
	builder.setFragmentDB(mFragmentDB);
	*protein=builder.construct();

	//convert Protein to structure
}

/******************************helper functions*****************************************/

//invert a matrix in to out
//the caller has to allocate memory to both in and out
void invert(double* in, double *out)
{       
        // Invert Matrix
        
        double KLx=in[0];
        double KLy=in[3];
        double KLz=in[6];
        double KLp=in[1];
        double KLq=in[4];
        double KLr=in[7];
        double KLa=in[2];
        double KLb=in[5]; 
        double KLc=in[8];

        double det=KLx*((KLq*KLc)-(KLb*KLr))-KLy*((KLp*KLc)-(KLa*KLr))+KLz*((KLp*KLb)-(KLq*KLa));
//      if(det==0)              // error - cannot invert matrix
//              return;

        // Calculate cofactors
  
        out[0]=((KLq*KLc)-(KLb*KLr));
        out[1]=-((KLp*KLc)-(KLa*KLr));
        out[2]=((KLp*KLb)-(KLq*KLa));
        out[3]=-((KLy*KLc)-(KLb*KLz));
        out[4]=((KLx*KLc)-(KLa*KLz));
        out[5]=-((KLx*KLb)-(KLa*KLy));
        out[6]=((KLy*KLr)-(KLq*KLz));
        out[7]=-((KLx*KLr)-(KLp*KLz));
        out[8]=((KLx*KLq)-(KLp*KLy));

        for (int i=0; i<9; i++)
                out[i] *= 1.0/det;

/*
	double veri=in[0]*out[0]+in[1]*out[3]+in[2]*out[6];
  	double veri2=in[3]*out[1]+in[4]*out[4]+in[5]*out[7];
  	double veri3=in[6]*out[2]+in[7]*out[5]+in[8]*out[8];

  	double veri4=in[3]*out[0]+in[4]*out[3]+in[5]*out[6];
  	double veri5=in[6]*out[1]+in[7]*out[4]+in[8]*out[7];
  	double veri6=in[0]*out[2]+in[1]*out[5]+in[2]*out[8];

  	double veri7=in[0]*out[0]+in[1]*out[3]+in[2]*out[6];
  	double veri8=in[3]*out[1]+in[4]*out[4]+in[5]*out[7];
  	double veri9=in[6]*out[2]+in[7]*out[5]+in[8]*out[8];
*/

}

//angle.x is theta and angle.y is tau
//convert a pair of angles to a unit vector
void thetaTau2XYZ(Vector2 & angle, Vector3 & unitVector){
	unitVector.x=cos(angle.x);
	unitVector.y=sin(angle.x)*cos(angle.y);
	unitVector.z=sin(angle.x)*sin(angle.y);
}

