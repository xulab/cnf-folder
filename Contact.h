
#ifndef _CONTACT_H
#define _CONTACT_H

#include <vector>
#include <time.h>
#include <string>
#include <math.h>

#include "BALL/MATHS/vector3.h"
#include "ScoreMatrix.h"

using namespace BALL;

typedef vector<int> NodeList;

struct Bipartite {
	Bipartite(){folded1=folded2=inversed=false;};

	// we require that head1<tail1<min(head2,tail2)
	int head1, tail1, head2, tail2;
	bool folded1, folded2; // folded1==true if the first party is folded; folded2==true for the second party
	bool inversed;
	double min_err; // the error toward the predicted distance after minimization

	int idx;
};

// sorted list by head1 and head2
class BipartiteList: public vector<Bipartite > 
{
public:
	void Insert(Bipartite& bp);
	// search for the Bipartite contain the give pair <p1, p2> using binary search
	Bipartite* search(int pos1, int pos2);
	Bipartite* searchUnfoldedWithin(int pos1, int pos2);
	void searchUnfoldedWithin(int pos1, int pos2, int* unfoldedBP);
	bool isFolded(int pos); 
	void InitFoldFlags(int seqLen);
	void SetFoldFlag(int idx, int fold);

private:
	NodeList foldFlags;
	int seqLen;
};

class BipartiteDAG {
public:
	BipartiteDAG();
	void initBipartiteDAG(BipartiteList& bpList);

	// Add a note into DAG, return NULL if no circle found; or return the node with the shortest length if adding bp incurs a circle
	bool addNode(int bp_idx, int child_idx);
	void removeNode(int node_idx); // remove a node from the DAG

private:
	NodeList ends;
	ScoreMatrix starters;
};

class Contact {
public:
	Contact(){};
	Contact(string contact_file, int seq_length);

	int seq_length;
	vector<Vector3 > unitVecs;
	BipartiteList bpList;
	ScoreMatrix contactMap;
	int nFolded;
	BipartiteDAG bpDAG;

	void buildBipartiteList();
	void buildBipartiteDAG();
	void readContactFile(string contact_file);
	bool isClosedBipartite(Bipartite& bp);
	bool isFolded(int pos);
	void resetFoldFlag();

	void setBipartiteClose(Bipartite& bp, double err);
};
#endif
