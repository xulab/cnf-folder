#include <cstdlib>
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <values.h>

#include "newconfig.h"
#include "ConformationOptimizer.h"
#include "StructureBuilder.h"
#include "stringTokenizer.h"
#include "strtokenizer.h"
#include "newutil.h"

#include <BALL/KERNEL/protein.h>
#include <BALL/KERNEL/atomContainer.h>
#include <BALL/FORMAT/PDBFile.h>
#include <BALL/KERNEL/residue.h>
#include <BALL/KERNEL/atom.h>
#include <BALL/STRUCTURE/geometricProperties.h>

#include "bbq.h"

using namespace std;
using namespace BALL;

int MAX_SEG_LENGTH=15;

#define MODEL_PHIPSI  1
#define MODEL_THETATAU  2

#define ALG_GREEDY  1
#define ALG_MCMC  2
#define ALG_REMC  3 //Replica Exchange

#define SCORE_RADIUS   1
#define SCORE_DOPE   2
#define SCORE_DOPE_BACK 3
#define SCORE_BMK   4
#define SCORE_TSP   5
#define SCORE_ESP  6
#define SCORE_DOPE_BETA 7

#define DEF_CONFIG_FILE  "proteinFolder.conf"
#define DEF_OUT_DIR  "./"

void Usage(){
  cerr<< "Usage: ProteinFolder [-A additional_file] [-R refinementPDB [-S refineSegments] [-M mergePDB]] [-C Contact File] [-p numbers] [-m model ] [-a algorithm] [-e scoring] [-t temperature] [-s] [-r random_seed] [-f config_file] [-o out_dir] [-n native_path] proteinName"<<endl;
  cerr << "\t-A: additional file used. For now, this option is used by the EPAD energy function only. For each target, we need to provide the *.epad feature file."<<endl;
  cerr << "\t-R: the PDB file for refinement. When the -R option is used, -S must be used to give a description file to indicate the segments for refinement. A *.pir file will suffice."<<endl;
  cerr << "\t\t-M: the mergePDB file is actually the decoys sampled for the refinementPDB. If this option is provided, we rotate the decoy to the same coord system with the template." << endl;
  cerr << "\t-C: the contact file contains the pairs of predicted distance between residues. ProteinFolder will use the distances as slack constraints during folding." << endl;
  cerr << "\t-p: this option prints the sampling probability table(Vi) for the postions in numbers. Example: 23, 12-15, or 12,15,31"<<endl;
  cerr << "\t-m model: protein representation model: thetatau or phipsi (def=thetatau)"<<endl;
  cerr << "\t-a algorithm: conformation optimization algorithm: greedy, Monte Carlo(MCMC) or Replica Exchange(REMC). (def=greedy)"<<endl;
  cerr << "\t-e scoring: the scoring function: by radius or a true scoring function (def=radius)"<<endl;
  cerr << "\t-t temperature: the absolute temperature used for DOPE. Default value is 273.0 "<<endl;
  cerr << "\t-s: this option enables the MCMC search to use Simulated Annealing " <<endl;
  cerr << "\t-r random_seed: the seed for random generator. If not specified, the program will generate the seed from system state. This seed will also be used to name the result file."<<endl;
  cerr << "\t-f config_file: configuration file name (def=proteinFolder.conf)"<<endl;
  cerr << "\t-o out_dir: output directory (def=./)"<<endl;
  cerr << "\t-n native_path: the path of the native protein (used for calculating the RMSD during sampling for test)."<<endl;
  cerr << "\t  proteinName: protein name, its corresponding sequence file and ss file are proteinName.seq and proteinName.newss, respectively."<<endl;
  cerr << "\tBuilt Date: 20120312"<<endl;
}

Config* config=0;
double temperature_DOPE = 273.0;

// read the pdb file to get the structure of a protein and fill into <protein>
// return -1 if not succeed
int GetConformation(string pdb_file, vector<RESIDUE> &g_backbone, 
                    vector<Vector2>& g_angle)
{
  g_backbone.clear();
  Protein* protein = new Protein;
  // read the pdb protein file
        PDBFile* pdb=0;
  try{
    pdb = new PDBFile(pdb_file, std::ios::in);
  }catch(Exception::FileNotFound){
    cerr<<"Can not find protein file "<<pdb_file <<endl;
    return -1;
  }

  *pdb >> *protein;
  cerr << "protein " << pdb_file << " read with " 
       << protein->countChains() << " chains; ";
  cerr << protein->countResidues() << " residues; " 
       << protein->countPDBAtoms() << " atoms\n";

  int nRes=0;
  vector<Atom> CAs;
  // retrieve the residue and atoms from the Protein structure and insert
    // into the bbq acceptable structure
  for (ResidueIterator resIter = protein->beginResidue(); 
                       resIter!= protein->endResidue(); resIter++)
  {
    RESIDUE res;
    res.name= resIter->getFullName(Residue::NO_VARIANT_EXTENSIONS);
    res.seq = nRes;
    if (res.name.size()<3)
      break; //continue;
    Vector3 pos;
    for(AtomIterator at=resIter->beginAtom();at!=resIter->endAtom();at++){
      pos = at->getPosition();
      ATOM atom;
      atom.name = at->getName();

      atom.x = pos.x;
      atom.y = pos.y;
      atom.z = pos.z;

      if (nRes==(int)protein->countResidues()-1)
        cerr << nRes << ":" << atom.name 
             << "(" << atom.x << "," << atom.y << ","<< atom.z << endl;
      if (atom.name!="N" && atom.name!="O" && atom.name!="CA" 
          && atom.name!="CB" && atom.name!="C" && atom.name!="NH" 
          && atom.name!="HN")
      {
        continue;
      }
      res.addAtom( atom);
      if (at->getName()=="CA") 
        CAs.push_back(*at);
      //cerr << "BALLatom=" << atom.name << "(" << atom.x << ", " << atom.y << ", " << atom.z << ")\n";
    }
    nRes++;
    
    if (res.getAtom("CA").name=="")
      break;

    ATOM C, CB, N, O;
    C.name = "C";
    CB.name = "CB";
    O.name = "O";
    N.name = "N";
    if (res.getAtom("CB").name==""&&res.name!="GLY")
      res.addAtom(CB);
    if (res.getAtom("C").name=="")
      res.addAtom(C);
    if (res.getAtom("O").name=="")
      res.addAtom(O);
    if (res.getAtom("N").name=="")
      res.addAtom(N);

    g_backbone.push_back(res);
  }
  

  // Calculate the bond angles     
  g_angle.resize(CAs.size());
  vector<Vector3> FBVec(CAs.size());
  for(int i=0;i<(int)CAs.size();i++){
    Angle tau(2*Constants::PI);
    Angle theta(2*Constants::PI);
    if (i>0 && i<(int)CAs.size()-1){
      //theta exists
      //here we use 180 - theta to make it consistent with the program dang's output
      theta=Angle(Constants::PI)-calculateBondAngle(CAs[i-1],CAs[i],CAs[i+1]);
    }

    if (i>1 && i < (int)CAs.size()-1){
      //tau exists
      tau=calculateTorsionAngle(CAs[i-2],CAs[i-1],CAs[i],CAs[i+1]);
    }
    Vector2 angle;
    angle.x = theta.toDegree()*Constants::PI/180.;
    angle.y = tau.toDegree()*Constants::PI/180.;
    //cerr <<i<< '\t'<< angle.x <<'\t'<< angle.y << "==";
    g_angle[i]=angle;
    thetaTau2XYZ(g_angle[i],FBVec[i]);
  }

  for(int i=0;i<(int)CAs.size();i++){
    g_angle[i].x = acos(FBVec[i][0]);
    g_angle[i].y = atan2(FBVec[i][2],FBVec[i][1]);
  }

  //cerr << endl;


  if (protein) delete protein;
  if (pdb) delete pdb;

  cerr << "getConformation done." << endl;
  return 1;
}

int main(int argc, char** argv)
{
  if(argc<2) {
    Usage();
    exit(0);
  }
  
  int model=MODEL_THETATAU;
  int algorithm=ALG_GREEDY;
  int scoring=SCORE_RADIUS;
  string configFile=DEF_CONFIG_FILE;
  string outdir=DEF_OUT_DIR;
  string sNativeFile="", contact_file="", additional_f="";

  string refine_pdb="", segments_file="", merge_pdb="";
  
  unsigned int randomSeed=0;

  extern char* optarg;
  extern int optind;
  
  char c=0;
  int bUseSimulatedAnnealing = 0, DEBUG_MODE=0;
  vector<int> arrPrint;
  while((c=getopt(argc,argv,":A:R:S:M:C:m:n:a:e:r:f:o:t:s:p:L:D:"))!=EOF){
    switch(c)
    {
    case 'A':
      additional_f=optarg;
      break;
    case 'L':
      MAX_SEG_LENGTH=atoi(optarg);
      break;
    case 'D':
      DEBUG_MODE=atoi(optarg);
      break;
    case 'R':
      refine_pdb=optarg;
      break;
    case 'S':
      segments_file=optarg;
      break;
    case 'M':
      merge_pdb=optarg;
      break;
    case 'C':
      contact_file=optarg;
      break;
    case 'm':
      if (strcasecmp(optarg,"phipsi")==0)
        model=MODEL_PHIPSI;
      break;
    case  'a':
      if (strcasecmp(optarg,"MCMC")==0)
        algorithm=ALG_MCMC;
      else if (strcasecmp(optarg,"REMC")==0)
        algorithm=ALG_REMC;
      break;
    case 'e':
      if (strcasecmp(optarg, "dope")==0)
        scoring=SCORE_DOPE;
      else if (strcasecmp(optarg, "dope-back")==0)
        scoring=SCORE_DOPE_BACK;
      else if (strcasecmp(optarg, "bmk")==0)
        scoring=SCORE_BMK;
      else if (strcasecmp(optarg, "tsp")==0)
        scoring=SCORE_TSP;
      else if (strcasecmp(optarg, "esp")==0)
        scoring=SCORE_ESP;
      else if (strcasecmp(optarg, "dopebeta")==0)
        scoring=SCORE_DOPE_BETA;
      break;
    case 'r':
      randomSeed=atoi(optarg);
      break;
    case 'f':
      configFile=optarg;    
      break;
    case 'o':
      outdir=optarg;
      break;
    case 't':
      temperature_DOPE = atof(optarg);
      break;
    case 's':
      bUseSimulatedAnnealing = 1;
      break;
    case 'n':
      sNativeFile=optarg;
      break;
    case 'p':
      {
        StringTokenizer* strTokens=new StringTokenizer(optarg,",");
        while (1){
        char* token=strTokens->getNextToken();
        if (!token){ 
          if (strTokens) delete strTokens;
          break;
        }
        arrPrint.push_back(atoi(token));
        }
      }
      break;
    default:
      Usage();
      exit(0);
    }
  }

  string proteinName;
  if (optind<argc) proteinName=argv[optind];
  //set random seed
  if (randomSeed <=0 ) {
    // here we donot use time() to get the random seed, instead we use the
    // system kernel state to generate a seed
    ifstream in("/dev/urandom",ios::in);
    in.read((char*)&randomSeed, sizeof(unsigned)/sizeof(char));
//    cout<<randomSeed<<endl;
    in.close();

    unsigned id=getpid();
    randomSeed=randomSeed*randomSeed+id*id;
    srand(randomSeed);
    srandom(randomSeed);
    srand48(randomSeed);
  }

  //read configuration file
  config=new Config(configFile);
  config->configure();
        
  //generate three major objects
  ConformationOptimizer* conformationOptimizer=0;
  StructureBuilder* structureBuilder=0;

  if (model==MODEL_THETATAU){
    conformationOptimizer=new ThetaTauOptimizer();
    structureBuilder=new ThetaTauBuilder();
  }else if (model==MODEL_PHIPSI){
    conformationOptimizer=new PhiPsiOptimizer();
    structureBuilder=new PhiPsiBuilder();
  }

  conformationOptimizer->setProteinName(proteinName);
  structureBuilder->setProteinName(proteinName);
  conformationOptimizer->setStructureBuilder(structureBuilder);
  if ((int)sNativeFile.size()>0)
    conformationOptimizer->GetNativeConformation(sNativeFile);
  
  cerr << "checkpoint 4\n";
  //Initialize has to be called before other actions
  conformationOptimizer->Initialize(randomSeed, DEBUG_MODE, additional_f);
  if (contact_file.length()>0)
    conformationOptimizer->setContactConditions(contact_file);

  cerr << "checkpoint 5\n";
  vector<Vector2> g_angle;
  vector<RESIDUE> g_merge;
  vector<RESIDUE> g_backbone;
  vector<Vector3> initCAs;
  vector<double> bondlenList; 
  //bondlenList.push_back(3.8);
/////////////////////// Feng Test ///////////////////////
  if (arrPrint.size()){
    for (int i=0; i<(int)arrPrint.size(); i++){
      int pos=arrPrint[i];
      conformationOptimizer->printVi(pos);
    }
  } else {
    if (refine_pdb!=""){
      if (merge_pdb!=""){
        GetConformation( merge_pdb, g_merge, g_angle);
        conformationOptimizer->calcLSP(g_merge, true);
        conformationOptimizer->bMergePDB=true;
  
        RESIDUE resL=g_merge[0];
        ATOM caL=resL.getAtom("CA");
        for (int i=1; i<(int)g_merge.size(); i++){
          RESIDUE res=g_merge[i];
          ATOM ca=res.getAtom("CA");
          double dl=sqrt((ca.x-caL.x)*(ca.x-caL.x)+(ca.y-caL.y)*(ca.y-caL.y)
                                                  +(ca.z-caL.z)*(ca.z-caL.z));
          conformationOptimizer->presetBondLengths.push_back(dl);
          bondlenList.push_back(dl);
          caL=ca;
        }
        vector<Vector2> g_angle1;
        GetConformation( refine_pdb, g_backbone, g_angle1);
        initCAs.resize(g_backbone.size());
        for (int i=0; i<(int)g_backbone.size(); i++){
          RESIDUE res=g_backbone[i];
          ATOM ca=res.getAtom("CA");
          initCAs[i].x=ca.x;
          initCAs[i].y=ca.y;
          initCAs[i].z=ca.z;
        }
      } else {
        GetConformation( refine_pdb, g_backbone, g_angle);
        conformationOptimizer->calcLSP(g_backbone, true);
        conformationOptimizer->ReAngle();
  
        RESIDUE resL=g_backbone[0];
        ATOM caL=resL.getAtom("CA");
        for (int i=1; i<(int)g_backbone.size(); i++){
          RESIDUE res=g_backbone[i];
          ATOM ca=res.getAtom("CA");
          double dl=sqrt((ca.x-caL.x)*(ca.x-caL.x)+(ca.y-caL.y)*(ca.y-caL.y)
                                                  +(ca.z-caL.z)*(ca.z-caL.z));
          conformationOptimizer->presetBondLengths.push_back(dl);
          caL=ca;
        }
      }

      string segment = segments_file;
      int minus_pos = segment.find('-');
      int seg_start = atoi(segment.substr(0,minus_pos).c_str()),
          seg_end = atoi(segment.substr(minus_pos+1).c_str());
      cerr << "     REFINING Segment: " << seg_start << "--" << seg_end << endl;
      for (int i=0; i<(int)g_backbone.size(); i++){
        if (i>=seg_start && i<=seg_end)
          // no preset bond length, using 3.8
          conformationOptimizer->presetBondLengths[i]=-1;
        //cerr << conformationOptimizer->presetBondLengths[i] << ",";
      }
      conformationOptimizer->refine_segments.push_back(make_pair(seg_start,seg_end));
/*
      ifstream idataf(segments_file.c_str());
      string line;
      int metFirstSEQ=0, metSecondSEQ=0;
      string sequence="",sequence1="", sequence2="";
      while (getline(idataf, line)) {
        cerr << line << endl;
        if ((line[0]>='a' && line[0]<='z') || line=="") continue;
        else if (line[0]=='>'){
          if (!metFirstSEQ) metFirstSEQ = 1;
          else { 
            metSecondSEQ = 1;
            getline(idataf, line);
          }
        } else if ( metSecondSEQ)
          sequence2+=line;
        else sequence1+=line;
      }
      if (sequence.find('-')>=0) sequence=sequence1;
      else sequence=sequence2;

      cerr << "Refining: "<< sequence << endl;
      // set up the allowed segments and bond-lengths for ConformationOptimizer
      int seg_start=-1, seg_end=-1;
      for (int i=0; i<(int)g_backbone.size(); i++){
        if (sequence[i]=='-'){
          conformationOptimizer->presetBondLengths[i]=-1; // no preset bond length, using 3.8
          if (seg_start==-1 && seg_end==-1)
            seg_start=i;
          if (i==(int)g_backbone.size()-1){
            seg_end=i;
            cerr << "last segment:" << seg_start<<"-"<<seg_end<<endl;
            conformationOptimizer->refine_segments.push_back(make_pair(seg_start,seg_end));
          }
        } else if (seg_end==-1 && seg_start>=0){
          seg_end=i-1;
          cerr << "insert segment:" << seg_start<<"-"<<seg_end<<endl;
          conformationOptimizer->refine_segments.push_back(make_pair(seg_start,seg_end));
          seg_start=seg_end=-1;
        }
        //cerr << conformationOptimizer->presetBondLengths[i] << ",";
      }
*/
    }
    int nTotal=2;
    if (merge_pdb != ""){
      string mergePdb = "m_" + merge_pdb;
      ((ThetaTauOptimizer*)conformationOptimizer)->mergeDecoy(
                                  mergePdb, g_angle, bondlenList, initCAs);
      nTotal=0;
      return 0;
    }
    if ((int)conformationOptimizer->refine_segments.size()>0)
      nTotal=20;
    cerr << "Generating " << nTotal << " decoys." << endl;
    for (int i=0; i<nTotal; i++) {
      //optimize the conformation
      if (algorithm==ALG_GREEDY)
        conformationOptimizer->GreedySearch(outdir);
      else if (algorithm==ALG_MCMC)
        conformationOptimizer->MCMCSearch(outdir, bUseSimulatedAnnealing);
      else if (algorithm==ALG_REMC){
        conformationOptimizer->ReplicaExchange(outdir);
        break;
      }
    }
  }
///////////////Feng Test //////////////////

  //free memory
  if (conformationOptimizer) delete conformationOptimizer;
  //if (scoringFunction) delete scoringFunction;
  if (structureBuilder) delete structureBuilder;
  if (config) delete config;
  return 0;
}
