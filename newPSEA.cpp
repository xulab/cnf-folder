/* -------------------------------------------------------------------- */
/* p-sea.c								*/
/*									*/
/* Assignations des structures secondaires par analyses des angles et	*/
/* distances entre Ca							*/
/*									*/
/* Calcul des angles et liaisons a l'oeuvre dans les proteines		*/
/*									*/
/* calcul des angles phi, psi, omega 					*/
/* phi = (C-1,N,CA,C)							*/
/* psi = (N,CA,C,N+1)							*/
/* omega = (CA,C,N+1,CA+1)						*/
/*									*/
/* calcul de chi1, chi2, chi3 d'une entree de la pdb			*/
/*									*/
/* calcul de alpha, tau et r12						*/
/* (Levitt J. Mol. Biol. 1976 ,vol. 104, 59-107)			*/
/*									*/
/* ici les angles sont defini comme suit : 				*/
/* alpha = (CA-1,CA,CA+1,CA+2)						*/
/* tau = (CA_CA-1,CA_CA+1)						*/
/* beta = (CB-1,CB,CB+1,CB+2)						*/
/* nu = (CB-1,CB,CB+1)							*/
/*									*/
/* calcul de la distance (r13) entre le CA+1 et le CA-1 pour un residu	*/
/*									*/
/* les angles sont donnes entre -180 et 180 degres			*/
/* si un angle ne peut etre determine pour une raison			*/
/* ou une autre dans un residu, il prend la valeur			*/
/* 500 (NO_ANGLE) de meme pour la distance				*/
/*									*/
/* -------------------------------------------------------------------- */

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>


char THREE[26][ 3] =
        {{'G','L','Y'},
         {'A','L','A'},
        {'L','E','U'},  {'V','A','L'},  {'P','H','E'},  {'I','L','E'},
        {'P','R','O'},  {'H','Y','P'},
        {'A','S','P'},  {'G','L','U'},
        {'L','Y','S'},  {'A','R','G'},
        {'S','E','R'},  {'T','H','R'},
        {'H','I','S'},
        {'C','Y','S'},  {'S','E','C'},
        {'A','S','N'},  {'G','L','N'},
        {'T','Y','R'},  {'M','E','T'},  {'M','S','E'},  {'T','R','P'},
        {'A','S','X'},  {'G','L','X'},
        {'U','N','K'},
        };
char ONE[26] =  {
        'G',    'A',
        'L',    'V',    'F',    'I',
        'P',    'P',
        'D',    'E',
        'K',    'R',
        'S',    'T',
        'H',
        'C',    'C',
        'N',    'Q',
        'Y',    'M',    'M',    'W',
        'B',    'Z',
        'X'
        };

#include "newPSEA.h"
//#include "p-sea.h"
/* -------------------- */
/* CONSTANTES		*/
/* -------------------- */
# define LINE_LENGTH	128	/* length max of a line in pdb file	*/

#define PI		3.141592645	/* acos(-1) */
#define D2R		0.017453293	/* PI/180. */
#define R2D		1./D2R		/* 180./PI */
#define DIST_MAX_CA	20.0

#define POURCENT	'%'

#define OK 1
#define YES 1
#define NO 0

#define HAS_NO_CA_ATOM		'A'
#define HAS_NO_CB_ATOM		'B'
#define HAS_NO_N_ATOM		'N'
#define HAS_NO_CO_ATOM		'C'
#define HAS_NO_CG_ATOM		'G'
#define HAS_NO_CD_ATOM		'D'
#define HAS_NO_CE_ATOM		'E'
#define AT_START_OF_CHAIN	'S'
#define AT_END_OF_CHAIN		'F'

#define alfa  	'H'
#define	beta 	'E'
#define coil  	'C'
#define sheet  	'S'

#define N_AA 26
#define N_ST 4
#define N_K 8

#define MLL 80				/* longueur ligne d'impression */
#define FNL 80				/* longueur ligne d'impression */
/* -------------------- */
/* variables		*/
/* -------------------- */
int 		
   		comp_cons = 0,
   		comp_total = 0,
		nb_res_tot = 0;

   float	dca_max = DIST_MAX_CA;

   char		fica[FNL],
		filout[FNL],
		pdbnam[FNL],
		filnam[FNL],
		getfil[FNL];

int 	HYDRE[26] = {0,0,1,0,0,1,0,0,1,0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,1,0};
int	TOT_ST[N_AA][N_ST];
/* -------------------- */
/*internal prototypes	*/
/* -------------------- */
/* -------------------------------------------------------------------- */

static char *sResiduNames        = "/UNK/ALA/CYS/CSE/MSE/HYP/ASP/GLU/PHE/GLY/HIS/ILE/LYS/LEU/MET\
					/ASN/PRO/GLN/ARG/SER/THR/VAL/TRP/TYR/";
static char *sResiduNoChi1       = "/GLY/ALA/";
static char *sResiduNoChi2       = "/CYS/CSE/SER/THR/VAL/";
static char *sResiduNoChi3       = "/ILE/LEU/ASP/ASN/";

/* -------------------------------------------------------------------- */
/* macros								*/
/* -------------------------------------------------------------------- */
#ifndef NEW
#define NEW(obj)		(obj *)malloc(sizeof(obj))
#define NEWN(obj, dim)		(obj *)malloc(dim * sizeof(obj))
#endif
#define TV(var, lim, flo)       (var < (lim + flo) && var > (lim - flo))
#define TNT(var, li, ls)       (var < ls && var > li)
#define MAX(a,b)                ((a > b)? a : b)
#define PM(a, b, c)             sqrt(a*a + b*b + c*c)

#define GET_DIST(O1, O2) (float)(sqrt((double)((O1->x - O2->x)*(O1->x - O2->x) + \
				(O1->y - O2->y)*(O1->y - O2->y) + \
				 (O1->z - O2->z)*(O1->z - O2->z))))

/* -------------------------------------------------------------------- */
/* prototypes   internes       						*/
/* -------------------------------------------------------------------- */
static	ResAtom	*resatom_cpy(ResAtom *dst, ResAtom *src);
static	ResAtom	*atom_resatom_cpy(ResAtom *dst, PSEAAtom *src);
static	ResAtom	*get_named_atom(char *name, Residu *r, ResAtom *dst);
static	double  resatom_distca( ResAtom *at);
static	void	angle_error(char *nam1, int num1, char *spos,
		   char *nam2, int num2, char smiss, int subroutine);
static	double	atom_dihed(ResAtom *ra);
static	double	atom_angle(ResAtom *ra);
static	int	compute_alpha_tau(Residu  *r, ResAtom *ra,
		int do_tau, int do_alpha);
static	void	myInfoOnStderr(char *info);
static ResAtom *atom_pos(ResAtom *ra, float dist, float angl, float dihd);
	double 	get_distNC( Residu *r);
	void	anal_cons234(Residu *res);
	void	assign_def(Residu *fir_res);
	void	libere(Residu *fir_res);
/*--------------------------------------------------------------*/
/* analyse							*/
/*--------------------------------------------------------------*/
void analyse(Residu *first_res)
{
   Residu	*res;
   float	dihint = 50.0,
		angint = 108.0,
		disint = 3.81;
   int		limite = 500, n;
	ResAtom *ra = NEW(ResAtom);

	for (res = first_res->next, nb_res_tot = 0 ; res ; res = res->next, nb_res_tot++) {
	   res->cons = 'C';
           get_alpha_tau(res);
           get_distca_1_2(res);
           get_distca_1_5(res);	/* contient get_distca_1_3() et get_distca_1_4()*/
	}
	assign_def(first_res);

        for(res = first_res->next, nb_res_tot = 0 ; res ; res = res->next, nb_res_tot++)
	{
	   cout<<res->cons;
	}
	cout<<endl;
}
/*--------------------------------------------------------------*/
/* main program							*/
/*--------------------------------------------------------------*/
/*
main(int argc, char **argv)
{
   char		str[256],
		*ptr;
   Residu	*cur_res,
		*first_res;
   int		i, index = 0, dec = 0,  j, argv2lu = 0,
		hit = 0,
		limite = 500,
		carg,
		errflag = 0;
   extern	char *optarg;
   extern	int optind, opterr;
   char		*map_ca,
		*map,
		line[MLL];

    SimPDB* pdb=new SimPDB(argv[1]);
        first_res=buildResidue(pdb);		
	
	first_res->chain = first_res->next->chain; 
        analyse(first_res);		

	libere(first_res);	

}
*/
/****************************************************************************************/
/*	test d'assignement 								*/
/****************************************************************************************/
void	libere(Residu *first_res)
{
	Residu 	*res, *r;
	
  	for (res = first_res->next; res ;) {
		r = res;
		free(res);
		res = r->next;
	}
}
/****************************************************************************************/
/*	test d'assignement 								*/
/****************************************************************************************/
void	assign_def(Residu *first_res)
{
	int	n = 0;
	Residu 	*r = NULL, *res = NULL;
	ResAtom at[2];

	anal_cons234(first_res);
}
/****************************************************************************************/
/*	test d'assignement par distance							*/
/****************************************************************************************/
int	test_consa(Residu *res)
{

	return ( (TV(res->alpha, 50., 25.) && TNT(res->tau, 79, 100.))
		||  (TV(res->alpha, 51., 13.) && TNT(res->tau, 77, 106.))
		|| (TNT(res->dis14, 4.75, 5.55) && TNT(res->dis15, 5.75, 6.45))
		);

}
/****************************************************************************************/
/*	test d'assignement 								*/
/****************************************************************************************/
int	test_consb(Residu *res)
{

	/*return ( 
		((TNT(res->alpha, -180.0, -160.) || TNT(res->alpha, 165., 180.0) )
		&& TV(res->tau, 125., 20.))
		||
		((TNT(res->alpha, -180.0, -110.) || TNT(res->alpha, 140., 180.0) )
		&& TV(res->tau, 135., 12.))
		|| (TV(res->dis14, 9.95, 0.95) && TV(res->dis13, 6.75, 0.75)));*/
	return ( 
		((TNT(res->alpha, -180.0, -150.) || TNT(res->alpha, 165., 180.0) )
		&& TV(res->tau, 125., 20.))
		||
		((TNT(res->alpha, -180.0, -110.) || TNT(res->alpha, 140., 180.0) )
		&& TV(res->tau, 132., 12.))
		|| (TV(res->dis14, 9.95, 0.95) && 
			TV(res->dis13, 6.75, 0.75)
			/*TNT(res->dis15, 11.3, 13.6)*/
			));

}
/****************************************************************************************/
/*	detection de feuillet		 						*/
/****************************************************************************************/
int	test_feuillet(Residu *res, int n, Residu *first)
{
	int 	SBETA = 0;
	Residu 	*r = NULL, *rer = NULL;
	ResAtom at[2];

	SBETA = 0;
	for (rer = res; n > 0; rer = rer->next, n--){
		if (!get_named_atom("CA", rer, at+1))
			continue;
      		for (r = first->next; r && (r != res || r != res->prev || r != res->prev->prev); r = r->next) {
			if (!get_named_atom("CA", r, at) || 
					!(TNT(r->tau, 117., 145.) || test_consa(r)))
				continue;
			if (TNT(resatom_distca(at), 4.25, 5.25))
				SBETA++;
		}
		if (!r || !r->next || !r->next->next || !rer->next->next->next)
			continue;
      		for (r = rer->next->next->next; r; r = r->next) {
			if (!get_named_atom("CA", r, at) 
					|| !(TNT(r->tau, 117., 145.) || test_consa(r)))
				continue;
			if (TNT(resatom_distca(at), 4.25, 5.25))
				SBETA++;
		}
		/*fprintf(stderr,"SBETC:%3d", SBETA);*/
	}
	/*fprintf(stderr,"\n");*/
	return (SBETA > 4);
}
/****************************************************************************************/
/*	assignement 									*/
/****************************************************************************************/
void   anal_cons234(Residu *first_res)
{
	char	bulge = 'u', turn = 't', omega = 'w';
	float	aai = 79., aas = 99., dof = 0., doi = 0.0, dol = 0.0;
	int	n = 0, SBETA = 0, p = 0;
	Residu *r = NULL, *res = NULL, *rer = NULL;
	ResAtom at[2];

  	for (res = first_res->next; res; res = res->next) {		
	   if (test_consa(res)) {
	     	for (r = res->next, n = 2; r; r = r->next, n++) 
			if (!test_consa(r))
				break;
		if (r)
			if (TNT(r->dis14, 4.80, 5.45) && TNT(r->tau, aai, aas)){
				n++;
			}
		if (n > 4 /*|| (n==4 && TNT(r->tau, aai, aas))*/) {
			if (TNT(res->prev->dis14, 4.75, 5.45) && TNT(r->prev->tau, aai, aas)){
				res->prev->cons = alfa;
			}
	     		for (;res && n > 0; res = res->next, n--) {
				res->cons = alfa;
			}
			if (res && (TNT(res->dis14, 4.75, 5.45) && TNT(res->tau, aai, aas))){
				res->cons = alfa;
			}
		}
	   }
	}
  	for (res = first_res->next; res; res = res->next) {
		if (res->cons == alfa)
			continue;
	      if (test_consb(res)) {
		for (r = res->next, n = 1; r; r = r->next, n++) 
			 if (!test_consb(r) || r->cons == alfa)
				break;
		if (r && r->cons != alfa)
			if (TNT(r->dis13, 6.05, 7.10) || TNT(r->tau, 118., 145.)){
				n++;
			}
		if (n > 3 || (n == 3 && test_feuillet(res, n, first_res))) {
			if ( TNT(res->prev->dis13, 6.35, 7.35)/* ||  TV(res->tau, 133., 16.)*/)
				res->prev->cons = beta;
	     		for (; res && n > 0; res = res->next, n--) {
				res->cons = beta;
			}
			if (TNT(res->dis13, 6.35, 7.30) || TNT(res->tau, 118., 145.)){
				res->cons = beta;
                        }
		}
	      }
  	}
/*-------------------------------------------------------------------------------------*/
}
/* -------------------------------------------------------------------- */
/* gestion (simplifiee) des erreurs					*/
/* imprime un message et arrete le massacre				*/
/* -------------------------------------------------------------------- */
void myerror(char *msg, int status)
{
	fprintf(stderr, " *error* %s\n", msg);
	if (status) exit(status);
}

/* -------------------------------------------------------------------- */
/* gestion (simplifiee) des informations (pas vraiment erreurs)		*/
/* imprime un message sur stderr					*/
/* -------------------------------------------------------------------- */
static void myInfoOnStderr(char *info)
{
	fprintf(stderr,"%s\n", info);
}

/* -------------------------------------------------------------------- */
/* creation d'un residu							*/
/* -------------------------------------------------------------------- */
Residu *new_residu(void)
{
	Residu *r;

	if (! (r = NEW(Residu))) {
	   myerror("new_residu:: not enough memory", 0);
	   return NULL;
	}

	memset(r, 0, sizeof(Residu));

	r->nb_neighbours = 0;

	return r;
}

/* -------------------------------------------------------------------- */
/* copy resatom -> resatom						*/
/* -------------------------------------------------------------------- */
static ResAtom *resatom_cpy(ResAtom *dst, ResAtom *src)
{
	strcpy(dst->name, src->name);
	dst->x =  src->x;
	dst->y =  src->y;
	dst->z =  src->z;
	
	return dst;
}

/* -------------------------------------------------------------------- */
/* copy atom -> resatom							*/
/* -------------------------------------------------------------------- */
static ResAtom *atom_resatom_cpy(ResAtom *dst, PSEAAtom *src)
{
	strcpy(dst->name, src->name);
	dst->x =  src->x;
	dst->y =  src->y;
	dst->z =  src->z;
	
	return dst;
}

/*------------------------------------------------------------------------------*/
/* compute the distance between CA in Resatom at[0] and at[1]			*/
/*------------------------------------------------------------------------------*/
static double   resatom_distca( ResAtom *at)
{
	return( sqrt(   ((at+1)->x - at->x)*((at+1)->x - at->x) +
			((at+1)->y - at->y)*((at+1)->y - at->y) +
			((at+1)->z - at->z)*((at+1)->z - at->z)));
}

/* -------------------------------------------------------------------- */
/* adds an atom in residue (returns the associated, new 		*/
/* resatom)								*/
/* returns the Resatom (or NULL if it can't be allocated )		*/ 
/* -------------------------------------------------------------------- */
ResAtom *put_atom_in_res(PSEAAtom *atom, Residu *res)
{
	ResAtom *ra = NULL;

	if (! (ra = NEW(ResAtom)))
	
	   myerror("put_atom_in_res:: not enough memory", 0);

	else {
	
	   atom_resatom_cpy(ra, atom);

	   ra->next = NULL;

	   if (! res->first_atom)
	      res->first_atom = ra;
	   else
	      res->last_atom->next = ra;

	   res->last_atom  = ra;
	}

	return ra;
}

/* -------------------------------------------------------------------- */
/* maintains the list of the neighbours of a residue			*/
/* returns the number of neighbours or -1 if too much neighbours	*/
/* -------------------------------------------------------------------- */
int set_residue_neighbours(Residu *res, Residu *first)
{	
	Residu	*r;

	res->nb_neighbours = 0;

	for (r = first; r; r = r->next) {
	   if (r == res)
	      continue;
	   /* d = GET_DIST(r, res); i*//* debug */
	   if (GET_DIST(r, res) <= DIST_MAX_BETWEEN_NEIGHBOUR_RESIDUE) {
	       if (++res->nb_neighbours >= MAX_NEIGHBOURS ) {
		   myerror("set_residue_neighbours:: trop de voisins", 4);
		   return -1;
	       }
	       else
		   res->neighbour[res->nb_neighbours-1] = r;
	   }
	}
	
	return res->nb_neighbours;
}

/* -------------------------------------------------------------------- */
/* find the geometric center of atoms in a residue			*/
/* and set the x, y, z field of the Residu structure			*/
/* return the number of atom found					*/
/* -------------------------------------------------------------------- */
int      set_residue_geocenter(Residu *r)
{
	ResAtom *a;
	int	n = 0;
	float	sx = 0.0, sy = 0.0, sz = 0.0, inv;

	for (a = r->first_atom; a != NULL; a = a->next) {
	   sx += a->x;
	   sy += a->y;
	   sz += a->z;
	   n++;
	}

	if (n > 0) {
		inv = 1./n;
	   r->x = sx*inv;
	   r->y = sy*inv;
	   r->z = sz*inv;
	}
	else
	   r->x = r->y = r->z = 0.;
	
	return n;
}

/* -------------------------------------------------------------------- */
/* get the maximum distance of an atom from the				*/
/* geometric center of the residue					*/
/* -------------------------------------------------------------------- */
float get_dist_max_from_geocenter(Residu *r)
{
	float d, dmax = -1.0;
	ResAtom	*a;

	for (a = r->first_atom; a != NULL; a = a->next) {
	   d = GET_DIST(r, a);
	   if (d > dmax) {
		dmax = d;
	   }
	}

	return dmax;
}
/* -------------------------------------------------------------------- */
/* extract atom (by name) from residu					*/
/* if found, copy atom into dst and returns atom			*/
/* else, returns NULL							*/
/* -------------------------------------------------------------------- */
static ResAtom *get_named_atom(char *name, Residu *r, ResAtom *dst)
{
	ResAtom *atm;

	for (atm = r->first_atom ; atm ; atm = atm->next)
	  if (! strcmp(name, atm->name)) {
		resatom_cpy(dst, atm);
		return atm;
	  }

	return NULL;

}
/*------------------------------------------------------------------------------*/
/* compute the distance between CA-1 and CA+3					*/
/*------------------------------------------------------------------------------*/
int  get_distca_1_5( Residu *r)
{
	ResAtom at[2];
	char	strerr[256];
						/* respectively: at[0] -> CA-1	*/
						/*               at[1] -> CA+3	*/
	r->dis14 = NO_ANGLE;
	r->dis15 = NO_ANGLE;

#define ERRORDIST(X,NX,S,Y,NY,CAR)	{ r->dis15 = NO_ANGLE;			\
					angle_error(X,NX,S,Y,NY,CAR,4);		\
					return 1; }

	if ( (! r->prev) || (r->num != r->prev->num + 1))
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_START_OF_CHAIN)

						/* find CA atom of prev residue	*/
	else if ( ! get_named_atom( "CA", r->prev, at)) /* n-1 */
	   ERRORDIST(r->prev->name, r->prev->num, "preceding", r->name, r->num, HAS_NO_CA_ATOM)

	if (( ! r->next) || (r->num != r->next->num - 1) )
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)

						/* find CA atom of next residue */
	else if ( ! get_named_atom( "CA", r->next, at+1))
	   ERRORDIST(r->next->name, r->next->num, "following", r->name, r->num, HAS_NO_CA_ATOM)

	r->dis13 = (float)resatom_distca(at); /* entre r->prev et r->next */

	if ( !r->next->next || (r->num != r->next->next->num - 2))
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)

	else if ( ! get_named_atom( "CA", r->next->next, at+1))	/* n+2 */
	   ERRORDIST(r->next->next->name, r->next->next->num, "following", r->name, r->num, HAS_NO_CA_ATOM)

	r->dis14 = (float)resatom_distca(at); /* entre r->prev et r->next->next */
									/* find CA atom of next residue */
	if ( !r->next->next->next || r->num != (r->next->next->next->num - 3) )
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)

	else if ( ! get_named_atom( "CA", r->next->next->next, at+1))	/* n+3 */
	   ERRORDIST(r->next->next->next->name, r->next->next->next->num, "following", r->name, r->num,
	   							HAS_NO_CA_ATOM)

	r->dis15 = (float)resatom_distca(at); /* entre r->prev et r->next->next->next */

	return 0;

#undef ERRORDIST

}
/*------------------------------------------------------------------------------*/
/* compute the distance between CA-1 and CA+2					*/
/*------------------------------------------------------------------------------*/
int  get_distca_1_4( Residu *r)
{
	ResAtom at[2];
	char	strerr[256];
						/* respectively: at[0] -> CA-1	*/
						/*               at[1] -> CA+2	*/
	r->dis14 = NO_ANGLE;

#define ERRORDIST(X,NX,S,Y,NY,CAR)	{ r->dis14 = NO_ANGLE;			\
					angle_error(X,NX,S,Y,NY,CAR,4);		\
					return 1; }

	if ( (! r->prev) || (r->num != r->prev->num + 1))

	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_START_OF_CHAIN)

						/* find CA atom of prev residue	*/
	else if ( ! get_named_atom( "CA", r->prev, at))

	   ERRORDIST(r->prev->name, r->prev->num, "preceding", r->name, r->num,
	   							HAS_NO_CA_ATOM)

	if (( ! r->next) || ( ! r->next->next) || (r->num != r->next->next->num - 2) )

	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)

						/* find CA atom of next residue */
	else if ( ! get_named_atom( "CA", r->next->next, at+1))

	   ERRORDIST(r->next->next->name, r->next->next->num, "following", r->name, r->num,
	   							HAS_NO_CA_ATOM)

	r->dis14 = (float)resatom_distca(at);

	return 0;

#undef ERRORDIST

}
/*------------------------------------------------------------------------------*/
/* compute the peptide bond length if N et C available				*/
/*------------------------------------------------------------------------------*/
double get_distNC( Residu *r)
{
	float	dis = 0.;
	ResAtom at[2];
						/* respectively: at[0] -> C atome i	*/
						/*               at[1] -> N atome i+1	*/

#define ERRORDIST(X,NX,S,Y,NY,CAR)		{ r->dis12 = NO_ANGLE;		\
						angle_error(X,NX,S,Y,NY,CAR,3);	\
						return 1; }

	if (! r) {
	   return 0.;
	}
	/*else if ( ! get_named_atom( "C", r, at))
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, HAS_NO_CA_ATOM)

	if ( ! r->next) 
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)
	else if ( ! get_named_atom( "N", r->next, at+1))
	   ERRORDIST(r->next->name, r->next->num, "following", r->name, r->num,
	   						HAS_NO_CA_ATOM)*/
	 if (! r || ! get_named_atom( "C", r, at))
		return 0;
	if ( ! r->next || ! get_named_atom( "N", r->next, at+1))
		return 0;

	dis = resatom_distca(at);

	return dis;

#undef ERRORDIST
}
/*------------------------------------------------------------------------------*/
/* compute the distance between CA and CA+1					*/
/*------------------------------------------------------------------------------*/
int get_distca_1_2( Residu *r)
{
	ResAtom at[2];
						/* respectively: at[0] -> CA	*/
						/*               at[1] -> CA+1	*/

#define ERRORDIST(X,NX,S,Y,NY,CAR)		{ r->dis12 = NO_ANGLE;		\
						angle_error(X,NX,S,Y,NY,CAR,3);	\
						return 1; }
	r->dis12 = 0.0;
			r->ca = 1;

	if (! r) {
	   myerror("get_distca_1_2:: residue does not exist",0);
	   return 1;
	}
					/* find CA atom of current residue */
	else 	if ( ! get_named_atom( "CA", r, at)){
			r->ca = -1;
	   		ERRORDIST(r->name, r->num, NULL, NULL, 0, HAS_NO_CA_ATOM)
		}
	if (( ! r->next) || (r->num != r->next->num - 1) )
	   ERRORDIST(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN)
					/* find CA atom of next residue */
	else if ( ! get_named_atom( "CA", r->next, at+1))
	   ERRORDIST(r->next->name, r->next->num, "following", r->name, r->num,
	   						HAS_NO_CA_ATOM)

	r->dis12 = (float)resatom_distca(at);	/* entre r et r->next */

	return 0;

#undef ERRORDIST
}

/* -------------------------------------------------------------------- */
/* put a warning message when an angle cannot be			*/
/* computed (somewhat verbose ?)					*/
/* -------------------------------------------------------------------- */
static void angle_error(char *nam1, int num1, char *spos,
		char *nam2, int num2, char smiss, int subroutine)
{
	char subroutine_name[20], smissmsg[50], strerr[256];

#define SUBCPY(X)	strcpy(subroutine_name, X); break

	switch(subroutine) {

	   case 0: SUBCPY("phi_psi   ");

	   case 1: SUBCPY("chi       ");

	   case 2: SUBCPY("alpha_tau ");

	   case 3: SUBCPY("distca_1_2");

	   case 4: SUBCPY("distca_1_3");

	}

#undef SUBCPY

#define SMISSCPY(X)	strcpy(smissmsg, X); break

	switch(smiss) {

/*	   case HAS_NO_CA_ATOM: SMISSCPY("has no CA atom");

	   case HAS_NO_CB_ATOM:SMISSCPY("has no CB atom");

	   case HAS_NO_N_ATOM: SMISSCPY("has no N atom");

	   case HAS_NO_CO_ATOM: SMISSCPY("has no CO atom");

	   case HAS_NO_CG_ATOM: SMISSCPY("has no CG(1), OG(1) or SG atom");

	   case HAS_NO_CD_ATOM: SMISSCPY("has no CD(1), OD(1), SD or ND(1) atom");

	   case HAS_NO_CE_ATOM: SMISSCPY("has no CE, CE(1), OE(1), NE or NE(1) atom");

	   case AT_START_OF_CHAIN: SMISSCPY("at start of chain");

	   case AT_END_OF_CHAIN: SMISSCPY("at end of chain");*/

	   case HAS_NO_CA_ATOM: SMISSCPY("pas de CA");

	   case HAS_NO_CB_ATOM:SMISSCPY("pas de CB");

	   case HAS_NO_N_ATOM: SMISSCPY("pas de N");

	   case HAS_NO_CO_ATOM: SMISSCPY("pas de CO");

	   case HAS_NO_CG_ATOM: SMISSCPY("pas de CG(1), OG(1) ou SG");

	   case HAS_NO_CD_ATOM: SMISSCPY("pas de CD(1), OD(1), SD ou ND(1)");

	   case HAS_NO_CE_ATOM: SMISSCPY("pas de CE, CE(1), OE(1), NE ou NE(1)");

	   case AT_START_OF_CHAIN: SMISSCPY("debut de chaine");

	   case AT_END_OF_CHAIN: SMISSCPY("fin de chaine");

	}

#undef SMISSCPY

	if (nam2) {

	   sprintf(strerr, "%s :: residu %s %4d %s residu %s %4d %s",
		subroutine_name, nam1, num1, spos,nam2, num2, smissmsg);

	   myInfoOnStderr(strerr);

	}

	else {

	   sprintf(strerr, "%s :: residu %s %4d %s",
		subroutine_name, nam1, num1, smissmsg);

	   myInfoOnStderr(strerr);

	}

}
/*----------------------------------------------------------------------*/
/* rebuild from internal coordinates					*/
/*----------------------------------------------------------------------*/
static ResAtom *atom_pos(ResAtom *ra, float dist, float angl, float dihd)
{
	double	xn,yn,zn,ax,ay,az,bx,by,bz,cx,cy,cz,a,b,c,pm;
	char	got[10];

	angl = angl * D2R;
	dihd = dihd * D2R;

	xn = dist*cos(angl);
	yn = dist*sin(angl)*cos(dihd);
	zn = -dist*sin(angl)*sin(dihd);

	ax = (ra+1)->x - (ra+2)->x;			/* calcul des composantes vectorielles	*/
	bx = (ra+1)->y - (ra+2)->y;
	cx = (ra+1)->z - (ra+2)->z;

	pm = sqrt(ax*ax + bx*bx + cx*cx);
	xn = xn/pm;

	a = (ra)->x - (ra+1)->x;
	b = (ra)->y - (ra+1)->y;
	c = (ra)->z - (ra+1)->z;

	az = bx*c - cx*b;
	bz = cx*a - ax*c;
	cz = ax*b - bx*a;

	pm = sqrt(az*az + bz*bz + cz*cz);
	zn = zn/pm;

	ay = bz*cx - cz*bx;
	by = cz*ax - az*cx;
	cy = az*bx - bz*ax;

	pm = sqrt(ay*ay + by*by + cy*cy);
	yn = yn/pm;

	(ra+3)->x = ax*xn+ay*yn+az*zn + (ra+2)->x;
	(ra+3)->y = bx*xn+by*yn+bz*zn + (ra+2)->y;
	(ra+3)->z = cx*xn+cy*yn+cz*zn + (ra+2)->z;

return ra;
}

/* -------------------------------------------------------------------- */
/* computes the angle defined by the 3 points				*/
/* given in ra. returns angle in [-180, 180] deg.			*/
/* -------------------------------------------------------------------- */
static double atom_angle(ResAtom *ra)
{
	double	xa, ya, za, xb, yb, zb,
		xc, yc, zc, np1np2, ang;

				/* calcul des composantes vectorielles	*/

	xa = ra->x - (ra+1)->x;
	ya = ra->y - (ra+1)->y;
	za = ra->z - (ra+1)->z;

	xb = (ra+2)->x - (ra+1)->x;
	yb = (ra+2)->y - (ra+1)->y;
	zb = (ra+2)->z - (ra+1)->z;


				/* (produit des normes)			*/

	np1np2 = sqrt((xa*xa+ya*ya+za*za) * (xb*xb+yb*yb+zb*zb));

	if (np1np2 == 0) 
		return NO_ANGLE;

				/* produit vectoriel 1*2		*/

	xc =   ya*zb - yb*za;
	yc = - xa*zb + xb*za;
	zc =   xa*yb - xb*ya;
				/* arcsinus de norme du produit vect.	*/

	ang = asin(sqrt(xc*xc+yc*yc+zc*zc)/np1np2);

				/* ang is between -Pi/2 and Pi/2	*/
				/* we need to know the sign		*/
				/* of cosinus (produit scalaire)	*/
	
	if ((xa*xb+ya*yb+za*zb) <  0)			/* cos is < 0	*/
		ang = PI - ang;


	return ang * R2D;				/* rad -> deg	*/

}

/* -------------------------------------------------------------------- */
/* computes the dihedral angle defined by the 4 points			*/
/* given in ra. returns angle in [-180, 180] deg.			*/
/* -------------------------------------------------------------------- */
static double atom_dihed(ResAtom *ra)
{
	double	xa, ya, za, px1, py1, pz1,
		xb, yb, zb, px2, py2, pz2,
		xc, yc, zc, px3, py3, pz3,
		np1np2, dihed;

				/* calcul des composantes vectorielles	*/

	xa = (ra+1)->x - ra->x;
	ya = (ra+1)->y - ra->y;
	za = (ra+1)->z - ra->z;

	xb = (ra+2)->x - (ra+1)->x;
	yb = (ra+2)->y - (ra+1)->y;
	zb = (ra+2)->z - (ra+1)->z;

	xc = (ra+3)->x - (ra+2)->x;
	yc = (ra+3)->y - (ra+2)->y;
	zc = (ra+3)->z - (ra+2)->z;

					/* 1er produit vectoriel	*/

	px1 =   ya*zb - yb*za;
	py1 = - xa*zb + xb*za;
	pz1 =   xa*yb - xb*ya;

					/* 2eme produit vectoriel	*/

	px2 =   yb*zc - yc*zb;
	py2 = - xb*zc + xc*zb;
	pz2 =   xb*yc - xc*yb;

				/* (produit des normes)			*/

	np1np2 = sqrt((px1*px1+py1*py1+pz1*pz1) * (px2*px2+py2*py2+pz2*pz2));

	if (np1np2 == 0) 
		return NO_ANGLE;

				/* arccosinus du produit scalaire	*/
				/* des produits vectoriels 		*/


	dihed = acos((px1*px2+py1*py2+pz1*pz2)/np1np2);

				/* dihed is between 0 and Pi		*/
				/* we need the sign			*/
				/* produit vectoriel des produits vect.	*/

	px3 =   py1*pz2 - py2*pz1;
	py3 = - px1*pz2 + px2*pz1;
	pz3 =   px1*py2 - px2*py1;
	
				/* produit scalaire du 2eme vect	*/
				/* avec prod. vect. des prod. vect.	*/
	
	if ((px3*xb + py3*yb + pz3*zb) <  0)		/* sine is < 0	*/
		dihed = -dihed;


	return dihed * R2D;			/* rad -> deg	*/

}


/* -------------------------------------------------------------------- */
/* check  phi, psi and omega computability, compute 			*/
/* them (put result in r) and return 1 if all angles			*/
/* are defined								*/
/* -------------------------------------------------------------------- */
static int compute_alpha_tau(Residu  *r, ResAtom *ra,
		int do_tau, int do_alpha)
{
	r->tau   = (do_tau   ? atom_angle(ra)  : NO_ANGLE);
	r->alpha = (do_alpha ? atom_dihed(ra)  : NO_ANGLE);

	return (    (r->tau != NO_ANGLE)
		 && (r->alpha != NO_ANGLE));
}

/* -------------------------------------------------------------------- */
/* computes the tau angle between CA-1,CA and CA+1			*/
/* and computes the alpha angle between CA-1,CA, CA+1 and CA+2		*/
/* returns what compute_alpha_tau returns				*/
/* find an angle : 1, cannot find an angle : 0				*/
/* -------------------------------------------------------------------- */
int	get_alpha_tau(Residu *r)
{
	int	dtau, dalpha;		/* computability flags		*/	
	ResAtom ra[4];			/* ra[0] -> CA-1		*/
					/* ra[1] -> CA  		*/
					/* ra[2] -> CA+1  		*/
					/* ra[3] -> CA+2  		*/

#define CHECK_ANGLES 	if (! (dtau || dalpha )) \
			   return compute_alpha_tau(r, ra, 0, 0)

	dtau = dalpha = 1;		/* assume all is ok		*/

	/* ------------------------------------------------------------ */
	/* verify that all residues exist				*/
	/* ------------------------------------------------------------ */
	if ( ! r) {
	   myerror("get_alpha_tau:: residue does not exist",0);
	   return 0;
	}
	else if ( ! (r->prev && (r->prev->num == r->num-1))) {
	   angle_error(r->name, r->num, NULL, NULL, 0, AT_START_OF_CHAIN, 2);
	   dtau = dalpha = 0;
	}
	else if ( ! (r->next && (r->next->num == r->num+1))) {
	   angle_error(r->name, r->num, NULL, NULL, 0, AT_END_OF_CHAIN, 2);
	   dtau = dalpha = 0;
	}
	else if(! ((r->next->next) &&
			(r->next->next->num == r->num+2))) {
	   angle_error(r->next->name, r->next->num, NULL, NULL, 0,
	   					AT_END_OF_CHAIN, 2);
	   dalpha = 0;
	}

	CHECK_ANGLES;

	/* here dtau is 1 */

	/* ------------------------------------------------------------ */
	/* we need CA atom of current residue for both tau and alpha	*/
	/* ------------------------------------------------------------ */
	if ( ! get_named_atom( "CA", r, ra+1) ) {
	   angle_error(r->name, r->num, NULL, NULL, 0, HAS_NO_CA_ATOM, 2);
	   dtau = dalpha = 0;
	}

	CHECK_ANGLES;

	/* ------------------------------------------------------------ */
	/* we need CA atom of previous residue for both tau and alpha	*/
	/* ------------------------------------------------------------ */

	if ( ! get_named_atom( "CA", r->prev, ra)) {
	   angle_error(r->prev->name, r->prev->num, NULL, NULL, 0,
	   						HAS_NO_CA_ATOM, 2);
	   dtau = dalpha = 0;
	}

	CHECK_ANGLES;

	
	/* ------------------------------------------------------------ */
	/* we need CA atom of next residue for both tau and alpha	*/
	/* ------------------------------------------------------------ */
	if ( ! get_named_atom( "CA", r->next, ra+2)) {
	   angle_error(r->next->name, r->next->num, NULL, NULL, 0,
	   					HAS_NO_CA_ATOM, 2);
	   dtau = dalpha = 0;
	}

	CHECK_ANGLES;

	/* here dtau is 1 */

	/* ------------------------------------------------------------ */
	/* we need CA atom of next->next residue for alpha		*/
	/* ------------------------------------------------------------ */

	if (dalpha && (! get_named_atom( "CA", r->next->next, ra+3))) {
	   angle_error(r->next->next->name, r->next->next->num,
	   				NULL, NULL, 0, HAS_NO_CA_ATOM, 2);
	   dalpha = 0;
	}

	return compute_alpha_tau(r, ra, dtau, dalpha);

#undef CHECK_ANGLES

}
/* -------------------------------------------------------------------- */
/* reads residues and atoms from a pdb file return the first residue	*/
/* which is a dummy one	or NULL if fails				*/
/* -------------------------------------------------------------------- */
/*
Residu* buildResidue(SimPDB* pdb)
{
   PSEAAtom		atom;
   int		i, nb_residus_inseres = 0, total_residus_inseres = 0;
   Residu	*cur_res, *prev_res, *first_res;
   

   first_res=new_residu();
   cur_res=first_res; 
   first_res->prev  = NULL;
   first_res->num = -32000;			

   for(int i=0; i<pdb->mNumResidue; i++)
   {
      prev_res = cur_res;
      cur_res = new_residu();
      prev_res->next = cur_res;
      cur_res->prev  = prev_res;
      cur_res->num = i;
      strcpy(cur_res->name, pdb->mResName[i]);
      cur_res->pdbnum  = i;
      cur_res->chain   = 'A';
      cur_res->inser   = NO_INSER;
      cur_res->next    = NULL;
      cur_res->nb_ca	= -1;
      cur_res->code = pdb->mSeq[i];
      cur_res->nrel = i;
      cur_res->cut = cur_res->num - prev_res->num;
      //use a for loop to add more atoms here
      
      strcpy(atom.name, "CA");
      atom.x=pdb->mCAlpha[i*3];
      atom.y=pdb->mCAlpha[i*3+1];
      atom.z=pdb->mCAlpha[i*3+2];
      
      put_atom_in_res(&atom, cur_res); 
   }   
   return first_res; 
}*/
void setCoord(Residu* res, double * stru, int len)
{
   Residu *cur_res=res->next;
   for(int i=0; i<len; i++, cur_res=cur_res->next)
   {

     cur_res->first_atom->x=stru[3*i];
     cur_res->first_atom->y=stru[3*i+1];
     cur_res->first_atom->z=stru[3*i+2];	    
   }
   res->chain = res->next->chain; 
   analyse(res);
}

void setCoord(Residu* res, double ** stru, int len)
{
   Residu *cur_res=res->next;
   for(int i=0; i<len; i++, cur_res=cur_res->next)
   {

     cur_res->first_atom->x=stru[i][0];
     cur_res->first_atom->y=stru[i][1];
     cur_res->first_atom->z=stru[i][2];	    
   }
   res->chain = res->next->chain; 
   analyse(res);
}



