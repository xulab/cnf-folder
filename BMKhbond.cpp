/*******************************************************************************

								BMKhbond.cpp
								------------

  ****************************************************************************
  *										  *
  *   This library is free software; you can redistribute it and/or		  *
  *   modify it under the terms of the GNU Lesser General Public		  *
  *   License as published by the Free Software Foundation; either		  *
  *   version 2.1 of the License, or (at your option) any later version.	  *
  *										  *
  *   This library is distributed in the hope that it will be useful,		  *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of		  *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	  	  *
  *   Lesser General Public License for more details.				  *
  *										  *
  *   You should have received a copy of the GNU Lesser General Public	   	*
  *   License along with this library; if not, write to the Free Software	*
  *   Foundation, Inc.,								*
  *   59 Temple Place, Suite 330, Boston, MA  02111-1307  USA			*
  *										*
  ****************************************************************************

  Copyright (C) 2005 The University of Chicago

  Authors:
  Andres Colubri

  Description:
  PL plugin to calculate the Hydrogen Bond potential from Kortemme,
  Morozov & Baker.

*******************************************************************************/

#include <cstddef> // Needed to use NULL.
#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

#include "BMKhbond.h"
#include "bbq.h"
#include "strtokenizer.h"

/*************************** TDeltaEnergy implementations ***********************/

bool TDeltaEnergy::InRange(double delta)
{
	return (1.40 <= delta) && (delta <= 3.0);
}

double TDeltaEnergy::Energy(int int_type, double delta)
{
	if (delta <= 1.425) return values[int_type][0];
	else if (2.975 <= delta) return values[int_type][31];
	else {
		int i = idx(delta);
		double v0 = values[int_type][i];
		double v1 = values[int_type][i + 1];
		double delta0 = 1.425 + 0.05 * i;
		return LinearInterpolation(v0, v1, delta0, delta, 20);
	}
}

double TDeltaEnergy::Minimum(int int_type)
{
	double v = values[int_type][0];
	for (int i = 1; i < 32; i++)
		if (values[int_type][i] < v) v = values[int_type][i];
	return v;
}

int TDeltaEnergy::LoadFromFile(int int_type, string &fn)
{
	ifstream par_file;
	double energy;
	int n;

	par_file.open(fn.c_str());
	n = -1;
	while (true) {
		par_file >> energy;
		n++;

		if (par_file.eof()) break;

		values[int_type][n] = energy;
	}
	par_file.close();
	return 0;
}

/*************************** TChiEnergy implementations ***********************/

double TChiEnergy::Energy(int int_type, double chi)
{
	if (chi <= -177.5)
		return values[int_type][0];
	else if (177.5 <= chi) 
		return values[int_type][71];
	else {
		int i = idx(chi);
		double v0 = values[int_type][i];
		double v1 = values[int_type][i + 1];
		double chi0 = -177.5 + 5 * i;
		return LinearInterpolation(v0, v1, chi0, chi, double(0.2));
	}
}

double TChiEnergy::Minimum(int int_type)
{
	double v = values[int_type][0];
	for (int i = 1; i < 72; i++)
		if (values[int_type][i] < v) v = values[int_type][i];
	return v;
}

int TChiEnergy::LoadFromFile(int int_type, string &fn)
{
	ifstream par_file;
	double energy;
	int n;

	par_file.open(fn.c_str());
	n = -1;
	while (true) {
		par_file >> energy;
		n++;

		if (par_file.eof()) break;

		values[int_type][n] = energy;
	}
	par_file.close();
	return 0;
}

/*************************** TAngleEnergy implementations ***********************/

double TAngleEnergy::Energy(int int_type, double angle)
{
	if (angle <= 2.5) return values[int_type][0];
	else if (177.5 <= angle) return values[int_type][35];
	else {
		int i = idx(angle);
		double v0 = values[int_type][i];
		double v1 = values[int_type][i + 1];
		double angle0 = 2.5 + 5 * i;
		return LinearInterpolation(v0, v1, angle0, angle, double(0.2));
	}
}

double TAngleEnergy::Minimum(int int_type)
{
	double v = values[int_type][0];
	for (int i = 1; i < 36; i++)
		if (values[int_type][i] < v) v = values[int_type][i];
	return v;
}

int TAngleEnergy::LoadFromFile(int int_type, string &fn)
{
	ifstream par_file;
	double energy;
	int n;

	par_file.open(fn.c_str());
	n = -1;
	while (true) {
		par_file >> energy;
		n++;

		if (par_file.eof()) break;

		values[int_type][n] = energy;
	}
	par_file.close();
	return 0;
}

/*************************** TBMKhbond implementations ***********************/

TBMKhbond::TBMKhbond()
{
	SetDefConfig();
}

TBMKhbond::~TBMKhbond()
{
}
    
void TBMKhbond::SetDefConfig()
{
	MinChainDist = 3;

	Min_delta = 1.70;
	Max_delta = 2.60;

	Min_theta = 85.0;
	Max_theta = 180.0;

	Min_psi = 75.0;
	Max_psi = 180.0;

	Burial_renorm = false;
	Energy_coeff = 1.0;
	Penalty_coeff = 0.0;
	Burial_sphere_rad = 7.0;
	Burial_min_atcount = 5;
	Burial_max_atcount = 20;
	Cap_energy_to_zero = false;
	
	Delta_helix_parfile = "Delta-helix.par";
	Delta_strand_parfile = "Delta-strand.par";
	Delta_other_parfile = "Delta-other.par";

	Chi_helix_parfile = "Chi-helix.par";
	Chi_strand_parfile = "Chi-strand.par";
	Chi_other_parfile = "Chi-other.par";

	Theta_helix_parfile = "Theta-helix.par";
	Theta_strand_parfile = "Theta-strand.par";
	Theta_other_parfile = "Theta-other.par";

	Psi_helix_parfile = "Psi-helix.par";
	Psi_strand_parfile = "Psi-strand.par";
	Psi_other_parfile = "Psi-other.par";

	SecStr_plugin = "libSecStr.so";
	SecStr_config = "";

	Delta_coeff = Theta_coeff = Psi_coeff = Chi_coeff = 1.0;
}

int TBMKhbond::ReadConfigFile(string CfgName)
{
	ifstream CfgFile;
	CfgFile.open(CfgName.c_str(), ios::in | ios::binary);
	string line, par, val;
	bool recog_line;

	while (true) {
		getline(CfgFile, line); trim2(line);

		if ((line != "") && (line[0] != '#')) {
			strtokenizer strtokens(line,"=");
			int num=strtokens.count_tokens();
			if (num<2) continue;
			par = strtokens.token(0);
			val = strtokens.token(1);
			trim2(par);
			trim2(val);
			//cerr << par << "=" << val << endl;

			recog_line = false;

			if (par == "MIN CHAIN DIST") { MinChainDist = atoi(val.c_str()); recog_line = true; }

			if (par == "BURIAL RENORMALIZATION") { Burial_renorm = (val=="yes"); recog_line = true; }
			if (par == "ENERGY COEFFICIENT") { Energy_coeff = atof(val.c_str()); recog_line = true; }
			if (par == "PENALTY COEFFICIENT") { Penalty_coeff = atof(val.c_str()); recog_line = true; }
			if (par == "BURIAL SPHERE RADIUS") { Burial_sphere_rad = atof(val.c_str()); recog_line = true; }
			if (par == "BURIAL MIN ATOM COUNT") { Burial_min_atcount = atoi(val.c_str()); recog_line = true; }
			if (par == "BURIAL MAX ATOM COUNT") { Burial_max_atcount = atoi(val.c_str()); recog_line = true; }
			if (par == "CAP ENERGY TO ZERO") { Cap_energy_to_zero =  (val=="yes"); recog_line = true; }
			
			if (par == "MINIMUM DELTA") { Min_delta = atof(val.c_str()); recog_line = true; }
			if (par == "MAXIMUM DELTA") { Max_delta = atof(val.c_str()); recog_line = true; }
			if (par == "DELTA HELIX FILE") { Delta_helix_parfile = val; recog_line = true; }
			if (par == "DELTA STRAND FILE") { Delta_strand_parfile = val; recog_line = true; }
			if (par == "DELTA OTHER FILE") { Delta_other_parfile = val; recog_line = true; }

			if (par == "CHI HELIX FILE") { Chi_helix_parfile = val; recog_line = true; }
			if (par == "CHI STRAND FILE") { Chi_strand_parfile = val; recog_line = true; }
			if (par == "CHI OTHER FILE") { Chi_other_parfile = val; recog_line = true; }

			if (par == "MINIMUM THETA") { Min_theta = atof(val.c_str()); recog_line = true; }
			if (par == "MAXIMUM THETA") { Max_theta = atof(val.c_str()); recog_line = true; }
			if (par == "THETA HELIX FILE") { Theta_helix_parfile = val; recog_line = true; }
			if (par == "THETA STRAND FILE") { Theta_strand_parfile = val; recog_line = true; }
			if (par == "THETA OTHER FILE") { Theta_other_parfile = val; recog_line = true; }

			if (par == "MINIMUM PSI") { Min_psi = atof(val.c_str()); recog_line = true; }
			if (par == "MAXIMUM PSI") { Max_psi = atof(val.c_str()); recog_line = true; }
			if (par == "PSI HELIX FILE") { Psi_helix_parfile = val; recog_line = true; }
			if (par == "PSI STRAND FILE") { Psi_strand_parfile = val; recog_line = true; }
			if (par == "PSI OTHER FILE") { Psi_other_parfile = val; recog_line = true; }

			if (par == "DELTA COEFFICIENT") { Delta_coeff = atof(val.c_str()); recog_line = true; }
			if (par == "THETA COEFFICIENT") { Theta_coeff = atof(val.c_str()); recog_line = true; }
			if (par == "PSI COEFFICIENT") { Psi_coeff = atof(val.c_str()); recog_line = true; }
			if (par == "CHI COEFFICIENT") { Chi_coeff = atof(val.c_str()); recog_line = true; }
		}

		if (CfgFile.eof()) break;
	}
	CfgFile.close();
	return 0;
}

void TBMKhbond::Init(string CfgName)
{
	ReadConfigFile(CfgName);

	delta_energy.LoadFromFile(HELIX_HELIX, Delta_helix_parfile);
	delta_energy.LoadFromFile(STRAND_STRAND, Delta_strand_parfile);
	delta_energy.LoadFromFile(OTHER, Delta_other_parfile);

	chi_energy.LoadFromFile(HELIX_HELIX, Chi_helix_parfile);
	chi_energy.LoadFromFile(STRAND_STRAND, Chi_strand_parfile);
	chi_energy.LoadFromFile(OTHER, Chi_other_parfile);

	theta_energy.LoadFromFile(HELIX_HELIX, Theta_helix_parfile);
	theta_energy.LoadFromFile(STRAND_STRAND, Theta_strand_parfile);
	theta_energy.LoadFromFile(OTHER, Theta_other_parfile);

	psi_energy.LoadFromFile(HELIX_HELIX, Psi_helix_parfile);
	psi_energy.LoadFromFile(STRAND_STRAND, Psi_strand_parfile);
	psi_energy.LoadFromFile(OTHER, Psi_other_parfile);

	// Calculating minimum energy of a hydrogen bond.
	double mean_min_delta_energy, mean_min_theta_energy, mean_min_psi_energy, mean_min_chi_energy;
	mean_min_delta_energy = mean_min_theta_energy = mean_min_psi_energy = mean_min_chi_energy = 0;
	for (int itype = HELIX_HELIX; itype <= OTHER; itype++) {
		mean_min_delta_energy += delta_energy.Minimum(itype);
		mean_min_theta_energy += theta_energy.Minimum(itype);
		mean_min_psi_energy += psi_energy.Minimum(itype);
		mean_min_chi_energy += chi_energy.Minimum(itype);
	}

	mean_min_delta_energy /= 3.0;
	mean_min_theta_energy /= 3.0;
	mean_min_psi_energy /= 3.0;
	mean_min_chi_energy /= 3.0;

	HBond_Min_Energy = Delta_coeff * mean_min_delta_energy + Theta_coeff * mean_min_theta_energy +
			   Psi_coeff * mean_min_psi_energy + Chi_coeff * mean_min_chi_energy;
}

int TBMKhbond::IntType(int ss1, int ss2)
{
	if ((ss1 == HELIX) && (ss2 == HELIX)) return HELIX_HELIX;
	if ((ss1 == STRAND) && (ss2 == STRAND)) return STRAND_STRAND;
	else return OTHER;
}

// compute the pair energy between a Backbone CO of residue1 and a Backbone NH of residue2.
// ChainDist is the chain distance between two atoms
// delta is the euclid distance between two atoms
// theta = VectAngle(NH, OH);
// psi = VectAngle(HO, CO);
// chi = DihedralAngle(CA_1->Pos, C_1->Pos, O_1->Pos, H_2->Pos);
// CO_burial= CO_burial(id1, aa1), NH_burial= NH_burial(id2, aa2)
double TBMKhbond::BMKHBPairEnergy(int ss1, int ss2, int ChainDist, double delta, double theta, 
								  double psi, double chi, 
								  double CO_burial, double NH_burial)
{
	double PairEnergy = 0;
	//cerr << "delta::" << Min_delta << "<=" << delta << "<=" << Max_delta << "&&" << MinChainDist << "<=" <<ChainDist<< endl;
	if ((Min_delta <= delta) && (delta <= Max_delta) && delta_energy.InRange(delta) && (MinChainDist < ChainDist)) {
		if ((Min_theta <= theta) && (theta <= Max_theta) && (Min_psi <= psi) && (psi <= Max_psi)) {
//*
			int itype = IntType(ss1, ss2);
			//cerr << itype << "~~" << Min_theta << "<=" << theta << "<=" <<Max_theta<<"&&"<<Min_psi <<"<="<< psi << "<=" << Max_psi << endl;
			if (Cap_energy_to_zero)
				PairEnergy = Delta_coeff * min(double(0.0), delta_energy(itype, delta)) +
					 Theta_coeff * min(double(0.0), theta_energy(itype, theta)) +
					 Psi_coeff * min(double(0.0), psi_energy(itype, psi)) +
					 Chi_coeff * min(double(0.0), chi_energy(itype, chi));
			else
				PairEnergy = Delta_coeff * delta_energy(itype, delta) +
					 Theta_coeff * theta_energy(itype, theta) +
					 Psi_coeff * psi_energy(itype, psi) +
					 Chi_coeff * chi_energy(itype, chi);
//*/
			//if (Burial_renorm)
				// Renormalizing pairwise energy with the geometric mean of the
				// CO and NH burials.
				//PairEnergy *= sqrt(CO_burial * NH_burial);
//			PairEnergy = -1.0;
			//cerr << PairEnergy << "*= sqrt(" << CO_burial <<" *" << NH_burial<<")" << endl;;
		}
	}
	return PairEnergy;
}

// TLinkedList<double> CO_burial, NH_burial;

