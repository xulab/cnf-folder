#ifndef _PSP_H_
#define _PSP_H_

#include <stdio.h>
#include <stdlib.h>

#include "IP_constant.h"
#include "config.h"


class PSPInfo{
	private:
		Config* config;
	
	public:
		PSPInfo(Config* conf);
		~PSPInfo();

		//load PSP information from one file into PSP_array
		//the user is resposible for allocate a block of memory
		//of size seqLen*21 to PSP_array

		bool Load(char* file,short* PSP_array,int* seqLen);
};

char* trim(char* s);

#endif
