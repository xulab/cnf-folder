
#include "Contact.h"
#include "strtokenizer.h"
#include "BALL/MATHS/vector3.h"

using namespace std;

//we require pos2>pos1
Bipartite* BipartiteList::search(int pos1, int pos2)
{
	//cerr << "BipartiteList::search(" << pos1 << "," << pos2 << ")" << endl;
	for (int i=0; i<(int)size(); i++){
		//cerr << "\tbp=(" << at(i).head1 << "," << at(i).tail1 << "," << at(i).head2 << "," << at(i).tail2 << ")" << endl;
		if (at(i).tail1>=pos1 && pos1>=at(i).head1 && (at(i).head2-pos2)*(at(i).tail2-pos2)<=0)
			return &(at(i));
	}
	return NULL;
}

void BipartiteList::Insert(Bipartite& bp)
{
	bool inserted=false;
	//cerr << " INSERTING (" << bp.head1<< "," << bp.head2 << "):(" << bp.tail1 << "," <<bp.tail2 << ")"<< endl;
	for (iterator it = begin(); it<end(); it++){
		//cerr << "   checking (" << (*it).head1<< "," << (*it).tail1 << "):(" << (*it).head2 << "," <<(*it).tail2 << ")" << endl;
		if (((*it).tail1-bp.head1)*((*it).head1-bp.head1)<=0 && ((*it).head2-bp.head2)*((*it).tail2-bp.head2)<=0){
			if (bp.head1<(*it).head1 || bp.tail1>(*it).tail1){
				(*it).head1=bp.head1; (*it).head2=bp.head2; (*it).tail1=bp.tail1; (*it).tail2=bp.tail2;
				//cerr << "       Replaced" << endl;
			} //else cerr << "??" << bp.head1<<"<"<<(*it).head1 << ":" << bp.tail1<<">"<<(*it).tail1 << endl;
			return;
		}
		if ((*it).head1>bp.head1){
			bp.idx=it-begin();
			//cerr << "       inserted before (" << (*it).head1<< "," << (*it).tail1 << "):(" << (*it).head2 << "," <<(*it).tail2 << ")" << endl;
			insert(it, bp); inserted=true; break;
		}
		if ( ((*it).head1==bp.head1 && (*it).head2<bp.head2) ) {
			bp.idx=it-begin();
			//cerr << "       inserted before (" << (*it).head1<< "," << (*it).tail1 << "):(" << (*it).head2 << "," <<(*it).tail2 << ")" << endl;
			insert(it, bp); inserted=true; 
			break;
		}
	}
	if (!inserted) {
		bp.idx=size();
		push_back(bp); 
		//cerr << "       Append to the tail."<< endl;
	} 
}

bool BipartiteList::isFolded(int pos)
{
	return foldFlags[pos]==1;
}

void BipartiteList::SetFoldFlag(int idx, int fold)
{
	//cerr << "SetFoldFlag(" << idx << ", " << fold << ") from " << foldFlags.size() << endl;
	foldFlags[idx]=fold; // 1: fold; 0: unfold
}

void BipartiteList::InitFoldFlags(int seqLen)
{
	for (iterator it = begin(); it<end(); it++){
		(*it).folded1=(*it).folded2=false;
		(*it).min_err=100;
	}
	foldFlags.clear();
	foldFlags.resize(seqLen,0);
}

Bipartite* BipartiteList::searchUnfoldedWithin(int pos1, int pos2) 
{
	//cerr << "BipartiteList::searchUnfoldedWithin("<< pos1<<","<<pos2<<") in "<<size()<<" elements"<<endl;
	for (int i=0; i<(int)size(); i++){
		int head=min(min(at(i).head1, at(i).tail1), min(at(i).head2, at(i).tail2));
		int tail=max(max(at(i).head1, at(i).tail1), max(at(i).head2, at(i).tail2));
		//cerr << "~~" << head << "," << tail;
		if (head<=pos1 && pos2<=tail) continue;
		if (head<=pos1 && pos1<tail && tail<=pos2 && at(i).folded2==false){
			double width=tail-pos1+0.1; //cerr << "width=" << width;
			double ratio1=(pos1-head)/width;
			double ratio2=(pos2-tail)/width;
			if (ratio1<ratio2){
				//cerr << " situation 1: " << head << "<=pos1<=" << tail << "; r1=" << ratio1 << "; r2=" << ratio2 << endl;
				return &at(i);
			}
		}
		if (head>=pos1 && head<pos2 && pos2<=tail && at(i).folded1==false){
			double width=pos2-head+0.1; //cerr << "width=" << width;
			double ratio1=(pos2-head)/width;
			double ratio2=(head-pos1)/width;
			if (ratio1<ratio2){
				//cerr << " situation 2: " << head << "<=pos2<=" << tail << "; r1=" << ratio1 << "; r2=" << ratio2 << endl;
				return &at(i);
			}
		}
		if (pos1<=head && tail<=pos2 && (at(i).folded1==false || at(i).folded2==false)){
			//cerr << "situation 3: pos1<=" << head << "<=" << tail << "<=pos2 and " << at(i).folded1 << "--" << at(i).folded2<< endl;
			return &at(i);
		}
		//if (pos1>=tail) break;
	}
	cerr << endl;
	return NULL;
};

void BipartiteList::searchUnfoldedWithin(int pos1, int pos2, int* unfoldedBP) 
{
	//cerr << "BipartiteList::searchUnfoldedWithin("<< pos1<<","<<pos2<<") in "<<size()<<" elements"<<endl;
	int unfold_idx=0;
	for (int i=0; i<(int)size(); i++){
		int head=min(min(at(i).head1, at(i).tail1), min(at(i).head2, at(i).tail2));
		int tail=max(max(at(i).head1, at(i).tail1), max(at(i).head2, at(i).tail2));
		//cerr << "~~" << head << "," << tail;
		if (head<=pos1 && pos2<=tail) continue;
		if (head<=pos1 && pos1<tail && tail<=pos2 && at(i).folded2==false){
			double width=tail-pos1+0.1; //cerr << "width=" << width;
			double ratio1=(pos1-head)/width;
			double ratio2=(pos2-tail)/width;
			if (ratio1<ratio2){
				//cerr << " situation 1: " << head << "<=pos1<=" << tail << "; r1=" << ratio1 << "; r2=" << ratio2 << endl;
				unfoldedBP[unfold_idx++]=at(i).idx;
				continue;
			}
		}
		if (head>=pos1 && head<pos2 && pos2<=tail && at(i).folded1==false){
			double width=pos2-head+0.1; //cerr << "width=" << width;
			double ratio1=(pos2-head)/width;
			double ratio2=(head-pos1)/width;
			if (ratio1<ratio2){
				//cerr << " situation 2: " << head << "<=pos2<=" << tail << "; r1=" << ratio1 << "; r2=" << ratio2 << endl;
				unfoldedBP[unfold_idx++]=at(i).idx;
				continue;
			}
		}
		if (pos1<=head && tail<=pos2 && (at(i).folded1==false || at(i).folded2==false)){
			//cerr << "situation 3: pos1<=" << head << "<=" << tail << "<=pos2 and " << at(i).folded1 << "--" << at(i).folded2<< endl;
			unfoldedBP[unfold_idx++]=at(i).idx;
		}
	}
	//cerr << "  searchUnfoldedWithin found " << unfold_idx << " segments " << endl;
}

BipartiteDAG::BipartiteDAG()
{
}

void BipartiteDAG::initBipartiteDAG(BipartiteList& bpList)
{
	ends.resize(bpList.size(), -1);
	starters.resize(bpList.size(), bpList.size());
	starters.Fill(-1);
}

// Add a note into DAG. return true if no circle; otherwise return false
bool BipartiteDAG::addNode(int bp_idx, int child_idx)
{
	if (ends[bp_idx]==child_idx) // edge already existed
		return false;

	// check if the edge bp_idx->child_idx causes a circle.
	int idx=ends[child_idx];
	while (idx!=-1){
		if (idx==bp_idx)
			return false;
		idx=ends[idx];
	}
	
	// Not causing circle
	ends[bp_idx]=child_idx;
	starters(child_idx,bp_idx)=1;
	return true;
}

void BipartiteDAG::removeNode(int node_idx)
{
	//cerr << "removeNode("<< node_idx<< "; #ends=" << ends.size() << endl;
	ends[node_idx]=-1;
	for (int i=0; i<(int)starters.cols; i++) {
		int starter=(int)starters(node_idx,i);
		//cerr << " starter("<<i<<")=" << starter << ", ";
		if (starter!=-1)
			ends[i]=-1;
		starters(node_idx, i)=-1;
	}
	//cerr << endl;
}

Contact::Contact(string contact_file, int seq_length)
{
	this->seq_length=seq_length;
	contactMap.resize(seq_length, seq_length);
	readContactFile(contact_file);
	buildBipartiteList();
	this->nFolded=0;
}

bool Contact::isClosedBipartite(Bipartite& bp)
{
	int pos1=min(min(bp.head1, bp.tail1), min(bp.head2, bp.tail2));
	int pos2=max(max(bp.head1, bp.tail1), max(bp.head2, bp.tail2));
	int* unfoldedBP=new int[bpList.size()];
	for (int i=0; i<(int)bpList.size(); i++)
		unfoldedBP[i]=-1;
	bpList.searchUnfoldedWithin(pos1, pos2, unfoldedBP);
	for (int i=0; i<(int)bpList.size(); i++){
		if (unfoldedBP[i]==-1)
			break;
		else if (bpDAG.addNode(bp.idx, unfoldedBP[i])){
			//cerr << "unfolded BP: " << b->head1 << "," << b->tail2 << endl;
			delete unfoldedBP;
			return false;
		}
	}
	delete unfoldedBP;
	return true;
}

bool Contact::isFolded(int pos)
{
	return bpList.isFolded(pos); 
}

void Contact::resetFoldFlag()
{
	nFolded=0;
	bpList.InitFoldFlags(seq_length);
	bpDAG.initBipartiteDAG(bpList);
}

void Contact::setBipartiteClose(Bipartite& bp, double err)
{ 
	//cerr << "  Contact::setBipartiteClose:" << bp.head1 << "--" << bp.head2 << endl;
	Bipartite* pbp=bpList.search(bp.head1, bp.head2);
	if (pbp!=NULL) {
		//cerr << "  \tFound: " << pbp->head1 << "--" << pbp->tail2 << endl;
		int head=min(min(pbp->head1, pbp->tail1), min(pbp->head2, pbp->tail2));
		int tail=max(max(pbp->head1, pbp->tail1), max(pbp->head2, pbp->tail2));
		for (int i=head; i<tail; i++){
			bpList.SetFoldFlag(i, 1);
		}
		pbp->folded1=true;
		pbp->folded2=true;
		pbp->min_err = err;
		nFolded++;
		bpDAG.removeNode(pbp->idx);
	}
}

// Contact file has the following format: residue1, residue2, 0, 8, confidence, sorted by confidence. if confidence is less than 0.3, discard it.
void Contact::readContactFile(string contact_file)
{
	ifstream idataf(contact_file.c_str());
	if (!idataf.is_open()) {
		cerr << "Can't open file: " << contact_file << endl;
		return;
	}
	string line;
	int total=0;
	while (getline(idataf, line)) {
		if (line[0]=='#')
			continue;
		strtokenizer strtokens( line ," ,\t");
		int num=strtokens.count_tokens();
		if (num<5) {
			cerr << "Bad contact: [" << line << "] in file: " << contact_file << endl;
			continue;
		}
		int r1 = atoi(strtokens.token(0).c_str())-1;
		int r2 = atoi(strtokens.token(1).c_str())-1;
		double confidence = atof(strtokens.token(4).c_str());
		if (confidence<0.30 || total>10)
			break;
		if (abs(r1-r2)<10)
			continue;
		total++;
		cerr << "Contact " << total << "@ " << r1 <<"," << r2 << ":" << confidence << endl;
		contactMap(r1,r2)=confidence;
	}
	return;
}

// In this case, we assume the contact are obtained from the contact prediction. Therefore all pair are non-redundant.
// And, the contactmap is filled with euclidean distances.
void Contact::buildBipartiteList()
{
	int length_s=seq_length;
	for (int j=0; j<length_s; j++){
		for (int k=0; k<length_s; k++){
			if (abs(k-j)<6) continue;

			if (contactMap(j,k)>0){ 
				// a contact is found. 
				// First, check if it's already contained in a found Bipartite
				if (bpList.search(j, k)) continue;

				//Now we need to extend it on both side of both subsequence to build a cluster
				int headj=j, headk=k, tailj=j, tailk=k;
				bool inverse=false;
				int step=1; // step=-1 if it's inveres sequence; h_non counts the non contact residues in the head
				while (1) { // check for head neighbouring residues, if not in contact, then need to check if it's inverse
					if (headj>1 && headk>1 && headk-step<length_s) {
						if (contactMap(headj-1, headk-step)>0){ 
							headj-=1; headk-=step; continue;
						} else if (!inverse && (contactMap(headj-1, tailk+1)>0) ){
							inverse = true; step = -1; headj--; 
							int tmp = headk; headk=tailk+1; tailk=tmp;
							continue;
						}
					}
					
					// check for tail neighbouring residues, if not in contact, then need to check if it's inverse
					if (tailj<length_s-1 && tailk>1 && tailk<length_s-1) {
						if (contactMap(tailj+1, tailk+step)>0){ 
							tailj+=1; tailk+=step; continue;
						} else if (!inverse && (contactMap(headj-1, tailk+1)>0)) {
							inverse = true; step =-1; tailj++;
							int tmp = headk; headk=tailk+1; tailk=tmp;
							continue;
						}
					}
					break; // no place to extend, so stop the loop
				}

				int len=tailj-headj+1;
				Bipartite b;
				b.head1=headj; b.tail1=tailj; b.head2=headk; b.tail2=tailk;
				//cerr << "Insert Bipartite: (" << b.head1 << "," << b.tail1 << ") -- (" << b.head2 << "," << b.tail2 << ")" << endl;
				bpList.Insert(b);
			}
		}
	}
	for (int i=0; i<(int)bpList.size(); i++){
		Bipartite b=bpList[i];
		cerr << "  Bipartite: (" << b.head1 << "," << b.tail1 << ") -- (" << b.head2 << "," << b.tail2 << ")" << endl;
	}
}

/*
// Here, the contactmap is filled with labels.
void Contact::buildBipartiteListForTraining()
{
	int length_s=seq_length;
	vector<Bipartite> seq_found;
	for (int j=0; j<length_s; j++){
		for (int k=0; k<length_s; k++){
			if (abs(k-j)<=6) continue;

			if (contactmap(j,k)>=0){ 
				// a contact is found. 
				// First, check if it's already contained in a found Bipartite
				bool found=false;
				for (int s=0; s<seq_found.size(); s++){
					Bipartite b=seq_found[s];
					if (b.head1<=j && b.tail1>=j && (b.tail2-b.head2)*(k-b.head2)>=0 && abs(b.tail2-b.head2)>=abs(k-b.head2))
						found=true;
				}
				if (found) countiue;
				//Now we need to extend it on both side of both subsequence to build a cluster
				int headj=j, headk=k, tailj=j, tailk=k;
				bool inverse=false;
				int gap_max=10, gap_allow=3;
				int step=1, h_non=0, t_non=0; // step=-1 if it's inveres sequence; h_non counts the non contact residues in the head
				do {
					// check for head neighbouring residues, if not in contact, then need to check if it's inverse
					if (headj>1 && headk>step && headk-step<length_s) {
						if (contactmap(headj-1, headk-step)!=-1){ 
							if (h_non>gap_allow){ h_non=gap_max; continue; }
							headj-=1; headk-=step; h_non=0; 
						} else {
							if (!inverse && (contactmap(headj-1, tailk)!=-1 || contactmap(tailj, headk-1)!=-1)) {
								inverse = true; step = -1; h_non=0; t_non =0;
								int tmp = headk; headk=tailk; tailk=tmp;
								continue;
							}
							if (contactmap(headj-1, headk)!=-1) { 
								if (h_non>gap_allow){ h_non=gap_max; continue; }
								headj-=1; h_non=0;
							} else if (contactmap(headj, headk-step)!=-1) { 
								if (h_non>gap_allow){ h_non=gap_max; continue; }
								headk-=step; h_non=0; continue; 
							}
							h_non++;
						}
					} else h_non=gap_max;
					
					// check for tail neighbouring residues, if not in contact, then need to check if it's inverse
					if (tailj<length_s-1 && tailk+step>0 && tailk+step<length_s) {
						if (contactmap(tailj+1, tailk+step)!=-1){ 
							if (t_non>gap_allow){ t_non=gap_max; continue; }
							tailj+=1; tailk+=step; t_non=0;
						} else {
							if (!inverse && (contactmap(headj, tailk+1)!=-1 || contactmap(tailj+1, headk)!=-1)) {
								inverse = true; step =-1; h_non=0; t_non=0; 
								int tmp = headk; headk=tailk; tailk=tmp;
								continue;
							}
							if (contactmap(tailj+1, tailk)!=-1) { 
								if (t_non>gap_allow){ t_non=gap_max; continue; }
								tailj+=1; t_non=0; continue;
							} else if (contactmap(tailj, tailk+step)!=-1) { 
								if (t_non>gap_allow){ t_non=gap_max; continue; }
								tailk+=step; t_non=0; continue; 
							}
							t_non++;
						}
					} else t_non=gap_max;
					
				} while (h_non<gap_max && t_non<gap_max);

				int len1=tailj-headj+1, len2=abs(tailk-headk)+1;
				SEQUENCE *seq = new SEQUENCE(len1, len2, this);
				for (int cj=0; cj<len1; cj++){
					seq->obs_feature1->RowCopy(cj, (*obs_feature)[cj+headj]);
					for (int ck=0; ck<len2; ck++){
						if (ck==0) seq->obs_feature2->RowCopy(ck, obs_feature(ck*step+headk));
						(*seq->obs_label)(cj, ck)=contactmap(cj+headj, ck*step+headk);

						if (cj==0 || ck==0) continue;
						int lastLabel = (*seq->obs_label)(cj-1, ck-1);
						int curLabel = (*seq->obs_label)(cj, ck);
						int idx = lastLabel*num_labels+curLabel;
						if (mtrMi1[idx]==0) {
							mtrMi1[idx]=1;
							sparseMi.insert(lastLabel+1, curLabel)=idx; // store the state
						}
					}
				}
				Bipartite b;
				b.head1=headj; b.tail1=tailj; b.head2=headk; b.tail2=tailk;
				seq_found.append(b);
				k=max(headk, tailk);
			}
		}
	}
}
*/
