/*******************************************************************************
                                  DOPE-Back-AllAtom.cpp
                                  ---------------------
  Copyright (C) 2005 The University of Chicago
  Authors: 
  Min-yi Shen, original code
  Andres Colubri, coversion to C++
  James Fitzgerald, backbone dependence
  Description:
  PL plugin to calculate Min-yi's Statistical Potential for Fold
  Recognition. Backbone Dependent and allows for all atom treatment.
*******************************************************************************/
#include <cstddef> // Needed to use NULL.
#include <fstream>
#include <cstring>
#include <cmath>
#include <string>
#include <iostream>
using namespace std;

#include "DOPE.h"

DOPE::DOPE(){
	dist_cutoff = 15.0;
	n_bins = 30;
	max_atom_code = MAX_ATOM_CODE;
	Energy=0;
	pres_s2i=0;
	pres_i2s=0;
	patom_s2i=0;
	patom_i2s=0;
	for (int i=0; i<10; i++)
		atomMap[i]=-1;
	for (int i=0; i<26; i++)
		resMap[i]=-1;
}

int DOPE::LoadFromFile(string fn, double *Energy, bool bSymmetry)//[20][MAX_ATOM_CODE+1][20][MAX_ATOM_CODE+1])
{
	string aa0_str, at0_str, aa1_str, at1_str;
   	int aa0, at0, aa1, at1, bin;
   	double energy;

	ifstream par_file;
	par_file.open(fn.c_str(), ios::in);
	while (true)
	{
		par_file >> aa0_str;
		par_file >> at0_str;
		par_file >> aa1_str;
		par_file >> at1_str;

		if (par_file.eof()) break;

		aa0 = GetResidueCode(aa0_str, 1);
		at0 = GetAtomCode(at0_str, 1);
		aa1 = GetResidueCode(aa1_str, 1);
		at1 = GetAtomCode(at1_str, 1);

	//cerr << "LoadPar(aa0="<<aa0_str<<"; atom1="<<at0_str<<"; aa1="<<aa1_str<<"; atom2="<<at1_str<<endl;
	//cerr << "LoadPar(aa0="<<aa0<<"; atom1="<<at0<<"; aa1="<<aa1<<"; atom2="<<at1<<endl;
		for (bin = 0; bin < n_bins; bin++) {
       			par_file >> energy;
       			//Energy[aa0][at0][aa1][at1][bin] = energy;
			if (at0>=0 && at1>=0) {
	       			Energy[bin+n_bins*(at1+(max_atom_code+1)*(aa1+20*(at0+(max_atom_code+1)*aa0)))] = energy;
	       			// If elements with aa < aa1 appear in the file, this condition ensures that they are not overwritten onto the symetric positions.
       				if (aa0 < aa1 && bSymmetry)
       					Energy[bin+n_bins*(at0+(max_atom_code+1)*(aa0+20*(at1+(max_atom_code+1)*aa1)))] = energy; //Energy[aa1][at1][aa0][at0][bin] = energy;
			}
		}
	}
	par_file.close();
	cerr<<"Successfully read file: "<<fn<<" with n_bins="<<n_bins<<" max_atom_code="<<max_atom_code<< "; pres_s2i:" << pres_s2i->size() << "; patom_s2i:"<<patom_s2i->size() << endl;
	return 0;
}

// Search the DOPE table to get the energy for 2 atoms
double DOPE::calcAtomDOPE(string residue_id1, string atom1, string residue_id2, string atom2, double dist) 
{
	double e_pair = 0;
	if ((0.0 < dist) && (dist < dist_cutoff))
	{
		int aa0 = GetResidueCode(residue_id1);
		int at0 = GetAtomCode(atom1, 0);
		int aa1 = GetResidueCode(residue_id2);
		int at1 = GetAtomCode(atom2, 0);
		if (aa0==-1 || aa1==-1|| at0==-1|| at1==-1)
			return 0;    
		// The distance lies between middle points of bins ri and ri + 1.
		double r1 = 2.0 * (dist - 0.25);
		int ri = int(r1);

		// Linear interpolation between the energy value at those bins.
		double f1 = r1 - ri;
		double f2 = 1.0 - f1;
		//cerr <<"calcAtomDOPE(r_id1="<<residue_id1<<"; atom1="<<atom1<<"; r_id2="<<residue_id2<<"; atom2="<<atom2<<"; dist="<<dist<<endl;
		int idx = n_bins*(at1+(max_atom_code+1)*(aa1+20*(at0+(max_atom_code+1)*aa0)));
		if (0 < ri)
		{
			if (ri>=n_bins)
				return 0;
			// When ri = n_bins - 1, StatPot(ri + 1) gives 0, so we are interpolating
    		// between the last energy value stored in the table and 0, which is ok.
			double e1 = Energy[ri+idx];//[aa0][at0][aa1][at1][ri];
			double e2=0;
			if (ri+1<n_bins)
				e2 = Energy[(ri+1)+idx];//[aa0][at0][aa1][at1][ri+1];
    		e_pair = f2 * e1 + f1 * e2;

			//cerr <<"; ri="<<ri<<"; f1="<<f1<<"; e1="<<e1<<"; e2="<<e2<<endl;
		}
		else 
    		e_pair = Energy[0+idx];//[aa0][at0][aa1][at1][0];
	}

	//cerr <<"; energy="<<e_pair<<endl;
	return e_pair;
};

void DOPE::InitializeMemory(int n, double*& Energy)//[][20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1]) 
{
	n_bins = n;
	int size = 20*(max_atom_code+1)*20*(max_atom_code+1)*n_bins;
	//cerr << "DOPE::InitializeMemory "<<size<< endl;
/*
	int aa0, aa1, at0, at1;
    for (aa0 = 0; aa0 < 20; aa0++)
        for (at0 = 0; at0 <= MAX_ATOM_CODE; at0++)
            for (aa1 = 0; aa1 < 20; aa1++)
                for (at1 = 0; at1 <= MAX_ATOM_CODE; at1++)
                    if (0 < n_bins) Energy[aa0][at0][aa1][at1] = new double[n_bins];
                    else Energy[aa0][at0][aa1][at1] = 0;
*/
	Energy = new double[size];
	if (pres_s2i) 
		pres_s2i->clear();
	else 
		pres_s2i = new mapRESstr2int;
	if (pres_i2s) 
		pres_i2s->clear();
	else 
		pres_i2s = new mapRESint2str;
	if (patom_s2i)
		patom_s2i->clear();
	else
		patom_s2i = new mapATOMstr2int;
	if (patom_i2s)
		patom_i2s->clear();
	else 
		patom_i2s = new mapATOMint2str;
}
	
void DOPE::ReleaseMemory(double *&Energy)//[20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1][]) 
{
/*
    if (0 < n_bins) {
        int aa0, aa1, at0, at1;
        for (aa0 = 0; aa0 < 20; aa0++)
            for (at0 = 0; at0 <= MAX_ATOM_CODE; at0++)
                for (aa1 = 0; aa1 < 20; aa1++)
                    for (at1 = 0; at1 <= MAX_ATOM_CODE; at1++)
                        delete[] Energy[aa0][at0][aa1][at1];
    }
*/
	//cerr << "DOPE::ReleaseMemory " << n_bins << endl;
	delete Energy;
	Energy=0;
}
	
void DOPE::InitializeEnergy(double *Energy)//[20][MAX_ATOM_CODE + 1][20][MAX_ATOM_CODE + 1][]) 
{
/*
    int aa0, aa1, at0, at1, bin;
    for (aa0 = 0; aa0 < 20; aa0++)
        for (at0 = 0; at0 <= MAX_ATOM_CODE; at0++)
            for (aa1 = 0; aa1 < 20; aa1++)
                for (at1 = 0; at1 <= MAX_ATOM_CODE; at1++)
                    for (bin = 0; bin < n_bins; bin++)
                        Energy[aa0][at0][aa1][at1][bin] = 0.0;
*/
	//cerr << "DOPE::InitializeEnergy " << n_bins << endl;
	memset(Energy, 0, sizeof(double)*20*(max_atom_code+1)*20*(max_atom_code+1)*n_bins);
}

// Added by Feng Zhao
// used to get the index of the residue given its name
// insert_flag: 0-- search the residue code only; 
//		1-- add the aa_str into the residue code map if not found
// return -1 for "result not found"
int DOPE::GetResidueCode(string aa_str, int insert_flag)
{
	mapRESstr2int::iterator resmapit;
	resmapit = pres_s2i->find(aa_str);	
	if (resmapit == pres_s2i->end()) {
		if (insert_flag==1) {
			// label not found, insert it to the map
			int icode = pres_s2i->size();
			pres_s2i->insert(pair<string, int>(aa_str, icode));
			pres_i2s->insert(pair<int, string>(icode, aa_str));
			//cerr<<"GetResidueCode<"<<icode<<","<<aa_str<<">"<<endl;
			string arrResidueName[25]={"ALA","", "CYS","ASP","GLU","PHE","GLY","HIS","ILE","", "LYS","LEU",
        	                  "MET","ASN","", "PRO","GLN","ARG","SER","THR","", "VAL","TRP","", "TYR"};
			for (int i=0; i<25; i++){
				if (arrResidueName[i]==aa_str){
					resMap[i]=icode;
					break;
				}
			}

			return icode;
		} else
			return -1;
	} else {
		 return resmapit->second;
	}
};
int DOPE::GetResidueCode(int aa)
{
	return resMap[aa];
}

// get the index of the atom given its name
// insert_flag: 0-- search the atom code only; 
//		1-- add the aa_str into the atom code map if not found
// return -1 for "result not found"
int DOPE::GetAtomCode(string atom_str, int insert_flag)
{
	mapATOMstr2int::iterator mapit;
	mapit = patom_s2i->find(atom_str);	
	if (mapit == patom_s2i->end()) {
		if (insert_flag==1) {
			// label not found, insert it to the map
			//int icode = patom_s2i->size();
			int icode = -1;
			string arrAtomName[7]={"CA", "CB", "O", "N", "C", "HN", "SG"};
			for (int i=0; i<7; i++){
				if (arrAtomName[i]==atom_str){
					//atomMap[i]=icode;
					atomMap[i]=i; icode = i;
					break;
				}
			}
			if (icode>=0){
				patom_s2i->insert(pair<string, int>(atom_str, icode));
				patom_i2s->insert(pair<int, string>(icode, atom_str));
				//cerr<<"GetAtomCode<"<<icode<<","<<atom_str<<">"<<endl;
			}
			
			return icode;
		} else
			return -1;
	} else {
		 return mapit->second;
	}
}; 
int DOPE::GetAtomCode(int atom)
{
	return atomMap[atom];
}
