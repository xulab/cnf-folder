#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
using namespace std;

#include "SimpPDB.h"

char* aa[]={"BCK","GLY","ALA","SER","CYS","VAL","THR","ILE",
           "PRO","MET","ASP","ASN","LEU",
           "LYS","GLU","GLN","ARG",
           "HIS","PHE","TYR","TRP","CYX", "MSE"};
     
char slc[]={'X','G','A','S','C','V','T','I',
    	'P','M','D','N','L','K','E','Q','R',
          'H','F','Y','W','C', 'm'};

int toInt(const string& aString)
{
  char* st=new char[20];
  int start=0;
  for(int i=0; i<(int)aString.size(); i++)
  {
     if(aString[i]!=' ')
     {
        st[start++]=aString[i];
     }
  }
  st[start]='\0';
  int rev=atoi(st);
  delete [] st;
  return rev;
}

double toFloat(const string& aString)
{
  char* st=new char[20];
  int start=0;
  for(int i=0; i<(int)aString.size(); i++)
  {
     if(aString[i]!=' ')
     {
        st[start++]=aString[i];
     }
  }
  st[start]='\0';
  double rev=atof(st);
  delete [] st;
  return rev;
}

SimPDB::SimPDB(const char *aFileName)
{
  mProteinFileName=aFileName;
  mCAlpha=new double[3*LONGEST_CHAIN];
  //mSeq   =new char[LONGEST_CHAIN];
  read();
}
SimPDB::~SimPDB() 
{
   delete [] mCAlpha;
  // delete [] mSeq;
}


void
SimPDB::read()
{
   FILE *input=fopen(mProteinFileName, "r");
   if(!input)
   {
      cerr<<"Can not find protein file "<<mProteinFileName<<endl;
      exit(0);
   }
    
   char buf[400];
   mNumResidue=0;
   double x, y, z;
   char c='a';
   while(c!=EOF)
   {
      int lineIndex=0;
      //char c;
      while((c=fgetc(input))!='\n'&&c!=EOF)
      {
         buf[lineIndex++]=c;
      }
      //if(c==EOF) break;
      buf[lineIndex]='\0';	
      string line=buf;
      if(line.substr(0, 3)=="TER") break;
      if(line.substr(0, 6)=="ENDMDL") break;

      if(line.substr(0, 4)!= "ATOM") continue; // && line.substr(0, 6)!= "HETATM") continue;			
      if(line.substr(13, 4)=="CA  " || line.substr(13, 4)==" CA "
		      || line.substr(13, 4)=="  CA")
      {
  //       cout<<buf<<endl;    	      
         if(toupper(line[21])=='A'|| line[21]==' '||toupper(line[21])=='C')
	 {   
//         cout<<"------==========="<<line[21]<<"========-------------"<<endl;

	    x=toFloat(line.substr(30, 8));
            y=toFloat(line.substr(38, 8));
            z=toFloat(line.substr(46, 8));
	    string AAType=line.substr();
            
	    mCAlpha[mNumResidue*3]=x; 
	    mCAlpha[mNumResidue*3+1]=y; 
     	    mCAlpha[mNumResidue*3+2]=z; 	    
	    AAType=  line.substr(17, 3);
	    
            int residueID=toInt(line.substr(22, 6));
	    /*for(int j=0; j<23; j++)
	    {
               if(strcmp(AAType.c_str(), aa[j])==0)
	       {
	          mSeq[mNumResidue]=slc[j]; 
	       }	       
	    } */
	    mNumResidue++;
           //cout<<mNumResidue<<endl;
	 }
      }
   }//while
}

/*	
int main(int argc, char** argv)
{
   SimPDB* sim=new SimPDB(argv[1]);
   
   cout<<"----"<<sim->mNumResidue<<endl;
   for(int i=0; i<sim->mNumResidue; i++)
   { 
       cout<<sim->mSeq[i]<<" "<<sim->mCAlpha[i*3]<<" "<<sim->mCAlpha[i*3+1]<<" "<<sim->mCAlpha[i*3+2]<<endl;
   }
}*/

