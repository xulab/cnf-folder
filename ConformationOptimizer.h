//this file defines classes for all the conformation optimizer algorithms

#ifndef _CONFORMATION_OPTIMIZER
#define _CONFORMATION_OPTIMIZER

#include <vector>
#include <time.h>
#include <string>

#include "CRFSampler.h"
#include "MCsampler.h"
#include "PhiPsiCluster.h"
#include "FBCluster.h"
#include "StructureBuilder.h"
#include "ScoringFunction.h"

#include "RamaBasin.h"

// For bbq.h, in the makefile, need to add include path: BBQ/ann/ann_1.1/include/
// lib path: BBQ/ann/ann_1.1/lib/ 
// and the library flags: -lANN
#include "bbq.h"
#include "ScoreMatrix.h"
#include "Contact.h"

#include <BALL/KERNEL/protein.h>
#include <BALL/MATHS/vector3.h>


using namespace std;
using namespace BALL;

typedef map<string, pair<Vector3, Vector3> > mapSideChainCenter;
class ConformationOptimizer
{
public:
  ConformationOptimizer();
  ConformationOptimizer(string& proteinName);
  virtual ~ConformationOptimizer();

  // this function should be called before the search functions
  virtual void Initialize(int randomSeed, int DEBUG_MODE, string additional_f);

  void printVi(int pos);

  // greedy strategy
  void GreedySearch(string outdir);

  // call GreedySearch many times until the final structural model has good
  // secondary structure
  void GreedySearch(string outdir, int numRepeats);

  // Markov chain Monte Carlo
  // bSimulatedAnnealing indicates if we perform Simulated Annealing;
  // AnnealSteps is the steps sampled at each temperature;
  // AnnealFact is the annaling factor: T(k) = (T1 - T0) t(k) + T0
  //     where t(k) = AnnealFact * t(k-1) and t(0) = 1.
  void MCMCSearch(string outdir, int bSimulatedAnnealing=0, double AnnealFact=0.9); 
  void ReplicaExchange(string outdir);

  // should be called before Initialize if the proteinName is not specified
  // in constructor  
  void setProteinName(string& proteinName);
  void setContactConditions(string contact_file);
  Contact* mpContact;

  void setStructureBuilder(StructureBuilder* );
  void setScoringFunction(ScoringFunction* );
  void setScoringFunction();

  //output
  virtual void writeDecoy(string& outdir);
  virtual void ReAngle(){};
  
  double calc_RefLog_P();
  bool ConstructTerminalN(Vector3 &C, Vector3 &Cb, Vector3 &Ca, 
                          double &x, double &y, double &z);  
  bool ConstructNH(Vector3 &C0, Vector3& N1, Vector3 &CA1,
                          double &x, double &y, double &z);
  
  // CAp is the position of the previous Ca; CAn is the position of the
  // next Ca; aa is the amino acid type
  // return the center of the side chain
  bool ConstructSG(Vector3 &CAp, Vector3& CA, Vector3 &CAn, Vector3 &SG,
                          string aa);

  double estimateNativeScore(string& sequence, string& ss);
  //double evaluate(string& sequence, string& ss, vector<Vector3 >& structure);
  //double evaluate(string& sequence, string& ss, Protein* protein);
  double evaluate(string& sequence, string& ss, vector<RESIDUE >& structure,
                          int start);
  
  bool ReadSideChainComm(string fname);
  double calcLSP(vector<RESIDUE >& structure, bool relabel_only);
  void ReLabel(int start, int end);
  void readLib();

  int GetNativeConformation(string pdb_file);

  vector<double > presetBondLengths;
  vector<pair<int, int> > refine_segments;
  vector<Vector3> initCAs;
  bool bMergePDB; // flag for merging a refined PDB with its original template pdb
  int DEBUG_MODE;
  string additional_f;

protected:
  StructureBuilder* mStructureBuilder;
  ScoringFunction* mScoringFunction;
  vector<pair<ScoringFunction*, double> > mScoringFunctionList;
  mapSideChainCenter mSideChainCenters;
  vector<string> mPSPSeq;
  
  double weight_radius;
  int mMaxIteration;
  double mSA_StartTemperature;
  double mSA_EndTemperature;
  int mAnnealSteps;
  int mStopEspSteps;

  int mStartingWeightedSimulationStep;
  int mSimulationWeightAlpha;
  int mSimulationWeightBeta;
  int mSimulationWeightCoil;
  int mCurrentSimulationStep;

  //the followingmember variables are used for input information
  string mProteinName;

  //the number of amino acids in the protein
  int mProteinSize;

  //amino acid sequence of the protein
  string  mProteinSeq;

  //secondary structure, can be predicted or native
  string  mSecStruSeq;
  int mSSConfidenceSeq[8192];
  double mProbAsSegmentStart[8192];

  //pretag file is the input of the CRF model
  string  mPretagFile;

  //the CRFSampler
  CRFSampler* mCRFSampler;

  //for the storage of the sampled labels
  vector<string> mLabel;

  //label  for the best conformation
  vector<string> mBestLabel;

  //mStructure only saves C-alpha coordinates
  vector<Vector3 > mStructure;
  vector<Vector2 > mAngle;
  //vector<Vector3 > mStructureSave;
  vector<Vector3 > mBestStructure;

  //the native score
  double mNativeScore;

  // Backbone builder
  BBQ *mBBQ;

  RamaTypology* mpRamaTyp;

  double mJumpFactor;
  double mHBEnergy;
  double mEnergy;
  double mBurialPenalty;
  double new_rg, new_ru, new_br;
  double mContactEnergy, mEPADEnergy;
  double NEFF;

  // fragment length
  int fragLen;

  // library size
  int libLen;

  // interval
  int interval;

  string fragLibFile;

  double*** fragments;

  vector<Vector3 > mStructureNative;
  int mNativeStart;
  double** fragNative;

  vector<Vector3 > mBestFBVec;
  
  int randomSeed;

protected:
  //some helper functions

  void loadSequenceAndSS();

  //sample the CRF hidden states for a protein segment
  void SampleLabels(int start, int end);

  //sample angles from the hidden states for a protein segment
  virtual void SampleAngles(int start, int end){};

  //resbuild the structure of a protein from sampled angles
  virtual void BuildStructure(int start, int end);
  virtual void ApplyContactConstraint(){};

  //randomly pick up a segment to resample its angles
  virtual void SampleASegment(int* start, int* end);

  //check if there is a steric clash in a structural model
  virtual bool detectClashes(){return false;};

  //count the number of steric clashes in a structural model
  virtual int  countClashes(){return 0;};

  //calculate the enery of a structural model
  virtual double calcEnergy(){ return 0;};
  virtual double calcEnergy(vector<RESIDUE>& protein, int start){return 0;};

  // an alternative way to calculate the energy, if available
  virtual double calcEnergy2(vector<RESIDUE>& protein, int start){ return 0;};

  //roll  back to previous structural model
  virtual void Undo(int start, int end){};

  //accept current structure as the best-so-far structure
  virtual void KeepStructure();
  int readPSPFile(string pspname, vector<string>& str_vec);

  // The acceptance rule for the MC simulation.
  // Accept the state with lower engergy
  // Otherwise, sample to accept the new state according to the distribution
  // of P(y)/P(y')
  // prev: the energy of the former state;
  // curr: the energy of the current state.
  // return true for accepting the curr state
  bool MCSacceptMove(double prev, double curr, double temperature);

  void GetConformation(string seq, vector<Vector3 > structure,
                       vector<RESIDUE>& protein);
  double calcRMSD();
};

//this class optimizes the structure in the theta/tau space
class ThetaTauOptimizer: public ConformationOptimizer
{
public:
  ThetaTauOptimizer();
  ThetaTauOptimizer(string& proteinName);
  ~ThetaTauOptimizer();

  virtual void Initialize(int randomSeed, int DEBUG_MODE, string additional_f);
  
  virtual void SampleAngles(int start, int end);
  virtual void BuildStructure(int start, int end);
  virtual void ApplyContactConstraint();
  virtual void SampleASegment(int* start, int* end);
  virtual bool detectClashes();
  virtual int  countClashes();
  virtual double calcEnergy();
  virtual double calcEnergy(vector<RESIDUE>& protein, int start);
  virtual double calcEnergy2(vector<RESIDUE>& protein, int start);
  virtual void Undo(int start, int end);
  virtual void KeepStructure();
  
  virtual void ReAngle();

  virtual void writeDecoy(string& resultPDBfile);
  void mergeDecoy(string& pdbfile, vector<Vector2>& g_angle, 
                  vector<double> & bondlenList, vector<Vector3>& initCAs);

protected:
  void writeDecoyAtom( ofstream& ofile, int res_seq, string residue,
                       int atm_seq, string atom, double x, double y, double z);

  // the sampler for the unit vectors, each unit vector corresponds to a pair
  // of theta and tau
  FB5Cluster* mFB5Sampler;

  //the unit vectors
  vector<Vector3 > mFBVec;
};

//this class optimizes the structure in the phi/psi space
class PhiPsiOptimizer: public ConformationOptimizer
{
public:
  PhiPsiOptimizer();
  PhiPsiOptimizer(string& proteinName);
  ~PhiPsiOptimizer();
  
  virtual void Initialize(int randomSeed, int DEBUG_MODE, string additional_f);
  
  virtual void SampleAngles(int start, int end);
  virtual void BuildStructure(int start, int end);
  virtual void ApplyContactConstraint(){};
  //virtual void SampleASegment(int* start, int* end);
  virtual bool detectClashes();
  virtual int  countClashes();
  virtual double calcEnergy();
  virtual void Undo(int start, int end);
  virtual void KeepStructure();
  virtual void ReAngle(){};
  
  virtual void writeDecoy(string& resultPDBfile);
  
protected:
  //it is used to sample a pair of phi  and psi from a distribution
  MCsampler* mAngleSampler;

  //each cluster corresponds to a hidden state in the CRF model
  PhiPsiCluster* mPhiPsiCluster;
  PhiPsiCluster* mPsiCluster;
  PhiPsiCluster* mPhiCluster;

  // the Phi and Psi angles, which should be  between -PI and PI
  // (i.e., radian degree)
  vector<Vector2 > mAngles;
  vector<Vector2 > mBestAngles;

  //Protein is a class defined in the BALL library, used to
  // store the sampled structure
  //Protein contains much more information than mStructure
  Protein* mProtein;
  Protein* mBestProtein;
};

//this is a helper function, not sure where to put
//get all the Ca atom coordinates from a Protein 
void getCalphaPositions(Protein*, vector<Vector3 >& );

#endif
